<?php

namespace App\Http\Controllers;

use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Http\Models\TicketMapping;
use Illuminate\Http\Request;

class TicketBookingController extends BaseController
{
	public function changeBookingPartner($ticketBookingId, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			if ($ticketBooking->party_date_time >= time())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to change partner as the party is still not over [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			if ($ticketBooking->ticket->status_id != config('evibe.ticket.status.booked'))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Ticket is not in the booked status to change partner [Ticket Id: ' . $ticketBooking->ticket->id . ']'
				                        ]);
			}

			$oldPartnerId = $ticketBooking->map_id;
			$oldPartnerTypeId = $ticketBooking->mp_type_id;

			$partnerId = $request['partnerId'];
			$partnerTypeId = $request['partnerTypeId'];

			if (!$partnerId || !$partnerTypeId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide required partner details'
				                        ]);
			}

			$partnerCheck = $this->validatePartner($partnerId, $partnerTypeId);

			if (!($partnerCheck['success']))
			{
				return response()->json($partnerCheck);
			}
			else
			{
				$partner = $partnerCheck['partner'];
			}

			if (($oldPartnerId != $partnerId) || ($oldPartnerTypeId != $partnerTypeId))
			{
				$ticketBooking->update([
					                       'map_id'             => $partnerId,
					                       'map_type_id'        => $partnerTypeId,
					                       'is_partner_changed' => 1
				                       ]);

				return response()->json([
					                        'success'         => true,
					                        'ticketBookingId' => $ticketBookingId
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to change partner as the old partner and new partner are same [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}

	public function updateBookingDetails($ticketBookingId, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$x = json_decode($request);

			//$x = array_count_values($request);
			return $x;

		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}

	public function addBooking($ticketId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticket = Ticket::find($ticketId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket [T Id: ' . $ticket . ']'
				                        ]);
			}

			if(!$ticket->status_id == config('evibe.ticket.status.booked'))
			{
				return response()->json([
					'success' => false,
					'error' => 'Ticket is not ina valid state to add booking [Ticket Id: '.$ticketId.']'
				                        ]);
			}



			$ticketMapping = TicketMapping::create([
				                                       'ticket_id'   => $ticketId,
				                                       'map_id'      => $mapId,
				                                       'map_type_id' => config('evibe.ticket.type.decor'),
				                                       'created_at'  => date('Y-m-d H:i:s'),
				                                       'updated_at'  => date('Y-m-d H:i:s')
			                                       ]);

			$bookingInsertData = [
				'booking_id'                  => $enquiryId,
				'booking_info'                => $bookingInfo,
				'product_price'               => $decor->min_price,
				'booking_amount'              => $totalBookAmount,
				'advance_amount'              => $tokenAdvance,
				'estimated_transport_charges' => $estimatedTransCharges,
				'transport_charges'           => $transCharges,
				'map_id'                      => $decor->provider->id,
				'map_type_id'                 => $mapTypeId,
				'ticket_id'                   => $ticketId,
				'ticket_mapping_id'           => $ticketMappingId,
				'is_venue_booking'            => 0,
				'type_ticket_booking_id'      => $typeTicketBookingId,
				'booking_type_details'        => ucwords(config('evibe.auto-book.' . $ticketTypeId)),
				'created_at'                  => date('Y-m-d H:i:s'),
				'updated_at'                  => date('Y-m-d H:i:s')
			];

			$ticketBooking = TicketBooking::create([
				'ticket_id' => $ticketId,
			                                       ]);
		} catch (\Exception $exception)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}
}