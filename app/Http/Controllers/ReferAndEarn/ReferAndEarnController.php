<?php

namespace App\Http\Controllers\ReferAndEarn;

use App\Http\Controllers\BaseController;
use App\Exceptions\CustomException;
use App\Http\Models\Coupon\Coupon;
use App\Http\Models\Coupon\CouponUsers;
use App\Http\Models\ReferAndEarn\LogReferralFriendActions;
use App\Http\Models\ReferAndEarn\LogReferralPreActions;
use App\Http\Models\ReferAndEarn\UserSpecialDate;
use App\Http\Models\Ticket;
use App\Http\Models\User;
use App\Jobs\Mail\ReferEarn\CustomerFriendSignUpSuccess;
use App\Jobs\Mail\ReferEarn\ReferralFriendSignUpSuccess;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class ReferAndEarnController extends BaseController
{
	public function showUserReferAndEarnStatistics($userId, Request $request)
	{
		$tokenUserId = $this->checkForAccessToken($request);

		if ($userId != $tokenUserId)
		{
			throw new CustomException("URL not found, Please cross check the URL you have entered.", 403);
		}

		$user = User::find($userId);
		if ($user)
		{
			$couponIds = CouponUsers::where("user_id", $userId)->pluck("coupon_id")->toArray();
			$coupons = Coupon::select("id", "coupon_code", "offer_start_time", "offer_end_time", "min_order", "discount_amount", "discount_percent", "max_discount_amount")
			                 ->whereIn("id", $couponIds)
			                 ->get()
			                 ->toArray();

			$friendActions = LogReferralFriendActions::where("customer_id", $userId)->get();
			$preActions = LogReferralPreActions::where("customer_id", $userId)->get();

			$statistics = [
				"signUpCount" => $friendActions->where("type_referral_event", "signup")->count(),
				"bookedCount" => $friendActions->where("type_referral_event", "book")->count(),
				"sharesCount" => $preActions->where("type_action", "share")->count(),
				"viewsCount"  => $preActions->where("type_action", "view")->count()
			];

			return response()->json([
				                        "success"     => true,
				                        "referralUrl" => config("evibe.live.host") . "/re/" . $user->referral_code,
				                        "statistics"  => $statistics,
				                        "coupons"     => $coupons
			                        ]);
		}
		else
		{
			return $this->returnFalseJsonResponse("User not found, Please sign up to create an account");
		}
	}

	public function generateUniqueReferralLink(Request $request)
	{
		$this->checkForAccessToken($request);

		try
		{
			$ticket = Ticket::find($request["ticketId"]);
			if (!$ticket)
			{
				return $this->returnFalseJsonResponse("Error Occurred while fetching your data, Please reload the page & try again");
			}

			$userName = $ticket->email;
			$phone = $ticket->phone;

			$user = User::withTrashed()->where("username", "LIKE", "%" . $userName . "%")->get();
			$existingReferralCodes = User::all()->pluck("referral_code")->toArray();

			if ($user->count() == 0)
			{
				// create user
				$user = User::updateOrCreate([
					                             "username" => $userName
				                             ],
				                             [
					                             "name"       => $ticket->name,
					                             "phone"      => $phone,
					                             "password"   => Hash::make("$" . $phone),
					                             "role_id"    => config("evibe.role.customer"),
					                             "updated_at" => Carbon::now()
				                             ]);
			}
			elseif ($user->count() == 1)
			{
				$user = $user->first();
			}
			elseif ($user->count() > 1)
			{
				Log::info("More than one user found. Email: " . $ticket->email . ", Ticket id: " . $ticket->id);

				$user = $user->first();
			}
			else
			{
				$user = null;

				// error log
				Log::info("Exception while finding user with email . " . $ticket->email . ", Ticket id: " . $ticket->id);
			}

			// Checking if the user already exists
			if (!is_null($user))
			{
				$referralCode = $user->referral_code;

				if (!$referralCode && is_null($referralCode))
				{
					$referralCode = $this->generateReferralCode($user->name, array_unique($existingReferralCodes));

					$user->update([
						              "referral_code" => $referralCode
					              ]);
				}

				$hashContent = str_replace(' ', '', $user->id . $user->name);
				$userToken = Hash::make((string)$hashContent);

				if ($user->deleted_at)
				{
					Log::info("Deleted user issue, user Id: " . $user->id);
				}

				return response()->json([
					                        "success"          => true,
					                        "userId"           => $user->id,
					                        "referralUrl"      => config("evibe.live.host") . "/re/" . $user->referral_code,
					                        "userAutoLoginUrl" => config("evibe.live.host") . "/my/re/share?id=" . $user->id . "&token=" . $userToken
				                        ]);
			}
			else
			{
				return $this->returnFalseJsonResponse("Error Occurred while fetching your data, Please reload the page & try again");
			}
		} catch (\Exception $e)
		{
			Log::info("Error occured while creating user for ticket Id:" . $request["ticketId"]);
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function submitFriendSignup(Request $request)
	{
		$this->checkForAccessToken($request);

		try
		{
			$rules = [
				'firstName'    => 'required',
				'phone'        => 'required|phone',
				'email'        => 'required|email',
				'referralCode' => 'required'
			];

			$messages = [
				'firstName.required'    => 'Please enter you name',
				'email.required'        => 'Please enter your email address',
				'email.email'           => 'Please enter a valid email address',
				'phone.required'        => 'Please enter your phone number',
				'phone.phone'           => 'Please enter a valid phone number',
				'referralCode.required' => 'Referral code not found'
			];

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return $this->returnFalseJsonResponse($validation['error']);
			}

			$userName = $request["email"];
			$phone = $request["phone"];
			$referralCode = $request["referralCode"];
			$request["lastName"] = isset($request["lastName"]) ? $request["lastName"] : null;

			$users = User::select("referral_code", "referred_by", "username", "id")->get()->toArray();
			$referralUserId = array_search($referralCode, array_column($users, "referral_code", "id"));

			// Checking for the referral code in the DB
			if ($referralUserId && !is_null($referralUserId) && $referralUserId > 0)
			{
				$referralIds = array_column($users, "referred_by", "username");

				// Checking if the user already exists
				$oldUsername = array_key_exists($userName, $referralIds);
				if ($oldUsername)
				{
					// Checking if the user is already referred or not
					if (is_null($referralIds[$userName]))
					{
						$invalidRoleIds = [];

						$user = User::where("username", $userName)
						            ->whereNotIn("role_id", $invalidRoleIds)
						            ->first();

						// Checking if the user is in selected role ID's
						if ($user)
						{
							// Checking if the same user tried to signing up
							if ($user->id != $referralUserId)
							{
								$user->update([
									              "first_name"  => $request["firstName"],
									              "last_name"   => $request["lastName"],
									              "referred_by" => $referralUserId,
									              "updated_at"  => Carbon::now()
								              ]);

								if (count($request["specialDates"]) > 0)
								{
									foreach ($request["specialDates"] as $specialDate)
									{
										UserSpecialDate::create([
											                        "user_id"                       => $user->id,
											                        "type_user_special_occasion_id" => $specialDate["occasion"],
											                        "type_user_special_relation_id" => $specialDate["relation"],
											                        "date"                          => $specialDate["date"]
										                        ]);
									}
								}

								return $this->returnSuccessReferralLink($user, array_column($users, "referral_code", "username"));
							}
							else
							{
								return $this->returnFalseJsonResponse("User cannot refer himself, Please use another email address");
							}
						}
						else
						{
							return $this->returnFalseJsonResponse("Cannot sign up with this email due to Evibe.in T&C, Please use another email address.");
						}
					}
					else
					{
						return $this->returnFalseJsonResponse("This email was already referred.");
					}
				}
				else
				{
					$user = User::create([
						                     "first_name"  => $request["firstName"],
						                     "last_name"   => $request["lastName"],
						                     "referred_by" => $referralUserId,
						                     "name"        => $request["firstName"] . " " . $request["lastName"],
						                     "username"    => $userName,
						                     "phone"       => $phone,
						                     "password"    => Hash::make("$" . $phone),
						                     "role_id"     => config("evibe.role.customer")
					                     ]);

					if (count($request["specialDates"]) > 0)
					{
						foreach ($request["specialDates"] as $specialDate)
						{
							UserSpecialDate::create([
								                        "user_id"                       => $user->id,
								                        "type_user_special_occasion_id" => $specialDate["occasion"],
								                        "type_user_special_relation_id" => $specialDate["relation"],
								                        "date"                          => $specialDate["date"]
							                        ]);
						}
					}

					return $this->returnSuccessReferralLink($user, array_column($users, "referral_code", "username"));
				}
			}
			else
			{
				return $this->returnFalseJsonResponse("Referral code not found, Please check the URL & try again");
			}
		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function returnSuccessReferralLink($user, $existingReferralCodes)
	{
		$code = $this->generateReferralCode($user->name, $existingReferralCodes);

		$user->update([
			              "referral_code" => $code
		              ]);

		// Increase the sign up count of the customer
		LogReferralFriendActions::create([
			                                 "customer_id"         => $user->referred_by,
			                                 "friend_id"           => $user->id,
			                                 "type_referral_event" => "signup"
		                                 ]);

		$couponCode = $this->createCoupon([
			                                  "couponPrefix"   => "RE",
			                                  "stringLength"   => 8,
			                                  "discountAmount" => "200",
			                                  "minOrder"       => "2000",
			                                  "offerEndTime"   => Carbon::now()->addYear(),
			                                  "description"    => "Refer & Earn Friend Signup Coupon",
			                                  "userId"         => $user->id
		                                  ]);

		if ($couponCode)
		{
			$customer = User::where('id', $user->referred_by)->first();

			$hashContent = str_replace(' ', '', $customer->id . $customer->name);
			$userToken = Hash::make((string)$hashContent);
			$dashBoardUrl = config("evibe.live.host") . "/my/re/share?id=" . $customer->id . "&token=" . $userToken . "&ref=email";
			$coupon = Coupon::where("coupon_code", $couponCode)->first();

			$data = [
				"referredBy"         => $user->referred_by,
				"referralCode"       => $couponCode,
				"name"               => $user->name,
				"email"              => $user->username,
				"dashboardUrl"       => $dashBoardUrl,
				"whatsappShareLink"  => $dashBoardUrl . "#EmailWhatsapp",
				"messengerShareLink" => $dashBoardUrl . "#EmailMessenger",
				"smsShareLink"       => $dashBoardUrl . "#EmailSMS",
				"validUpto"          => $coupon ? date('d M Y', strtotime($coupon->offer_end_time)) : ""
			];

			$this->dispatch(new CustomerFriendSignUpSuccess($data));
			$this->dispatch(new ReferralFriendSignUpSuccess($data));

			return response()->json([
				                        "success"     => true,
				                        "redirectUrl" => config("evibe.live.host") . "/re/friend/thankyou/" . $code . "/" . $couponCode
			                        ]);
		}
		else
		{
			return $this->returnFalseJsonResponse("Internal error occurred while generating your coupon code, Please contact us for further details");
		}
	}

	public function generateReferralCode($name, $codes)
	{
		$startName = substr(preg_replace("/[^a-zA-Z]+/", "", $name), 0, 3);

		if (strlen($startName) < 3)
		{
			$startName = $this->generateRandomString(3);
		}

		$code = $startName . rand("10000", "99999");

		if (in_array($code, $codes))
		{
			return $this->generateReferralCode($name, $codes);
		}
		else
		{
			return $code;
		}
	}
}