<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Models\AccessToken;
use App\Http\Models\AddOn\AddOn;
use App\Http\Models\AddOn\AddOnTags;
use App\Http\Models\AuthClient;
use App\Http\Models\AvailCheck\AvailabilityCheck;
use App\Http\Models\Coupon\Coupon;
use App\Http\Models\Coupon\CouponUsers;
use App\Http\Models\Notification;
use App\Http\Models\PackageTag;
use App\Http\Models\SiteError;
use App\Http\Models\Tags;
use App\Http\Models\TicketMapping;
use App\Http\Models\Util\ProductSortOrder;
use App\Http\Models\Util\StarOption;
use App\Http\Models\Venue;
use App\Http\Models\TypeEvent;
use App\Jobs\Notification\mailGenericErrorToAdmin;
use App\Http\Models\Cake;
use App\Http\Models\City;
use App\Http\Models\Decor;
use App\Http\Models\Package;
use App\Http\Models\Planner;
use App\Http\Models\TicketUpdate;
use App\Http\Models\Trends;
use App\Http\Models\TypeService;
use App\Http\Models\VenueHall;
use Carbon\Carbon;
use Evibe\Utility\TicketStatus;
use Evibe\Utility\TypeTicketUtility;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\URL;
use Laravel\Lumen\Routing\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use GuzzleHttp\Exception\ClientException;

class BaseController extends Controller
{
	protected $utility;

	function __construct()
	{
		$this->utility = \app()->make('Utility');

		// Tracking serve CPU utilization
		Log::info(URL::current());
	}

	protected function checkForAccessToken(Request $request)
	{
		if (!$request->header('access-token'))
		{
			throw new CustomException("Invalid access token", 403);
		}

		$accessToken = AccessToken::find($request->header('access-token'));

		if (!$accessToken)
		{
			throw new CustomException("Invalid access token", 403);
		}

		return $accessToken->user_id;
	}

	protected function validateJSONPayload(Request $request, $rules, $messages = [])
	{
		$validator = Validator::make($request->json()->all(), $rules, $messages);

		if ($validator->fails())
		{
			throw new CustomException($validator->errors()->first(), 422);
		}
	}

	protected function validateJSONData(Request $request, $rules, $messages = [])
	{
		$validator = Validator::make($request->json()->all(), $rules, $messages);

		if ($validator->fails())
		{
			return $response = [
				'success' => false,
				'error'   => $validator->errors()->first() ? $validator->errors()->first() : 'Some error occurred'
			];
		}

		return $response['success'] = true;
	}

	protected function validatePayload(Request $request, $rules)
	{
		$validator = Validator::make($request->all(), $rules, ['boolean' => 'The :attribute field must be 0 or 1']);

		if ($validator->fails())
		{
			throw new CustomException($validator->errors()->first(), 422);
		}
	}

	protected function validateArray($array, $rules)
	{
		$validator = Validator::make($array, $rules, ['boolean' => 'The :attribute field must be 0 or 1']);

		if ($validator->fails())
		{
			throw new CustomException($validator->errors()->first(), 422);
		}
	}

	protected function validateClientId($clientId)
	{
		// Checking if clientId is valid
		if (!$authClient = AuthClient::find($clientId))
		{
			throw new CustomException("Invalid client id.", 401);
		}

		return $authClient;
	}

	protected function checkForPagination($request)
	{
		$limit = 0;

		if ($request->has('limit'))
		{
			$limit = $request->get('limit');
		}

		$offset = 0;

		if ($request->has('offset') && $request->get('offset') > 0)
		{
			$offset = $request->get('offset');
		}

		return
			[
				"limit"  => $limit,
				"offset" => $offset
			];
	}

	public function formatPrice($price)
	{
		$explrestUnits = "";
		$paise = "";

		if (strpos($price, '.'))
		{
			$numArray = explode('.', $price);
			$paise = $numArray[1];
			$price = $numArray[0];
		}

		if (strlen($price) > 3)
		{
			$lastthree = substr($price, strlen($price) - 3, strlen($price));
			$restunits = substr($price, 0, strlen($price) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestUnits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestUnits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestUnits . $lastthree;

		}
		else
		{
			$thecash = $price;
		}

		if ($paise)
		{
			$thecash .= '.' . $paise;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function camelCaseToSnakeCaseArrayAttibutes($array)
	{
		$return = [];

		foreach ($array as $key => $value)
		{
			$transformedKey = snake_case($key);

			if (is_array($value))
			{
				$return[$transformedKey] = $this->camelCaseToSnakeCaseArrayAttibutes($value);
			}
			else
			{
				$return[$transformedKey] = $value;
			}
		}

		return $return;
	}

	protected function stringifyPriceFormat($price)
	{
		return (string)$this->formatPrice($price);
	}

	public function getShortUrl($longUrl, $debug = false)
	{
		$shortUrl = $longUrl;

		if (!strlen($longUrl))
		{
			Log::error("empty link sent, debug message: " . $debug);

			return $shortUrl;
		}

		if (strpos($longUrl, "goo.gl") === false)
		{
			try
			{
				//$shortUrl = $this->getEvibesShortLink($longUrl);
				$shortUrl = $this->getGooGlShortLink($longUrl);
				Log::info("return short url: $shortUrl");

			} catch (\Exception $e)
			{
				Log::error("ERROR:: While using the bitly url short with link " . $longUrl);
			}
		}

		return $shortUrl;
	}

	private function getEvibesShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ((strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			$url = "http://evib.es/api/v2/action/shorten";
			$method = "POST";
			$accessToken = "";
			$jsonData = [
				'url'           => $longUrl,
				'key'           => config("evibe.evibes.access_token"),
				'custom_ending' => '',
				'is_secret'     => false,
				'response_type' => 'json'
			];

			try
			{
				$client = new Client();
				$res = $client->request($method, $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $jsonData,
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);
				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;

				$this->saveError([
					                 'fullUrl' => request()->fullUrl(),
					                 'message' => 'Error occurred in Base controller while doing guzzle request',
					                 'code'    => 0,
					                 'details' => $res['error']
				                 ]);

				return false;
			}
		}

		return $shortUrl;
	}

	private function getGooGlShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ($longUrl && (strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			try
			{
				/* @see: changing goo.gl to bit.ly
				$baseUrl = "https://www.googleapis.com/urlshortener/v1/url?key=" . config("evibe.google.shortener_key");

				$postData = ['longUrl' => $longUrl];
				$jsonData = json_encode($postData);

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, ['Content-type:application/json']);
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

				$response = curl_exec($curlObj);

				// Change the response json string to object
				curl_close($curlObj);

				$shortUrl = \GuzzleHttp\json_decode($response)->id;
				 */

				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if (time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);
				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => 'Bitly Url Shortener',
						                                     'method'    => 'GET',
						                                     'message'   => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'exception' => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'trace'     => print_r($response, true),
						                                     'details'   => print_r($response, true)
					                                     ]);
				}
			} catch (\Exception $exception)
			{
				$this->sendAndSaveReport($exception);
			}
		}

		return $shortUrl;
	}

	protected function uploadImageToServer($imageFile, $imageUploadTo, $isOptimize = true, $isCompress = true)
	{
		$parentDirs = config('evibe.root.gallery');
		$fullUploadTo = $parentDirs . $imageUploadTo; // append with parent dirs
		$imageName = $imageFile->getClientOriginalName();
		$originalName = str_replace(" ", "", $imageName);
		$newImageName = trim(time() . '-' . $originalName);

		if (!file_exists($fullUploadTo))
		{
			$oldumask = umask(0);
			mkdir($fullUploadTo, 0777, true);
			umask($oldumask);
		}

		try
		{
			// compress image
			if ($isCompress)
			{
				$this->compressImage($imageFile);
			}

			// move to the correct place
			$imageFile->move($fullUploadTo, $newImageName);
			if ($isOptimize)
			{
				$this->optimizeImages($fullUploadTo, $newImageName);
			}

			return $newImageName;

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e, false);
			Log::error("Exception with uploading image: " . $fullUploadTo . $newImageName);
		}

		return false; // failed case
	}

	private function optimizeImages($basePath, $imageName)
	{
		$defaultWidth = 640;
		$defaultHeight = null;
		$types = [
			"profile" => ['width' => 640],
			"results" => ['width' => 394],
			"thumbs"  => ['width' => 80]
		];

		$newImagePath = $basePath . $imageName;

		$img = Image::make($newImagePath);
		foreach ($types as $type => $values)
		{
			$optImgPath = $basePath . $type . "/";

			if (!file_exists($optImgPath))
			{
				mkdir($optImgPath, 0777, true);
			}

			$width = array_key_exists("width", $values) ? $values["width"] : $defaultWidth;
			$height = array_key_exists("height", $values) ? $values["height"] : $defaultHeight;

			$img->resize($width, $height, function ($constraint) {
				$constraint->aspectRatio();
				$constraint->upsize();
			});
			try
			{
				$img->save($optImgPath . $imageName);
			} catch (\Exception $e)
			{
				$this->sendAndSaveReport($e);
				Log::error("Exception with saving optimised image: " . $optImgPath . $imageName);
			}
		}
	}

	protected function getMappingData($ticketMapping, $isCollection = false)
	{
		if ($isCollection)
		{
			return $this->fillMappingValues([
				                                'isCollection' => $isCollection,
				                                'mapTypeId'    => $ticketMapping->map_type_id
			                                ]);
		}
		else
		{
			return $this->fillMappingValues([
				                                'isCollection' => $isCollection,
				                                'mapTypeId'    => $ticketMapping->map_type_id,
				                                'mapId'        => $ticketMapping->map_id
			                                ]);
		}
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  27 Oct 2016
	 *
	 * Using OneSignal (onesignal.com) to send web push notifications
	 */
	protected function sendWebPushNotification($data)
	{
		$notificationTitle = isset($data['title']) ? $data['title'] : "New Notification";
		$notificationMessage = isset($data['message']) ? $data['message'] : "New message from Evibe.in APIs.";
		$includedSegments = array_merge(config("onesignal.web.segments.crm"), config("onesignal.web.segments.admins"));

		// post request
		$headers = [
			"Content-Type"  => "application/json",
			"Authorization" => "Basic " . config("onesignal.web.ApiKey")
		];

		$payload = [
			"app_id"            => config("onesignal.web.appId"),
			"included_segments" => $includedSegments,
			"headings"          => [
				"en" => $notificationTitle
			],
			"contents"          => [
				"en" => $notificationMessage
			]
		];

		$oneSignalApi = config("onesignal.path") . "/notifications";
		$client = new Client();

		$response = $client->post($oneSignalApi, [
			"headers" => $headers,
			"json"    => $payload
		]);
	}

	// Mapping values function shifted from DASH at 28th December

	public function fillMappingValues($options)
	{
		$mappingValues = $mappers = [];
		$hasProvider = false;
		$mapTypeId = $options['mapTypeId'];
		$typeId = $options['mapTypeId'];
		$mapId = array_key_exists('mapId', $options) ? $options['mapId'] : false;
		$mapType = "";

		/**
		 * @see whereNull is required as using $someVariable = Vendor; results
		 * @see `mapTypeId` is matched against `type_ticket`  in "evibe.php" config file
		 *      which is in sync with database table type_ticket
		 */

		switch ($mapTypeId)
		{
			// packages
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.resort'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.lounge'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.couple-experiences'):
			case config('evibe.ticket.type.generic-package'):
			case config('evibe.ticket.type.venue-deals'):
				$mappers = $mapId ? Package::with('provider')->find($mapId) : Package::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "packages";
				break;

			// planners
			case 2:
				$mappers = $mapId ? Planner::find($mapId) : Planner::where('is_live', 1)->get();
				$mapType = "vendors";
				break;

			// venues (aka halls)
			case 3:
				$venueHalls = VenueHall::with('venue');
				$mappers = $mapId ? $venueHalls->find($mapId) : VenueHall::with('venue')->get();
				$mapType = "venues";
				break;

			// services
			case 4:
				$mappers = $mapId ? TypeService::find($mapId) : TypeService::all();
				$hasProvider = false;
				$mapType = "services";
				break;

			// trends
			case 5:
				$mappers = $mapId ? Trends::with('provider')->find($mapId) : Trends::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "trends";
				break;

			// cakes
			case 6:
				$mappers = $mapId ? Cake::with('provider')->find($mapId) : Cake::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "cakes";
				break;

			// decors
			case 13:
				$mappers = $mapId ? Decor::with('provider')->find($mapId) : Decor::where('is_live', 1)->get();
				$hasProvider = true;
				$mapType = "decors";
				break;

			// add-ons
			case config('evibe.ticket.type.add-on'):
				$mappers = $mapId ? AddOn::withTrashed()->find($mapId) : AddOn::where('is_live', 1)->get();
				$hasProvider = false;
				$mapType = "add-ons";
		}

		if ($options['isCollection'])
		{
			foreach ($mappers as $mapper)
			{
				if (!$mapper)
				{
					continue;
				}

				$basicVal = [
					'code'    => $mapper->code,
					'name'    => $mapTypeId == TypeTicketUtility::CAKE ? $mapper->title : ($mapper->name ? $mapper->name : ""),
					'type'    => $mapType,
					'deleted' => $mapper->deleted_at,
					'info'    => $mapper->info
				];

				// push hall only if venues exists
				if ($mapTypeId == TypeTicketUtility::VENUE && $mapper->venue && $mapper->venue->is_live == 1)
				{
					array_set($basicVal, "name", $mapper->venue->name . " - " . $mapper->name);
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->venue->price_min_veg,
						'person' => $mapper->venue->person,
						'phone'  => $mapper->venue->phone,
						'userId' => $mapper->venue->user_id
					]);
				}
				elseif ($mapTypeId == TypeTicketUtility::SERVICE || $mapTypeId == TypeTicketUtility::ENTERTAINMENT)
				{
					$servicePrice = $this->formatPrice($mapper->min_price);
					$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'code'   => $mapper->name,
						'name'   => substr($mapper->quote_info, 0, 60) . '...',
						'price'  => $servicePrice,
						'person' => 'Evibe',
						'phone'  => config('evibe.phone_plain'),
					]);
				}
				elseif ($mapTypeId == TypeTicketUtility::DECOR)
				{
					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->min_price,
						'person' => $mapper->provider->person,
						'phone'  => $mapper->provider->phone,
						'userId' => $mapper->provider->user_id
					]);
				}
				else
				{
					// packages exists, but vendors is not active
					$isValid = $hasProvider ? $mapper->provider : $mapper->person;
					if (!$isValid)
					{
						continue;
					}

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price'  => $mapper->price,
						'person' => $hasProvider ? $mapper->provider->person : $mapper->person,
						'phone'  => $hasProvider ? $mapper->provider->phone : $mapper->phone,
						'userId' => $hasProvider ? $mapper->provider->user_id : $mapper->user_id,
					]);
				}
			}
		}
		else
		{
			/**
			 * @todo generate public link based on mapType
			 * @todo venues and vendors should be separated for viewing
			 */
			$mapper = $mappers;
			$forLink = $mapType;
			$liveHost = $this->getGalleryBaseUrl();

			// load default values
			$mappingValues = [
				'id'            => '',
				'code'          => '',
				'name'          => '',
				'price'         => '',
				'info'          => '',
				'worth'         => '',
				'pic'           => '',
				'deleted'       => '',
				'provider'      => [
					'name'     => '',
					'person'   => '',
					'phone'    => '',
					'email'    => '',
					'location' => '',
					'userId'   => '',
					'altPhone' => '',
					'self'     => null
				],
				'link'          => '',
				'url'           => '',
				'cityUrl'       => '',
				'publicLink'    => '',
				'type'          => '',
				'venueDelivery' => 0,
				'self'          => '',
			];

			// package & related
			$packageTypes = [
				config('evibe.ticket.type.packages'),
				config('evibe.ticket.type.venue-deals'),
				config('evibe.ticket.type.priests'),
				config('evibe.ticket.type.tents'),
				config('evibe.ticket.type.food'),
				config('evibe.ticket.type.couple-experiences'),
				config('evibe.ticket.type.resort'),
				config('evibe.ticket.type.villa'),
				config('evibe.ticket.type.lounge'),
				config('evibe.ticket.type.generic-package')
			];
			if (in_array($mapTypeId, $packageTypes))
			{
				$mapTypeId = config('evibe.ticket.type.packages');
			}

			if ($mapper)
			{
				// special case for tickets raised for services and add-ons
				if ($mapTypeId == config('evibe.ticket.type.add-on'))
				{
					$mappingValues = [
						'id'            => $mapper->id,
						'name'          => $mapper->name,
						'code'          => $mapper->code,
						'price'         => $mapper->price_per_unit,
						'worth'         => $mapper->price_worth_per_unit,
						'maxUnits'      => $mapper->max_units,
						'info'          => $mapper->info,
						'venueDelivery' => $mapper->venue_delivery,
						'deleted'       => $mapper->deleted_at,
						'type'          => $mapType,
					];
				}
				elseif ($mapTypeId == TypeTicketUtility::SERVICE || $mapTypeId == TypeTicketUtility::ENTERTAINMENT)
				{
					$cityUrl = City::find(1)->url;
					$servicePrice = 'Rs. ' . $this->formatPrice($mapper->min_price);
					$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues = [
						'id'         => $mapper->id,
						'name'       => $mapper->name ? $mapper->name : "",
						'code'       => $mapper->name,
						'price'      => $servicePrice,
						'worth'      => $mapper->worth_price,
						'range_info' => $mapper->range_info,
						'info'       => $mapper->info,
						'max_price'  => $mapper->max_price,
						'deleted'    => $mapper->deleted_at,
						'link'       => '/' . $forLink . '/details/' . $mapper->id,
						'type'       => $mapType,
						'publicLink' => $this->getGalleryBaseUrl() . '/' . $cityUrl . '/' . config('evibe.live.profile_url.entertainment') . '/' . $mapper->url,
						'pic'        => $mapper->getProfilePic()
					];
				}
				elseif ($mapTypeId == TypeTicketUtility::VENUE)
				{
					if (!$mapper->venue)
					{
						return $mappingValues;
					}

					// venues (aka halls)
					$cityUrl = $mapper->venue->city ? $mapper->venue->city->url : "bangalore"; // default city
					$typeProfileUrl = config('evibe.live.profile_url.venues');
					$publicLink = $liveHost . '/' . $cityUrl . '/' . $typeProfileUrl . '/' . $mapper->url;

					$mappingValues = [
						'id'         => $mapper->id,
						'code'       => $mapper->code,
						'name'       => $mapper->name ? $mapper->name : "",
						'info'       => $mapper->venue->info,
						'tax'        => $mapper->venue->tax_percent,
						'worth'      => $mapper->getPriceWorth(),
						'price'      => $mapper->venue->price_min_veg,
						'pic'        => $mapper->getProfilePic(),
						'deleted'    => $mapper->deleted_at,
						'provider'   => [
							'name'     => $mapper->venue->name,
							'person'   => $mapper->venue->person,
							'phone'    => $mapper->venue->phone,
							'email'    => $mapper->venue->email,
							'location' => $mapper->venue->area->name,
							'userId'   => $mapper->venue->user_id,
							'altPhone' => $mapper->venue->alt_phone,
							'self'     => $mapper->venue
						],
						'link'       => '/' . $forLink . '/hall/' . $mapper->id,
						'url'        => $mapper->url,
						'cityUrl'    => $cityUrl,
						'publicLink' => $publicLink,
						'type'       => ucfirst($mapType),
						'self'       => $mapper
					];
				}
				else
				{
					// for decors / cakes / trends / packages / planners
					$mapName = ($mapTypeId == TypeTicketUtility::CAKE) ? $mapper->title : ($mapper->name ? $mapper->name : "");
					$cityUrl = $hasProvider ? $mapper->provider ? $mapper->provider->city->url : 'Not Available' : $mapper->city->url;
					$typeProfileUrl = config('evibe.live.profile_url.' . $mapType);
					$publicLink = $liveHost . '/' . $cityUrl . '/' . $typeProfileUrl . '/' . $mapper->url;
					$linkForDash = '/' . $forLink . '/view/' . $mapper->id;

					if ($mapTypeId == TypeTicketUtility::DECOR)
					{
						$price = $mapper->min_price;
						$prerequisites = $mapper->more_info;
					}
					else
					{
						$price = $mapper->price;
						$prerequisites = $mapper->prerequisites;
					}

					if ($mapTypeId == TypeTicketUtility::CAKE || $mapTypeId == TypeTicketUtility::DECOR)
					{
						$linkForDash = '/' . $forLink . '/' . $mapper->id . '/info';
					}
					//@see changed the local dash link due to the categories in packages type.
					if ($mapTypeId == TypeTicketUtility::PACKAGE)
					{
						if ($mapper->map_type_id == TypeTicketUtility::PLANNER)
						{
							$linkForDash = $this->getGalleryBaseUrl() . '/packages/vendor/view/' . $mapper->id;
						}
						elseif ($mapper->map_type_id == TypeTicketUtility::ARTIST)
						{
							$linkForDash = $this->getGalleryBaseUrl() . '/packages/venue/view/' . $mapper->id;
						}
						elseif ($mapper->map_type_id == TypeTicketUtility::VENUE)
						{
							$linkForDash = $this->getGalleryBaseUrl() . '/packages/artist/view/' . $mapper->id;;
						}
					}

					$mappingValues = [
						'id'            => $mapper->id,
						'code'          => $mapper->code,
						'name'          => $mapName,
						'price'         => $price,
						'info'          => $mapper->info,
						'prerequisites' => $prerequisites,
						'facts'         => $mapper->facts,
						'worth'         => $mapper->getPriceWorth(),
						'pic'           => $mapper->provider ? $mapper->getProfilePic() : null,
						'deleted'       => $mapper->deleted_at,
						'link'          => $linkForDash,
						'provider'      => [
							'name'     => $hasProvider ? $mapper->provider ? $mapper->provider->name : '' : $mapper->name,
							'person'   => $hasProvider ? $mapper->provider ? $mapper->provider->person : '' : $mapper->person,
							'phone'    => $hasProvider ? $mapper->provider ? $mapper->provider->phone : '' : $mapper->phone,
							'email'    => $hasProvider ? $mapper->provider ? $mapper->provider->email : '' : $mapper->email,
							'location' => $hasProvider ? $mapper->provider ? $mapper->provider->area->name : '' : $mapper->area->name,
							'userId'   => $hasProvider ? $mapper->provider ? $mapper->provider->user_id : '' : $mapper->user_id,
							'altPhone' => $hasProvider ? $mapper->provider ? $mapper->provider->alt_phone : '' : '',
							'self'     => $hasProvider ? $mapper->provider : null,
						],
						'url'           => $mapper->url,
						'cityUrl'       => $cityUrl,
						'publicLink'    => $publicLink,
						'type'          => ucfirst($mapType),
						'self'          => $mapper
					];

				}
			}
		}

		/* Adding map_type_id, whether product or partner */
		$mappingValues['mapTypeId'] = $typeId;

		return $mappingValues;
	}

	/**
	 * @author Anji <anji@evibe.in>
	 * @since  21 Dec 2016
	 *
	 * Compress uploaded image. We are not restricting on image size now.
	 */
	private function compressImage($image)
	{
		$img = Image::make($image->getRealPath());
		$imgPath = $image->getRealPath();
		$img->resize(640, null, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		try
		{
			$img->save(); // save to the same location
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e, false);
			Log::error("Exception with compressing image: $imgPath");
		}
	}

	protected function sendAndSaveReport($e, $sendEmail = true)
	{
		$code = $e->getCode() ? $e->getCode() : 'Error';

		if (config('evibe.email_error'))
		{
			$data = [
				'fullUrl'  => \Illuminate\Support\Facades\Request::fullUrl(),
				'code'     => $code,
				'messages' => $e->getMessage(),
				'details'  => $e->getTraceAsString()
			];

			if ($sendEmail)
			{
				$emailData = [
					'code'     => $code,
					'url'      => $data['fullUrl'],
					'method'   => \Illuminate\Support\Facades\Request::method(),
					'messages' => $data['messages'],
					'trace'    => $data['details']
				];
				dispatch(new mailGenericErrorToAdmin($emailData));
			}

			$this->saveError($data);
		}
	}

	// @see: careful with keys 'message' and 'messages'
	protected function sendAndSaveNonExceptionReport($data, $sendEmail = true)
	{
		$errorData = [
			'code'       => isset($data['code']) && $data['code'] ? $data['code'] : 0,
			'url'        => isset($data['url']) && $data['url'] ? $data['url'] : config('evibe.hosts.api'),
			'method'     => isset($data['method']) && $data['method'] ? $data['method'] : \Illuminate\Support\Facades\Request::method(),
			'messages'   => isset($data['message']) && $data['message'] ? $data['message'] : 'Some error occurred',
			'trace'      => isset($data['trace']) && $data['trace'] ? $data['trace'] : null,
			'project_id' => isset($data['projectId']) && $data['projectId'] ? $data['projectId'] : config('evibe.project_id'),
			'exception'  => isset($data['exception']) && $data['exception'] ? $data['exception'] : 'Unknown exception',
			'details'    => isset($data['details']) && $data['details'] ? $data['details'] : 'No clear details'
		];

		if ($sendEmail)
		{
			$emailData = [
				'code'     => $errorData['code'],
				'url'      => $errorData['url'],
				'method'   => $errorData['method'],
				'messages' => $errorData['messages'],
				'trace'    => $errorData['details']
			];

			dispatch(new mailGenericErrorToAdmin($emailData));
		}

		$this->saveError($errorData);
	}

	private function saveError($data)
	{
		SiteError::create([
			                  'project_id' => config('evibe.project_id'),
			                  'url'        => isset($data['fullUrl']) && $data['fullUrl'] ? $data['fullUrl'] : (isset($data['url']) && $data['url'] ? $data['url'] : null),
			                  'exception'  => $data['messages'],
			                  'code'       => $data['code'],
			                  'details'    => $data['details']
		                  ]);
	}

	protected function getGalleryBaseUrl()
	{
		return config('evibe.live.host');
	}

	protected function getDashBaseUrl()
	{
		return config('evibe.hosts.dash');
	}

	protected function getImageTitle($image)
	{
		return pathinfo($image->getClientOriginalName(), PATHINFO_FILENAME);
	}

	protected function updateTicketAction($data, $handler = true)
	{
		$typeUpdate = config('evibe.ticket.type_update.auto');
		$ticket = $data['ticket'];
		$statusId = $ticket->status_id;
		$statusAt = time();
		$isUpdateTicket = false;
		$userId = isset($data['userId']) ? $data['userId'] : null;

		if (array_key_exists('typeUpdate', $data))
		{
			$typeUpdate = $data['typeUpdate'];
		}

		if (array_key_exists('statusId', $data))
		{
			$statusId = $data['statusId'];
			$isUpdateTicket = true;
		}

		if (array_key_exists('statusAt', $data))
		{
			$statusAt = $data['statusAt'];
		}
		if ($ticket->status_id == TicketStatus::BOOKED || $ticket->status_id == TicketStatus::CONFIRMED)
		{
			// @see: update ticket to given status id and not ticket' status id.
			// example: manual cancel ticket, all bookings are deleted
			if (array_key_exists('isNoBooking', $data) && $data['isNoBooking'] == true)
			{
				//do nothing
			}
			elseif ((array_key_exists('isManualCancellation', $data) && $data['isManualCancellation'] == true))
			{
				$statusId = config('evibe.ticket.status.cancelled');
			}
			else
			{
				$statusId = $ticket->status_id;
			}
		}

		$updateData = [
			'ticket_id'   => $ticket->id,
			'status_id'   => $statusId,
			'handler_id'  => $userId,
			'comments'    => $data['comments'],
			'status_at'   => $statusAt,
			'type_update' => $typeUpdate
		];

		if (TicketUpdate::create($updateData))
		{
			if ($isUpdateTicket)
			{
				$ticket->status_id = $statusId;
			}
			if ($handler)
			{
				$ticket->handler_id = $userId;
			}
			$ticket->updated_at = Carbon::now();
			$ticket->save();
		}
	}

	protected function validatePartner($partnerId, $partnerTypeId)
	{
		$partner = null;
		if (($partnerTypeId == config('evibe.ticket.type.planners')) || ($partnerTypeId == config('evibe.ticket.type.artists')))
		{
			$partner = Planner::find($partnerId);
		}
		elseif ($partnerTypeId == config('evibe.ticket.type.venues'))
		{
			$partner = Venue::find($partnerId);
		}

		if (!$partner)
		{
			return $res = [
				'success' => false,
				'error'   => 'Partner does not exist [PartnerId: ' . $partnerId . ' and PartnerTypeId: ' . $partnerTypeId . ']'
			];
		}

		return $res = [
			'success' => true,
			'partner' => $partner
		];
	}

	// function that gives parent id from type event table
	public function getParentEventId($eventId)
	{
		$parentDetails = TypeEvent::select('parent_id')->where('id', $eventId)->first();

		if ($parentDetails && $parentDetails->parent_id)
		{
			return $parentDetails->parent_id;
		}

		return $eventId;
	}

	public function convertNumberToWord($num = '')
	{
		$num = ( string )(( int )$num);

		if (( int )($num) && ctype_digit($num))
		{
			$words = [];

			$num = str_replace([',', ' '], '', trim($num));

			$list1 = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven',
				'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen',
				'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

			$list2 = ['', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty',
				'seventy', 'eighty', 'ninety', 'hundred'];

			$list3 = ['', 'thousand', 'million', 'billion', 'trillion',
				'quadrillion', 'quintillion', 'sextillion', 'septillion',
				'octillion', 'nonillion', 'decillion', 'undecillion',
				'duodecillion', 'tredecillion', 'quattuordecillion',
				'quindecillion', 'sexdecillion', 'septendecillion',
				'octodecillion', 'novemdecillion', 'vigintillion'];

			$num_length = strlen($num);
			$levels = ( int )(($num_length + 2) / 3);
			$max_length = $levels * 3;
			$num = substr('00' . $num, -$max_length);
			$num_levels = str_split($num, 3);

			foreach ($num_levels as $num_part)
			{
				$levels--;
				$hundreds = ( int )($num_part / 100);
				$hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' Hundred' . ($hundreds == 1 ? '' : 's') . ' ' : '');
				$tens = ( int )($num_part % 100);
				$singles = '';

				if ($tens < 20)
				{
					$tens = ($tens ? ' ' . $list1[$tens] . ' ' : '');
				}
				else
				{
					$tens = ( int )($tens / 10);
					$tens = ' ' . $list2[$tens] . ' ';
					$singles = ( int )($num_part % 10);
					$singles = ' ' . $list1[$singles] . ' ';
				}
				$words[] = $hundreds . $tens . $singles . (($levels && ( int )($num_part)) ? ' ' . $list3[$levels] . ' ' : '');
			}

			$commas = count($words);

			if ($commas > 1)
			{
				$commas = $commas - 1;
			}

			$words = implode(', ', $words);

			//Some Finishing Touch
			//Replacing multiples of spaces with one space
			$words = trim(str_replace(' ,', ',', trim(ucwords($words))), ', ');
			if ($commas)
			{
				$words = str_replace_last(',', ' and', $words);
			}

			return $words;
		}
		else
		{
			if (!(( int )$num))
			{
				return 'Zero';
			}
		}

		return '';
	}

	public function fetchServiceOptions($request, $serviceId, $serviceTags, $eventRelatedTags, $cityId, $eventId)
	{
		$options = [];
		$filters = [];
		$pageId = $serviceId;
		$mapTypeId = $serviceId;
		$totalOptionsCount = 0;

		$limit = isset($request["limit"]) && $request["limit"] ? $request["limit"] : null;
		$offset = isset($request["offset"]) && $request["offset"] ? $request["offset"] : null;

		$eventId = $this->getParentEventId($eventId);

		switch ($serviceId)
		{
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.resort'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.lounge'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.couple-experiences'):
			case config('evibe.ticket.type.venue-deals'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.generic-package'):
				$packages = Package::select('planner_package.*')
				                   ->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
				                   ->where('type_ticket_id', $serviceId)
					//->whereIn('planner_package_tags.tile_tag_id', $serviceTags)
					               ->where('planner_package.event_id', $eventId)
				                   ->where('planner_package.city_id', $cityId)
				                   ->whereNull('planner_package.deleted_at')
				                   ->where('planner_package.is_live', 1)
				                   ->distinct('planner_package.id')
				                   ->orderBy('price');

				$mapTypeId = config('evibe.ticket.type.packages');
				$active = null;
				$clearFilter = false;

				// filter:search
				$userSearch = "";
				if (isset($request['search']) && $request['search'] && trim($request['search']))
				{
					$clearFilter = true;
					$userSearch = trim($request['search']);
					$searchWords = explode(' ', $userSearch);

					$packages->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('planner_package.name', 'LIKE', "%$word%")
							            ->orWhere('planner_package.code', 'LIKE', "%$word%")
							            ->orWhere('planner_package.price', 'LIKE', "%$word%");
						}
					});
				}

				// filter:category
				$categoryUrl = (isset($request['category']) && $request['category']) ? $request['category'] : '';
				$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);

				$allTagIdsBank = $filterData['allTagIdsBank'];
				$allCategories = $filterData['allCategories'];
				$allFilterTagIds = $filterData['allFilterTagIds'];
				$hasFilter = $filterData['hasFilter'];
				$userFilterId = null;

				if ($hasFilter)
				{
					$packages->whereIn('planner_package_tags.tile_tag_id', $allFilterTagIds);
					$clearFilter = true;
					$active = $categoryUrl;
					//$userFilterId = $filterData['userFilterTag']->id;
				}

				// filter:price
				$minPrices = $packages->pluck('price');
				$maxPrices = $packages->pluck('price_max');
				$calMinPrice = $packages->pluck('price')->min();
				$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
				$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
				$priceMin = isset($request['min_price']) && $request['min_price'] ? $request['min_price'] : 0;
				$priceMax = isset($request['max_price']) && $request['max_price'] ? $request['max_price'] : 0;
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
				// user has changed min / max price
				if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
				{
					$clearFilter = true;
					//$hidePriceCategories = true;
					$packages->whereBetween('planner_package.price', [$priceMin, $priceMax]);
				}

				// no price category for now

				$totalOptionsCount = $packages->count();

				if ($offset)
				{
					$packages = $packages->offset($offset);
				}

				if ($limit)
				{
					$packages = $packages->limit($limit);
				}
				$packages = $packages->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $packages);

				$filters = [
					'queryParams'   => $this->getExistingQueryParams($request),
					'active'        => $active,
					'priceMin'      => $priceMin,
					'priceMax'      => $priceMax,
					'search'        => $userSearch,
					'clearFilter'   => $clearFilter,
					'catCounts'     => $this->getFilterCategoryCounts($packages, $allTagIdsBank, $serviceTags),
					'allCategories' => $allCategories,
				];

				break;

			case config('evibe.ticket.type.cakes'):
				$cakes = Cake::select('cake.*')
				             ->join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
				             ->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
				             ->join('planner', 'planner.id', '=', 'cake.provider_id')
					//->whereIn('cake_tags.tag_id', $serviceTags)
					         ->where('cake_event.event_id', $eventId)
				             ->where('planner.city_id', $cityId)
				             ->whereNull('cake.deleted_at')
				             ->whereNull('cake_tags.deleted_at')
				             ->whereNull('cake_event.deleted_at')
				             ->where('cake.is_live', 1)
				             ->distinct('cake.id')
				             ->orderBy('price_per_kg');

				$clearFilter = false;
				$mapTypeId = config('evibe.ticket.type.cakes');

				// Search query
				$searchQuery = $request->input('search');
				if ($searchQuery)
				{
					// @see: if 'trim' can be used...
					$searchWords = explode(' ', $searchQuery);
					$clearFilter = true;

					$cakes = $cakes->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('cake.title', 'LIKE', "%$word%")
							            ->orWhere('cake.code', 'LIKE', "%$word%");
						}
					});
				}

				// category filter
				$active = "";
				$categoryUrl = (isset($request['category']) && $request['category']) ? $request['category'] : '';
				$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);

				$allTagIdsBank = $filterData['allTagIdsBank'];
				$allCategories = $filterData['allCategories'];
				$allFilterTagIds = $filterData['allFilterTagIds'];
				$hasFilter = $filterData['hasFilter'];
				$userFilterId = null;

				if ($hasFilter)
				{
					$cakes->whereIn('cake_tags.tag_id', $allFilterTagIds);
					$clearFilter = true;
					$active = $categoryUrl;
					//$userFilterId = $filterData['userFilterTag']->id;
				}

				$defaultMinPrice = $cakes->pluck('price_per_kg')->min();
				$defaultMaxPrice = $cakes->pluck('price_per_kg')->max();

				// Price filter
				$minPrice = isset($request['price_min']) && $request['price_min'] ? (int)$request['price_min'] : 0;
				$maxPrice = isset($request['price_max']) && $request['price_max'] ? (int)$request['price_max'] : 0;

				if ($minPrice || $maxPrice)
				{
					$cakes->where(function ($query) use ($minPrice, $maxPrice) {
						if ($minPrice && $maxPrice)
						{
							$query->whereBetween('cake.price_per_kg', [$minPrice, $maxPrice]);
						}
						elseif (!$minPrice && $maxPrice)
						{
							$query->where('cake.price_per_kg', '<=', $minPrice);
						}
						elseif ($minPrice && !$maxPrice)
						{
							$query->where('cake.price_per_kg', '>=', $minPrice);
						}
					});
				}

				$totalOptionsCount = $cakes->count();

				if ($offset)
				{
					$cakes = $cakes->offset($offset);
				}

				if ($limit)
				{
					$cakes = $cakes->limit($limit);
				}
				$cakes = $cakes->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $cakes);

				$filters = [
					'queryParams'   => $this->getExistingQueryParams($request),
					'catCounts'     => $this->getFilterCategoryCounts($cakes, $allTagIdsBank, $serviceTags),
					'priceMin'      => ($minPrice && $minPrice > 0) ? $minPrice : $defaultMinPrice,
					'priceMax'      => ($maxPrice && $maxPrice > 0 && $maxPrice >= $minPrice) ? $maxPrice : $defaultMaxPrice,
					'allCategories' => $allCategories,
					'active'        => $active,
					'clearFilter'   => $clearFilter,
					'search'        => $searchQuery,
				];
				break;

			case config('evibe.ticket.type.decors'):

				$decorTagIds = Decor\DecorTags::select("decor_id AS option_id", "tag_id")->get()->toArray();
				$sortedDecorIds = $this->getSortedOptionIdsByTagIds($decorTagIds, $serviceTags, $eventRelatedTags);

				$decors = Decor::select('decor.*')
				               ->join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
				               ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
				               ->join('planner', 'planner.id', '=', 'decor.provider_id')
				               ->whereIn("decor.id", $sortedDecorIds)
				               ->where('decor_event.event_id', 1)
				               ->where('planner.city_id', $cityId)
				               ->whereNull('decor.deleted_at')
				               ->whereNull('decor_tags.deleted_at')
				               ->whereNull('decor_event.deleted_at')
				               ->where('decor.is_live', 1)
				               ->distinct('decor.id')
				               ->orderBy('min_price');

				$mapTypeId = config('evibe.ticket.type.decors');
				$clearFilter = false;

				// Filter by search term
				$userSearch = "";
				if (isset($request['search']) && $request['search'] && trim($request['search']))
				{
					$clearFilter = true;
					$userSearch = trim($request['search']);
					$searchWords = explode(' ', $userSearch);

					$decors->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('decor.name', 'LIKE', "%$word%")
							            ->orWhere('decor.code', 'LIKE', "%$word%");
						}
					});
				}

				// Get decors by categories (all / selected category)
				$active = "";
				$categoryUrl = (isset($request['category']) && $request['category']) ? $request['category'] : '';
				$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);
				$allTagIdsBank = $filterData['allTagIdsBank'];
				$allCategories = $filterData['allCategories'];
				$allFilterTagIds = $filterData['allFilterTagIds'];
				$hasFilter = $filterData['hasFilter'];
				$userFilterId = null;

				if ($hasFilter)
				{
					$decors->whereIn('decor_tags.tag_id', $allFilterTagIds);
					$clearFilter = true;
					$active = $categoryUrl;
					//$userFilterId = $filterData['userFilterTag']->id;

				}

				// Filter by price
				$minPrices = $decors->pluck('min_price');
				$maxPrices = $decors->pluck('max_price');
				$calMinPrice = $minPrices->min();
				$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
				$priceMin = isset($request['price_min']) && $request['price_min'] ? $request['price_min'] : 0;
				$priceMax = isset($request['price_max']) && $request['price_max'] ? $request['price_max'] : 0;
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

				// user has changed min / max price
				if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
				{
					$clearFilter = true;
					$decors->where(function ($query1) use ($priceMin, $priceMax) {
						$query1->where(function ($query11) use ($priceMin, $priceMax) {
							$query11->where('max_price', '=', 0)
							        ->where('min_price', '>=', $priceMin)
							        ->where('min_price', '<=', $priceMax);
						})->orWhere(function ($query12) use ($priceMin, $priceMax) {
							$query12->where('max_price', '!=', 0)
							        ->where('min_price', '<=', $priceMin)
							        ->where('max_price', '>=', $priceMin)
							        ->where('max_price', '<=', $priceMax);
						})->orWhere(function ($query13) use ($priceMin, $priceMax) {
							$query13->where('max_price', '!=', 0)
							        ->where('min_price', '>=', $priceMin)
							        ->where('min_price', '<=', $priceMax)
							        ->where('max_price', '>=', $priceMax);
						})->orWhere(function ($query14) use ($priceMin, $priceMax) {
							$query14->where('max_price', '!=', 0)
							        ->where('min_price', '>=', $priceMin)
							        ->where('max_price', '<=', $priceMax);
						});
					});
				}

				$totalOptionsCount = $decors->count();

				if ($offset)
				{
					$decors = $decors->offset($offset);
				}

				if ($limit)
				{
					$decors = $decors->limit($limit);
				}
				$decors = $decors->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $decors);

				$filters = [
					'queryParams'   => $this->getExistingQueryParams($request),
					'catCounts'     => $this->getFilterCategoryCounts($decors, $allTagIdsBank, $serviceTags),
					'priceMin'      => $priceMin,
					'priceMax'      => $priceMax,
					'allCategories' => $allCategories,
					'active'        => $active,
					'clearFilter'   => $clearFilter,
					'search'        => $userSearch,
				];
				break;

			case config('evibe.ticket.type.trends'):
				$trends = Trends::select('trending.*')
				                ->join('trend_event', 'trend_event.trend_id', '=', 'trending.id')
				                ->where('trend_event.type_event_id', $eventId)
				                ->where('trending.city_id', $cityId)
				                ->whereNull('trending.deleted_at')
				                ->whereNull('trend_event.deleted_at')
				                ->where('trending.is_live')
				                ->distinct('trending.id')
				                ->orderBy('price');

				$clearFilter = false;
				$mapTypeId = config('evibe.ticket.type.trends');

				// Filter by search term
				$userSearch = "";
				if (isset($request['search']) && $request['search'] && trim($request['search']))
				{
					$clearFilter = true;
					$userSearch = trim($request['search']);
					$searchWords = explode(' ', $userSearch);

					$trends->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('trending.name', 'LIKE', "%$word%")
							            ->orWhere('trending.code', 'LIKE', "%$word%");
						}
					});
				}

				// filter:price
				$minPrices = $trends->pluck('price');
				$maxPrices = $trends->pluck('price_max');
				$calMinPrice = $trends->pluck('price')->min();
				$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
				$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
				$priceMin = isset($request['min_price']) && $request['min_price'] ? $request['min_price'] : 0;
				$priceMax = isset($request['max_price']) && $request['max_price'] ? $request['max_price'] : 0;
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
				// user has changed min / max price
				if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
				{
					$clearFilter = true;
					$trends->whereBetween('trending.price', [$priceMin, $priceMax]);
				}

				$totalOptionsCount = $trends->count();

				if ($offset)
				{
					$trends = $trends->offset($offset);
				}

				if ($limit)
				{
					$trends = $trends->limit($limit);
				}
				$trends = $trends->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $trends);

				$filters = [
					'queryParams' => $this->getExistingQueryParams($request),
					'priceMin'    => $priceMin,
					'priceMax'    => $priceMax,
					'clearFilter' => $clearFilter,
					'search'      => $userSearch,
				];

				break;

			// @see: services == ent
			case config('evibe.ticket.type.entertainments'):
			case config('evibe.ticket.type.services'):
				$services = TypeService::select('type_service.*')
				                       ->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
				                       ->where('service_event.type_event_id', $eventId)
				                       ->where('type_service.city_id', $cityId)
				                       ->whereNull('type_service.deleted_at')
				                       ->whereNull('service_event.deleted_at')
				                       ->where('type_service.is_live', 1)
				                       ->distinct('type_service.id')
				                       ->orderBy('min_price');

				$clearFilter = false;
				$mapTypeId = config('evibe.ticket.type.services');

				// Filter by search term
				$userSearch = "";
				if (isset($request['search']) && $request['search'] && trim($request['search']))
				{
					$clearFilter = true;
					$userSearch = trim($request['search']);
					$searchWords = explode(' ', $userSearch);

					$services->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('type_service.name', 'LIKE', "%$word%")
							            ->orWhere('type_service.code', 'LIKE', "%$word%");
						}
					});
				}

				// filter:price
				$minPrices = $services->pluck('min_price');
				$maxPrices = $services->pluck('max_price');
				$calMinPrice = $services->pluck('min_price')->min();
				$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
				$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
				$priceMin = isset($request['min_price']) && $request['min_price'] ? $request['min_price'] : 0;
				$priceMax = isset($request['max_price']) && $request['max_price'] ? $request['max_price'] : 0;
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
				// user has changed min / max price
				if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
				{
					$clearFilter = true;
					$services->whereBetween('type_service.min_price', [$priceMin, $priceMax]);
				}

				$totalOptionsCount = $services->count();

				if ($offset)
				{
					$services = $services->offset($offset);
				}

				if ($limit)
				{
					$services = $services->limit($limit);
				}
				$services = $services->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $services);

				$filters = [
					'queryParams' => $this->getExistingQueryParams($request),
					'priceMin'    => $priceMin,
					'priceMax'    => $priceMax,
					'clearFilter' => $clearFilter,
					'search'      => $userSearch,
				];

				break;

			case config('evibe.ticket.type.venue_halls'):
			case config('evibe.ticket.type.venues'):
				$halls = VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
				                  ->join('venue_hall_event', 'venue_hall_event.venue_hall_id', '=', 'venue_hall.id')
				                  ->whereNull('venue_hall_event.deleted_at')
				                  ->where('venue_hall_event.event_id', $eventId)
				                  ->where('venue.city_id', $cityId)
				                  ->where('venue.is_live', 1)
				                  ->whereNull('venue.deleted_at')
				                  ->select('venue_hall.*')
				                  ->distinct('venue_hall.id')
				                  ->orderBy('price_min_veg');

				$clearFilter = false;
				$mapTypeId = config('evibe.ticket.type.venues');

				// Filter by search term
				$userSearch = "";
				if (isset($request['search']) && $request['search'] && trim($request['search']))
				{
					$clearFilter = true;
					$userSearch = trim($request['search']);
					$searchWords = explode(' ', $userSearch);

					$halls->where(function ($searchQuery) use ($searchWords) {
						foreach ($searchWords as $word)
						{
							$searchQuery->orWhere('venue_hall.name', 'LIKE', "%$word%")
							            ->orWhere('venue_hall.code', 'LIKE', "%$word%");
						}
					});
				}

				// Price filter
				$minMax = Venue::select(DB::raw('min(price_min_veg) minPriceMin, max(price_min_veg) maxPriceMin, max(price_min_nonveg) maxPriceMax, min(cap_min) calCapMin, max(cap_max) calCapMax'))
				               ->where('is_live', 1)
				               ->first();

				$calMinPrice = $minMax->minPriceMin;
				$calMaxPrice = max($minMax->maxPriceMin, $minMax->maxPriceMax);
				$priceMin = isset($request['price_min']) && $request['price_min'] ? $request['price_min'] : 0;
				$priceMax = isset($request['price_max']) && $request['price_max'] ? $request['price_max'] : 0;
				$priceMin = $priceMin ? $priceMin : $calMinPrice;
				$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

				// user has changed min / max price
				if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
				{
					$clearFilter = true;
					$halls->whereBetween('venue.price_min_veg', [$priceMin, $priceMax]);
				}

				$halls->groupBy('venue_hall.id');
				$totalOptionsCount = $halls->count();

				if ($offset)
				{
					$halls = $halls->offset($offset);
				}

				if ($limit)
				{
					$halls = $halls->limit($limit);
				}
				$halls = $halls->get();

				$options = $this->workflowOptionsArrayFormat($serviceId, $halls);

				$filters = [
					'queryParams' => $this->getExistingQueryParams($request),
					'priceMin'    => $priceMin,
					'priceMax'    => $priceMax,
					'clearFilter' => $clearFilter,
					'search'      => $userSearch,
				];
				// @todo: need to change at top and check modified categories

				break;

			default:
				break;
		}

		return [
			'options'           => $options,
			'filters'           => $filters,
			'mapTypeId'         => $mapTypeId,
			'totalOptionsCount' => $totalOptionsCount
		];
	}

	protected function getFilterData($eventId, $pageId, $categoryUrl = '')
	{
		$allFilterTagIds = [];
		$tagsSeoTitle = "";
		$tagsSeoDesc = "";
		$filterTagName = "";
		$hasFilter = false;
		$userFilterTag = false;
		$catsData = $this->getAllValidCategories($eventId, $pageId);

		$allCategories = $catsData['cats'];
		$allTagIdsBank = $catsData['bank'];

		// style selected, join tags for further filter
		if ($categoryUrl && $categoryUrl != 'all')
		{
			$catUrls = explode(',', $categoryUrl);
			if (count($catUrls))
			{
				foreach ($catUrls as $catUrl)
				{
					// get tag id based on url
					$userFilterTag = Tags::where('url', $catUrl)->first();

					if ($userFilterTag)
					{
						$hasFilter = true;
						$filterTagId = $userFilterTag->id;

						array_push($allFilterTagIds, $filterTagId);
						//$tagsSeoTitle .= $userFilterTag->seo_title ? $userFilterTag->seo_title : "";
						//$tagsSeoDesc .= $userFilterTag->seo_desc ? $userFilterTag->seo_desc : "";
						$filterTagName = $userFilterTag->identifier ? $userFilterTag->identifier : $userFilterTag->name;

						// collect all relevant tag ids (if parent, then all child)
						if (isset($allCategories[$filterTagId]))
						{
							// is parent tag
							if (count($allCategories[$filterTagId]['child']))
							{
								$filterTagChildTags = $allCategories[$filterTagId]['child'];
								foreach ($filterTagChildTags as $key => $value)
								{
									array_push($allFilterTagIds, $key);
									//$tagsSeoTitle .= $value['seoTitle'] ? $value['seoTitle'] : "";
									//$tagsSeoDesc .= $value['seoDesc'] ? $value['seoDesc'] : "";
								}
							}
						}
					}
				}
			}

		}

		$data = [
			'allCategories'   => $allCategories,
			'allTagIdsBank'   => $allTagIdsBank,
			'allFilterTagIds' => $allFilterTagIds,
			'hasFilter'       => $hasFilter,
			'filterTagName'   => $filterTagName,
			//'tagsSeoTitle'    => $tagsSeoTitle,
			//'tagsSeoDesc'     => $tagsSeoDesc,
			'userFilterTag'   => $userFilterTag
		];

		return $data;
	}

	private function getSortedOptionIdsByTagIds($options, $serviceTags, $eventRelatedTags)
	{
		$eventRelatedOptionIds = [];
		$genderRelatedOptionsIds = [];
		$serviceTagsOptionIds = [];

		/*
		 * If event tag has neutral include both boy & girl
		 * If event tag has boy include boy & neutral
		 * If event tag has girl include girl & neutral
		 * */
		$genderRelatedTags = [config("evibe.workflow.gender.tags.neutral")];
		if (in_array(config("evibe.workflow.gender.tags.boy"), $eventRelatedTags))
		{
			$genderRelatedTags[] = config("evibe.workflow.gender.tags.boy");
			$key = array_search(config("evibe.workflow.gender.tags.boy"), $eventRelatedTags);
		}
		elseif (in_array(config("evibe.workflow.gender.tags.girl"), $eventRelatedTags))
		{
			$genderRelatedTags[] = config("evibe.workflow.gender.tags.girl");
			$key = array_search(config("evibe.workflow.gender.tags.girl"), $eventRelatedTags);
		}
		elseif (in_array(config("evibe.workflow.gender.tags.neutral"), $eventRelatedTags))
		{
			$genderRelatedTags[] = config("evibe.workflow.gender.tags.boy");
			$genderRelatedTags[] = config("evibe.workflow.gender.tags.girl");
			$key = array_search(config("evibe.workflow.gender.tags.neutral"), $eventRelatedTags);
		}

		unset($eventRelatedTags[$key]);

		if (is_array($eventRelatedTags) && (count($eventRelatedTags) > 0))
		{
			foreach ($eventRelatedTags as $eventRelatedTag)
			{
				$eventRelatedOptionIds[$eventRelatedTag] = [];
				array_filter($options, function ($option) use ($eventRelatedTag, &$eventRelatedOptionIds) {
					if ($option["tag_id"] == $eventRelatedTag)
					{
						$eventRelatedOptionIds[$eventRelatedTag][] = $option["option_id"];
					}
				});
			}

			$eventRelatedOptionIds = call_user_func_array('array_intersect', $eventRelatedOptionIds);

			if (is_array($genderRelatedTags) && (count($genderRelatedTags) > 0))
			{
				foreach ($genderRelatedTags as $genderRelatedTag)
				{
					$genderRelatedOptionsIds[$genderRelatedTag] = [];
					array_filter($options, function ($option) use ($genderRelatedTag, &$genderRelatedOptionsIds) {
						if ($option["tag_id"] == $genderRelatedTag)
						{
							$genderRelatedOptionsIds[$genderRelatedTag][] = $option["option_id"];
						}
					});
				}

				$genderRelatedOptionsIds = call_user_func_array('array_merge', $genderRelatedOptionsIds);

				$eventRelatedOptionIds = array_intersect($eventRelatedOptionIds, $genderRelatedOptionsIds);
			}
		}

		if (is_array($serviceTags) && (count($serviceTags) > 0))
		{
			foreach ($serviceTags as $serviceTag)
			{
				$serviceTagsOptionIds[$serviceTag] = [];
				array_filter($options, function ($option) use ($serviceTag, &$serviceTagsOptionIds) {
					if ($option["tag_id"] == $serviceTag)
					{
						$serviceTagsOptionIds[$serviceTag][] = $option["option_id"];
					}
				});
			}

			$serviceTagsOptionIds = call_user_func_array('array_merge', $serviceTagsOptionIds);
		}

		return array_intersect($eventRelatedOptionIds, $serviceTagsOptionIds);
	}

	private function getAllValidCategories($eventId, $pageId)
	{
		$allCategories = [];
		$allTagIdsBank = [];
		$allParentTags = Tags::forEvent($eventId)
		                     ->forPage($pageId)
		                     ->whereNull('parent_id')
		                     ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
		                     ->get();

		foreach ($allParentTags as $parentTag)
		{
			$tagId = $parentTag->id;
			array_push($allTagIdsBank, $tagId);

			if (!isset($allCategories[$tagId]))
			{
				$allCategories[$tagId] = [
					'id'       => $tagId,
					'name'     => $parentTag->identifier ? $parentTag->identifier : $parentTag->name,
					'url'      => $parentTag->url,
					'count'    => 0,
					'seoTitle' => $parentTag->seo_title,
					'seoDesc'  => $parentTag->seo_desc,
					'child'    => []
				];
			}

			$childTags = Tags::where('parent_id', $tagId)
			                 ->filterable()
			                 ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                 ->get();

			foreach ($childTags as $childTag)
			{
				$childTagId = $childTag->id;
				array_push($allTagIdsBank, $childTagId);

				$allCategories[$tagId]['child'][$childTagId] = [
					'id'       => $childTagId,
					'name'     => $childTag->identifier ? $childTag->identifier : $childTag->name, // take identifier
					'url'      => $childTag->url,
					'seoTitle' => $childTag->seo_title,
					'seoDesc'  => $childTag->seo_desc,
					'count'    => 0
				];
			}
		}

		$retData = [
			'cats' => $allCategories,
			'bank' => array_unique($allTagIdsBank) // take unique tags ids
		];

		return $retData;
	}

	private function getExistingQueryParams($request)
	{
		$existingQueryParams = [];

		foreach ($request->all() as $key => $value)
		{
			if ($key != 'page')
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

	private function getFilterCategoryCounts($list, $allTags, $catIds)
	{
		$counts = [];
		$haystack = [];
		$total = 0;

		foreach ($allTags as $tag)
		{
			$counts[$tag] = 0;
		}

		// initialize
		if (is_array($catIds))
		{
			foreach ($catIds as $catId)
			{
				$counts[$catId] = 0;
				array_push($haystack, $catId);
			}
		}

		foreach ($list as $item)
		{
			$itemTags = $item->tags; // to avoid repetitive tags
			foreach ($itemTags as $itemTag)
			{
				$itemTagId = $itemTag->id; // getting from pivot table
				if (in_array($itemTagId, $haystack))
				{
					if (!array_key_exists($itemTagId, $counts))
					{
						$counts[$itemTagId] = 0;
					}
					$counts[$itemTagId] = $counts[$itemTagId] + 1;

					// increase parent tag count
					$parentId = $itemTag->parent_id;
					if ($parentId && in_array($parentId, $haystack))
					{
						$counts[$parentId] = $counts[$parentId] + 1;
					}

					$total += 1;
				}
			}
		}

		$counts['total'] = $total;

		return $counts;
	}

	public function workflowOptionsArrayFormat($productTypeId, $products)
	{
		$options = [];

		if (count($products))
		{
			switch ($productTypeId)
			{
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.resort'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.lounge'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.couple-experiences'):
				case config('evibe.ticket.type.venue-deals'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.generic-package'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"                 => $value->id,
							"name"               => $value->name,
							"code"               => $value->code,
							"url"                => $this->getLiveUrl(config("evibe.ticket.type.packages"), $value),
							"minPrice"           => $value->price,
							"maxPrice"           => $value->price_max,
							"priceWorth"         => $value->price_worth,
							"priceRangeInfo"     => $value->range_info,
							"pricePerExtraGuest" => $value->price_per_extra_guest,
							"profilePic"         => $value->getProfilePic(),
							"isSelected"         => 0
						]);
					}
					break;

				case config('evibe.ticket.type.cakes'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"                 => $value->id,
							"name"               => $value->title,
							"code"               => $value->code,
							"url"                => $this->getLiveUrl(config("evibe.ticket.type.cakes"), $value),
							"minPrice"           => $value->price,
							"maxPrice"           => $value->max_price,
							"priceWorth"         => $value->price_worth,
							"pricePerKg"         => $value->price_per_kg,
							"pricePerExtraGuest" => $value->price_per_extra_guest,
							"profilePic"         => $value->getProfilePic(),
							"isSelected"         => 0
						]);
					}
					break;

				case config('evibe.ticket.type.decors'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => $this->getLiveUrl(config("evibe.ticket.type.decors"), $value),
							"minPrice"       => $value->min_price,
							"maxPrice"       => $value->max_price,
							"priceWorth"     => $value->worth,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}
					break;

				case config('evibe.ticket.type.trends'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => $this->getLiveUrl(config("evibe.ticket.type.trends"), $value),
							"minPrice"       => $value->price,
							"maxPrice"       => $value->price_max,
							"priceWorth"     => $value->price_worth,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}
					break;

				case config('evibe.ticket.type.entertainments'):
				case config('evibe.ticket.type.services'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => $this->getLiveUrl(config("evibe.ticket.type.services"), $value),
							"minPrice"       => $value->min_price,
							"maxPrice"       => $value->max_price,
							"priceWorth"     => $value->worth_price,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}
					break;

				case config('evibe.ticket.type.venue_halls'):
				case config('evibe.ticket.type.venues'):
					foreach ($products as $value)
					{
						array_push($options, [
							"id"         => $value->id,
							"name"       => $value->name,
							"code"       => $value->code,
							"url"        => $this->getLiveUrl(config("evibe.ticket.type.venues"), $value),
							"minPrice"   => $value->price_min_veg,
							"maxPrice"   => $value->price_max_veg,
							"priceWorth" => $value->price_worth_veg,
							"profilePic" => $value->getProfilePic(),
							"isSelected" => 0
						]);
					}
					break;
			}
		}

		return $options;
	}

	public function createMapping($ticketId, $productId, $productTypeId, $handlerId = null)
	{
		switch ($productTypeId)
		{
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.resort'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.lounge'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.couple-experiences'):
			case config('evibe.ticket.type.venue-deals'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.generic-package'):
				$mapTypeId = config('evibe.ticket.type.packages');
				break;

			case config('evibe.ticket.type.cakes'):
				$mapTypeId = config('evibe.ticket.type.cakes');
				break;

			case config('evibe.ticket.type.decors'):
				$mapTypeId = config('evibe.ticket.type.decors');
				break;

			case config('evibe.ticket.type.services'):
			case config('evibe.ticket.type.entertainments'):
				$mapTypeId = config('evibe.ticket.type.services');
				break;

			case config('evibe.ticket.type.venues'):
			case config('evibe.ticket.type.venue_halls'):
				$mapTypeId = config('evibe.ticket.type.venues');
				break;

			case config('evibe.ticket.type.trends'):
				$mapTypeId = config('evibe.ticket.type.trends');
				break;

			default:
				$mapTypeId = $productTypeId;
				break;

		}

		// @todo:check whether map_type_id or type_ticket_id
		$ticketMapping = TicketMapping::where('ticket_id', $ticketId)
		                              ->where('map_id', $productId)
		                              ->where('map_type_id', $mapTypeId)
		                              ->whereNull('deleted_at')
		                              ->first();

		if ($ticketMapping)
		{
			return $data = [
				'success' => false,
				'error'   => 'The option has already been selected'
			];
		}
		else
		{
			// @todo: validate whether the mapping option exists or not
			switch ($mapTypeId)
			{
				case config('evibe.ticket.type.packages'):
					if (!$option = Package::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				case config('evibe.ticket.type.cakes'):
					if (!$option = Cake::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				case config('evibe.ticket.type.decors'):
					if (!$option = Decor::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				case config('evibe.ticket.type.services'):
					if (!$option = TypeService::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				case config('evibe.ticket.type.venues'):
					if (!$option = VenueHall::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				case config('evibe.ticket.type.trends'):
					if (!$option = Trends::find($productId))
					{
						return $data = [
							'success' => false,
							'error'   => 'The selected option does not exist'
						];
					}
					break;

				default:
					return $data = [
						'success' => false,
						'error'   => 'The selected option does not exist'
					];
					break;
			}

			// create ticket mapping
			$mapping = TicketMapping::create([
				                                 'ticket_id'   => $ticketId,
				                                 'map_id'      => $productId,
				                                 'map_type_id' => $mapTypeId,
				                                 'handler_id'  => $handlerId,
				                                 'is_selected' => 1,
				                                 'selected_at' => date("Y-m-d H:i:s"),
				                                 'created_at'  => Carbon::now(),
				                                 'updated_at'  => Carbon::now()
			                                 ]);
			if ($mapping)
			{
				return $data = [
					'success'         => true,
					'ticketId'        => $ticketId,
					'ticketMappingId' => $mapping->id
				];
			}
		}
	}

	public function getPackageGalleryUrl($gallery, $isNew = false)
	{
		$url = config('evibe.hosts.gallery');

		$package = $gallery->package;

		if ($package->provider)
		{
			if ($package->map_type_id == config('evibe.ticket.type.venues'))
			{
				$url .= '/venues/' . $package->provider->id . '/images/packages/' . $package->id;
			}
			else
			{
				$url .= '/planner/' . $package->provider->id . '/images/packages/' . $package->id;
			}
		}

		$url .= '/' . $gallery->url;

		return $url;
	}

	public function getImagesBaseUrl()
	{
		return config('evibe.hosts.gallery');
	}

	public function returnFalseJsonResponse($message)
	{
		return response()->json([
			                        'success' => false,
			                        'error'   => $message ?: "Error occurred while submitting the request, Please try again."
		                        ]
		);
	}

	public function generateRandomString($length)
	{
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++)
		{
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}

		return $randomString;
	}

	public function createCouponRestricted(Request $request)
	{
		$this->checkForAccessToken($request);

		return response()->json([
			                        "success"    => true,
			                        "couponCode" => $this->createCoupon($request->all())
		                        ]);
	}

	public function createCoupon($couponDetails)
	{
		if (!isset($couponDetails["couponPrefix"]) || is_null($couponDetails["couponPrefix"]) || !isset($couponDetails["stringLength"]) || ($couponDetails["stringLength"] <= 0))
		{
			return null;
		}

		$couponCode = $this->generateCouponCode($couponDetails["couponPrefix"], $couponDetails["stringLength"]);

		$coupon = Coupon::create([
			                         "coupon_code"         => $couponCode,
			                         "max_usage_count"     => 1,
			                         "min_order"           => isset($couponDetails["minOrder"]) ? $couponDetails["minOrder"] : null,
			                         "discount_amount"     => isset($couponDetails["discountAmount"]) ? $couponDetails["discountAmount"] : null,
			                         "discount_percent"    => isset($couponDetails["discountPercent"]) ? $couponDetails["discountPercent"] : null,
			                         "max_discount_amount" => isset($couponDetails["maxDiscount"]) ? $couponDetails["maxDiscount"] : null,
			                         "offer_start_time"    => isset($couponDetails["offerStartTime"]) ? $couponDetails["offerStartTime"] : null,
			                         "offer_end_time"      => isset($couponDetails["offerEndTime"]) ? $couponDetails["offerEndTime"] : null,
			                         "description"         => isset($couponDetails["description"]) ? $couponDetails["description"] : null
		                         ]);

		if ($coupon)
		{
			if (isset($couponDetails["userId"]) && ($couponDetails["userId"] > 0))
			{
				$couponUsers = CouponUsers::create([
					                                   "coupon_id" => $coupon->id,
					                                   "user_id"   => $couponDetails["userId"]
				                                   ]);

				if ($couponUsers)
				{
					return $couponCode;
				}
			}
			else
			{
				return $couponCode;
			}
		}

		return null;
	}

	private function generateCouponCode($prefix, $stringLength)
	{
		$couponCode = false;

		while (true)
		{
			$randomString = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil($stringLength / strlen($x)))), 1, $stringLength);
			$couponCode = $prefix . $randomString;

			$existingCoupons = Coupon::where('coupon_code', $couponCode)->get();

			if ($existingCoupons->count() == 0)
			{
				break;
			}
		}

		return $couponCode;
	}

	public function getLiveUrl($typeId, $item)
	{
		$cityUrl = null;
		if (($item->provider) && ($item->provider->city->url))
		{
			$cityUrl = $item->provider->city->url;
		}

		$cityDetails = City::select('id', 'url')->where('is_active', 1)->get()->toArray();

		$liveUrl = config('evibe.live.host');

		if ($typeId == config('evibe.ticket.type.cakes'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket.type.decors'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket.type.services'))
		{
			if (is_null($cityUrl))
			{
				$cityDetails = City::select('id', 'url')->where('is_active', 1)->get()->toArray();
				$key = array_search($item->city_id, array_column($cityDetails, 'id'));
			}

			$liveUrl .= '/liveUrl/' . $cityDetails[$key]['url'] . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket.type.packages'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}
		elseif ($typeId == config('evibe.ticket.type.trends'))
		{
			$liveUrl .= '/liveUrl/' . $cityUrl . '/' . $typeId . '/' . $item->id . '/' . 'redirect';
		}

		return $liveUrl . "?ref=workflow";
	}

	public function getClosureDate($ticketCreationDate, $typeLikeliness)
	{
		$ticketCreationDate = $ticketCreationDate->timestamp;
		$closureDate = $ticketCreationDate;
		$today7Pm = Carbon::parse('today 7pm')->timestamp;
		$today12MidNight = Carbon::parse('today 12AM')->timestamp;
		$today12Noon = Carbon::parse('today 12Pm')->timestamp;
		switch (config('evibe.ticket.bookingLikeliness.' . $typeLikeliness))
		{
			case config('evibe.ticket.bookingLikeliness.1'):
				if ($ticketCreationDate > $today7Pm && $ticketCreationDate < $today12MidNight)
				{
					$closureDate = Carbon::createFromTimestamp($ticketCreationDate)->addDay(1)->parse('today 12Pm')->timestamp;
				}
				else
				{
					if ($ticketCreationDate > $today12Noon)
					{
						$closureDate = $today7Pm;
					}

					else
					{
						$closureDate = $today12Noon;
					}
				}
				break;
			case config('evibe.ticket.bookingLikeliness.2'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addDay(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.3'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addWeek(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.4'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addMonth(1)->timestamp;
				break;
			case config('evibe.ticket.bookingLikeliness.5'):
				$closureDate = null;
				break;
			case config('evibe.ticket.bookingLikeliness.6'):
				$closureDate = Carbon::createFromTimestamp($closureDate)->parse('12Pm')->addDay(3)->timestamp;
				break;
			default:
				$closureDate = null;
				break;
		}

		$dayOfTheWeek = $closureDate ? Carbon::createFromTimestamp($closureDate)->dayOfWeek : $closureDate;
		$closureDate = $dayOfTheWeek == 0 && $closureDate ? Carbon::createFromTimestamp($closureDate)->addDay(1)->timestamp : $closureDate;

		return $closureDate;
	}

	public function getTicketStatuses()
	{
		$invalid = [
			config('evibe.ticket.status.related'),
			config('evibe.ticket.status.duplicate'),
			config('evibe.ticket.status.irrelevant')
		];

		$progress = [
			config('evibe.ticket.status.initiated'),
			config('evibe.ticket.status.progress'),
			config('evibe.ticket.status.followup'),
			config('evibe.ticket.status.confirmed'),
			config('evibe.ticket.status.auto_pay'),
			config('evibe.ticket.status.service_auto_pay'),
			config('evibe.ticket.status.no_response'),
			config('evibe.ticket.status.invalid_email')
		];

		$followup = [
			config('evibe.ticket.status.initiated'),
			config('evibe.ticket.status.progress'),
			config('evibe.ticket.status.followup'),
			config('evibe.ticket.status.confirmed'),
			config('evibe.ticket.status.auto_pay'),
			config('evibe.ticket.status.service_auto_pay'),
			config('evibe.ticket.status.no_response'),
			config('evibe.ticket.status.invalid_email'),
			config('evibe.ticket.status.booked')
		];

		$end = [
			config('evibe.ticket.status.booked'),
			config('evibe.ticket.status.cancelled'),
			config('evibe.ticket.status.autocancel'),
			config('evibe.ticket.status.not_interested'),
			config('evibe.ticket.status.enquiry')
		];

		return [
			"invalid"  => $invalid,
			"followup" => $followup,
			"progress" => $progress,
			"end"      => array_merge($invalid, $end),
			"all"      => array_merge($invalid, $progress, $end)
		];
	}

	protected function fetchAvailabilityCheckList($request)
	{

		try
		{
			$userId = $this->checkForAccessToken($request);
			$availChecks = AvailabilityCheck::select('availability_check.*', 'ticket.name', 'ticket.phone', 'ticket.event_date', 'type_ticket.name AS type_ticket_name')
			                                ->join('ticket', 'availability_check.ticket_id', '=', 'ticket.id')
			                                ->join('type_ticket', 'type_ticket.id', '=', 'availability_check.map_type_id')
			                                ->whereNull('availability_check.deleted_at')
			                                ->whereNull('ticket.deleted_at')
			                                ->whereNull('type_ticket.deleted_at');
			// search filter
			$searchQuery = $request['query'] ? $request['query'] : false;
			if ($searchQuery)
			{
				$availChecks->where(function ($innerQuery) use ($searchQuery) {
					$innerQuery->where('ticket.name', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.phone', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.alt_phone', 'LIKE', '%' . $searchQuery . '%');
				});
			}
			$isDescended = false;

			// party date filters
			if (isset($request['start_date']) && isset($request['end_date']) && (strtotime($request['start_date']) > strtotime($request['end_date'])))
			{
				$temp = $request['end_date'];
				$request['end_date'] = $request['start_date'];
				$request['start_date'] = $temp;
			}

			if ($request['start_date'] && $filterPDStart = strtotime($request['start_date']))
			{
				$availChecks->where('ticket.event_date', '>=', $filterPDStart);
			}
			else
			{
				$availChecks->where('ticket.event_date', '>=', strtotime(Carbon::now()->startOfDay()));
			}

			if ($request['end_date'] && $filterPDEnd = (strtotime($request['end_date']) + 24 * 60 * 60 - 1))
			{
				$availChecks->where('ticket.event_date', '<=', $filterPDEnd);
			}

			if (isset($request['created_start_date']) && isset($request['created_end_date']) && (strtotime($request['created_start_date']) > strtotime($request['created_end_date'])))
			{
				$temp = $request['created_end_date'];
				$request['created_end_date'] = $request['created_start_date'];
				$request['created_start_date'] = $temp;
			}

			//created at filters
			if ($request['created_start_date'] && $filterCDStart = strtotime($request['created_start_date']))
			{
				$availChecks->whereDate('availability_check.created_at', '>=', date('Y-m-d', $filterCDStart));
			}
			if ($request['created_end_date'] && $filterCDEnd = (strtotime($request['created_end_date'])))
			{
				$availChecks->whereDate('availability_check.created_at', '<=', date('Y-m-d', $filterCDEnd));
			}

			// party date and created at sorting
			if (isset($request['partyDateAsc']) && $request['partyDateAsc'] == 'true')
			{

				$availChecks->orderBy('ticket.event_date', 'ASC');
			}
			elseif (isset($request['partyDateDesc']) && $request['partyDateDesc'] == 'true')
			{

				$availChecks->orderBy('ticket.event_date', 'DESC');
			}
			elseif (isset($request['createdAtAsc']) && $request['createdAtAsc'] == 'true')
			{
				$availChecks->orderBy('availability_check.created_at', 'ASC');
			}
			else
			{
				$isDescended = true;
				$availChecks->orderBy('availability_check.created_at', 'DESC');
			}

			$availChecks = $availChecks->get();

			$allPartnersData = [];
			$allPartners = Planner::select('planner.*')
			                      ->where('is_live', '=', 1)
			                      ->wherenull('deleted_at')
			                      ->get();

			if (count($allPartners) > 0)
			{
				foreach ($allPartners as $allPlanner)
				{
					array_push($allPartnersData, [
						'plannerId'     => $allPlanner->id,
						'plannerName'   => $allPlanner->name,
						'plannerCode'   => $allPlanner->code,
						'plannerPerson' => $allPlanner->person,
						'plannerUserId' => $allPlanner->user_id
					]);
				}
			}

			$availCheckData = [];
			if (count($availChecks))
			{
				foreach ($availChecks as $availCheck)
				{
					$option = null;
					$planners = null;
					switch ($availCheck->map_type_id)
					{
						// packages
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.couple-experiences'):
							$option = Package::select('id', 'code')
							                 ->where('is_live', 1)
							                 ->where('id', '=', $availCheck->map_id)
							                 ->first();
							$planner_package = Package::find($availCheck->map_id);
							if ($planner_package->map_type_id == config('evibe.ticket.type.venues'))
							{
								$planners = Venue::select('venue.name', 'venue.user_id')
									//->where('is_live','=',1)
									             ->where('id', '=', $planner_package->map_id)
								                 ->get();

							}
							else
							{
								$planners = Planner::select('planner.name', 'planner.user_id')
								                   ->where('is_live', '=', 1)
								                   ->wherenull('deleted_at')
								                   ->get();

							}
							break;
						case config('evibe.ticket.type.entertainments'):
							$option = TypeService::select('id', 'code')
							                     ->where('type_service.is_live', 1)
							                     ->where('type_service.id', '=', $availCheck->map_id)
							                     ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.cakes'):
							$option = Cake::select('id', 'code')
							              ->where('cake.is_live', 1)
							              ->where('cake.id', '=', $availCheck->map_id)
							              ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						// decors
						case config('evibe.ticket.type.decors'):
							$option = Decor::select('id', 'code')
							               ->where('decor.is_live', 1)
							               ->where('decor.id', '=', $availCheck->map_id)
							               ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.add-on'):
							$option = AddOn::select('id', 'code')
							               ->where('add_on.is_live', 1)
							               ->where('add_on.id', '=', $availCheck->map_id)
							               ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.planners'):
							$option = null;
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

					}

					$partnersData = [];
					$partners = Notification::select('notifications.*')
					                        ->where('notifications.availability_check_id', '=', $availCheck->id)
					                        ->whereNull('notifications.deleted_at')
					                        ->get();

					if (count($partners) > 0)
					{

						foreach ($partners as $partner)
						{
							$plannerName = $planners->where('user_id', '=', $partner->created_for)->first();
							array_push($partnersData, [
								'name'          => isset($plannerName->name) ? $plannerName->name : null,
								'isReplied'     => $partner->is_replied,
								'repliedAt'     => $partner->replied_at,
								'replyDecision' => $partner->reply_decision,
								'replyText'     => $partner->reply_text,
								'budget'        => $partner->budget_price,
								'info'          => $partner->text
							]);
						}
					}

					// star order
					$starOption = StarOption::where('option_id', $availCheck->map_id)
					                        ->where('option_type_id', $availCheck->map_type_id)
					                        ->whereNull('deleted_at')
					                        ->first();

					array_push($availCheckData, [
						'availCheckId' => $availCheck->id,
						'ticketId'     => $availCheck->ticket_id,
						'createdDate'  => $availCheck->created_at,
						'name'         => $availCheck->name,
						'phoneNumber'  => $availCheck->phone,
						'partyDate'    => $availCheck->event_date,
						'optionType'   => $availCheck->type_ticket_name,
						'budget'       => $availCheck->budget,
						'option'       => ($option && $option->code) ? $option->code : " Custom ",
						'optionId'     => ($option && $option->id) ? $option->id : null,
						'partners'     => $partnersData,
						'isStarOption' => $starOption ? 1 : 0
					]);
				}

			}

			return $res = [
				'availCheckData' => $availCheckData,
				'allPartners'    => $allPartnersData,
				'isDescended'    => $isDescended
			];

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return ['success' => false];
		}
	}

	protected function getPartnerQuickLoginParams($partnerUserId)
	{
		$uniqueNumber = 10022014;

		return [
			'iId'    => $partnerUserId + $uniqueNumber,
			'eToken' => Hash::make($partnerUserId)
		];
	}

	protected function fetchAddOns($cityId, $eventId, $productTypeId, $productId, $ticketBooking)
	{
		$arrayAddOns = null;

		if ($cityId && $eventId)
		{
			if (in_array($eventId, [
				config('evibe.occasion.first_birthday.id'),
				config('evibe.occasion.birthday_2-5.id'),
				config('evibe.occasion.birthday_6-12.id'),
				config('evibe.occasion.naming_ceremony.id')
			]))
			{
				$eventId = config('evibe.occasion.kids_birthdays.id');
			}

			$addOns = AddOn::forEvent($eventId)
			               ->forCity($cityId)
			               ->isLive();

			if ($productId && $productTypeId)
			{
				$tagIds = [];

				switch ($productTypeId)
				{
					// @see: $typeTicketBooking exists only for order processed bookings
					case config('evibe.ticket.type.decor'):
						$tagIds = Decor\DecorTags::where('decor_id', $productId)
						                         ->whereNull('deleted_at')
						                         ->pluck('tag_id')
						                         ->toArray();

						break;

					case config('evibe.ticket.type.planner'):
						// based on ttb for a ticket
						// todo: hard-code ttb
						if ($ticketBooking && $ticketBooking->type_ticket_booking_id)
						{
							$typeTicketBookingId = $ticketBooking->type_ticket_booking_id;
							// attach add-ons to a specific tag and use them
							switch ($typeTicketBookingId)
							{
								case config('evibe.type-ticket-booking.theme-decoration'):
									$tagIds = [
										config('evibe.type-tag.simple-theme-decoration'),
										config('evibe.type-tag.custom-theme-decoration')
									];
									break;
								case config('evibe.type-ticket-booking.custom-balloon-decoration'):
									$tagIds = [
										config('evibe.type-tag.custom-balloon-decoration')
									];
									break;
								case config('evibe.type-ticket-booking.cake'):
									$tagIds = [
										config('evibe.type-tag.cake')
									];
									break;
								case config('evibe.type-ticket-booking.decoration-entertainment'):
									$tagIds = [
										config('evibe.type-tag.decoration-entertainment')
									];
									break;
								case config('evibe.type-ticket-booking.entertainment'):
								case config('evibe.type-ticket-booking.organiser'):
								case config('evibe.type-ticket-booking.photographer'):
								case config('evibe.type-ticket-booking.videographer'):
								case config('evibe.type-ticket-booking.special-artist'):
								case config('evibe.type-ticket-booking.add-on-services'):
									$tagIds = [
										config('evibe.type-tag.service-category') // todo: need to verify
									];
									break;
								case config('evibe.type-ticket-booking.catering'):
								case config('evibe.type-ticket-booking.snacks'):
									$tagIds = [
										config('evibe.type-tag.food'), // todo: need to verify
									];
									break;
								default:
									break;
							}
						}
						break;

					case config('evibe.ticket.type.cake'):
						$tagIds = [
							config('evibe.type-tag.cake')
						];
						break;

					case config('evibe.ticket.type.service'):
					case config('evibe.ticket.type.entertainment'):
						$tagIds = [
							config('evibe.type-tag.service-category') // todo: need to verify
						];
						break;

					case config('evibe.ticket.type.package'):
					case config('evibe.ticket.type.surprises'):
					case config('evibe.ticket.type.generic-package'):
						// try for decor + ent
						$tagIds = PackageTag::where('planner_package_id', $productId)
						                    ->whereNull('deleted_at')
						                    ->pluck('tile_tag_id')
						                    ->toArray();
						break;

					case config('evibe.ticket.type.food'):
						// check if it gets included in package or not
						$tagIds = [
							config('evibe.type-tag.food'), // todo: need to verify
						];
						break;

					default:
						$tagIds = [];
						break;
				}

				$taggedAddOnIds = AddOnTags::whereIn('tag_id', $tagIds)
				                           ->whereNull('deleted_at')
				                           ->pluck('add_on_id')
				                           ->toArray();

				if (count($taggedAddOnIds))
				{
					$addOns = $addOns->whereIn('add_on.id', $taggedAddOnIds);
				}
				else
				{
					// get all the applicable occasion related add-ons (generic)
				}
			}

			$addOns = $addOns->get();

			$sortOrderIds = ProductSortOrder::where('product_type_id', config('evibe.ticket.type.add-on'))
			                                ->where('event_id', $eventId)
			                                ->where('city_id', $cityId)
			                                ->orderBy('score', 'DESC')
			                                ->pluck('product_id')
			                                ->toArray();

			$arrayAddOns = $addOns->all();

			usort($arrayAddOns, function ($a, $b) use ($sortOrderIds) {
				return $this->sortItemsByPriority($a, $b, $sortOrderIds);
			});
		}

		return $arrayAddOns;
	}

	// Get the list of auto bookable packages
	public function sortItemsByPriority($item1, $item2, $priority)
	{
		$item1Id = is_int($item1) ? $item1 : $item1->id;
		$item2Id = is_int($item2) ? $item2 : $item2->id;
		$search1 = array_search($item1Id, $priority);
		$search2 = array_search($item2Id, $priority);

		if ($search1 == $search2)
		{
			return 0;
		}
		elseif ($search1 > $search2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}