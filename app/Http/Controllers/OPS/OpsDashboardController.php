<?php

namespace App\Http\Controllers\OPS;

use App\Http\Controllers\BaseController;
use App\Http\Models\AddOn\AddOn;
use App\Http\Models\AvailCheck\AvailabilityCheck;
use App\Http\Models\AvailCheck\AvailabilityCheckGallery;
use App\Http\Models\Cake;
use App\Http\Models\City;
use App\Http\Models\CustomerOrderProof;
use App\Http\Models\Decor;
use App\Http\Models\Notification;
use App\Http\Models\Package;
use App\Http\Models\Planner;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Http\Models\TypeService;
use App\Http\Models\User;
use App\Http\Models\TicketPostBookingRequests;
use App\Http\Models\CustomerReviews;
use App\Http\Models\TypeEvent;
use App\Http\Models\Types\TypeProof;
use App\Http\Models\TypeTicketStatus;
use App\Http\Models\Util\OptionAvailability;
use App\Http\Models\Util\OptionUnavailability;
use App\Http\Models\Util\StarOption;
use App\Http\Models\Venue;
use App\Jobs\Mail\MailProofValidationStatusToCustomerJob;
use App\Jobs\Sms\SMSProofValidationStatusToCustomerJob;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use App\Http\Controllers\Notification\PaymentNotificationController;
use Illuminate\Support\Facades\Validator;

class OpsDashboardController extends BaseController
{
	public function fetchTabsCount(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);

			$orderConfirmationStatus = [
				config('evibe.ticket.status.confirmed'),
				config('evibe.ticket.status.booked'),
				config('evibe.ticket.status.autopay'),
				config('evibe.ticket.status.service_auto_pay'),
				config('evibe.ticket.status.autocancel')
			];
			$startTime = "2019-03-01 00:00:00";

			$orderConfirmationsCount = TicketBooking::select('ticket_bookings.*', 'ticket.name', 'ticket.email', 'ticket.phone', 'ticket.user_id', 'ticket.status_id', 'ticket_mapping.map_id AS product_id', 'ticket_mapping.map_type_id AS product_type_id')
			                                        ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                        ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
			                                        ->whereIn('ticket.status_id', $orderConfirmationStatus)
			                                        ->whereNull('ticket_bookings.cancelled_at')
			                                        ->whereNull('ticket_bookings.partner_confirmed_at')
			                                        ->whereNull('ticket.deleted_at')
			                                        ->whereNull('ticket_mapping.deleted_at')
			                                        ->whereNull('ticket_bookings.deleted_at')
			                                        ->where('ticket_bookings.party_date_time', '>=', time())
			                                        ->orderBy('ticket_bookings.party_date_time', 'ASC')
			                                        ->get()->count();

			$customerUpdatesCount = TicketPostBookingRequests::select('post_booking_requests.*', 'ticket.name', 'ticket.phone', 'ticket.email', 'ticket.event_date')
			                                                 ->join('ticket', 'ticket.id', '=', 'post_booking_requests.ticket_id')
			                                                 ->where('post_booking_requests.created_at', '>=', $startTime)
			                                                 ->whereNull('post_booking_requests.resolved_at')
			                                                 ->whereNull('ticket.deleted_at')
			                                                 ->whereNull('post_booking_requests.deleted_at')
			                                                 ->get()->count();

			$orderProofTicketIds = CustomerOrderProof::select('ticket.id')
			                                         ->join('ticket', 'ticket.id', '=', 'customer_order_proofs.ticket_id')
			                                         ->where('ticket.event_date', '>=', time())
			                                         ->whereNull('ticket.deleted_at')
			                                         ->whereNull('customer_order_proofs.approved_at')
			                                         ->whereNull('customer_order_proofs.rejected_at')
			                                         ->whereNull('customer_order_proofs.deleted_at')
			                                         ->pluck('ticket.id')->toArray();

			$orderProofTicketIds = array_unique($orderProofTicketIds);

			return response()->json([
				                        'success'                 => true,
				                        'orderConfirmationsCount' => $orderConfirmationsCount,
				                        'customerUpdatesCount'    => $customerUpdatesCount,
				                        'customerProofsCount'     => count($orderProofTicketIds)
			                        ]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['success' => false]);
		}
	}

	public function showOrderConfirmationTickets(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);
			$query = $request->input('query');
			$partyDate = $request->input('partyDate');
			$confirmedDate = $request->input('confirmDate');
			$handlerId = $request->input('handlerId');
			$handlerRoleIds = [
				config('evibe.role.bd'),
				config('evibe.role.customer_delight'),
				config('evibe.role.operations'),
				config('evibe.role.sr_crm'),
				config('evibe.role.super_admin'),
				config('evibe.role.sr_bd')
			];
			$handlerNames = User::select('user.id', 'user.name')
			                    ->whereIn('user.role_id', $handlerRoleIds)->get();
			$handlers = [];
			if (count($handlerNames))
			{
				foreach ($handlerNames as $handler)
				{
					array_push($handlers, [
						'handlerId'   => $handler->id,
						'handlerName' => $handler->name
					]);

				}
			};

			$orderConfirmationStatus = [
				config('evibe.ticket.status.confirmed'),
				config('evibe.ticket.status.booked'),
				config('evibe.ticket.status.autopay'),
				config('evibe.ticket.status.service_auto_pay'),
				config('evibe.ticket.status.autocancel')
			];

			$ticketBookings = TicketBooking::select('ticket_bookings.*', 'ticket.name', 'ticket.email', 'ticket.phone', 'ticket.user_id', 'ticket.status_id', 'ticket_mapping.map_id AS product_id', 'ticket_mapping.map_type_id AS product_type_id')
			                               ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                               ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
			                               ->whereIn('ticket.status_id', $orderConfirmationStatus)
			                               ->whereNull('ticket_bookings.cancelled_at')
			                               ->whereNull('ticket.deleted_at')
			                               ->whereNull('ticket_mapping.deleted_at')
			                               ->whereNull('ticket_bookings.deleted_at');

			if ($query == 'pending' or !isset($query))
			{
				// pending
				$ticketBookings->whereNull('ticket_bookings.partner_confirmed_at');

				$todayStartTime = strtotime(Carbon::createFromTimestamp(time())->startOfDay());
				if ($partyDate)
				{
					$partyDateStartTime = strtotime(Carbon::createFromTimestamp(strtotime($partyDate))->startOfDay());
					if ($partyDateStartTime < $todayStartTime)
					{
						return response()->json([
							                        'errorMsg' => 'Kindly give a party date from today'
						                        ]);
					}
					else
					{
						$partyDateStartTime = strtotime(Carbon::createFromTimestamp(strtotime($partyDate))->startOfDay());
						$partyDateEndTime = strtotime(Carbon::createFromTimestamp(strtotime($partyDate))->endOfDay());

						$ticketBookings->where('ticket_bookings.party_date_time', '>=', $partyDateStartTime)
						               ->where('ticket_bookings.party_date_time', '<', $partyDateEndTime);
					}
				}
				else
				{
					$ticketBookings->where('ticket_bookings.party_date_time', '>=', $todayStartTime);
				}
			}
			else
			{
				// confirmed
				$ticketBookings->whereNotNull('ticket_bookings.partner_confirmed_at');

				if ($confirmedDate)
				{
					// stored as timestamp in DB
					$ticketBookings->whereDate('ticket_bookings.partner_confirmed_at', $confirmedDate);
				}

				if ($handlerId)
				{
					$ticketBookings->where('ticket_bookings.partner_confirmed_handler_id', $handlerId);
				}

				if ($partyDate)
				{
					$partyDateStartTime = strtotime(Carbon::createFromTimestamp(strtotime($partyDate))->startOfDay());
					$partyDateEndTime = strtotime(Carbon::createFromTimestamp(strtotime($partyDate))->endOfDay());

					$ticketBookings->where('ticket_bookings.party_date_time', '>=', $partyDateStartTime)
					               ->where('ticket_bookings.party_date_time', '<', $partyDateEndTime);
				}

				if (!$partyDate && !$confirmedDate)
				{
					// default start time: 30 days
					$startTime = strtotime(Carbon::createFromTimestamp(time())->startOfDay()->subDays(30));
					$ticketBookings->where('ticket_bookings.party_date_time', '>=', $startTime);
				}
			}

			$ticketBookings = $ticketBookings->orderBy('ticket_bookings.party_date_time', 'ASC')
			                                 ->get();

			$orderConfirmationData = [];
			$ticketStatus = TypeTicketStatus::all();

			if (count($ticketBookings))
			{
				foreach ($ticketBookings as $ticketBooking)
				{
					//$productData = $this->fillMappingValues([
					//	                                        'isCollection' => false,
					//	                                        'mapTypeId'    => $ticketBooking->map_type_id,
					//	                                        'mapId'        => $ticketBooking->map_id
					//                                        ]);
					$partner = $ticketBooking->provider;
					$handler = $ticketBooking->handler;

					$starOrder = StarOption::where('option_id', $ticketBooking->product_id)
					                       ->where('option_type_id', $ticketBooking->product_type_id)
					                       ->whereNull('deleted_at')
					                       ->first();

					array_push($orderConfirmationData, [
						'customerName'         => $ticketBooking->name,
						'customerPhone'        => $ticketBooking->phone,
						'customerEmail'        => $ticketBooking->email,
						'statusId'             => $ticketBooking->status_id,
						'status'               => $ticketBooking->status_id ? $ticketStatus->where('id', $ticketBooking->status_id)->first()->name : "",
						'productId'            => $ticketBooking->product_id,
						'productTypeId'        => $ticketBooking->product_type_id,
						'productInfo'          => $ticketBooking->booking_info,
						'partnerId'            => $ticketBooking->map_id,
						'partnerTypeId'        => $ticketBooking->map_type_id,
						'partnerPersonName'    => $partner ? $partner->person : "",
						'partnerCompanyName'   => $partner ? $partner->name : "",
						'partnerPhone'         => $partner ? $partner->phone : "",
						'partnerEmail'         => $partner ? $partner->email : "",
						'partnerCode'          => $partner ? $partner->code : "",
						'partyDateTime'        => $ticketBooking->party_date_time ? date('d M y, g:i a', $ticketBooking->party_date_time) : "",
						'bookingAmount'        => $ticketBooking->booking_amount,
						'advanceAmount'        => $ticketBooking->advance_amount,
						'ticketId'             => $ticketBooking->ticket_id,
						'ticketMappingId'      => $ticketBooking->ticket_mapping_id,
						'ticketBookingId'      => $ticketBooking->id,
						'partnerConfirmedAt'   => $ticketBooking->partner_confirmed_at,
						'confirmedByHandler'   => $handler ? $handler->name : "",
						'confirmedByHandlerId' => $ticketBooking->partner_confirmed_handler_id,
						'isStarOrder'          => $starOrder ? 1 : 0

					]);
				}
			}

			$data = [
				'orderConfirmations' => $orderConfirmationData,
				'handlers'           => $handlers
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json([
				                        'errorMsg' => $exception->getMessage()
			                        ]);
		}
	}

	public function searchOrderConfirmationsData(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);
			$query = $request->input('query');
			$ticketId = $request->input('ticketId');
			$ticketBookingId = $request->input('ticketBookingId');

			// no ticketId fro now

			$orderConfirmationsSearchData = [];

			$orderConfirmationStatus = [
				config('evibe.ticket.status.confirmed'),
				config('evibe.ticket.status.booked'),
				config('evibe.ticket.status.autopay'),
				config('evibe.ticket.status.service_auto_pay'),
				config('evibe.ticket.status.autocancel')
			];

			$ticketStatus = TypeTicketStatus::all();
			$cities = City::all();
			$events = TypeEvent::all();
			$ticketBookings = null;

			if ($query)
			{
				$ticketBookings = TicketBooking::select('ticket_bookings.*', 'ticket.name', 'ticket.email', 'ticket.phone', 'ticket.user_id', 'ticket.status_id', 'ticket.city_id', 'ticket.event_id', 'ticket_mapping.map_id AS product_id', 'ticket_mapping.map_type_id AS product_type_id')
				                               ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
				                               ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
				                               ->whereIn('ticket.status_id', $orderConfirmationStatus)
				                               ->whereNull('ticket_bookings.cancelled_at')
				                               ->whereNull('ticket.deleted_at')
				                               ->whereNull('ticket_mapping.deleted_at')
				                               ->whereNull('ticket_bookings.deleted_at')
				                               ->where('ticket_bookings.party_date_time', '>=', time())
				                               ->where(function ($innerQuery) use ($query) {
					                               $innerQuery->where("ticket.phone", $query)
					                                          ->orWhere("ticket.alt_phone", $query);
				                               })
				                               ->orderBy('ticket_bookings.party_date_time', 'ASC')
				                               ->get();
			}
			elseif ($ticketBookingId)
			{
				$ticketBookings = TicketBooking::select('ticket_bookings.*', 'ticket.name', 'ticket.email', 'ticket.phone', 'ticket.user_id', 'ticket.status_id', 'ticket.city_id', 'ticket.event_id', 'ticket_mapping.map_id AS product_id', 'ticket_mapping.map_type_id AS product_type_id')
				                               ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
				                               ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
				                               ->where('ticket_bookings.id', $ticketBookingId)
				                               ->get();
			}

			if (count($ticketBookings))
			{
				foreach ($ticketBookings as $ticketBooking)
				{
					$partner = $ticketBooking->provider;

					array_push($orderConfirmationsSearchData, [
						'customerName'       => $ticketBooking->name,
						'customerPhone'      => $ticketBooking->phone,
						'customerEmail'      => $ticketBooking->email,
						'statusId'           => $ticketBooking->status_id,
						'status'             => $ticketBooking->status_id ? $ticketStatus->where('id', $ticketBooking->status_id)->first()->name : "",
						'productId'          => $ticketBooking->product_id,
						'productTypeId'      => $ticketBooking->product_type_id,
						'productInfo'        => $ticketBooking->booking_info,
						'partnerId'          => $ticketBooking->map_id,
						'partnerTypeId'      => $ticketBooking->map_type_id,
						'partnerPersonName'  => $partner ? $partner->person : "",
						'partnerCompanyName' => $partner ? $partner->name : "",
						'partnerPhone'       => $partner ? $partner->phone : "",
						'partnerEmail'       => $partner ? $partner->email : "",
						'partnerCode'        => $partner ? $partner->code : "",
						'partyDateTime'      => $ticketBooking->party_date_time ? date('d M y, g:i a', $ticketBooking->party_date_time) : "",
						'bookingAmount'      => $ticketBooking->booking_amount,
						'advanceAmount'      => $ticketBooking->advance_amount,
						'ticketId'           => $ticketBooking->ticket_id,
						'ticketMappingId'    => $ticketBooking->ticket_mapping_id,
						'ticketBookingId'    => $ticketBooking->id,
						'partnerConfirmedAt' => $ticketBooking->partner_confirmed_at ? date('d M y, g:i a', strtotime($ticketBooking->partner_confirmed_at)) : "",
						'cityId'             => $ticketBooking->city_id,
						'city'               => ($ticketBooking->city_id && $cities) ? $cities->where('id', $ticketBooking->city_id)->first()->name : "",
						'eventId'            => $ticketBooking->event_id,
						'event'              => ($ticketBooking->event_id && $events) ? $events->where('id', $ticketBooking->event_id)->first()->name : "",
					]);
				}
			}

			$data = [
				'searchData' => $orderConfirmationsSearchData
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	public function confirmPartner(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$ticketBookingId = $request->input('ticketBookingId');

		try
		{

			$ticketBooking = TicketBooking::find($ticketBookingId);

			if (!$ticketBooking)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.update_function'),
					                                     'url'       => $request->fullUrl(),
					                                     'method'    => $request->method(),
					                                     'message'   => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to find ticket booking details while confirming partner.',
					                                     'exception' => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to find ticket booking details while confirming partner.',
					                                     'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                     'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                     ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$ticket = Ticket::find($ticketBooking->ticket_id);
			if (!$ticket)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.update_function'),
					                                     'url'       => $request->fullUrl(),
					                                     'method'    => $request->method(),
					                                     'message'   => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to find ticket details while confirming partner.',
					                                     'exception' => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to find ticket details while confirming partner.',
					                                     'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . '] [Ticket Id: ' . $ticketBooking->ticket_id . ']',
					                                     'details'   => '[Ticket Booking Id: ' . $ticketBookingId . '] [Ticket Id: ' . $ticketBooking->ticket_id . ']'
				                                     ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			$ticketBooking->partner_confirmed_at = Carbon::now();
			$ticketBooking->updated_at = Carbon::now();
			$ticketBooking->partner_confirmed_handler_id = $userId;
			$ticketBooking->save();

			$successMsg = 'Partner has been successfully confirmed for this order.';

			// Send out auto booking confirmation if all the conditions are satisfied:
			// + Ticket should in 'auto_pay' or 'service_auto_pay'
			// + All the bookings should have partners
			// + All the bookings should have their partners confirmed
			if (($ticket->status_id == config('evibe.ticket.status.autopay')) || ($ticket->status_id == config('evibe.ticket.status.service_auto_pay')))
			{
				$bookings = TicketBooking::where('ticket_id', $ticket->id)->whereNull('cancelled_at')->whereNull('deleted_at')->get();
				$sendConfirmation = true;
				foreach ($bookings as $booking)
				{
					if ($booking->map_id && $booking->map_type_id && $booking->partner_confirmed_at)
					{
						// do nothing
					}
					else
					{
						$sendConfirmation = false;
					}
				}

				if ($sendConfirmation)
				{
					$response = $this->confirmAutoBookingRequest($ticket->id, false, $userId);

					if (isset($response['success']) && $response['success'])
					{
						$successMsg .= ' Auto Booking confirmation successfully sent for the customer ticket';
					}
					else
					{
						// can restrict team, but for now send notifications
						$this->sendAndSaveNonExceptionReport([
							                                     'code'      => config('evibe.error_code.update_function'),
							                                     'url'       => $request->fullUrl(),
							                                     'method'    => $request->method(),
							                                     'message'   => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to send auto booking confirmation.',
							                                     'exception' => '[OpsDashboardController.php - ' . __LINE__ . ']. Unable to send auto booking confirmation.',
							                                     'trace'     => '[Ticket Id: ' . $ticketBooking->ticket_id . ']',
							                                     'details'   => '[Ticket Id: ' . $ticketBooking->ticket_id . ']'
						                                     ]);
					}
				}
			}

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => $successMsg
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json([
				                        'success'  => false,
				                        'errorMsg' => 'Something unexpected happened. Kindly refresh the page and try again.'
			                        ]);
		}

	}

	public function showCustomerUpdates(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);
			$startTime = "2019-03-01 00:00:00";

			$postBookingRequests = TicketPostBookingRequests::select('post_booking_requests.*', 'ticket.name', 'ticket.phone', 'ticket.email', 'ticket.event_date')
			                                                ->join('ticket', 'ticket.id', '=', 'post_booking_requests.ticket_id')
			                                                ->where('post_booking_requests.created_at', '>=', $startTime)
			                                                ->whereNull('post_booking_requests.resolved_at')
			                                                ->whereNull('ticket.deleted_at')
			                                                ->whereNull('post_booking_requests.deleted_at')
			                                                ->get();

			$customerUpdates = [];

			if (count($postBookingRequests))
			{
				foreach ($postBookingRequests as $request)
				{
					array_push($customerUpdates, [
						'customerName'  => $request->name,
						'customerPhone' => $request->phone,
						'customerEmail' => $request->email,
						'query'         => $request->request,
						'queryComments' => $request->comments,
						'queryAskedAt'  => date('d M Y, h:i A', strtotime($request->created_at)),
						'partyDateTime' => $request->event_date ? date('d M Y, h:i A', $request->event_date) : "",
						'ticketId'      => $request->ticket_id,
						'queryId'       => $request->id
					]);
				}
			}

			$data = [
				'customerUpdates' => $customerUpdates
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	public function showCustomerProofs(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);

			$customerTicketIds = CustomerOrderProof::select('ticket.id')
			                                       ->join('ticket', 'ticket.id', '=', 'customer_order_proofs.ticket_id')
			                                       ->where('ticket.event_date', '>=', time())
			                                       ->whereNull('ticket.deleted_at')
			                                       ->whereNull('customer_order_proofs.approved_at')
			                                       ->whereNull('customer_order_proofs.rejected_at')
			                                       ->whereNull('customer_order_proofs.deleted_at')
			                                       ->pluck('ticket.id')->toArray();

			$customerTicketIds = array_unique($customerTicketIds);

			$ticketListData = Ticket::with('customerProofs', 'status')
			                        ->whereIn('id', $customerTicketIds)->get();

			$data = [
				'ticketsListData' => $ticketListData
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	public function searchCustomerProofsData(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);
			$query = $request->input('query');
			$ticketId = $request->input('ticketId');

			$orderConfirmationStatus = [
				config('evibe.ticket.status.confirmed'),
				config('evibe.ticket.status.booked'),
				config('evibe.ticket.status.autopay'),
				config('evibe.ticket.status.service_auto_pay')
			];

			$tickets = null;

			if ($query)
			{
				$tickets = Ticket::with('customerProofs', 'customerProofs.typeProof', 'status', 'city', 'event')
				                 ->where(function ($innerQuery) use ($query) {
					                 $innerQuery->where("phone", $query)
					                            ->orWhere("alt_phone", $query);
				                 })
				                 ->whereIn('status_id', $orderConfirmationStatus)
				                 ->where('event_date', '>=', time())
				                 ->whereNull('deleted_at')
				                 ->orderBy('event_date', 'ASC')
				                 ->get();
			}
			elseif ($ticketId)
			{
				$tickets = Ticket::with('customerProofs', 'customerProofs.typeProof', 'status', 'city', 'event')->where('id', $ticketId)->get();
			}

			$data = [
				'searchData' => $tickets
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	public function validateCustomerProofs(Request $request)
	{
		/*
		 * Can provide validation status individually for each image, but unable to provide support at the time.
		 * So, for now, even if 1 sid eof the proof has issue, will ask the customer to upload the proof again.
		 */

		try
		{

			$userId = $this->checkForAccessToken($request);
			$ticketId = $request->input('ticketId');
			$customerProofData = $request->input('customerProofData');
			$customerProofStatus = $request->input('customerProofStatus');
			$customerProofComments = $request->input('customerProofComments');

			$ticket = Ticket::find($ticketId);
			if (!$ticket)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly unable to fetch required data. Kindly refresh the data and try again.'
				                        ]);
			}

			if (($customerProofStatus == 2) && !$customerProofComments)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Kindly enter comments for rejecting the ID proof'
				                        ]);
			}

			if (count($customerProofData))
			{
				$proofTypes = TypeProof::all();

				//foreach ($customerProofData as $key => $proofDatum)
				//{
				//	if (($proofDatum['status'] == 2) && !$proofDatum['comments'])
				//	{
				//		$typeProofName = $proofTypes->find($proofDatum['proofTypeId'])->name;
				//		$proofSide = $proofDatum['proofSide'] == 1 ? 'Front Side' : 'Back Side';
				//		$proofName = $typeProofName . ' - ' . $proofSide;
				//		$proofDatum['proofName'] = $proofName;
				//
				//		return response()->json([
				//			                        'success'  => false,
				//			                        'errorMsg' => 'Kindly enter comments for rejecting ', $proofName
				//		                        ]);
				//	}
				//}

				$messageToCustomer = $customerProofComments;
				$proofStatus = true;
				foreach ($customerProofData as $proofDatum)
				{
					$customerProof = CustomerOrderProof::find($proofDatum['id']);
					if (!$customerProof)
					{
						Log::error("Unable to find uploaded customer proof: [ticketId: $ticketId]");
						continue;
					}

					if ($customerProofStatus == 1)
					{
						$customerProof->approved_at = Carbon::now();
					}
					else
					{
						$customerProof->rejected_at = Carbon::now();
						$customerProof->handler_comments = $customerProofComments;
						$customerProof->handler_id = $userId;

						$proofStatus = false;
					}

					$customerProof->updated_at = Carbon::now();
					$customerProof->save();
				}

				$tmoToken = $tmoToken = Hash::make($ticketId . config('evibe.token.track-my-order'));
				$tmoLink = config('evibe.live.host') . '/track?ref=proofValidationMail&id=' . $ticketId . '&token=' . $tmoToken;
				$tmoLink = $this->getShortUrl($tmoLink);

				if ($proofStatus)
				{
					$tplCustomer = config('evibe.sms_tpl.customer_proof.accept');
					$replaces = [
						'#customer#' => $ticket->name,
						'#tmoLink#'  => $tmoLink,
					];
				}
				else
				{
					$tplCustomer = config('evibe.sms_tpl.customer_proof.reject');
					$replaces = [
						'#customer#' => $ticket->name,
						'#tmoLink#'  => $tmoLink,
					];
				}

				$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);
				$smsData = [
					'to'   => $ticket->phone,
					'text' => $smsText
				];

				$emailData = [
					'ticketId'          => $ticketId,
					'customerName'      => $ticket->name,
					'customerEmail'     => $ticket->email,
					'partyDate'         => $ticket->event_date ? date('d M Y', $ticket->event_date) : "--",
					'proofStatus'       => $proofStatus,
					'messageToCustomer' => $messageToCustomer,
					'tmoLink'           => $tmoLink,
					'ticketStatusId'    => $ticket->status_id,
					'autoPayStatus'     => config('evibe.ticket.status.autopay')
				];

				$this->dispatch(new SMSProofValidationStatusToCustomerJob($smsData));
				$this->dispatch(new MailProofValidationStatusToCustomerJob($emailData));
			}

			return response()->json(['success' => true]);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['success' => false]);
		}
	}

	public function showCustomerReviews(Request $request)
	{

		try
		{

			$userId = $this->checkForAccessToken($request);

			$minDate = Carbon::now()->startOfDay()->subDays(7);
			$customerReviews = CustomerReviews::select('ticket_review.*', 'ticket.name', 'ticket.phone', 'ticket.email')
			                                  ->join('ticket', 'ticket.id', '=', 'ticket_review.ticket_id')
			                                  ->where('ticket_review.created_at', '>=', $minDate)
			                                  ->get();

			$customerReviewsData = [];
			if (count($customerReviews))
			{
				foreach ($customerReviews as $review)
				{
					$ratingsCount = 0;
					$totalRating = 0;
					$averageRating = 0;
					if ($review->customer_care)
					{
						$totalRating += $review->customer_care;
						$ratingsCount++;
					}

					if ($review->ease_of_booking)
					{
						$totalRating += $review->ease_of_booking;
						$ratingsCount++;
					}

					if ($review->price_for_quality)
					{
						$totalRating += $review->price_for_quality;
						$ratingsCount++;
					}

					if ($totalRating && $ratingsCount)
					{
						$averageRating = $totalRating / $ratingsCount;
					}

					if ($averageRating >= 4)
					{
						// need only reviews with avg.rating < 4
						continue;
					}

					array_push($customerReviewsData, [
						'customerName'     => $review->name,
						'customerPhone'    => $review->phone,
						'customerEmail'    => $review->email,
						'ticketId'         => $review->ticket_id,
						'customerCare'     => $review->customer_care,
						'easeOfBooking'    => $review->ease_of_booking,
						'priceForQuality'  => $review->price_for_quality,
						'averageRating'    => $averageRating,
						'isRecommendEvibe' => $review->is_recommend_evibe,
						'customerReview'   => $review->review,
						'sixStar'          => $review->six_star_review,
						'createdAt'        => date("d M Y H:i A", strtotime($review->created_at))
					]);
				}
			}

			$data = [
				'customerReviews' => $customerReviewsData
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	public function resolveCustomerReviews(Request $request)
	{
		//try
		//{
		//    $userId = $this->checkForAccessToken($request);
		//    $ticketId = $request->input('ticketId');
		//    $comment =$request->input('comment');
		//    Log::info($comment);
		//    Log::info($ticketId);
		//    if(strlen($comment)>5)
		//    {
		//        $resolve=CustomerReviews::where('ticket_id', $ticketId)
		//                                  ->update(['resolve_comment'=>$comment, 'resolved_at'=>Carbon::now(),'resolved_handler_id'=>$userId]);
		//
		//        $data=[];
		//        if($resolve){
		//            return response()->json([
		//                'success' => true
		//            ]);
		//
		//        }
		//        else{
		//            return response()->json([
		//                                        'success' => false,
		//                                        'errorMsg' => 'unable to resolve the review, please try again'
		//
		//                ]);
		//
		//        }
		//    }
		//    else{
		//        return response()->json([
		//            'success' => false,
		//            'errorMsg' => 'Resolve comment length should be minimum of 6 letters'
		//
		//        ]);
		//
		//    }
		//} catch (\Exception $exception)
		//{
		//    Log::error($exception->getMessage());
		//    return response()->json(['success' => false]);
		//}

	}

	protected function confirmAutoBookingRequest($ticketId, $isResend, $handlerId)
	{
		try
		{
			$ticket = Ticket::find($ticketId);
			$token = Hash::make($ticket->phone . $ticket->created_at);
			$typeTicketId = $ticket->type_ticket_id;
			if (!$typeTicketId)
			{
				return response()->json(['success' => false, 'error' => 'mapTypeId is missing']);
			}
			else
			{
				switch ($typeTicketId)
				{
					case config('evibe.ticket.type.packages'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.venue-deals'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.couple-experiences'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.lounge'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.villa'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.resort'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.food'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.priests'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.tents'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.generic-package'):
						$mapTypeId = config('evibe.ticket.type.packages');
						break;
					case config('evibe.ticket.type.cakes'):
						$mapTypeId = config('evibe.ticket.type.cakes');
						break;
					case config('evibe.ticket.type.decors'):
						$mapTypeId = config('evibe.ticket.type.decors');
						break;
					case config('evibe.ticket.type.services'):
					case config('evibe.ticket.type.entertainments'):
						$mapTypeId = config('evibe.ticket.type.services');
						break;
				}

				if ($typeTicketId == config('evibe.ticket.type.services'))
				{
					$typeTicketId = config('evibe.ticket.type.entertainments');
				}

				if ($isResend)
				{
					$isResend = 1;
				}

				$data = [
					'mapTypeId'    => $mapTypeId,
					'typeTicketId' => $typeTicketId,
					'ticketId'     => $ticketId,
					'isFromDash'   => 1,
					'isResend'     => $isResend,
					'token'        => $token,
					'handlerId'    => $handlerId
				];

				$paymentNotification = new PaymentNotificationController();
				$response = $paymentNotification->confirmAutoBookingTicket($data);
				$this->setCreatedHandlerId($ticket, $handlerId);

				return $response;
			}
		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}
	}

	protected function setCreatedHandlerId($ticket, $userId = null)
	{
		if (!$userId)
		{
			$userId = Auth::user()->id;
		}

		if (is_int($ticket))
		{
			$ticket = Ticket::find($ticket); // id is given;
		}

		if (!$ticket->created_handler_id)
		{
			$ticket->created_handler_id = $userId;
			$ticket->save();
		}
	}

	public function showAvailChecks(Request $request)
	{
		try
		{
			$data = $this->fetchAvailabilityCheckList($request);

			return response()->json($data);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function fetchAvailabilityData(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);

			$availCheckId = $request->input('availCheckId');

			$availCheck = AvailabilityCheck::select('availability_check.*', 'ticket.name', 'ticket.phone', 'ticket.event_date', 'type_ticket.name AS type_ticket_name')
			                               ->join('ticket', 'availability_check.ticket_id', '=', 'ticket.id')
			                               ->join('type_ticket', 'type_ticket.id', '=', 'availability_check.map_type_id')
			                               ->where('availability_check.id', '=', $availCheckId)
			                               ->first();

			$imageUrls = AvailabilityCheckGallery::where('availability_check_gallery.availability_check_id', '=', $availCheckId)
			                                     ->wherenull('availability_check_gallery.deleted_at')
			                                     ->get();
			$imageData = [];
			if (count($imageUrls) > 0)
			{
				foreach ($imageUrls as $imageUrl)
				{
					if ($imageUrl->type_id == config('evibe.gallery.type.image'))
					{
						$url = config('evibe.hosts.gallery') . '/ticket/' . $availCheck->ticket_id . '/images/avail-check/' . $availCheckId . '/' . $imageUrl->url;
					}
					else
					{
						$url = $imageUrl->url;
					}
					array_push($imageData, [
						'imageId' => $imageUrl->id,
						'url'     => $url
					]);

				}
			}
			$option = [];
			$planners = [];
			$defaultPartnerId = 0;
			switch ($availCheck->map_type_id)
			{
				// packages
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.couple-experiences'):
					$option = Package::select('id', 'name', 'code')
					                 ->where('is_live', 1)
					                 ->where('id', '=', $availCheck->map_id)
					                 ->first();
					$planner_package = Package::find($availCheck->map_id);
					$defaultPartnerId = $planner_package->map_id;
					if ($planner_package->map_type_id == config('evibe.ticket.type.venues'))
					{
						$planners = Venue::select('venue.*')
							//->where('is_live','=',1)
							             ->where('id', '=', $planner_package->map_id)
						                 ->get();
					}
					break;
				//services
				case config('evibe.ticket.type.entertainments'):
					$option = TypeService::select('id', 'name', 'code')
					                     ->where('type_service.is_live', 1)
					                     ->where('type_service.id', '=', $availCheck->map_id)
					                     ->first();
					break;
				//cakes
				case config('evibe.ticket.type.cakes'):
					$option = Cake::select('id', 'code', 'title AS name', 'provider_id')
					              ->where('cake.is_live', 1)
					              ->where('cake.id', '=', $availCheck->map_id)
					              ->first();
					$defaultPartnerId = $option->provider_id;
					break;
				// decors
				case config('evibe.ticket.type.decors'):
					$option = Decor::select('id', 'name', 'code', 'provider_id')
					               ->where('decor.is_live', 1)
					               ->where('decor.id', '=', $availCheck->map_id)
					               ->first();
					$defaultPartnerId = $option->provider_id;
					break;
				case config('evibe.ticket.type.add-on'):
					$option = AddOn::select('id', 'name', 'code')
					               ->where('add_on.is_live', 1)
					               ->where('add_on.id', '=', $availCheck->map_id)
					               ->first();
					break;

			}
			$availCheckData = [];
			$plannerData = [];
			if ($availCheck)
			{
				array_push($availCheckData, [
					'option'       => (isset($option->code) && $option->code) ? $option->code : 'Custom',
					'customerName' => $availCheck->name,
					'optionType'   => $availCheck->type_ticket_name,
					'optionName'   => (isset($option->name) && $option->name) ? $option->name : null,
					'info'         => $availCheck->info,
					'budget'       => $availCheck->budget,
					'partyDate'    => date("d/m/Y h:i A", $availCheck->event_date),
				]);

				if (count($planners) > 0)
				{
					foreach ($planners as $planner)
					{
						array_push($plannerData, [
							'plannerId'     => $planner->id,
							'plannerName'   => $planner->name,
							'plannerCode'   => $planner->code,
							'plannerPerson' => $planner->person,
							'plannerUserId' => $planner->user_id
						]);
					}
				}

				return $res = [
					'success'          => true,
					'availCheckData'   => $availCheckData[0],
					'imageUrls'        => $imageData,
					'planners'         => $plannerData,
					'defaultPartnerId' => $defaultPartnerId
				];

			}

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return $res = [
				'success'  => false,
				'errorMsg' => 'something went wrong while fetching details,please refresh the browser and try again'
			];
		}
	}

	public function confirmAvailabilityCheck(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$data = $request->all();
			$option = json_decode($data['optionsData']);
			$options = $option[0];
			$availCheckId = $options->availCheckId;
			$selectedPartners = $options->selectedPartners;
			$info = $options->info;
			$budget = $options->budget;
			$title = $options->OptionType;
			$partyDate = $options->partyTime;
			$typeId = config('evibe.enquiry.type.avlCheck');
			$availCheck = AvailabilityCheck::find($availCheckId);
			$ticketId = $availCheck->ticket_id;
			$uploadedImageLinks = json_decode($data['uploadedImageLinks']);
			$deletedImages = json_decode($data['removedImageLinks']);
			$partyDate = Ticket::find($ticketId)->event_date;
			$res = [
				'ticketId'     => $ticketId,
				'availCheckId' => $availCheckId
			];

			if (count($selectedPartners) < 1)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => "no partner selected"
				                        ]);

			}

			$validationArray = [
				'budget' => $budget,
				'info'   => $info
			];

			$rules = [
				'budget' => 'numeric',
				'info'   => 'required|min:10'
			];

			$messages = [
				'budget.numeric' => 'Budget must be a number',
				'info.required'  => 'Please enter some Info about availability check',
				'info.min'       => 'info length should be minimum of 10'
			];

			$validator = Validator::make($validationArray, $rules, $messages);

			if ($validator->fails() || count($selectedPartners) < 1)
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validator->messages()->first()
				                        ]);
			}

			$validImages = [];
			$invalidImageLinks = [];
			foreach ($deletedImages as $deletedImage)
			{
				$image = AvailabilityCheckGallery::find($deletedImage);
				if ($image->type_id != 0)
				{
					$image->deleted_at = Carbon::now();
					$image->save();
				}
				else
				{
					$url = config('evibe.root.gallery') . '/ticket/' . $ticketId . '/images/avail-check/' . $availCheckId . '/' . $image->url;
					$pathInfo = pathinfo($url);
					$fileName = $pathInfo["filename"];
					$extension = $pathInfo["extension"];
					$directory = $pathInfo["dirname"];

					$originalFile = $directory . '/' . $fileName . '_original.' . $extension;
					unlink($url);
					unlink($originalFile);
					$image->deleted_at = Carbon::now();
					$image->save();
				}
			}
			foreach ($uploadedImageLinks as $imageUrl)
			{
				if (!empty($imageUrl))
				{
					try
					{
						$validImg = getimagesize($imageUrl);
						if ($validImg)
						{
							$validImages[] = [
								'title'   => pathinfo($imageUrl, PATHINFO_FILENAME),
								'url'     => $imageUrl,
								'type_id' => 2,
							];
						}
						else
						{
							$invalidImageLinks[] = $imageUrl;
						}
					} catch (\Exception $e)
					{
						$invalidImageLinks[] = $imageUrl;
					}
				}
			}
			if (count($invalidImageLinks) > 0)
			{
				return $res = [
					'success'       => false,
					'errorMsg'      => 'Some image links doesn\'t have images or invalid, please check the links below',
					'hasImageError' => true,
					'invalidImage'  => implode(",", $invalidImageLinks)
				];
			}
			if (isset($validImages) && count($validImages) > 0)
			{
				foreach ($validImages as $key => $value)
				{
					$validImages[$key]['availability_check_id'] = $availCheckId;
				}
				$imageUpload = AvailabilityCheckGallery::insert($validImages);

			}
			foreach ($selectedPartners as $selectedPartner)
			{
				$deadlineHrs = 2;
				$availCheckData = [
					'title'                 => $title,
					'text'                  => $info,
					'type_id'               => $typeId,
					'created_by'            => $userId,
					'created_for'           => $selectedPartner->userId,
					'budget_price'          => $budget,
					'ticket_id'             => $ticketId,
					'availability_check_id' => $availCheckId,
					'is_auto_booking'       => $request->has('isAutoBooking') ? $request->has('isAutoBooking') : 0,
					'deadline_at'           => Carbon::now()->addHour($deadlineHrs)->timestamp // putting deadline as 2 hrs
				];

				$newAvailCheck = Notification::create($availCheckData);
				$screenId = $newAvailCheck->id;
				if ($newAvailCheck)
				{
					$notificationData = [
						'screen'           => config('evibe.partner_app_key.enquiry.details'),
						'screenId'         => $screenId,
						'title'            => "New avail. check for " . date('d/m/y', $partyDate),
						'message'          => "Pls. respond within $deadlineHrs hours",
						'typeNotification' => 'newEnq',
						'userId'           => $selectedPartner->userId,
						'isAutoBook'       => $newAvailCheck->is_auto_booking,
						'isAvailCheck'     => true
					];

					if (isset($notificationData))
					{
						$url = url('partner/v1/notification/create') . '?isFromApi=true';

						try
						{
							$client = new Client();
							$client->request('POST', $url, [
								'headers' => [
								],
								'json'    => $notificationData
							]);

							$res['success'] = true;
						} catch (ClientException $e)
						{
							$apiResponse = $e->getResponse()->getBody(true);
							$apiResponse = \GuzzleHttp\json_decode($apiResponse);
							$res['error'] = $apiResponse->errorMessage;
							$this->sendAndSaveReport($apiResponse->errorMessage);
							$notificationFailure = true;
						}

					}
				}
			}

			return response()->json($res);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			$res = [
				'success'  => false,
				'errorMsg' => 'something went wrong, please try  again'
			];

			return response()->json($res);
		}

	}

	public function showOptionAvailability(Request $request)
	{
		try
		{
			$data = [];

			$allOptionSlots = OptionAvailability::whereNull('deleted_at')
			                                    ->where('bookings_available', '>', 0)
			                                    ->get();

			$optionsData = [];
			// options data
			// for now provide only surprise venues
			$packages = Package::where('type_ticket_id', config('evibe.ticket.type.couple-experiences'))
			                   ->where('map_type_id', config('evibe.ticket.type.venues'))
			                   ->where('is_live', 1)
			                   ->whereNull('deleted_at')
			                   ->get();
			if (count($packages))
			{
				foreach ($packages as $package)
				{
					$optionSlots = $allOptionSlots->where('map_type_id', $package->type_ticket_id)
					                              ->where('map_id', $package->id);

					$optionData = [
						'ouId'         => $package->type_ticket_id . '_' . $package->id,
						'optionId'     => $package->id,
						'optionTypeId' => $package->type_ticket_id,
						'name'         => $package->name,
						'code'         => $package->code
					];

					if (count($optionSlots))
					{
						$optionSlotsData = [];
						foreach ($optionSlots as $optionSlot)
						{
							array_push($optionSlotsData, [
								'slotId'                => $optionSlot->id,
								'slotName'              => $optionSlot->slot_start_time . ' - ' . $optionSlot->slot_end_time,
								'slotStartTime'         => $optionSlot->slot_start_time,
								'slotEndTime'           => $optionSlot->slot_end_time,
								'totalBookingsPossible' => $optionSlot->bookings_available,
							]);
						}

						$optionData['slots'] = $optionSlotsData;
					}

					array_push($optionsData, $optionData);
				}
			}

			$data['options'] = $optionsData;

			$ouId = $request->input('ouId');
			$startDate = $request->input('startDate');
			$endDate = $request->input('endDate');
			$optionId = null;
			$optionTypeId = null;
			if ($ouId)
			{
				$ouIdData = explode('_', $ouId);
				$optionTypeId = $ouIdData[0];
				$optionId = $ouIdData[1];
			}
			else
			{
				// request for optionId and optionTypeId
			}

			// @todo: hardcoded - change later
			$option = Package::find($optionId);

			if (!$startDate && !$endDate)
			{
				// if both startDate and endDate are not provided, default date range
				// @todo: date format? "YYYY-MM-DD"
				$startDate = Carbon::createFromTimestamp(time())->startOfDay()->toDateString();
				$endDate = Carbon::createFromTimestamp(time())->startOfDay()->addDay(7)->toDateString();
			}

			if ($startDate && !$endDate)
			{
				// add 7 days to start date
				$endDate = Carbon::createFromTimestamp(strtotime($startDate))->startOfDay()->addDay(7)->toDateString();
			}

			if ($endDate && !$startDate)
			{
				// subtract 7 days to start date
				$startDate = Carbon::createFromTimestamp(strtotime($startDate))->startOfDay()->subDays(7)->toDateString();
			}

			if (strtotime($endDate) < strtotime($startDate))
			{
				$temp = $startDate;
				$startDate = $endDate;
				$endDate = $temp;
			}

			if ($optionId && $optionTypeId)
			{
				// check for data in option_availability and option_unavailability tables
				// get applicable slot ids
				$availableSlots = $allOptionSlots->where('map_type_id', $optionTypeId)
				                                 ->where('map_id', $optionId);

				if (count($availableSlots))
				{
					$optionSlotsAvailabilityData = [];
					$availableSlotIds = $availableSlots->pluck('id')->toArray();
					$availableSlotIds = array_unique($availableSlotIds); // just in case

					// get all the unavailable slots for the slot ids
					$unavailableSlots = OptionUnavailability::whereIn('option_slot_id', $availableSlotIds);
					if ($startDate)
					{
						$unavailableSlots = $unavailableSlots->where('slot_date', '>=', $startDate);
					}
					if ($endDate)
					{
						$unavailableSlots = $unavailableSlots->where('slot_date', '<=', $endDate);
					}
					$unavailableSlots = $unavailableSlots->whereNull('deleted_at')
					                                     ->get();

					// update data based on bookings while forming the array itself below
					$bookings = TicketBooking::select('ticket_bookings.id', 'ticket_bookings.party_date_time')
					                         ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
					                         ->join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
					                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
					                         ->where('ticket_mapping.map_id', $optionId)
					                         ->where('ticket_mapping.map_type_id', $optionTypeId)
					                         ->where('ticket_bookings.party_date_time', '>=', strtotime($startDate))
					                         ->where('ticket_bookings.party_date_time', '<', strtotime($endDate) + 86400)
					                         ->whereNotNull('ticket.paid_at')
					                         ->where('ticket_bookings.is_advance_paid', 1)
					                         ->whereNull('ticket_bookings.cancelled_at')
					                         ->whereNull('ticket.deleted_at')
					                         ->whereNull('ticket_bookings.deleted_at')
					                         ->get();

					// form an array with the date range and slots
					// @see: available count also needs to be shown for each date - slot
					for ($i = strtotime($startDate); $i <= strtotime($endDate); $i += 86400)
					{
						$optionSlotsAvailabilityData[$i] = [];
						foreach ($availableSlots as $availableSlot)
						{
							$slotStartTime = strtotime(date('Y-m-d', $i) . ' ' . $availableSlot->slot_start_time);
							$slotEndTime = strtotime(date('Y-m-d', $i) . ' ' . $availableSlot->slot_end_time);
							$slotBookings = $bookings->where('party_date_time', '>=', $slotStartTime)
							                         ->where('party_date_time', '<', $slotEndTime);

							$optionSlotsAvailabilityData[$i][$availableSlot->id] = [
								'slotId'                => $availableSlot->id,
								'slotStartTime'         => $availableSlot->slot_start_time,
								'slotEndTime'           => $availableSlot->slot_end_time,
								'totalBookingsPossible' => $availableSlot->bookings_available,
								'existingBookings'      => count($slotBookings),
								'markedUnavailable'     => 0,
							];
						}
					}

					// update data based on unavailability slots marked
					if (count($unavailableSlots))
					{
						foreach ($unavailableSlots as $uSlot)
						{
							$optionSlotsAvailabilityData[strtotime($uSlot->slot_date)][$uSlot['option_slot_id']]['markedUnavailable'] += $uSlot->slots_unavailable;
						}
					}

					$data['slotsData'] = $availableSlots;
					$data['optionSlotsAvailabilityData'] = $optionSlotsAvailabilityData;

					// @see: same date - single user - same slot - multiple time marking as unavailable
				}
			}

			$data['ouId'] = $ouId;
			$data['optionId'] = $optionId;
			$data['optionTypeId'] = $optionTypeId;
			$data['optionName'] = $option ? $option->name : "";
			$data['startDate'] = $startDate;
			$data['endDate'] = $endDate;

			return response()->json($data);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function fetchOptionSlots(Request $request)
	{
		try
		{

			$ouId = $request->input('ouId');
			$ouIdData = explode('_', $ouId);
			$optionTypeId = $ouIdData[0];
			$optionId = $ouIdData[1];

			$optionSlots = OptionAvailability::whereNull('deleted_at')
			                                 ->where('bookings_available', '>', 0)
			                                 ->where('map_type_id', $optionTypeId)
			                                 ->where('map_id', $optionId)
			                                 ->get();

			if (count($optionSlots))
			{
				return response()->json([
					                        'optionSlots' => $optionSlots->toArray(),
					                        'success'     => true
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => true
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			$res = [
				'success'  => false,
				'errorMsg' => 'something went wrong, please try  again'
			];

			return response()->json($res);
		}
	}

	public function addUnavailability(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$ouId = $request->input("ouId");
			$slotId = $request->input("slotId");
			$slotDate = $request->input("slotDate");
			$oaUnavailableSlotsCount = $request->input("oaUnavailableSlotsCount");
			$oaComments = $request->input("oaComments");

			// data validation
			// @todo: optimised data validation
			if ((!$ouId) || ($ouId == "-1"))
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Kindly select an option"
				                        ]);
			}

			if (!$slotId)
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Kindly select a slot"
				                        ]);
			}

			if (!$slotDate)
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Kindly provide the date"
				                        ]);
			}

			if (!$oaComments)
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Kindly provide the unavailability reason"
				                        ]);
			}

			if (!$oaUnavailableSlotsCount)
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Kindly provide the slot bookings count you wish to mark as unavailable"
				                        ]);
			}

			// date validation
			$today = Carbon::createFromTimestamp(time())->startOfDay()->toDateString();
			if ($today > $slotDate)
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Slot date cannot be before today"
				                        ]);
			}

			// slots validation
			$optionAvailabilitySlot = OptionAvailability::find($slotId);
			$ouIdData = explode('_', $ouId);
			$optionId = null;
			$optionTypeId = null;
			if (is_array($ouIdData) && $ouIdData)
			{
				$optionTypeId = $ouIdData[0];
				$optionId = $ouIdData[1];

				if (($optionAvailabilitySlot->map_type_id != $optionTypeId) || ($optionAvailabilitySlot->map_id != $optionId))
				{
					return response()->json([
						                        "success"  => false,
						                        "errorMsg" => "Option and slot mismatch. Kindly update the tech team"
					                        ]);
				}
			}

			// compare the count with bookings and existing unavailability
			$slotDateStart = strtotime($slotDate . ' ' . $optionAvailabilitySlot->slot_start_time);
			$slotDateEnd = strtotime($slotDate . ' ' . $optionAvailabilitySlot->slot_end_time);
			$bookings = TicketBooking::select('ticket_bookings.id')
			                         ->join('ticket_mapping', 'ticket_mapping.id', '=', 'ticket_bookings.ticket_mapping_id')
			                         ->join('ticket', 'ticket.id', '=', 'ticket_mapping.ticket_id')
			                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
			                         ->where('ticket_mapping.map_id', $optionId)
			                         ->where('ticket_mapping.map_type_id', $optionTypeId)
			                         ->where('ticket_bookings.party_date_time', '>=', $slotDateStart)
			                         ->where('ticket_bookings.party_date_time', '<=', $slotDateEnd)
			                         ->whereNotNull('ticket.paid_at')
			                         ->where('ticket_bookings.is_advance_paid', 1)
			                         ->whereNull('ticket_bookings.cancelled_at')
			                         ->whereNull('ticket.deleted_at')
			                         ->whereNull('ticket_bookings.deleted_at')
			                         ->get();
			$bookingsCount = count($bookings);

			$existingUnavailableCount = 0;
			$existingUnavailabilitySlots = OptionUnavailability::where('option_slot_id', $slotId)
			                                                   ->where('slot_date', $slotDate)
			                                                   ->whereNull('deleted_at')
			                                                   ->get();
			if (count($existingUnavailabilitySlots))
			{
				foreach ($existingUnavailabilitySlots as $euSlot)
				{
					$existingUnavailableCount += $euSlot->slots_unavailable;
				}
			}

			if ($oaUnavailableSlotsCount > ($optionAvailabilitySlot->bookings_available - ($bookingsCount + $existingUnavailableCount)))
			{
				return response()->json([
					                        "success"  => false,
					                        "errorMsg" => "Invalid unavailability slots count. You are trying to mark more that possible slots as unavailable. [Total Possible: $optionAvailabilitySlot->bookings_available] [Existing Bookings: $bookingsCount] [Existing Unavailability: $existingUnavailableCount] [New slots you want to mark unavailable: $oaUnavailableSlotsCount]"
				                        ]);
			}

			// saving slots data
			OptionUnavailability::create([
				                             "user_id"              => $userId,
				                             "option_slot_id"       => $slotId,
				                             "slot_date"            => $slotDate,
				                             "slots_unavailable"    => $oaUnavailableSlotsCount,
				                             "unavailable_comments" => $oaComments,
				                             "created_at"           => Carbon::now(),
				                             "updated_at"           => Carbon::now()
			                             ]);

			return response()->json([
				                        "success" => true
			                        ]);
		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			$res = [
				'success'  => false,
				'errorMsg' => 'something went wrong, please try  again'
			];

			return response()->json($res);
		}
	}
}