<?php

namespace App\Http\Controllers\AvailCheck;

use App\Http\Controllers\BaseController;
use App\Http\Models\AvailCheck\PartnerAvailability;
use App\Http\Models\Package;
use App\Http\Models\Planner;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Http\Models\TicketMapping;
use App\Http\Models\TypeTicket;
use App\Http\Models\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AvailCheckController extends BaseController
{
	public function getInitializedData(Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		$partnerData = $this->getPartnerData();

		return response()->json([
			                        "success"     => true,
			                        "partnerData" => $partnerData
		                        ]);
	}

	public function getSpecificPartnerData($partnerTypeId, $partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$isFromMain = isset($request["isFromMain"]) ? $request["isFromMain"] : false;

		if ($partnerTypeId == config("evibe.ticket.type.planners"))
		{
			$partner = Planner::where("code", $partnerCode)
			                  ->first();

			if (!$partner)
			{
				return response()->json([
					                        "success" => true,
					                        "error"   => "Partner not found, Please try again.",
				                        ]);
			}

			$availCheckData = [];
			$ticketData = [];
			$currentTime = Carbon::now()->toDateString();

			$partnerAvailability = PartnerAvailability::where("partner_type_id", $partnerTypeId)
			                                          ->where("partner_id", $partner->id)
			                                          ->where("date", ">=", $currentTime)
			                                          ->get();

			$validTicketIds = Ticket::where("status_id", config("evibe.ticket.status.booked"))
			                        ->where("event_date", ">=", Carbon::now()->startOfDay()->timestamp)
			                        ->get()
			                        ->pluck("id")
			                        ->toArray();

			$partnerBookingsData = TicketBooking::select("party_date_time")
			                                    ->where("map_type_id", $partnerTypeId)
			                                    ->where("map_id", $partner->id)
			                                    ->where("party_date_time", ">=", Carbon::now()->startOfDay()->timestamp)
			                                    ->whereIn("ticket_id", $validTicketIds)
			                                    ->get();

			foreach ($partnerBookingsData as $partnerBookingData)
			{
				$minutesInPartyDate = $partnerBookingData["party_date_time"] % 86400;

				foreach (config("evibe.avail_check.slots") as $key => $value)
				{
					if ($minutesInPartyDate >= $value[0] && $minutesInPartyDate < $value[1])
					{
						$ticketData[date('Y-m-d', $partnerBookingData["party_date_time"])][] = $key;
					}
				}
			}

			foreach ($partnerAvailability as $availability)
			{
				$availCheckData[$availability["date"]][] = $availability["slot"];
			}

			$responseData = [
				"success"        => true,
				"availCheckData" => $availCheckData,
				"ticketData"     => $ticketData,
				"selectedCode"   => $partnerTypeId . "_" . $partnerCode
			];

			if (!$isFromMain)
			{
				$responseData["partnerData"] = $this->getPartnerData();
			}

			return response()->json($responseData);
		}
		elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
		{
			$partner = Venue::where("code", $partnerCode)
			                ->first();
			$productId = isset($request["productId"]) ? $request["productId"] : "";

			if (!$partner || !$productId || $productId == "")
			{
				return response()->json([
					                        "success" => true,
					                        "error"   => "Partner not found, Please try again.",
				                        ]);
			}

			$availCheckData = [];
			$ticketData = [];
			$currentTime = Carbon::now()->toDateString();

			$partnerAvailability = PartnerAvailability::where("partner_type_id", $partnerTypeId)
			                                          ->where("partner_id", $partner->id)
			                                          ->where("date", ">=", $currentTime)
			                                          ->where("package_id", $productId)
			                                          ->get();

			$validTicketIds = Ticket::where("status_id", config("evibe.ticket.status.booked"))
			                        ->where("event_date", ">=", Carbon::now()->startOfDay()->timestamp)
			                        ->get()
			                        ->pluck("id")
			                        ->toArray();

			$partnerBookingsData = TicketBooking::select("party_date_time", "ticket_mapping_id")
			                                    ->where("map_type_id", $partnerTypeId)
			                                    ->where("map_id", $partner->id)
			                                    ->where("party_date_time", ">=", Carbon::now()->startOfDay()->timestamp)
			                                    ->whereIn("ticket_id", $validTicketIds)
			                                    ->get();

			$validPackageIds = TypeTicket::where("is_package", config("evibe.ticket.type.packages"))
			                             ->orWhere("id", config("evibe.ticket.type.packages"))
			                             ->pluck("id")
			                             ->toArray();

			$mappings = TicketMapping::whereIn("id", $partnerBookingsData->pluck("ticket_mapping_id")->toArray())
			                         ->whereIn("map_type_id", $validPackageIds)
			                         ->pluck("map_id", "id");

			foreach ($partnerBookingsData as $partnerBookingData)
			{
				$minutesInPartyDate = $partnerBookingData["party_date_time"] % 86400;

				foreach (config("evibe.avail_check.slots") as $key => $value)
				{
					if ($minutesInPartyDate >= $value[0] && $minutesInPartyDate < $value[1] && (isset($mappings[$partnerBookingData["ticket_mapping_id"]]) && $mappings[$partnerBookingData["ticket_mapping_id"]] == $productId))
					{
						$ticketData[date('Y-m-d', $partnerBookingData["party_date_time"])][] = $key;
					}
				}
			}

			foreach ($partnerAvailability as $availability)
			{
				$availCheckData[$availability["date"]][] = $availability["slot"];
			}

			$responseData = [
				"success"        => true,
				"availCheckData" => $availCheckData,
				"ticketData"     => $ticketData,
				"selectedCode"   => $partnerTypeId . "_" . $partnerCode,
				"venuePackageId" => $productId
			];

			if (!$isFromMain)
			{
				$responseData["partnerData"] = $this->getPartnerData();
			}

			return response()->json($responseData);
		}
		else
		{
			return response()->json([
				                        "success" => true,
				                        "error"   => "No partner data found, Please try again."
			                        ]);
		}
	}

	public function getSpecificPartnerVenueData($partnerTypeId, $partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		$venue = Venue::where("code", $partnerCode)
		              ->get()
		              ->first();

		if (!$venue)
		{
			return response()->json([
				                        "success" => true,
				                        "error"   => "Partner not found, Please try again.",
			                        ]);
		}

		$packages = Package::where("map_type_id", $partnerTypeId)
		                   ->where("map_id", $venue->id)
		                   ->get();

		return response()->json([
			                        "success"      => true,
			                        "partnerData"  => $this->getPartnerData(),
			                        "selectedCode" => $partnerTypeId . "_" . $partnerCode,
			                        "packages"     => $packages
		                        ]);
	}

	public function storeUnavailableSlot($partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$code = explode("_", $partnerCode);

		$partnerTypeId = isset($code[0]) ? $code[0] : "";
		$partnerCode = isset($code[1]) ? $code[1] : "";

		$date = isset($request["date"]) ? $request["date"] : "";
		$slots = isset($request["selectedSlots"]) ? $request["selectedSlots"] : [];

		if (!$date || $date == "" || count($slots) == 0)
		{
			return $this->returnJsonError("Please select all the fields to continue.");
		}

		if ($partnerTypeId == config("evibe.ticket.type.planners"))
		{
			$partner = Planner::where("code", $partnerCode)
			                  ->first();

			if (!$partner)
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->get()
			                                       ->pluck("slot")
			                                       ->toArray();

			if (count($unavailableSlots) > 0 && in_array(config("evibe.avail_check.ids.all"), $unavailableSlots))
			{
				return $this->returnJsonError("Partner is unavailable in all the slots. Please modify the old one to add new.");
			}
			elseif (count($unavailableSlots) > 0)
			{
				if (in_array(config("evibe.avail_check.ids.all"), $slots))
				{
					return $this->returnJsonError("Partner is unavailable in few slots. you can't add all slots until you modify the old one.");
				}
				elseif (count(array_intersect($unavailableSlots, $slots)) > 0)
				{
					return $this->returnJsonError("Partner is already unavailable in few of the slots. Please modify the old one to add new.");
				}
				else
				{
					foreach ($slots as $slot)
					{
						PartnerAvailability::create([
							                            "partner_type_id" => config("evibe.ticket.type.planners"),
							                            "partner_id"      => $partner->id,
							                            "date"            => date('Y-m-d', strtotime($date)),
							                            "slot"            => $slot,
							                            "handler_id"      => $userId
						                            ]);
					}
				}
			}
			else
			{
				foreach ($slots as $slot)
				{
					PartnerAvailability::create([
						                            "partner_type_id" => config("evibe.ticket.type.planners"),
						                            "partner_id"      => $partner->id,
						                            "date"            => date('Y-m-d', strtotime($date)),
						                            "slot"            => $slot,
						                            "handler_id"      => $userId
					                            ]);
				}
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Added successfully."
			                        ]);
		}
		elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
		{
			$partner = Venue::where("code", $partnerCode)
			                ->first();

			$productId = isset($request["venuePackageId"]) ? $request["venuePackageId"] : "";

			if (!$partner || !$productId || $productId == "")
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->where('package_id', $productId)
			                                       ->get()
			                                       ->pluck("slot")
			                                       ->toArray();

			if (count($unavailableSlots) > 0 && in_array(config("evibe.avail_check.ids.all"), $unavailableSlots))
			{
				return $this->returnJsonError("Partner is unavailable in all the slots. Please modify the old one to add new.");
			}
			elseif (count($unavailableSlots) > 0)
			{
				if (in_array(config("evibe.avail_check.ids.all"), $slots))
				{
					return $this->returnJsonError("Partner is unavailable in few slots. you can't add all slots until you modify the old one.");
				}
				elseif (count(array_intersect($unavailableSlots, $slots)) > 0)
				{
					return $this->returnJsonError("Partner is already unavailable in few of the slots. Please modify the old one to add new.");
				}
				else
				{
					foreach ($slots as $slot)
					{
						PartnerAvailability::create([
							                            "partner_type_id" => config("evibe.ticket.type.venues"),
							                            "partner_id"      => $partner->id,
							                            "package_id"      => $productId,
							                            "date"            => date('Y-m-d', strtotime($date)),
							                            "slot"            => $slot,
							                            "handler_id"      => $userId
						                            ]);
					}
				}
			}
			else
			{
				foreach ($slots as $slot)
				{
					PartnerAvailability::create([
						                            "partner_type_id" => config("evibe.ticket.type.venues"),
						                            "partner_id"      => $partner->id,
						                            "package_id"      => $productId,
						                            "date"            => date('Y-m-d', strtotime($date)),
						                            "slot"            => $slot,
						                            "handler_id"      => $userId
					                            ]);
				}
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Added successfully."
			                        ]);
		}
		else
		{
			return $this->returnJsonError("No partner data found, Please try again.");
		}
	}

	public function editUnavailableSlot($partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$code = explode("_", $partnerCode);

		$partnerTypeId = isset($code[0]) ? $code[0] : "";
		$partnerCode = isset($code[1]) ? $code[1] : "";

		$date = isset($request["date"]) ? $request["date"] : "";
		$slots = isset($request["selectedSlots"]) ? $request["selectedSlots"] : [];
		$selectedSlot = isset($request["selectedSlot"]) ? $request["selectedSlot"] : [];
		$selectedDate = isset($request["selectedDate"]) ? $request["selectedDate"] : [];

		if (!$date || $date == "" || count($slots) == 0)
		{
			return $this->returnJsonError("Please select all the fields to continue.");
		}

		if ($partnerTypeId == config("evibe.ticket.type.planners"))
		{
			$partner = Planner::where("code", $partnerCode)
			                  ->first();

			if (!$partner)
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->get()
			                                       ->pluck("slot")
			                                       ->toArray();

			if (count($unavailableSlots) > 0 && in_array(config("evibe.avail_check.ids.all"), $unavailableSlots))
			{
				return $this->returnJsonError("Partner is unavailable in all the slots. Please modify the old one to add new.");
			}
			elseif (count($unavailableSlots) > 0)
			{
				if (count(array_intersect($unavailableSlots, $slots)) > 0)
				{
					return $this->returnJsonError("Partner is already unavailable in few of the slots. Please modify the old one to add new.");
				}
				else
				{
					$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
					                                       ->where("partner_id", $partner->id)
					                                       ->where("date", date('Y-m-d', strtotime($selectedDate)))
					                                       ->where("slot", $selectedSlot)
					                                       ->get()
					                                       ->first();

					if ($unavailableSlots)
					{
						$unavailableSlots->update([
							                          "date" => date('Y-m-d', strtotime($date)),
							                          "slot" => $slots[0]
						                          ]);
					}
				}
			}
			else
			{
				$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
				                                       ->where("partner_id", $partner->id)
				                                       ->where("date", date('Y-m-d', strtotime($selectedDate)))
				                                       ->where("slot", $selectedSlot)
				                                       ->get()
				                                       ->first();

				if ($unavailableSlots)
				{
					$unavailableSlots->update([
						                          "date" => date('Y-m-d', strtotime($date)),
						                          "slot" => $slots[0]
					                          ]);
				}
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Updated successfully."
			                        ]);
		}
		elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
		{
			$partner = Venue::where("code", $partnerCode)
			                ->first();
			$productId = isset($request["venuePackageId"]) ? $request["venuePackageId"] : "";

			if (!$partner || !$productId || $productId == "")
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.venues"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->where('package_id', $productId)
			                                       ->get()
			                                       ->pluck("slot")
			                                       ->toArray();

			if (count($unavailableSlots) > 0 && in_array(config("evibe.avail_check.ids.all"), $unavailableSlots))
			{
				return $this->returnJsonError("Partner is unavailable in all the slots. Please modify the old one to add new.");
			}
			elseif (count($unavailableSlots) > 0)
			{
				if (count(array_intersect($unavailableSlots, $slots)) > 0)
				{
					return $this->returnJsonError("Partner is already unavailable in few of the slots. Please modify the old one to add new.");
				}
				else
				{
					$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.venues"))
					                                       ->where("partner_id", $partner->id)
					                                       ->where("date", date('Y-m-d', strtotime($selectedDate)))
					                                       ->where('package_id', $productId)
					                                       ->where("slot", $selectedSlot)
					                                       ->get()
					                                       ->first();

					if ($unavailableSlots)
					{
						$unavailableSlots->update([
							                          "date" => date('Y-m-d', strtotime($date)),
							                          "slot" => $slots[0]
						                          ]);
					}
				}
			}
			else
			{
				$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
				                                       ->where("partner_id", $partner->id)
				                                       ->where("date", date('Y-m-d', strtotime($selectedDate)))
				                                       ->where('package_id', $productId)
				                                       ->where("slot", $selectedSlot)
				                                       ->get()
				                                       ->first();

				if ($unavailableSlots)
				{
					$unavailableSlots->update([
						                          "date" => date('Y-m-d', strtotime($date)),
						                          "slot" => $slots[0]
					                          ]);
				}
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Added successfully."
			                        ]);
		}
		else
		{
			return $this->returnJsonError("No partner data found, Please try again.");
		}
	}

	public function checkUnavailableSlot($partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$code = explode("_", $partnerCode);

		$partnerTypeId = isset($code[0]) ? $code[0] : "";
		$partnerCode = isset($code[1]) ? $code[1] : "";

		$selectedSlot = isset($request["slot"]) ? $request["slot"] : [];
		$selectedDate = isset($request["date"]) ? $request["date"] : [];

		if ($partnerTypeId == config("evibe.ticket.type.planners"))
		{
			$partner = Planner::where("code", $partnerCode)
			                  ->first();

			if (!$partner)
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$ticketData = [];

			$partnerAvailability = PartnerAvailability::where("partner_type_id", $partnerTypeId)
			                                          ->where("partner_id", $partner->id)
			                                          ->where("date", date("Y-m-d", strtotime($selectedDate)))
			                                          ->where("slot", $selectedSlot)
			                                          ->get();

			if ($partnerAvailability->count() > 0)
			{
				return $this->returnJsonError("Partner not available on selected slot & date.");
			}

			$validTicketIds = Ticket::where("status_id", config("evibe.ticket.status.booked"))
			                        ->where("event_date", ">=", Carbon::now()->startOfDay()->timestamp)
			                        ->get()
			                        ->pluck("id")
			                        ->toArray();

			$partnerBookingsData = TicketBooking::select("party_date_time")
			                                    ->where("map_type_id", $partnerTypeId)
			                                    ->where("map_id", $partner->id)
			                                    ->where("party_date_time", ">=", Carbon::now()->startOfDay()->timestamp)
			                                    ->whereIn("ticket_id", $validTicketIds)
			                                    ->get();

			foreach ($partnerBookingsData as $partnerBookingData)
			{
				$minutesInPartyDate = $partnerBookingData["party_date_time"] % 86400;

				foreach (config("evibe.avail_check.slots") as $key => $value)
				{
					if ($minutesInPartyDate >= $value[0] && $minutesInPartyDate < $value[1])
					{
						$ticketData[date('Y-m-d', $partnerBookingData["party_date_time"])][] = $key;
					}
				}
			}

			if (isset($ticketData[$selectedDate]) && in_array($selectedSlot, $selectedDate))
			{
				return $this->returnJsonError("Partner not available on selected slot & date.");
			}
			else
			{
				return response()->json([
					                        "success" => true
				                        ]);
			}
		}
		elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
		{
			$partner = Venue::where("code", $partnerCode)
			                ->first();
			$productId = isset($request["venuePackageId"]) ? $request["venuePackageId"] : "";

			if (!$partner || !$productId || $productId == "")
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$ticketData = [];

			$partnerAvailability = PartnerAvailability::where("partner_type_id", $partnerTypeId)
			                                          ->where("partner_id", $partner->id)
			                                          ->where("date", date("Y-m-d", strtotime($selectedDate)))
			                                          ->where("slot", $selectedSlot)
			                                          ->where("package_id", $productId)
			                                          ->get();

			if ($partnerAvailability->count() > 0)
			{
				return $this->returnJsonError("Partner not available on selected slot & date.");
			}

			$validTicketIds = Ticket::where("status_id", config("evibe.ticket.status.booked"))
			                        ->where("event_date", ">=", Carbon::now()->startOfDay()->timestamp)
			                        ->get()
			                        ->pluck("id")
			                        ->toArray();

			$partnerBookingsData = TicketBooking::select("party_date_time", "ticket_mapping_id")
			                                    ->where("map_type_id", $partnerTypeId)
			                                    ->where("map_id", $partner->id)
			                                    ->where("party_date_time", ">=", Carbon::now()->startOfDay()->timestamp)
			                                    ->whereIn("ticket_id", $validTicketIds)
			                                    ->get();

			$mappings = TicketMapping::whereIn("id", $partnerBookingsData->pluck("ticket_mapping_id")->toArray())
			                         ->where("map_type_id", config("evibe.ticket.type.packages"))
			                         ->pluck("map_id", "id");

			foreach ($partnerBookingsData as $partnerBookingData)
			{
				$minutesInPartyDate = $partnerBookingData["party_date_time"] % 86400;

				foreach (config("evibe.avail_check.slots") as $key => $value)
				{
					if ($minutesInPartyDate >= $value[0] && $minutesInPartyDate < $value[1] && (isset($mappings[$partnerBookingData["ticket_mapping_id"]]) && $mappings[$partnerBookingData["ticket_mapping_id"]] == $productId))
					{
						$ticketData[date('Y-m-d', $partnerBookingData["party_date_time"])][] = $key;
					}
				}
			}

			if (isset($ticketData[$selectedDate]) && in_array($selectedSlot, $selectedDate))
			{
				return $this->returnJsonError("Partner not available on selected slot & date.");
			}
			else
			{
				return response()->json([
					                        "success" => true
				                        ]);
			}
		}
		else
		{
			return $this->returnJsonError("No partner data found, Please try again.");
		}
	}

	public function deleteUnavailableSlot($partnerCode, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$code = explode("_", $partnerCode);

		$partnerTypeId = isset($code[0]) ? $code[0] : "";
		$partnerCode = isset($code[1]) ? $code[1] : "";

		$date = isset($request["date"]) ? $request["date"] : "";
		$slot = isset($request["slot"]) ? $request["slot"] : "";

		if (!$date || $date == "" || !$slot || $slot == "")
		{
			return $this->returnJsonError("Please select all the fields to continue.");
		}

		if ($partnerTypeId == config("evibe.ticket.type.planners"))
		{
			$partner = Planner::where("code", $partnerCode)
			                  ->first();

			if (!$partner)
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.planners"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->where("slot", $slot)
			                                       ->get()
			                                       ->first();

			if ($unavailableSlots)
			{
				$unavailableSlots->update([
					                          "deleted_at" => Carbon::now()
				                          ]);
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Deleted successfully."
			                        ]);
		}
		elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
		{
			$partner = Venue::where("code", $partnerCode)
			                ->first();

			$productId = isset($request["venuePackageId"]) ? $request["venuePackageId"] : "";

			if (!$partner || !$productId || $productId == "")
			{
				return $this->returnJsonError("Partner not found, Please try again.");
			}

			$unavailableSlots = PartnerAvailability::where("partner_type_id", config("evibe.ticket.type.venues"))
			                                       ->where("partner_id", $partner->id)
			                                       ->where("date", date('Y-m-d', strtotime($date)))
			                                       ->where('package_id', $productId)
			                                       ->where("slot", $slot)
			                                       ->get()
			                                       ->first();

			if ($unavailableSlots)
			{
				$unavailableSlots->update([
					                          "deleted_at" => Carbon::now()
				                          ]);
			}

			return response()->json([
				                        "success" => true,
				                        "error"   => "Slots Deleted successfully."
			                        ]);
		}
		else
		{
			return $this->returnJsonError("No partner data found, Please try again.");
		}
	}

	private function getPartnerData()
	{
		$partnerData = [];

		/*
		 * Getting all the valid planners & venues, merging into a single array with code as key
		 */
		$validPlanners = Planner::select("person", "name", "code", "id")
		                        ->where("is_live", 1)
		                        ->whereNull("deleted_at")
		                        ->get()
		                        ->toArray();

		foreach ($validPlanners as $validPlanner)
		{
			$partnerData[config("evibe.ticket.type.planners") . "_" . $validPlanner["code"]] = $validPlanner["name"] . " [" . $validPlanner["person"] . " - " . $validPlanner["code"] . "]";
		}

		$validVenues = Venue::select("person", "name", "code", "id")
		                    ->where("is_live", 1)
		                    ->whereNull("deleted_at")
		                    ->get()
		                    ->toArray();

		foreach ($validVenues as $validVenue)
		{
			$partnerData[config("evibe.ticket.type.venues") . "_" . $validVenue["code"]] = $validVenue["name"] . " [" . $validVenue["person"] . " - " . $validVenue["code"] . "]";
		}

		return $partnerData;
	}

	private function returnJsonError($errorMessage)
	{
		return response()->json([
			                        "success" => false,
			                        "error"   => $errorMessage
		                        ]);
	}
}