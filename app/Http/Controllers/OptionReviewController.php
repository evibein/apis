<?php

namespace App\Http\Controllers;

use App\Http\Models\AddOn\AddOn;
use App\Http\Models\Cake;
use App\Http\Models\City;
use App\Http\Models\Decor;
use App\Http\Models\DeliveryGallery;
use App\Http\Models\OptionReview;
use App\Http\Models\OptionReviewGallery;
use App\Http\Models\Package;
use App\Http\Models\Planner;
use App\Http\Models\TypeEvent;
use App\Http\Models\TypeService;
use App\Http\Models\TypeTicket;
use App\Http\Models\Venue;
use App\Models\Review\Review;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class OptionReviewController extends BaseController
{
	public function showUnmappedReviews(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$optionType = TypeTicket::select("name", "id")->get();
			$optionTypeData = [];
			$partners = [];

			$vendors = Planner::select("id", "name", "code", "person", "phone", "alt_phone")
			                  ->whereNull('deleted_at')
			                  ->get();
			$venues = Venue::select("id", "name", "code", "person", "phone", "alt_phone")
			               ->whereNull('deleted_at')
			               ->get();

			foreach ($vendors as $vendor)
			{
				$vendor = $vendor->toArray();
				$vendor['type'] = config("evibe.ticket.type.planners");
				$partners[] = $vendor;
			}

			foreach ($venues as $venue)
			{
				$venue = $venue->toArray();
				$venue['type'] = config("evibe.ticket.type.venues");
				$partners[] = $venue;
			}

			$cities = City::select('id', 'name')
			              ->where('is_active', 1)
			              ->whereNull('deleted_at')
			              ->get();

			$occasions = TypeEvent::select('id', 'name')
			                      ->where('show_customer', 1)
			                      ->whereNull('deleted_at')
			                      ->get();

			$requiredOptionTypeIds = [
				config("evibe.ticket.type.packages"),
				config("evibe.ticket.type.cakes"),
				config("evibe.ticket.type.decors"),
				config("evibe.ticket.type.entertainments"),
				config("evibe.ticket.type.villa"),
				config("evibe.ticket.type.food"),
				config("evibe.ticket.type.couple-experiences"),
				config("evibe.ticket.type.priests"),
				config("evibe.ticket.type.tents"),
				config("evibe.ticket.type.add-on")
			];

			if (count($optionType))
			{
				foreach ($optionType as $option)
				{
					if (in_array($option->id, $requiredOptionTypeIds))
					{
						array_push($optionTypeData, [
							'optionTypeId'    => $option->id,
							'optionTypeValue' => $option->name
						]);
					}
				}
			}

			$reviews = Review::select('planner_review.*', 'ticket_mapping.map_type_id AS option_type_id', 'ticket_mapping.map_id AS option_id', 'ticket_bookings.party_date_time', 'ticket_bookings.ticket_id', 'ticket_bookings.map_id AS partner_id', 'ticket_bookings.map_type_id AS partner_type_id', 'ticket_bookings.booking_info')
			                 ->join('ticket_bookings', 'planner_review.ticket_booking_id', '=', 'ticket_bookings.id')
			                 ->join('ticket_mapping', 'ticket_bookings.ticket_mapping_id', '=', 'ticket_mapping.id')
			                 ->join('ticket', 'ticket_mapping.ticket_id', '=', 'ticket.id')
			                 ->whereNull('planner_review.deleted_at')
			                 ->whereNull('ticket_bookings.deleted_at')
			                 ->whereNull('ticket_mapping.deleted_at')
			                 ->whereNull('ticket.deleted_at')
			                 ->whereNotIn('planner_review.id', function ($query) {

				                 $query->select('partner_review_id')->from('option_review');
			                 });

			// filters
			$isShowReset = false;

			// name, reviewID, mobile review filter
			$searchQuery = isset($request['query']) && $request['query'] ? $request['query'] : false;
			if ($searchQuery)
			{
				$isShowReset = true;
				$reviews->where(function ($innerQuery) use ($searchQuery) {
					$innerQuery->where('planner_review.name', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.phone', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.email', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('planner_review.id', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('planner_review.review', 'LIKE', '%' . $searchQuery . '%');
				});
			}

			// party date filter
			if ($request['start_date'] && $filterPDStart = strtotime($request['start_date']))
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.party_date_time', '>=', $filterPDStart);
			}

			if ($request['end_date'] && $filterPDEnd = (strtotime($request['end_date']) + 24 * 60 * 60 - 1))
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.party_date_time', '<=', $filterPDEnd);
			}

			// rating Filter
			if (isset($request['min_rating']) && $request['min_rating'])
			{
				$isShowReset = true;
				$reviews->where('planner_review.rating', '>=', $request['min_rating']);
			}

			if (isset($request['max_rating']) && $request['max_rating'])
			{
				$isShowReset = true;
				$reviews->where('planner_review.rating', '<=', $request['max_rating']);
			}

			// provider filters
			$partner = explode('-', $request['p_id']);
			$partnerId = isset($partner[0]) ? $partner[0] : null;
			$partnerTypeId = isset($partner[1]) ? $partner[1] : null;
			if ($partnerId != 'all' && $partnerTypeId != 'all' && $partnerId > 0 && $partnerTypeId > 0)
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.map_type_id', $partnerTypeId)
				        ->where('ticket_bookings.map_id', $partnerId);
			}

			// city filter
			$cityId = isset($request['city']) && $request['city'] ? $request['city'] : 'all';
			if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
			{
				$isShowReset = true;
				$reviews->where('ticket.city_id', $cityId);
			}
			$eventId = isset($request['occasion']) && $request['occasion'] ? $request['occasion'] : 'all';
			if ($eventId != 'all' && ($eventId = (int)$eventId) && $eventId > 0)
			{
				$isShowReset = true;
				$reviews->where('ticket.event_id', $eventId);
			}

			$reviews = $reviews->orderBy('planner_review.updated_at', 'DESC')->paginate(15);

			$pageLinks = $reviews->links()->toHtml();

			$reviewsData = [];

			if (count($reviews))
			{
				foreach ($reviews as $review)
				{
					$optionType = TypeTicket::find($review->option_type_id)->name;
					if ($review->partner_type_id == config('evibe.ticket.type.planners'))
					{
						$partnerData = Planner::find($review->partner_id);
					}
					else
					{
						$partnerData = Venue::find($review->partner_id);
					}
					$option = null;
					$deliveryImageData = [];
					$deliveryImages = DeliveryGallery::select('delivery_gallery.*')
					                                 ->where('delivery_gallery.ticket_booking_id', $review->ticket_booking_id)
					                                 ->whereNull('delivery_gallery.deleted_at')
					                                 ->get();
					if (count($deliveryImages))
					{
						foreach ($deliveryImages as $deliveryImage)
						{
							$url = config('evibe.hosts.gallery') . "/ticket/$review->ticket_id/$review->ticket_booking_id/partner/$deliveryImage->url";
							$deliveryImageData[$deliveryImage->id] = $url;
						}
					}

					switch ($review->option_type_id)
					{
						// packages
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.couple-experiences'):
							$option = Package::select('id', 'name', 'code')
							                 ->where('is_live', 1)
							                 ->where('id', '=', $review->option_id)
							                 ->first();

							break;
						//services
						case config('evibe.ticket.type.entertainments'):
							$option = TypeService::select('id', 'name', 'code')
							                     ->where('type_service.is_live', 1)
							                     ->where('type_service.id', '=', $review->option_id)
							                     ->first();
							break;
						//cakes
						case config('evibe.ticket.type.cakes'):
							$option = Cake::select('id', 'code', 'title AS name', 'provider_id')
							              ->where('cake.is_live', 1)
							              ->where('cake.id', '=', $review->option_id)
							              ->first();
							break;
						// decors
						case config('evibe.ticket.type.decors'):
							$option = Decor::select('id', 'name', 'code', 'provider_id')
							               ->where('decor.is_live', 1)
							               ->where('decor.id', '=', $review->option_id)
							               ->first();
							break;
						case config('evibe.ticket.type.add-on'):
							$option = AddOn::select('id', 'name', 'code')
							               ->where('add_on.is_live', 1)
							               ->where('add_on.id', '=', $review->option_id)
							               ->first();
							break;
					}

					array_push($reviewsData, [
						'customerName'     => $review->name,
						'customerLocation' => $review->location,
						'ticketId'         => $review->ticket_id,
						'partyDate'        => $review->party_date_time ? date('d-M-Y h-i A', $review->party_date_time) : null,
						'reviewId'         => $review->id,
						'rating'           => $review->rating,
						'review'           => $review->review,
						'optionTypeId'     => in_array($review->option_type_id, $requiredOptionTypeIds) ? $review->option_type_id : null,
						'optionId'         => in_array($review->option_type_id, $requiredOptionTypeIds) ? $review->option_id : null,
						'optionType'       => $optionType,
						'option'           => (isset($option->code) && $option->code) ? $option->code : null,
						'optionName'       => (isset($option->name) && $option->name) ? $option->name : null,
						'links'            => $deliveryImageData,
						'partnerName'      => $partnerData->name,
						'partnerPerson'    => $partnerData->person,
						'bookingInfo'      => $review->booking_info,
						'reviewAt'         => $review->created_at ? date('d-M-Y h-i A', strtotime($review->created_at)) : null,

					]);
				}
			}

			$res = [
				'reviewsData'    => $reviewsData,
				'optionTypeData' => $optionTypeData,
				'partners'       => array_unique($partners, SORT_REGULAR),
				'links'          => $pageLinks,
				'cities'         => $cities,
				'occasions'      => $occasions,
				'isShowReset'    => $isShowReset
			];

			return response()->json($res);

		}
		catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}

	}

	public function getOptionsByType(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$mapTypeId = $request->input('optionTypeId');
		$hasProvider = false;
		$mappingValues = $mappers = [];
		switch ($mapTypeId)
		{
			// packages
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.couple-experiences'):
				$mappers = Package::with("provider")
				                  ->where('is_live', 1)
				                  ->where('type_ticket_id', $mapTypeId)
				                  ->get();
				$hasProvider = true;
				break;

			case config('evibe.ticket.type.entertainments'):
				$mappers = TypeService::select('type_service.*')
				                      ->where('type_service.is_live', 1)
				                      ->whereNull('type_service.deleted_at')
				                      ->get();

				break;

			case config('evibe.ticket.type.cakes'):
				$mappers = Cake::with("provider")
				               ->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
				               ->where('cake.is_live', 1)
				               ->whereNull('cake.deleted_at')
				               ->whereNull('cake_event.deleted_at')
				               ->select('cake.*')
				               ->get();
				$hasProvider = true;
				break;

			// decors
			case config('evibe.ticket.type.decors'):
				$mappers = Decor::with("provider")
				                ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
				                ->where('decor.is_live', 1)
				                ->whereNull('decor.deleted_at')
				                ->select('decor.*')
				                ->get();
				$hasProvider = true;
				break;

			case config('evibe.ticket.type.add-on'):
				$mappers = AddOn::select('add_on.*')
				                ->where('add_on.is_live', 1)
				                ->whereNull('add_on.deleted_at')
				                ->get();
		}

		if (count($mappers) > 0)
		{
			foreach ($mappers as $mapper)
			{
				$basicVal = [
					'code' => $mapper->code,
					'name' => $mapTypeId == config('evibe.ticket.type.cakes') ? $mapper->title : ($mapper->name ? $mapper->name : ""),
					'info' => $mapper->info
				];

				if ($mapTypeId == config('evibe.ticket.type.decors'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->min_price,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.cakes'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price_per_kg,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.entertainments'))
				{
					$servicePrice = $this->formatPrice($mapper->min_price);
					$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $servicePrice,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.add-on'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price_per_unit,

					]);
				}
				else
				{
					// packages exists, but vendors is not active
					$isValid = $hasProvider ? $mapper->provider : $mapper->person;
					if (!$isValid)
					{
						continue;
					}

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price,

					]);
				}
			}
		}

		return $mappingValues;
	}

	public function saveReviewMapping(Request $request)
	{

		try
		{
			$userId = $this->checkForAccessToken($request);
			$reviewId = $request->input('reviewId');
			$optionTypeId = $request->input('optionTypeId');
			$optionId = $request->input('optionId');
			$imagesId = $request->input('imagesId') ? $request->input('imagesId') : [];
			$edit = $request->input('isEdit');

			$rules = [
				'reviewId'     => 'required',
				'optionTypeId' => 'required',
				'optionId'     => 'required',
			];

			$messages = [
				'reviewId.required'     => 'Please provide Review Id',
				'optionTypeId.required' => 'Please select Option Type',
				'optionId.required'     => 'Please Select option'
			];

			$validator = Validator::make($request->json()->all(), $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validator->errors()->first() ? $validator->errors()->first() : 'Some error occurred'
				                        ]
				);
			}

			if (!$edit) //mapping
			{
				$result = OptionReview:: create([
					                                'option_type_id'    => $optionTypeId,
					                                'partner_review_id' => $reviewId,
					                                'option_id'         => $optionId,
					                                'created_at'        => Carbon::now(),
					                                'updated_at'        => Carbon::now()
				                                ]);

				$review = Review::find($reviewId);
				$review->is_accepted = 1;
				$review->accepted_by_id = $userId;
				$review->updated_at = Carbon::now();
				$review->save();

				if ($result)
				{
					if (count($imagesId))
					{
						foreach ($imagesId as $imageId)
						{
							OptionReviewGallery::create([
								                            'option_review_id'    => $result->id,
								                            'delivery_gallery_id' => $imageId,
								                            'created_at'          => Carbon::now(),
								                            'updated_at'          => Carbon::now()
							                            ]);

						}
					}
				}
			}
			else
			{
				$result = OptionReview::select('option_review.*')
				                      ->where('option_review.partner_review_id', $reviewId)
				                      ->whereNull('option_review.deleted_at')
				                      ->first();
				$result->option_type_id = $optionTypeId;
				$result->option_id = $optionId;
				$result->updated_at = Carbon::now();
				$result->save();

				$mappedImages = OptionReviewGallery::select('option_review_gallery.delivery_gallery_id')
				                                   ->where('option_review_gallery.option_review_id', $result->id)
				                                   ->whereNull('option_review_gallery.deleted_at')
				                                   ->get();
				$mappedImageData = [];
				foreach ($mappedImages as $mappedImage)
				{
					$mappedImageData[] = $mappedImage['delivery_gallery_id'];
				}

				$deletedImagesId = array_diff($mappedImageData, $imagesId);
				$addedImagesId = array_diff($imagesId, $mappedImageData);

				foreach ($addedImagesId as $addedImageId)
				{
					OptionReviewGallery::create([
						                            'option_review_id'    => $result->id,
						                            'delivery_gallery_id' => $addedImageId,
						                            'created_at'          => Carbon::now(),
						                            'updated_at'          => Carbon::now()
					                            ]);
				}

				foreach ($deletedImagesId as $deletedImageId)
				{
					$deleteImage = OptionReviewGallery::select('*')
					                                  ->where('delivery_gallery_id', $deletedImageId)
					                                  ->whereNull('deleted_at')
					                                  ->first();
					$deleteImage->deleted_at = Carbon::now();
					$deleteImage->save();
				}
			}
			if ($result)
			{
				return response()->json([
					                        'success' => true,
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'some error occured while mapping the review'
				                        ]);
			}

		}
		catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	public function showMappedReviews(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$cities = City::select('id', 'name')
			              ->where('is_active', 1)
			              ->whereNull('deleted_at')
			              ->get();
			$occasions = TypeEvent::select('id', 'name')
			                      ->where('show_customer', 1)
			                      ->whereNull('deleted_at')
			                      ->get();
			$partners = [];

			$vendors = Planner::select("id", "name", "code", "person", "phone", "alt_phone")
			                  ->whereNull('deleted_at')
			                  ->get();
			$venues = Venue::select("id", "name", "code", "person", "phone", "alt_phone")
			               ->whereNull('deleted_at')
			               ->get();
			foreach ($vendors as $vendor)
			{
				$vendor = $vendor->toArray();
				$vendor['type'] = config("evibe.ticket.type.planners");
				$partners[] = $vendor;
			}
			foreach ($venues as $venue)
			{
				$venue = $venue->toArray();
				$venue['type'] = config("evibe.ticket.type.venues");
				$partners[] = $venue;
			}

			$reviews = Review::select('planner_review.*', 'option_review.id AS option_review_id', 'option_review.option_type_id', 'option_review.option_id', 'ticket_bookings.party_date_time', 'ticket_bookings.ticket_id', 'ticket_bookings.map_id AS partner_id', 'ticket_bookings.map_type_id AS partner_type_id', 'ticket_bookings.booking_info')
			                 ->join('ticket_bookings', 'planner_review.ticket_booking_id', '=', 'ticket_bookings.id')
			                 ->join('ticket_mapping', 'ticket_bookings.ticket_mapping_id', '=', 'ticket_mapping.id')
			                 ->join('ticket', 'ticket_mapping.ticket_id', '=', 'ticket.id')
			                 ->join('option_review', 'planner_review.id', '=', 'option_review.partner_review_id')
			                 ->whereNull('planner_review.deleted_at')
			                 ->whereNull('ticket_bookings.deleted_at')
			                 ->whereNull('ticket_mapping.deleted_at');

			// filters
			$isShowReset = false;

			// name, reviewID, mobile review filter
			$searchQuery = isset($request['query']) && $request['query'] ? $request['query'] : false;
			if ($searchQuery)
			{
				$isShowReset = true;
				$reviews->where(function ($innerQuery) use ($searchQuery) {
					$innerQuery->where('planner_review.name', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.phone', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('ticket.email', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('planner_review.id', 'LIKE', '%' . $searchQuery . '%')
					           ->orWhere('planner_review.review', 'LIKE', '%' . $searchQuery . '%');
				});
			}

			// party date filter
			if ($request['start_date'] && $filterPDStart = strtotime($request['start_date']))
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.party_date_time', '>=', $filterPDStart);
			}

			if ($request['end_date'] && $filterPDEnd = (strtotime($request['end_date']) + 24 * 60 * 60 - 1))
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.party_date_time', '<=', $filterPDEnd);
			}

			// rating Filter
			if (isset($request['min_rating']) && $request['min_rating'])
			{
				$isShowReset = true;
				$reviews->where('planner_review.rating', '>=', $request['min_rating']);
			}

			if (isset($request['max_rating']) && $request['max_rating'])
			{
				$isShowReset = true;
				$reviews->where('planner_review.rating', '<=', $request['max_rating']);
			}

			// provider filters
			$partner = explode('-', $request['p_id']);
			$partnerId = isset($partner[0]) ? $partner[0] : null;
			$partnerTypeId = isset($partner[1]) ? $partner[1] : null;
			if ($partnerId != 'all' && $partnerTypeId != 'all' && $partnerId > 0 && $partnerTypeId > 0)
			{
				$isShowReset = true;
				$reviews->where('ticket_bookings.map_type_id', $partnerTypeId)
				        ->where('ticket_bookings.map_id', $partnerId);
			}

			//city filter
			$cityId = isset($request['city']) && $request['city'] ? $request['city'] : 'all';
			if ($cityId != 'all' && ($cityId = (int)$cityId) && $cityId > 0)
			{

				$isShowReset = true;
				$reviews->where('ticket.city_id', $cityId);
			}
			$eventId = isset($request['occasion']) && $request['occasion'] ? $request['occasion'] : 'all';
			if ($eventId != 'all' && ($eventId = (int)$eventId) && $eventId > 0)
			{
				$isShowReset = true;
				$reviews->where('ticket.event_id', $eventId);
			}

			$reviews = $reviews->orderBy('option_review.updated_at', 'DESC')->paginate(15);
			$pageLinks = $reviews->links()->toHtml();

			$optionType = TypeTicket::select("name", "id")->get();

			$optionTypeData = [];
			$requiredOptionTypeIds = [
				config("evibe.ticket.type.packages"),
				config("evibe.ticket.type.cakes"),
				config("evibe.ticket.type.decors"),
				config("evibe.ticket.type.entertainments"),
				config("evibe.ticket.type.villa"),
				config("evibe.ticket.type.food"),
				config("evibe.ticket.type.couple-experiences"),
				config("evibe.ticket.type.priests"),
				config("evibe.ticket.type.tents"),
				config("evibe.ticket.type.add-on")
			];

			if (count($optionType))
			{
				foreach ($optionType as $option)
				{
					if (in_array($option->id, $requiredOptionTypeIds))
					{
						array_push($optionTypeData, [
							'optionTypeId'    => $option->id,
							'optionTypeValue' => $option->name
						]);
					}
				}
			}

			$reviewsData = [];

			if (count($reviews))
			{
				foreach ($reviews as $review)
				{
					$optionType = TypeTicket::find($review->option_type_id)->name;
					$option = null;
					$deliveryImageData = [];
					$allDeliveryImageData = [];

					if ($review->partner_type_id == config('evibe.ticket.type.planners'))
					{
						$partnerData = Planner::find($review->partner_id);
					}
					else
					{
						$partnerData = Venue::find($review->partner_id);
					}

					$allDeliveryImages = DeliveryGallery::select('delivery_gallery.*')
					                                    ->where('delivery_gallery.ticket_booking_id', $review->ticket_booking_id)
					                                    ->whereNull('delivery_gallery.deleted_at')
					                                    ->get();

					if (count($allDeliveryImages))
					{
						foreach ($allDeliveryImages as $deliveryImage)
						{
							$url = config('evibe.hosts.gallery') . "/ticket/$review->ticket_id/$review->ticket_booking_id/partner/$deliveryImage->url";
							$allDeliveryImageData[$deliveryImage->id] = $url;
						}
					}

					$deliveryImages = OptionReviewGallery::select('delivery_gallery.id', 'delivery_gallery.url')
					                                     ->join('delivery_gallery', 'delivery_gallery.id', '=', 'option_review_gallery.delivery_gallery_id')
					                                     ->where('option_review_gallery.option_review_id', $review->option_review_id)
					                                     ->whereNull('option_review_gallery.deleted_at')
					                                     ->whereNull('delivery_gallery.deleted_at')
					                                     ->get();

					if (count($deliveryImages))
					{
						foreach ($deliveryImages as $deliveryImage)
						{
							$url = config('evibe.hosts.gallery') . "/ticket/$review->ticket_id/$review->ticket_booking_id/partner/$deliveryImage->url";
							$deliveryImageData[$deliveryImage->id] = $url;
						}
					}
					switch ($review->option_type_id)
					{
						// packages
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.couple-experiences'):

							$option = Package::select('id', 'name', 'code')
							                 ->where('is_live', 1)
							                 ->where('id', '=', $review->option_id)
							                 ->first();

							break;
						//services
						case config('evibe.ticket.type.entertainments'):

							$option = TypeService::select('id', 'name', 'code')
							                     ->where('type_service.is_live', 1)
							                     ->where('type_service.id', '=', $review->option_id)
							                     ->first();
							break;
						//cakes
						case config('evibe.ticket.type.cakes'):

							$option = Cake::select('id', 'code', 'title AS name', 'provider_id')
							              ->where('cake.is_live', 1)
							              ->where('cake.id', '=', $review->option_id)
							              ->first();
							break;
						// decors
						case config('evibe.ticket.type.decors'):

							$option = Decor::select('id', 'name', 'code', 'provider_id')
							               ->where('decor.is_live', 1)
							               ->where('decor.id', '=', $review->option_id)
							               ->first();
							break;
						case config('evibe.ticket.type.add-on'):

							$option = AddOn::select('id', 'name', 'code')
							               ->where('add_on.is_live', 1)
							               ->where('add_on.id', '=', $review->option_id)
							               ->first();
							break;
					}

					array_push($reviewsData, [
						'customerName'     => $review->name,
						'customerLocation' => $review->location,
						'ticketId'         => $review->ticket_id,
						'partyDate'        => $review->party_date_time ? date('d-M-Y h-i A', $review->party_date_time) : null,
						'reviewId'         => $review->id,
						'rating'           => $review->rating,
						'review'           => $review->review,
						'optionTypeId'     => $review->option_type_id,
						'optionId'         => $review->option_id,
						'optionInfo'       => $optionType . ' - ' . ((isset($option->code) && $option->code) ? $option->code : null) . ' - ' . ((isset($option->name) && $option->name) ? $option->name : null),
						'option'           => (isset($option->code) && $option->code) ? $option->code : null,
						'optionName'       => (isset($option->name) && $option->name) ? $option->name : null,
						'links'            => $deliveryImageData,
						'unSelectedImages' => array_diff_assoc($allDeliveryImageData, $deliveryImageData),
						'partnerName'      => $partnerData->name,
						'partnerPerson'    => $partnerData->person,
						'reviewAt'         => $review->created_at ? date('d-M-Y h-i A', strtotime($review->created_at)) : null,
						'bookingInfo'      => $review->booking_info
					]);
				}
			}

			$res = [
				'reviewsData'    => $reviewsData,
				'optionTypeData' => $optionTypeData,
				'isShowReset'    => $isShowReset,
				'links'          => $pageLinks,
				'cities'         => $cities,
				'occasions'      => $occasions,
				'partners'       => array_unique($partners, SORT_REGULAR),

			];

			return response()->json($res);
		}
		catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

}