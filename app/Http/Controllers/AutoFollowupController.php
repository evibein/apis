<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Models\Cake;
use App\Http\Models\Coupon\Coupon;
use App\Http\Models\Coupon\UserCoupon;
use App\Http\Models\Decor;
use App\Http\Models\Package;
use App\Http\Models\Ticket;
use App\Http\Models\TicketCoupon;
use App\Http\Models\TicketFollowups;
use App\Http\Models\TicketRemindersStack;
use App\Http\Models\TicketUpdate;
use App\Http\Models\Trends;
use App\Http\Models\Types\TypeReminder;
use App\Http\Models\TypeService;
use App\Http\Models\User;
use App\Http\Models\Util\UnSubscribedUsers;
use App\Http\Models\VenueHall;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AutoFollowupController extends BaseController
{
	public function createAutoFollowup($ticketId, $typeReminderId, Request $request)
	{
		// access token
		$this->checkForAccessToken($request);

		$res = $this->createAutoFollowupReminder($ticketId, $typeReminderId);

		return response()->json($res);
	}

	public function createAutoFollowupReminder($ticketId, $typeReminderId)
	{
		// query params and variables validation
		$ticket = Ticket::with('mappings')
		                ->find($ticketId);
		if (!$ticket)
		{
			throw new CustomException('Ticket not found', 404);
		}

		$typeReminder = TypeReminder::find($typeReminderId);
		if (!$typeReminder)
		{
			throw new CustomException('Reminder type does not exist', 404);
		}

		try
		{
			$errorPrefix = '[APIs] - Creating followup - ';

			switch ($typeReminderId)
			{
				case config('evibe.type_reminder.followup'):

					// check whether reminder exists for that ticket or not
					$ticketReminder = TicketRemindersStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
					                                      ->where('ticket_reminders_stack.ticket_id', $ticketId)
					                                      ->where('type_reminder_group.type_reminder_id', $typeReminderId)
					                                      ->whereNull('type_reminder_group.deleted_at')
					                                      ->whereNull('ticket_reminders_stack.terminated_at')
					                                      ->whereNull('ticket_reminders_stack.invalid_at')
					                                      ->whereNull('ticket_reminders_stack.deleted_at')
					                                      ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
					                                      ->first();

					// followup ticket validation
					$validation = $this->validateFollowup($ticket);
					if (!$validation['success'])
					{
						$error = isset($validation['error']) && $validation['error'] ? $validation['error'] : "Some error occurred while doing validation";
						$ticketUpdateData = [
							'ticket_id'   => $ticketId,
							'status_id'   => $ticket->status_id,
							'type_update' => config('evibe.ticket.type_update.auto'),
							'status_at'   => time(),
							'created_at'  => date('Y-m-d H:i:s'),
							'updated_at'  => date('Y-m-d H:i:s')
						];

						if ($ticketReminder)
						{
							$res = $this->invalidateReminder($ticketReminder->id, $error);

							if ($res['success'])
							{
								$ticketUpdateData['comments'] = 'Auto followup has been invalidated: ' . $error;
								TicketUpdate::create($ticketUpdateData);
							}

							return $res;
						}
						else
						{
							$ticketUpdateData['comments'] = 'Unable to create auto followup: ' . $error;
							TicketUpdate::create($ticketUpdateData);
							// cannot store as there is no reminder
							// cannot invalidate also
							return $res = [
								'success' => false,
								'error'   => $errorPrefix . $error
							];
						}
					}

					// followup calculation
					$response = $this->calculateFollowup($ticket, $typeReminderId);
					if (!$response['success'])
					{
						$error = isset($response['error']) && $response['error'] ? $response['error'] : "Some error occurred while calculating followup";
						$ticketUpdateData = [
							'ticket_id'   => $ticketId,
							'status_id'   => $ticket->status_id,
							'type_update' => config('evibe.ticket.type_update.auto'),
							'status_at'   => time(),
							'created_at'  => date('Y-m-d H:i:s'),
							'updated_at'  => date('Y-m-d H:i:s')
						];

						if ($ticketReminder)
						{
							$res = $this->invalidateReminder($ticketReminder->id, $error);

							if ($res['success'])
							{
								$ticketUpdateData['comments'] = 'Auto followup has been invalidated: ' . $error;
								TicketUpdate::create($ticketUpdateData);
							}

							return $res;
						}
						else
						{
							$ticketUpdateData['comments'] = 'Unable to create auto followup: ' . $error;
							TicketUpdate::create($ticketUpdateData);
							// cannot store as there is no reminder
							// cannot invalidate also
							return $res = [
								'success' => false,
								'error'   => $errorPrefix . $error
							];
						}
					}

					// creation or update of followup
					if (isset($response['reminderData']) && $response['reminderData'])
					{
						$reminderData = $response['reminderData'];
						$reminderData['ticket_id'] = $ticketId;

						if ($ticketReminder)
						{
							$res = $this->updateReminder($reminderData, $ticketReminder->id);
						}
						else
						{
							$res = $this->stackReminder($reminderData);
						}

						return $res;
					}
					else
					{
						return [
							'success' => false,
							'error'   => $errorPrefix . 'Unable to fetch reminder data for ticket: ' . $ticketId
						];
					}
					break;

				case config('evibe.type_reminder.retention'):
					// @see: ignoring multiple tickets with different occasions by a single user if inside time period
					$user = User::withTrashed()->where('username', $ticket->email)->first();
					if ($user && $user->deleted_at)
					{
						// If user exists and is deleted, then do not create user
						Log::error("User for the ticket with id: " . $ticketId . " has already been deleted from user table");

						return $res = [
							'success' => false,
							'error'   => $errorPrefix . 'User has already been deleted from user table. Ticket Id: ' . $ticketId
						];
					}

					if (!$user && $ticket->email)
					{
						// either user doesn't exist with that mobile number
						// or there is no phone for that ticket (most unlikely to happen)
						// todo: what if ticket has user_id? [most likely it won't happen]
						$user = User::create([
							                     'username'   => $ticket->email,
							                     'password'   => $ticket->phone ? Hash::make($ticket->phone) : Hash::make($ticket->email),
							                     'name'       => $ticket->name,
							                     'phone'      => $ticket->phone,
							                     'role_id'    => config('evibe.role.customer'),
							                     'created_at' => Carbon::now(),
							                     'updated_at' => Carbon::now()
						                     ]);
					}

					if (!$user)
					{
						Log::error("Unable to find/create user for the ticketId: " . $ticketId);

						// todo: inform team that email id is missing
						// most likely it won't happen as every booked ticket will have email
						return $res = [
							'success' => false,
							'error'   => $errorPrefix . 'Unable to find/create user for the ticketId: ' . $ticketId
						];
					}

					// validate
					// ticket should be in booked state and payment should have been made
					if (!(($ticket->status_id == config('evibe.ticket.status.booked')) && $ticket->paid_at))
					{
						Log::error("Ticket is either not in booked state or payment has not been done to this ticket: " . $ticketId);

						return $res = [
							'success' => false,
							'error'   => $errorPrefix . 'Ticket is either not in booked state or payment has not been done to this ticket'
						];
					}

					// get all retention reminders for that user
					// considering only one reminder because, if it is terminated (un-subscribed), we shouldn't create reminder
					$ticketReminder = TicketRemindersStack::select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
					                                      ->join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
					                                      ->where('ticket_reminders_stack.user_id', $user->id)
					                                      ->where('type_reminder_group.type_reminder_id', $typeReminderId)
					                                      ->whereNull('type_reminder_group.deleted_at')
					                                      ->whereNull('ticket_reminders_stack.invalid_at')
					                                      ->whereNull('ticket_reminders_stack.deleted_at')
					                                      ->first();

					if ($ticketReminder)
					{
						if ($ticketReminder->terminated_at)
						{
							return $res = [
								'success' => false,
								'error'   => $errorPrefix . 'Customer has already un-subscribed to our retention reminder service'
							];
						}
						else
						{
							return $res = [
								'success' => false,
								'error'   => $errorPrefix . 'An active retention reminder is already in progress'
							];
						}
					}

					// check if any ticket has been raised in last 30 days
					// @see: some initiated tickets might not have email ids yet
					$userEmail = $user->username;
					$userPhone = $user->phone;
					$raisedTickets = Ticket::whereNull('deleted_at')
					                       ->where('created_at', '>=', Carbon::createFromTimestamp(time())->startOfDay()->subDays(30)->toDateTimeString())
					                       ->where(function ($query) use ($userEmail, $userPhone) {
						                       $query->where('email', $userEmail)
						                             ->orWhere('phone', $userPhone);
					                       })
					                       ->get();

					if (count($raisedTickets))
					{
						return $res = [
							'success' => false,
							'error'   => $errorPrefix . 'An enquiry has already been raised for the user.'
						];
					}

					$response = $this->calculateFollowup($ticket, $typeReminderId, $user);
					if (!$response['success'])
					{
						$error = isset($response['error']) && $response['error'] ? $response['error'] : "Some error occurred while calculating followup";
						$ticketUpdateData = [
							'ticket_id'   => $ticketId,
							'status_id'   => $ticket->status_id,
							'type_update' => config('evibe.ticket.type_update.auto'),
							'status_at'   => time(),
							'created_at'  => date('Y-m-d H:i:s'),
							'updated_at'  => date('Y-m-d H:i:s')
						];

						$ticketUpdateData['comments'] = 'Unable to create auto followup: ' . $error;
						TicketUpdate::create($ticketUpdateData);
						// cannot store as there is no reminder
						// cannot invalidate also
						return $res = [
							'success' => false,
							'error'   => $errorPrefix . $error
						];

					}

					// creation of followup
					if (isset($response['reminderData']) && $response['reminderData'])
					{
						$reminderData = $response['reminderData'];
						$reminderData['ticket_id'] = $ticketId;

						if (isset($reminderData['next_reminder_at']) && $reminderData['next_reminder_at'])
						{
							// create reminder
							$res = $this->stackReminder($reminderData);

							return $res;
						}
						else
						{
							// no need to create reminder as there are no buffer days
							return $res = [
								'success' => false,
								'error'   => $errorPrefix . 'Unable to create reminder as there are no buffer days [Ticket Id: ' . $ticket->id . ']'
							];
						}
					}
					else
					{
						return [
							'success' => false,
							'error'   => $errorPrefix . 'Unable to fetch reminder data for ticket: ' . $ticketId
						];
					}

					break;
			}
		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function stopAutoFollowup($ticketId, $typeReminderId, Request $request)
	{
		// access token
		$this->checkForAccessToken($request);

		// query params and variables validation
		$ticket = Ticket::with('mappings')->find($ticketId);
		if (!$ticket)
		{
			throw new CustomException('Ticket not found', 404);
		}

		$typeReminder = TypeReminder::find($typeReminderId);
		if (!$typeReminder)
		{
			throw new CustomException('Reminder type does not exist', 404);
		}

		try
		{
			$errorPrefix = '[APIs] - Stopping followup - ';
			$followupTermination = false;
			$res = ['success' => true];

			// check if ticket reminder is available
			$ticketReminder = TicketRemindersStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
			                                      ->where('ticket_reminders_stack.ticket_id', $ticketId)
			                                      ->where('type_reminder_group.type_reminder_id', $typeReminderId)
			                                      ->whereNull('type_reminder_group.deleted_at')
			                                      ->whereNull('ticket_reminders_stack.terminated_at')
			                                      ->whereNull('ticket_reminders_stack.invalid_at')
			                                      ->whereNull('ticket_reminders_stack.deleted_at')
			                                      ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
			                                      ->first();

			if (!$ticketReminder)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $errorPrefix . 'Unable to find ticket reminder to stop auto followup'
				                        ]);
			}

			if (isset($request['invalidAt']) && $request['invalidAt'])
			{
				$ticketReminder->invalid_at = $request['invalidAt'];
				$ticketReminder->stop_message = $request['stopMessage'];
				$ticketReminder->updated_at = Carbon::now();
				$ticketReminder->save();
				$followupTermination = true;

			}
			elseif (isset($request['terminatedAt']) && $request['terminatedAt'])
			{
				$ticketReminder->terminated_at = $request['terminatedAt'];
				$ticketReminder->stop_message = $request['stopMessage'];
				$ticketReminder->updated_at = Carbon::now();
				$ticketReminder->save();
				$followupTermination = true;

				/*
				 * Moving to master un-subscribed users list:
				 *
				 * For now, the table will be plain with default subscription as retention
				 * In future, we can have 'type_subscription_id', which maps to 'type_reminder.id' where, each reminder type can be thought of as a subscription
				 * Note: 'user' table should have only user related data, not functionality related data
				 */
				$user = User::find($ticketReminder->user_id);
				if (!$user)
				{
					$user = User::select('user.*')
					            ->join('ticket', 'ticket.email', '=', 'user.username')
					            ->where('ticket.id', $ticketReminder->ticket_id)
					            ->whereNull('user.deleted_at')
					            ->first();
				}

				if ($user)
				{
					UnSubscribedUsers::create([
						                          'user_id' => $user->id
					                          ]);
				}
				else
				{
					Log::error("Unable to find user to add in un-subscription list");
				}
			}

			if ($followupTermination && isset($request['stopMessage']) && $request['stopMessage'])
			{
				// do ticket update here itself with appropriate message instead of at every trigger
				TicketUpdate::create([
					                     'ticket_id'   => $ticketId,
					                     'status_id'   => $ticket->status_id,
					                     'type_update' => config('evibe.ticket.type_update.auto'),
					                     'comments'    => $request['stopMessage'] . ' - Auto Followup reminders have been stopped',
					                     'status_at'   => time(),
					                     'created_at'  => date('Y-m-d H:i:s'),
					                     'updated_at'  => date('Y-m-d H:i:s')
				                     ]);
			}

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function recalculateAutoFollowup($ticketId, $typeReminderId, Request $request)
	{
		// access token
		$this->checkForAccessToken($request);

		// query params and variables validation
		$ticket = Ticket::with('mappings')->find($ticketId);
		if (!$ticket)
		{
			throw new CustomException('Ticket not found', 404);
		}

		$typeReminder = TypeReminder::find($typeReminderId);
		if (!$typeReminder)
		{
			throw new CustomException('Reminder type does not exist', 404);
		}

		try
		{
			$errorPrefix = '[APIs] - Updating followup - ';

			// check whether reminder exists for that ticket or not
			// for one reminder type, only one ticket should exist
			$ticketReminder = TicketRemindersStack::join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
			                                      ->where('ticket_reminders_stack.ticket_id', $ticketId)
			                                      ->where('type_reminder_group.type_reminder_id', $typeReminderId)
			                                      ->whereNull('type_reminder_group.deleted_at')
			                                      ->whereNull('ticket_reminders_stack.terminated_at')
			                                      ->whereNull('ticket_reminders_stack.invalid_at')
			                                      ->whereNull('ticket_reminders_stack.deleted_at')
			                                      ->select('ticket_reminders_stack.*', 'type_reminder_group.type_reminder_id')
			                                      ->first();

			if (!$ticketReminder)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $errorPrefix . 'The ticket does not have any live auto followup'
				                        ]);
			}

			switch ($typeReminderId)
			{
				case config('evibe.type_reminder.followup'):

					// followup ticket validation
					$validation = $this->validateFollowup($ticket);
					if (!$validation['success'])
					{
						$error = isset($validation['error']) && $validation['error'] ? $validation['error'] : "Some error occurred while doing validation";
						$res = $this->invalidateReminder($ticketReminder->id, $error);

						if ($res['success'])
						{
							TicketUpdate::create([
								                     'ticket_id'   => $ticketId,
								                     'status_id'   => $ticket->status_id,
								                     'type_update' => config('evibe.ticket.type_update.auto'),
								                     'comments'    => 'Auto followup has been invalidated: ' . $error,
								                     'status_at'   => time(),
								                     'created_at'  => date('Y-m-d H:i:s'),
								                     'updated_at'  => date('Y-m-d H:i:s')
							                     ]);
						}

						return response()->json($res);
					}

					// followup calculation
					$response = $this->calculateFollowup($ticket, $typeReminderId);
					if (!$response['success'])
					{
						$error = isset($response['error']) && $response['error'] ? $response['error'] : "Some error occurred while calculating followup";
						$res = $this->invalidateReminder($ticketReminder->id, $error);

						if ($res['success'])
						{
							TicketUpdate::create([
								                     'ticket_id'   => $ticketId,
								                     'status_id'   => $ticket->status_id,
								                     'type_update' => config('evibe.ticket.type_update.auto'),
								                     'comments'    => 'Auto followup has been invalidated: ' . $error,
								                     'status_at'   => time(),
								                     'created_at'  => date('Y-m-d H:i:s'),
								                     'updated_at'  => date('Y-m-d H:i:s')
							                     ]);
						}

						return response()->json($res);
					}

					// update of followup
					if (isset($response['reminderData']) && $response['reminderData'])
					{
						$reminderData = $response['reminderData'];
						$reminderData['ticket_id'] = $ticketId;

						$res = $this->updateReminder($reminderData, $ticketReminder->id);

						return response()->json($res);
					}
					else
					{
						return response()->json([
							                        'success' => false,
							                        'error'   => $errorPrefix . 'Unable to fetch reminder data for ticket: ' . $ticketId
						                        ]);
					}

					break;
				case config('evibe.type_reminder.retention'):

					$user = User::where('username', $ticket->email)->whereNull('deleted_at')->first();
					if (!$user)
					{
						return response()->json([
							                        'success' => 'false',
							                        'error'   => $errorPrefix . 'Ticket does not have valid user to re calculate auto followup'
						                        ]);
					}
					// check if any ticket has been raised in last 30 days
					// @see: some initiated tickets might not have email ids yet
					$userEmail = $user->username;
					$userPhone = $user->phone;
					$raisedTickets = Ticket::whereNull('deleted_at')
					                       ->where('created_at', '>=', Carbon::createFromTimestamp(time())->startOfDay()->subDays(30)->toDateTimeString())
					                       ->where(function ($query) use ($userEmail, $userPhone) {
						                       $query->where('email', $userEmail)
						                             ->orWhere('phone', $userPhone);
					                       })
					                       ->get();

					if (count($raisedTickets))
					{
						return response()->json([
							                        'success' => false,
							                        'error'   => $errorPrefix . 'An enquiry has already been raised for the user.'
						                        ]);
					}

					// followup calculation
					$response = $this->calculateFollowup($ticket, $typeReminderId, $user);
					if (!$response['success'])
					{
						$error = isset($response['error']) && $response['error'] ? $response['error'] : "Some error occurred while calculating followup";
						$res = $this->invalidateReminder($ticketReminder->id, $error);

						if ($res['success'])
						{
							TicketUpdate::create([
								                     'ticket_id'   => $ticketId,
								                     'status_id'   => $ticket->status_id,
								                     'type_update' => config('evibe.ticket.type_update.auto'),
								                     'comments'    => 'Auto followup has been invalidated: ' . $error,
								                     'status_at'   => time(),
								                     'created_at'  => date('Y-m-d H:i:s'),
								                     'updated_at'  => date('Y-m-d H:i:s')
							                     ]);
						}

						return response()->json($res);
					}

					// update of followup
					if (isset($response['reminderData']) && $response['reminderData'])
					{
						$reminderData = $response['reminderData'];
						$reminderData['ticket_id'] = $ticketId;

						if (isset($reminderData['next_reminder_at']) && $reminderData['next_reminder_at'])
						{
							$res = $this->updateReminder($reminderData, $ticketReminder->id);

							return response()->json($res);
						}
						else
						{
							$error = "There are no buffer days to send next reminder";
							$res = $this->invalidateReminder($ticketReminder->id, $error);

							if ($res['success'])
							{
								TicketUpdate::create([
									                     'ticket_id'   => $ticketId,
									                     'status_id'   => $ticket->status_id,
									                     'type_update' => config('evibe.ticket.type_update.auto'),
									                     'comments'    => 'Auto followup has been invalidated: ' . $error,
									                     'status_at'   => time(),
									                     'created_at'  => date('Y-m-d H:i:s'),
									                     'updated_at'  => date('Y-m-d H:i:s')
								                     ]);
							}

							return response()->json($res);
						}

					}
					else
					{
						return response()->json([
							                        'success' => false,
							                        'error'   => $errorPrefix . 'Unable to fetch reminder data for ticket: ' . $ticketId
						                        ]);
					}
					break;

			}

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function testFollowupReminderCases(Request $request)
	{
		$this->checkForAccessToken($request);
		$T = $request['T'];
		$P = $request['P'];
		$followupDateTime = $request['followupDateTime'];
		$switchDateTime = $request['switchDateTime'];
		$T = $this->calculateTWithFollowupDateTime($T, $followupDateTime);

		// calculate present day count from 'T'
		// in testing, day count can be zero
		$currentDayDifference = strtotime('today midnight') - strtotime(date("Y-m-d", $T));
		$dayCount = (($currentDayDifference) / (24 * 60 * 60));

		$timeLineInfo = [
			'diffInDays'          => $this->calculateDiffInDays($T, $P),
			'dayCount'            => $dayCount,
			'T'                   => $T,
			'P'                   => $P,
			'firstRemindersCount' => config('evibe.reminders.followup_forward'),
			'lastRemindersCount'  => config('evibe.reminders.followup_backward'),
			'recommendationType'  => config('evibe.type_reminder_group.followup.type.case_not_viewed')
		];

		$dayCount = 0;
		$reminderDataArray = [];

		while ($dayCount < $timeLineInfo['diffInDays'])
		{
			$timeLineInfo['dayCount'] = $dayCount;
			if ($switchDateTime && ($switchDateTime >= $T) && ($switchDateTime < ($T + (($dayCount + 1) * 24 * 60 * 60))))
			{
				$timeLineInfo['recommendationType'] = config('evibe.type_reminder_group.followup.type.case_viewed');
			}
			$reminderData = $this->fetchTimeLineData($timeLineInfo);
			if (isset($reminderData['typeReminderGroupId']) && $reminderData['typeReminderGroupId'])
			{
				array_push($reminderDataArray, $reminderData);
			}
			$dayCount++;
		}

		return response()->json([
			                        'success'      => true,
			                        'reminderData' => $reminderDataArray
		                        ]);
	}

	public function createRetentionReminders(Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			// get all tickets which has party date as today date from 2014
			// todo: 16 days from now
			$time = time() + (16 * 24 * 60 * 60);
			$startYear = 2014;
			$year = date('Y', time());
			$diffYears = $year - $startYear;

			$timeRanges = [];
			while ($diffYears != 0)
			{
				array_push($timeRanges, [
					'startTime' => Carbon::createFromTimestamp($time)->startOfDay()->subYears($diffYears)->toDateTimeString(),
					'endTime'   => Carbon::createFromTimestamp($time)->endOfDay()->subYears($diffYears)->toDateTimeString(),
				]);

				$diffYears--;
			}

			if (count($timeRanges))
			{
				$tickets = Ticket::whereNull('deleted_at');

				foreach ($timeRanges as $key => $timeRange)
				{
					$startTime = $timeRange['startTime'];
					$endTime = $timeRange['endTime'];

					if ($key == 0)
					{
						$tickets->where(function ($query1) use ($startTime, $endTime) {
							$query1->where('event_date', '>=', strtotime($startTime))
							       ->where('event_date', '<=', strtotime($endTime));
						});
					}
					else
					{
						$tickets->orWhere(function ($query2) use ($startTime, $endTime) {
							$query2->where('event_date', '>=', strtotime($startTime))
							       ->where('event_date', '<=', strtotime($endTime));
						});
					}
				}

				// @see: if kept outside, 'event_date' will be ignored and all booked tickets will appear

				// group tickets by user (mobile number)
				// if needed, can be done here. Anyway, it will be validated for sure before creating reminder
				//$tickets->groupBy('phone');
				$tickets = $tickets->get();

				// create reminder
				if (count($tickets))
				{
					foreach ($tickets as $ticket)
					{
						// trigger to create retention reminder
						// for a ticket, retention reminder may or may not be created [No error notification]
						$res = $this->createAutoFollowupReminder($ticket->id, config('evibe.type_reminder.retention'));
					}
				}
				else
				{
					// no booked tickets are found for this date
					return response()->json([
						                        'success' => false,
						                        'error'   => 'There are no tickets in the time range to create retention reminders.'
					                        ]);
				}

				return response()->json(['success' => true]);
			}
			else
			{
				// There are no booked tickets in the past for this date
				return response()->json([
					                        'success' => false,
					                        'error'   => 'There are no tickets in the time range to create retention reminders.'
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	private function validateFollowup($ticket)
	{
		$res = [
			'success' => true
		];
		$mappings = $ticket->mappings;

		// status check
		$progressStatus = [
			config('evibe.ticket.status.followup'),
			config('evibe.ticket.status.no_response')
		];
		if (!$ticket->status_id || !in_array($ticket->status_id, $progressStatus))
		{
			return $res = [
				'success' => false,
				'error'   => 'Ticket should be in the follow-up or no-response state to send recommendation follow ups.'
			];
		}

		// party date check
		elseif (!$ticket->event_date)
		{
			return $res = [
				'success' => false,
				'error'   => 'Party date time should not be empty for a ticket to send follow up notifications.'
			];
		}

		elseif ($ticket->event_date <= time())
		{
			return $res = [
				'success' => false,
				'error'   => 'Party date time should be greater than current time.'
			];
		}

		// mappings check
		elseif ((!$mappings) || ($mappings && !$mappings->max('recommended_at')))
		{
			return $res = [
				'success' => false,
				'error'   => 'There are no active recommended mappings to send follow ups.'
			];
		}

		// did not like recommendations
		elseif (!is_null($ticket->last_reco_dislike_at) && ($ticket->last_reco_dislike_at >= $mappings->max('recommended_at')))
		{
			return $res = [
				'success' => false,
				'error'   => 'Cannot send follow ups, as customer responded [Did not like recommendations].'
			];
		}

		// shortlisted some recommendations
		elseif (!is_null($mappings->max('selected_at')) && ($mappings->max('selected_at') >= $mappings->max('recommended_at')))
		{
			return $res = [
				'success' => false,
				'error'   => 'Cannot send follow ups, as customer responded [Shortlisted some recommendations].'
			];
		}

		// customer rating for our recommendations is < 3
		elseif (($ticket->last_reco_rated_at >= $mappings->max('recommended_at')) && $ticket->last_reco_rating < 3)
		{
			return $res = [
				'success' => false,
				'error'   => 'Cannot send follow ups, as customer rated < 3 for our recommendations.'
			];
		}

		return $res;
	}

	private function calculateFollowup($ticket, $typeReminderId, $user = null)
	{
		$reminderData = [];
		switch ($typeReminderId)
		{
			case config('evibe.type_reminder.followup'):
				// @todo: start form here
				$mappings = $ticket->mappings;
				$recommendedAt = $mappings->max('recommended_at'); // timestamp
				$lastSeenAt = $mappings->max('last_seen_at'); // timestamp
				$recommendationType = config('evibe.type_reminder_group.followup.type.case_not_viewed'); // case 1

				if ($lastSeenAt && $lastSeenAt > $recommendedAt)
				{
					$recommendationType = config('evibe.type_reminder_group.followup.type.case_viewed'); // case 2
				}

				// T = recommended at time string
				// P = party date time string
				// T-P is used to calculate the slot that can be used to identify the story line that needs to be sent ot the customer.

				$T = strtotime($recommendedAt);
				$P = $ticket->event_date; // already in epoch unique time string

				// we use ticket followup time as optimal time to send reminder
				$followup = TicketFollowups::where('ticket_id', $ticket->id)
				                           ->whereNull('is_complete')
				                           ->whereNull('followup_type')
				                           ->whereNull('deleted_at')
				                           ->first();

				$followupDateTime = ($followup && $followup->followup_date) ? $followup->followup_date : null;
				$T = $this->calculateTWithFollowupDateTime($T, $followupDateTime);

				$diffInDays = $this->calculateDiffInDays($T, $P);

				$firstRemindersCount = config('evibe.reminders.followup_forward'); // 'T+' cases, cannot be more that '7'
				$lastRemindersCount = config('evibe.reminders.followup_backward'); // 'P-' cases, cannot be more than '7'

				// calculate present day count from 'T'
				$dayCount = $this->calculateDayCount($T);

				$reminderData['start_time'] = strtotime($recommendedAt);
				$reminderData['end_time'] = $P;

				$timeLineData = [];
				$timeLineInfo = [
					'diffInDays'          => $diffInDays,
					'dayCount'            => $dayCount,
					'T'                   => $T,
					'P'                   => $P,
					'firstRemindersCount' => $firstRemindersCount,
					'lastRemindersCount'  => $lastRemindersCount,
					'recommendationType'  => $recommendationType
				];

				if ($diffInDays <= 0)
				{
					return $res = [
						'success' => false,
						'error'   => 'There are no buffer days to send follow-up notifications.'
					];
				}
				else
				{
					$timeLineData = $this->fetchTimeLineData($timeLineInfo);
				}

				if (!$timeLineData)
				{
					return $res = [
						'success' => false,
						'error'   => 'Unable to fetch time line data for ticket: ' . $ticket->id
					];
				}

				if ($diffInDays == $dayCount)
				{
					return $res = [
						'success' => false,
						'error'   => 'Stack of followup reminders that needs to be sent for the ticket: ' . $ticket->id . ' are completed.'
					];
				}

				// @see: for now, coupon code is being used in only rr7 notification
				if (isset($timeLineData['typeReminderGroupId']) && $timeLineData['typeReminderGroupId'] == config('evibe.type_reminder_group.followup.rr7'))
				{
					// calculate coupon code and store
					$couponCreation = $this->calculateAndSaveCouponCode($ticket->id, $mappings, $T, $P);
					if (isset($couponCreation['success']) && $couponCreation['success'] == false && isset($couponCreation['error']))
					{
						$this->sendAndSaveNonExceptionReport([
							                                     'code'      => 0,
							                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
							                                     'method'    => \Illuminate\Support\Facades\Request::method(),
							                                     'message'   => '[AutoFollowupController.php - ' . __LINE__ . '] ' . $couponCreation['error'],
							                                     'exception' => '[AutoFollowupController.php - ' . __LINE__ . '] ' . $couponCreation['error'],
							                                     'trace'     => '[Ticket Id: ' . $ticket->id . ']',
							                                     'details'   => '[Ticket Id: ' . $ticket->id . ']'
						                                     ]);
					}
				}

				$reminderData['next_reminder_at'] = isset($timeLineData['nextReminderAt']) ? $timeLineData['nextReminderAt'] : null;
				$reminderData['type_reminder_group_id'] = isset($timeLineData['typeReminderGroupId']) ? $timeLineData['typeReminderGroupId'] : null;
				$reminderData['count'] = isset($timeLineData['count']) ? $timeLineData['count'] : null;

				return $res = [
					'success'      => true,
					'reminderData' => $reminderData
				];
				break;

			case config('evibe.type_reminder.retention'):
				/*
				 * @see: Conditions:
				 * There should be at-least 1 month gap between party date and coupon expiry date
				 * First Coupon Expiry reminder should be after party date
				 */
				if (!$user)
				{
					return $res = [
						'success' => false,
						'error'   => 'User is required to calculate retention reminder'
					];
				}

				switch ($ticket->event_id)
				{
					case config('evibe.event.special_experience'):
					case config('evibe.event.kids_birthday'):
					case config('evibe.event.naming_ceremony'):
					case config('evibe.event.first_birthday'):
					case config('evibe.event.birthday_2-5'):
					case config('evibe.event.birthday_6-12'):
						$triggerTime = Carbon::createFromTimestamp(time())->toDateTimeString();

						// future party date
						$yearDiff = date('Y', time()) - date('Y', $ticket->event_date);
						$P = Carbon::createFromTimestamp($ticket->event_date)->addYears($yearDiff)->startOfDay()->toDateTimeString();

						// create expiry
						$CE = Carbon::createFromTimestamp($ticket->event_date)->addYears($yearDiff)->addDays(90)->endOfDay()->toDateTimeString();

						// reminders time frame
						/*
						 * R1: P - 15
						 * R2: P - 8
						 * R3: P - 3
						 * R4: CE - 8
						 * R5: CE - 3
						 */

						// reminder time min array
						$remindersTimeArray = [
							[
								'dayStart' => Carbon::createFromTimestamp(strtotime($P))->startOfDay()->subDays(15)->toDateTimeString(),
								'dayEnd'   => Carbon::createFromTimestamp(strtotime($P))->endOfDay()->subDays(15)->toDateTimeString(),
							],
							[
								'dayStart' => Carbon::createFromTimestamp(strtotime($P))->startOfDay()->subDays(8)->toDateTimeString(),
								'dayEnd'   => Carbon::createFromTimestamp(strtotime($P))->endOfDay()->subDays(8)->toDateTimeString(),
							],
							[
								'dayStart' => Carbon::createFromTimestamp(strtotime($P))->startOfDay()->subDays(3)->toDateTimeString(),
								'dayEnd'   => Carbon::createFromTimestamp(strtotime($P))->endOfDay()->subDays(3)->toDateTimeString(),
							]
						];

						// add CE reminders only if time gap is at least 10 days
						if ((strtotime($CE) - strtotime($P)) >= (9 * 24 * 60 * 60))
						{
							array_push($remindersTimeArray, [
								'dayStart' => Carbon::createFromTimestamp(strtotime($CE))->startOfDay()->subDays(8)->toDateTimeString(),
								'dayEnd'   => Carbon::createFromTimestamp(strtotime($CE))->endOfDay()->subDays(8)->toDateTimeString(),
							]);
						}
						if ((strtotime($CE) - strtotime($P)) >= (4 * 24 * 60 * 60))
						{
							array_push($remindersTimeArray, [
								'dayStart' => Carbon::createFromTimestamp(strtotime($CE))->startOfDay()->subDays(3)->toDateTimeString(),
								'dayEnd'   => Carbon::createFromTimestamp(strtotime($CE))->endOfDay()->subDays(3)->toDateTimeString(),
							]);
						}

						$totalRemindersCount = count($remindersTimeArray);

						if (!$totalRemindersCount)
						{
							return $res = [
								'success' => false,
								'error'   => 'Unable to create retention reminder as there is some error with reminder time line'
							];
						}

						$startTime = $remindersTimeArray[0]['dayStart'];
						$endTime = $CE;
						$count = $totalRemindersCount;
						$nextReminderAt = null;
						$couponPrefix = "RTEVB";
						$now = Carbon::createFromTimestamp(time())->toDateTimeString();

						$coupon = Coupon::select('coupon.*')
						                ->join('coupon_users', 'coupon_users.coupon_id', '=', 'coupon.id')
						                ->whereNull('coupon.deleted_at')
						                ->where('coupon_users.user_id', $user->id)
						                ->where('coupon.coupon_code', 'LIKE', $couponPrefix . "%")
						                ->where('coupon.offer_end_time', '>', $now)
						                ->first();
						// consider expiry time while getting coupon
						// increase offer to 300
						if (!$coupon)
						{
							$couponDetails = [
								"couponPrefix"    => $couponPrefix,
								"stringLength"    => 6,
								"userId"          => $user->id,
								"discountPercent" => 30,
								"maxDiscount"     => 300,
								"offerStartTime"  => $startTime,
								"offerEndTime"    => $endTime,
								"description"     => "Retention coupon"

							];
							$couponCode = $this->createCoupon($couponDetails);
							if (is_null($couponCode))
							{
								return $res = [
									'success' => false,
									'error'   => "Unable to create coupon for user: " . $user->id
								];
							}
						}

						// @see: set next reminders at 10 AM
						// assuming that by trigger time, reminder has been sent and calculation is for future reminders only
						foreach ($remindersTimeArray as $key => $reminderTime)
						{
							if ($triggerTime < $reminderTime['dayStart'])
							{
								$nextReminderAt = Carbon::createFromTimestamp(strtotime($reminderTime['dayStart']))->addHours(12)->addMinutes(30)->toDateTimeString();
								$count = $totalRemindersCount - $key;
								break;
							}
							elseif (($triggerTime >= $reminderTime['dayStart']) && ($triggerTime <= $reminderTime['dayEnd']))
							{
								if (!isset($remindersTimeArray[$key + 1]))
								{
									$nextReminderAt = null;
									$count = 0;
									break;
								}
								else
								{
									$nextReminderAt = Carbon::createFromTimestamp(strtotime($remindersTimeArray[$key + 1]['dayStart']))->addHours(12)->addMinutes(30)->toDateTimeString();
									$count = $totalRemindersCount - $key - 1;
									break;
								}
							}

							if (!(isset($remindersTimeArray[$key + 1])) && ($triggerTime > $reminderTime['dayEnd']))
							{
								$nextReminderAt = null;
								$count = 0;
								break;
							}
						}

						$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt1');
						if ($nextReminderAt)
						{
							switch ($count)
							{
								case 5:
									$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt1');
									break;

								case 4:
									$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt2');
									break;

								case 3:
									$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt3');
									break;

								case 2:
									$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt4');
									break;

								case 1:
									$typeReminderGroupId = config('evibe.type_reminder_group.retention.rt5');
									break;
							}
						}

						// create reminder
						// @see: inside occasion for now
						$reminderData = [
							'start_time'             => strtotime($startTime),
							'end_time'               => strtotime($endTime),
							'ticket_id'              => $ticket->id,
							'user_id'                => $user->id,
							'next_reminder_at'       => strtotime($nextReminderAt),
							'type_reminder_group_id' => $typeReminderGroupId,
							'count'                  => $count
						];

						return $res = [
							'success'      => true,
							'reminderData' => $reminderData
						];
						break;
				}
				break;
		}
	}

	private function calculateTWithFollowupDateTime($T, $followupDateTime)
	{
		if ($followupDateTime)
		{
			$followupDate = strtotime(date("Y-m-d", $followupDateTime));
			$followupTime = $followupDateTime - $followupDate;

			$T = strtotime(date("Y-m-d", $T)) + $followupTime;
		}
		else
		{
			// default time for sending followup notifications is 4 PM
			$T = strtotime(date("Y-m-d", $T)) + (16 * 60 * 60);
		}

		return $T;
	}

	private function calculateDiffInDays($T, $P)
	{
		$difference = strtotime(date("Y-m-d", $P)) - strtotime(date("Y-m-d", $T));

		return $diffInDays = (($difference) / (24 * 60 * 60)) - 1;
	}

	private function calculateDayCount($T)
	{
		// calculate present day count from 'T'
		$currentDayDifference = strtotime('today midnight') - strtotime(date("Y-m-d", $T));
		if ($currentDayDifference < 0)
		{
			// will not happen, but need to handle (by any chance)
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Day count should not be negative for an auto followup reminder'
			                        ]);
		}

		return $dayCount = (($currentDayDifference) / (24 * 60 * 60));
	}

	private function fetchTimeLineData($timeLineInfo)
	{
		// @see: assuming, 'diffInDays' > 0
		// @todo: if no use, merge back
		$diffInDays = isset($timeLineInfo['diffInDays']) ? $timeLineInfo['diffInDays'] : null;
		$dayCount = isset($timeLineInfo['dayCount']) ? $timeLineInfo['dayCount'] : null;
		$T = isset($timeLineInfo['T']) ? $timeLineInfo['T'] : null;
		$P = isset($timeLineInfo['P']) ? $timeLineInfo['P'] : null;
		$firstRemindersCount = isset($timeLineInfo['firstRemindersCount']) ? $timeLineInfo['firstRemindersCount'] : null;
		$lastRemindersCount = isset($timeLineInfo['lastRemindersCount']) ? $timeLineInfo['lastRemindersCount'] : null;

		$timeLineData = [];
		$nextReminderAt = null;

		if ($diffInDays <= $firstRemindersCount)
		{
			$nextReminderAt = strtotime(date("Y-m-d H:i", $T)) + (($dayCount + 1) * (24 * 60 * 60));
			$timeLineData = $this->forwardCases($timeLineInfo);
		}
		elseif ($diffInDays > $firstRemindersCount && $diffInDays <= ($firstRemindersCount + $lastRemindersCount))
		{
			$nextReminderAt = strtotime(date("Y-m-d H:i", $T)) + (($dayCount + 1) * (24 * 60 * 60));

			if ($dayCount < $diffInDays - $lastRemindersCount)
			{
				$timeLineData = $this->forwardCases($timeLineInfo);
			}
			else
			{
				$timeLineData = $this->backwardCases($timeLineInfo);
			}
		}
		elseif ($diffInDays > ($firstRemindersCount + $lastRemindersCount))
		{
			$nextReminderAt = strtotime(date("Y-m-d H:i", $T)) + (($dayCount + 1) * (24 * 60 * 60));
			$noReminderCount = $diffInDays - ($firstRemindersCount + $lastRemindersCount);

			if ($dayCount < $firstRemindersCount)
			{
				$timeLineData = $this->forwardCases($timeLineInfo);
			}
			elseif (($dayCount >= ($diffInDays - $lastRemindersCount)) && ($dayCount < $diffInDays))
			{
				$timeLineData = $this->backwardCases($timeLineInfo);

				$timeLineData['count'] = (isset($timeLineData['count']) && ($timeLineData['count'] > ($lastRemindersCount))) ? ($timeLineData['count'] - $noReminderCount) : $timeLineData['count'];
			}
			else
			{
				$timeLineData = $this->backwardCases($timeLineInfo);
				$nextReminderAt = strtotime(date("Y-m-d H:i", $T)) + (($diffInDays - $lastRemindersCount + 1) * (24 * 60 * 60));
				$timeLineData['count'] = $lastRemindersCount;
			}
		}
		else
		{
			return false;
		}

		if (!$nextReminderAt || !$lastRemindersCount)
		{
			return false;
		}

		$timeLineData['nextReminderAt'] = $nextReminderAt;

		return $timeLineData;
	}

	private function forwardCases($data)
	{
		// based on 'recommendationType', only final 'type_reminder_group_id' will change
		$diffInDays = isset($data['diffInDays']) ? $data['diffInDays'] : null;
		$dayCount = isset($data['dayCount']) ? $data['dayCount'] : null;
		$T = isset($data['T']) ? $data['T'] : null;
		$recoType = isset($data['recommendationType']) ? $data['recommendationType'] : null;
		$reminderData = [];
		$daysToParty = $diffInDays - $dayCount;

		switch ($dayCount + 1)
		{
			case 1: // T + 1
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr2');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr8');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr5');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr6');
					}
				}
				break;

			case 2: // T + 2
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr3');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr5');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr6');
					}
				}
				break;

			case 3: // T + 3
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr3');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr8');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
					}
				}
				break;

			case 4: // T + 4
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr8');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr9');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
					}
				}
				break;

			case 5: // T + 5
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr9');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr6');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr8');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
					}
				}
				break;

			case 6: // T + 6
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr10');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr10');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
					}
				}
				break;

			case 7:
				// T + 7
				// followup.type.case_not_viewed
				if ($daysToParty > 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr11');
				}
				elseif ($daysToParty == 2)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
				}
				elseif ($daysToParty == 1)
				{
					$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				}

				if ($recoType == config('evibe.type_reminder_group.followup.type.case_viewed'))
				{
					if ($daysToParty > 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr11');
					}
					elseif ($daysToParty == 2)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr4');
					}
					elseif ($daysToParty == 1)
					{
						$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
					}
				}
				break;
		}

		$reminderData['count'] = $daysToParty;

		return $reminderData;
	}

	private function backwardCases($data)
	{
		// based on 'recommendationType', only final 'type_reminder_group_id' will change
		$diffInDays = isset($data['diffInDays']) ? $data['diffInDays'] : null;
		$dayCount = isset($data['dayCount']) ? $data['dayCount'] : null;
		$T = isset($data['T']) ? $data['T'] : null;
		$P = isset($data['P']) ? $data['P'] : null;
		$reminderData = [];
		$daysToParty = $diffInDays - $dayCount;

		// @see: reverse order, compared to 'forward cases'
		switch ($daysToParty)
		{
			case 1: // P - 1
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr7');
				break;

			case 2: // P - 2
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr6');
				break;

			case 3: // P - 3
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr11');
				break;

			case 4: // P - 4
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr10');
				break;

			case 5: // P - 5
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr9');
				break;

			case 6: // P - 6
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr8');
				break;

			case 7: // P - 7
			default:
				$reminderData['typeReminderGroupId'] = config('evibe.type_reminder_group.followup.rr6');
				break;
		}

		$reminderData['count'] = $daysToParty;

		return $reminderData;
	}

	private function calculateAndSaveCouponCode($ticketId, $mappings, $T, $P)
	{
		try
		{

			if (!$mappings)
			{
				return $res = [
					'success' => false,
					'error'   => "Unable to calculate coupon code because of lack of mappings"
				];
			}

			$priceStack = [];

			foreach ($mappings as $mapping)
			{
				if ($mapping->map_type_id && $mapping->map_id)
				{
					switch ($mapping->map_type_id)
					{
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.resort'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.lounge'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.couple-experiences'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.generic-package'):
							$option = Package::find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->price);
							}
							break;

						case config('evibe.ticket.type.cakes'):
							$option = Cake::find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->price);
							}
							break;

						case config('evibe.ticket.type.decors'):
							$option = Decor::find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->min_price);
							}
							break;

						case config('evibe.ticket.type.services'):
						case config('evibe.ticket.type.entertainments'):
							$option = TypeService::find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->min_price);
							}
							break;

						case config('evibe.ticket.type.trends'):
							$option = Trends::find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->price);
							}
							break;

						case config('evibe.ticket.type.venues'):
							$option = VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
							                   ->select(DB::raw('venue.price_min_veg AS venue_price_min_veg'))
							                   ->find($mapping->map_id);
							if ($option)
							{
								array_push($priceStack, $option->venue_price_min_veg);
							}
							break;
					}
				}
			}

			if (count($priceStack))
			{
				$minPrice = min($priceStack);
				$discount = $minPrice * 5 / 100;

				if ($discount < 50)
				{
					$discount = 50;
				}
				elseif ($discount > 300)
				{
					$discount = 300;
				}
				else
				{
					// round
					$change = $discount / 5;
					$discount = (int)$change * 5;
				}

				$validStart = strtotime(date("Y-m-d", $T + (24 * 60 * 60)));
				$validEnd = strtotime(date("Y-m-d", $P)) - 1;

				$validityDateCode = date("d", $validEnd);

				$couponCode = 'E' . $discount . 'V' . $validityDateCode;

				$oldCoupon = TicketCoupon::where('ticket_id', $ticketId)->whereNull('deleted_at')->whereNull('used_at')->first();
				if ($oldCoupon)
				{
					$oldCoupon->update([
						                   'code'                => $couponCode,
						                   'amount'              => $discount,
						                   'validity_start_time' => $validStart,
						                   'validity_end_time'   => $validEnd,
						                   'updated_at'          => Carbon::now()
					                   ]);

					return $res = ['success' => true];
				}
				else
				{
					$ticketCoupon = TicketCoupon::create([
						                                     'code'                => $couponCode,
						                                     'amount'              => $discount,
						                                     'ticket_id'           => $ticketId,
						                                     'validity_start_time' => $validStart,
						                                     'validity_end_time'   => $validEnd,
						                                     'created_at'          => Carbon::now(),
						                                     'updated_at'          => Carbon::now()
					                                     ]);

					if ($ticketCoupon)
					{
						return $res = ['success' => true];
					}
					else
					{
						return $res = [
							'success' => false,
							'error'   => "Some error occurred while creating coupon code for the ticket: $ticketId"
						];
					}
				}
			}

			return $res = [
				'success' => false,
				'error'   => "There are no prices for the mappings to calculate discounts for the ticket: $ticketId"
			];

		} catch (\Exception $e)
		{
			return $res = [
				'success' => false,
				'error'   => $e->getMessage()
			];
		}
	}

	private function stackReminder($data)
	{
		try
		{
			$response = ['success' => false];

			if (!$data)
			{
				return $response;
			}

			$data['created_at'] = Carbon::now();
			$data['updated_at'] = Carbon::now();
			$ticketReminderStack = TicketRemindersStack::create($data);

			if ($ticketReminderStack)
			{
				$response['success'] = true;
			}

			return $response;

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	private function updateReminder($data, $ticketReminderId)
	{
		try
		{
			$response = ['success' => false];

			if (!$data || !$ticketReminderId)
			{
				return $response;
			}

			$ticketReminder = TicketRemindersStack::where('id', $ticketReminderId)
			                                      ->first()
			                                      ->update([
				                                               'type_reminder_group_id' => isset($data['type_reminder_group_id']) ? $data['type_reminder_group_id'] : config('evibe.type_reminder_group.followup.rr1'),
				                                               'next_reminder_at'       => isset($data['next_reminder_at']) ? $data['next_reminder_at'] : null,
				                                               'count'                  => isset($data['count']) ? $data['count'] : null,
				                                               'start_time'             => isset($data['start_time']) ? $data['start_time'] : null,
				                                               'end_time'               => isset($data['end_time']) ? $data['end_time'] : null,
				                                               'updated_at'             => Carbon::now()
			                                               ]);

			if ($ticketReminder)
			{
				$response['success'] = true;
			}

			return $response;

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	private function invalidateReminder($ticketReminderId, $error)
	{
		$response = ['success' => false];
		if (!$ticketReminderId)
		{
			return $response;
		}

		$ticketReminder = TicketRemindersStack::where('id', $ticketReminderId)
		                                      ->first()
		                                      ->update([
			                                               'invalid_at'   => time(),
			                                               'stop_message' => $error . ' - Auto followup has been invalidated',
			                                               'updated_at'   => Carbon::now()
		                                               ]);
		if ($ticketReminder)
		{
			$response['success'] = true;
		}

		return $response;
	}
}