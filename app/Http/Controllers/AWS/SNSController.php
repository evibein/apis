<?php

namespace App\Http\Controllers\AWS;

use App\Http\Controllers\BaseController;
use App\Http\Models\Planner;
use App\Http\Models\Suppression;
use App\Http\Models\Ticket;
use App\Http\Models\TicketRemindersStack;
use App\Http\Models\User;
use App\Http\Models\Venue;
use App\Jobs\Mail\EmailUpdateAlertToCustomerJob;
use App\Jobs\Mail\InvalidEmailUpdateAlertToTeam;
use App\Jobs\Sms\SMSInvalidEmailToUserJob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\Exception\InvalidSnsMessageException;
use Illuminate\Support\Facades\Validator;

class SNSController extends BaseController
{
	public function subscribeNotifications()
	{
		$message = Message::fromRawPostData();
		$validator = new MessageValidator();

		// Validate the message and log errors if invalid.
		try
		{
			$validator->validate($message);
		} catch (InvalidSnsMessageException $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json(['success' => false]);
		}

		// Check the type of the message and handle the subscription.
		if ($message['Type'] === 'SubscriptionConfirmation' && isset($message['SubscribeURL']))
		{
			// Confirm the subscription by sending a GET request to the SubscribeURL
			file_get_contents($message['SubscribeURL']);
		}

		if ($message['Type'] === 'Notification')
		{
			$messageBody = isset($message['Message']) ? json_decode($message['Message'], true) : [];

			// Handle Bounce
			if (isset($messageBody['notificationType']) && $messageBody['notificationType'] === "Bounce")
			{
				// release 1
				// update ticket status to "Invalid Email"
				// delete email from ticket
				// alert CRM team
				// save in suppression list with all required details
				if (isset($messageBody['bounce']))
				{
					$bounce = $messageBody['bounce'];

					if (isset($bounce['bouncedRecipients']))
					{
						$bouncedRecipients = $bounce['bouncedRecipients'];

						$bouncedEmail = isset($bouncedRecipients[0]['emailAddress']) ? $bouncedRecipients[0]['emailAddress'] : null;
						$subject = isset($messageBody['mail']['commonHeaders']['subject']) ? $messageBody['mail']['commonHeaders']['subject'] : null;

						if ($bouncedEmail)
						{
							$statusArray = [
								config('evibe.ticket.status.initiated'),
								config('evibe.ticket.status.progress'),
								config('evibe.ticket.status.followup'),
							];

							$tickets = Ticket::whereNull('deleted_at')
							                 ->where('email', $bouncedEmail)
							                 ->whereIn('status_id', $statusArray)
							                 ->orderBy('updated_at', 'DESC')
							                 ->get();

							$user = User::where('username', $bouncedEmail)
							            ->whereNull('deleted_at')
							            ->first();

							$userPhone = $user ? $user->phone : null;
							$userId = $user ? $user->id : null;
							$suppressionTicket = false;
							$correctionSMS = false;
							$handlerId = null;
							$retentionReminder = null;

							if (count($tickets))
							{
								// definitely a customer

								foreach ($tickets as $ticket)
								{
									// update status in ticket update
									$ticket->email = null;
									$ticket->updated_at = Carbon::now();
									$ticket->save();

									$this->updateTicketAction([
										                          'ticket'   => $ticket,
										                          //'statusId' => config('evibe.ticket.status.invalid_email'), Not converting the status to invalid email, if the entered email address is wrong
										                          'comments' => 'Invalid email address was detected and has been removed from the ticket.'
									                          ]);
								}

								$suppressionTicket = $tickets->first();

								$userPhone = $suppressionTicket->phone;
								$userId = $suppressionTicket->user_id;
								$handlerId = $suppressionTicket->handler_id;

								$handler = $suppressionTicket->handler;

								$mailData = [
									'roleId'              => config('evibe.role.customer'),
									'customerName'        => $suppressionTicket ? $suppressionTicket->name : null,
									'customerPhone'       => $userPhone,
									'invalidEmailAddress' => $bouncedEmail,
									'handlerName'         => $handler && $handler->name ? $handler->name : 'No current handler',
									'unsentEmailSubject'  => $subject,
									'dashTicketLink'      => $suppressionTicket ? config('evibe.hosts.dash') . '/tickets/' . $suppressionTicket->id : null
								];

								// @todo: link to submit email id via sms
								$correctionSMS = true;
							}
							else
							{
								// may be partner or handler
								$planner = Planner::whereNull('deleted_at')
								                  ->where('email', $bouncedEmail)
								                  ->orWhere('alt_email', $bouncedEmail)
								                  ->first();

								$venue = Venue::whereNull('deleted_at')
								              ->where('email', $bouncedEmail)
								              ->orWhere('alt_email', $bouncedEmail)
								              ->first();

								if ($venue || $planner)
								{
									if ($venue)
									{
										$roleId = config('evibe.role.venue');
										$userId = $venue->user_id ? $venue->user_id : $userId;
										$userPhone = $venue->phone;
										$partnerName = $venue->name;
										$isAltEmail = ($venue->alt_email == $bouncedEmail) ? true : false;
										$dashPartnerLink = config('evibe.hosts.dash') . '/venues/view/' . $venue->id;
									}
									else
									{
										$roleId = config('evibe.role.planner');
										$userId = $planner->user_id ? $planner->user_id : $userId;
										$userPhone = $planner->phone;
										$partnerName = $planner->name;
										$isAltEmail = ($planner->alt_email == $bouncedEmail) ? true : false;
										$dashPartnerLink = config('evibe.hosts.dash') . '/vendors/view/' . $planner->id;
									}

									$mailData = [
										'roleId'              => $roleId,
										'partnerName'         => $partnerName,
										'partnerPhone'        => $userPhone ? $userPhone : ($user ? $user->phone : null),
										'invalidEmailAddress' => $bouncedEmail,
										'isAltEmail'          => $isAltEmail,
										'unsentEmailSubject'  => $subject,
										'dashPartnerLink'     => $dashPartnerLink
									];
								}
								else
								{
									$mailData = [
										'roleId'              => null,
										'invalidEmailAddress' => $bouncedEmail
									];
								}

							}

							// @see: removed by @Anji on 28 Jun 2018
							// $this->dispatch(new InvalidEmailAddressAlertToTeam($mailData));

							$suppression = $this->validateAndCreateSuppression([
								                                                   'bouncedEmail' => $bouncedEmail,
								                                                   'ticketId'     => $suppressionTicket ? $suppressionTicket->id : null,
								                                                   'userId'       => $userId,
								                                                   'userPhone'    => $userPhone,
								                                                   'handlerId'    => $handlerId
							                                                   ]);

							if ($userId)
							{
								// @see: even if userId may be null, there might be reminders in stack in that state
								// @see: no direct DB validation
								$retentionReminder = TicketRemindersStack::select('ticket_reminders_stack.*')
								                                         ->join('type_reminder_group', 'type_reminder_group.id', '=', 'ticket_reminders_stack.type_reminder_group_id')
								                                         ->whereNull('ticket_reminders_stack.deleted_at')
								                                         ->whereNull('ticket_reminders_stack.invalid_at')
								                                         ->whereNull('ticket_reminders_stack.terminated_at')
								                                         ->where('type_reminder_group.type_reminder_id', config('evibe.type_reminder.retention'))
								                                         ->where('ticket_reminders_stack.user_id', $userId)
								                                         ->first();
							}

							// release 2
							// send sms to customer
							// @see: if bounced email is in active retention reminder, do not send 'email correction sms'
							// todo: notify team regarding bounced retention email
							if ($suppression && !$retentionReminder)
							{
								if ($correctionSMS)
								{
									if ($userPhone)
									{
										$emailCorrectionLink = config('evibe.live.host') . '/validate-email/' . $suppression->id;
										$token = Hash::make($suppression->id);

										$emailCorrectionLink .= '?token=' . $token;
										$this->dispatch(new SMSInvalidEmailToUserJob([
											                                             'customerPhone'       => $userPhone,
											                                             'customerName'        => $suppressionTicket->name ? $suppressionTicket->name : ($user ? $user->name : null),
											                                             'incorrectEmail'      => $bouncedEmail,
											                                             'emailCorrectionLink' => $this->getShortUrl($emailCorrectionLink),
										                                             ]));
									}
								}
							}
							else
							{
								if ($correctionSMS)
								{
									$this->sendAndSaveNonExceptionReport([
										                                     'code'       => 0,
										                                     'url'        => 'AWS SNS - correction sms',
										                                     'method'     => 'POST',
										                                     'message'    => $message ? print_r($message, true) : 'Got Empty Message',
										                                     'trace'      => 'AWS SNS error - unable to send correction sms due to lack of suppression',
										                                     'project_id' => config('evibe.project_id'),
										                                     'exception'  => 'AWS SNS error - unable to send correction sms due to lack of suppression',
										                                     'details'    => $message ? print_r($message, true) : 'Got Empty Message'
									                                     ]);
								}
							}
						}
						else
						{
							$this->sendAndSaveNonExceptionReport([
								                                     'code'       => 0,
								                                     'url'        => 'AWS SNS - bounced email',
								                                     'method'     => 'POST',
								                                     'message'    => $message ? print_r($message, true) : 'Got Empty Message',
								                                     'trace'      => 'AWS SNS error fetching bounced email',
								                                     'project_id' => config('evibe.project_id'),
								                                     'exception'  => 'AWS SNS error fetching bounced email',
								                                     'details'    => $message ? print_r($message, true) : 'Got Empty Message'
							                                     ]);
						}
					}
					else
					{
						$this->sendAndSaveNonExceptionReport([
							                                     'code'       => 0,
							                                     'url'        => 'AWS SNS - bounced recipients',
							                                     'method'     => 'POST',
							                                     'message'    => $message ? print_r($message, true) : 'Got Empty Message',
							                                     'trace'      => 'AWS SNS error fetching bounced recipients',
							                                     'project_id' => config('evibe.project_id'),
							                                     'exception'  => 'AWS SNS error fetching bounced recipients',
							                                     'details'    => $message ? print_r($message, true) : 'Got Empty Message'
						                                     ]);
					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'       => 0,
						                                     'url'        => 'AWS SNS - bounced data',
						                                     'method'     => 'POST',
						                                     'message'    => $message ? print_r($message, true) : 'Got Empty Message',
						                                     'trace'      => 'AWS SNS error fetching bounced data',
						                                     'project_id' => config('evibe.project_id'),
						                                     'exception'  => 'AWS SNS error fetching bounced data',
						                                     'details'    => $message ? print_r($message, true) : 'Got Empty Message'
					                                     ]);
				}
			}

			// Handle Complaints
			if (isset($messageBody['notificationType']) && $messageBody['notificationType'] === "Complaint")
			{
				Log::info("received complaint");
			}

		}

		return response()->json(['success' => true]);
	}

	public function updateCustomerEmails(Request $request)
	{
		$correctedEmail = $request['correctedEmail'];
		$suppressionId = $request['suppressionId'];

		$rules = [
			'suppressionId' => 'required',
			'email'         => 'required|email'
		];

		$messages = [
			'email.required'         => 'Kindly enter the correct email',
			'email.email'            => 'Entered email is not in valid format',
			'suppressionId.required' => 'Some error occurred while fetching data. Please try again later'
		];

		$data = ['email' => $correctedEmail, 'suppressionId' => $suppressionId];

		$validator = Validator::make($data, $rules, $messages);

		if ($validator->fails())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => $validator->messages()->first()
			                        ]);
		}

		// @todo: additional email validation

		$suppression = Suppression::find($suppressionId);

		if ($suppression)
		{
			if ($correctedEmail == $suppression->email)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'You have entered the same invalid email. Kindly enter a correct email.'
				                        ]);
			}

			$suppression->is_corrected = 1;
			$suppression->corrected_email = $correctedEmail;
			$suppression->updated_at = Carbon::now();
			$suppression->save();

			$ticket = Ticket::find($suppression->ticket_id);
			if ($ticket)
			{
				$ticket->email = $correctedEmail;
				$ticket->updated_at = Carbon::now();
				$ticket->save();

				$this->updateTicketAction([
					                          'ticket'   => $ticket,
					                          'comments' => 'Correct email id has been updated'
				                          ], false);

				// alert CRM team on email update
				/*
				$this->dispatch(new InvalidEmailUpdateAlertToTeam([
					                                                  'customerName'        => $ticket->name ? $ticket->name : null,
					                                                  'customerPhone'       => $ticket->phone ? $ticket->phone : ($suppression->phone ? $suppression->phone : null),
					                                                  'invalidEmailAddress' => $suppression->email,
					                                                  'correctedEmail'      => $correctedEmail,
					                                                  'dashTicketLink'      => config('evibe.hosts.dash') . '/tickets/' . $ticket->id
				                                                  ]));
				*/

				// send welcome email to customer
				// since this system is only for customers
				$this->dispatch(new EmailUpdateAlertToCustomerJob([
					                                                  'emailId'      => $correctedEmail,
					                                                  'invalidEmail' => $suppression->email,
					                                                  'ticket'       => $ticket
				                                                  ]));
			}
			else
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'       => 0,
					                                     'url'        => 'AWS SNS - ticket update',
					                                     'method'     => 'POST',
					                                     'message'    => '[Suppression Id: ' . $suppressionId . '] Unable to find ticket to update.',
					                                     'trace'      => 'AWS SNS error fetching ticket to update and send alerts and welcome email.',
					                                     'project_id' => config('evibe.project_id'),
					                                     'exception'  => 'AWS SNS error fetching ticket to update and send alerts and welcome email.',
					                                     'details'    => '[Suppression Id: ' . $suppressionId . '] Unable to find ticket to update.'
				                                     ]);
			}

			$user = User::find($suppression->user_id);
			if ($user)
			{
				$user->username = $correctedEmail;
				$user->updated_at = Carbon::now();
				$user->save();
			}

			return response()->json([
				                        'success' => true
			                        ]);
		}
		else
		{
			$this->sendAndSaveNonExceptionReport([
				                                     'code'       => 0,
				                                     'url'        => 'AWS SNS - suppression data',
				                                     'method'     => 'POST',
				                                     'message'    => '[Suppression Id: ' . $suppressionId . '] Unable to find suppression data.',
				                                     'trace'      => 'AWS SNS error fetching suppression data',
				                                     'project_id' => config('evibe.project_id'),
				                                     'exception'  => 'AWS SNS error fetching suppression data',
				                                     'details'    => '[Suppression Id: ' . $suppressionId . '] Unable to find suppression data.'
			                                     ]);

			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error occurred while saving data'
			                        ]);
		}
	}

	protected function validateAndCreateSuppression($suppressionData)
	{
		$bouncedEmail = isset($suppressionData['bouncedEmail']) ? $suppressionData['bouncedEmail'] : null;
		$ticketId = isset($suppressionData['ticketId']) ? $suppressionData['ticketId'] : null;
		$userId = isset($suppressionData['userId']) ? $suppressionData['userId'] : null;
		$userPhone = isset($suppressionData['userPhone']) ? $suppressionData['userPhone'] : null;
		$handlerId = isset($suppressionData['handlerId']) ? $suppressionData['handlerId'] : null;

		if ($ticketId)
		{
			$existingSuppression = Suppression::where('ticket_id', $ticketId)->first();
		}
		else
		{
			$existingSuppression = Suppression::where('email', $bouncedEmail)->where('is_corrected', 0)->first();
		}

		if ($existingSuppression)
		{
			$existingSuppression->email = $bouncedEmail;
			$existingSuppression->is_corrected = 0;
			$existingSuppression->corrected_email = null;
			$existingSuppression->updated_at = Carbon::now();
			$existingSuppression->save();

			return $existingSuppression;
		}
		else
		{
			$suppression = Suppression::create([
				                                   'email'      => $bouncedEmail,
				                                   'ticket_id'  => $ticketId,
				                                   'handler_id' => $handlerId,
				                                   'user_id'    => $userId,
				                                   'phone'      => $userPhone,
				                                   'created_at' => Carbon::now(),
				                                   'updated_at' => Carbon::now()
			                                   ]);
			if ($suppression)
			{
				return $suppression;
			}
			else
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'       => 0,
					                                     'url'        => 'AWS SNS - create suppression',
					                                     'method'     => 'POST',
					                                     'message'    => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $userId . '] [User Phone: ' . $userPhone . '] [Bounced Email: ' . $bouncedEmail . ']',
					                                     'trace'      => 'AWS SNS error creating suppression',
					                                     'project_id' => config('evibe.project_id'),
					                                     'exception'  => 'AWS SNS error creating suppression',
					                                     'details'    => '[Ticket Id: ' . $ticketId . '] [User Id: ' . $userId . '] [User Phone: ' . $userPhone . '] [Bounced Email: ' . $bouncedEmail . ']'
				                                     ]);

				return null;
			}
		}
	}
}