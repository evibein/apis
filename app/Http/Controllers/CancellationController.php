<?php

namespace App\Http\Controllers;

use App\Http\Models\Cancellations\CustomerCancellationPolicy;
use App\Http\Models\Refunds\CustomerRefunds;
use App\Http\Models\Settlements\LogBookingFinance;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\Settlements\Settlements;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\Booking\MailBookingCancelToCustomerJob;
use App\Jobs\Mail\Booking\MailBookingCancelToPartnerJob;
use App\Jobs\Mail\Finance\MailInitiateCancellationRefundToTeamJob;
use App\Jobs\Mail\MailBookingCancellationToTeamJob;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CancellationController extends BaseController
{
	public function fetchCancellationCharges($ticketBookingId, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$isCustomer = $request['isCustomer'];

			$res = $this->calculateCancellationCharges($ticketBooking, $userId, $isCustomer);

			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while calculating cancellation charges';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.cancel_booking'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[CancellationController.php - ' . __LINE__ . '] ' . $error,
					                                     'exception' => '[CancellationController.php - ' . __LINE__ . '] ' . $error,
					                                     'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . '] [Customer Cancellation: ' . $isCustomer . ']',
					                                     'details'   => '[Ticket Booking Id: ' . $ticketBookingId . '] [Customer Cancellation: ' . $isCustomer . ']'
				                                     ]);
			}

			return $res;

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function cancelTicketBooking($ticketBookingId, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			$isCustomer = $request['isCustomer'];
			$cancellationComments = $request['comments'];
			$sendReceipts = $request['sendReceipts'];
			$res = $this->validateAndCancelBooking($ticketBookingId, $userId, $isCustomer, $cancellationComments, $sendReceipts);

			if (isset($res['success']) && !$res['success'])
			{
				$error = 'Some error occurred while cancelling a booking';

				if (isset($res['error']) && $res['error'])
				{
					$error = $res['error'];
				}

				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.cancel_booking'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[CancellationController.php - ' . __LINE__ . '] ' . $error,
					                                     'exception' => '[CancellationController.php - ' . __LINE__ . '] ' . $error,
					                                     'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . '] [Customer Cancellation: ' . $isCustomer . ']',
					                                     'details'   => '[Ticket Booking Id: ' . $ticketBookingId . '] [Customer Cancellation: ' . $isCustomer . ']'
				                                     ]);
			}

			return response()->json($res);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function validateAndCancelBooking($ticketBookingId, $userId, $isCustomer, $cancellationComments, $sendReceipts = false)
	{
		if (!$ticketBooking = TicketBooking::find($ticketBookingId))
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to find ticket booking [Ticket Booking Id: ' . $ticketBookingId . ']'
			];
		}

		// status check
		if (!$ticket = Ticket::find($ticketBooking->ticket_id))
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to find ticket for the booking [Ticket Booking Id: ' . $ticketBookingId . ']'
			];
		}

		if (!$ticketBooking->is_advance_paid)
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to cancel booking, as advance is not yet paid'
			];
		}

		if (!$cancellationComments)
		{
			return $res = [
				'success' => false,
				'error'   => 'Kindly enter the reason for booking cancellation.'
			];
		}

		$refundAmount = 0;
		$evibeFee = 0;
		$partnerFee = 0;

		$res = $this->calculateCancellationCharges($ticketBooking, $userId, $isCustomer);

		if (isset($res['success']) && $res['success'])
		{
			$refundAmount = isset($res['refundAmount']) ? $res['refundAmount'] : $refundAmount;
			$evibeFee = isset($res['evibeFee']) ? $res['evibeFee'] : $evibeFee;
			$partnerFee = isset($res['$partnerFee']) ? $res['$partnerFee'] : $partnerFee;

			// @todo: customer cancellation charges (or) handler cancellation reason

			// initiate refund amount

			// adjustment (or) ticket_booking
			// update the required

			//$ticketBooking->refund_amount = $refundAmount;

			$ticketBooking->evibe_service_fee = $evibeFee;
			$ticketBooking->cgst_amount = 0;
			$ticketBooking->sgst_amount = 0;
			$ticketBooking->igst_amount = 0;
			$ticketBooking->cancellation_comments = $cancellationComments;

			$ticketBooking->cancelled_user_id = $userId;
			$ticketBooking->cancelled_at = Carbon::now();
			if ($isCustomer)
			{
				$ticketBooking->customer_cancelled_at = Carbon::now();
				$logComments = 'Ticket Booking has been cancelled by customer';
			}
			else
			{
				$ticketBooking->handler_cancelled_at = Carbon::now();
				$logComments = 'Ticket Booking has been cancelled by handler';
			}

			$ticketBooking->updated_at = Carbon::now();
			$ticketBooking->save();

			$settlementBooking = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
			                                               ->whereNull('deleted_at')
			                                               ->whereNull('settlement_done_at')
			                                               ->first();

			if ($settlementBooking)
			{
				$settlement = Settlements::select('settlements.*', 'schedule_settlement_bookings.settlement_amount AS booking_settlement_amount')
				                         ->join('settlement_details', 'settlement_details.settlement_id', '=', 'settlements.id')
				                         ->join('schedule_settlement_bookings', 'schedule_settlement_bookings.id', '=', 'settlement_details.settlement_booking_id')
				                         ->whereNull('schedule_settlement_bookings.deleted_at')
				                         ->whereNull('settlement_details.deleted_at')
				                         ->whereNull('settlements.deleted_at')
				                         ->where('schedule_settlement_bookings.ticket_booking_id', $ticketBooking->id)
				                         ->first();

				if ($settlement)
				{
					// settlement exists
					// do not disturb schedule settlement booking (lookup table) as it is already in settlements
					// lookup will be inline with last ticket booking update (before cancellation trigger)

					// create adjustment with appropriate comments and settlement amount included in settlements
					$autoSettlement = new AutoSettlementController();
					PartnerAdjustments::create([
						                           'partner_id'        => $ticketBooking->map_id,
						                           'partner_type_id'   => $ticketBooking->map_type_id,
						                           'ticket_booking_id' => $ticketBooking->id,
						                           'adjustment_amount' => $settlement->booking_settlement_amount,
						                           'pay_evibe'         => 1,
						                           'handler_id'        => $userId,
						                           'comments'          => 'Adjustment created to nullify the settlement amount of a cancelled booking that has been included to the partner',
						                           'schedule_date'     => $autoSettlement->calculateSettlementScheduleDateTime(time())
					                           ]);
				}
				else
				{
					$settlementBooking->settlement_amount = $partnerFee;
					$settlementBooking->evibe_service_fee = $evibeFee;
					$settlementBooking->cgst_amount = 0;
					$settlementBooking->sgst_amount = 0;
					$settlementBooking->igst_amount = 0;
					$settlementBooking->gst_amount = 0;
					$settlementBooking->updated_at = Carbon::now();
					$settlementBooking->save();
				}
			}
			elseif ($partnerFee)
			{
				// create partner adjustment
				$autoSettlement = new AutoSettlementController();
				$partnerAdjustment = PartnerAdjustments::create([
					                                                'partner_id'        => $ticketBooking->map_id,
					                                                'partner_type_id'   => $ticketBooking->map_type_id,
					                                                'ticket_booking_id' => $ticketBooking->id,
					                                                'adjustment_amount' => $partnerFee,
					                                                'pay_evibe'         => 0,
					                                                'handler_id'        => $userId,
					                                                'comments'          => 'Adjustment created due to booking cancellation',
					                                                'schedule_date'     => $autoSettlement->calculateSettlementScheduleDateTime(time())
				                                                ]);
			}

			if ($refundAmount)
			{
				$customerRefund = CustomerRefunds::create([
					                                          'ticket_booking_id' => $ticketBookingId,
					                                          'refund_amount'     => $refundAmount,
					                                          'evibe_bear'        => 0,
					                                          'partner_bear'      => 0,
					                                          'handler_id'        => $userId,
					                                          'comments'          => 'Refund initiated because of cancellation',
					                                          'created_at'        => Carbon::now(),
					                                          'updated_at'        => Carbon::now()
				                                          ]);

				if (!$customerRefund)
				{
					return $res = [
						'success' => false,
						'error'   => 'Unable to create customer refund'
					];
				}

				$this->dispatch(new MailInitiateCancellationRefundToTeamJob([
					                                                            'refundAmount'        => $this->formatPrice($refundAmount),
					                                                            'name'                => $ticket->name,
					                                                            'email'               => $ticket->email,
					                                                            'phone'               => $ticket->phone,
					                                                            'ticketId'            => $ticket->id,
					                                                            'ticketBookingId'     => $ticketBookingId,
					                                                            'bookingAmount'       => $this->formatPrice($ticketBooking->booking_amount),
					                                                            'advanceAmount'       => $this->formatPrice($ticketBooking->advance_amount),
					                                                            'evibeFee'            => $this->formatPrice($evibeFee),
					                                                            'partnerFee'          => $this->formatPrice($partnerFee),
					                                                            'partyDate'           => date("d M Y h:i A", $ticketBooking->party_date_time),
					                                                            'customerCancelledAt' => $ticketBooking->customer_cancelled_at,
					                                                            'handlerCancelledAt'  => $ticketBooking->handler_cancelled_at,
					                                                            'dashLink'            => config('evibe.hosts.dash') . '/finance/customer-refunds'
				                                                            ]));

				$successMsg = 'Ticket Booking has been successfully cancelled and customer refund has been initiated.';

				LogBookingFinance::create([
					                          'ticket_booking_id'    => $ticketBookingId,
					                          'handler_id'           => $userId,
					                          'type_update'          => 'Auto',
					                          'comments'             => 'Customer refund has been created due to cancellation of booking',
					                          'total_booking_amount' => $ticketBooking->booking_amount,
					                          'refund_amount'        => $refundAmount,
					                          'created_at'           => Carbon::now(),
					                          'updated_at'           => Carbon::now()
				                          ]);
			}

			// @see: party cancellation happens before party date time only

			$successMsg = 'Ticket booking has been successfully cancelled.';

			// notify team
			if ($isCustomer)
			{
				$mailData = [
					'name'            => $ticket->name,
					'email'           => $ticket->email,
					'phone'           => $ticket->phone,
					'ticketId'        => $ticket->id,
					'ticketBookingId' => $ticketBookingId,
					'bookingAmount'   => $ticketBooking->booking_amount,
					'advanceAmount'   => $ticketBooking->advance_amount,
					'evibeFee'        => $evibeFee,
					'partnerFee'      => $partnerFee,
					'partyDate'       => date("d M Y h:i A", $ticketBooking->party_date_time),
					'dashLink'        => config('evibe.hosts.dash') . '/tickets/' . $ticket->id . '/bookings'
				];

				$this->dispatch(new MailBookingCancellationToTeamJob($mailData));
			}

			if ($ticketBooking->cancelled_at && $sendReceipts)
			{
				// @todo: whether to notify customer regarding cancellation or not
				$this->sendBookingCancellationReceipts($ticketBooking, $userId, true);
			}

			// log the action
			LogBookingFinance::create([
				                          'ticket_booking_id'         => $ticketBookingId,
				                          'handler_id'                => $userId,
				                          'type_update'               => 'Manual',
				                          'comments'                  => $logComments,
				                          'total_booking_amount'      => $ticketBooking->booking_amount,
				                          'partner_settlement_amount' => $partnerFee,
				                          'refund_amount'             => $refundAmount,
				                          'evibe_service_fee'         => $evibeFee,
				                          'created_at'                => Carbon::now(),
				                          'updated_at'                => Carbon::now()
			                          ]);

			return $res = [
				'success'    => true,
				'successMsg' => $successMsg
			];
		}
		else
		{
			$error = 'Some error occurred while cancelling ticket booking [Ticket Booking Id: ' . $ticketBookingId . ']';
			if (isset($res['error']) && $res['error'])
			{
				$error = $res['error'];
			}

			return $res = [
				'success' => false,
				'error'   => $error
			];
		}
	}

	public function resendBookingCancellationReceipts($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			// validations
			$ticketBooking = TicketBooking::find($ticketBookingId);

			if (!$ticketBooking)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.cancel_booking'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[CancellationController.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking',
					                                     'exception' => '[CancellationController.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking',
					                                     'trace'     => '[Ticket Booking Id: ' . $ticketBookingId . ']',
					                                     'details'   => '[Ticket Booking Id: ' . $ticketBookingId . ']'
				                                     ]);

				return response()->json([
					                        'success' => false,
				                        ]);
			}

			if ($ticketBooking->cancelled_at)
			{
				// send receipts
				$this->sendBookingCancellationReceipts($ticketBooking, $handlerId, false);

				return response()->json([
					                        'success'    => true,
					                        'successMsg' => 'Booking cancellation receipts sent to both customer and partner'
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Booking has not been cancelled to send booking cancellation receipts.'
				                        ]);
			}

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

	private function calculateCancellationCharges($ticketBooking, $userId, $isCustomer)
	{
		$diffTime = $ticketBooking->party_date_time - time();
		$refundAmount = 0;
		$evibeFee = 0;
		$partnerFee = 0;

		$calculatedAmount = $ticketBooking->booking_amount;

		if ($calculatedAmount > $ticketBooking->advance_amount)
		{
			$calculatedAmount = $ticketBooking->advance_amount;
		}

		if ($isCustomer)
		{
			// if post party cancellation enabled for customer, evibe & partner shares in a booking will get disturbed
			// do not allow customer cancellation if cancellation time is greater than party date time
			if ($diffTime < 0)
			{
				return $res = [
					'success' => false,
					'error'   => 'Unable to cancel ticket booking as the party has already happened [Ticket booking Id: ' . $ticketBooking->id . ']'
				];
			}
			$diffDays = round($diffTime / (24 * 60 * 60));

			$cancellationPolicy = CustomerCancellationPolicy::where('min_day', '<=', $diffDays)
			                                                ->where('max_day', '>=', $diffDays)
			                                                ->whereNull('deleted_at')
			                                                ->first();

			if (!$cancellationPolicy)
			{
				return [
					'success' => false,
					'error'   => 'Unable to find the right cancellation policy [Ticket Booking Id: ' . $ticketBooking->id . ']'
				];
			}

			$refundAmount = ($calculatedAmount * $cancellationPolicy->refund_percent) / 100;
			$evibeFee = ($calculatedAmount * $cancellationPolicy->evibe_fee_percent) / 100;
			$partnerFee = ($calculatedAmount * $cancellationPolicy->partner_fee_percent) / 100;
		}
		else
		{
			$refundAmount = $calculatedAmount;
			$evibeFee = 0;
			$partnerFee = 0;
		}

		if ($ticketBooking->ticket->status_id = !config('evibe.ticket.status.booked'))
		{
			$evibeFee += $partnerFee;
			$partnerFee = 0;
		}

		return $res = [
			'success'       => true,
			'partyDateTime' => date("Y-m-d H:i:s", $ticketBooking->party_date_time),
			'refundAmount'  => $refundAmount,
			'evibeFee'      => $evibeFee,
			'partnerFee'    => $partnerFee,
			'bookingAmount' => $ticketBooking->booking_amount,
			'advanceAmount' => $ticketBooking->advance_amount
		];
	}

	private function sendBookingCancellationReceipts($ticketBooking, $userId, $ticketUpdate = false)
	{
		$ticket = Ticket::find($ticketBooking->ticket_id);
		$handler = $ticket->handler;
		$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBooking->id)
		                                 ->whereNull('deleted_at')
		                                 ->whereNull('rejected_at')
		                                 ->first();

		$option = $this->fillMappingValues([
			                                   'isCollection' => false,
			                                   'mapId'        => $ticketBooking->ticketMapping->map_id,
			                                   'mapTypeId'    => $ticketBooking->ticketMapping->map_type_id
		                                   ]);

		// if only one booking exist 'Cancel' otherwise same status
		// update that a booking has been cancelled
		$bookedBookings = TicketBooking::where('ticket_id', $ticket->id)
		                               ->where('is_advance_paid', 1)
		                               ->whereNull('deleted_at')
		                               ->get();

		$liveBookings = $bookedBookings->filter(function ($item) {
			return $item->cancelled_at == null;
		});

		if (isset($option['type']) && $option['type'] == 'Vendors')
		{
			$option['name'] = null;
		}

		// notify customer
		// email
		// @todo: see whether we should include refund amount or not
		// todo: check whether reminders can be sent or not?
		$this->dispatch(new MailBookingCancelToCustomerJob([
			                                                   'ticketId'            => $ticket->id,
			                                                   'customerName'        => $ticket->name ? $ticket->name : 'Customer',
			                                                   'customerEmail'       => $ticket->email,
			                                                   'partyDateTime'       => $ticketBooking->party_date_time ? date('d M Y h:i A', $ticketBooking->party_date_time) : ($ticket->event_date ? date('d M Y', $ticket->event_date) : '--'),
			                                                   'optionName'          => isset($option['name']) && $option['name'] ? $option['name'] : null,
			                                                   'refundAmount'        => $customerRefund ? $customerRefund->refund_amount : null,
			                                                   'bookingId'           => $ticketBooking->booking_id,
			                                                   'customerCancelledAt' => $ticketBooking->customer_cancelled_at,
			                                                   'bookedBookings'      => count($bookedBookings) > 1 ? true : false,
			                                                   'handlerEmail'        => $handler ? $handler->username : null,
			                                                   'handlerName'         => $handler ? $handler->name : null,
			                                                   'handlerPhone'        => $handler ? $handler->phone : null,
		                                                   ]));
		// sms

		// notify partner if exists
		$partner = $ticketBooking->provider;
		if ($partner)
		{

			/*
			 * Clearly mention regarding the cancelled booking info
			 * Show a table with all the bookings mapped to the partner for that ticket and show individual booking status
			 * Show (booking for a particular partner for a ticket): currently cancelled + remaining live (or) [all the cancelled + pending live]
			 * */

			$partnerBookingsData = [];
			$livePartnerBookingsCount = 0;
			$partnerBookings = $bookedBookings->where('map_id', $ticketBooking->map_id)
			                                  ->where('map_type_id', $ticketBooking->map_type_id);
			if (count($partnerBookings))
			{
				foreach ($partnerBookings as $partnerBooking)
				{
					$partnerBookingOption = $this->fillMappingValues([
						                                                 'isCollection' => false,
						                                                 'mapId'        => $partnerBooking->ticketMapping->map_id,
						                                                 'mapTypeId'    => $partnerBooking->ticketMapping->map_type_id
					                                                 ]);

					$partnerBookingOptionType = $partnerBooking->booking_type_details;
					if (in_array($partnerBookingOptionType, [
						'Venue',
						'Provider',
						'Planner'
					]))
					{
						$partnerBookingOptionType = null;
					}

					array_push($partnerBookingsData, [
						'bookingOption' => isset($partnerBookingOption['name']) && $partnerBookingOption['name'] ? $partnerBookingOption['name'] : ($partnerBookingOptionType ? $partnerBookingOptionType : null),
						'bookingId'     => $partnerBooking->booking_id,
						'bookingAmount' => $partnerBooking->booking_amount,
						'partyDateTime' => $partnerBooking->party_date_time ? date('d M Y, h:i A', $partnerBooking->party_date_time) : null,
						'bookingStatus' => ($partnerBooking->cancelled_at == null) ? 1 : 0
					]);
				}

				$livePartnerBookings = $partnerBookings->filter(function ($item) {
					return $item->cancelled_at == null;
				});
				$livePartnerBookingsCount = $livePartnerBookings ? $livePartnerBookings->count() : 0;
			}

			// will be helpful in-case option name can't be fetched
			// do not consider if the booking type is generic partner types
			$optionType = $ticketBooking->booking_type_details;
			if (in_array($optionType, [
				'Venue',
				'Provider',
				'Planner'
			]))
			{
				$optionType = null;
			}

			{
				$this->dispatch(new MailBookingCancelToPartnerJob([
					                                                  'ticketId'                 => $ticket->id,
					                                                  'bookingId'                => $ticketBooking->booking_id,
					                                                  'partnerName'              => $partner->person,
					                                                  'partnerEmail'             => $partner->email,
					                                                  'optionName'               => isset($option['name']) && $option['name'] ? $option['name'] : ($optionType ? $optionType : null),
					                                                  'partyDateTime'            => $ticketBooking->party_date_time ? date('d M Y h:i A', $ticketBooking->party_date_time) : ($ticket->event_date ? date('d M Y', $ticket->event_date) : '--'),
					                                                  'customerCancelledAt'      => $ticketBooking->customer_cancelled_at,
					                                                  'customerName'             => $ticket->name,
					                                                  'partnerBookings'          => $partnerBookingsData,
					                                                  'livePartnerBookingsCount' => $livePartnerBookingsCount
				                                                  ]));
			}
		}

		// ticket update
		if ($ticketUpdate)
		{
			$this->updateTicketAction([
				                          'ticket'               => $ticket,
				                          'userId'               => $userId,
				                          'comments'             => '#' . $ticketBooking->booking_id . ' is cancelled by ' . ($ticketBooking->customer_cancelled_at ? 'Customer' : 'Handler') . '. Booking cancellation receipts sent.',
				                          'statusId'             => count($liveBookings) ? $ticket->status_id : config('evibe.ticket.status.cancelled'),
				                          'isManualCancellation' => count($liveBookings) ? false : true,
			                          ]);
		}
	}
}