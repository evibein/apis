<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Models\Area;
use App\Http\Models\Cake;
use App\Http\Models\CheckoutFields;
use App\Http\Models\CheckoutFieldValue;
use App\Http\Models\Decor;
use App\Http\Models\Package;
use App\Http\Models\Tags;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Http\Models\TicketFollowups;
use App\Http\Models\TicketFollowupsType;
use App\Http\Models\TicketMapping;
use App\Http\Models\TicketServicesTags;
use App\Http\Models\TicketUpdate;
use App\Http\Models\Trends;
use App\Http\Models\TypeBooking;
use App\Http\Models\TypeCategoryTags;
use App\Http\Models\TypeEvent;
use App\Http\Models\TypeService;
use App\Http\Models\TypeTicket;
use App\Http\Models\TypeTicketSource;
use App\Http\Models\User;
use App\Http\Models\Util\ProductSortOrder;
use App\Http\Models\Venue;
use App\Http\Models\VenueHall;
use App\Jobs\Mail\MailRecommendationsToCustomerJob;
use App\Jobs\Sms\SMSRecommendationsAlertToCustomer;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class TicketController extends BaseController
{
	// @todo: need to optimise code (mainly queries) and create functions to reuse code
	// @todo: need to test for all kinds of inputs
	// @todo: need to figure all possible kind of errors and output with a code

	public function getBasicPartyDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		$data = [
			'success'            => true,
			'ticketId'           => $ticket->id,
			'customerGender'     => $ticket->customer_gender,
			'name'               => $ticket->name,
			'eventId'            => $ticket->event_id,
			'cityId'             => $ticket->city_id,
			'budget'             => $ticket->budget,
			'typeVenueId'        => $ticket->type_venue_id,
			'eventDate'          => $ticket->event_date,
			'zip_code'           => $ticket->zip_code,
			'area_id'            => $ticket->area_id,
			'teamNotes'          => $ticket->team_notes,
			'customerComments'   => $ticket->comments,
			'eventSlot'          => $ticket->event_date,//converting to time in the view file
			'kidsBirthdayGender' => $ticket->gender,
			'email'              => $ticket->email
			//'flexEventDate' => $ticket->flex_event_date
		];

		$this->updateTicketHandler($ticketId, $userId);

		return response()->json($data);
	}

	public function getEventDate($eventDate, $eventSlot)
	{
		switch ($eventSlot)
		{
			case 1:
				//Breakfast
				$eventDate = Carbon::createFromTimestamp(strtotime($eventDate))->startOfDay()->addHour(7);
				break;
			case 2:
				//Lunch
				$eventDate = Carbon::createFromTimestamp(strtotime($eventDate))->startOfDay()->addHour(11);
				break;
			case 3:
				//HiTea
				$eventDate = Carbon::createFromTimestamp(strtotime($eventDate))->startOfDay()->addHour(15);
				break;
			case 4:
				//Dinner
				$eventDate = Carbon::createFromTimestamp(strtotime($eventDate))->startOfDay()->addHour(19);
				break;
			default:
				$eventDate = $eventDate = Carbon::createFromTimestamp(strtotime($eventDate))->startOfDay();
				break;
		}

		return $eventDate;
	}

	public function createTicketWithBasicDetails(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		try
		{
			$rules = [
				"customerGender" => 'required',
				'name'           => 'required',
				'eventId'        => 'required|integer',
				'cityId'         => 'required|integer',
				'eventDate'      => 'required|date|after:yesterday',
				'zip_code'       => 'required|integer',
				'area_id'        => 'required',
				'eventSlot'      => 'required',
				'email'          => 'required|email'
			];

			$messages = [
				'customerGender.required' => 'Please select gender',
				'eventId.required'        => 'Occasion should not be empty',
				'eventId.integer'         => 'Please select a valid occasion',
				'cityId.required'         => 'City should not be empty',
				'cityId.integer'          => 'Please select a valid city',
				'zip_code.required'       => 'Please enter the pin code',
				'area_id.required'        => 'Please enter area name',
				'zip_code.integer'        => 'Please enter valid pin code',
				'eventSlot.required'      => 'Please select event slot',
				'email.required'          => 'Your email is required',
				'email.email'             => 'Your email id should be in the form of example@abc.com',
			];

			if ($request['eventId'] == config("evibe.event.kids_birthday"))
			{
				$rules = array_merge($rules, [
					'typeVenueId'   => 'required|integer',
					'specificEvent' => 'required'
				]);

				$messages = array_merge($messages, [
					'typeVenueId.required'   => 'Please Select Venue Type',
					'typeVenueId.integer'    => 'Please Select a Venue Type',
					'specificEvent.required' => 'Please Select Event',
				]);

				if ($request['specificEvent'] != config('evibe.event.baby_shower') && $request['specificEvent'] != config('evibe.event.saree_ceremony') && $request['specificEvent'] != config('evibe.event.dothi_ceremony'))
				{
					if ($request['specificEvent'] != config('evibe.event.baby_shower') && $request['specificEvent'] != config('evibe.event.saree_ceremony') && $request['specificEvent'] != config('evibe.event.dothi_ceremony'))
					{
						//baby gender not required in baby shower,saree ceremony,dhothi ceremony
						$rules = array_merge($rules, [
							'babyGender' => 'required'
						]);

						$messages = array_merge($messages, [
							'babyGender.required' => 'Please Select Gender'
						]);
					}
				}

			}

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validation['error']
				                        ]
				);
			}
			$eventDate = $request['eventDate'];
			$eventSlot = $request['eventSlot'];
			$time = "$eventDate" . " $eventSlot";
			$eventDate = strtotime($time);
			//$eventDate = $this->getEventDate($eventDate, $eventSlot);
			//$eventDate = $eventDate->timestamp;
			$ticketData = [
				'customer_gender'   => $request['customerGender'],
				'name'              => $request['name'],
				'event_id'          => $request['eventId'],
				'city_id'           => $request['cityId'],
				'email'             => $request['email'],
				'event_date'        => $eventDate,
				'enquiry_source_id' => config('evibe.ticket.enquiry_source.direct'),
				'created_at'        => Carbon::now(),
				'updated_at'        => Carbon::now(),
				'zip_code'          => $request['zip_code'],
				'area_id'           => $request['area_id'],
				'team_notes'        => $request['teamNotes'],
				//'event_slot'        => $eventSlot,
			];

			if ($request['eventId'] == config("evibe.event.kids_birthday"))
			{
				$ticketData['type_venue_id'] = $request['typeVenueId'] > 0 ? $request['typeVenueId'] : null;
				$ticketData['gender'] = $request['babyGender'];
				$ticketData['event_id'] = $request['specificEvent'];
			}
			elseif ($request['eventId'] == config("evibe.event.special_experience"))
			{
				$ticketData['type_venue_id'] = $request['typeVenueId'] > 0 ? $request['typeVenueId'] : null;
			}

			$ticket = Ticket::create($ticketData);

			if ($ticket)
			{
				$ticket->enquiry_id = 'ENQ-BLR-DI-' . $ticket->id;
				$ticket->save();
				$this->updateTicketHandler($ticket->id, $userId);

				return response()->json(['success' => 'true', 'ticketId' => $ticket->id]);
			}

			return response()->json(['success' => 'false', 'error' => 'Some error occurred while creating a ticket']);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function updateBasicPartyDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$rules = [
				'customerGender' => 'required',
				'name'           => 'required',
				'eventId'        => 'required|integer',
				'cityId'         => 'required|integer',
				'eventDate'      => 'required|date|after:yesterday',
				'zip_code'       => 'required|integer',
				'area_id'        => 'required',
				'eventSlot'      => 'required',
				'email'          => 'required|email'
				//'flexEventDate' => 'date|after:eventDate'
			];

			$messages = [
				'customerGender.required' => 'Please Select gender of customer',
				'eventId.required'        => 'Occasion should not be empty',
				'eventId.integer'         => 'Please select a valid occasion',
				'cityId.required'         => 'City should not be empty',
				'cityId.integer'          => 'Please select a valid city',
				'zip_code.required'       => 'Please enter the pin code',
				'area_id.required'        => 'Please enter area name',
				'zip_code.integer'        => 'Please enter valid pin code',
				'eventSlot.required'      => 'Please select event slot',
				'email.required'          => 'Your email is required',
				'email.email'             => 'Your email id should be in the form of example@abc.com',
			];

			if ($request['eventId'] == config("evibe.event.kids_birthday"))
			{
				$rules = array_merge($rules, [
					'typeVenueId'   => 'required|integer',
					'specificEvent' => 'required',
				]);

				$messages = array_merge($messages, [
					'typeVenueId.required'   => 'Please Select Venue Type',
					'typeVenueId.integer'    => 'Please Select a Venue Type',
					'specificEvent.required' => 'Please Select Event',

				]);

				if ($request['specificEvent'] != config('evibe.event.baby_shower') && $request['specificEvent'] != config('evibe.event.saree_ceremony') && $request['specificEvent'] != config('evibe.event.dothi_ceremony'))
				{
					//baby gender not required in baby shower,saree ceremony,dhothi ceremony
					$rules = array_merge($rules, [
						'babyGender' => 'required'
					]);

					$messages = array_merge($messages, [
						'babyGender.required' => 'Please Select Gender'
					]);
				}
			}

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validation['error']
				                        ]
				);
			}

			$parentEventId = $this->getParentEventId($request['selectedEventId']);
			$eventDate = $request['eventDate'];
			$eventSlot = $request['eventSlot'];
			$time = "$eventDate" . " $eventSlot";
			$eventDate = strtotime($time);
			//$eventDate = $this->getEventDate($eventDate, $eventSlot);
			//$eventDate = $eventDate->timestamp;

			$ticketBookings = TicketBooking::where('ticket_id', $ticketId)
			                               ->whereNull('deleted_at')
			                               ->whereNull('cancelled_at')
			                               ->get();
			if ($ticketBookings && $ticketBookings->min('party_date_time'))
			{
				$eventDate = $ticketBookings->min('party_date_time');
			}

			$ticketData = [
				'customer_gender' => $request['customerGender'],
				'name'            => $request['name'],
				'event_id'        => $parentEventId == $request['eventId'] ? $request['selectedEventId'] : $request['eventId'],
				'city_id'         => $request['cityId'],
				'budget'          => $request['budget'],
				'event_date'      => $eventDate,
				'updated_at'      => Carbon::now(),
				'zip_code'        => $request['zip_code'],
				'area_id'         => $request['area_id'],
				'team_notes'      => $request['teamNotes'],
				//'event_slot'      => $eventSlot,
				'email'           => $request['email']
			];

			if ($request['eventId'] == config("evibe.event.kids_birthday"))
			{
				$ticketData['type_venue_id'] = $request['typeVenueId'] > 0 ? $request['typeVenueId'] : null;
				$ticketData['gender'] = $request['babyGender'];
				$ticketData['event_id'] = $request['specificEvent'];
			}
			elseif ($request['eventId'] == config("evibe.event.special_experience"))
			{
				$ticketData['type_venue_id'] = $request['typeVenueId'] > 0 ? $request['typeVenueId'] : null;
			}

			//if ($request['flexEventDate'])
			//{
			//	$ticketData['flex_event_date'] = strtotime($request['flexEventDate']);
			//}

			$ticket = Ticket::find($ticketId)->update($ticketData);

			if ($ticket)
			{
				$this->updateTicketHandler($ticketId, $userId);

				return response()->json(['success' => true, 'ticketId' => $ticketId]);
			}

			throw new CustomException('Ticket not found', 404);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function getAdditionalPartyDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$ticket->event_id)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly update Occasion to proceed with ticket additional details'
			                        ]);
		}

		try
		{
			$eventId = $ticket->event_id;
			$parentIdOfEventId = $this->getParentEventId($eventId);

			$kidsBirthdayData = TypeEvent::select('id', 'name', 'parent_id')
			                             ->where('parent_id', $parentIdOfEventId)
			                             ->get();

			$res = [
				'success'            => true,
				'ticketId'           => $ticketId,
				'eventId'            => $eventId,
				'parentIdOfEventId'  => $parentIdOfEventId,
				'kidsBirthdayGender' => $ticket->gender,
				'kidsBirthDayInfo'   => $kidsBirthdayData,
				'customerComments'   => $ticket->comments,
				'teamNotes'          => $ticket->team_notes
			];

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function saveAdditionalPartyDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$kidsBirthDayInfo = $request['childEventId'];
			$genderOfBaby = $request['kidsBirthdayGender'];
			$teamNotes = $request['teamNotes'];
			$rules = [
				'childEventId'       => 'required',
				'kidsBirthdayGender' => 'required'
			];
			$messages = [
				'childEventId.required'       => 'Please Select Event',
				'kidsBirthdayGender.required' => 'Please Select Gender'
			];
			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success'  => false,
					                        'error'    => $validation['error'],
					                        "ticketId" => $ticketId
				                        ]
				);
			}
			$ticket = Ticket::find($ticketId);
			if ($ticket)
			{
				$ticket->gender = $genderOfBaby;
				$ticket->event_id = $kidsBirthDayInfo;
				$ticket->team_notes = $teamNotes;
				$ticket->save();
			}

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json(["success" => true, "ticketId" => $ticketId]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function updateAdditionalPartyDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$kidsBirthDayInfo = $request['childEventId'];
			if ((is_null($kidsBirthDayInfo)) && ($kidsBirthDayInfo <= 0))
			{
				throw new CustomException('Please enter valid event in party requirements', 400);
			}

			$genderOfBaby = $request['kidsBirthdayGender'];
			$teamNotes = $request['teamNotes'];
			$ticket = Ticket::find($ticketId);
			if ($ticket)
			{
				$ticket->gender = $genderOfBaby;
				$ticket->event_id = $kidsBirthDayInfo;
				$ticket->team_notes = $teamNotes;
				$ticket->save();
			}

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json(["success" => true, "ticketId" => $ticketId]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function getPartyServices($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		// @todo: validate with all necessary ticket data used in various functions
		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$ticket->event_id)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly update Occasion to proceed with ticket services'
			                        ]);
		}

		try
		{
			$eventId = $this->getParentEventId($ticket->event_id);
			$eventId = $this->fetchPrimaryEventId($eventId);

			$option = [];
			$uploadGallery = [];
			$customEnquiry = [];
			$services = [];

			// uploaded images for custom enquiry
			if ($ticket->gallery->count())
			{
				foreach ($ticket->gallery as $key => $uploadedImages)
				{
					$uploadGallery[$key]['id'] = $uploadedImages->id;
					$uploadGallery[$key]['imageLink'] = $uploadedImages->imageLink();
				}
			}

			// get custom enquiry related data
			if ($ticket->enquiry_source_id == config('evibe.ticket.enquiry_source.product_enquiry') && $ticket->type_ticket_id && $ticket->type_id)
			{
				$name = null;
				$code = null;
				$url = null;
				$minPrice = null;
				$maxPrice = null;
				$priceWorth = null;
				$priceRangeInfo = null;
				$pricePerExtraGuest = null;
				$pricePerKg = null;
				$inclusions = null;
				$profilePic = null;

				switch ($ticket->type_ticket_id)
				{
					case config('evibe.ticket.type.packages'):
					case config('evibe.ticket.type.resort'):
					case config('evibe.ticket.type.villa'):
					case config('evibe.ticket.type.lounge'):
					case config('evibe.ticket.type.food'):
					case config('evibe.ticket.type.couple-experiences'):
					case config('evibe.ticket.type.venue-deals'):
					case config('evibe.ticket.type.priests'):
					case config('evibe.ticket.type.tents'):
					case config('evibe.ticket.type.generic-package'):
						$package = Package::find($ticket->type_id);

						$name = $package->name;
						$code = $package->code;
						$url = "packages/view/" . $package->id;
						$minPrice = $package->price;
						$maxPrice = $package->price_max;
						$priceWorth = $package->price_worth;
						$priceRangeInfo = $package->range_info;
						$pricePerExtraGuest = $package->price_per_extra_guest;
						$inclusions = $package->info;
						$profilePic = $package->getProfilePic();

						break;
					case config('evibe.ticket.type.cakes'):
						$cake = Cake::find($ticket->type_id);

						$name = $cake->title;
						$code = $cake->code;
						$url = "cakes/" . $cake->id . "/info";
						$minPrice = $cake->price;
						$maxPrice = $cake->max_price;
						$priceWorth = $cake->price_worth;
						$pricePerKg = $cake->price_per_kg;
						$inclusions = $cake->info;
						$profilePic = $cake->getProfilePic();
						break;

					case config('evibe.ticket.type.decors'):
						$decor = Decor::find($ticket->type_id);

						$name = $decor->name;
						$code = $decor->code;
						$url = "decors/" . $decor->id . "/info";
						$minPrice = $decor->min_price;
						$maxPrice = $decor->max_price;
						$priceWorth = $decor->worth;
						$priceRangeInfo = $decor->range_info;
						$inclusions = $decor->info;
						$profilePic = $decor->getProfilePic();
						break;

					case config('evibe.ticket.type.trends'):
						$trend = Trends::find($ticket->type_id);

						$name = $trend->name;
						$code = $trend->code;
						$url = "trends/view/" . $trend->id;
						$minPrice = $trend->price;
						$maxPrice = $trend->price_max;
						$priceWorth = $trend->price_worth;
						$priceRangeInfo = $trend->range_info;
						$inclusions = $trend->info;
						$profilePic = $trend->getProfilePic();
						break;

					case config('evibe.ticket.type.entertainments'):
						$service = TypeService::find($ticket->type_id);

						$name = $service->name;
						$code = $service->code;
						$url = "services/details/" . $service->id;
						$minPrice = $service->min_price;
						$maxPrice = $service->max_price;
						$priceWorth = $service->worth_price;
						$priceRangeInfo = $service->range_info;
						$inclusions = $service->info;
						$profilePic = $service->getProfilePic();
						break;

					case config('evibe.ticket.type.venue_halls'):
						$hall = VenueHall::find($ticket->type_id);

						$name = $hall->name;
						$code = $hall->code;
						$url = "venues/view/" . $hall->id;
						$minPrice = $hall->price_min_veg;
						$maxPrice = $hall->price_max_veg;
						$profilePic = $hall->getProfilePic();
						break;

					default:
						break;
				}

				$link = config("evibe.hosts.dash") . "/" . $url;

				$option = [
					"id"                 => $ticket->type_id,
					"optionTypeId"       => $ticket->type_ticket_id,
					"name"               => $name,
					"code"               => $code,
					"dashLink"           => $link,
					"profilePic"         => $profilePic,
					"minPrice"           => $minPrice,
					"maxPrice"           => $maxPrice,
					"priceWorth"         => $priceWorth,
					"priceRangeInfo"     => $priceRangeInfo,
					"pricePerExtraGuest" => $pricePerExtraGuest,
					"pricePerKg"         => $pricePerKg,
					"inclusions"         => $inclusions,
				];
			}
			elseif ($ticket->enquiry_source_id == config('evibe.ticket.enquiry_source.cd_food') ||
				$ticket->enquiry_source_id == config('evibe.ticket.enquiry_source.cd_decor') ||
				$ticket->enquiry_source_id == config('evibe.ticket.enquiry_source.cd_cake')
			)
			{
				$customEnquiry["foodType"] = $ticket->food_type;
				$customEnquiry["foodServiceType"] = $ticket->food_service_type;

				// if API is requested for a particular ticket booking
				if ($ticket->custom_booking_type)
				{
					$reqFieldsArray = $this->getRequirementFields($ticketId, $ticket->custom_booking_type, $eventId);

					$customEnquiry["selectedBookingType"] = [
						"BookingTypeId"     => $ticket->custom_booking_type,
						"requirementFields" => $reqFieldsArray,
					];
				}
				// get all applicable ticket bookings

				$bookingTypes = [];
				$typeTicketBookings = TypeBooking::where('for_auto_booking', 0)->get();
				foreach ($typeTicketBookings as $key => $value)
				{
					$bookingTypes[$key] = [
						"id"   => $value->id,
						"name" => $value->name,
					];
				}
				$customEnquiry["bookingTypes"] = $bookingTypes;

			}

			$eventCategories = config('evibe.event-categories');

			// get all applicable category tags for the services
			$typeCategoryTags = TypeCategoryTags::join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                                    ->select('type_category_tags.*', DB::raw('type_tags.identifier AS tag_name, type_tags.map_type_id AS map_type_id, type_tags.type_event AS event_id'))
			                                    ->where(function ($query) use ($eventId) {
				                                    $query->where('type_tags.type_event', $eventId)
				                                          ->orWhere('type_tags.type_event', null);
			                                    });

			if (isset($eventCategories[$eventId]) && count($eventCategories[$eventId]))
			{
				$typeCategoryTags->whereIn('type_tags.map_type_id', $eventCategories[$eventId]);
			}
			else
			{
				$typeCategoryTags->whereNotNull('type_tags.map_type_id');
			}

			$typeCategoryTags = $typeCategoryTags->whereNull('type_tags.deleted_at')
			                                     ->get();

			foreach ($typeCategoryTags as $value)
			{
				$tagSelection = $value->getCategoryTagSelection($ticketId);

				array_push($services, [
					"id"           => $value->id,
					"name"         => $value->tag_name,
					"optionsCount" => $value->count,
					"minPrice"     => $value->min_price,
					"maxPrice"     => $value->max_price,
					"description"  => $value->info,
					"serviceType"  => $value->map_type_id,
					"eventId"      => $value->event_id,
					"isSelected"   => $tagSelection['success'],
					"selectedBy"   => $tagSelection['handlerName'],
					"selectedAt"   => $tagSelection['selectedAt']
				]);
			}

			$res = [
				"success"        => true,
				"ticketId"       => $ticketId,
				"enquiredOption" => $option,
				"customEnquiry"  => [
					"uploadGallery" => $uploadGallery,
					"enquiryData"   => $customEnquiry,
				],
				"services"       => $services,
				"comments"       => $ticket->comments,
				"teamNotes"      => $ticket->team_notes,
				"maxBudget"      => $ticket->max_budget,
				"minBudget"      => $ticket->min_budget,
				"eventId"        => $this->getParentEventId($ticket->event_id)
			];

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function getServicesRequirementFields($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$bookingType = $request['bookingType'];
		$eventId = $request['eventId'];

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$bookingType)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Booking type is required to fetch service requirement fields'
			                        ]);
		}

		if (!$eventId)
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Kindly update Occasion to proceed with service requirement fields'
			                        ]);
		}

		$eventId = $this->getParentEventId($ticket->event_id);
		$eventId = $this->fetchPrimaryEventId($eventId);

		try
		{
			$reqFieldsArray = $this->getRequirementFields($ticketId, $bookingType, $eventId);
			$res = [
				"success"        => true,
				"ticketId"       => $ticketId,
				"reqFieldsArray" => $reqFieldsArray
			];

			$ticket->custom_booking_type = $bookingType;
			$ticket->save();

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function saveServicesRequirements($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{

			$requirementFieldValues = $request['requirementFields'];
			$selectedServices = $request['selectedServices'];
			$minBudget = $request['minBudget'];
			$maxBudget = $request['maxBudget'];
			if ($minBudget >= $maxBudget)
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "Min Budget should be less than max budget"
				                        ]);
			}
			if ($requirementFieldValues && count($requirementFieldValues) > 0)
			{
				foreach ($requirementFieldValues as $requirementFieldValue)
				{
					CheckoutFieldValue::create([
						                           "ticket_id"         => $ticketId,
						                           "checkout_field_id" => $requirementFieldValue['id'],
						                           "value"             => $requirementFieldValue['value'],
						                           "created_at"        => Carbon::now(),
						                           "updated_at"        => Carbon::now()
					                           ]);
				}
			}

			if ($selectedServices && count($selectedServices) > 0)
			{
				foreach ($selectedServices as $service)
				{
					TicketServicesTags::create([
						                           "ticket_id"            => $ticketId,
						                           "type_category_tag_id" => $service['id'],
						                           "handler_id"           => $userId,
						                           "created_at"           => Carbon::now(),
						                           "updated_at"           => Carbon::now()
					                           ]);
				}
			}

			$ticket->team_notes = $request['teamNotes'];
			$ticket->max_budget = $maxBudget;
			$ticket->min_budget = $minBudget;
			$ticket->save();

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json([
				                        "success"  => true,
				                        "ticketId" => $ticketId
			                        ]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function updateServicesRequirements($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$minBudget = $request['minBudget'];
			$maxBudget = $request['maxBudget'];
			if ($minBudget >= $maxBudget)
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "Min Budget should be less than max budget"
				                        ]);
			}
			// update comments
			$ticket->team_notes = $request['teamNotes'];
			$ticket->max_budget = $maxBudget;
			$ticket->min_budget = $minBudget;
			$ticket->save();

			// update requirement fields
			$requirementFields = $request['requirementFields'];

			if ($requirementFields && count($requirementFields) > 0)
			{
				foreach ($requirementFields as $requirementField)
				{
					$checkoutFieldValue = CheckoutFieldValue::where([
						                                                "checkout_field_id" => $requirementField['id'],
						                                                "ticket_id"         => $ticketId
					                                                ])
					                                        ->update([
						                                                 "value"      => $requirementField['value'],
						                                                 "updated_at" => Carbon::now()
					                                                 ]);

					if (!$checkoutFieldValue)
					{
						CheckoutFieldValue::create([
							                           "ticket_id"         => $ticketId,
							                           "checkout_field_id" => $requirementField['id'],
							                           "value"             => $requirementField['value'],
							                           "created_at"        => Carbon::now(),
							                           "updated_at"        => Carbon::now()
						                           ]);
					}
				}
			}

			// update selected tags
			$selectedServices = $request['selectedServices'];
			$ssArray = [];
			if ($selectedServices && count($selectedServices) > 0)
			{
				// get selected array
				foreach ($selectedServices as $selectedService)
				{
					$ssArray[] = $selectedService['id'];
				}

				$existingServices = TicketServicesTags::where('ticket_id', $ticketId)
				                                      ->whereNull('deleted_at')
				                                      ->pluck('type_category_tag_id')
				                                      ->toArray();

				$unusedServices = array_diff($existingServices, $ssArray);
				$newServices = array_diff($ssArray, $existingServices);
				$usedServices = array_intersect($ssArray, $existingServices);

				// delete unused services
				TicketServicesTags::where('ticket_id', $ticketId)
				                  ->whereIn('type_category_tag_id', $unusedServices)
				                  ->update([
					                           "deleted_at" => Carbon::now()
				                           ]);

				// save new services
				foreach ($newServices as $newService)
				{
					TicketServicesTags::create([
						                           "ticket_id"            => $ticketId,
						                           "type_category_tag_id" => $newService,
						                           "created_at"           => Carbon::now(),
						                           "updated_at"           => Carbon::now()
					                           ]);
				}

				// update used services
				TicketServicesTags::where('ticket_id', $ticketId)
				                  ->whereIn('type_category_tag_id', $usedServices)
				                  ->update([
					                           "updated_at" => Carbon::now()
				                           ]);

				// get existing array
				// a-b comparison (get exceptional and delete)
				// add new or previously deleted (check for existing live)

				$this->updateTicketHandler($ticketId, $userId);

				return response()->json(["success" => true, "ticketId" => $ticketId]);
			}

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function getContactDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$nextAutoFollowUp = null;

			$currentFollowUp = TicketFollowups::where('ticket_id', $ticketId)
			                                  ->whereNull('is_complete')
			                                  ->first();

			$followupType = TicketFollowupsType::select('id', 'name')
			                                   ->get();

			$ticketSource = TypeTicketSource::select('id', 'name')->whereNull('deleted_at')->get();

			$res = [
				"success"              => true,
				"ticketId"             => $ticketId,
				"phone"                => $ticket->phone,
				"altPhone"             => $ticket->alt_phone,
				"venueLandmark"        => $ticket->venue_landmark,
				"teamNotes"            => $ticket->team_notes,
				"followupType"         => $followupType,
				"popUpEnquiryTimeSlot" => $ticket->customer_preferred_slot,
				"ticketSource"         => $ticketSource,
				"thisTicketSourceId"   => $ticket->source_id,
				'sourceSpecific'       => $ticket->source_specific,
				"ticketFollowup"       => $currentFollowUp ? $currentFollowUp->followup_type : "",
				"closureDate"          => $ticket->expected_closure_date,
				"altPhone"             => $ticket->alt_phone
			];

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	// @todo: function usage for `post` and `put`
	public function saveContactDetails($ticketId, Request $request)
	{
		return $this->partyContactDetails($ticketId, $request);
	}

	public function updateContactDetails($ticketId, Request $request)
	{
		return $this->partyContactDetails($ticketId, $request);
	}

	public function getStatusDetails($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException("Ticket not found", 404);
		}

		$whenCustomerPlanToBookThisServices = [
			['value' => 1, 'name' => "Immediately"],
			['value' => 2, 'name' => "In a day"],
			['value' => 6, 'name' => "In 3 days"],
			['value' => 3, 'name' => "In a week"],
			['value' => 4, 'name' => "In a month"],
			['value' => 5, 'name' => "Just started planning"]
		];

		$followupType = TicketFollowupsType::select('id', 'name')
		                                   ->get();

		$todayHours = Carbon::now()->hour;

		$nextAutoFollowUp = null;

		$currentFollowUp = TicketFollowups::where('ticket_id', $ticketId)
		                                  ->whereNull('is_complete')
		                                  ->first();

		if ($currentFollowUp)
		{
			$nextAutoFollowUp = date('Y/m/d H:i', $currentFollowUp->followup_date);
		}
		elseif ($todayHours < 13)
		{
			$nextAutoFollowUp = date('Y/m/d H:i', Carbon::tomorrow()->addHours(12)->timestamp);
		}
		elseif ($todayHours >= 13)
		{
			$nextAutoFollowUp = date('Y/m/d H:i', Carbon::tomorrow()->addHours(17)->timestamp);
		}
		elseif (Carbon::now()->isSaturday())
		{
			$nextAutoFollowUp = date('Y/m/d H:i', Carbon::now()->next()->startOfWeek()->addHours(12)->timestamp);
		}

		$ticketSource = TypeTicketSource::select('id', 'name')->whereNull('deleted_at')->get();

		$data = [
			"whenCustomerPlanToBookThisServices" => $whenCustomerPlanToBookThisServices,
			"nextAutoFollowUp"                   => $nextAutoFollowUp,
			"bookingLikelinessId"                => $ticket->booking_likeliness_id,
			"ticketSource"                       => $ticketSource,
			"thisTicketSourceId"                 => $ticket->source_id,
			'sourceSpecific'                     => $ticket->source_specific,
			"teamNotes"                          => $ticket->team_notes,
			"altEmail"                           => $ticket->alt_email,
			"altPhone"                           => $ticket->alt_phone,
			"decisionMaker"                      => $ticket->type_decision_maker,
			"popUpEnquiryTimeSlot"               => $ticket->customer_preferred_slot,
			"followupType"                       => $followupType,
			"ticketFollowup"                     => $currentFollowUp ? $currentFollowUp->followup_type : ""
		];

		return response()->json($data);
	}

	public function saveStatusDetails($ticketId, Request $request)
	{
		return $this->baseStatusDetails($ticketId, $request);
	}

	public function updateStatusDetails($ticketId, Request $request)
	{
		return $this->baseStatusDetails($ticketId, $request);
	}

	public function baseStatusDetails($ticketId, $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$ticketSource = $request['ticketSource'];
			$ticketSourceSpecific = $request['sourceSpecific'];
			$leadStatusId = "";
			$rules = [
				'ticketSource'  => 'required',
				'status'        => 'required',
				'decisionMaker' => 'required',
				'altEmail'      => 'email',
				'altPhone'      => 'digits:10'
			];
			$messages = [
				'ticketSource.required'  => 'Please Select Ticket Source',
				'status.required'        => 'Please Select Booking Likeliness',
				'decisionMaker.required' => 'Please Select the Decision Maker',
				'altEmail.email'         => "Please Enter Valid email",
				'altPhone.digits'        => "Please Enter Valid Phone"
			];
			$validation = $this->validateJSONData($request, $rules, $messages);

			if ($validation['error'])
			{
				return response()->json([
					                        'success'  => false,
					                        'error'    => $validation['error'],
					                        "ticketId" => $ticket->id
				                        ]
				);
			}

			if (in_array($request['status'], [1, 2, 3, 6]))
			{
				$leadStatusId = config("evibe.ticket.leadStatus.hot");
			}
			elseif (in_array($request['status'], [4]))
			{
				$leadStatusId = config("evibe.ticket.leadStatus.medium");
			}
			elseif (in_array($request['status'], [5]))
			{
				$leadStatusId = config("evibe.ticket.leadStatus.cold");
			}

			$closureDate = $this->getClosureDate(Carbon::createFromFormat('Y-m-d H:i:s', $ticket->created_at), $request['status']);
			$ticket->update([
				                "booking_likeliness_id"   => $request['status'],
				                "lead_status_id"          => $leadStatusId,
				                "status_id"               => ($ticket->status_id != config("evibe.ticket.status.booked")) ? config("evibe.ticket.status.followup") : $ticket->status_id,
				                "source_id"               => $ticketSource > 0 ? $ticketSource : null,
				                "source_specific"         => $ticketSourceSpecific ? $ticketSourceSpecific : null,
				                "expected_closure_date"   => $closureDate,
				                "team_notes"              => $request['teamNotes'],
				                "alt_phone"               => $request['altPhone'],
				                "alt_email"               => $request['altEmail'],
				                "type_decision_maker"     => $request['decisionMaker'],
				                "customer_preferred_slot" => $request['popUpEnquiryTimeSlot'],
			                ]);
			$ticket->save();

			TicketFollowups::where('ticket_id', $ticketId)->update(['is_complete' => 1]);

			TicketFollowups::create([
				                        "followup_type" => $request['typeFollowup'],
				                        "ticket_id"     => $ticketId,
				                        "followup_date" => strtotime($request['nextFollowUp']),
				                        "handler_id"    => $userId,

			                        ]);

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json(["success" => true, "ticketId" => $ticket->id]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	private function getRequirementFields($ticketId, $typeTicketBookingId, $eventId)
	{
		if (!$typeTicketBookingId)
		{
			throw new CustomException('Booking type not found', 404);
		}

		if (!$eventId)
		{
			throw new CustomException('Event id not found', 404);
		}

		$reqFieldsArray = [];
		$reqFields = CheckoutFields::where([
			                                   'type_ticket_booking_id' => $typeTicketBookingId,
			                                   'event_id'               => $eventId,
			                                   'is_auto_booking'        => 0
		                                   ])->get();

		foreach ($reqFields as $value)
		{
			$reqFieldValue = $value->getCheckoutValue($ticketId);

			array_push($reqFieldsArray, [
				"id"          => $value->id,
				"typeFieldId" => $value->type_field_id,
				"identifier"  => $value->identifier,
				"hintMessage" => $value->hint_message,
				"isCrm"       => $value->is_crm,
				"value"       => $reqFieldValue
			]);
		}

		return $reqFieldsArray;
	}

	private function partyContactDetails($ticketId, $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$rules = [
				'phone'    => 'required|digits:10',
				'altPhone' => 'digits:10'
			];

			$messages = [
				'phone.required'  => 'Your phone number is required',
				'phone.digits'    => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'altPhone.digits' => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)'
			];

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success'  => false,
					                        'error'    => $validation['error'],
					                        "ticketId" => $ticket->id
				                        ]
				);
			}

			// if email is updated, send welcome email
			//if ($request['email'] && ($request['email'] != $ticket->email))
			//{
			//	$this->dispatch(new WelcomeEmailToCustomerJob([
			//		                                              'emailId' => $request['email'],
			//		                                              'ticket'  => $ticket
			//	                                              ]));
			//}

			//$closureDate = Carbon::createFromFormat('d/m/Y H:i', $request['closureDate'])->timestamp;

			$ticket->update([
				                "phone"                   => $request['phone'],
				                "venue_landmark"          => $request['venueLandmark'],
				                "status_id"               => ($ticket->status_id != config("evibe.ticket.status.booked")) ? config("evibe.ticket.status.followup") : $ticket->status_id,
				                // "expected_closure_date"   => $closureDate,
				                "customer_preferred_slot" => $request['popUpEnquiryTimeSlot'],
				                "team_notes"              => $request['teamNotes'],
				                "alt_phone"               => $request['altPhone']
			                ]);

			//TicketFollowups::where('ticket_id', $ticketId)->update(['is_complete' => 1]);
			//
			//TicketFollowups::create([
			//	                        "followup_type" => $request['typeFollowup'],
			//	                        "ticket_id"     => $ticketId,
			//	                        "followup_date" => Carbon::createFromFormat('d/m/Y H:i', $request['nextFollowUp'])->timestamp,
			//	                        "handler_id"    => $userId,
			//                        ]);

			$this->updateTicketHandler($ticketId, $userId);

			return response()->json(["success" => true, "ticketId" => $ticket->id]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	private function updateTicketHandler($ticketId, $userId, $statusId = null)
	{
		// @todo: remove this and implement 'updateTicketAction' function
		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$userId)
		{
			throw new CustomException('User id is received as NULL', 404);
		}

		if (!$statusId)
		{
			$statusId = $ticket->status_id ? $ticket->status_id : config('evibe.ticket.status.initiated');
		}

		$handlerId = $ticket->handler_id;

		// check if first handler
		if (!$ticket->created_handler_id)
		{
			$ticket->created_handler_id = $handlerId;
			$ticket->save();
		}

		if ($handlerId && ($handlerId == $userId))
		{
			// do nothing
		}
		else
		{
			// set permissions, if required
			$ticket->handler_id = $userId;
			$ticket->save();

			TicketUpdate::create([
				                     'ticket_id'   => $ticket->id,
				                     'status_id'   => $statusId,
				                     'handler_id'  => $userId,
				                     //'comments'    => ,
				                     'status_at'   => time(),
				                     'type_update' => config('evibe.ticket.type_update.manual')
			                     ]);
		}
	}

	public function getServiceSpecificOptions($ticketId, $serviceId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$service = TypeTicket::find($serviceId))
		{
			throw new CustomException('Service not found', 404);
		}

		try
		{
			$limit = $request["limit"];
			$offset = $request["offset"];
			$cityId = $ticket->city_id;
			$eventId = $ticket->event_id;

			if (!$eventId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly update the occasion to proceed with recommendations'
				                        ]);
			}

			if (!$cityId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly update the city to proceed with recommendations'
				                        ]);
			}

			$areasData = [];
			$areas = Area::where('city_id', $cityId)
			             ->whereNull('deleted_at')
			             ->get();

			$allAreas = [];
			foreach ($areas as $area)
			{
				$areasData[$area->id] = $area->name;
			}

			sort($areasData);//sorting based on names

			foreach ($areasData as $areaId => $areaName)
			{
				array_push($allAreas, [
					'area_id'   => $areaId,
					'area_name' => $areaName
				]);
			}

			$ticketMappings = TicketMapping::where('ticket_id', $ticket->id)
			                               ->whereNull('deleted_at')
			                               ->get();

			$lastRecommendedAt = $ticketMappings && $ticketMappings->max('recommended_at') ? $ticketMappings->max('recommended_at') : null;

			$eventId = $this->getParentEventId($ticket->event_id);
			$eventId = $this->fetchPrimaryEventId($eventId);

			// @see: event validation not required

			$oldTags = TicketServicesTags::select('type_category_tag_id')
			                             ->where('ticket_id', $ticketId)
			                             ->whereNull('deleted_at')
			                             ->pluck('type_category_tag_id')
			                             ->toArray();

			$defaultCategoryTags = config('evibe.default-category-tag');

			$defaultTagSelected = false;
			foreach ($defaultCategoryTags as $mapTypeId => $tagId)
			{
				if (in_array($tagId, $oldTags))
				{
					$defaultTagSelected = true;
					break;
				}
			}

			if ($serviceId == config("evibe.ticket.type.services"))
			{
				$serviceId = config("evibe.ticket.type.entertainments");
			}

			$query = TypeCategoryTags::select('type_category_tags.*')
			                         ->join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                         ->where(function ($query) use ($eventId) {
				                         $query->where('type_tags.type_event', $eventId)
				                               ->orWhere('type_tags.type_event', null);
			                         })
			                         ->where('type_tags.map_type_id', $serviceId);

			if (!$defaultTagSelected)
			{
				$query = $query->join('ticket_services_tags', 'ticket_services_tags.type_category_tag_id', '=', 'type_category_tags.id')
				               ->whereNull('ticket_services_tags.deleted_at')
				               ->where('ticket_services_tags.ticket_id', $ticketId);
			}

			$serviceTags = $query->distinct('type_category_tags.type_tag_id')
			                     ->pluck('type_category_tags.type_tag_id')
			                     ->toArray();

			// @todo: need to optimise tags related code (join + modal)

			$options = [];
			$optionIds = [];
			$filters = [];
			$pageId = $serviceId;
			$mapTypeId = $serviceId;
			$priceSort = $request->get('price_sort');
			switch ($serviceId)
			{
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.resort'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.lounge'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.couple-experiences'):
				case config('evibe.ticket.type.venue-deals'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.generic-package'):
					$packages = Package::select('planner_package.*')
					                   ->join('planner_package_tags', 'planner_package_tags.planner_package_id', '=', 'planner_package.id')
					                   ->where('type_ticket_id', $serviceId)
					                   ->whereIn('planner_package_tags.tile_tag_id', $serviceTags)
					                   ->where('planner_package.event_id', $eventId)
					                   ->where('planner_package.city_id', $cityId)
					                   ->whereNull('planner_package.deleted_at')
					                   ->whereNull('planner_package_tags.deleted_at')
					                   ->where('planner_package.is_live', 1)
					                   ->distinct('planner_package.id');
					if ($priceSort == 'phtl')
					{
						$packages->orderBy('price', 'desc');
					}
					else
					{
						$packages->orderBy('price');
					}

					$mapTypeId = config('evibe.ticket.type.packages');
					$active = null;
					$clearFilter = false;

					// filter:search
					$userSearch = "";
					if ($request->has('search') && trim($request->get('search')))
					{
						$clearFilter = true;
						$userSearch = trim($request->get('search'));
						$searchWords = explode(' ', $userSearch);

						$packages->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('planner_package.name', 'LIKE', "%$word%")
								            ->orWhere('planner_package.code', 'LIKE', "%$word%")
								            ->orWhere('planner_package.price', 'LIKE', "%$word%");
							}
						});
					}
					//filter: area
					$areaUrl = $request->has('area') ? $request['area'] : '';
					$selectedAreas = [];
					if ($areaUrl)
					{
						$selectedAreas = explode('_', $areaUrl);
						$packages->whereIn('planner_package.area_id', $selectedAreas);

					}

					// filter:category
					$categoryUrl = $request->has('category') ? $request['category'] : '';
					$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);

					$allTagIdsBank = $filterData['allTagIdsBank'];
					$allCategories = $filterData['allCategories'];
					$allFilterTagIds = $filterData['allFilterTagIds'];
					$hasFilter = $filterData['hasFilter'];
					$userFilterId = null;

					if ($hasFilter)
					{
						$packages->whereIn('planner_package_tags.tile_tag_id', $allFilterTagIds);
						$clearFilter = true;
						$active = $categoryUrl;
						//$userFilterId = $filterData['userFilterTag']->id;
					}

					// filter:price
					$minPrices = $packages->pluck('price');
					$maxPrices = $packages->pluck('price_max');
					$calMinPrice = $packages->pluck('price')->min();
					$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
					$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
					$priceMin = $request->get('price_min');
					$priceMax = $request->get('price_max');
					$priceMin = $priceMin ? $priceMin : $calMinPrice;
					$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
					// user has changed min / max price
					if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
					{
						$clearFilter = true;
						//$hidePriceCategories = true;
						$packages->whereBetween('planner_package.price', [$priceMin, $priceMax]);
					}

					// no price category for now

					$packages = $packages->get();
					$optionIds = $packages->pluck('id')->toArray();

					foreach ($packages as $value)
					{
						array_push($options, [
							"id"                 => $value->id,
							"name"               => $value->name,
							"code"               => $value->code,
							"url"                => config('evibe.hosts.dash') . "/packages/view/" . $value->id,
							"minPrice"           => $value->price,
							"maxPrice"           => $value->price_max,
							"priceWorth"         => $value->price_worth,
							"priceRangeInfo"     => $value->range_info,
							"pricePerExtraGuest" => $value->price_per_extra_guest,
							"profilePic"         => $value->getProfilePic(),
							"isSelected"         => 0
						]);
					}

					$filters = [
						'queryParams'   => $this->getExistingQueryParams($request),
						'active'        => $active,
						'priceMin'      => $priceMin,
						'priceMax'      => $priceMax,
						'search'        => $userSearch,
						'clearFilter'   => $clearFilter,
						'catCounts'     => $this->getFilterCategoryCounts($packages, $allTagIdsBank, $serviceTags),
						'allCategories' => $allCategories,
					];
					//sending area data only for surprise party
					if ($serviceId == config('evibe.ticket.type.couple-experiences'))
					{
						$filters['allAreas'] = $allAreas;
						$filters['activeAreas'] = $selectedAreas;
					}

					break;

				case config('evibe.ticket.type.cakes'):
					$cakes = Cake::select('cake.*')
					             ->join('cake_tags', 'cake_tags.cake_id', '=', 'cake.id')
					             ->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
					             ->join('planner', 'planner.id', '=', 'cake.provider_id')
					             ->whereIn('cake_tags.tag_id', $serviceTags)
					             ->where('cake_event.event_id', $eventId)
					             ->where('planner.city_id', $cityId)
					             ->whereNull('cake.deleted_at')
					             ->whereNull('cake_tags.deleted_at')
					             ->whereNull('cake_event.deleted_at')
					             ->where('cake.is_live', 1)
					             ->distinct('cake.id');
					if ($priceSort == 'phtl')
					{
						$cakes->orderBy('price_per_kg', 'desc');
					}
					else
					{
						$cakes->orderBy('price_per_kg');
					}

					$clearFilter = false;
					$mapTypeId = config('evibe.ticket.type.cakes');

					// Search query
					$searchQuery = $request->input('search');
					if ($searchQuery)
					{
						// @see: if 'trim' can be used...
						$searchWords = explode(' ', $searchQuery);
						$clearFilter = true;

						$cakes = $cakes->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('cake.title', 'LIKE', "%$word%")
								            ->orWhere('cake.code', 'LIKE', "%$word%");
							}
						});
					}

					// category filter
					$active = "";
					$categoryUrl = $request->has('category') ? $request['category'] : '';
					$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);

					$allTagIdsBank = $filterData['allTagIdsBank'];
					$allCategories = $filterData['allCategories'];
					$allFilterTagIds = $filterData['allFilterTagIds'];
					$hasFilter = $filterData['hasFilter'];
					$userFilterId = null;

					if ($hasFilter)
					{
						$cakes->whereIn('cake_tags.tag_id', $allFilterTagIds);
						$clearFilter = true;
						$active = $categoryUrl;
						//$userFilterId = $filterData['userFilterTag']->id;
					}

					$defaultMinPrice = $cakes->pluck('price_per_kg')->min();
					$defaultMaxPrice = $cakes->pluck('price_per_kg')->max();

					// Price filter
					$minPrice = $request->has('price_min') ? (int)$request->input('price_min') : 0;
					$maxPrice = $request->has('price_max') ? (int)$request->input('price_max') : 0;

					if ($minPrice || $maxPrice)
					{
						$cakes->where(function ($query) use ($minPrice, $maxPrice) {
							if ($minPrice && $maxPrice)
							{
								$query->whereBetween('cake.price_per_kg', [$minPrice, $maxPrice]);
							}
							elseif (!$minPrice && $maxPrice)
							{
								$query->where('cake.price_per_kg', '<=', $minPrice);
							}
							elseif ($minPrice && !$maxPrice)
							{
								$query->where('cake.price_per_kg', '>=', $minPrice);
							}
						});
					}

					$cakes = $cakes->get();
					$optionIds = $cakes->pluck('id')->toArray();

					foreach ($cakes as $value)
					{
						array_push($options, [
							"id"                 => $value->id,
							"name"               => $value->title,
							"code"               => $value->code,
							"url"                => config('evibe.hosts.dash') . "/cakes/" . $value->id . "/info",
							"minPrice"           => $value->price,
							"maxPrice"           => $value->max_price,
							"priceWorth"         => $value->price_worth,
							"pricePerKg"         => $value->price_per_kg,
							"pricePerExtraGuest" => $value->price_per_extra_guest,
							"profilePic"         => $value->getProfilePic(),
							"isSelected"         => 0
						]);
					}

					$filters = [
						'queryParams'   => $this->getExistingQueryParams($request),
						'catCounts'     => $this->getFilterCategoryCounts($cakes, $allTagIdsBank, $serviceTags),
						'priceMin'      => ($minPrice && $minPrice > 0) ? $minPrice : $defaultMinPrice,
						'priceMax'      => ($maxPrice && $maxPrice > 0 && $maxPrice >= $minPrice) ? $maxPrice : $defaultMaxPrice,
						'allCategories' => $allCategories,
						'active'        => $active,
						'clearFilter'   => $clearFilter,
						'search'        => $searchQuery,
					];
					break;

				case config('evibe.ticket.type.decors'):

					$decors = Decor::select('decor.*')
					               ->join('decor_tags', 'decor_tags.decor_id', '=', 'decor.id')
					               ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
					               ->join('planner', 'planner.id', '=', 'decor.provider_id')
					               ->whereIn('decor_tags.tag_id', $serviceTags)
					               ->where('decor_event.event_id', $eventId)
					               ->where('planner.city_id', $cityId)
					               ->whereNull('decor.deleted_at')
					               ->whereNull('decor_tags.deleted_at')
					               ->whereNull('decor_event.deleted_at')
					               ->where('decor.is_live', 1)
					               ->distinct('decor.id');
					if ($priceSort == 'phtl')
					{
						$decors->orderBy('min_price', 'desc');
					}
					else
					{
						$decors->orderBy('min_price');
					}

					$mapTypeId = config('evibe.ticket.type.decors');
					$clearFilter = false;

					// Filter by search term
					$userSearch = "";
					if ($request->has('search') && trim($request->get('search')))
					{
						$clearFilter = true;
						$userSearch = trim($request->get('search'));
						$searchWords = explode(' ', $userSearch);

						$decors->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('decor.name', 'LIKE', "%$word%")
								            ->orWhere('decor.code', 'LIKE', "%$word%");
							}
						});
					}

					// Get decors by categories (all / selected category)
					$active = "";
					$categoryUrl = $request->has('category') ? $request['category'] : '';
					$filterData = $this->getFilterData($eventId, $pageId, $categoryUrl);
					$allTagIdsBank = $filterData['allTagIdsBank'];
					$allCategories = $filterData['allCategories'];
					$allFilterTagIds = $filterData['allFilterTagIds'];
					$hasFilter = $filterData['hasFilter'];
					$userFilterId = null;

					if ($hasFilter)
					{
						$decors->whereIn('decor_tags.tag_id', $allFilterTagIds);
						$clearFilter = true;
						$active = $categoryUrl;
						//$userFilterId = $filterData['userFilterTag']->id;

					}

					// Filter by price
					$minPrices = $decors->pluck('min_price');
					$maxPrices = $decors->pluck('max_price');
					$calMinPrice = $minPrices->min();
					$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
					$priceMin = $request['price_min'];
					$priceMax = $request['price_max'];
					$priceMin = $priceMin ? $priceMin : $calMinPrice;
					$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

					// user has changed min / max price
					if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
					{
						$clearFilter = true;
						$decors->where(function ($query1) use ($priceMin, $priceMax) {
							$query1->where(function ($query11) use ($priceMin, $priceMax) {
								$query11->where('max_price', '=', 0)
								        ->where('min_price', '>=', $priceMin)
								        ->where('min_price', '<=', $priceMax);
							})->orWhere(function ($query12) use ($priceMin, $priceMax) {
								$query12->where('max_price', '!=', 0)
								        ->where('min_price', '<=', $priceMin)
								        ->where('max_price', '>=', $priceMin)
								        ->where('max_price', '<=', $priceMax);
							})->orWhere(function ($query13) use ($priceMin, $priceMax) {
								$query13->where('max_price', '!=', 0)
								        ->where('min_price', '>=', $priceMin)
								        ->where('min_price', '<=', $priceMax)
								        ->where('max_price', '>=', $priceMax);
							})->orWhere(function ($query14) use ($priceMin, $priceMax) {
								$query14->where('max_price', '!=', 0)
								        ->where('min_price', '>=', $priceMin)
								        ->where('max_price', '<=', $priceMax);
							});
						});
					}

					$decors = $decors->get();
					$optionIds = $decors->pluck('id')->toArray();

					foreach ($decors as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => config('evibe.hosts.dash') . "/decors/" . $value->id . "/info",
							"minPrice"       => $value->min_price,
							"maxPrice"       => $value->max_price,
							"priceWorth"     => $value->worth,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}

					$filters = [
						'queryParams'   => $this->getExistingQueryParams($request),
						'catCounts'     => $this->getFilterCategoryCounts($decors, $allTagIdsBank, $serviceTags),
						'priceMin'      => $priceMin,
						'priceMax'      => $priceMax,
						'allCategories' => $allCategories,
						'active'        => $active,
						'clearFilter'   => $clearFilter,
						'search'        => $userSearch,
					];
					break;

				case config('evibe.ticket.type.trends'):

					$trends = Trends::select('trending.*')
					                ->join('trend_event', 'trend_event.trend_id', '=', 'trending.id')
					                ->where('trend_event.type_event_id', $eventId)
					                ->where('trending.city_id', $cityId)
					                ->whereNull('trending.deleted_at')
					                ->whereNull('trend_event.deleted_at')
					                ->where('trending.is_live', 1)
					                ->distinct('trending.id');
					if ($priceSort == 'phtl')
					{
						$trends->orderBy('price', 'desc');
					}
					else
					{
						$trends->orderBy('price');
					}

					$clearFilter = false;
					$mapTypeId = config('evibe.ticket.type.trends');

					// Filter by search term
					$userSearch = "";
					if ($request->has('search') && trim($request->get('search')))
					{
						$clearFilter = true;
						$userSearch = trim($request->get('search'));
						$searchWords = explode(' ', $userSearch);

						$trends->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('trending.name', 'LIKE', "%$word%")
								            ->orWhere('trending.code', 'LIKE', "%$word%");
							}
						});
					}

					// filter:price
					$minPrices = $trends->pluck('price');
					$maxPrices = $trends->pluck('price_max');
					$calMinPrice = $trends->pluck('price')->min();
					$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
					$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
					$priceMin = $request->get('price_min');
					$priceMax = $request->get('price_max');
					$priceMin = $priceMin ? $priceMin : $calMinPrice;
					$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
					// user has changed min / max price
					if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
					{
						$clearFilter = true;
						$trends->whereBetween('trending.price', [$priceMin, $priceMax]);
					}

					$trends = $trends->get();
					$optionIds = $trends->pluck('id')->toArray();

					foreach ($trends as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => config('evibe.hosts.dash') . "/trends/view/" . $value->id,
							"minPrice"       => $value->price,
							"maxPrice"       => $value->price_max,
							"priceWorth"     => $value->price_worth,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}

					$filters = [
						'queryParams' => $this->getExistingQueryParams($request),
						'priceMin'    => $priceMin,
						'priceMax'    => $priceMax,
						'clearFilter' => $clearFilter,
						'search'      => $userSearch,
					];

					break;

				// @see: services == ent
				case config('evibe.ticket.type.entertainments'):
				case config('evibe.ticket.type.services'):
					$services = TypeService::select('type_service.*')
					                       ->join('service_event', 'service_event.type_service_id', '=', 'type_service.id')
					                       ->where('service_event.type_event_id', $eventId)
					                       ->where('type_service.city_id', $cityId)
					                       ->whereNull('type_service.deleted_at')
					                       ->whereNull('service_event.deleted_at')
					                       ->where('type_service.is_live', 1)
					                       ->distinct('type_service.id');
					if ($priceSort == 'phtl')
					{
						$services->orderBy('min_price', 'desc');
					}
					else
					{
						$services->orderBy('min_price');
					}

					$clearFilter = false;
					$mapTypeId = config('evibe.ticket.type.services');

					// Filter by search term
					$userSearch = "";
					if ($request->has('search') && trim($request->get('search')))
					{
						$clearFilter = true;
						$userSearch = trim($request->get('search'));
						$searchWords = explode(' ', $userSearch);

						$services->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('type_service.name', 'LIKE', "%$word%")
								            ->orWhere('type_service.code', 'LIKE', "%$word%");
							}
						});
					}

					// filter:price
					$minPrices = $services->pluck('min_price');
					$maxPrices = $services->pluck('max_price');
					$calMinPrice = $services->pluck('min_price')->min();
					$calMinPrice = $calMinPrice ? $calMinPrice : 0; // setting to 0 if $calMinPrice is null
					$calMaxPrice = ($maxPrices->max() > $minPrices->max()) ? $maxPrices->max() : $minPrices->max();
					$priceMin = $request->get('price_min');
					$priceMax = $request->get('price_max');
					$priceMin = $priceMin ? $priceMin : $calMinPrice;
					$priceMax = ($priceMax && $priceMax >= $priceMin) ? $priceMax : $calMaxPrice;
					// user has changed min / max price
					if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
					{
						$clearFilter = true;
						$services->whereBetween('type_service.min_price', [$priceMin, $priceMax]);
					}

					$services = $services->get();
					$optionIds = $services->pluck('id')->toArray();

					foreach ($services as $value)
					{
						array_push($options, [
							"id"             => $value->id,
							"name"           => $value->name,
							"code"           => $value->code,
							"url"            => config('evibe.hosts.dash') . "/services/details/" . $value->id,
							"minPrice"       => $value->min_price,
							"maxPrice"       => $value->max_price,
							"priceWorth"     => $value->worth_price,
							"priceRangeInfo" => $value->range_info,
							"profilePic"     => $value->getProfilePic(),
							"isSelected"     => 0
						]);
					}

					$filters = [
						'queryParams' => $this->getExistingQueryParams($request),
						'priceMin'    => $priceMin,
						'priceMax'    => $priceMax,
						'clearFilter' => $clearFilter,
						'search'      => $userSearch,
					];

					break;

				case config('evibe.ticket.type.venue_halls'):
				case config('evibe.ticket.type.venues'):
					$halls = VenueHall::join('venue', 'venue.id', '=', 'venue_hall.venue_id')
					                  ->join('venue_hall_event', 'venue_hall_event.venue_hall_id', '=', 'venue_hall.id')
					                  ->whereNull('venue_hall_event.deleted_at')
					                  ->where('venue_hall_event.event_id', $eventId)
					                  ->where('venue.city_id', $cityId)
					                  ->where('venue.is_live', 1)
					                  ->whereNull('venue.deleted_at')
					                  ->select('venue_hall.*')
					                  ->distinct('venue_hall.id');
					if ($priceSort == 'phtl')
					{
						$halls->orderBy('price_min_veg', 'desc');
					}
					else
					{
						$halls->orderBy('price_min_veg');
					}

					$clearFilter = false;
					$mapTypeId = config('evibe.ticket.type.venues');

					// Filter by search term
					$userSearch = "";
					if ($request->has('search') && trim($request->get('search')))
					{
						$clearFilter = true;
						$userSearch = trim($request->get('search'));
						$searchWords = explode(' ', $userSearch);

						$halls->where(function ($searchQuery) use ($searchWords) {
							foreach ($searchWords as $word)
							{
								$searchQuery->orWhere('venue_hall.name', 'LIKE', "%$word%")
								            ->orWhere('venue_hall.code', 'LIKE', "%$word%");
							}
						});
					}

					// Price filter
					$minMax = Venue::select(DB::raw('min(price_min_veg) minPriceMin, max(price_min_veg) maxPriceMin, max(price_min_nonveg) maxPriceMax, min(cap_min) calCapMin, max(cap_max) calCapMax'))
					               ->where('is_live', 1)
					               ->first();

					$calMinPrice = $minMax->minPriceMin;
					$calMaxPrice = max($minMax->maxPriceMin, $minMax->maxPriceMax);
					$priceMin = $request['price_min'];
					$priceMax = $request['price_max'];
					$priceMin = $priceMin ? $priceMin : $calMinPrice;
					$priceMax = ($priceMax && $priceMax > $priceMin) ? $priceMax : $calMaxPrice;

					// user has changed min / max price
					if ($calMinPrice != $priceMin || $calMaxPrice != $priceMax)
					{
						$clearFilter = true;
						$halls->whereBetween('venue.price_min_veg', [$priceMin, $priceMax]);
					}

					$halls = $halls->groupBy('venue_hall.id')->get();
					$optionIds = $halls->pluck('id')->toArray();

					foreach ($halls as $value)
					{
						array_push($options, [
							"id"         => $value->id,
							"name"       => $value->name,
							"code"       => $value->code,
							"url"        => config('evibe.hosts.dash') . "/venues/view/" . $value->id,
							"minPrice"   => $value->price_min_veg,
							"maxPrice"   => $value->price_max_veg,
							"priceWorth" => $value->price_worth_veg,
							"profilePic" => $value->getProfilePic(),
							"isSelected" => 0
						]);
					}

					$filters = [
						'queryParams' => $this->getExistingQueryParams($request),
						'priceMin'    => $priceMin,
						'priceMax'    => $priceMax,
						'clearFilter' => $clearFilter,
						'search'      => $userSearch,
					];
					// @todo: need to change at top and check modified categories

					break;

				default:
					break;
			}

			// change `is_selected` field based on ticket mappings
			$mappingIds = TicketMapping::where('ticket_id', $ticketId)
			                           ->whereNull('deleted_at')
			                           ->where('map_type_id', $serviceId)
			                           ->pluck('map_id')
			                           ->toArray();

			if ($mappingIds && $options)
			{
				foreach ($options as $key => $option)
				{
					if (in_array($option['id'], $mappingIds))
					{
						$options[$key]['isSelected'] = 1;
					}
				}
			}

			if (count($optionIds) && $options && (is_null($priceSort) || ($priceSort == "")))
			{
				$sortOrderIds = ProductSortOrder::where('product_type_id', $serviceId)
				                                ->where('event_id', $eventId)
				                                ->where('city_id', $cityId)
				                                ->whereNull('deleted_at')
				                                ->orderBy('score', 'DESC')
				                                ->pluck('product_id')
				                                ->toArray();

				$priorityIds = array_unique(array_merge($sortOrderIds, $optionIds));

				usort($options, function ($a, $b) use ($priorityIds) {
					return $this->sortItemsByPriority($a, $b, $priorityIds);
				});
			}

			// @todo: selected mappings
			// @todo: update handler id function for recos

			$response = [
				'success'           => true,
				'ticketId'          => $ticketId,
				'lastRecommendedAt' => $lastRecommendedAt ?: null,
				'recoUrl'           => $ticket->reco_url ?: null,
				'properties'        => [
					'id'    => $serviceId,
					'name'  => $service->name,
					'count' => count($options)
				],
				'options'           => $options,
				'filters'           => $filters,
				'priceSort'         => $priceSort
			];

			//$this->updateTicketHandler($ticketId, $userId);

			return response()->json($response);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function getServiceStartOverOptions($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			if (!$ticket->event_id)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly update the occasion to proceed with recommendations'
				                        ]);
			}

			$eventId = $this->getParentEventId($ticket->event_id);
			$eventId = $this->fetchPrimaryEventId($eventId);

			$ticketMappings = TicketMapping::where('ticket_id', $ticket->id)
			                               ->whereNull('deleted_at')
			                               ->get();

			$lastRecommendedAt = $ticketMappings && $ticketMappings->max('recommended_at') ? $ticketMappings->max('recommended_at') : null;

			// reset ticket mappings that have been created

			// get all applicable and selected category tags for the services
			$serviceTags = [];

			$typeCategoryTags = TypeCategoryTags::join('ticket_services_tags', 'ticket_services_tags.type_category_tag_id', '=', 'type_category_tags.id')
			                                    ->join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                                    ->select('type_category_tags.*', DB::raw('type_tags.name AS tag_name, type_tags.map_type_id AS map_type_id'))
			                                    ->where(function ($query) use ($eventId) {
				                                    $query->where('type_tags.type_event', $eventId)
				                                          ->orWhere('type_tags.type_event', null);
			                                    })
			                                    ->where('ticket_services_tags.ticket_id', $ticketId)
			                                    ->whereNotNull('type_tags.map_type_id')
			                                    ->whereNull('ticket_services_tags.deleted_at')
			                                    ->distinct('type_tags.id')
			                                    ->orderBy('type_tags.map_type_id')
			                                    ->get();

			if ($typeCategoryTags)
			{
				foreach ($typeCategoryTags as $value)
				{
					array_push($serviceTags, [
						'id'                => $value->type_tag_id,
						'name'              => $value->tag_name,
						'mapTypeId'         => $value->map_type_id,
						'typeCategoryTagId' => $value->id,
						'optionsCount'      => $value->count,
						'minPrice'          => $value->min_price,
						'maxPrice'          => $value->max_price,
						'description'       => $value->info
					]);
				}
			}

			$response = [
				'response'          => true,
				'ticketId'          => $ticketId,
				'lastRecommendedAt' => $lastRecommendedAt ?: null,
				'recoUrl'           => $ticket->reco_url ?: null,
				'serviceTags'       => $serviceTags
			];

			//$this->updateTicketHandler($ticketId, $userId);

			return response()->json($response);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function createTicketMapping($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		// @todo: change status and handler issues
		// @todo: need to test

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{
			$optionId = $request['optionId'];
			$optionTypeId = $request['optionTypeId'];

			if (!$optionId || !$optionTypeId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide option parameters to proceed'
				                        ]);
			}

			switch ($optionTypeId)
			{
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.resort'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.lounge'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.couple-experiences'):
				case config('evibe.ticket.type.venue-deals'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.generic-package'):
					$mapTypeId = config('evibe.ticket.type.packages');
					break;

				case config('evibe.ticket.type.cakes'):
					$mapTypeId = config('evibe.ticket.type.cakes');
					break;

				case config('evibe.ticket.type.decors'):
					$mapTypeId = config('evibe.ticket.type.decors');
					break;

				case config('evibe.ticket.type.services'):
				case config('evibe.ticket.type.entertainments'):
					$mapTypeId = config('evibe.ticket.type.services');
					break;

				case config('evibe.ticket.type.venues'):
				case config('evibe.ticket.type.venue_halls'):
					$mapTypeId = config('evibe.ticket.type.venues');
					break;

				case config('evibe.ticket.type.trends'):
					$mapTypeId = config('evibe.ticket.type.trends');
					break;

				default:
					$mapTypeId = $optionTypeId;
					break;

			}

			// @todo:check whether map_type_id or type_ticket_id
			// @see: changed to 'option_type_id' on 28 Mar 2019
			$ticketMapping = TicketMapping::where('ticket_id', $ticketId)
			                              ->where('map_id', $optionId)
			                              ->where('map_type_id', $optionTypeId)
			                              ->whereNull('deleted_at')
			                              ->first();

			if ($ticketMapping)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'The option has already been selected'
				                        ]);
			}
			else
			{
				// @todo: validate whether the mapping option exists or not
				switch ($mapTypeId)
				{
					case config('evibe.ticket.type.packages'):
						if (!$option = Package::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					case config('evibe.ticket.type.cakes'):
						if (!$option = Cake::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					case config('evibe.ticket.type.decors'):
						if (!$option = Decor::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					case config('evibe.ticket.type.services'):
						if (!$option = TypeService::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					case config('evibe.ticket.type.venues'):
						if (!$option = VenueHall::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					case config('evibe.ticket.type.trends'):
						if (!$option = Trends::find($optionId))
						{
							return response()->json([
								                        'success' => false,
								                        'error'   => 'The selected option does not exist'
							                        ]);
						}
						break;

					default:
						return response()->json([
							                        'success' => false,
							                        'error'   => 'The selected option does not exist'
						                        ]);
						break;
				}

				// create ticket mapping
				$mapping = TicketMapping::create([
					                                 'ticket_id'   => $ticketId,
					                                 'map_id'      => $optionId,
					                                 'map_type_id' => $optionTypeId,
					                                 'handler_id'  => $userId,
					                                 'created_at'  => Carbon::now(),
					                                 'updated_at'  => Carbon::now()
				                                 ]);
				if ($mapping)
				{
					return response()->json([
						                        'success'         => true,
						                        'ticketId'        => $ticketId,
						                        'ticketMappingId' => $mapping->id
					                        ]);
				}
			}

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function deleteTicketMapping($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		// @todo: change status and handler issues
		// @todo: need to test

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{

			$optionId = $request['optionId'];
			$optionTypeId = $request['optionTypeId'];

			if (!$optionId || !$optionTypeId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide option parameters to proceed'
				                        ]);
			}

			// getting the base option typeId
			switch ($optionTypeId)
			{
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.resort'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.lounge'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.couple-experiences'):
				case config('evibe.ticket.type.venue-deals'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.generic-package'):
					$mapTypeId = config('evibe.ticket.type.packages');
					break;

				case config('evibe.ticket.type.cakes'):
					$mapTypeId = config('evibe.ticket.type.cakes');
					break;

				case config('evibe.ticket.type.decors'):
					$mapTypeId = config('evibe.ticket.type.decors');
					break;

				case config('evibe.ticket.type.services'):
				case config('evibe.ticket.type.entertainments'):
					$mapTypeId = config('evibe.ticket.type.services');
					break;

				case config('evibe.ticket.type.venues'):
				case config('evibe.ticket.type.venue_halls'):
					$mapTypeId = config('evibe.ticket.type.venues');
					break;

				case config('evibe.ticket.type.trends'):
					$mapTypeId = config('evibe.ticket.type.trends');
					break;

				default:
					$mapTypeId = $optionTypeId;
					break;

			}

			$ticketMapping = TicketMapping::where('ticket_id', $ticketId)
			                              ->where('map_id', $optionId)
			                              ->where('map_type_id', $mapTypeId)
			                              ->whereNull('deleted_at')
			                              ->first();

			if (!$ticketMapping)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'The option cannot be removed as it is not selected in the first place'
				                        ]);
			}
			else
			{
				$ticketMapping->update([
					                       'updated_at' => Carbon::now(),
					                       'deleted_at' => Carbon::now(),
					                       'handler_id' => $userId
				                       ]);

				return response()->json([
					                        'success'          => true,
					                        'ticketId'         => $ticketId,
					                        'deletedMappingId' => $ticketMapping->id

				                        ]);
			}

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function previewShortlistedOptions($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		try
		{

			$ticketMappings = TicketMapping::where('ticket_id', $ticketId)
			                               ->whereNull('deleted_at')
			                               ->get();

			if (!$ticketMappings)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'No options have been selected to send to the customer as recommendations'
				                        ]);
			}

			$lastRecommendedAt = $ticketMappings->max('recommended_at') ? $ticketMappings->max('recommended_at') : null;

			$ticketTypeIds = [];
			$recommendationData = [];
			foreach ($ticketMappings as $mapping)
			{
				$mappingData = $this->getRecommendationData($mapping);
				$type = ucfirst($mappingData['type']);

				if ($type)
				{
					$recommendationData[$type][] = $mappingData;

					if (!in_array($mappingData['ticketTypeId'], $ticketTypeIds))
					{
						$ticketTypeIds[] = $mappingData['ticketTypeId'];
					}
				}

			}

			return response()->json([
				                        'success'            => true,
				                        'ticketId'           => $ticketId,
				                        'statusId'           => $ticket->status_id,
				                        'recoUrl'            => $ticket->reco_url ? $ticket->reco_url . '&isFromDash=1' : null,
				                        'lastRecommendedAt'  => $lastRecommendedAt ?: null,
				                        'recommendationData' => $recommendationData
			                        ]);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	public function sendRecommendations($ticketId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if (!$ticket = Ticket::with('handler', 'mappings')->find($ticketId))
		{
			throw new CustomException('Ticket not found', 404);
		}

		if (!$user = User::find($userId))
		{
			throw new CustomException('User not found', 404);
		}
		// @todo: be cautious of $userId

		try
		{
			$today = Carbon::now()->timestamp;
			$followUp = null;
			$ticketFollowUp = TicketFollowups::where('ticket_id', $ticketId)
			                                 ->whereNull('is_complete')
			                                 ->where('followup_date', '>=', $today)
			                                 ->first();

			if ($ticketFollowUp)
			{
				$followUp = date('Y/m/d H:i', $ticketFollowUp->followup_date);
			}
			else
			{
				$followUp = date('Y/m/d H:i', Carbon::today()->addHour(1)->timestamp);
			}

			$res = ['success' => true, "followUp" => $followUp];

			$ccAddresses = [
				// $user->username,
				// config('evibe.contact.enquiry.group')
			];

			if ($ticket->handler && $ticket->handler->id != $user->id)
			{
				// array_push($ccAddresses, $ticket->handler->username);
			}

			if ($ticket->alt_email)
			{
				array_push($ccAddresses, $ticket->alt_email); // alt email in CC
			}

			if (!count($ticket->mappings))
			{
				$res = [
					"success" => false,
					"error"   => "No mappings found, could not send recommendation email."
				];
			}
			elseif (!$ticket->event_id)
			{
				$res = [
					"success" => false,
					"error"   => "Ticket occasion not found, could not send recommendation email."
				];
			}
			else
			{
				// get event
				$event = TypeEvent::where('id', $ticket->event_id)->first();
				$eventName = $event ? ($event->meta_name ? $event->meta_name : $event->name) : "party";

				// create the link
				$hashedUrl = config('evibe.live.host') . '/recommendation' . '?tId=' . $ticket->id . '&token=' . Hash::make($ticket->id);

				// Send email
				$data = [
					'to'                => [$ticket->email],
					'name'              => ucfirst($ticket->name),
					//'mappings'          => $ticket->mappings, // @see: removed because, the data is not required for mail::send
					'partyDate'         => date("d M Y", $ticket->event_date),
					'enquiryId'         => $ticket->enquiry_id,
					'ccAddresses'       => $ccAddresses,
					'replyTo'           => [config('evibe.contact.enquiry.group')],
					'recommendationUrl' => $hashedUrl . '&ref=email',
					'handlerName'       => $ticket->handler ? $ticket->handler->name : "Team Evibe.in",
					'eventName'         => $eventName
				];

				// Send SMS
				$smsData = [
					'to'       => $ticket->phone,
					'name'     => ucfirst($ticket->name),
					'email'    => $ticket->email,
					'recoLink' => $this->getShortUrl($hashedUrl . "&ref=sms")
				];

				// validating email and phone
				$resEmail = $this->validateEmail($data['to'], $ticket, "Customer");
				if (!$resEmail['success'])
				{
					return response()->json($resEmail);
				}
				$this->dispatch(new MailRecommendationsToCustomerJob($data));

				$resPhone = $this->validatePhone($smsData['to'], $ticket, "Customer");
				if ($resPhone['success'])
				{
					$this->dispatch(new SMSRecommendationsAlertToCustomer($smsData));
				}

				// Set recommended_at timestamp
				foreach ($ticket->mappings as $mapping)
				{
					$mapping->update(['handler_id' => $userId, 'recommended_at' => date('Y-m-d H:i:s')]);
				}

				// ticket action update
				$ticketUpdate = [
					'ticket'   => $ticket,
					'userId'   => $userId,
					'statusId' => config('evibe.ticket.status.followup'),
					'comments' => config('evibe.ticket_status_message.recommendation')
				];
				$this->updateTicketAction($ticketUpdate);

				$ticket->reco_url = $data['recommendationUrl'];
				$ticket->save();

				$res["name"] = $ticket->name;
				$res["phone"] = $ticket->phone;
				$res["callingCode"] = $ticket->calling_code ?: "+91";
				$res["recoUrl"] = $this->getShortUrl($hashedUrl . "&ref=whatsapp");
			}

			return response()->json($res);

		} catch (\Exception $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}
	}

	protected function getFilterData($eventId, $pageId, $categoryUrl = '')
	{
		$allFilterTagIds = [];
		$tagsSeoTitle = "";
		$tagsSeoDesc = "";
		$filterTagName = "";
		$hasFilter = false;
		$userFilterTag = false;
		$catsData = $this->getAllValidCategories($eventId, $pageId);

		$allCategories = $catsData['cats'];
		$allTagIdsBank = $catsData['bank'];

		// style selected, join tags for further filter
		if ($categoryUrl && $categoryUrl != 'all')
		{
			$catUrls = explode(',', $categoryUrl);
			if (count($catUrls))
			{
				foreach ($catUrls as $catUrl)
				{
					// get tag id based on url
					$userFilterTag = Tags::where('url', $catUrl)->first();

					if ($userFilterTag)
					{
						$hasFilter = true;
						$filterTagId = $userFilterTag->id;

						array_push($allFilterTagIds, $filterTagId);
						//$tagsSeoTitle .= $userFilterTag->seo_title ? $userFilterTag->seo_title : "";
						//$tagsSeoDesc .= $userFilterTag->seo_desc ? $userFilterTag->seo_desc : "";
						$filterTagName = $userFilterTag->identifier ? $userFilterTag->identifier : $userFilterTag->name;

						// collect all relevant tag ids (if parent, then all child)
						if (isset($allCategories[$filterTagId]))
						{
							// is parent tag
							if (count($allCategories[$filterTagId]['child']))
							{
								$filterTagChildTags = $allCategories[$filterTagId]['child'];
								foreach ($filterTagChildTags as $key => $value)
								{
									array_push($allFilterTagIds, $key);
									//$tagsSeoTitle .= $value['seoTitle'] ? $value['seoTitle'] : "";
									//$tagsSeoDesc .= $value['seoDesc'] ? $value['seoDesc'] : "";
								}
							}
						}
					}
				}
			}

		}

		$data = [
			'allCategories'   => $allCategories,
			'allTagIdsBank'   => $allTagIdsBank,
			'allFilterTagIds' => $allFilterTagIds,
			'hasFilter'       => $hasFilter,
			'filterTagName'   => $filterTagName,
			//'tagsSeoTitle'    => $tagsSeoTitle,
			//'tagsSeoDesc'     => $tagsSeoDesc,
			'userFilterTag'   => $userFilterTag
		];

		return $data;
	}

	private function getAllValidCategories($eventId, $pageId)
	{
		$allCategories = [];
		$allTagIdsBank = [];
		$allParentTags = Tags::forEvent($eventId)
		                     ->forPage($pageId)
		                     ->whereNull('parent_id')
		                     ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
		                     ->get();

		foreach ($allParentTags as $parentTag)
		{
			$tagId = $parentTag->id;
			array_push($allTagIdsBank, $tagId);

			if (!isset($allCategories[$tagId]))
			{
				$allCategories[$tagId] = [
					'id'       => $tagId,
					'name'     => $parentTag->identifier ? $parentTag->identifier : $parentTag->name,
					'url'      => $parentTag->url,
					'count'    => 0,
					'seoTitle' => $parentTag->seo_title,
					'seoDesc'  => $parentTag->seo_desc,
					'child'    => []
				];
			}

			$childTags = Tags::where('parent_id', $tagId)
			                 ->filterable()
			                 ->orderBy(DB::raw("ISNULL(priority), priority"), 'ASC')
			                 ->get();

			foreach ($childTags as $childTag)
			{
				$childTagId = $childTag->id;
				array_push($allTagIdsBank, $childTagId);

				$allCategories[$tagId]['child'][$childTagId] = [
					'id'       => $childTagId,
					'name'     => $childTag->identifier ? $childTag->identifier : $childTag->name, // take identifier
					'url'      => $childTag->url,
					'seoTitle' => $childTag->seo_title,
					'seoDesc'  => $childTag->seo_desc,
					'count'    => 0
				];
			}
		}

		$retData = [
			'cats' => $allCategories,
			'bank' => array_unique($allTagIdsBank) // take unique tags ids
		];

		return $retData;
	}

	private function getExistingQueryParams($request)
	{
		$existingQueryParams = [];

		foreach ($request->all() as $key => $value)
		{
			if ($key != 'page')
			{
				$existingQueryParams[$key] = urldecode($value);
			}
		}

		return $existingQueryParams;
	}

	private function getFilterCategoryCounts($list, $allTags, $catIds)
	{
		$counts = [];
		$haystack = [];
		$total = 0;

		foreach ($allTags as $tag)
		{
			$counts[$tag] = 0;
		}

		// initialize
		if (is_array($catIds))
		{
			foreach ($catIds as $catId)
			{
				$counts[$catId] = 0;
				array_push($haystack, $catId);
			}
		}

		foreach ($list as $item)
		{
			$itemTags = $item->tags; // to avoid repetitive tags
			foreach ($itemTags as $itemTag)
			{
				$itemTagId = $itemTag->id; // getting from pivot table
				if (in_array($itemTagId, $haystack))
				{
					if (!array_key_exists($itemTagId, $counts))
					{
						$counts[$itemTagId] = 0;
					}
					$counts[$itemTagId] = $counts[$itemTagId] + 1;

					// increase parent tag count
					$parentId = $itemTag->parent_id;
					if ($parentId && in_array($parentId, $haystack))
					{
						$counts[$parentId] = $counts[$parentId] + 1;
					}

					$total += 1;
				}
			}
		}

		$counts['total'] = $total;

		return $counts;
	}

	// function for validating the emails
	protected function validateEmail($inputEmail, $ticket, $fieldName = null)
	{
		$email = is_array($inputEmail) ? $inputEmail[0] : $inputEmail;
		$fieldTitle = $fieldName ? $fieldName : "Sender";
		$res = ['success' => true];

		$rules = [
			'name'  => 'required',
			'email' => 'required|email'
		];

		$messages = [
			'email.required' => $fieldTitle . ' email address not found',
			'email.email'    => $fieldTitle . ' email address is not valid',
			'name.required'  => 'Please enter the customer (Ticket) name.'
		];

		$data = ['email' => $email, 'name' => $ticket->name];

		$validator = Validator::make($data, $rules, $messages);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		return $res;
	}

	// function for validating the phone number for sms
	protected function validatePhone($phone, $ticket, $fieldName = null)
	{
		$res = ['success' => true];
		$rules = [
			'name'  => 'required',
			'phone' => 'required|phone'
		];

		$fieldName = $fieldName ? $fieldName : "Sender";

		$messages = [
			'name.required'  => 'Please enter the customer (Ticket) name.',
			'phone.required' => $fieldName . ' phone number not found',
			'phone.phone'    => $fieldName . ' phone number is not valid'
		];

		$data['phone'] = $phone;
		$data['name'] = $ticket->name;

		$validator = Validator::make($data, $rules, $messages);

		if ($validator->fails())
		{
			$res = [
				'success' => false,
				'error'   => $validator->messages()->first()
			];
		}

		return $res;

	}

	protected function getRecommendationData($mapping)
	{
		$mapTypeId = $mapping->map_type_id;
		$mapId = $mapping->map_id;
		$ticket = $mapping->ticket;

		$data = [
			'ticketMappingId' => $mapping->id,
			'id'              => '',
			'name'            => '',
			'type'            => '',
			'ticketTypeId'    => '',
			'imageUrl'        => '',
			'url'             => '',
			'priceMin'        => '',
			'priceMax'        => '',
			'priceWorth'      => '',
			'code'            => '',
			'extras'          => '',
			'venueHall'       => '',
			'isSelected'      => $mapping->is_selected
		];

		// @todo: provide DASH link
		switch ($mapTypeId)
		{
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.resort'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.lounge'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.couple-experiences'):
			case config('evibe.ticket.type.venue-deals'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.generic-package'):
				$package = Package::find($mapId);

				if ($package)
				{
					$packageWithEvent = $this->getPackageCategory($package);

					$data['id'] = $package->id;
					$data['name'] = $package->name;
					$data['type'] = $packageWithEvent['type'];
					$data['ticketTypeId'] = $packageWithEvent['ticketTypeId'];
					$data['imageUrl'] = $package->getProfilePic();
					$data['url'] = $packageWithEvent['url'];
					$data['priceMin'] = $package->price;
					$data['priceMax'] = $package->price_max;
					$data['priceWorth'] = $package->getPriceWorth();
					$data['code'] = $package->code;
				}
				break;

			case config('evibe.ticket.type.venues'):
			case config('evibe.ticket.type.venue_halls'):
				$venueHall = VenueHall::with('venue')->find($mapId);
				if ($venueHall && $venueHall->venue && $venueHall->venue->is_live == 1)
				{
					$profileUrl = config('evibe.hosts.dash') . "/venues/view/" . $venueHall->id;

					$data['id'] = $venueHall->id;
					$data['name'] = $venueHall->type_id == 1 ? 'Party Space' : $venueHall->type->name;
					$data['name'] .= ' at ' . $venueHall->venue->area->name;
					$data['type'] = 'Venues';
					$data['ticketTypeId'] = config('evibe.ticket.type.venues');
					$data['imageUrl'] = $venueHall->getProfilePic();
					$data['venueHall'] = $venueHall;
					$data['url'] = $profileUrl;
					$data['code'] = $venueHall->code;
				}
				break;

			case config('evibe.ticket.type.services'):
			case config('evibe.ticket.type.entertainments'):
				$service = TypeService::find($mapId);
				if ($service)
				{
					$profileUrl = config('evibe.hosts.dash') . "/services/details/" . $service->id;

					$data['id'] = $service->id;
					$data['name'] = $service->name;
					$data['type'] = 'Entertainment + AddOn';
					$data['ticketTypeId'] = config('evibe.ticket.type.services');
					$data['imageUrl'] = $service->getProfilePic();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $service->min_price;
					$data['priceMax'] = $service->max_price;
					$data['priceWorth'] = $service->worth_price;
					$data['code'] = $service->code;
				}
				break;

			case config('evibe.ticket.type.trends'):
				$trend = Trends::find($mapId);
				if ($trend)
				{
					$data['id'] = $trend->id;
					$data['name'] = $trend->name;
					$data['type'] = 'Trends';
					$data['imageUrl'] = $trend->getProfilePic();
					$data['url'] = config('evibe.hosts.dash') . "/trends/view/" . $trend->id;
					$data['priceMin'] = $trend->price;
					$data['priceMax'] = $trend->price_max;
					$data['priceWorth'] = $trend->price_worth;
					$data['ticketTypeId'] = config('evibe.ticket.type.trends');
					$data['code'] = $trend->code;
				}
				break;

			case config('evibe.ticket.type.cakes'):
				$cake = Cake::find($mapId);
				if ($cake)
				{
					$profileUrl = config('evibe.hosts.dash') . "/cakes/" . $cake->id . "/info";

					$data['id'] = $cake->id;
					$data['name'] = $cake->title;
					$data['type'] = 'Cakes';
					$data['imageUrl'] = $cake->getProfilePic();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $cake->price;
					$data['extras'] = "/ " . $cake->min_order . " kg";
					$data['ticketTypeId'] = config('evibe.ticket.type.cakes');
					$data['code'] = $cake->code;
				}
				break;

			case config('evibe.ticket.type.decors'):
				$decor = Decor::where('is_live', 1)->find($mapId);
				if ($decor)
				{
					$profileUrl = config('evibe.hosts.dash') . "/decors/" . $decor->id . "/info";

					$data['id'] = $decor->id;
					$data['name'] = $decor->name;
					$data['type'] = 'Decors';
					$data['imageUrl'] = $decor->getProfilePic();
					$data['url'] = $profileUrl;
					$data['priceMin'] = $decor->min_price;
					$data['priceMax'] = $decor->max_price;
					$data['priceWorth'] = $decor->worth;
					$data['ticketTypeId'] = config('evibe.ticket.type.decors');
					$data['code'] = $decor->code;
				}
				break;
		}

		return $data;
	}

	private function getPackageCategory($package)
	{
		$url = config('evibe.hosts.dash') . "/packages/view/" . $package->id;
		$type = 'Packages';
		$ticketTypeId = config('evibe.ticket.type.packages');

		switch ($package->type_ticket_id)
		{
			case config('evibe.ticket.type.packages'):
				$type = 'Kids Packages';
				$ticketTypeId = config('evibe.ticket.type.packages');
				break;
			case config('evibe.ticket.type.resort'):
				$type = 'Resorts';
				$ticketTypeId = config('evibe.ticket.type.resort');
				break;
			case config('evibe.ticket.type.villa'):
				$type = 'Villas + Farms';
				$ticketTypeId = config('evibe.ticket.type.villa');
				break;
			case config('evibe.ticket.type.lounge'):
				$type = 'Lounges';
				$ticketTypeId = config('evibe.ticket.type.lounge');
				break;
			case config('evibe.ticket.type.food'):
				$type = 'Food Packages';
				$ticketTypeId = config('evibe.ticket.type.food');
				break;
			case config('evibe.ticket.type.couple-experiences'):
				$type = 'Surprise Packages';
				$ticketTypeId = config('evibe.ticket.couple-experiences');
				break;
			case config('evibe.ticket.type.venue-deals'):
				$type = 'Venue Deals';
				$ticketTypeId = config('evibe.ticket.venue-deals');
				break;
			case config('evibe.ticket.type.priests'):
				$type = 'Priest Packages';
				$ticketTypeId = config('evibe.ticket.priests');
				break;
			case config('evibe.ticket.type.tents'):
				$type = 'Tent Packages';
				$ticketTypeId = config('evibe.ticket.tents');
				break;
			case config('evibe.ticket.type.generic-package'):
				$type = 'Generic Packages';
				$ticketTypeId = config('evibe.ticket.generic-package');
				break;
		}

		return $data = [
			'type'         => $type,
			'url'          => $url,
			'ticketTypeId' => $ticketTypeId
		];
	}

	private function fetchPrimaryEventId($eventId)
	{
		// @see: grouping kids related events as kids birthday party
		$kidsBirthdayOccasions = [
			config('evibe.event.kids_birthday'),
			config('evibe.event.naming_ceremony'),
			config('evibe.event.baby_shower'),
		];

		if (in_array($eventId, $kidsBirthdayOccasions))
		{
			$eventId = config('evibe.event.kids_birthday');
		}

		return $eventId;
	}

	public function sortItemsByPriority($item1, $item2, $priority)
	{
		// @see: assuming that 'items' are in array format
		$item1Id = is_int($item1) ? $item1 : $item1['id'];
		$item2Id = is_int($item2) ? $item2 : $item2['id'];
		$search1 = array_search($item1Id, $priority);
		$search2 = array_search($item2Id, $priority);

		if ($search1 == $search2)
		{
			return 0;
		}
		elseif ($search1 > $search2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}

	public function updateCustomerInfo(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$ticketId = $request->input('ticketId');
			$customer = Ticket::where('id', $ticketId)->first();
			$customer->name = $request->input('customerName');
			$customer->venue_landmark = trim($request->input('landmark'));
			$customer->customer_gender = $request->input('customerGender');
			$response = $customer->save();

			if ($response)
			{
				return response()->json([
					                        'success' => true
				                        ]);

			}
			else
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => 'Unable to update the customer information,please try again'

				                        ]);

			}

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['success'  => false,
			                         'errorMsg' => 'some exception occuered while updating the info'
			                        ]);
		}
	}

}