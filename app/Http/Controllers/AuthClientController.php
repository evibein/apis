<?php namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller;

use App\Http\Models\AuthClient;
use Illuminate\Support\Str;
use Exception;
use App\Exceptions\CustomException;
use Carbon\Carbon;

class AuthClientController extends Controller
{
	public function getListOfAuthClients()
	{
		return response()->json(AuthClient::all()->toArrayCamel());
	}

	public function createAuthClient(Request $request)
	{
		if (!$request->json('clientName'))
		{
			throw new CustomException("Client name missing in the payload.", 400);
		}

		$authClient = new AuthClient;

		$authClient->auth_client_id = Str::random();
		$authClient->auth_client_secret = Str::random(32);
		$authClient->auth_client_name = $request->json('clientName');
		$authClient->save();

		return response()->json($authClient->toArrayCamel());

	}

	public function getASpecificClient($clientId)
	{
		if (!$authClient = AuthClient::find($clientId))
		{
			throw new CustomException("Invalid client id.", 400);
		}

		return response()->json($authClient->toArrayCamel());
	}

	public function updateASpecificClient(Request $request, $clientId)
	{
		if (!$request->json('clientName'))
		{
			throw new CustomException("Client name missing in the payload.", 400);
		}

		if (!$authClient = AuthClient::find($clientId))
		{
			throw new CustomException("Invalid client id.", 400);
		}

		$authClient->auth_client_name = $request->json('clientName');
		$authClient->save();

		return response()->json($authClient->toArrayCamel());

	}

	public function deleteASpecificClient($clientId)
	{
		if (!$authClient = AuthClient::find($clientId))
		{
			throw new CustomException("Invalid client id.", 400);
		}

		$authClient->delete();

		return response()->json(['success' => true]);
	}
}
