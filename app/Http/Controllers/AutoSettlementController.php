<?php

namespace App\Http\Controllers;

use App\Http\Models\Area;
use App\Http\Models\City;
use App\Http\Models\Planner;
use App\Http\Models\Settlements\LogBookingFinance;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\Settlements\SettlementDetails;
use App\Http\Models\Settlements\Settlements;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use App\Http\Models\User;

use App\Http\Models\Util\StarOption;
use App\Http\Models\Venue;
use App\Jobs\Mail\Finance\MailProcessSettlementAlertToPartnerJob;
use App\Jobs\Mail\Finance\MailSettlementCreationAlertToTeam;
use App\Jobs\Sms\SMSProcessSettlementAlertToPartnerJob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;

class AutoSettlementController extends BaseController
{
	public function getBookingSettlementsList($query, Request $request)
	{
		// access token
		$this->checkForAccessToken($request);

		try
		{
			// Initiate modal
			$settlementBookings = ScheduleSettlementBookings::join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
			                                                ->select('schedule_settlement_bookings.*', 'ticket_bookings.party_date_time', 'ticket_bookings.ticket_id')
			                                                ->whereNull('schedule_settlement_bookings.deleted_at')
			                                                ->whereNull('ticket_bookings.deleted_at');
			// @see: ambiguity does not exist as 'ssb' and 'tb' are having mostly different columns

			// query params
			if ($query == 'list')
			{
				// do nothing
			}
			elseif ($query == 'pending')
			{
				$settlementBookings->whereNull('halted_at');
				$settlementBookings->whereNull('settlement_done_at');
			}
			elseif ($query == 'halted')
			{
				$settlementBookings->whereNotNull('halted_at');
			}
			elseif ($query == 'completed')
			{
				$settlementBookings->whereNotNull('settlement_done_at');
			}

			// filters
			if (isset($request['partnerTypeId']) && $request['partnerTypeId'])
			{
				$settlementBookings->where('partner_type_id', $request['partnerTypeId']);
			}

			if (isset($request['partnerId']) && $request['partnerId'])
			{
				$settlementBookings->where('partner_type_id', $request['partnerId']);
			}

			if (isset($request['scheduleDate']) && $request['scheduleDate'])
			{
				// schedule date is expected to be given in date() format
				// scheduled date 12:00 AM
				$date = date('Y-m-d', strtotime($request['scheduleDate']));
				$startDate = date('Y-m-d H:i:s', strtotime($date));
				$endDate = date('Y-m-d H:i:s', strtotime($date) + (24 * 60 * 60));

				$settlementBookings->where('schedule_date', '>=', $startDate);
				$settlementBookings->where('schedule_date', '<', $endDate);
			}

			if (isset($request['minPartyDate']) && $request['minPartyDate'])
			{
				$settlementBookings->where('ticket_bookings.party_date_time', '>=', strtotime($request['minPartyDate']));
			}

			if (isset($request['maxPartyDate']) && $request['maxPartyDate'])
			{
				$settlementBookings->where('ticket_bookings.party_date_time', '<=', strtotime($request['maxPartyDate']));
			}

			$settlementBookings = $settlementBookings->get();
			$tempHandlerDetails = [];
			$tempPartnerDetails = [];
			$bookingsList = [];
			if (count($settlementBookings))
			{
				foreach ($settlementBookings as $settlementBooking)
				{
					// @todo: store these details into a temp array,
					// to look up without doing query again and again
					$handlerDetails = [];
					$partnerDetails = [];
					$ticket = Ticket::find($settlementBooking->ticket_id);

					// customer details
					$user = User::where('phone', $ticket->phone)
					            ->orWhere('username', $ticket->email)
					            ->first();

					$customerDetails = [
						'userId' => $user ? $user->id : null,
						'name'   => $user ? $user->name : $ticket->name,
						'email'  => $user ? $user->username : $ticket->email,
						'phone'  => $user ? $user->phone : $ticket->phone
					];

					// handler details
					if ($ticket->handler_id)
					{
						$handlerQuery = true;
						$handlerId = $ticket->handler_id;
						foreach ($tempHandlerDetails as $key => $tempHandler)
						{
							if ($tempHandler['handlerId'] == $handlerId)
							{
								$handlerDetails = $tempHandlerDetails[$key];
								$handlerQuery = false;
								break;
							}
						}

						if ($handlerQuery && $handler = User::find($handlerId))
						{
							$handlerDetails = [
								'handlerId' => $handler->id,
								'name'      => $handler->name,
								'email'     => $handler->username,
								'phone'     => $handler->phone,
								'roleId'    => $handler->role_id,
								'role'      => config('evibe.role.id.' . $handler->role_id)
							];

							array_push($tempHandlerDetails, $handlerDetails);
						}
					}

					// partner details
					// @todo: or should we get from role
					$partnerQuery = true;
					foreach ($tempPartnerDetails as $key => $tempPartner)
					{
						if (($settlementBooking->partner_id == $tempPartner['partnerId']) && ($settlementBooking->partner_type_id == $tempPartner['partnerTypeId']))
						{
							$partnerDetails = $tempPartnerDetails[$key];
							$partnerQuery = false;
							break;
						}
					}
					if ($partnerQuery && ($partner = $settlementBooking->bookings->provider))
					{
						$partnerDetails = [
							'partnerId'     => $partner->id,
							'name'          => $partner->name,
							'email'         => $partner->name,
							'partnerTypeId' => $settlementBooking->partner_type_id,
							'partnerType'   => config('evibe.partner_type.' . $settlementBooking->partner_type_id)
						];

						array_push($tempPartnerDetails, $partnerDetails);
					}

					$bookingLink = config('evibe.hosts.dash') . '/tickets/' . $ticket->id . '/bookings';

					array_push($bookingsList, [
						'settlementBookingId' => $settlementBooking->id,
						'ticketBookingId'     => $settlementBooking->ticket_booking_id,
						'ticketBookingLink'   => $bookingLink,
						'scheduleDate'        => $settlementBooking->schedule_date,
						'settlementAmount'    => $settlementBooking->settlement_amount,
						'haltedAt'            => $settlementBooking->halted_at,
						'haltComments'        => $settlementBooking->halt_comments,
						'partnerDetails'      => $partnerDetails,
						'handlerDetails'      => $handlerDetails,
						'customerDetails'     => $customerDetails
					]);
				}

			}

			return response()->json([
				                        'success'      => true,
				                        'bookingsList' => $bookingsList
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function createScheduledBookingSettlement($ticketBookingId, Request $request)
	{
		// access token
		$this->checkForAccessToken($request);

		try
		{
			$res = $this->validateAndCreateBookingSettlement($ticketBookingId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function validateAndCreateBookingSettlement($ticketBookingId)
	{
		$ticketBooking = TicketBooking::find($ticketBookingId);
		if (!$ticketBooking)
		{
			return $res = [
				"success" => false,
				"error"   => "Ticket booking not found"
			];
		}

		if (!$ticketBooking->is_advance_paid)
		{
			return $res = [
				"success" => false,
				"error"   => "Unable to create in booking lookup table as advance is not paid fot the booking"
			];
		}

		// get partnerId and partnerTypeId from ticket booking
		$partnerId = $ticketBooking->map_id;
		$partnerTypeId = $ticketBooking->map_type_id;

		if (!$partnerId || !$partnerTypeId)
		{
			return $res = [
				"success" => false,
				"error"   => "Partner id and partner type id are required to create scheduled settlement booking"
			];
		}

		$handlerId = $ticketBooking->ticket->handler_id;

		if (!$ticket = Ticket::find($ticketBooking->ticket_id))
		{
			return $res = [
				"success" => false,
				"error"   => "Ticket not found"
			];
		}

		if (!($ticket->status_id &&
			(($ticket->status_id == config('evibe.ticket.status.booked')) ||
				($ticket->status_id == config('evibe.ticket.status.confirmed')) ||
				($ticket->status_id == config('evibe.ticket.status.autopay')) ||
				($ticket->status_id == config('evibe.ticket.status.service_auto_pay'))))
		)
		{
			return $res = [
				"success" => false,
				"error"   => "Ticket is not in a valid state to create schedule settlement booking. Invalid status id: " . $ticket->status_id . ", TicketId: " . $ticket->id . ", TBId: " . $ticketBooking->id
			];
		}

		$settlementRes = $this->calculateBookingSettlementAmount($ticketBooking);
		$scheduleDate = $this->calculateSettlementScheduleDateTime($ticketBooking->party_date_time);

		$existingBookingSettlement = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
		                                                       ->whereNull('deleted_at')
		                                                       ->first();
		if ($existingBookingSettlement)
		{
			return $res = [
				"success" => false,
				"error"   => "Active Booking settlement already exists for the booking [Ticket Booking Id: $ticketBookingId]"
			];
		}

		$settlementAmount = isset($settlementRes['settlementAmount']) ? $settlementRes['settlementAmount'] : null;
		$scheduledBookingSettlement = ScheduleSettlementBookings::create([
			                                                                 'ticket_booking_id'   => $ticketBookingId,
			                                                                 'partner_id'          => $partnerId,
			                                                                 'partner_type_id'     => $partnerTypeId,
			                                                                 'handler_id'          => $handlerId,
			                                                                 'settlement_amount'   => $settlementAmount,
			                                                                 'booking_amount'      => isset($settlementRes['bookingAmount']) ? $settlementRes['bookingAmount'] : null,
			                                                                 'advance_amount'      => isset($settlementRes['advanceAmount']) ? $settlementRes['advanceAmount'] : null,
			                                                                 'cgst_amount'         => isset($settlementRes['cgstAmount']) ? $settlementRes['cgstAmount'] : null,
			                                                                 'sgst_amount'         => isset($settlementRes['sgstAmount']) ? $settlementRes['sgstAmount'] : null,
			                                                                 'igst_amount'         => isset($settlementRes['igstAmount']) ? $settlementRes['igstAmount'] : null,
			                                                                 'gst_amount'          => isset($settlementRes['gstAmount']) ? $settlementRes['gstAmount'] : null,
			                                                                 'evibe_service_fee'   => isset($settlementRes['evibeServiceFee']) ? $settlementRes['evibeServiceFee'] : null,
			                                                                 'partner_bore_refund' => isset($settlementRes['partnerBoreRefund']) ? $settlementRes['partnerBoreRefund'] : null,
			                                                                 'schedule_date'       => $scheduleDate,
			                                                                 'created_at'          => Carbon::now(),
			                                                                 'updated_at'          => Carbon::now()
		                                                                 ]);

		// log this
		LogBookingFinance::create([
			                          'ticket_booking_id'         => $ticketBookingId,
			                          'handler_id'                => $handlerId,
			                          'type_update'               => 'Auto',
			                          'comments'                  => 'Scheduled booking settlement has been created',
			                          'total_booking_amount'      => $ticketBooking->booking_amount,
			                          'evibe_service_fee'         => $ticketBooking->evibe_service_fee,
			                          'partner_settlement_amount' => $settlementAmount,
			                          'created_at'                => Carbon::now(),
			                          'updated_at'                => Carbon::now()
		                          ]);

		if ($scheduledBookingSettlement)
		{
			return $res = [
				'success'                      => true,
				'scheduledBookingSettlementId' => $scheduledBookingSettlement->id
			];
		}

		return $res = [
			'success' => false,
			'error'   => 'Unable to create scheduled booking settlement'
		];
	}

	public function haltScheduledBookingSettlement($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$settlementBooking = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
			                                               ->whereNull('deleted_at')
			                                               ->whereNull('halted_at')
			                                               ->whereNull('settlement_done_at')
			                                               ->first();

			if ($settlementBooking)
			{
				$ticketBooking = TicketBooking::find($settlementBooking->ticket_booking_id);

				$settlementBooking->handler_id = $handlerId;
				$settlementBooking->halted_at = Carbon::now();
				$settlementBooking->halt_comments = $request['comments'];
				$settlementBooking->updated_at = Carbon::now();
				$settlementBooking->save();

				LogBookingFinance::create([
					                          'ticket_booking_id'         => $ticketBookingId,
					                          'handler_id'                => $handlerId,
					                          'type_update'               => 'Manual',
					                          'comments'                  => 'Scheduled booking settlement has been halted',
					                          'total_booking_amount'      => $ticketBooking->booking_amount,
					                          'evibe_service_fee'         => $ticketBooking->evibe_service_fee,
					                          'partner_settlement_amount' => $ticketBooking->settlement_amount,
					                          'created_at'                => Carbon::now(),
					                          'updated_at'                => Carbon::now()
				                          ]);

				return response()->json([
					                        'success'             => true,
					                        'settlementBookingId' => $settlementBooking->id
				                        ]);
			}
			else
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'There is no valid settlement booking to halt [Ticket booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function updateScheduledBookingSettlement($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$updateParams = [
				'scheduleDate' => $request['scheduleDate'],
				'resumeHalt'   => $request['resumeHalt']
			];

			$res = $this->validateAndUpdateScheduleBookingSettlement($ticketBookingId, $updateParams);

			return response()->json($res);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function validateAndUpdateScheduleBookingSettlement($ticketBookingId, $updateParams)
	{
		$ticketBooking = TicketBooking::find($ticketBookingId);
		if (!$ticketBooking)
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to find ticket booking [Ticket booking Id: ' . $ticketBookingId . ']'
			];
		}

		if (!$ticketBooking->is_advance_paid)
		{
			return $res = [
				'success' => false,
				'error'   => 'Unable to update in booking lookup table as advance is not paid for the booking'
			];
		}

		// get partnerId and partnerTypeId from ticket booking
		$partnerId = $ticketBooking->map_id;
		$partnerTypeId = $ticketBooking->map_type_id;

		if (!$partnerId || !$partnerTypeId)
		{
			return $res = [
				"success" => false,
				"error"   => "Partner id and partner type id are required to update scheduled settlement booking"
			];
		}

		if (!$ticket = Ticket::find($ticketBooking->ticket_id))
		{
			return $res = [
				"success" => false,
				"error"   => "Ticket not found"
			];
		}

		if (!($ticket->status_id &&
			(($ticket->status_id == config('evibe.ticket.status.booked')) ||
				($ticket->status_id == config('evibe.ticket.status.confirmed')) ||
				($ticket->status_id == config('evibe.ticket.status.autopay')) ||
				($ticket->status_id == config('evibe.ticket.status.service_auto_pay'))))
		)
		{
			return $res = [
				"success" => false,
				"error"   => "Ticket is not in a valid state to update schedule settlement booking. Invalid status id: " . $ticket->status_id . ", TicketId: " . $ticket->id . ", TBId: " . $ticketBooking->id
			];
		}

		$handlerId = $ticketBooking->ticket->handler_id;

		$newScheduleDate = isset($updateParams['scheduleDate']) && $updateParams['scheduleDate'] ? $updateParams['scheduleDate'] : null;
		$resumeHalt = isset($updateParams['resumeHalt']) ? $updateParams['resumeHalt'] : 0;

		$settlementBooking = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
		                                               ->whereNull('deleted_at')
		                                               ->whereNull('settlement_done_at')
		                                               ->first();

		if (!$settlementBooking)
		{
			$res = $this->validateAndCreateBookingSettlement($ticketBookingId);

			return $res;
		}

		$settlementDetails = SettlementDetails::where('settlement_booking_id', $settlementBooking->id)->first();
		/* @todo: remove after star orders fix
		 * if ($settlementDetails)
		 * {
		 * return $res = [
		 * 'success' => false,
		 * 'error'   => 'The booking has already been included in a settlement. Hence, unable to update.'
		 * ];
		 * }
		 */

		// which is always true
		if (!$newScheduleDate)
		{
			$newScheduleDate = $this->calculateSettlementScheduleDateTime($ticketBooking->party_date_time);
		}

		$settlementRes = $this->calculateBookingSettlementAmount($ticketBooking);
		$oldSettlementAmount = $settlementBooking->settlement_amount;
		$oldScheduleDate = $settlementBooking->schedule_date;
		$oldPartnerId = $settlementBooking->partner_id;
		$oldPartnerTypeId = $settlementBooking->partner_type_id;

		$comments = 'Scheduled booking settlement has been updated.';

		$settlementAmount = isset($settlementRes['settlementAmount']) ? $settlementRes['settlementAmount'] : null;
		$settlementBooking->booking_amount = isset($settlementRes['bookingAmount']) ? $settlementRes['bookingAmount'] : null;
		$settlementBooking->advance_amount = isset($settlementRes['advanceAmount']) ? $settlementRes['advanceAmount'] : null;
		$settlementBooking->cgst_amount = isset($settlementRes['cgstAmount']) ? $settlementRes['cgstAmount'] : null;
		$settlementBooking->sgst_amount = isset($settlementRes['sgstAmount']) ? $settlementRes['sgstAmount'] : null;
		$settlementBooking->igst_amount = isset($settlementRes['igstAmount']) ? $settlementRes['igstAmount'] : null;
		$settlementBooking->gst_amount = isset($settlementRes['gstAmount']) ? $settlementRes['gstAmount'] : null;
		$settlementBooking->evibe_service_fee = isset($settlementRes['evibeServiceFee']) ? $settlementRes['evibeServiceFee'] : null;
		$settlementBooking->partner_bore_refund = isset($settlementRes['partnerBoreRefund']) ? $settlementRes['partnerBoreRefund'] : null;

		if ($settlementAmount != $oldSettlementAmount)
		{
			$comments .= ' Settlement amount has been updated from ' . $oldSettlementAmount . ' to ' . $settlementAmount . '.';
			$settlementBooking->settlement_amount = $settlementAmount;
		}
		if ($newScheduleDate && $newScheduleDate != $oldScheduleDate)
		{
			$comments .= ' Schedule date has been updated from ' . $oldScheduleDate . ' to ' . $newScheduleDate . '.';
			$settlementBooking->schedule_date = $newScheduleDate ? $newScheduleDate : $oldScheduleDate;
		}
		if ($resumeHalt)
		{
			$comments .= ' Halt has been resumed.';
			$settlementBooking->halted_at = null;
		}

		if (($partnerId != $oldPartnerId) || ($partnerTypeId != $oldPartnerTypeId))
		{
			$comments .= ' Partner has been updated.';
			$settlementBooking->partner_id = $partnerId;
			$settlementBooking->partner_type_id = $partnerTypeId;
		}

		$settlementBooking->handler_id = $handlerId;
		$settlementBooking->updated_at = Carbon::now();
		$settlementBooking->save();

		LogBookingFinance::create([
			                          'ticket_booking_id'         => $ticketBookingId,
			                          'handler_id'                => $handlerId,
			                          'type_update'               => 'Manual',
			                          'comments'                  => $comments,
			                          'total_booking_amount'      => $ticketBooking->booking_amount,
			                          'evibe_service_fee'         => $ticketBooking->evibe_service_fee,
			                          'partner_settlement_amount' => $settlementAmount,
			                          'created_at'                => Carbon::now(),
			                          'updated_at'                => Carbon::now()
		                          ]);

		return $res = [
			'success'             => true,
			'settlementBookingId' => $settlementBooking->id
		];
	}

	public function deleteScheduledBookingSettlement($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::withTrashed()->where('id', $ticketBookingId)->first())
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find deleted ticket booking'
				                        ]);
			}

			if (!$ticket = Ticket::find($ticketBooking->ticket_id))
			{
				return response()->json([
					                        "success" => false,
					                        "error"   => "Ticket not found"
				                        ]);
			}

			$settlementBooking = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
			                                               ->whereNull('deleted_at')
			                                               ->whereNull('settlement_done_at')
			                                               ->first();
			if (!$settlementBooking)
			{
				return response()->json([
					                        'success'    => true,
					                        'successMsg' => 'There is no valid settlement booking to delete.'
				                        ]);
			}

			$settlementDetails = SettlementDetails::where('settlement_booking_id', $settlementBooking->id)->first();
			if ($settlementDetails)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'The booking has already been included in a settlement. Hence, unable to delete.'
				                        ]);
			}

			$settlementBooking->handler_id = $handlerId;
			$settlementBooking->deleted_at = Carbon::now();
			$settlementBooking->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => 'Booking settlement has been deleted',
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'evibe_service_fee'    => $ticketBooking->evibe_service_fee,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			return response()->json([
				                        'success'             => true,
				                        'settlementBookingId' => $settlementBooking->id
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function createPartnerAdjustment($partnerId, $partnerTypeId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!($partnerId || $partnerTypeId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Please enter partner details to make an adjustment'
				                        ]);
			}

			$partnerCheck = $this->validatePartner($partnerId, $partnerTypeId);

			if (!($partnerCheck['success']))
			{
				return response()->json($partnerCheck);
			}
			else
			{
				$partner = $partnerCheck['partner'];
			}

			if (!$request['adjustmentAmount'])
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide adjustment amount'
				                        ]);
			}

			if (!$request['comments'])
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide adjustment reasons in comments'
				                        ]);
			}

			$ticketBooking = null;
			// settlement check
			if ($request['ticketBookingId'])
			{
				if (!$ticketBooking = TicketBooking::find($request['ticketBookingId']))
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Unable to find ticket booking [Ticket booking Id: ' . $request['ticketBookingId'] . ']'
					                        ]);
				}

				if (($ticketBooking->map_id != $partnerId) || ($ticketBooking->map_type_id != $partnerTypeId))
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Provided partner details did not match with booking partner details [Partner Id: ' . $partnerId . 'and Partner Type Id: ' . $partnerTypeId . ']'
					                        ]);
				}
				$bookingSettlement = ScheduleSettlementBookings::where('ticket_booking_id', $request['ticketBookingId'])->first();

				if (!$bookingSettlement)
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Booking settlement has not been created yet for the booking.'
					                        ]);
				}
				else
				{
					$settlementDetails = SettlementDetails::where('settlement_booking_id', $bookingSettlement->id)->first();

					if (!$settlementDetails)
					{
						return response()->json([
							                        'success' => false,
							                        'error'   => 'Settlement has not been created yet for the booking. Kindly edit in ticket booking.'
						                        ]);
					}
				}
			}

			$partnerAdjustment = PartnerAdjustments::create([
				                                                'partner_id'        => $partnerId,
				                                                'partner_type_id'   => $partnerTypeId,
				                                                'ticket_booking_id' => $request['ticketBookingId'] ? $request['ticketBookingId'] : null,
				                                                'adjustment_amount' => $request['adjustmentAmount'],
				                                                'pay_evibe'         => $request['isPayback'] ? $request['isPayback'] : 0,
				                                                'handler_id'        => $handlerId,
				                                                'comments'          => $request['comments'],
				                                                'schedule_date'     => $this->calculateSettlementScheduleDateTime(time())
			                                                ]);

			if ($partnerAdjustment && $request['ticketBookingId'])
			{
				if ($ticketBooking)
				{
					LogBookingFinance::create([
						                          'ticket_booking_id'         => $ticketBooking->id,
						                          'handler_id'                => $handlerId,
						                          'type_update'               => 'Manual',
						                          'comments'                  => 'Partner Adjustment has been created [Ticket Booking Id: ' . $ticketBooking->id . ']',
						                          'total_booking_amount'      => $ticketBooking->booking_amount,
						                          'evibe_service_fee'         => $ticketBooking->evibe_service_fee,
						                          'partner_settlement_amount' => $partnerAdjustment->adjustment_amount,
						                          'created_at'                => Carbon::now(),
						                          'updated_at'                => Carbon::now()
					                          ]);
				}
			}

			return response()->json([
				                        'success'      => true,
				                        'adjustmentId' => $partnerAdjustment->id
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function updatePartnerAdjustment($adjustmentId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$partnerAdjustment = PartnerAdjustments::where('id', $adjustmentId)
			                                       ->whereNull('adjustment_done_at')
			                                       ->first();

			if (!$partnerAdjustment)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find partner adjustment [Adjustment Id: ' . $adjustmentId . ']'
				                        ]);
			}

			// this will update the adjustment in all the settlements (unprocessed)
			$unprocessedSettlements = null;
			$settlementMappings = SettlementDetails::where('adjustment_id', $adjustmentId)
			                                       ->whereNull('deleted_at')
			                                       ->get();

			if (count($settlementMappings))
			{
				$settlementIds = $settlementMappings->pluck('settlement_id');

				$unprocessedSettlements = Settlements::whereIn('id', $settlementIds)
				                                     ->whereNull('settlement_done_at')
				                                     ->whereNull('deleted_at')
				                                     ->get();
			}

			$oldAdjustmentAmount = $partnerAdjustment->adjustment_amount;
			$oldScheduleDate = date("Y-m-d", strtotime($partnerAdjustment->schedule_date));
			$oldPayBack = $partnerAdjustment->pay_evibe ?: 0;

			$comments = 'Partner adjustment has been updated.';
			$adjustmentAmount = $request['adjustmentAmount'];
			$isPayback = $request['isPayback'];
			$scheduleDate = $request['scheduleDate'];

			if ($adjustmentAmount && ($adjustmentAmount != $oldAdjustmentAmount))
			{
				$comments .= ' Adjustment amount has been updated from ' . $oldAdjustmentAmount . ' to ' . $adjustmentAmount . '.';
				$partnerAdjustment->adjustment_amount = $adjustmentAmount;
			}

			if ($isPayback)
			{
				$comments .= ' Adjustment amount is a payback.';
				$partnerAdjustment->pay_evibe = $isPayback;
			}
			else
			{
				$partnerAdjustment->pay_evibe = 0;
			}

			if (isset($request['comments']) && $request['comments'])
			{
				$partnerAdjustment->comments = $request['comments'];
			}

			if ($scheduleDate && ($scheduleDate != $oldScheduleDate))
			{
				// @see: only thursday validation
				$day = date('w', strtotime($scheduleDate));
				if ($day == 4) // thursdays
				{
					$comments .= ' Schedule date has been updated from ' . $oldScheduleDate . ' to ' . $scheduleDate . '.';
					$partnerAdjustment->schedule_date = $scheduleDate;
				}
				else
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Schedule date should always be a Thursday'
					                        ]);
				}
			}

			$partnerAdjustment->save();

			if (count($unprocessedSettlements))
			{
				foreach ($unprocessedSettlements as $settlement)
				{
					$settlementAmount = $settlement->settlement_amount;
					if ($oldPayBack)
					{
						$settlementAmount = $settlementAmount + $oldAdjustmentAmount;
					}
					else
					{
						$settlementAmount = $settlementAmount - $oldAdjustmentAmount;
					}

					if ($isPayback)
					{
						$settlementAmount = $settlementAmount - $adjustmentAmount;
					}
					else
					{
						$settlementAmount = $settlementAmount + $adjustmentAmount;
					}

					$settlement->settlement_amount = $settlementAmount;
					$settlement->updated_at = Carbon::now();
					$settlement->save();
				}
			}

			if ($partnerAdjustment->ticket_booking_id)
			{
				LogBookingFinance::create([
					                          'ticket_booking_id'         => $partnerAdjustment->ticket_booking_id,
					                          'handler_id'                => $handlerId,
					                          'type_update'               => 'Manual',
					                          'comments'                  => $comments,
					                          'partner_adjustment_amount' => $adjustmentAmount,
					                          'created_at'                => Carbon::now(),
					                          'updated_at'                => Carbon::now()
				                          ]);
			}

			return response()->json([
				                        'success'      => true,
				                        'adjustmentId' => $partnerAdjustment->id
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function deletePartnerAdjustment($adjustmentId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$partnerAdjustment = PartnerAdjustments::where('id', $adjustmentId)
			                                       ->whereNull('adjustment_done_at')
			                                       ->first();

			if (!$partnerAdjustment)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find partner adjustment to delete'
				                        ]);
			}

			$partnerAdjustment->updated_at = Carbon::now();
			$partnerAdjustment->deleted_at = Carbon::now();
			$partnerAdjustment->save();

			// this will update the adjustment in all the settlements (unprocessed)
			$unprocessedSettlements = null;
			$settlementMappings = SettlementDetails::where('adjustment_id', $adjustmentId)
			                                       ->whereNull('deleted_at')
			                                       ->get();

			if (count($settlementMappings))
			{
				$settlementIds = $settlementMappings->pluck('settlement_id');

				$unprocessedSettlements = Settlements::whereIn('id', $settlementIds)
				                                     ->whereNull('settlement_done_at')
				                                     ->whereNull('deleted_at')
				                                     ->get();

				if ($unprocessedSettlements)
				{
					foreach ($unprocessedSettlements as $settlement)
					{
						$settlementAmount = $settlement->settlement_amount;
						$settlementDetails = SettlementDetails::where('settlement_id', $settlement->id)
						                                      ->whereNull('deleted_at')
						                                      ->get();

						if (count($settlementDetails) == 1)
						{
							// only partner adjustment is in this settlement
							$settlementMapping = $settlementDetails->first();
							$settlementMapping->updated_at = Carbon::now();
							$settlementMapping->deleted_at = Carbon::now();
							$settlementMapping->save();

							$settlement->updated_at = Carbon::now();
							$settlement->deleted_at = Carbon::now();
							$settlement->save();
						}
						elseif (count($settlementDetails) > 1)
						{
							if ($partnerAdjustment->pay_evibe)
							{
								$settlement->settlement_amount = $settlementAmount + $partnerAdjustment->adjustment_amount;
							}
							else
							{
								$settlement->settlement_amount = $settlementAmount - $partnerAdjustment->adjustment_amount;
							}
							$settlement->updated_at = Carbon::now();
							$settlement->save();
						}
						else
						{
							// count 0 - can't happen
							// do nothing
						}
					}
				}
			}

			if ($partnerAdjustment->ticket_booking_id)
			{
				LogBookingFinance::create([
					                          'ticket_booking_id'         => $partnerAdjustment->ticket_booking_id,
					                          'handler_id'                => $handlerId,
					                          'type_update'               => 'Manual',
					                          'comments'                  => 'Partner adjustment has benn deleted',
					                          'partner_adjustment_amount' => $partnerAdjustment->adjustment_amount,
					                          'created_at'                => Carbon::now(),
					                          'updated_at'                => Carbon::now()
				                          ]);
			}

			return response()->json([
				                        'success'      => true,
				                        'adjustmentId' => $partnerAdjustment->id
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function getPartnerAdjustmentsList($query, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$partnerAdjustments = PartnerAdjustments::whereNull('deleted_at')->orderBy('updated_at', 'DESC');
			$adjustmentsList = [];

			$settlementDetailsArray = SettlementDetails::join('settlements', 'settlements.id', '=', 'settlement_details.settlement_id')
			                                           ->whereNotNull('settlement_details.adjustment_id')
			                                           ->select('settlement_details.id', 'settlement_details.adjustment_id', 'settlements.settlement_reference', 'settlement_details.settlement_id')
			                                           ->whereNull('settlement_details.deleted_at')
			                                           ->whereNull('settlements.deleted_at')
			                                           ->get()->toArray();

			// fetching bookings for last 90 days to be shown in adjustment
			$ticketBookingsArray = TicketBooking::join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                    ->select('ticket_bookings.id', 'ticket_bookings.booking_id', 'ticket.name', 'ticket_bookings.ticket_id', 'ticket_bookings.map_id', 'ticket_bookings.map_type_id', 'ticket_bookings.party_date_time', 'ticket.phone')
			                                    ->whereNull('ticket.deleted_at')
			                                    ->whereNull('ticket_bookings.deleted_at')
			                                    ->orderBy('ticket_bookings.updated_at', 'DESC')
			                                    ->where('ticket_bookings.party_date_time', '>=', strtotime('today midnight') - (90 * 24 * 60 * 60))
			                                    ->get()
			                                    ->toArray();

			// get all settled adjustment ids
			$key = 'adjustment_id';
			$settledAdjustmentIds = array_map(function ($item) use ($key) {
				return $item[$key];
			}, $settlementDetailsArray);

			$productCount = null;

			if ($query == 'pending')
			{
				$partnerAdjustments->whereNull('adjustment_done_at');
				$partnerAdjustments->whereNotIn('id', $settledAdjustmentIds);
			}
			elseif ($query == 'settled')
			{
				$productCount = 50;
				$partnerAdjustments->whereNotNull('adjustment_done_at');
			}
			elseif ($query = 'processed')
			{
				$partnerAdjustments->whereNull('adjustment_done_at');
				$partnerAdjustments->whereIn('id', $settledAdjustmentIds);
			}

			// filters
			$clearFilter = false;
			$partnerTypeId = isset($request['partnerTypeId']) && $request['partnerTypeId'] ? $request['partnerTypeId'] : null;
			$partnerId = isset($request['partnerId']) && $request['partnerId'] ? $request['partnerId'] : null;
			$scheduleDate = isset($request['scheduleDate']) && $request['scheduleDate'] ? date("Y-m-d", strtotime($request['scheduleDate'])) : null;

			if (isset($partnerTypeId) && $partnerTypeId)
			{
				$clearFilter = true;
				$partnerAdjustments->where('partner_type_id', $partnerTypeId);

				if (isset($partnerId) && $partnerId)
				{
					$partnerAdjustments->where('partner_id', $partnerId);
				}
			}

			if (isset($scheduleDate) && $scheduleDate)
			{
				$clearFilter = true;
				$partnerAdjustments->where('schedule_date', '>=', $scheduleDate);
				$partnerAdjustments->where('schedule_date', '<', date("Y-m-d H:i:s", (strtotime($scheduleDate) + (24 * 60 * 60))));
			}

			$partnerAdjustments->orderBy('updated_at', 'DESC');
			if(!is_null($productCount) && $productCount > 0)
			{
				$partnerAdjustments = $partnerAdjustments->take($productCount)->get();
			}
			else
			{
				$partnerAdjustments = $partnerAdjustments->get();
			}
			$venuePartnersList = Venue::select('id', 'name', 'email')->get()->toArray();
			$plannerPartnersList = Planner::select('id', 'name', 'email')->get()->toArray();

			if (count($partnerAdjustments))
			{
				foreach ($partnerAdjustments as $partnerAdjustment)
				{
					$handlerDetails = [];
					$partnerDetails = [];

					$handler = $partnerAdjustment->handler;
					if ($handler)
					{
						$handlerDetails = [
							'id'     => $handler->id,
							'name'   => $handler->name,
							'roleId' => $handler->role_id
						];
					}

					$partner = $partnerAdjustment->partner;
					if ($partner)
					{
						$partnerDetails = [
							'partnerId'     => $partner->id,
							'partnerTypeId' => $partnerAdjustment->partner_type_id,
							'name'          => $partner->name,
							'partnerType'   => config('evibe.partner_type.' . $partnerAdjustment->partner_type_id),
							'email'         => $partner->email,
							'phone'         => $partner->phone
						];
					}

					// @todo: get from computed array
					$bookingId = null;
					$customerName = null;
					$ticketId = null;
					$partyDateTime = null;
					$isProcessed = 0;
					$settlementReference = null;
					$settlementId = null;
					foreach ($ticketBookingsArray as $ticketBooking)
					{
						if ($ticketBooking['id'] == $partnerAdjustment->ticket_booking_id)
						{
							$bookingId = $ticketBooking['booking_id'];
							$customerName = $ticketBooking['name'];
							$ticketId = $ticketBooking['ticket_id'];
							$partyDateTime = ($ticketBooking['party_date_time'] > 0) ? date('d M Y h:i A', $ticketBooking['party_date_time']) : 0;
							break;
						}
					}

					foreach ($settlementDetailsArray as $settlementDetails)
					{
						if ($settlementDetails['adjustment_id'] == $partnerAdjustment->id)
						{
							$isProcessed = 1;
							$settlementReference = $settlementDetails['settlement_reference'];
							$settlementId = $settlementDetails['settlement_id'];
							break;
						}
					}

					array_push($adjustmentsList, [
						'id'                  => $partnerAdjustment->id,
						'ticketBookingId'     => $partnerAdjustment->ticket_booking_id,
						'bookingId'           => $bookingId,
						'customerName'        => $customerName,
						'ticketId'            => $ticketId,
						'partyDateTime'       => $partyDateTime,
						'adjustmentAmount'    => $partnerAdjustment->adjustment_amount,
						'isPayback'           => $partnerAdjustment->pay_evibe,
						'isProcessed'         => $isProcessed,
						'settlementReference' => $settlementReference,
						'settlementId'        => $settlementId,
						'scheduleDate'        => date('Y-m-d', strtotime($partnerAdjustment->schedule_date)),
						'comments'            => $partnerAdjustment->comments,
						'adjustmentDoneAt'    => $partnerAdjustment->adjustment_done_at,
						'adjustmentCreatedAt' => date('d M Y H:i:s', strtotime($partnerAdjustment->created_at)),
						'adjustmentUpdatedAt' => date('d M Y H:i:s', strtotime($partnerAdjustment->updated_at)),
						'handlerDetails'      => $handlerDetails,
						'partnerDetails'      => $partnerDetails
					]);

				}
			}

			return response()->json([
				                        'success'             => true,
				                        'adjustmentsList'     => $adjustmentsList,
				                        'plannerPartnersList' => $plannerPartnersList,
				                        'venuePartnersList'   => $venuePartnersList,
				                        'ticketBookings'      => $ticketBookingsArray,
				                        'filters'             => [
					                        'partnerId'     => $partnerId,
					                        'partnerTypeId' => $partnerTypeId,
					                        'scheduleDate'  => $scheduleDate,
					                        'clearFilter'   => $clearFilter
				                        ]
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}

	}

	public function createSettlements(Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$isFromDash = isset($request['isFromDash']) ? true : false;

			// Specific start and end time are given from Dash
			if ($isFromDash)
			{
				$scheduleStart = $request['settlementStartTime'];
				$scheduleEnd = $request['settlementEndTime'];
				$partyStart = $request['partyStartTime'];
				$partyEnd = $request['partyEndTime'];
			}
			else
			{
				$scheduleStart = Carbon::now()->startOfDay()->timestamp; // today start
				$scheduleEnd = Carbon::now()->endOfDay()->timestamp; // today end

				// Monday to Sunday parties settled on Wednesday
				// just check look up table, these values are fetched only for showing purpose
				// and not for computing
				$partyStart = Carbon::now()->startOfWeek()->subWeek(1)->timestamp; // last week Monday
				$partyEnd = Carbon::now()->endOfWeek()->subWeek(1)->timestamp; // last week Sunday
			}

			// settlement scheduled from current Wednesday to Tuesday
			$scheduleStartDateTime = date("Y-m-d H:i:s", $scheduleStart);
			$scheduleEndDateTime = date("Y-m-d H:i:s", $scheduleEnd);

			// initialize
			$partnersArray = [
				config('evibe.ticket.type.planners') => [],
				config('evibe.ticket.type.venues')   => []
			];

			// fetch all scheduled booking settlements after passing all validations
			// party date last week, settlement scheduled for this week
			// status booked
			$scheduledBookingSettlements = ScheduleSettlementBookings::join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
			                                                         ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                                         ->whereNull('schedule_settlement_bookings.settlement_done_at')
			                                                         ->whereNull('schedule_settlement_bookings.halted_at')
			                                                         ->whereNotNull('schedule_settlement_bookings.schedule_date')
			                                                         ->where('schedule_settlement_bookings.schedule_date', '>=', $scheduleStartDateTime)
			                                                         ->where('schedule_settlement_bookings.schedule_date', '<=', $scheduleEndDateTime)
			                                                         ->whereNull('schedule_settlement_bookings.deleted_at')
			                                                         ->whereNull('ticket_bookings.deleted_at')
			                                                         ->whereNull('ticket.deleted_at')
			                                                         ->select('schedule_settlement_bookings.id', 'schedule_settlement_bookings.partner_id', 'schedule_settlement_bookings.partner_type_id')
			                                                         ->get();

			if (count($scheduledBookingSettlements))
			{
				foreach ($scheduledBookingSettlements as $bookingSettlement)
				{
					$partnerTypeId = $bookingSettlement->partner_type_id;
					$partnerId = $bookingSettlement->partner_id;

					// fix for artists, converting to planner
					if ($partnerTypeId == config('evibe.ticket.type.artists'))
					{
						$partnerTypeId = config('evibe.ticket.type.planners');
					}

					if (!array_key_exists($partnerTypeId, $partnersArray))
					{
						// @todo: some issue with partner type id, alert team
						continue;
					}

					if (!array_key_exists($partnerId, $partnersArray[$partnerTypeId]))
					{
						$partnersArray[$partnerTypeId][$partnerId] = [
							'settlementBookingIds' => [],
							'adjustmentIds'        => []
						];
					}

					array_push($partnersArray[$partnerTypeId][$partnerId]['settlementBookingIds'], $bookingSettlement->id);
				}
			}

			// fetch all partner adjustments
			// @see: if an adjustment is created, it can be included
			$partnerAdjustments = PartnerAdjustments::whereNull('deleted_at')
			                                        ->whereNull('adjustment_done_at')
			                                        ->whereNotNull('schedule_date')
			                                        ->get();

			if (count($partnerAdjustments))
			{
				foreach ($partnerAdjustments as $adjustment)
				{
					$partnerTypeId = $adjustment->partner_type_id;
					$partnerId = $adjustment->partner_id;

					if (!array_key_exists($partnerId, $partnersArray[$partnerTypeId]))
					{
						$partnersArray[$partnerTypeId][$partnerId] = [
							'settlementBookingIds' => [],
							'adjustmentIds'        => []
						];
					}

					array_push($partnersArray[$partnerTypeId][$partnerId]['adjustmentIds'], $adjustment->id);
				}
			}

			if (count($partnersArray))
			{
				// key = venue / planner
				// partners = complete array of each partner id
				$totalSettlements = 0;
				foreach ($partnersArray as $partnerTypeId => $partnersData)
				{
					foreach ($partnersData as $partnerId => $partnerData)
					{
						$res = $this->createPartnerSpecificSettlement($partnerId,
						                                              $partnerTypeId,
						                                              $partnerData['settlementBookingIds'],
						                                              $partnerData['adjustmentIds']);

						$totalSettlements++;

						if (isset($res['success']) && !$res['success'])
						{
							$error = "Some error occurred while creating settlement for a partner with $partnerId of type $partnerTypeId";

							if (isset($res['error']) && $res['error'])
							{
								$error .= $res['error'];
							}

							// @todo: alert tech team on the issue
							$this->sendAndSaveNonExceptionReport([
								                                     'code'      => config('evibe.error_code.create_settlement'),
								                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
								                                     'method'    => \Illuminate\Support\Facades\Request::method(),
								                                     'message'   => '[AutoSettlementController.php - ' . __LINE__ . '] ' . $error,
								                                     'exception' => '[AutoSettlementController.php - ' . __LINE__ . '] ' . $error,
								                                     'trace'     => '[Partner Id: ' . $partnerId . '] [Partner Type Id: ' . $partnerTypeId . ']',
								                                     'details'   => '[Partner Id: ' . $partnerId . '] [Partner Type Id: ' . $partnerTypeId . ']'
							                                     ]);
						}
					}
				}

				$mailData = [
					'startDate'           => date('d M Y', $partyStart),
					'endDate'             => date('d M Y', $partyEnd),
					'settlementsDashLink' => config('evibe.hosts.dash') . '/finance/settlements',
					'settlementsCount'    => $totalSettlements
				];

				$this->dispatch(new MailSettlementCreationAlertToTeam(['data' => $mailData]));

				return response()->json(['success' => true]);
			}

			return response()->json([
				                        'success'    => true,
				                        'successMsg' => 'There are no bookings or adjustments that needs to be settled.'
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function createPartnerSettlement($partnerId, $partnerTypeId, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$partnerCheck = $this->validatePartner($partnerId, $partnerTypeId);

			if (!($partnerCheck['success']))
			{
				return response()->json($partnerCheck);
			}
			else
			{
				$partner = $partnerCheck['partner'];
			}

			$res = $this->createPartnerSpecificSettlement($partnerId, $partnerTypeId);

			return response()->json($res);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	private function createPartnerSpecificSettlement($partnerId, $partnerTypeId, $bookingSettlementIds = [], $adjustmentIds = [])
	{
		$scheduledBookingSettlements = ScheduleSettlementBookings::whereIn('id', $bookingSettlementIds)
		                                                         ->get();

		$totalBookingSettlementAmount = $scheduledBookingSettlements->sum('settlement_amount');

		$partnerAdjustments = PartnerAdjustments::whereIn('id', $adjustmentIds)->get();

		$normalAdjustments = $partnerAdjustments->where('pay_evibe', 0);
		$paybackAdjustments = $partnerAdjustments->where('pay_evibe', 1);

		$totalAdjustmentsAmount = $normalAdjustments->sum('adjustment_amount') - $paybackAdjustments->sum('adjustment_amount');

		$settlementAmount = $totalBookingSettlementAmount + $totalAdjustmentsAmount;
		$bookingAmount = $scheduledBookingSettlements->sum('booking_amount');
		$advanceAmount = $scheduledBookingSettlements->sum('advance_amount');
		$cgstAmount = $scheduledBookingSettlements->sum('cgst_amount');
		$sgstAmount = $scheduledBookingSettlements->sum('sgst_amount');
		$igstAmount = $scheduledBookingSettlements->sum('igst_amount');
		$gstAmount = $scheduledBookingSettlements->sum('gst_amount');
		$evibeServiceFee = $scheduledBookingSettlements->sum('evibe_service_fee');
		$partnerBoreRefund = $scheduledBookingSettlements->sum('partner_bore_refund');

		if ($scheduledBookingSettlements || $partnerAdjustments)
		{
			$weekStart = Carbon::now()->startOfWeek();
			$weekEnd = Carbon::now()->endOfWeek();
			// if any pending settlement exists for a partner
			// if its created_at is in this week, then update that settlement rather than creating a new one
			// if any settlement created in other week exists, then it probably is a pending settlement
			$oldSettlement = Settlements::where('partner_id', $partnerId)
			                            ->where('partner_type_id', $partnerTypeId)
			                            ->whereNull('deleted_at')
			                            ->whereNull('settlement_done_at')
			                            ->where('created_at', '>=', $weekStart)
			                            ->where('created_at', '<=', $weekEnd)
			                            ->first();

			if ($oldSettlement)
			{
				// settlement update
				$oldSettlement->settlement_amount = $settlementAmount;
				$oldSettlement->booking_amount = $bookingAmount;
				$oldSettlement->advance_amount = $advanceAmount;
				$oldSettlement->cgst_amount = $cgstAmount;
				$oldSettlement->sgst_amount = $sgstAmount;
				$oldSettlement->igst_amount = $igstAmount;
				$oldSettlement->gst_amount = $gstAmount;
				$oldSettlement->evibe_service_fee = $evibeServiceFee;
				$oldSettlement->partner_bore_refund = $partnerBoreRefund;
				$oldSettlement->updated_at = Carbon::now();
				$oldSettlement->save();

				$settlement = $oldSettlement;

			}
			else
			{
				$settlement = Settlements::create([
					                                  'partner_id'          => $partnerId,
					                                  'partner_type_id'     => $partnerTypeId,
					                                  'settlement_amount'   => round($settlementAmount),
					                                  'booking_amount'      => $bookingAmount,
					                                  'advance_amount'      => $advanceAmount,
					                                  'cgst_amount'         => $cgstAmount,
					                                  'sgst_amount'         => $sgstAmount,
					                                  'igst_amount'         => $igstAmount,
					                                  'gst_amount'          => $gstAmount,
					                                  'evibe_service_fee'   => $evibeServiceFee,
					                                  'partner_bore_refund' => $partnerBoreRefund,
					                                  'created_at'          => Carbon::now(),
					                                  'updated_at'          => Carbon::now()
				                                  ]);
			}

			if ($settlement)
			{
				// booking
				if (count($scheduledBookingSettlements))
				{
					foreach ($scheduledBookingSettlements as $scheduledBookingSettlement)
					{
						SettlementDetails::updateOrCreate(
							['settlement_id' => $settlement->id, 'settlement_booking_id' => $scheduledBookingSettlement->id, 'deleted_at' => null],
							['updated_at' => Carbon::now()]
						);
					}
				}

				// @todo: what if a settlement has only a booking or adjustment and it has been removed before update?
				// remove deleted/settled lookup bookings
				$removableBookingSettlementDetails = SettlementDetails::whereNull('deleted_at')
				                                                      ->where('settlement_id', $settlement->id)
				                                                      ->whereNotNull('settlement_booking_id')
				                                                      ->whereNotIn('settlement_booking_id', $bookingSettlementIds)
				                                                      ->get();

				if (count($removableBookingSettlementDetails))
				{
					foreach ($removableBookingSettlementDetails as $removableBookingSettlementDetail)
					{
						$removableBookingSettlementDetail->updated_at = Carbon::now();
						$removableBookingSettlementDetail->deleted_at = Carbon::now();
						$removableBookingSettlementDetail->save();
					}
				}

				// adjustments
				if (count($partnerAdjustments))
				{
					foreach ($partnerAdjustments as $partnerAdjustment)
					{
						SettlementDetails::updateOrCreate(
							['settlement_id' => $settlement->id, 'adjustment_id' => $partnerAdjustment->id, 'deleted_at' => null],
							['updated_at' => Carbon::now()]
						);
					}
				}

				// remove deleted/adjusted partner adjustments
				$removablePartnerAdjustmentDetails = SettlementDetails::whereNull('deleted_at')
				                                                      ->where('settlement_id', $settlement->id)
				                                                      ->whereNotNull('adjustment_id')
				                                                      ->whereNotIn('adjustment_id', $adjustmentIds)
				                                                      ->get();
				if (count($removablePartnerAdjustmentDetails))
				{
					foreach ($removablePartnerAdjustmentDetails as $removablePartnerAdjustmentDetail)
					{
						$removablePartnerAdjustmentDetail->updated_at = Carbon::now();
						$removablePartnerAdjustmentDetail->deleted_at = Carbon::now();
						$removablePartnerAdjustmentDetail->save();
					}
				}

				return $res = [
					'success'      => true,
					'settlementId' => $settlement->id
				];
			}

			return $res = [
				'success' => false,
				'error'   => 'Some error occurred while creating settlement for the partner.'
			];
		}

		return $res = [
			'success' => false,
			'error'   => 'There are no booking settlements or adjustments to be settled to this partner'
		];
	}

	public function getSettlementsList($query, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			// based on {query} fetch settlements
			$settlements = Settlements::whereNull('deleted_at')->orderBy('updated_at', 'DESC');
			$clearFilter = false;

			if ($query == 'created' || $query == 'pending')
			{
				$settlements->whereNull('settlement_done_at');
			}
			elseif ($query == 'settled')
			{
				$settlements->whereNotNull('settlement_done_at');

				if (isset($request['minSettlementCreateDate']) && !$request['minSettlementCreateDate'] && isset($request['maxSettlementProcessDate']) && !$request['maxSettlementProcessDate'])
				{
					// @see: if no processed date is given, default date to get limited data
					$minSettlementProcessDate = Carbon::createFromTimestamp(time())->startOfDay()->startOfWeek()->subWeeks(1)->toDateTimeString();
					$maxSettlementProcessDate = Carbon::createFromTimestamp(time())->startOfDay()->startOfWeek()->toDateTimeString();

					$request['minSettlementProcessDate'] = date('Y-m-d', strtotime($minSettlementProcessDate));
					$request['maxSettlementProcessDate'] = date('Y-m-d', strtotime($maxSettlementProcessDate));
				}
			}

			if ($request['partnerTypeId'])
			{
				$clearFilter = true;
				$settlements->where('partner_type_id', $request['partnerTypeId']);

				if ($request['partnerId'])
				{
					$settlements->where('partner_id', $request['partnerId']);
				}
			}

			if ($request['minSettlementCreateDate'])
			{
				$clearFilter = true;
				$settlements->where('created_at', '>=', date('Y-m-d H:i:s', strtotime($request['minSettlementCreateDate'])));
			}

			if ($request['maxSettlementCreateDate'])
			{
				$clearFilter = true;
				$settlements->where('created_at', '<', date('Y-m-d H:i:s', strtotime($request['maxSettlementCreateDate']) + (24 * 60 * 60)));
			}

			if ($request['minSettlementProcessDate'])
			{
				$clearFilter = true;
				$settlements->where('settlement_done_at', '>=', date('Y-m-d H:i:s', strtotime($request['minSettlementProcessDate'])));
			}

			if ($request['maxSettlementProcessDate'])
			{
				$clearFilter = true;
				$settlements->where('settlement_done_at', '<', date('Y-m-d H:i:s', strtotime($request['maxSettlementProcessDate']) + (24 * 60 * 60)));
			}

			$settlements->orderBy('updated_at', 'DESC');
			$settlements = $settlements->get();

			// filter to differentiate between created and pending settlements
			// if time diff between 'created_at' and time() is > 2 days, created settlements are moved to pending
			if ($query == 'created')
			{
				$settlements = $settlements->filter(function ($item) {
					return (time() - strtotime($item->created_at)) <= 3 * 24 * 60 * 60;
				});
			}
			elseif ($query == 'pending')
			{
				$settlements = $settlements->filter(function ($item) {
					return (time() - strtotime($item->created_at)) > 3 * 24 * 60 * 60;
				});
			}

			$settlementsList = [];
			$totalSettlementAmount = 0;

			$venuePartnersList = [];
			$plannerPartnersList = [];

			if (isset($request['isPartnerDash']) && $request['isPartnerDash'])
			{
				// if the request is from partner dashboard, no need to fetch partner details
			}
			else
			{
				$venuePartnersList = Venue::select('id', 'name', 'email')->get()->toArray();
				$plannerPartnersList = Planner::select('id', 'name', 'email')->get()->toArray();
			}

			if (count($settlements))
			{
				$totalSettlementAmount = $settlements->sum('settlement_amount');
				foreach ($settlements as $settlement)
				{
					$partnerDetails = [];
					if ($partner = $settlement->partner)
					{
						$partnerDetails = [
							'partnerId'     => $settlement->partner_id,
							'partnerTypeId' => $settlement->partner_type_id,
							'partnerType'   => config('evibe.partner_type.' . $settlement->partner_type_id),
							'name'          => $partner->name,
							'email'         => $partner->email,
							'phone'         => $partner->phone,
							'dashLink'      => config('evibe.hosts.dash') . '/' . config('evibe.partner_type.dash_link.' . $settlement->partner_type_id) . '/view/' . $settlement->partner_id
						];
					}
					array_push($settlementsList, [
						'id'                  => $settlement->id,
						'settlementAmount'    => $settlement->settlement_amount,
						'settlementCreatedAt' => date('d M Y H:i:s', strtotime($settlement->created_at)),
						'settlementReference' => $settlement->settlement_reference,
						'settlementDoneAt'    => $settlement->settlement_done_at,
						'invoiceNumber'       => $settlement->invoice_number,
						'invoiceUrl'          => $settlement->invoice_url,
						'invoiceTitle'        => $settlement->invoice_title,
						'partnerDetails'      => $partnerDetails
					]);
				}
			}

			return response()->json([
				                        'success'               => true,
				                        'settlementsList'       => $settlementsList,
				                        'totalSettlementAmount' => $totalSettlementAmount,
				                        'venuePartnersList'     => $venuePartnersList,
				                        'plannerPartnersList'   => $plannerPartnersList,
				                        'filters'               => [
					                        'clearFilter'              => $clearFilter,
					                        'minSettlementCreateDate'  => $request['minSettlementCreateDate'],
					                        'maxSettlementCreateDate'  => $request['maxSettlementCreateDate'],
					                        'minSettlementProcessDate' => $request['minSettlementProcessDate'],
					                        'maxSettlementProcessDate' => $request['maxSettlementProcessDate'],
				                        ]
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function getSettlementDetails($settlementId, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$settlement = Settlements::find($settlementId);
			if (!$settlement)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find Settlement [Id: ' . $settlementId . ']'
				                        ]);
			}

			if (isset($request['isPartnerDash']) && $request['isPartnerDash'] && isset($request['partnerId']) && $request['partnerId'] && isset($request['partnerTypeId']) && $request['partnerTypeId'])
			{
				// request is from partner dashboard
				// validate whether the settlement belong to the requested partner or not
				if (($settlement->partner_id != $request['partnerId']) || ($settlement->partner_type_id != $request['partnerTypeId']))
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Sorry! you do not have access to this settlement. However, if you think this is a mistake, kindly report to ' . config('evibe.contact.business.group')
					                        ]);
				}
			}

			$settlementDetails = SettlementDetails::where('settlement_id', $settlementId)->get();

			$scheduledBookingsList = [];
			$adjustmentsList = [];
			$settlementItemsList = [];
			$settlementBookingIds = [];
			$adjustmentIds = [];
			if (count($settlementDetails))
			{
				foreach ($settlementDetails as $settlementDetail)
				{
					if ($settlementDetail->settlement_booking_id)
					{
						array_push($settlementBookingIds, $settlementDetail->settlement_booking_id);
					}
					elseif ($settlementDetail->adjustment_id)
					{
						array_push($adjustmentIds, $settlementDetail->adjustment_id);
					}
				}
			}

			$scheduledBookingSettlements = ScheduleSettlementBookings::whereIn('id', $settlementBookingIds)->get();

			if (count($scheduledBookingSettlements))
			{
				foreach ($scheduledBookingSettlements as $scheduledBookingSettlement)
				{
					if (!$booking = TicketBooking::find($scheduledBookingSettlement->ticket_booking_id))
					{
						continue;
					}
					$bookingDashLink = config('evibe.hosts.dash') . '/tickets/' . $booking->ticket_id . 'bookings';
					$handlerDetails = [];
					$ticket = $booking->ticket;
					$handler = $ticket->handler;
					$area = $ticket->area;

					if ($handler)
					{
						$handlerDetails = [
							'id'     => $handler->id,
							'name'   => $handler->name,
							'email'  => $handler->username,
							'phone'  => $handler->phone,
							'roleId' => $handler->role_id,
							'role'   => config('evibe.role.id.' . $handler->role_id)
						];
					}

					array_push($settlementItemsList, [
						'ticketBookingId'       => $scheduledBookingSettlement->ticket_booking_id,
						'bookingId'             => $booking->booking_id,
						'customerName'          => $ticket->name,
						'partyDateTime'         => date("Y-m-d H:i:s", $booking->party_date_time),
						'partyLocation'         => $area ? $area->name : null,
						'bookingAmount'         => $scheduledBookingSettlement->booking_amount,
						'advanceAmount'         => $scheduledBookingSettlement->advance_amount,
						'cgst'                  => $scheduledBookingSettlement->cgst_amount,
						'sgst'                  => $scheduledBookingSettlement->sgst_amount,
						'igst'                  => $scheduledBookingSettlement->igst_amount,
						'gst'                   => $scheduledBookingSettlement->gst_amount,
						'evibeServiceFee'       => $scheduledBookingSettlement->evibe_service_fee,
						'refundAmount'          => $booking->refund_amount,
						'cancelledAt'           => $booking->cancelled_at,
						'partnerBear'           => $scheduledBookingSettlement->partner_bore_refund,
						'settlementAmount'      => $scheduledBookingSettlement->settlement_amount,
						'idPayback'             => null, // maybe '0' or 'false'
						'adjustmentComment'     => null,
						'bookingDashLink'       => $bookingDashLink,
						'bookingSettlementLink' => $bookingDashLink,
						'adjustmentLink'        => null,
					]);

					array_push($scheduledBookingsList, [
						'ticketBookingId'  => $scheduledBookingSettlement->ticket_booking_id,
						'bookingId'        => $booking->booking_id,
						'settlementAmount' => $scheduledBookingSettlement->settlement_amount,
						'partyDateTime'    => date("Y-m-d H:i:s", $booking->party_date_time),
						'refundAmount'     => $booking->refund_amount,
						'refundComments'   => $booking->refund_comments,
						'partnerBear'      => $booking->partner_bear,
						'evibeBear'        => $booking->evibe_bear,
						'paymentReference' => $booking->payment_reference,
						'paymentGatewayId' => $booking->type_payment_gateway_id,
						'bookingDashLink'  => $bookingDashLink,
						'handlerDetails'   => $handlerDetails
					]);
				}
			}

			$partnerAdjustments = PartnerAdjustments::whereIn('id', $adjustmentIds)->get();

			if (count($partnerAdjustments))
			{
				foreach ($partnerAdjustments as $partnerAdjustment)
				{
					$handlerDetails = [];
					$handler = $partnerAdjustment->handler;
					if ($handler)
					{
						$handlerDetails = [
							'id'     => $handler->id,
							'name'   => $handler->name,
							'email'  => $handler->username,
							'phone'  => $handler->phone,
							'roleId' => $handler->role_id,
							'role'   => config('evibe.role.id.' . $handler->role_id)
						];
					}

					$bookingId = null;
					$bookingDashLink = null;
					$customerName = null;
					$partyLocation = null;
					$partyDateTime = null;
					$adjustmentLink = config('evibe.hosts.dash') . '/finance/partner-adjustments';

					if ($partnerAdjustment->adjustment_done_at)
					{
						$adjustmentLink .= '/settled';
					}

					if ($ticketBooking = TicketBooking::find($partnerAdjustment->ticket_booking_id))
					{
						$ticket = Ticket::find($partnerAdjustment->ticket_booking_id);
						$bookingId = $ticketBooking->booking_id;
						$bookingDashLink = config('evibe.hosts.dash') . '/tickets/' . $ticketBooking->ticket_id . 'bookings';
						$customerName = $ticket->name;
						$partyLocation = $ticket->area ? $ticket->area->name : null;
						$partyDateTime = $ticketBooking->party_date_time > 0 ? date("Y-m-d H:i:s", $ticketBooking->party_date_time) : null;
					}

					array_push($settlementItemsList, [
						'ticketBookingId'       => $partnerAdjustment->ticket_booking_id,
						'bookingId'             => $bookingId,
						'customerName'          => $customerName,
						'partyDateTime'         => $partyDateTime,
						'partyLocation'         => $partyLocation,
						'bookingAmount'         => null,
						'advanceAmount'         => null,
						'cgst'                  => null,
						'sgst'                  => null,
						'igst'                  => null,
						'gst'                   => null,
						'evibeServiceFee'       => null,
						'refundAmount'          => null,
						'cancelledAt'           => null,
						'partnerBear'           => null,
						'settlementAmount'      => $partnerAdjustment->adjustment_amount,
						'isPayback'             => $partnerAdjustment->pay_evibe,
						'adjustmentComment'     => $partnerAdjustment->comments,
						'bookingDashLink'       => $bookingDashLink,
						'bookingSettlementLink' => null,
						'adjustmentLink'        => $adjustmentLink
					]);

					array_push($adjustmentsList, [
						'adjustmentId'     => $partnerAdjustment->id,
						'ticketBookingId'  => $partnerAdjustment->ticket_booking_id,
						'bookingId'        => $bookingId,
						'bookingDashLink'  => $bookingDashLink,
						'adjustmentAmount' => $partnerAdjustment->adjustment_amount,
						'isPayback'        => $partnerAdjustment->pay_evibe,
						'comments'         => $partnerAdjustment->comments,
						'adjustmentDoneAt' => $partnerAdjustment->adjustment_done_at,
						'handlerDetails'   => $handlerDetails
					]);
				}
			}

			$partnerDetails = [];
			if ($settlement->partner_id && $settlement->partner_type_id && ($partner = $settlement->partner))
			{
				$partnerDetails = [
					'partnerId'     => $settlement->partner_id,
					'partnerTypeId' => $settlement->partner_type_id,
					'partnerType'   => config('evibe.partner_type.' . $settlement->partner_type_id),
					'name'          => $partner->name,
					'email'         => $partner->email,
					'phone'         => $partner->phone,
					'dashLink'      => config('evibe.hosts.dash') . '/' . config('evibe.partner_type.dash_link.' . $settlement->partner_type_id) . '/view/' . $settlement->partner_id
				];

				$user = $partner->user;
				$bankDetails = $user ? $user->bankDetails : null;

				if ($bankDetails)
				{
					$partnerDetails['accountNumber'] = $bankDetails->account_number;
					$partnerDetails['bankName'] = $bankDetails->bank_name;
					$partnerDetails['ifscCode'] = $bankDetails->ifsc_code;
				}

				$city = $partner->city;
				$partnerDetails['cityName'] = null;
				if ($city)
				{
					$partnerDetails['cityName'] = $city->name;
				}
			}

			$settlementDetailsArray = [
				'settlementId'        => $settlement->id,
				'settlementAmount'    => $settlement->settlement_amount,
				'settlementReference' => $settlement->settlement_reference,
				'settlementDoneAt'    => date("Y-m-d H:i:s", strtotime($settlement->settlement_done_at)),
				'invoiceNumber'       => $settlement->invoice_number,
				'invoiceUrl'          => $settlement->invoice_url,
				'invoiceTitle'        => $settlement->invoice_titile,
				'partnerDetails'      => $partnerDetails,
				//'scheduledBookingsList' => $scheduledBookingsList,
				//'adjustmentsList'       => $adjustmentsList,
				'settlementItemsList' => $settlementItemsList
			];

			return response()->json([
				                        'success'           => true,
				                        'settlementDetails' => $settlementDetailsArray
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function processSettlement($settlementId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$settlementReference = $request['settlementReference'];
			if (!$settlementReference)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide settlement reference for this settlement'
				                        ]);
			}

			$res = $this->processPartnerSettlement($settlementId, $settlementReference, $handlerId);

			// assuming that error message exists for every success failure
			return response()->json($res);

			//if (isset($res['success']) && $res['success'])
			//{
			//	return response()->json($res);
			//}
			//else
			//{
			//	$error = "Some error occurred while processing partner settlement";
			//	if (isset($res['error']) && $res['error'])
			//	{
			//		$error = $res['error'];
			//	}
			//
			//	return response()->json([
			//		                        'success' => false,
			//		                        'error'   => $error
			//	                        ]);
			//}

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	private function processPartnerSettlement($settlementId, $settlementReference, $handlerId)
	{
		try
		{

			$settlementDoneAt = Carbon::now();
			$settlement = Settlements::find($settlementId);
			if (!$settlement)
			{
				return [
					'success' => false,
					'error'   => 'Unable to find settlement [Settlement Id: ' . $settlementId . ']'
				];
			}

			if ($settlement->settlement_done_at)
			{
				return [
					'success' => false,
					'error'   => 'Settlement is already done for this partner'
				];
			}

			// validations
			$settlementDetails = SettlementDetails::where('settlement_id', $settlementId)->get();
			$settlementBookingIds = $settlementDetails->pluck('settlement_booking_id')->toArray();
			$partnerAdjustmentIds = $settlementDetails->pluck('adjustment_id')->toArray();

			$settlementBookingIds = array_filter($settlementBookingIds, function ($var) {
				return !is_null($var);
			});

			$partnerAdjustmentIds = array_filter($partnerAdjustmentIds, function ($var) {
				return !is_null($var);
			});

			$settlementBookings = null;
			$partnerAdjustments = null;

			if (count($settlementBookingIds))
			{
				$settlementBookings = ScheduleSettlementBookings::whereIn('id', $settlementBookingIds)->get();
			}

			if (count($partnerAdjustmentIds))
			{
				$partnerAdjustments = PartnerAdjustments::whereIn('id', $partnerAdjustmentIds)->get();
			}

			// check for all the settlement item's validity
			if (count($settlementBookings))
			{
				foreach ($settlementBookings as $settlementBooking)
				{
					if (is_null($settlementBooking->settlement_done_at))
					{
						// continue
					}
					else
					{
						return [
							'success' => false,
							'error'   => 'One of the ticket bookings included in this settlement has already been settled'
						];
					}
				}
			}

			if (count($partnerAdjustments))
			{
				foreach ($partnerAdjustments as $partnerAdjustment)
				{
					if (is_null($partnerAdjustment->adjustment_done_at))
					{
						// continue
					}
					else
					{
						return [
							'success' => false,
							'error'   => 'One of the adjustments included in this settlement has already been settled'
						];
					}
				}
			}

			// then update them
			if (count($settlementBookings))
			{
				foreach ($settlementBookings as $settlementBooking)
				{
					$settlementBooking->settlement_done_at = $settlementDoneAt;
					$settlementBooking->updated_at = Carbon::now();
					$settlementBooking->save();

					TicketBooking::where('id', $settlementBooking->ticket_booking_id)->update([
						                                                                          'settlement_done_at' => $settlementDoneAt,
						                                                                          'updated_at'         => Carbon::now()
					                                                                          ]);
				}
			}

			if (count($partnerAdjustments))
			{
				foreach ($partnerAdjustments as $partnerAdjustment)
				{
					$partnerAdjustment->adjustment_done_at = $settlementDoneAt;
					$partnerAdjustment->updated_at = Carbon::now();
					$partnerAdjustment->save();

				}
			}

			$invoiceRes = $this->generateAndSaveInvoice($settlement, $settlementBookingIds, $partnerAdjustmentIds);

			if (isset($invoiceRes['invoiceUrl']) && $invoiceRes['invoiceUrl'])
			{
				$settlement->invoice_url = $invoiceRes['invoiceUrl'];
			}

			if (isset($invoiceRes['invoiceTitle']) && $invoiceRes['invoiceTitle'])
			{
				$settlement->invoice_title = $invoiceRes['invoiceTitle'];
			}

			if (isset($invoiceRes['invoiceNumber']) && $invoiceRes['invoiceNumber'])
			{
				$settlement->invoice_number = $invoiceRes['invoiceNumber'];
			}

			// update required fields, tables
			$settlement->settlement_reference = $settlementReference;
			$settlement->settlement_done_at = $settlementDoneAt;
			$settlement->handler_id = $handlerId;
			$settlement->updated_at = Carbon::now();
			$settlement->save();

			$partner = $settlement->partner;

			$data = [
				'settlementId'                 => $settlement->id,
				'partnerName'                  => $partner->person,
				'companyName'                  => $partner->name,
				'partnerPhone'                 => $partner->phone,
				'partnerEmail'                 => $partner->email,
				'settlementAmount'             => $this->formatPrice($settlement->settlement_amount),
				'bookingsCount'                => count($settlementBookingIds),
				'settlementStartDate'          => isset($invoiceRes['settlementStartDate']) && $invoiceRes['settlementStartDate'] ? $invoiceRes['settlementStartDate'] : null,
				'settlementEndDate'            => isset($invoiceRes['settlementEndDate']) && $invoiceRes['settlementEndDate'] ? $invoiceRes['settlementEndDate'] : null,
				'invoiceNumber'                => $settlement->invoice_number,
				'invoiceUrl'                   => $settlement->invoice_url,
				'totalOrderAmount'             => isset($invoiceRes['totalOrderAmount']) && $invoiceRes['totalOrderAmount'] ? $invoiceRes['totalOrderAmount'] : null,
				'totalAdvanceAmount'           => isset($invoiceRes['totalAdvanceAmount']) && $invoiceRes['totalAdvanceAmount'] ? $invoiceRes['totalAdvanceAmount'] : null,
				'totalPartnerBoreRefund'       => isset($invoiceRes['totalPartnerBoreRefund']) && $invoiceRes['totalPartnerBoreRefund'] ? $invoiceRes['totalPartnerBoreRefund'] : null,
				'totalEvibeServiceFee'         => isset($invoiceRes['totalEvibeServiceFee']) && $invoiceRes['totalEvibeServiceFee'] ? $invoiceRes['totalEvibeServiceFee'] : null,
				'totalGST'                     => isset($invoiceRes['totalGST']) && $invoiceRes['totalGST'] ? $invoiceRes['totalGST'] : null,
				'totalBookingSettlementAmount' => isset($invoiceRes['totalBookingSettlementAmount']) && $invoiceRes['totalBookingSettlementAmount'] ? $invoiceRes['totalBookingSettlementAmount'] : null,
				'totalAdjustmentAmount'        => isset($invoiceRes['totalAdjustmentAmount']) && $invoiceRes['totalAdjustmentAmount'] ? $invoiceRes['totalAdjustmentAmount'] : null,
			];

			$ccAddress = [config('evibe.contact.accounts.group')];
			if ($partner->alt_email_1)
			{
				array_push($ccAddress, $partner->alt_email_1);
			}
			if ($partner->alt_email_2)
			{
				array_push($ccAddress, $partner->alt_email_2);
			}
			if ($partner->alt_email_3)
			{
				array_push($ccAddress, $partner->alt_email_3);
			}

			$data['ccAddress'] = $ccAddress;

			// partner dashboard link
			if ($partner && $partner->user_id)
			{
				$params = $this->getPartnerQuickLoginParams($partner->user_id);
				$iId = (isset($params['iId']) && $params['iId']) ? $params['iId'] : null;
				$eToken = (isset($params['eToken']) && $params['eToken']) ? $params['eToken'] : null;

				$partnerDashLink = config('evibe.live.host') . '/partner/settlements/' . $settlementId;
				$partnerDashLink .= '?iId=' . $iId . '&eToken=' . $eToken;

				// utm tagging
				$partnerDashEmailLink = $partnerDashLink . '&utm_source=settlement-info-quick-link&utm_campaign=partner-settlements&utm_medium=email';
				$partnerDashEmailShortLink = $this->getShortUrl($partnerDashEmailLink);

				$partnerDashSMSLink = $partnerDashLink . '&utm_source=settlement-info-quick-link&utm_campaign=partner-settlements&utm_medium=sms';
				$partnerDashSMSShortLink = $this->getShortUrl($partnerDashSMSLink);

				$data['partnerDashLink'] = $partnerDashLink;
				$data['partnerDashEmailLink'] = $partnerDashEmailShortLink;
				$data['partnerDashSMSLink'] = $partnerDashSMSShortLink;
			}

			// alert partner mail
			$this->dispatch(new MailProcessSettlementAlertToPartnerJob($data));

			// alert partner sms
			$this->dispatch(new SMSProcessSettlementAlertToPartnerJob($data));

			return [
				'success' => true
			];

		} catch (\Exception $exception)
		{
			return [
				'success' => false,
				'error'   => $exception->getMessage()
			];
		}
	}

	public function fetchSettlementsSheet($query, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$settlements = Settlements::whereNull('deleted_at');

			if ($query == 'created' || $query == 'created-bank' || $query == 'pending' || $query == 'deviations')
			{
				$settlements->whereNull('settlement_done_at');
			}
			elseif ($query == 'settled')
			{
				$settlements->whereNotNull('settlement_done_at');

				$settlementMonth = isset($request['settlementMonth']) ? $request['settlementMonth'] : null;
				if ($settlementMonth)
				{
					$monthStart = Carbon::createFromTimestamp(strtotime($settlementMonth))->startOfMonth();
					$monthEnd = Carbon::createFromTimestamp(strtotime($settlementMonth))->endOfMonth();

					$settlements->where('settlement_done_at', '>=', $monthStart);
					$settlements->where('settlement_done_at', '<=', $monthEnd);
				}
			}

			$settlements = $settlements->get();

			// filter to differentiate between created and pending settlements
			// if time diff between 'created_at' and time() is > 2 days, created settlements are moved to pending
			if ($query == 'created' || $query == "created-bank" || $query == 'deviations')
			{
				$settlements = $settlements->filter(function ($item) {
					return (time() - strtotime($item->created_at)) <= 3 * 24 * 60 * 60;
				});
			}
			elseif ($query == 'pending')
			{
				$settlements = $settlements->filter(function ($item) {
					return (time() - strtotime($item->created_at)) > 3 * 24 * 60 * 60;
				});
			}

			$settlementsList = [];
			$totalBookingAmount = 0;
			$totalAdvanceAmount = 0;
			$totalEvibeServiceFee = 0;
			$totalCGSTAmount = 0;
			$totalSGSTAmount = 0;
			$totalIGSTAmount = 0;
			$totalGSTAmount = 0;
			$totalRefundAmount = 0;
			$totalSettlementAmount = 0;

			if (count($settlements))
			{
				Log::info("sett count" . count($settlements));
				foreach ($settlements as $settlement)
				{
					$partner = $settlement->partner;
					$user = $partner->user;
					$city = $partner->city;
					$bankDetails = $user ? $user->bankDetails : null;

					$key = $settlement->partner_id . '_' . $settlement->partner_type_id;

					//$settlementsList[$key] = [
					//	'settlementId'            => $settlement->id,
					//	'partnerId'               => $settlement->partner_id,
					//	'partnerTypeId'           => $settlement->partner_type_id,
					//	'partnerName'             => $partner->person,
					//	'partnerCompanyName'      => $partner->name,
					//	'partnerCity'             => $city ? $city->name : null,
					//	'bookingAmount'           => $settlement->booking_amount,
					//	'advanceAmount'           => $settlement->advance_amount,
					//	'evibeServiceFee'         => $settlement->evibe_service_fee,
					//	'cgstAmount'              => $settlement->cgst_amount,
					//	'sgstAmount'              => $settlement->sgst_amount,
					//	'igstAmount'              => $settlement->igst_amount,
					//	'gstAmount'               => $settlement->gst_amount,
					//	'partnerBoreRefund'       => $settlement->partner_bore_refund,
					//	'settlementAmount'        => $settlement->settlement_amount,
					//	'settlementAmountDecimal' => sprintf("%.2f", $settlement->settlement_amount),
					//	'settlementDoneAt'        => $settlement->settlement_done_at,
					//	'accountNumber'           => $bankDetails ? $bankDetails->account_number : null,
					//	'accountName'             => $bankDetails ? $bankDetails->name_in_bank : null,
					//	'bankName'                => $bankDetails ? $bankDetails->bank_name : null,
					//	'ifscCode'                => $bankDetails ? $bankDetails->ifsc_code : null,
					//];

					array_push($settlementsList, [
						'settlementId'            => $settlement->id,
						'partnerId'               => $settlement->partner_id,
						'partnerTypeId'           => $settlement->partner_type_id,
						'partnerName'             => $partner->person,
						'partnerCompanyName'      => $partner->name,
						'partnerCity'             => $city ? $city->name : null,
						'bookingAmount'           => $settlement->booking_amount,
						'advanceAmount'           => $settlement->advance_amount,
						'evibeServiceFee'         => $settlement->evibe_service_fee,
						'cgstAmount'              => $settlement->cgst_amount,
						'sgstAmount'              => $settlement->sgst_amount,
						'igstAmount'              => $settlement->igst_amount,
						'gstAmount'               => $settlement->gst_amount,
						'partnerBoreRefund'       => $settlement->partner_bore_refund,
						'settlementAmount'        => $settlement->settlement_amount,
						'settlementAmountDecimal' => sprintf("%.2f", $settlement->settlement_amount),
						'settlementDoneAt'        => $settlement->settlement_done_at,
						'accountNumber'           => $bankDetails ? $bankDetails->account_number : null,
						'accountName'             => $bankDetails ? $bankDetails->name_in_bank : null,
						'bankName'                => $bankDetails ? $bankDetails->bank_name : null,
						'ifscCode'                => $bankDetails ? $bankDetails->ifsc_code : null,
						'gstin'                   => $partner ? $partner->gstin : null,
					]);

					$totalBookingAmount += $settlement->booking_amount;
					$totalAdvanceAmount += $settlement->advance_amount;
					$totalEvibeServiceFee += $settlement->evibe_service_fee;
					$totalCGSTAmount += $settlement->cgst_amount;
					$totalSGSTAmount += $settlement->sgst_amount;
					$totalIGSTAmount += $settlement->igst_amount;
					$totalGSTAmount += $settlement->gst_amount;
					$totalRefundAmount += $settlement->partner_bore_refund;
					$totalSettlementAmount += $settlement->settlement_amount;
				}
			}

			// deviations
			$deviationsList = [];
			$oldPartnerSettlementData = [];
			// disabling deviations for now
			/*
			if ($query == 'deviations')
			{
				Log::info("deviations");
				// Monday to Sunday
				$startTime = Carbon::now()->startOfWeek()->subWeeks(1)->timestamp;
				$endTime = Carbon::now()->startOfWeek()->timestamp;

				$bookings = TicketBooking::join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
				                         ->select('ticket_bookings.*')
				                         ->whereNull('ticket.deleted_at')
				                         ->whereNull('ticket_bookings.deleted_at')
				                         ->where('ticket_bookings.party_date_time', '>=', $startTime)
				                         ->where('ticket_bookings.party_date_time', '<', $endTime)
				                         ->where('ticket_bookings.is_advance_paid', 1)
				                         ->get();

				foreach ($bookings as $booking)
				{
					$vendor = $booking->provider;
					if (!$vendor)
					{
						continue;
					}

					$key = $booking->map_id . '_' . $booking->map_type_id;
					if (!isset($oldPartnerSettlementData[$key]))
					{
						$oldPartnerSettlementData[$key] = [
							'partnerId'        => $booking->map_id,
							'partnerTypeId'    => $booking->map_type_id,
							'partnerData'      => $vendor,
							'bookingAmount'    => 0,
							'settlementAmount' => 0
						];
					}

					$bookingAmount = $booking->booking_amount;
					$advanceAmount = $booking->advance_amount;
					$commissionPercentage = $vendor->commission_rate ? $vendor->commission_rate : 15;
					$commissionAmount = $bookingAmount * ($commissionPercentage / 100);
					$balanceAmount = $advanceAmount - $commissionAmount;

					$oldPartnerSettlementData[$key]['bookingAmount'] += $bookingAmount;
					$oldPartnerSettlementData[$key]['settlementAmount'] += $balanceAmount;
				}

				foreach ($oldPartnerSettlementData as $key => $oldPartnerSettlementDatum)
				{
					if (!isset($settlementsList[$key]))
					{
						// not available in new sheet
						Log::info("data unavailable in new sheet: $key");
						$deviationsList[$key] = [
							'partnerCompany' => $oldPartnerSettlementDatum['partnerData']->name,
							'deviation'      => 'Data unavailable in new settlement sheet'
						];
					}
					else
					{
						// available in new sheet
						Log::info("partner key: $key");
						Log::info("old booking: " . $oldPartnerSettlementDatum['bookingAmount']);
						Log::info("old settlement: " . $oldPartnerSettlementDatum['settlementAmount']);
						Log::info("new settlement: " . $settlementsList[$key]['bookingAmount']);
						Log::info("new settlement: " . $settlementsList[$key]['settlementAmount']);

						if ($settlementsList[$key]['bookingAmount'] == $oldPartnerSettlementDatum['bookingAmount'])
						{
							// booking amount is equal

							if ($settlementsList[$key]['settlementAmount'] == round($oldPartnerSettlementDatum['settlementAmount']))
							{
								// if settlement amount is equal, ignore
								Log::info("ignore, no wrong");
								continue;
							}
							else
							{
								// if settlement amount is not equal, these is an Adjustment
								Log::info("adjustment");
								$deviationsList[$key] = [
									'partnerCompany' => $oldPartnerSettlementDatum['partnerData']->name,
									'deviation'      => 'Partner Adjustment'
								];
							}
						}
						else
						{
							// booking amount is not equal

							if ($settlementsList[$key]['settlementAmount'] == round($oldPartnerSettlementDatum['settlementAmount']))
							{
								// if settlement amount is equal, there is Cancellation
								Log::info("cancellation");
								$deviationsList[$key] = [
									'partnerCompany' => $oldPartnerSettlementDatum['partnerData']->name,
									'deviation'      => 'Cancellation'
								];
							}
							else
							{
								// if settlement amount is not equal, these is Adjustment + Cancellation / Refund
								Log::info("A + C / R");
								$deviationsList[$key] = [
									'partnerCompany' => $oldPartnerSettlementDatum['partnerData']->name,
									'deviation'      => 'Adjustment + Cancellation / Refund'
								];
							}
						}
					}
				}

				// just in case
				foreach ($settlementsList as $key => $partnerSettlementDatum)
				{
					if (!isset($oldPartnerSettlementData[$key]))
					{
						// not available in old sheet
						Log::info("Unavailable in old sheet: $key");
						$deviationsList[$key] = [
							'partnerCompany' => $partnerSettlementDatum['partnerCompanyName'],
							'deviation'      => 'Data unavailable in old settlement sheet'
						];
					}
				}
			}
			*/

			return response()->json([
				                        'success'               => true,
				                        'settlementsList'       => $settlementsList,
				                        'deviationsList'        => $deviationsList,
				                        'totalBookingAmount'    => $totalBookingAmount,
				                        'totalAdvanceAmount'    => $totalAdvanceAmount,
				                        'totalEvibeServiceFee'  => $totalEvibeServiceFee,
				                        'totalCGSTAmount'       => $totalCGSTAmount,
				                        'totalSGSTAmount'       => $totalSGSTAmount,
				                        'totalIGSTAmount'       => $totalIGSTAmount,
				                        'totalGSTAmount'        => $totalGSTAmount,
				                        'totalRefundAmount'     => $totalRefundAmount,
				                        'totalSettlementAmount' => $totalSettlementAmount,
				                        'generatedAt'           => time()
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function getQuickSettlementsData(Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			$partnerId = (isset($request['partnerId']) && $request['partnerId']) ? $request['partnerId'] : null;
			$partnerTypeId = (isset($request['partnerTypeId']) && $request['partnerTypeId']) ? $request['partnerTypeId'] : null;

			$planners = Planner::select('id AS partner_id', DB::raw(config('evibe.ticket.type.planners') . ' as partner_type_id'), 'name', 'code')->where('is_live', 1)->get();
			$venues = Venue::select('id AS partner_id', DB::raw(config('evibe.ticket.type.venues') . ' as partner_type_id'), 'name', 'code')->where('is_live', 1)->get();

			// planners and venues cannot be empty
			$partners = $planners->toBase()->merge($venues);

			$partnerSettlementMsg = null;
			$partnerSettlementData = [];
			$partnerData = [];
			if ($partnerId && $partnerTypeId)
			{
				if (($partnerTypeId == config("evibe.ticket.type.planners")) || ($partnerTypeId == config("evibe.ticket.type.venues")))
				{
					$selectedPartner = null;
					if ($partnerTypeId == config("evibe.ticket.type.planners"))
					{
						$selectedPartner = Planner::find($partnerId);
					}
					elseif ($partnerTypeId == config("evibe.ticket.type.venues"))
					{
						$selectedPartner = Venue::find($partnerId);
					}

					if ($selectedPartner)
					{
						// valid partner

						// fetch required partner data
						$partnerCity = City::find($selectedPartner->city_id);
						$user = $selectedPartner->user;
						$bankDetails = $user ? $user->bankDetails : null;
						$partnerData = [
							'partnerId'     => $partnerId,
							'partnerTypeId' => $partnerTypeId,
							'partnerType'   => config('evibe.partner_type.' . $partnerTypeId),
							'name'          => $selectedPartner->name,
							'phone'         => $selectedPartner->phone,
							'email'         => $selectedPartner->email,
							'cityName'      => $partnerCity ? $partnerCity->name : null,
							'address'       => $selectedPartner->address,
							'zip'           => $selectedPartner->zip,
							'gstin'         => null,
							'dashLink'      => config('evibe.hosts.dash') . '/' . config('evibe.partner_type.dash_link.' . $partnerTypeId) . '/view/' . $partnerId
						];

						if ($bankDetails)
						{
							$partnerData['accountNumber'] = $bankDetails->account_number;
							$partnerData['bankName'] = $bankDetails->bank_name;
							$partnerData['ifscCode'] = $bankDetails->ifsc_code;
						}

						$qsStartLimit = "2019-01-01 00:00:00";
						// fetch all settlement bookings
						$scheduledBookingSettlements = ScheduleSettlementBookings::join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
						                                                         ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
						                                                         ->whereNull('schedule_settlement_bookings.settlement_done_at')
						                                                         ->whereNull('schedule_settlement_bookings.halted_at')
						                                                         ->whereNotNull('schedule_settlement_bookings.schedule_date')
						                                                         ->whereNull('schedule_settlement_bookings.deleted_at')
						                                                         ->whereNull('ticket_bookings.deleted_at')
						                                                         ->whereNull('ticket.deleted_at')
						                                                         ->where('ticket.created_at', '>=', $qsStartLimit)
						                                                         ->where('schedule_settlement_bookings.partner_id', $partnerId)
						                                                         ->where('schedule_settlement_bookings.partner_type_id', $partnerTypeId)
						                                                         ->select('schedule_settlement_bookings.*')
						                                                         ->get();
						// fetch all adjustments
						$partnerAdjustments = PartnerAdjustments::whereNull('deleted_at')
						                                        ->whereNull('adjustment_done_at')
						                                        ->whereNotNull('schedule_date')
						                                        ->where('partner_id', $partnerId)
						                                        ->where('partner_type_id', $partnerTypeId)
						                                        ->get();

						//$scheduledBookingsList = [];
						//$adjustmentsList = [];
						$settlementItemsList = [];
						$settlementSummary = [];
						$totalBookingSettlementAmount = 0;
						$totalAdjustmentsAmount = 0;

						if (count($scheduledBookingSettlements))
						{
							foreach ($scheduledBookingSettlements as $scheduledBookingSettlement)
							{
								if (!$booking = TicketBooking::find($scheduledBookingSettlement->ticket_booking_id))
								{
									continue;
								}
								$bookingDashLink = config('evibe.hosts.dash') . '/tickets/' . $booking->ticket_id . 'bookings';
								$handlerDetails = [];
								$ticket = $booking->ticket;
								$handler = $ticket->handler;
								$area = $ticket->area;

								if ($handler)
								{
									$handlerDetails = [
										'id'     => $handler->id,
										'name'   => $handler->name,
										'email'  => $handler->username,
										'phone'  => $handler->phone,
										'roleId' => $handler->role_id,
										'role'   => config('evibe.role.id.' . $handler->role_id)
									];
								}

								array_push($settlementItemsList, [
									'ticketBookingId'       => $scheduledBookingSettlement->ticket_booking_id,
									'bookingId'             => $booking->booking_id,
									'customerName'          => $ticket->name,
									'partyDateTime'         => date("Y-m-d H:i:s", $booking->party_date_time),
									'partyLocation'         => $area ? $area->name : null,
									'bookingAmount'         => $scheduledBookingSettlement->booking_amount,
									'advanceAmount'         => $scheduledBookingSettlement->advance_amount,
									'cgst'                  => $scheduledBookingSettlement->cgst_amount,
									'sgst'                  => $scheduledBookingSettlement->sgst_amount,
									'igst'                  => $scheduledBookingSettlement->igst_amount,
									'gst'                   => $scheduledBookingSettlement->gst_amount,
									'evibeServiceFee'       => $scheduledBookingSettlement->evibe_service_fee,
									'refundAmount'          => $booking->refund_amount,
									'cancelledAt'           => $booking->cancelled_at,
									'partnerBear'           => $scheduledBookingSettlement->partner_bore_refund,
									'settlementAmount'      => $scheduledBookingSettlement->settlement_amount,
									'idPayback'             => null, // maybe '0' or 'false'
									'adjustmentComment'     => null,
									'bookingDashLink'       => $bookingDashLink,
									'bookingSettlementLink' => $bookingDashLink,
									'adjustmentLink'        => null,
								]);
							}

							$totalBookingSettlementAmount = $scheduledBookingSettlements->sum('settlement_amount');

							$bookingAmount = $scheduledBookingSettlements->sum('booking_amount');
							$advanceAmount = $scheduledBookingSettlements->sum('advance_amount');
							$cgstAmount = $scheduledBookingSettlements->sum('cgst_amount');
							$sgstAmount = $scheduledBookingSettlements->sum('sgst_amount');
							$igstAmount = $scheduledBookingSettlements->sum('igst_amount');
							$gstAmount = $scheduledBookingSettlements->sum('gst_amount');
							$evibeServiceFee = $scheduledBookingSettlements->sum('evibe_service_fee');
							$partnerBoreRefund = $scheduledBookingSettlements->sum('partner_bore_refund');

							$settlementSummary = [
								'bookingAmount'     => $bookingAmount,
								'advanceAmount'     => $advanceAmount,
								'cgstAmount'        => $cgstAmount,
								'sgstAmount'        => $sgstAmount,
								'igstAmount'        => $igstAmount,
								'gstAmount'         => $gstAmount,
								'evibeServiceFee'   => $evibeServiceFee,
								'partnerBoreRefund' => $partnerBoreRefund,
							];
						}

						if (count($partnerAdjustments))
						{
							foreach ($partnerAdjustments as $partnerAdjustment)
							{
								$handlerDetails = [];
								$handler = $partnerAdjustment->handler;
								if ($handler)
								{
									$handlerDetails = [
										'id'     => $handler->id,
										'name'   => $handler->name,
										'email'  => $handler->username,
										'phone'  => $handler->phone,
										'roleId' => $handler->role_id,
										'role'   => config('evibe.role.id.' . $handler->role_id)
									];
								}

								$bookingId = null;
								$bookingDashLink = null;
								$customerName = null;
								$partyLocation = null;
								$partyDateTime = null;
								$adjustmentLink = config('evibe.hosts.dash') . '/finance/partner-adjustments';

								if ($partnerAdjustment->adjustment_done_at)
								{
									$adjustmentLink .= '/settled';
								}

								if ($ticketBooking = TicketBooking::find($partnerAdjustment->ticket_booking_id))
								{
									$ticket = Ticket::find($partnerAdjustment->ticket_booking_id);
									$bookingId = $ticketBooking->booking_id;
									$bookingDashLink = config('evibe.hosts.dash') . '/tickets/' . $ticketBooking->ticket_id . 'bookings';
									$customerName = $ticket->name;
									$partyLocation = $ticket->area ? $ticket->area->name : null;
									$partyDateTime = $ticketBooking->party_date_time > 0 ? date("Y-m-d H:i:s", $ticketBooking->party_date_time) : null;
								}

								array_push($settlementItemsList, [
									'ticketBookingId'       => $partnerAdjustment->ticket_booking_id,
									'bookingId'             => $bookingId,
									'customerName'          => $customerName,
									'partyDateTime'         => $partyDateTime,
									'partyLocation'         => $partyLocation,
									'bookingAmount'         => null,
									'advanceAmount'         => null,
									'cgst'                  => null,
									'sgst'                  => null,
									'igst'                  => null,
									'gst'                   => null,
									'evibeServiceFee'       => null,
									'refundAmount'          => null,
									'cancelledAt'           => null,
									'partnerBear'           => null,
									'settlementAmount'      => $partnerAdjustment->adjustment_amount,
									'isPayback'             => $partnerAdjustment->pay_evibe,
									'adjustmentComment'     => $partnerAdjustment->comments,
									'bookingDashLink'       => $bookingDashLink,
									'bookingSettlementLink' => null,
									'adjustmentLink'        => $adjustmentLink
								]);
							}

							$normalAdjustments = $partnerAdjustments->where('pay_evibe', 0);
							$paybackAdjustments = $partnerAdjustments->where('pay_evibe', 1);

							$totalAdjustmentsAmount = $normalAdjustments->sum('adjustment_amount') - $paybackAdjustments->sum('adjustment_amount');
						}

						$settlementSummary['settlementAmount'] = $totalBookingSettlementAmount + $totalAdjustmentsAmount;

						$partnerSettlementData['settlementItemsList'] = $settlementItemsList;
						$partnerSettlementData['settlementSummary'] = $settlementSummary;
						$partnerSettlementData['partnerData'] = $partnerData;
					}
					else
					{
						$partnerSettlementMsg = "The selected partner is not valid.";
					}
				}
				else
				{
					$partnerSettlementMsg = "The selected partner is not valid.";
				}
			}

			return response()->json([
				                        'success'               => true,
				                        'partners'              => $partners,
				                        'selectedPartnerId'     => $partnerId,
				                        'selectedPartnerTypeId' => $partnerTypeId,
				                        'partnerSettlementMsg'  => $partnerSettlementMsg,
				                        'partnerSettlementData' => $partnerSettlementData
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function processQuickSettlement(Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			$settlementReference = $request['settlementReference'];
			if (!$settlementReference)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide settlement reference for this settlement'
				                        ]);
			}

			$partnerId = (isset($request['partnerId']) && $request['partnerId']) ? $request['partnerId'] : null;
			$partnerTypeId = (isset($request['partnerTypeId']) && $request['partnerTypeId']) ? $request['partnerTypeId'] : null;

			if (!$partnerId || !$partnerTypeId)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "Kindly provide a valid partner"
				                        ]);
			}

			$partner = null;
			if ($partnerTypeId == config("evibe.ticket.type.venues"))
			{
				$partner = Venue::find($partnerId);
			}
			else
			{
				$partner = Planner::find($partnerId);
			}

			if (!$partner)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "Kindly provide a valid partner"
				                        ]);
			}

			$qsStartLimit = "2019-01-01 00:00:00";
			// fetch all settlement bookings
			$scheduledBookingSettlements = ScheduleSettlementBookings::join('ticket_bookings', 'ticket_bookings.id', '=', 'schedule_settlement_bookings.ticket_booking_id')
			                                                         ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                                         ->whereNull('schedule_settlement_bookings.settlement_done_at')
			                                                         ->whereNull('schedule_settlement_bookings.halted_at')
			                                                         ->whereNotNull('schedule_settlement_bookings.schedule_date')
			                                                         ->whereNull('schedule_settlement_bookings.deleted_at')
			                                                         ->whereNull('ticket_bookings.deleted_at')
			                                                         ->whereNull('ticket.deleted_at')
			                                                         ->where('ticket.created_at', '>=', $qsStartLimit)
			                                                         ->where('schedule_settlement_bookings.partner_id', $partnerId)
			                                                         ->where('schedule_settlement_bookings.partner_type_id', $partnerTypeId)
			                                                         ->select('schedule_settlement_bookings.*')
			                                                         ->get();
			// fetch all adjustments
			$partnerAdjustments = PartnerAdjustments::whereNull('deleted_at')
			                                        ->whereNull('adjustment_done_at')
			                                        ->whereNotNull('schedule_date')
			                                        ->where('partner_id', $partnerId)
			                                        ->where('partner_type_id', $partnerTypeId)
			                                        ->get();

			// create settlement for the partner bookings and mapping
			// this will be the most recent settlement for this partner
			$res = $this->createPartnerSpecificSettlement($partnerId,
			                                              $partnerTypeId,
			                                              $scheduledBookingSettlements->pluck('id')->toArray(),
			                                              $partnerAdjustments->pluck('id')->toArray());

			if (!(isset($res['success']) && $res['success'] && isset($res['settlementId']) && $res['settlementId']))
			{
				return response()->json([
					                        'success' => false
				                        ]);
			}
			$settlementId = $res['settlementId'];
			$settlement = Settlements::find($settlementId);
			if (!$settlement)
			{
				// unable to find created settlement
				return response()->json([
					                        'success' => false
				                        ]);
			}

			// remove any present settlements for this partner (not old pending settlements)
			// 3 days is the cutoff time to place a settlement into pending bucket
			// clear any settlement that is less that 6 day sold
			$oldSettlementLimit = Carbon::createFromTimestamp(time())->startOfDay()->subDays('6')->toDateTimeString();
			$oldSettlements = Settlements::where('created_at', '>=', $oldSettlementLimit)
			                             ->where('partner_id', $partnerId)
			                             ->where('partner_type_id', $partnerTypeId)
			                             ->whereNull('settlement_done_at')
			                             ->whereNull('deleted_at')
			                             ->whereNotIn('id', [$settlementId])
			                             ->update([
				                                      'deleted_at' => Carbon::now()
			                                      ]);

			// try to form some base function so that data doesn't deviate adn will always be the same for preview and process
			// settle up
			$res = $this->processPartnerSettlement($settlementId, $settlementReference, $handlerId);

			// assuming that error message exists for every success failure
			return response()->json($res);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function createPartnerSettlementInvoice($settlementId, Request $request)
	{
		Log::info("access");
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			Log::info("accessig");
			$settlement = Settlements::find($settlementId);

			if (!$settlement->settlement_done_at)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => "Settlement is not yet processed to generate invoice"
				                        ]);
			}

			$settlementDetails = SettlementDetails::where('settlement_id', $settlement->id)->get();
			$settlementBookingIds = $settlementDetails->pluck('settlement_booking_id')->toArray();
			$partnerAdjustmentIds = $settlementDetails->pluck('adjustment_id')->toArray();

			$settlementBookingIds = array_filter($settlementBookingIds, function ($var) {
				return !is_null($var);
			});

			$partnerAdjustmentIds = array_filter($partnerAdjustmentIds, function ($var) {
				return !is_null($var);
			});

			$this->generateAndSaveInvoice($settlement, $settlementBookingIds, $partnerAdjustmentIds);

			$data = [
				'success' => true,
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json([
				                        'success' => false,
				                        'error'   => $exception->getMessage()
			                        ]);
		}
	}

	private function generateInvoiceNumber($settlement)
	{
		$magicNumber = 41022001; // reverse of our formation day
		$increment = $settlement->id - 1;
		$invoiceNumber = sprintf("%011d", $magicNumber + $increment);
		$invoiceNumString = 'EVBI' . $invoiceNumber;

		return $invoiceNumString;
	}

	private function generateAndSaveInvoice($settlement, $settlementBookingIds, $partnerAdjustmentIds)
	{
		$settlementCreatedAt = date('Y-m-d H:i:s', strtotime($settlement->created_at));
		$settlementCreatedTime = strtotime($settlementCreatedAt);

		if ($settlementCreatedTime >= 1521570600 && $settlementCreatedTime <= 1521656999)
		{
			// if settlement created date is 21 Mar
			$settlementWeekStart = 1520965800; // 14 Mar, 0:00 AM
			$settlementWeekEnd = 1521570599; // 20 Mar, 11:59:59 PM
		}
		elseif ($settlementCreatedTime >= 1522175400 && $settlementCreatedTime <= 1522261799)
		{
			// if settlement created date is 28 Mar
			$settlementWeekStart = 1521570600; // 21 Mar, 0:00 AM
			$settlementWeekEnd = 1522175399; // 27 Mar, 11:59:59 PM
		}
		elseif ($settlementCreatedTime >= 1522780200 && $settlementCreatedTime <= 1522866599)
		{
			// if settlement created date is 4 Apr
			$settlementWeekStart = 1522175400; // 28 Mar, 0:00 AM
			$settlementWeekEnd = 1522607399; // 1 Apr, 11:59:59 PM
		}
		else
		{
			$settlementWeekStart = Carbon::createFromTimestamp($settlementCreatedTime)
			                             ->startOfWeek()
			                             ->subWeek(1)->timestamp;

			$settlementWeekEnd = Carbon::createFromTimestamp($settlementCreatedTime)
			                           ->endOfWeek()
			                           ->subWeek(1)->timestamp;
		}

		$partner = $settlement->partner;
		$user = $partner->user;
		$bankDetails = $user ? $user->bankDetails : null;

		$areaName = null;
		$cityName = null;
		$area = Area::find($partner->area_id);
		$city = City::find($partner->city_id);

		$invoiceNumber = $this->generateInvoiceNumber($settlement);

		if ($area)
		{
			$areaName = $area->name;
		}

		if ($city)
		{
			$cityName = $city->name;
		}

		// create and save invoice
		$invoiceData = [
			'gstin'                => config('evibe.business.gstin'),
			'pan'                  => config('evibe.business.pan'),
			'date'                 => date('d M Y', time()),
			'settlementStartDate'  => date('d/m/Y', $settlementWeekStart),
			'settlementEndDate'    => date('d/m/Y', $settlementWeekEnd),
			'settlementAmount'     => $settlement->settlement_amount,
			'settlementAmountText' => $this->convertNumberToWord($settlement->settlement_amount) ? '(Rupees ' . $this->convertNumberToWord($settlement->settlement_amount) . ')' : null,
			'logoFullPath'         => config('evibe.hosts.gallery') . '/img/logo/logo_evibe.png',
			'invoiceNumber'        => $invoiceNumber,
		];

		if ($partner)
		{
			$invoiceData['partnerDetails'] = [
				'firstName'      => $partner->person,
				'companyName'    => $partner->name,
				'address'        => $partner->address ? $partner->address : ($partner->full_address ? $partner->full_address : null), // it differs for venue and planner
				'area'           => $areaName,
				'city'           => $cityName,
				'zip'            => $partner->zip,
				'gstin'          => $partner->gstin,
				'billingAddress' => $partner->billing_address
			];

			if ($bankDetails)
			{
				$invoiceData['partnerDetails']['accountNumber'] = $bankDetails->account_number;
				$invoiceData['partnerDetails']['bankName'] = $bankDetails->bank_name;
				$invoiceData['partnerDetails']['ifscCode'] = $bankDetails->ifsc_code;
			}
		}

		$settlementBookingsList = [];
		$cancelledBookingsList = [];
		$adjustmentsList = [];

		$totalBookingSettlementAmount = 0;
		$totalCancelledBookingSettlementAmount = 0;
		$totalAdjustmentAmount = 0;

		$totalBookingOrderAmount = 0;
		$totalCancelledBookingOrderAmount = 0;
		$totalBookingAdvanceAmount = 0;
		$totalCancelledBookingAdvanceAmount = 0;
		$totalBookingPartnerBoreRefund = 0;
		$totalCancelledBookingCustomerRefund = 0;
		$totalBookingEvibeServiceFee = 0;
		$totalCancelledBookingEvibeServiceFee = 0;
		$totalBookingGST = 0;

		$ticketBookings = TicketBooking::join('schedule_settlement_bookings', 'schedule_settlement_bookings.ticket_booking_id', '=', 'ticket_bookings.id')
		                               ->whereIn('schedule_settlement_bookings.id', $settlementBookingIds)
		                               ->select('ticket_bookings.*', 'schedule_settlement_bookings.settlement_amount')
		                               ->get();
		if (count($ticketBookings))
		{
			foreach ($ticketBookings as $ticketBooking)
			{
				$ticket = $ticketBooking->ticket;
				$area = $ticket->area;

				$gst = $ticketBooking->cgst_amount + $ticketBooking->sgst_amount + $ticketBooking->igst_amount;

				if ($ticketBooking->cancelled_at)
				{
					array_push($cancelledBookingsList, [
						'partyDate'        => date('d/m/Y', $ticketBooking->party_date_time),
						'bookingId'        => $ticketBooking->booking_id,
						'customerName'     => $ticket->name,
						'partyLocation'    => $area ? $area->name : null,
						'bookingAmount'    => $ticketBooking->booking_amount,
						'advanceAmount'    => $ticketBooking->advance_amount,
						'customerRefund'   => $ticketBooking->advance_amount - $ticketBooking->settlement_amount - $ticketBooking->evibe_service_fee,
						'evibeServiceFee'  => $ticketBooking->evibe_service_fee,
						'gst'              => $gst,
						'settlementAmount' => $ticketBooking->settlement_amount,
						'cancelledAt'      => $ticketBooking->cancelled_at,
					]);

					$totalCancelledBookingSettlementAmount += $ticketBooking->settlement_amount;
					$totalCancelledBookingOrderAmount += $ticketBooking->booking_amount;
					$totalCancelledBookingAdvanceAmount += $ticketBooking->advance_amount;
					$totalCancelledBookingCustomerRefund += ($ticketBooking->advance_amount - $ticketBooking->settlement_amount - $ticketBooking->evibe_service_fee);
					$totalCancelledBookingEvibeServiceFee += $ticketBooking->evibe_service_fee;
				}
				else
				{
					array_push($settlementBookingsList, [
						'partyDate'         => date('d/m/Y', $ticketBooking->party_date_time),
						'bookingId'         => $ticketBooking->booking_id,
						'customerName'      => $ticket->name,
						'partyLocation'     => $area ? $area->name : null,
						'bookingAmount'     => $ticketBooking->booking_amount,
						'advanceAmount'     => $ticketBooking->advance_amount,
						'partnerBoreRefund' => $ticketBooking->partner_bear,
						'evibeServiceFee'   => $ticketBooking->evibe_service_fee,
						'gst'               => $gst,
						'settlementAmount'  => $ticketBooking->settlement_amount,
						'cancelledAt'       => $ticketBooking->cancelled_at,
					]);

					$totalBookingSettlementAmount += $ticketBooking->settlement_amount;
					$totalBookingOrderAmount += $ticketBooking->booking_amount;
					$totalBookingAdvanceAmount += $ticketBooking->advance_amount;
					$totalBookingPartnerBoreRefund += $ticketBooking->partner_bear;
					$totalBookingEvibeServiceFee += $ticketBooking->evibe_service_fee;
					$totalBookingGST += $gst;
				}
			}
		}

		$invoiceData['settlementBookingsList'] = $settlementBookingsList;
		$invoiceData['totalBookingOrderAmount'] = $totalBookingOrderAmount;
		$invoiceData['totalBookingAdvanceAmount'] = $totalBookingAdvanceAmount;
		$invoiceData['totalBookingPartnerBoreRefund'] = $totalBookingPartnerBoreRefund;
		$invoiceData['totalBookingEvibeServiceFee'] = $totalBookingEvibeServiceFee;
		$invoiceData['totalBookingGST'] = $totalBookingGST;
		$invoiceData['totalBookingAlreadySettledAmount'] = 0;
		$invoiceData['totalBookingSettlementAmount'] = $totalBookingSettlementAmount;

		$invoiceData['cancelledBookingsList'] = $cancelledBookingsList;
		$invoiceData['totalCancelledBookingOrderAmount'] = $totalCancelledBookingOrderAmount;
		$invoiceData['totalCancelledBookingAdvanceAmount'] = $totalCancelledBookingAdvanceAmount;
		$invoiceData['totalCancelledBookingCustomerRefund'] = $totalCancelledBookingCustomerRefund;
		$invoiceData['totalCancelledBookingEvibeServiceFee'] = $totalCancelledBookingEvibeServiceFee;
		$invoiceData['totalCancelledBookingAlreadySettledAmount'] = 0;
		$invoiceData['totalCancelledBookingSettlementAmount'] = $totalCancelledBookingSettlementAmount;

		$partnerAdjustments = PartnerAdjustments::whereIn('id', $partnerAdjustmentIds)
		                                        ->get();

		if (count($partnerAdjustments))
		{
			foreach ($partnerAdjustments as $partnerAdjustment)
			{
				$partyDate = null;
				$customerName = null;
				$bookingId = null;

				if ($partnerAdjustment->ticket_booking_id)
				{
					$booking = TicketBooking::find($partnerAdjustment->ticket_booking_id);
					if ($booking)
					{
						$partyDate = date('d/m/Y', $booking->party_date_time);
						$bookingId = $booking->booking_id;

						$ticket = $booking->ticket;
						if ($ticket)
						{
							$customerName = $ticket->name;
						}
					}
				}

				array_push($adjustmentsList, [
					'adjustmentAmount'   => $partnerAdjustment->adjustment_amount,
					'payEvibe'           => $partnerAdjustment->pay_evibe,
					'adjustmentComments' => $partnerAdjustment->comments,
					'partyDate'          => $partyDate,
					'customerName'       => $customerName,
					'bookingId'          => $bookingId
				]);

				if ($partnerAdjustment->pay_evibe)
				{
					$totalAdjustmentAmount -= $partnerAdjustment->adjustment_amount;
				}
				else
				{
					$totalAdjustmentAmount += $partnerAdjustment->adjustment_amount;
				}
			}
		}

		$invoiceData['totalAdjustmentAmount'] = $totalAdjustmentAmount;
		$invoiceData['adjustmentsList'] = $adjustmentsList;
		$invoiceData['commissionRate'] = $partner ? $partner->commission_rate : config('evibe.default.evibe_service_fee_percent');

		$invoicePath = config('evibe.root.gallery') . '/invoices/';
		if (!file_exists($invoicePath))
		{
			mkdir($invoicePath, 0777, true);
		}

		$pdf = PDF::loadView('emails.finance.partner-settlement-invoice', ['data' => $invoiceData]);
		$output = $pdf->output();

		$invoiceTitle = date('d-m-y', $settlementWeekStart) . '_' . date('d-m-y', $settlementWeekEnd);
		if ($partner && $partner->name)
		{
			$partnerCompanyName = $partner->name;
			$partnerCompanyName = preg_replace('/\s+/', '-', $partnerCompanyName); // replaces multiple spaces with '-'
			$partnerCompanyName = preg_replace('/[^A-Za-z0-9\-]/', '', $partnerCompanyName); // removes special characters
			$invoiceTitle = $partnerCompanyName . '_' . $invoiceTitle;
		}
		else
		{
			$invoiceTitle = $invoiceNumber . '_' . $invoiceTitle;
		}

		$invoiceTitle = $invoiceTitle . '.pdf';

		$invoiceFullPath = $invoicePath . $invoiceTitle;
		file_put_contents($invoiceFullPath, $output);

		return $res = [
			'invoiceUrl'                   => $invoiceTitle,
			'invoiceTitle'                 => $invoiceTitle,
			'invoiceNumber'                => $invoiceNumber,
			'settlementStartDate'          => date('d/m/Y', $settlementWeekStart),
			'settlementEndDate'            => date('d/m/Y', $settlementWeekEnd - 1),
			'totalOrderAmount'             => $totalBookingOrderAmount + $totalCancelledBookingOrderAmount,
			'totalAdvanceAmount'           => $totalBookingAdvanceAmount + $totalCancelledBookingAdvanceAmount,
			'totalPartnerBoreRefund'       => $totalBookingPartnerBoreRefund,
			'totalEvibeServiceFee'         => $totalBookingEvibeServiceFee,
			'totalGST'                     => $totalBookingGST,
			'totalBookingSettlementAmount' => $totalBookingSettlementAmount + $totalCancelledBookingSettlementAmount,
			'totalAdjustmentAmount'        => $totalAdjustmentAmount
		];
	}

	public function calculateBookingSettlementAmount($ticketBooking)
	{
		$res = [];
		if ($ticketBooking->is_advance_paid)
		{
			$bookingAmount = $ticketBooking->booking_amount;
			$advanceAmount = $ticketBooking->advance_amount;
			$ticketMapping = $ticketBooking->ticketMapping;
			$isStarOrder = $ticketBooking->is_star_order;
			$partnerBear = $ticketBooking->partner_bear;

			$partnerAmount = 0;
			$settlementAmount = 0;
			$evibeServiceFee = 0;

			/*
			 * 1. Calculate evibe service fee for all types of orders (normal or star)
			 * 2. Calculate partner settlement amount based on advance paid, evibe service fee, gst and etc.
			 */

			Log::info("star order: $isStarOrder");
			// check if already marked as star order (either auto or from DASH)
			if ($isStarOrder)
			{
				// marked as star order

				// check whether any partner amount has been provided
				if ($ticketBooking->star_partner_amount)
				{
					// use given amount (by team mostly) to compute
					$partnerAmount = $ticketBooking->star_partner_amount;
					$balanceAmount = $bookingAmount - $partnerAmount;
					$evibeServiceFee = $balanceAmount / (1 + config('evibe.default.gst_star'));
					Log::info("1");
				}
				else
				{
					Log::info("2");
					// calculate amount for star order
					$starOption = null;
					if ($ticketMapping)
					{
						$starOption = StarOption::where('option_id', $ticketMapping->map_id)
						                        ->where('option_type_id', $ticketMapping->map_type_id)
						                        ->whereNull('deleted_at')
						                        ->first();
					}

					if ($starOption && ($starOption->commission_rate || $starOption->partner_amount))
					{
						Log::info("3");
						// if it is a star option, calculate based on the DB data
						if ($starOption->partner_amount)
						{
							Log::info("4");
							$partnerAmount = $starOption->partner_amount;
							$balanceAmount = $bookingAmount - $partnerAmount;
							$evibeServiceFee = $balanceAmount / (1 + config('evibe.default.gst_star'));
						}
						elseif ($starOption->commission_rate)
						{
							Log::info("5");
							$evibeServiceFee = $bookingAmount * $starOption->commission_rate / 100;
						}
					}
					else
					{
						Log::info("6");
						// if not a star option, calculate using standard formula (50%)
						$evibeServiceFee = $bookingAmount * 50 / 100;
					}
				}
			}
			else
			{
				///////////////////
				// @todo: remove after 25 Sep 2019
				// for already existing options but not marked as star order
				Log::info("7");
				$starOption = null;
				if ($ticketMapping)
				{
					$starOption = StarOption::where('option_id', $ticketMapping->map_id)
					                        ->where('option_type_id', $ticketMapping->map_type_id)
					                        ->whereNull('deleted_at')
					                        ->first();
				}

				if ($starOption && ($starOption->commission_rate || $starOption->partner_amount))
				{
					Log::info("8");
					// if it is a star option, calculate based on the DB data
					if ($starOption->partner_amount)
					{
						Log::info("9");
						$partnerAmount = $starOption->partner_amount;
						$balanceAmount = $bookingAmount - $partnerAmount;
						$evibeServiceFee = $balanceAmount / (1 + config('evibe.default.gst_star'));
					}
					elseif ($starOption->commission_rate)
					{
						Log::info("10");
						$evibeServiceFee = $bookingAmount * $starOption->commission_rate / 100;
					}

					$ticketBooking->is_star_order = 1;
					$ticketBooking->updated_at = Carbon::now();
					$ticketBooking->save();

					$isStarOrder = 1;
				}
				elseif (($partner = $ticketBooking->provider) && ($commissionRate = $partner->commission_rate))
				{
					Log::info("11");
					$evibeServiceFee = $bookingAmount * $commissionRate / 100;
				}
				else
				{
					Log::info("12");
					$evibeServiceFee = $bookingAmount * config('evibe.default.evibe_service_fee');
				}
				///////////////////

				// not a star order
				// @todo: uncomment the following after removing the above code
				//if (($partner = $ticketBooking->provider) && ($commissionRate = $partner->commission_rate))
				//{
				//	$evibeServiceFee = $bookingAmount * $commissionRate / 100;
				//}
				//else
				//{
				//	$evibeServiceFee = $bookingAmount * config('evibe.default.evibe_service_fee');
				//}
			}

			// Karnataka, Telangana - SGST + CGST
			// Any other state - IGST

			if (($ticketBooking->ticket->city_id == config('evibe.city.Bengaluru')) || ($ticketBooking->ticket->city_id == config('evibe.city.Hyderabad')))
			{
				$cgst = $evibeServiceFee * config('evibe.default.partial_gst');
				$sgst = $evibeServiceFee * config('evibe.default.partial_gst');
				$igst = 0;

				if ($isStarOrder)
				{
					$cgst = $evibeServiceFee * config('evibe.default.partial_gst_star');
					$sgst = $evibeServiceFee * config('evibe.default.partial_gst_star');
				}
			}
			else
			{
				$cgst = 0;
				$sgst = 0;
				$igst = $evibeServiceFee * config('evibe.default.gst');

				if ($isStarOrder)
				{
					$igst = $evibeServiceFee * config('evibe.default.gst_star');
				}
			}

			$ticketBooking->evibe_service_fee = $evibeServiceFee;
			$ticketBooking->cgst_amount = $cgst;
			$ticketBooking->sgst_amount = $sgst;
			$ticketBooking->igst_amount = $igst;
			$ticketBooking->save();

			$gst = $sgst + $cgst + $igst;

			// settlement amount may result in negative amounts also (depends on advance paid)
			$settlementAmount = $advanceAmount - ($evibeServiceFee + $gst + $partnerBear);

			$res = [
				'settlementAmount'  => $settlementAmount,
				'bookingAmount'     => $bookingAmount,
				'advanceAmount'     => $advanceAmount,
				'cgstAmount'        => $cgst,
				'sgstAmount'        => $sgst,
				'igstAmount'        => $igst,
				'gstAmount'         => $gst,
				'evibeServiceFee'   => $evibeServiceFee,
				'partnerBoreRefund' => $partnerBear
			];
		}

		return $res;
	}

	public function calculateSettlementScheduleDateTime($partyDateTime)
	{
		// @see
		// The partyDateTime is used for temp purpose
		// to introduce the new policy system - next scheduled settlements
		// @todo: just use "else" condition and remove other code after 2nd April

		$mar14Start = 1520965800;
		$mar20End = 1521570599;
		$mar21Start = 1521570600;
		$mar27End = 1522175399;
		$mar28Start = 1522175400;
		$apr1End = 1522521000;

		if ($partyDateTime >= $mar14Start && $partyDateTime <= $mar20End)
		{
			// if party date between 14 Mar (Wed) to 20 Mar 2018 (Tue) => to 21 Mar 2018 (Wed)
			$scheduleDateTime = 1521606600; // 21 March, 10:00 AM

		}
		elseif ($partyDateTime >= $mar21Start && $partyDateTime <= $mar27End)
		{
			// if party date between 21 Mar (Wed) to 27 Mar 2018 (Tue) => to 28 Mar 2018 (Wed)
			$scheduleDateTime = 1522211400; // 28 March, 10:00 AM

		}
		elseif ($partyDateTime >= $mar28Start && $partyDateTime <= $apr1End)
		{
			// if party date between 28 Mar 2018 (Wed) to 1 Apr 2018 (Sun) => 4 Apr 2018 (Wed)
			$scheduleDateTime = 1522816200; // 4 April, 10:00 AM
		}
		else
		{
			// default case: Monday to Sunday on Wednesday
			// if party date between 2 Apr 2018 (Mon) to 8 Apr 2018 (Sun) => 11 Apr 2018 (Wed)

			$scheduleDateTime = Carbon::createFromTimestamp($partyDateTime)
			                          ->startOfWeek()
			                          ->addWeek(1)
			                          ->addDays(2)
			                          ->addHours(10)->timestamp;
		}

		return date("Y-m-d H:i:s", $scheduleDateTime);
	}
}