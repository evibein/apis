<?php

namespace App\Http\Controllers\CRM;

use App\Exceptions\CustomException;
use App\Http\Controllers\BaseController;
use App\Http\Models\Area;
use App\Http\Models\City;
use App\Http\Models\Ticket;
use App\Http\Models\TicketUpdate;
use App\Http\Models\User;
use App\Http\Models\Util\LandingSignupUsers;
use App\Jobs\Mail\MailJdLeadToTeamJob;

use Carbon\Carbon;
use Evibe\Utility\TicketStatus;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CRMController extends BaseController
{
	const MISSED = 'missed';
	const RECEIVED = 'received';

	public function integrateMyOperator(Request $request)
	{
		if (!$request->has('client-id'))
		{
			throw new CustomException("Client id is required.", 403);
		}
		$this->validateClientId($request->get('client-id'));

		$data = ['success' => 'true'];
		$params = json_decode($this->pluck('myoperator', $request->all()), true);
		$phone = $this->pluck('_cr', $params, true);
		$voiceMail = $this->pluck('_fu', $params);

		if ($phone && strlen($phone) == 10)
		{
			$ticket = $this->getTicketByPhone($phone);
			$operatorData = $this->getOperatorData($params);

			// create ticket
			if (!$ticket)
			{
				// @todo: push to partner management if dialed "new-vendor" extension
				$ticketData = [
					'area_id'           => null,
					'handler_id'        => $operatorData['userId'],
					'source_id'         => config('evibe.ticket.source.auto'), // auto ticket,
					'status_id'         => config('evibe.ticket.status.initiated'),
					'phone'             => $phone,
					'created_at'        => date('Y-m-d H:i:s', $operatorData['startTime']),
					'updated_at'        => date('Y-m-d H:i:s', $operatorData['startTime']),
					'enquiry_source_id' => config('evibe.ticket.enquiry_source.phone')
				];

				$ticket = $this->createNewTicket($ticketData);
			}

			$updateData = [
				'ticket'     => $ticket,
				'userId'     => $operatorData['userId'],
				'statusAt'   => $operatorData['startTime'],
				'typeUpdate' => 'Phone (IVR)'
			];

			if ($operatorData['type'] == $this::MISSED)
			{
				$comments = 'Call missed on IVR.';

				if ($voiceMail)
				{
					$comments .= " <a target='_blank' href='" . $voiceMail . "'>Voice Mail</a>";
				}

				$updateData['comments'] = $comments;
				$this->saveTicketAction($updateData);

				$pushData = array_merge($updateData, [
					'title'   => 'Missed Call On MyOperator',
					'message' => '[' . date('h:i A, d M', $operatorData['startTime']) . '] we missed a call from : ' . $phone . '. Please call ASAP.'
				]);

				$this->sendWebPushNotification($pushData);
			}
			elseif ($operatorData['type'] == $this::RECEIVED)
			{
				$comments = 'Received call on IVR.';
				if ($voiceMail)
				{
					$comments .= " <a target='_blank' href='" . $voiceMail . "'>Call Recording</a>";
				}

				$updateData['comments'] = $comments;
				$this->saveTicketAction($updateData);
			}
			else
			{
				// do nothing. Ideally should never be here.
				$error = 'Call status is either not missed / received. Check corner case, caller number: '
					. $phone . ' at ' . $operatorData['startTime'];

				$this->alertAdmin(['error' => $error]);
			}
		}
		else
		{
			$this->alertAdmin(['error' => 'Invalid phone number ' . $phone]);
		}

		return response()->json($data);
	}

	/**
	 * Exotel Integration
	 *
	 * Documentation: https://support.exotel.com/support/solutions/articles/48283-working-with-passthru-applet
	 *
	 * GET Request
	 *
	 * Parameters:
	 *  - CallSid: string; unique identifier of the call
	 *  - From: Number of the caller / number at the end of the first leg of the call (ex: 09133704000)
	 *  - To: Your company number into which the call came (ex: 08033946510)
	 *  - Direction: 'incoming', 'outbound-dial'
	 *  - DialCallDuration: The duration of the call (when the call was connected to an agent)
	 *  - StartTime: Time in format YYYY-MM-DD HH:mm:ss; Date and time when the call request was initiated to the
	 *  operator
	 *  - CurrentTime: Current server time (format : yyyy-mm-dd hh:mm:ss)
	 *  - EndTime:  1970-01-01 05:30:00 // Unix time (also known as POSIX time or epoch time)
	 *  - CallType:
	 *  - digits: If there was a 'Gather' applet
	 *  - RecordingUrl:
	 *  - CustomField: If the call was initiated via API , the value that was passed in CustomField in the API call
	 *  - DialWhomNumber: Shows the number of the agent who was dialed to last (ex: 08639213191)
	 *
	 * Response:
	 *  - Send 200 OK on success
	 *  - Send 302 Found on Error
	 *
	 * @param       $request
	 */
	public function integrateExotel(Request $request)
	{
		$exotelData = $request->all();

		$from = isset($exotelData['From']) ? $exotelData['From'] : null;
		$handlerPhone = isset($exotelData['DialWhomNumber']) && $exotelData['DialWhomNumber'] ? $exotelData['DialWhomNumber'] : null;
		$currentTime = isset($exotelData['CurrentTime']) && $exotelData['CurrentTime'] ? $exotelData['CurrentTime'] : null;
		$callType = isset($exotelData['CallType']) && $exotelData['CallType'] ? $exotelData['CallType'] : null;
		$callSid = isset($exotelData['CallSid']) && $exotelData['CallSid'] ? $exotelData['CallSid'] : null;

		if (is_null($from) || is_null($callSid))
		{
			$this->alertAdmin(['error' => 'Exotel Error: From number is NULL or CallSid does not exist.']);

			return response()->json(["success" => false], 302);
		}

		// separate leading calling code from actual phone number
		$callingCode = substr($from, 0, (strlen($from) - 10));
		// if calling code is '0', then it is from within the country
		if (!$callingCode)
		{
			$callingCode = "+91";
		}

		if (strpos($callingCode, '+') !== false)
		{
			// calling code has +
		}
		else
		{
			$callingCode = '+' . $callingCode;
		}
		$callingCode = str_replace(' ', '', $callingCode);

		$from = substr($from, -10);
		$handlerPhone = !is_null($handlerPhone) ? substr($handlerPhone, 1) : null;

		if (!strlen($from) == 10)
		{
			$this->alertAdmin(['error' => 'Exotel Error: Invalid from phone number: ' . $from]);

			return response()->json(["success" => false], 302);
		}

		$ticket = $this->getTicketByPhone($from);
		$handler = !is_null($handlerPhone) ? $this->fetchHandlerByPhone($handlerPhone) : null;

		// ticket with valid status does not exist - create one
		if (!$ticket)
		{
			$ticketData = [
				'area_id'           => null,
				'handler_id'        => !is_null($handler) ? $handler->id : null,
				'source_id'         => config('evibe.ticket.source.auto'), // auto ticket,
				'status_id'         => config('evibe.ticket.status.initiated'),
				'phone'             => $from,
				'calling_code'      => $callingCode,
				'created_at'        => $currentTime,
				'updated_at'        => $currentTime,
				'enquiry_source_id' => config('evibe.ticket.enquiry_source.phone')
			];

			$ticket = $this->createNewTicket($ticketData, ['statusAt' => $currentTime]);
		}
		else
		{
			Log::info("Ticket already exists: " . $ticket->id);
		}

		$updateData = [
			'ticket'     => $ticket,
			'userId'     => !is_null($handler) ? $handler->id : null,
			'statusAt'   => $currentTime,
			'typeUpdate' => 'Phone (IVR)'
		];

		// CallTypes:
		//  - completed: Call conversation happened
		//  - client-hangup: Client hung up during connect applet
		//  - call-attempt: IVR only, no connect applet
		//  - incomplete: Connect applet, no agent picked up
		//  - voicemail: Went to voicemail applet

		$updateComments = "";
		$recordingUrl = isset($exotelData['RecordingUrl']) ? $exotelData['RecordingUrl'] : "";
		switch ($callType)
		{
			case "completed":
				$updateComments .= "Call received on Exotel. ";
				if ($handler)
				{
					$updateComments .= " By handler: " . $handler->name;
				}
				if (!is_null($recordingUrl))
				{
					$updateComments .= " <a target='_blank' href='" . $recordingUrl . "'>Listen Call Recording</a>.";
				}
				break;

			case "client-hangup":
				$updateComments .= "Customer DISCONNECTED call before connecting to any handler.";
				break;

			case "call-attempt":
				$updateComments .= "CALL INITIATED on Exotel ";
				break;

			case "incomplete":
				$updateComments .= "CALL MISSED: ";
				if ($handler)
				{
					$updateComments .= "Handler " . $handler->name . " did not pick up the call.";
				}
				else
				{
					$updateComments .= "No handler picked the call on Exotel.";
				}
				break;

			case "voicemail":
				$updateComments .= "Call landed in VOICEMAIL.";
				if (!is_null($recordingUrl))
				{
					$updateComments .= " <a target='_blank' href='" . $recordingUrl . "'>Listen Voice Mail</a>";
				}
				break;

			default:
				$updateComments .= "Default case. CallType: " . $callType;
				break;
		}

		$updateComments .= " CallSid: " . $callSid;
		$updateData['comments'] = $updateComments;
		$this->saveTicketAction($updateData);

		$pushData = array_merge($updateData, [
			'title'   => 'Call Update - ' . $from,
			'message' => $updateComments
		]);
		$this->sendWebPushNotification($pushData);

		return response()->json(["success" => true]);
	}

	public function exotelMissedCallCampaign(Request $request)
	{
		$exotelData = $request->all();

		$from = isset($exotelData['From']) ? $exotelData['From'] : null;
		$callType = isset($exotelData['CallType']) && $exotelData['CallType'] ? $exotelData['CallType'] : null;
		$callSid = isset($exotelData['CallSid']) && $exotelData['CallSid'] ? $exotelData['CallSid'] : null;

		if (is_null($from) || is_null($callSid))
		{
			$this->alertAdmin(['error' => 'Exotel Error: From number is NULL or CallSid does not exist.']);

			return response()->json(["success" => false], 302);
		}

		$from = substr($from, -10);
		if (!strlen($from) == 10)
		{
			$this->alertAdmin(['error' => 'Exotel Error: Invalid from phone number: ' . $from]);

			return response()->json(["success" => false], 302);
		}

		// CallTypes:
		//  - completed: Call conversation happened
		//  - client-hangup: Client hung up during connect applet
		//  - call-attempt: IVR only, no connect applet
		//  - incomplete: Connect applet, no agent picked up
		//  - voicemail: Went to voicemail applet

		$updateComments = "";
		$recordingUrl = isset($exotelData['RecordingUrl']) ? $exotelData['RecordingUrl'] : "";
		switch ($callType)
		{
			case "completed":
				$updateComments .= "Call received on Exotel. ";
				if (!is_null($recordingUrl))
				{
					$updateComments .= " <a target='_blank' href='" . $recordingUrl . "'>Listen Call Recording</a>.";
				}
				break;

			case "client-hangup":
				$updateComments .= "Customer DISCONNECTED call before connecting to any handler.";
				break;

			case "call-attempt":
				$updateComments .= "CALL INITIATED on Exotel ";
				break;

			case "incomplete":
				$updateComments .= "CALL MISSED: No handler picked the call on Exotel.";
				break;

			case "voicemail":
				$updateComments .= "Call landed in VOICEMAIL.";
				if (!is_null($recordingUrl))
				{
					$updateComments .= " <a target='_blank' href='" . $recordingUrl . "'>Listen Voice Mail</a>";
				}
				break;

			default:
				$updateComments .= "Default case. CallType: " . $callType;
				break;
		}

		LandingSignupUsers::create([
			                           "" => ""
		                           ]);

		Log::info($updateComments);

		return response()->json(["success" => true]);
	}

	/**
	 * Parameters sent by JustDial
	 *
	 * 1. leadid – Numeric unique value
	 * 2. leadtype – Company call / Category call
	 * 3. prefix – Mr / Ms / Dr / Col
	 * 4. name – Caller's Name
	 * 5. mobile – Caller's Mobile
	 * 6. phone – Caller's Land line
	 * 7. email – Caller's Email address
	 * 8. date – Date
	 * 9. category – Caller's requirement i.e the category name for which user taken information.
	 * 10. area - Caller's area
	 * 11. city - Caller's city
	 * 12. brancharea - Name of the branch area i.e Autoportal company area
	 * 13. dncmobile – If Mobile number is a DNC the flag will be 1 or else 0
	 * 14. dncphone – If Landline number is a DNC the flag will be 1 or else 0
	 * 15. company – Company Name i.e Autoportal
	 * 16. pincode – Caller's Pincode based on caller's area
	 * 17. time – TimeStamp on which lead generated
	 * 18. branchpin – Company's Pin code based on branch area
	 */
	public function saveLeadFromJustDial(Request $request)
	{
		/* @see: JD is incapable of handling user params
		 * if (!$request->has('client-id'))
		 * {
		 * throw new CustomException("Client id is required.", 403);
		 * }
		 * $this->validateClientId($request->get('client-id'));
		 */

		$jdParams = $request->all();
		$phone = $this->pluck('mobile', $jdParams);
		$landline = $this->pluck('phone', $jdParams);

		if (!$landline && !$phone)
		{
			throw new CustomException("No phone / landline number.", 403);
		}

		$ticket = $this->getTicketByPhone($phone);
		if (!$ticket)
		{
			$date = $this->pluck('date', $jdParams);
			$time = $this->pluck('time', $jdParams);
			$name = ucfirst($this->pluck('name', $jdParams));
			$leadTimestamp = Carbon::parse($date . ' ' . $time);

			$ticketData = [
				'phone'             => $phone,
				'name'              => $name,
				'email'             => $this->pluck('email', $jdParams),
				'zip_code'          => $this->pluck('pincode', $jdParams),
				'city_id'           => $this->getCityIdByName($this->pluck('city', $jdParams)),
				'area_id'           => $this->getAreaIdByName($this->pluck('area', $jdParams)),
				'source_id'         => config('evibe.ticket.source.justDial'),
				'status_id'         => config('evibe.ticket.status.initiated'),
				'comments'          => $this->getTicketCommentsByJDParams($jdParams),
				'created_at'        => $leadTimestamp,
				'updated_at'        => $leadTimestamp,
				'enquiry_source_id' => config('evibe.ticket.enquiry_source.phone')
			];

			$ticket = $this->createNewTicket($ticketData, ['prefix' => 'ENQ-JD-', 'comment' => 'New ticket created from JD lead']);

			$emailData = [
				'subject'  => "New lead from JustDial $name - $phone",
				'name'     => $name,
				'phone'    => $phone,
				'dashLink' => config('evibe.hosts.dash') . '/tickets/' . $ticket->id
			];

			$this->dispatch(new MailJdLeadToTeamJob($emailData));
		}
		else
		{
			$updateData = [
				'ticket'     => $ticket,
				'typeUpdate' => 'JustDial',
				'comments'   => 'Lead captured from JustDial'
			];
			$this->saveTicketAction($updateData);
		}

		return "RECEIVED";
	}

	/**
	 * @param       $ticketData
	 * @param array $createData
	 *
	 * @return Ticket
	 */
	private function createNewTicket($ticketData, $createData = [])
	{
		// create new ticket
		$prefix = "ENQ-PH-";
		$comment = "New ticket created from IVR call.";
		$statusAt = null;
		if (is_array($createData))
		{
			$prefix = isset($createData['prefix']) ? $createData['prefix'] : $prefix;
			$comment = isset($createData['comment']) ? $createData['comment'] : $comment;
			$statusAt = isset($createData['statusAt']) ? $createData['statusAt'] : null;
		}

		$ticket = Ticket::create($ticketData);
		$ticket->enquiry_id = $prefix . $ticket->id;
		$ticket->save();

		$this->saveTicketAction([
			                        'typeUpdate' => $ticket->source->name,
			                        'ticket'     => $ticket,
			                        'comments'   => $comment,
			                        'statusAt'   => $statusAt
		                        ]);

		return $ticket;
	}

	protected function saveTicketAction($data)
	{
		$ticket = $data['ticket'];
		$statusId = $ticket->status_id;
		$isUpdateTicket = false;
		$statusAt = isset($data['statusAt']) ? $data['statusAt'] : Carbon::now()->timestamp;
		$typeUpdate = isset($data['typeUpdate']) ? $data['typeUpdate'] : 'Auto';
		$userId = isset($data['userId']) ? $data['userId'] : null;
		$comments = isset($data['comments']) ? $data['comments'] : '';

		// if status id either 3/4 do not update status id (confirmed / booked)
		if ($statusId != TicketStatus::BOOKED && $statusId != TicketStatus::CONFIRMED && isset($data['statusId']))
		{
			$statusId = $data['statusId'];
			$isUpdateTicket = true;
		}

		// $statusAt check for MyOperator (int) vs Exotel (timestamp)
		$updateData = [
			'ticket_id'   => $ticket->id,
			'status_id'   => $statusId,
			'handler_id'  => $userId,
			'comments'    => $comments,
			'status_at'   => is_int($statusAt) ? $statusAt : strtotime($statusAt),
			'type_update' => $typeUpdate
		];

		TicketUpdate::create($updateData);

		if ($isUpdateTicket)
		{
			$ticket->status_id = $statusId;
			$ticket->updated_at = is_int($statusAt) ? date('Y-m-d H:i:s', $statusAt) : $statusAt;
		}

		$ticket->save();
	}

	private function getOperatorData($callData)
	{
		$data = [
			'userId'    => null,
			'ext'       => null,
			'type'      => $this::MISSED,
			'startTime' => $this->pluck('_st', $callData, true),
			'duration'  => $this->pluck('_dr', $callData, true)
		];

		$logDetails = (array)$this->pluck('_ld', $callData, true);
		foreach ($logDetails as $currentLog)
		{
			if ($currentLog['_ac'] == $this::RECEIVED)
			{
				$data['type'] = $this::RECEIVED;
				$data['startTime'] = $currentLog['_st'];
				$data['duration'] = $currentLog['_dr'];

				$rr = end($currentLog['_rr']);
				$data['ext'] = $rr['_ex'];
				$data['userId'] = $this->fetchUserIdByEmail($rr['_em']);
			}
		}

		return $data;
	}

	private function fetchUserIdByEmail($email)
	{
		$userId = null;
		$user = User::where('username', 'LIKE', $email)->first();
		if ($user)
		{
			$userId = $user->id;
		}
		else
		{
			$this->alertAdmin(['error' => 'could not find user with email ' . $email]);
		}

		return $userId;
	}

	private function fetchHandlerByPhone($phone)
	{
		$user = null;

		if (!is_null($phone))
		{
			$user = User::where('phone', 'LIKE', $phone)->first();

			if (!$user)
			{
				$user = null;
				// @since: 12 Dec 2018
				// @comment: Syrow testing, frequest number changes
				// $this->alertAdmin(['error' => 'Exotel: Could not find user with phone ' . $phone]);
			}
		}

		return $user;
	}

	private function pluck($key, $array, $throw = false)
	{
		$value = '';
		if (!isset($array[$key]))
		{
			if ($throw)
			{
				throw new CustomException('Invalid Payload', 405);
			}
		}
		else
		{
			$value = $array[$key];
		}

		return $value;
	}

	private function alertAdmin($data)
	{
		$this->sendAndSaveNonExceptionReport([
			                                     'code'      => 0,
			                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
			                                     'method'    => \Illuminate\Support\Facades\Request::method(),
			                                     'message'   => '[CRMController.php - ' . __LINE__ . '] ' . isset($data['error']) ? $data['error'] : null,
			                                     'exception' => '[CRMController.php - ' . __LINE__ . '] ' . isset($data['error']) ? $data['error'] : null,
			                                     'trace'     => isset($data) && $data ? print_r($data, true) : 'No data available',
			                                     'details'   => isset($data) && $data ? print_r($data, true) : 'No data available'
		                                     ]);
	}

	private function getTicketByPhone($phone)
	{
		$ticket = null;
		$minStartTime = Carbon::now()->subDays(10)->startOfDay();
		$ticketStatuses = $this->getTicketStatuses();

		// if ticket created more than 10 days ago and not in initiated
		// consider not created
		if ($phone && strlen($phone) == 10)
		{
			$ticket = Ticket::select('ticket.*')
			                ->where('ticket.phone', 'LIKE', $phone)
			                ->where('ticket.created_at', '>', $minStartTime)
			                ->whereNotIn('status_id', $ticketStatuses["end"])
			                ->where(function ($query) {
				                $query->whereIn("status_id", [config("evibe.ticket.status.service_auto_pay"), config("evibe.ticket.status.service_auto_pay"), config("evibe.ticket.status.confirmed")])
				                      ->orWhere(function ($query1) {
					                      $query1->where("is_auto_booked", 0)
					                             ->whereIn("status_id", [config("evibe.ticket.status.initiated"), config("evibe.ticket.status.progress"),
						                             config("evibe.ticket.status.followup"), config("evibe.ticket.status.no_response"), config("evibe.ticket.status.invalid_email")]);
				                      });
			                })
			                ->first();
		}

		return $ticket;
	}

	private function getCityIdByName($cityName)
	{
		$cityName = trim(strtolower(str_replace("+", " ", $cityName))); // replaces + with " "
		$city = City::where('name', 'LIKE', $cityName)->first();
		if ($city)
		{
			return $city->id;
		}

		return null;
	}

	private function getAreaIdByName($areaName)
	{
		$areaName = trim(strtolower(str_replace("+", " ", $areaName))); // replaces + with " "
		$area = Area::where('name', 'LIKE', $areaName)->first();
		if ($area)
		{
			return $area->id;
		}

		return null;
	}

	private function getTicketCommentsByJDParams($data)
	{
		$comments = "";
		$params = [
			'category'   => 'Requirement',
			'phone'      => 'Landline',
			'date'       => 'Date',
			'pincode'    => 'Pin',
			'area'       => 'Area',
			'city'       => 'City',
			'prefix'     => 'Prefix',
			'brancharea' => 'Branch Area',
			'branchpin'  => 'Branch Pin',
			'leadid'     => 'Lead Id',
			'leadtype'   => 'Lead Type',
			'dncmobile'  => 'DncMobile',
			'dncphone'   => 'DncPhone',
			'company'    => 'Company',
			'time'       => 'Timestamp'
		];

		foreach ($params as $key => $value)
		{
			$keyValue = trim(str_replace("+", " ", $this->pluck($key, $data)));
			$comments .= $keyValue ? $value . ": " . $keyValue . "; " : "";
		}

		return substr($comments, 0, strlen($comments) - 2); // remove last "; "
	}
}