<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\BaseController;
use App\Http\Models\Ticket;
use App\Http\Models\TicketBooking;
use Carbon\Carbon;

class SalesDashboardController extends BaseController
{
	public function getBookingsDataForTL()
	{
		$data = [];
		$dates = [
			"month" => [
				'from' => Carbon::now()->startOfMonth()->toDateTimeString(),
				'to'   => Carbon::now()->toDateTimeString()
			],
			"week" => [
				'from' => Carbon::now()->startOfWeek()->toDateTimeString(),
				'to'   => Carbon::now()->toDateTimeString()
			],
		];

		foreach ($dates as $key => $date)
		{
			$timestamps = [Carbon::parse($date['from'])->timestamp, Carbon::parse($date['to'])->timestamp];

			$data[$key] = [
				'tickets'  => [
					'count' => 0
				],
				'bookings' => [
					'count' => 0,
					'aov'   => 0
				]
			];

			// tickets
			$invalidStatuses = [
				config('evibe.ticket.status.duplicate'),
				config('evibe.ticket.status.related'),
				config('evibe.ticket.status.irrelevant')
			];

			$tickets = Ticket::whereBetween('created_at', array_flatten($date))
			                 ->distinct('phone')
			                 ->whereNotIn('status_id', $invalidStatuses)
			                 ->pluck("id")
			                 ->toArray();

			$data[$key]['tickets']['count'] = count($tickets);

			// bookings
			$bookedTicketIds = Ticket::where('status_id', config('evibe.ticket.status.booked'))
			                         ->whereBetween('paid_at', $timestamps)
			                         ->pluck('id')
			                         ->toArray();

			$bookings = TicketBooking::select('ticket_id', 'map_type_id', 'map_id', 'booking_amount')
			                         ->whereIn('ticket_id', $bookedTicketIds)
			                         ->where('is_advance_paid', 1)
			                         ->get();

			// get total unique orders = multiple orders
			// for same partner for same ticket is considered as one
			$uniqueOrders = [];
			foreach ($bookings as $booking)
			{
				$uniqueOrderKey = $booking->ticket_id . "_" . $booking->map_type_id . "_" . $booking->map_id;
				if (!array_key_exists($uniqueOrderKey, $uniqueOrders))
				{
					$uniqueOrders[$uniqueOrderKey] = 0;
				}

				$uniqueOrders[$uniqueOrderKey] = $uniqueOrders[$uniqueOrderKey] + $booking->booking_amount;
			}

			$totalUniqueOrders = count($uniqueOrders);
			$sumUniqueOrders = array_sum($uniqueOrders);

			$data[$key]['bookings']['count'] = $totalUniqueOrders;
			$data[$key]['bookings']['aov'] = $totalUniqueOrders != 0 ? round($sumUniqueOrders / $totalUniqueOrders) : 0;
		}
		$data['success'] = true;

		return response()->json($data);
	}
}