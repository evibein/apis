<?php

namespace App\Http\Controllers\CRM;

use App\Http\Controllers\BaseController;
use App\Http\Models\City;
use App\Http\Models\Ticket;
use App\Http\Models\TicketFollowups;
use App\Http\Models\TicketUpdate;
use App\Http\Models\TypeEvent;
use App\Http\Models\Types\TypeEnquirySource;
use App\Http\Models\Types\TypeLeadStatus;
use App\Http\Models\TypeTicketSource;
use App\Http\Models\TypeTicketStatus;
use App\Http\Models\User;

use App\Http\Models\TicketFollowupsType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CRMDashboardController extends BaseController
{
	public function showMetricsNewTicketsData(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;

		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$dateRequirements = $this->dateRequirements($defaultStartTime, $defaultEndTime);
		$ticketStatuses = $this->getTicketStatuses();

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForCRMDash()
		                 ->whereNotIn('ticket.status_id', $ticketStatuses["invalid"])
		                 ->whereBetween('ticket.created_at', [$dateRequirements['filterChosenStartTime'], $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);

		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();
		$ticketIds = $tickets->pluck("id");

		// Getting the followup count
		// active followups = sum (today followups + pending followups)
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $ticketIds)
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count order processed
		$totalProcessedBookingsCount = $totalBooked->where('is_auto_booked', 0)->count();

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") &&
				!in_array($item->phone, $bookedUniquePhone);
		})->unique('phone');

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$data = [
			'filterReset'                  => $showFilterReset,
			'countOfNewTickets'            => $totalNewTickets,
			'countOfBookedTickets'         => count($totalBooked),
			'totalProcessedBookingsCount'  => $totalProcessedBookingsCount,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups
		];
		$data = array_merge($data, $this->fetchGenericCount($notBookedUniqueStatusTickets->merge($totalBooked)));

		return response()->json($data);
	}

	public function showMetricsInProgressData(Request $request)
	{
		// In Progress
		// -- ticket status is in progress
		// -- AND ticket event data not set and not in end status

		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;

		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$dateRequirements = $this->dateRequirements($defaultStartTime, $defaultEndTime);

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForCRMDash()
		                 ->where('ticket.status_id', config("evibe.ticket.status.progress"))
		                 ->where(function ($query) use ($dateRequirements) {
			                 $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                       ->orWhereNull('ticket.event_date');
		                 })
		                 ->whereBetween('ticket.created_at', [Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonth(), $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);

		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();
		$ticketIds = $tickets->pluck("id");

		// Getting the followup count
		// active followups = sum (today followups + pending followups)
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $ticketIds)
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count order processed
		$totalProcessedBookingsCount = $totalBooked->where('is_auto_booked', 0)->count();

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") &&
				!in_array($item->phone, $bookedUniquePhone);
		})->unique('phone');

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$data = [
			'filterReset'                  => $showFilterReset,
			'countOfNewTickets'            => $totalNewTickets,
			'countOfBookedTickets'         => count($totalBooked),
			'totalProcessedBookingsCount'  => $totalProcessedBookingsCount,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups
		];
		$data = array_merge($data, $this->fetchGenericCount($notBookedUniqueStatusTickets->merge($totalBooked)));

		return response()->json($data);
	}

	public function showMetricsFollowupsData(Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		$showFilterReset = false;

		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$dateRequirements = $this->dateRequirements($defaultStartTime, $defaultEndTime);
		$ticketStatuses = $this->getTicketStatuses();
		$twoMonthsPast = Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonths(2);

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		// FOLLOWUP TICKETS
		// -- active followup between selected times OR followup date not set
		// -- closure date not set OR closure date not today
		// -- party date is valid (applied below)
		// -- created date is < selected start (applied below)
		// -- progress status (applied below)

		$activeFollowUpIds = TicketFollowups::whereNull('is_complete')
		                                    ->whereNull('deleted_at')
		                                    ->whereNotNull('ticket_id')
		                                    ->where(function ($query) use ($dateRequirements) {
			                                    $query->whereBetween('followup_date', [$dateRequirements['selectedStartTimeInSeconds'] - (7 * 24 * 60 * 60), $dateRequirements['selectedEndTimeInSeconds']])
			                                          ->orWhereNull('followup_date');
		                                    })
		                                    ->pluck('ticket_id')
		                                    ->toArray();

		// Not showing In progress tickets in followup since it is having seperate tab
		$returnTickets = Ticket::whereIn('ticket.id', $activeFollowUpIds)
		                       ->whereNotIn("status_id", [config("evibe.ticket.status.progress")]);

		// all NON NEW should have been created earlier than start time
		// and have active event date
		// are not in end state
		$returnTickets = $returnTickets->selectColsForCRMDash()
		                               ->whereIn('ticket.status_id', $ticketStatuses["progress"])
		                               ->where(function ($query) use ($dateRequirements) {
			                               $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                                     ->orWhereNull('ticket.event_date');
		                               })
		                               ->whereBetween('ticket.created_at', [$twoMonthsPast, $dateRequirements['filterChosenStartTime']]); // let loose super old tickets

		$filterData = $this->filterTicketsByUserInputs($returnTickets, $request);
		$returnTickets = $filterData["tickets"];
		$returnTickets = $returnTickets->get();

		// to display followup date
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $returnTickets->pluck("id"))
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		// active followups = sum (today followups + pending followups)
		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		// to display last update for the ticket
		$ticketUpdate = TicketUpdate::select('ticket_id', 'updated_at', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $returnTickets->pluck('id'))
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id');

		$ticketUpdateFilterDates = $ticketUpdate->where('updated_at', '>=', date($dateRequirements['filterChosenStartTime']))->where('updated_at', '<=', date($dateRequirements['filterChosenEndTime']));
		$totalHotBookedLeads = $returnTickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.hot'))->unique('phone')->count();
		$totalMediumBookedLeads = $returnTickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.medium'))->unique('phone')->count();

		// get counts
		$totalReturnTickets = count($returnTickets);

		$totalBooked = $this->getReturnBookingsCount($dateRequirements);

		$totalOrderProcessed = $returnTickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.confirmed') &&
				$item->success_email_type == 1 &&
				$item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$totalFollowup = $returnTickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();

		$data = [
			'filterReset'                  => $showFilterReset,
			'countOfReturnTickets'         => $totalReturnTickets,
			'countOfBookedTickets'         => $totalBooked,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			"totalHotBookedLeads"          => $totalHotBookedLeads,
			"totalMediumBookedLeads"       => $totalMediumBookedLeads,
		];

		$data = array_merge($data, $this->fetchGenericCount($returnTickets));

		return response()->json($data);
	}

	public function showNewTickets(Request $request)
	{
		// NEW TICKETS
		// -- ticket created today
		// -- OR ticket event data not set and not in end status

		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));
		$ticketStatuses = $this->getTicketStatuses();

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForCRMDash()
		                 ->whereNotIn('ticket.status_id', $ticketStatuses["invalid"])
		                 ->whereBetween('ticket.created_at', [$dateRequirements['filterChosenStartTime'], $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);

		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();
		$ticketIds = $tickets->pluck("id");

		// Getting the followup count
		// active followups = sum (today followups + pending followups)
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $ticketIds)
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		$ticketUpdate = TicketUpdate::select('ticket_id', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $ticketIds)
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id')
		                            ->pluck('comments', 'ticket_id')
		                            ->toArray();

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count order processed
		$totalProcessedBookingsCount = $totalBooked->where('is_auto_booked', 0)->count();

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") &&
				!in_array($item->phone, $bookedUniquePhone);
		})->unique('phone');

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$data = [
			'tickets'                      => $notBookedUniqueStatusTickets->merge($totalBooked)->sortByDesc('created_at'),
			'filterReset'                  => $showFilterReset,
			'ticketUpdate'                 => $ticketUpdate,
			'humanReadableStartDate'       => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'         => $dateRequirements['humanReadableEndDate'],
			'startDate'                    => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                      => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'              => $activeFollowups,
			'countOfNewTickets'            => $totalNewTickets,
			'countOfBookedTickets'         => count($totalBooked),
			'totalProcessedBookingsCount'  => $totalProcessedBookingsCount,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			'startPartyTime'               => $filterData["startPartyTime"],
			'endPartyTime'                 => $filterData["endPartyTime"],
			"followupStartTime"            => $filterData["followUpStartTime"],
			"followupEndTime"              => $filterData["followUpEndTime"],
			"closureStartTime"             => $filterData["closureStartTime"],
			"closureEndTime"               => $filterData["closureEndTime"],
			"createdAtStartTime"           => $filterData['createdAtStartTime'],
			"createdAtEndTime"             => $filterData['createdAtEndTime']
		];
		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($notBookedUniqueStatusTickets->merge($totalBooked)));

		return response()->json($data);
	}

	public function showSalesNewTickets(Request $request)
	{
		// NEW TICKETS
		// -- ticket created today
		// -- OR ticket event data not set and not in end status
		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));
		$ticketStatuses = $this->getTicketStatuses();

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForSalesDash()
		                 ->whereNotIn('ticket.status_id', $ticketStatuses["invalid"])
		                 ->whereBetween('ticket.created_at', [$dateRequirements['filterChosenStartTime'], $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);
		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") && !in_array($item->phone, $bookedUniquePhone);
		});

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);

		$totalInitiated = $tickets->where('status_id', config('evibe.ticket.status.initiated'))->unique('phone')->count();
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalJustEnquiry = $tickets->where('status_id', config('evibe.ticket.status.enquiry'))->unique('phone')->count();
		$totalNoResponse = $tickets->where('status_id', config('evibe.ticket.status.no_response'))->unique('phone')->count();

		$totalBookedByHandler = $totalBooked->where("handler_id", $userId)->count();
		$totalInitiatedByHandler = $tickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.initiated'))->unique('phone')->count();
		$totalFollowupByHandler = $tickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalJustEnquiryByHandler = $tickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.enquiry'))->unique('phone')->count();
		$totalNoResponseByHandler = $tickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.no_response'))->unique('phone')->count();

		$data = [
			'tickets'                            => $notBookedUniqueStatusTickets->merge($totalBooked)->sortByDesc('created_at'),
			'filterReset'                        => $showFilterReset,
			'humanReadableStartDate'             => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'               => $dateRequirements['humanReadableEndDate'],
			'startDate'                          => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                            => $dateRequirements['selectedEndTimeInSeconds'],
			'countOfNewTickets'                  => $totalNewTickets,
			'countOfBookedTickets'               => count($totalBooked),
			'countOfFollowUpTickets'             => $totalFollowup,
			'countOfInitiatedTickets'            => $totalInitiated,
			'countOfJustEnquiryTickets'          => $totalJustEnquiry,
			'countOfNoResponseTickets'           => $totalNoResponse,
			'countOfNewTicketsByHandler'         => $totalBookedByHandler + $totalFollowupByHandler + $totalInitiatedByHandler + $totalJustEnquiryByHandler + $totalNoResponseByHandler,
			'countOfBookedTicketsByHandler'      => $totalBookedByHandler,
			'countOfFollowUpTicketsByHandler'    => $totalFollowupByHandler,
			'countOfInitiatedTicketsByHandler'   => $totalInitiatedByHandler,
			'countOfJustEnquiryTicketsByHandler' => $totalJustEnquiryByHandler,
			'countOfNoResponseTicketsByHandler'  => $totalNoResponseByHandler,
			'startPartyTime'                     => $filterData["startPartyTime"],
			'endPartyTime'                       => $filterData["endPartyTime"],
			"followupStartTime"                  => $filterData["followUpStartTime"],
			"followupEndTime"                    => $filterData["followUpEndTime"],
			"closureStartTime"                   => $filterData["closureStartTime"],
			"closureEndTime"                     => $filterData["closureEndTime"],
			"createdAtStartTime"                 => $filterData['createdAtStartTime'],
			"createdAtEndTime"                   => $filterData['createdAtEndTime']
		];
		$data = array_merge($data, $this->fetchGenericData());

		return response()->json($data);
	}

	public function showInProgressTickets(Request $request)
	{
		// In Progress
		// -- ticket status is in progress and booked
		// -- AND ticket event data not set and not in end status

		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForCRMDash()
		                 ->where('ticket.status_id', config("evibe.ticket.status.progress"))
		                 ->where(function ($query) use ($dateRequirements) {
			                 $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                       ->orWhereNull('ticket.event_date');
		                 })
		                 ->whereBetween('ticket.created_at', [Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonth(), $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);

		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();
		$ticketIds = $tickets->pluck("id");

		// Getting the followup count
		// active followups = sum (today followups + pending followups)
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $ticketIds)
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		$ticketUpdate = TicketUpdate::select('ticket_id', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $ticketIds)
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id')
		                            ->pluck('comments', 'ticket_id')
		                            ->toArray();

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count order processed
		$totalProcessedBookingsCount = $totalBooked->where('is_auto_booked', 0)->count();

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") &&
				!in_array($item->phone, $bookedUniquePhone);
		})->unique('phone');

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$data = [
			'tickets'                      => $notBookedUniqueStatusTickets->merge($totalBooked)->sortByDesc('created_at'),
			'filterReset'                  => $showFilterReset,
			'ticketUpdate'                 => $ticketUpdate,
			'humanReadableStartDate'       => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'         => $dateRequirements['humanReadableEndDate'],
			'startDate'                    => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                      => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'              => $activeFollowups,
			'countOfNewTickets'            => $totalNewTickets,
			'countOfBookedTickets'         => count($totalBooked),
			'totalProcessedBookingsCount'  => $totalProcessedBookingsCount,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			'startPartyTime'               => $filterData["startPartyTime"],
			'endPartyTime'                 => $filterData["endPartyTime"],
			"followupStartTime"            => $filterData["followUpStartTime"],
			"followupEndTime"              => $filterData["followUpEndTime"],
			"closureStartTime"             => $filterData["closureStartTime"],
			"closureEndTime"               => $filterData["closureEndTime"],
			"createdAtStartTime"           => $filterData['createdAtStartTime'],
			"createdAtEndTime"             => $filterData['createdAtEndTime']
		];
		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($notBookedUniqueStatusTickets->merge($totalBooked)));

		return response()->json($data);
	}

	public function showHistoryTickets(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));

		$partnerId = User::where('role_id', config('evibe.role.planner'))->pluck('id')->toArray();

		$ticketUpdateIds = TicketUpdate::whereBetween('updated_at', [$dateRequirements['filterChosenStartTime'], $dateRequirements['filterChosenEndTime']])
		                               ->whereNotIn('handler_id', $partnerId)
		                               ->whereNotNull('handler_id')
		                               ->get()
		                               ->unique('ticket_id')
		                               ->pluck('ticket_id')
		                               ->toArray();

		$previousState = TicketUpdate::whereIn('ticket_id', $ticketUpdateIds)
		                             ->whereNotNull('handler_id')
		                             ->where('updated_at', '<', $dateRequirements['filterChosenStartTime'])
		                             ->orderBy('updated_at', 'desc')
		                             ->get()
		                             ->unique('ticket_id')
		                             ->pluck('status_id', 'ticket_id')
		                             ->toArray();

		$tickets = Ticket::whereIn('id', $ticketUpdateIds);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);
		$tickets = $filterData["tickets"];
		$tickets = $tickets->get();

		$activeFollowups = TicketFollowups::whereIn("ticket_id", $tickets->pluck("id"))
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		// active followups = sum (today followups + pending followups)
		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		// to display last update for the ticket
		$ticketUpdate = TicketUpdate::select('ticket_id', 'updated_at', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $tickets->pluck('id'))
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id');

		$ticketUpdateFilterDates = $ticketUpdate->where('updated_at', '>=', date($dateRequirements['filterChosenStartTime']))->where('updated_at', '<=', date($dateRequirements['filterChosenEndTime']));
		$totalHotBookedLeads = $tickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.hot'))->unique('phone')->count();
		$totalMediumBookedLeads = $tickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.medium'))->unique('phone')->count();
		$ticketUpdate = $ticketUpdate->pluck('comments', 'ticket_id')->toArray();

		// get counts
		$totalReturnTickets = count($tickets);

		$totalBooked = $this->getReturnBookingsCount($dateRequirements);

		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.confirmed') &&
				$item->success_email_type == 1 &&
				$item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();

		$data = [
			'tickets'                      => $tickets,
			'filterReset'                  => $showFilterReset,
			'activeFollowUps'              => $activeFollowups,
			'ticketUpdate'                 => $ticketUpdate,
			'humanReadableStartDate'       => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'         => $dateRequirements['humanReadableEndDate'],
			'startDate'                    => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                      => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'              => $activeFollowups,
			'countOfReturnTickets'         => $totalReturnTickets,
			'countOfBookedTickets'         => $totalBooked,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			"totalHotBookedLeads"          => $totalHotBookedLeads,
			"totalMediumBookedLeads"       => $totalMediumBookedLeads,
			"followupStartTime"            => $filterData["followUpStartTime"],
			"followupEndTime"              => $filterData["followUpEndTime"],
			'startPartyTime'               => $filterData["startPartyTime"],
			'endPartyTime'                 => $filterData["endPartyTime"],
			"closureStartTime"             => $filterData["closureStartTime"],
			"closureEndTime"               => $filterData["closureEndTime"],
			"previousState"                => $previousState,
			"createdAtStartTime"           => $filterData['createdAtStartTime'],
			"createdAtEndTime"             => $filterData['createdAtEndTime']
		];

		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($tickets));

		return response()->json($data);
	}

	public function showReturnTickets(Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if ($request->is('*/missed*'))
		{
			$typeRequest = "MISSED";
		}
		elseif ($request->is('*/prospect*'))
		{
			$typeRequest = "PROSPECT";
		}
		else
		{
			$typeRequest = "FOLLOWUP";
		}

		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));
		$ticketStatuses = $this->getTicketStatuses();
		$twoMonthsPast = Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonths(2);

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		if ($typeRequest == "PROSPECT")
		{
			// PROSPECTS TICKETS
			// -- closure date between start & end time
			// -- party date is valid (applied below)
			// -- created date is < selected start (applied below)
			// -- progress status (applied below)

			$returnTickets = Ticket::where(function ($iq) use ($dateRequirements) {
				$iq->whereBetween('ticket.expected_closure_date', [$dateRequirements['selectedStartTimeInSeconds'], $dateRequirements['selectedEndTimeInSeconds']])
				   ->whereNotNull("ticket.expected_closure_date");
			});
		}
		elseif ($typeRequest == "FOLLOWUP")
		{
			// FOLLOWUP TICKETS
			// -- active followup between selected times OR followup date not set
			// -- closure date not set OR closure date not today
			// -- party date is valid (applied below)
			// -- created date is < selected start (applied below)
			// -- progress status (applied below)

			$activeFollowUpIds = TicketFollowups::whereNull('is_complete')
			                                    ->whereNull('deleted_at')
			                                    ->whereNotNull('ticket_id')
			                                    ->where(function ($query) use ($dateRequirements) {
				                                    $query->whereBetween('followup_date', [$dateRequirements['selectedStartTimeInSeconds'] - (7 * 24 * 60 * 60), $dateRequirements['selectedEndTimeInSeconds']])
				                                          ->orWhereNull('followup_date');
			                                    })
			                                    ->pluck('ticket_id')
			                                    ->toArray();

			// Not showing In progress tickets in followup since it is having seperate tab
			$returnTickets = Ticket::whereIn('ticket.id', $activeFollowUpIds)
			                       ->whereNotIn("status_id", [config("evibe.ticket.status.progress")]);
		}
		else
		{
			// MISSED TICKETS
			// -- missed follow-up
			// -- missed closure
			// -- party date is valid (applied below)
			// -- created date is < selected start (applied below)
			// -- progress status (applied below)
			$lapsedFollowupTicketIds = TicketFollowups::whereNull('is_complete')
			                                          ->where('followup_date', '<=', $dateRequirements['selectedStartTimeInSeconds'])
			                                          ->whereNull('deleted_at')
			                                          ->whereNotNull('ticket_id')
			                                          ->pluck('ticket_id')
			                                          ->toArray();

			$returnTickets = Ticket::where(function ($iq) use ($dateRequirements, $lapsedFollowupTicketIds, $twoMonthsPast) {
				$iq->whereIn('ticket.id', $lapsedFollowupTicketIds)
				   ->where('created_at', '>', $twoMonthsPast)
				   ->orWhere('ticket.expected_closure_date', '<=', $dateRequirements['selectedStartTimeInSeconds']);
			})
			                       ->orWhere(function ($iq2) use ($ticketStatuses, $twoMonthsPast) {
				                       $iq2->where("ticket.created_at", ">", $twoMonthsPast)
				                           ->whereNull('ticket.event_date')
				                           ->whereNotIn("ticket.status_id", $ticketStatuses["end"]);
			                       });
		}

		// all NON NEW should have been created earlier than start time
		// and have active event date
		// are not in end state
		$returnTickets = $returnTickets->selectColsForCRMDash()
		                               ->whereIn('ticket.status_id', $ticketStatuses["followup"])
		                               ->where(function ($query) use ($dateRequirements) {
			                               $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                                     ->orWhereNull('ticket.event_date');
		                               })
		                               ->whereBetween('ticket.created_at', [$twoMonthsPast, $dateRequirements['filterChosenStartTime']]); // let loose super old tickets

		$filterData = $this->filterTicketsByUserInputs($returnTickets, $request);
		$returnTickets = $filterData["tickets"];
		$returnTickets = $returnTickets->get();

		//if ($typeRequest == "FOLLOWUP")
		//{
		//	$returnTickets = $returnTickets->filter(function ($item) use ($dateRequirements) {
		//		return is_null($item->expected_closure_date) ||
		//			(
		//				!is_null($item->expected_closure_date) &&
		//				($item->expected_closure_date <= $dateRequirements['selectedStartTimeInSeconds'] ||
		//					$item->expected_closure_date >= $dateRequirements['selectedEndTimeInSeconds'])
		//			);
		//	});
		//}

		// to display followup date
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $returnTickets->pluck("id"))
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		// active followups = sum (today followups + pending followups)
		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		// to display last update for the ticket
		$ticketUpdate = TicketUpdate::select('ticket_id', 'updated_at', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $returnTickets->pluck('id'))
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id');

		$ticketUpdateFilterDates = $ticketUpdate->where('updated_at', '>=', date($dateRequirements['filterChosenStartTime']))->where('updated_at', '<=', date($dateRequirements['filterChosenEndTime']));
		$totalHotBookedLeads = $returnTickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.hot'))->unique('phone')->count();
		$totalMediumBookedLeads = $returnTickets->whereIn('id', $ticketUpdateFilterDates->pluck('ticket_id'))->where('lead_status_id', config('evibe.ticket.leadStatus.medium'))->unique('phone')->count();
		$ticketUpdate = $ticketUpdate->pluck('comments', 'ticket_id')->toArray();

		// get counts
		$totalReturnTickets = count($returnTickets);

		$totalBooked = $this->getReturnBookingsCount($dateRequirements);

		$totalOrderProcessed = $returnTickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.confirmed') &&
				$item->success_email_type == 1 &&
				$item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$totalFollowup = $returnTickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();

		$data = [
			'tickets'                      => $returnTickets,
			'filterReset'                  => $showFilterReset,
			'activeFollowUps'              => $activeFollowups,
			'ticketUpdate'                 => $ticketUpdate,
			'humanReadableStartDate'       => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'         => $dateRequirements['humanReadableEndDate'],
			'startDate'                    => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                      => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'              => $activeFollowups,
			'countOfReturnTickets'         => $totalReturnTickets,
			'countOfBookedTickets'         => $totalBooked,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			"totalHotBookedLeads"          => $totalHotBookedLeads,
			"totalMediumBookedLeads"       => $totalMediumBookedLeads,
			"followupStartTime"            => $filterData["followUpStartTime"],
			"followupEndTime"              => $filterData["followUpEndTime"],
			'startPartyTime'               => $filterData["startPartyTime"],
			'endPartyTime'                 => $filterData["endPartyTime"],
			"closureStartTime"             => $filterData["closureStartTime"],
			"closureEndTime"               => $filterData["closureEndTime"],
			"createdAtStartTime"           => $filterData['createdAtStartTime'],
			"createdAtEndTime"             => $filterData['createdAtEndTime']
		];

		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($returnTickets));

		return response()->json($data);
	}

	public function showSalesFollowupTickets(Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));
		$ticketStatuses = $this->getTicketStatuses();
		$twoMonthsPast = Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonths(2);

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		// FOLLOWUP TICKETS
		// -- active followup between selected times OR followup date not set
		// -- closure date not set OR closure date not today
		// -- party date is valid (applied below)
		// -- created date is < selected start (applied below)
		// -- progress status (applied below)
		$activeFollowUpIds = TicketFollowups::whereNull('is_complete')
		                                    ->whereNull('deleted_at')
		                                    ->whereNotNull('ticket_id')
		                                    ->where(function ($query) use ($dateRequirements) {
			                                    $query->whereBetween('followup_date', [$dateRequirements['selectedStartTimeInSeconds'] - (7 * 24 * 60 * 60), $dateRequirements['selectedEndTimeInSeconds']])
			                                          ->orWhereNull('followup_date');
		                                    })
		                                    ->pluck('ticket_id')
		                                    ->toArray();

		$totalBooked = Ticket::select("id", "handler_id")
		                     ->whereBetween('paid_at', [$dateRequirements['selectedStartTimeInSeconds'], $dateRequirements['selectedEndTimeInSeconds']])
		                     ->where('created_at', "<", $dateRequirements['filterChosenStartTime'])
		                     ->where('status_id', config('evibe.ticket.status.booked'))
		                     ->get();

		// Not showing In progress tickets in followup since it is having seperate tab
		$returnTickets = Ticket::whereIn('ticket.id', array_merge($activeFollowUpIds, $totalBooked->pluck("id")->toArray()))
		                       ->whereNotIn("status_id", [config("evibe.ticket.status.progress")])
		                       ->selectColsForCRMDash()
		                       ->whereIn('ticket.status_id', $ticketStatuses["followup"])
		                       ->where(function ($query) use ($dateRequirements) {
			                       $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                             ->orWhereNull('ticket.event_date');
		                       })
		                       ->whereBetween('ticket.created_at', [$twoMonthsPast, $dateRequirements['filterChosenStartTime']]); // let loose super old tickets

		$filterData = $this->filterTicketsByUserInputs($returnTickets, $request);
		$returnTickets = $filterData["tickets"];
		$returnTickets = $returnTickets->get();

		// to display followup date
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $returnTickets->pluck("id"))
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$totalInitiated = $returnTickets->where('status_id', config('evibe.ticket.status.initiated'))->unique('phone')->count();
		$totalFollowup = $returnTickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalJustEnquiry = $returnTickets->where('status_id', config('evibe.ticket.status.enquiry'))->unique('phone')->count();
		$totalNoResponse = $returnTickets->where('status_id', config('evibe.ticket.status.no_response'))->unique('phone')->count();

		$totalBookedByHandler = $totalBooked->where("handler_id", $userId)->count();
		$totalInitiatedByHandler = $returnTickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.initiated'))->unique('phone')->count();
		$totalFollowupByHandler = $returnTickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalJustEnquiryByHandler = $returnTickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.enquiry'))->unique('phone')->count();
		$totalNoResponseByHandler = $returnTickets->where("handler_id", $userId)->where('status_id', config('evibe.ticket.status.no_response'))->unique('phone')->count();

		$data = [
			'tickets'                               => $returnTickets,
			'filterReset'                           => $showFilterReset,
			'activeFollowUps'                       => $activeFollowups,
			'humanReadableStartDate'                => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'                  => $dateRequirements['humanReadableEndDate'],
			'startDate'                             => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                               => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'                       => $activeFollowups,
			'countOfBookedTickets'                  => $totalBooked->count(),
			'countOfBookedTicketsByHandler'         => $totalBookedByHandler,
			'countOfFollowUpReturnTickets'          => $returnTickets->count(),
			'countOfFollowUpReturnTicketsByHandler' => $returnTickets->where("handler_id", $userId)->count(),
			'countOfInitiatedTickets'               => $totalInitiated,
			'countOfFollowUpTickets'                => $totalFollowup,
			'countOfJustEnquiryTickets'             => $totalJustEnquiry,
			'countOfNoResponseTickets'              => $totalNoResponse,
			'countOfInitiatedTicketsByHandler'      => $totalInitiatedByHandler,
			'countOfFollowUpTicketsByHandler'       => $totalFollowupByHandler,
			'countOfJustEnquiryTicketsByHandler'    => $totalJustEnquiryByHandler,
			'countOfNoResponseTicketsByHandler'     => $totalNoResponseByHandler,
			"followupStartTime"                     => $filterData["followUpStartTime"],
			"followupEndTime"                       => $filterData["followUpEndTime"],
			'startPartyTime'                        => $filterData["startPartyTime"],
			'endPartyTime'                          => $filterData["endPartyTime"],
			"closureStartTime"                      => $filterData["closureStartTime"],
			"closureEndTime"                        => $filterData["closureEndTime"],
			"createdAtStartTime"                    => $filterData['createdAtStartTime'],
			"createdAtEndTime"                      => $filterData['createdAtEndTime']
		];

		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($returnTickets));

		return response()->json($data);
	}

	public function showSalesInProgressTickets(Request $request)
	{
		// In Progress
		// -- ticket status is in progress
		// -- AND ticket event data not set and not in end status
		$userId = $this->checkForAccessToken($request);
		$showFilterReset = false;
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));

		if ($request->input('filterChosenStartingTime') || $request->input('filterChosenEndingTime'))
		{
			$showFilterReset = true;
		}

		$tickets = Ticket::selectColsForCRMDash()
		                 ->where('ticket.status_id', config("evibe.ticket.status.progress"))
		                 ->where(function ($query) use ($dateRequirements) {
			                 $query->where('ticket.event_date', '>=', $dateRequirements['selectedStartTimeInSeconds'])
			                       ->orWhereNull('ticket.event_date');
		                 })
		                 ->whereBetween('ticket.created_at', [Carbon::parse($dateRequirements['filterChosenStartTime'])->subMonth(), $dateRequirements['filterChosenEndTime']]);

		$filterData = $this->filterTicketsByUserInputs($tickets, $request);

		$tickets = $filterData['tickets'];
		$tickets = $tickets->get();
		$ticketIds = $tickets->pluck("id");

		// Getting the followup count
		// active followups = sum (today followups + pending followups)
		$activeFollowups = TicketFollowups::whereIn("ticket_id", $ticketIds)
		                                  ->whereNull('is_complete')
		                                  ->get()
		                                  ->unique('ticket_id')
		                                  ->pluck('followup_date', 'ticket_id')
		                                  ->toArray();

		$todayFollowups = count(array_filter($activeFollowups, function ($item) {
			return $item >= Carbon::now()->timestamp;
		}));

		$pendingFollowups = count($activeFollowups) - $todayFollowups;

		$ticketUpdate = TicketUpdate::select('ticket_id', DB::raw("CONCAT(comments, '~', DATE_FORMAT(created_at, '%y/%m/%d %H:%i:%s')) AS comments"))
		                            ->whereIn('ticket_id', $ticketIds)
		                            ->orderBy('updated_at', 'DESC')
		                            ->get()
		                            ->unique('ticket_id')
		                            ->pluck('comments', 'ticket_id')
		                            ->toArray();

		// split tickets between booked and non booked
		// this is to avoid multiple tickets with same phone number for not booked tickets
		$totalBooked = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->status_id == config('evibe.ticket.status.booked') &&
				$item->paid_at >= $dateRequirements['selectedStartTimeInSeconds'] &&
				$item->paid_at <= $dateRequirements['selectedEndTimeInSeconds'];
		});

		// Count order processed
		$totalProcessedBookingsCount = $totalBooked->where('is_auto_booked', 0)->count();

		// Count Bookings
		$bookedUniquePhone = $totalBooked->unique('phone')->pluck('phone')->toArray();

		// If already booked, do not consider it for non-booked
		$notBookedUniqueStatusTickets = $tickets->filter(function ($item) use ($bookedUniquePhone) {
			return $item->status_id != config("evibe.ticket.status.booked") &&
				!in_array($item->phone, $bookedUniquePhone);
		})->unique('phone');

		// Counts
		$totalNewTickets = count($totalBooked) + count($notBookedUniqueStatusTickets);
		$totalFollowup = $tickets->where('status_id', config('evibe.ticket.status.followup'))->unique('phone')->count();
		$totalOrderProcessed = $tickets->filter(function ($item) use ($dateRequirements) {
			return $item->success_email_sent_at >= $dateRequirements['filterChosenStartTime'] &&
				$item->success_email_sent_at <= $dateRequirements['filterChosenEndTime'];
		})->count();

		$data = [
			'tickets'                      => $notBookedUniqueStatusTickets->merge($totalBooked)->sortByDesc('created_at'),
			'filterReset'                  => $showFilterReset,
			'ticketUpdate'                 => $ticketUpdate,
			'humanReadableStartDate'       => $dateRequirements['humanReadableStartDate'],
			'humanReadableEndDate'         => $dateRequirements['humanReadableEndDate'],
			'startDate'                    => $dateRequirements['selectedStartTimeInSeconds'],
			'endDate'                      => $dateRequirements['selectedEndTimeInSeconds'],
			'activeFollowups'              => $activeFollowups,
			'countOfNewTickets'            => $totalNewTickets,
			'countOfBookedTickets'         => count($totalBooked),
			'totalProcessedBookingsCount'  => $totalProcessedBookingsCount,
			'countOfFollowUpTickets'       => $totalFollowup,
			'countOfOrderProcessedTickets' => $totalOrderProcessed,
			'countOfTodayFollowUps'        => $todayFollowups,
			'countOfPendingFollowUps'      => $pendingFollowups,
			'startPartyTime'               => $filterData["startPartyTime"],
			'endPartyTime'                 => $filterData["endPartyTime"],
			"followupStartTime"            => $filterData["followUpStartTime"],
			"followupEndTime"              => $filterData["followUpEndTime"],
			"closureStartTime"             => $filterData["closureStartTime"],
			"closureEndTime"               => $filterData["closureEndTime"],
			"createdAtStartTime"           => $filterData['createdAtStartTime'],
			"createdAtEndTime"             => $filterData['createdAtEndTime']
		];
		$data = array_merge($data, $this->fetchGenericData());
		$data = array_merge($data, $this->fetchGenericCount($notBookedUniqueStatusTickets->merge($totalBooked)));

		return response()->json($data);
	}

	public function showReturnTicketUpgrades(Request $request, $ticketId)
	{
		$userId = $this->checkForAccessToken($request);
		if ($request->is('*/history*'))
		{
			$typeRequest = "HISTORY";
		}
		else
		{
			$typeRequest = "RETURN";
		}
		$dateRequirements = $this->dateRequirements($request->input('filterChosenStartingTime'), $request->input('filterChosenEndingTime'));

		$ticketUpdates = TicketUpdate::where('ticket_id', $ticketId)
		                             ->orderBy('updated_at', 'DESC');
		if ($typeRequest == "HISTORY")
		{
			// Do nothing
		}
		else
		{
			// Get all the tickets which got updated today
			$ticketUpdates = $ticketUpdates->whereBetween('updated_at', [$dateRequirements['filterChosenStartTime'], $dateRequirements['filterChosenEndTime']]);
		}
		$ticketUpdates = $ticketUpdates->get();
		$typeTicketStatus = TypeTicketStatus::all()->pluck("name", "id");
		$users = User::whereIn('role_id', [config("evibe.role.customer_delight"), config("evibe.role.sr_crm"), config("evibe.role.admin")])
		             ->whereNull('deleted_at')
		             ->pluck("name", "id");

		$updates = [];
		foreach ($ticketUpdates as $ticketUpdate)
		{
			$updates[] = [
				"status"        => $ticketUpdate->status_id ? (isset($typeTicketStatus[$ticketUpdate->status_id]) ? $typeTicketStatus[$ticketUpdate->status_id] : "") : '--',
				"handler"       => $ticketUpdate->handler_id ? (isset($users[$ticketUpdate->handler_id]) ? $users[$ticketUpdate->handler_id] : "Non CRM") : "Auto",
				"updated_at"    => $ticketUpdate->status_at ? date('h:i A d M Y', $ticketUpdate->status_at) : "",
				"update_source" => $ticketUpdate->type_update ?: "Auto",
				"comments"      => $ticketUpdate->comments ?: "--",
			];
		}
		$data = ["ticketUpdates" => $updates];
		$data['startTime'] = $dateRequirements['selectedStartTimeInSeconds'];

		return response()->json($data);
	}

	private function fetchGenericData()
	{
		// CRM, SR_CRM & ADMIN users
		$users = User::whereIn('role_id', [config("evibe.role.customer_delight"), config("evibe.role.sr_crm"), config("evibe.role.admin"), config("evibe.role.ops_agent"), config("evibe.role.ops_tl"), config("evibe.role.bd_agent"), config("evibe.role.bd_tl")])
		             ->whereNull('deleted_at')
		             ->pluck("name", "id");

		return [
			"cityDetails"       => City::all()->pluck("name", "id"),
			"eventTypeDetails"  => TypeEvent::all()->pluck("name", "id"),
			"typeLeadStatus"    => TypeLeadStatus::all()->pluck("name", "id"),
			"typeTicketSource"  => TypeTicketSource::all()->pluck("name", "id"),
			"typeTicketStatus"  => TypeTicketStatus::all()->pluck("name", "id"),
			"typeEnquirySource" => TypeEnquirySource::all()->pluck("name", "id"),
			"typeEvents"        => TypeEvent::all()->pluck("name", "id"),
			"bookingLikeliness" => config('evibe.ticket.bookingLikeliness'),
			"handlerDetails"    => $users,
			"typeFollowUp"      => TicketFollowupsType::all()->pluck("name", 'id'),
		];
	}

	private function fetchGenericCount($tickets)
	{
		$totalHotLeads = $tickets->where('lead_status_id', config('evibe.ticket.leadStatus.hot'))->unique('phone')->count();
		$totalMediumLeads = $tickets->where('lead_status_id', config('evibe.ticket.leadStatus.medium'))->unique('phone')->count();
		$totalColdLeads = $tickets->where('lead_status_id', config('evibe.ticket.leadStatus.cold'))->unique('phone')->count();
		$totalNotSetLeads = $tickets->where('lead_status_id', '')->unique('phone')->count();

		$totalInitiated = $tickets->where('status_id', config('evibe.ticket.status.initiated'))->unique('phone')->count();
		$totalJustEnquiry = $tickets->where('status_id', config('evibe.ticket.status.enquiry'))->unique('phone')->count();
		$totalNoResponse = $tickets->where('status_id', config('evibe.ticket.status.no_response'))->unique('phone')->count();

		return [
			"totalHotLeads"             => $totalHotLeads,
			"totalMediumLeads"          => $totalMediumLeads,
			"totalColdLeads"            => $totalColdLeads,
			"totalNotSetLeads"          => $totalNotSetLeads,
			'countOfInitiatedTickets'   => $totalInitiated,
			'countOfJustEnquiryTickets' => $totalJustEnquiry,
			'countOfNoResponseTickets'  => $totalNoResponse
		];
	}

	private function dateRequirements($startTime = null, $endTime = null)
	{
		$defaultStartTime = Carbon::yesterday()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);
		$defaultEndTime = Carbon::today()->endOfDay()->subHours(1)->subMinutes(59)->subSeconds(59);

		$filterChosenStartTime = $startTime ? date('Y-m-d H:i:s', strtotime($startTime)) : $defaultStartTime;
		$filterChosenEndTime = $endTime ? date('Y-m-d H:i:s', strtotime($endTime)) : $defaultEndTime;

		$selectedStartTimeInSeconds = strtotime($filterChosenStartTime);
		$selectedEndTimeInSeconds = strtotime($filterChosenEndTime);

		$thirtyDaysInSeconds = 2592000;

		//Server side validation as to show only 30 days data
		if (($selectedEndTimeInSeconds - $selectedStartTimeInSeconds) >= $thirtyDaysInSeconds)
		{
			$selectedStartTimeInSeconds = $selectedEndTimeInSeconds - $thirtyDaysInSeconds;
			$filterChosenStartTime = date('Y-m-d H:i:s', $selectedStartTimeInSeconds);
		}

		$humanReadableStartDate = date("F j, Y, g:i A", $selectedStartTimeInSeconds);
		$humanReadableEndDate = date("F j, Y, g:i A", $selectedEndTimeInSeconds);

		$data = [
			'defaultStartTime'           => $defaultStartTime,
			'defaultEndTime'             => $defaultEndTime,
			'filterChosenStartTime'      => $filterChosenStartTime,
			'filterChosenEndTime'        => $filterChosenEndTime,
			'selectedStartTimeInSeconds' => $selectedStartTimeInSeconds,
			'selectedEndTimeInSeconds'   => $selectedEndTimeInSeconds,
			'humanReadableStartDate'     => $humanReadableStartDate,
			'humanReadableEndDate'       => $humanReadableEndDate
		];

		return $data;
	}

	private function filterTicketsByUserInputs($tickets, $request)
	{
		// FILTER: Lead Status
		$userSelectedLeadStatus = $request->input('leadStatus');
		if ((($userSelectedLeadStatus && $userSelectedLeadStatus != 'all')) && ($userSelectedLeadStatus != 'notSet'))
		{
			$tickets = $tickets->where('lead_status_id', intval($userSelectedLeadStatus));
		}
		if ($userSelectedLeadStatus && $userSelectedLeadStatus == "notSet")
		{
			$tickets = $tickets->where('lead_status_id', null);
		}

		// FILTER: Current Handler
		$userSelectedHandler = $request->input('handler');
		if ($userSelectedHandler && $userSelectedHandler != 'all')
		{
			$tickets = $tickets->where('handler_id', intval($userSelectedHandler));
		}

		// FILTER: Booking Likeliness
		$userBookingLikeliness = $request->input('bookingLikeliness');
		if ((($userBookingLikeliness && $userBookingLikeliness != 'all')) && ($userBookingLikeliness != 'notSet'))
		{
			$tickets = $tickets->where('booking_likeliness_id', intval($userBookingLikeliness));
		}

		// FILTER: Created Handler
		$userSelectedCreatedByHandler = $request->input('createdByHandler');
		if ($userSelectedCreatedByHandler && $userSelectedCreatedByHandler != 'all')
		{
			$tickets = $tickets->where('created_handler_id', intval($userSelectedCreatedByHandler));
		}

		// FILTER: Ticket Status
		$userSelectedTicketStatus = $request->input('ticketStatus');
		if ($userSelectedTicketStatus && $userSelectedTicketStatus != 'all')
		{
			$tickets = $tickets->where('status_id', intval($userSelectedTicketStatus));
		}

		// FILTER: Ticket Occasion
		$userSelectedOccasion = $request->input('occasion');
		if ($userSelectedOccasion && $userSelectedOccasion != 'all')
		{
			$tickets = $tickets->where('event_id', intval($userSelectedOccasion));
		}

		// FILTER: Ticket Enquiry
		$userSelectedEnquirySource = $request->input('enquirySource');
		if ($userSelectedEnquirySource)
		{
			if ($userSelectedEnquirySource == "scheduled")
			{
				$tickets = $tickets->where('enquiry_source_id', '!=', config('evibe.ticket.enquiry_source.phone'));
			}
			elseif ($userSelectedEnquirySource == 'all')
			{
				// do nothing
			}
			else
			{
				$tickets = $tickets->where('enquiry_source_id', intval($userSelectedEnquirySource));
			}
		}

		// FILTER: Customer Source
		$userSelectedCustomerSource = $request->input('customerSource');
		if ($userSelectedCustomerSource && $userSelectedCustomerSource != "all")
		{
			$tickets = $tickets->where('source_id', intval($userSelectedCustomerSource));
		}

		// FILTER: Party Time
		$startPartyTime = $request->input('partyStartTime');
		$endPartyTime = $request->input('partyEndTime');
		if ($startPartyTime && $endPartyTime)
		{
			$startPartyTime = strtotime(date('Y-m-d H:i:s', strtotime($startPartyTime)));
			$endPartyTime = strtotime(date('Y-m-d H:i:s', strtotime($endPartyTime)));
			$tickets = $tickets->whereBetween('event_date', [$startPartyTime, $endPartyTime]);
		}

		// FILTER: Followup Time
		$followUpStartTime = $request->input(('followupStartTime'));
		$followUpEndTime = $request->input(('followupEndTime'));
		if ($followUpStartTime && $followUpEndTime)
		{
			$followUpStartTime = strtotime(date('Y-m-d H:i:s', strtotime($followUpStartTime)));
			$followUpEndTime = strtotime(date('Y-m-d H:i:s', strtotime($followUpEndTime)));
			$tickets = $tickets->join('ticket_followups', 'ticket.id', '=', 'ticket_followups.ticket_id')
			                   ->whereBetween('ticket_followups.followup_date', [$followUpStartTime, $followUpEndTime]);
		}

		// FILTER: Followup Type
		$ticketFollowUpType = $request->input('followUpType');
		if ($ticketFollowUpType && $ticketFollowUpType != "all")
		{
			$tickets = $tickets->join('ticket_followups', 'ticket.id', '=', 'ticket_followups.ticket_id')
			                   ->where(function ($query) use ($ticketFollowUpType) {
				                   $query->where('ticket_followups.comments', 'like', '%' . lcfirst($ticketFollowUpType) . '%')
				                         ->orWhere('ticket_followups.comments', 'like', '%' . ucfirst($ticketFollowUpType) . '%');
			                   });

		}

		// FILTER: Closure Time
		$closureStartTime = $request->input('closureStartTime');
		$closureEndTime = $request->input('closureEndTime');
		if ($closureStartTime && $closureEndTime)
		{
			$closureStartTime = strtotime(date('Y-m-d H:i:s', strtotime($closureStartTime)));
			$closureEndTime = strtotime(date('Y-m-d H:i:s', strtotime($closureEndTime)));
			$tickets = $tickets->whereBetween('expected_closure_date', [$closureStartTime, $closureEndTime]);
		}

		$createdAtStartTime = $request->input('createdAtStartTime');
		$createdAtEndTime = $request->input('createdAtEndTime');
		if ($createdAtStartTime && $createdAtEndTime)
		{
			$createdAtStartTime = date('Y-m-d H:i:s', strtotime($createdAtStartTime));
			$createdAtEndTime = date('Y-m-d H:i:s', strtotime($createdAtEndTime));
			$tickets = $tickets->whereBetween('created_at', [$createdAtStartTime, $createdAtEndTime]);
		}

		return [
			"tickets"            => $tickets,
			"startPartyTime"     => $startPartyTime,
			"endPartyTime"       => $endPartyTime,
			"followUpStartTime"  => $followUpStartTime,
			"followUpEndTime"    => $followUpEndTime,
			"closureStartTime"   => $closureStartTime,
			"closureEndTime"     => $closureEndTime,
			"createdAtStartTime" => $createdAtStartTime,
			"createdAtEndTime"   => $createdAtEndTime
		];
	}

	private function getReturnBookingsCount($dates)
	{
		$bookingsCount = Ticket::whereBetween('paid_at', [$dates['selectedStartTimeInSeconds'], $dates['selectedEndTimeInSeconds']])
		                       ->where('created_at', "<", $dates['filterChosenStartTime'])
		                       ->where('status_id', config('evibe.ticket.status.booked'))
		                       ->get()
		                       ->count();

		return $bookingsCount;
	}

	public function showAvailChecks(Request $request)
	{
		try
		{
			$data = $this->fetchAvailabilityCheckList($request);

			return response()->json($data);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}
}