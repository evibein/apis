<?php

namespace App\Http\Controllers\Partner;

use App\Http\Models\Area;
use App\Http\Models\City;
use App\Http\Models\DeliveryGallery;
use App\Http\Models\TicketBookingGallery;
use App\Models\Review\ReviewReply;
use App\Models\Review\TypePartnerReviewQuestion;
use App\Models\Types\TypeBookingConcept;
use Carbon\Carbon;
use App\Models\Review\Review;
use App\Http\Models\TicketBooking;
use App\Models\Review\PartnerReviewAnswer;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class PartnerDigestController extends BasePartnerController
{
	public function getReportData($providerId, $providerTypeId, Request $request)
	{
		$startDay = $request->input("from");
		$endDay = $request->input("to");
		$token = $request->input("token");

		// validate request using tokens
		if (!Hash::check($startDay . $endDay, $token))
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'Access denied'
			                        ]);
		}

		// either party completed in this week (or) new order
		$bookings = TicketBooking::select('ticket_bookings.*', 'ticket.name AS customer_name', 'ticket.paid_at AS paid_at', 'ticket.area_id AS ticket_area_id')
		                         ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
		                         ->where('ticket_bookings.is_advance_paid', 1)
		                         ->where('ticket.status_id', config('evibe.ticket.status.booked'))
		                         ->where('ticket_bookings.map_type_id', $providerTypeId)
		                         ->where('ticket_bookings.map_id', $providerId)
		                         ->where(function ($query) use ($startDay, $endDay) {
			                         $query->whereBetween('ticket_bookings.party_date_time', [$startDay, $endDay])
			                               ->orWhereBetween('ticket.paid_at', [$startDay, $endDay]);
		                         })
		                         ->whereNull('ticket.deleted_at')
		                         ->whereNull('ticket_bookings.cancelled_at')
		                         ->whereNull('ticket_bookings.deleted_at')
		                         ->get();

		if (!$bookings->count())
		{
			return response()->json([
				                        'success' => false,
				                        'error'   => 'No valid bookings found'
			                        ]);
		}

		// @see: since $bookings is now collection, we need cannot use where with ">" or "<"
		$newOrders = $bookings->filter(function ($booking, $key) use ($startDay, $endDay) {
			return ($booking->paid_at >= $startDay && $booking->paid_at <= $endDay);
		});

		$completedOrders = $bookings->filter(function ($booking, $key) use ($startDay, $endDay) {
			return ($booking->party_date_time >= $startDay && $booking->party_date_time <= $endDay);
		});

		// @see: for deliveries, we are not restricting to that week
		$totalDelivered = $completedOrders->filter(function ($booking, $key) use ($startDay, $endDay) {
			$deliveryDoneAt = $booking->delivery_done_at;
			$startDayTimestamp = Carbon::createFromTimestamp($startDay);

			return (!is_null($deliveryDoneAt) && $deliveryDoneAt >= $startDayTimestamp);
		});

		$completedOrderIds = $completedOrders->pluck("id")->toArray();
		$newOrderIds = $newOrders->pluck("id")->toArray();
		$reviewQuestions = null;
		$reviewAnswers = null;
		$reviewReply = null;

		$bookingConcepts = TypeBookingConcept::whereIn('id', $completedOrders->pluck("type_booking_concept_id")->toArray())
		                                     ->get();

		$bookingGallery = TicketBookingGallery::whereIn('ticket_booking_id', array_merge($completedOrderIds, $newOrderIds))
		                                      ->get();

		$deliveryGallery = DeliveryGallery::whereIn('ticket_booking_id', $completedOrderIds)
		                                  ->get();

		$areaIds = array_unique(array_merge($completedOrders->pluck("ticket.area_id")->toArray(), $newOrders->pluck("ticket.area_id")->toArray()));
		$cityIds = array_unique(array_merge($completedOrders->pluck("ticket.city_id")->toArray(), $newOrders->pluck("ticket.city_id")->toArray()));

		$areas = Area::whereIn('id', $areaIds)
		             ->get();

		$cities = City::whereIn('id', $cityIds)
		              ->get();

		$reviews = Review::where('is_accepted', 1)
		                 ->whereIn('ticket_booking_id', $completedOrderIds)
		                 ->get();

		if ($reviews)
		{
			$reviewAnswers = PartnerReviewAnswer::whereIn('review_id', $reviews->pluck("id")->toArray())
			                                    ->get();

			$reviewQuestions = TypePartnerReviewQuestion::whereIn('id', $reviewAnswers->pluck('question_id')->toArray())
			                                            ->get();

			$reviewReply = ReviewReply::whereIn('review_id', $reviews->pluck("id")->toArray())
			                          ->get();
		}

		$data = [
			'total_bookings_count'   => $newOrders->count(),
			'total_revenue'          => array_sum($newOrders->pluck('booking_amount')->toArray()),
			'new_orders'             => $newOrders,
			'completed_orders_count' => $completedOrders->count(),
			'completed_order'        => $completedOrders,
			'total_delivered_count'  => $totalDelivered->count(),
			'delivery_percent'       => $completedOrders->count() ? ($totalDelivered->count() / $completedOrders->count()) * 100 : 0,
			'areas'                  => $areas,
			'cities'                 => $cities,
			'bookingConcepts'        => $bookingConcepts,
			'bookingGallery'         => $bookingGallery,
			'deliveryGallery'        => $deliveryGallery,
			'reviews'                => $reviews,
			'reviewReplies'          => $reviewReply,
			'review_questions'       => $reviewQuestions,
			'review_answers'         => $reviewAnswers,
			'average_review'         => $reviews->count() ? (array_sum($reviews->pluck('rating')->toArray()) / $reviews->count()) : 0,
			'top_review'             => $reviews->count() ? $reviews->sortByDesc('rating')->first()->rating : 0,
			'least_review'           => $reviews->count() ? $reviews->sortBy('rating')->first()->rating : 0
		];

		return response()->json([
			                        'success' => true,
			                        'data'    => $data
		                        ]);
	}
}