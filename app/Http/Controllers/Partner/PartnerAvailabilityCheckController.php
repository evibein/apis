<?php

namespace App\Http\Controllers\Partner;

use App\Http\Models\CheckoutFields;
use App\Http\Models\Notification;
use App\Http\Models\Package;
use App\Http\Models\Planner;
use App\Http\Models\Ticket;
use App\Http\Models\TicketMapping;
use App\Http\Models\TicketMappingGallery;
use App\Http\Models\Venue;
use Carbon\Carbon;
use Evibe\Utility\TypeTicketStatus;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class PartnerAvailabilityCheckController extends PartnerEnquiryController
{
	public function checkOptionsAvailability(Request $request)
	{
		$venueTypeId = $request->input('availCheckVenueTypeId');

		if ($venueTypeId == config('evibe.ticket.type.planners'))
		{
			$res = $this->checkNonVenueOptionsAvailability($request);
		}
		elseif ($venueTypeId == config('evibe.ticket.type.venues'))
		{
			$res = $this->checkVenueOptionsAvailability($request);
		}
		else
		{
			$res = ['success' => false];
		}

		return response()->json($res);
	}

	protected function checkNonVenueOptionsAvailability(Request $request)
	{
		try
		{

			// validate
			// create transactions
			// create notification if successful
			$ticketId = $request->input('ticketId') ?: null;
			$handlerId = $request->input('handlerId') ?: null;
			$mappingIds = [];

			$notificationFailure = false;

			$ticket = Ticket::find($ticketId);
			if (!$ticketId)
			{
				Log::error("Unable to find ticket from ticketId: $ticketId");

				return $res = [
					'success' => false,
				];
			}

			$optionsData = $request->input('optionsData');
			if (!count($optionsData))
			{
				Log::error("No partners are selected");

				return $res = [
					'success'  => false,
					'errorMsg' => 'Kindly select at least one option to check availability'
				];
			}

			$optionData = $optionsData[0]; // because only one option per availability for non-venue

			$partnersData = $request->input('partnersData');
			if (!count($partnersData))
			{
				Log::error("No planners are selected");

				return $res = [
					'success'  => false,
					'errorMsg' => 'Kindly select at least one partner to check availability'
				];
			}

			// ticket action updated
			$avlDateTimestamp = strtotime($request->input('avl-party-date'));
			$partyDate = date('Y-m-d H:i:s', $avlDateTimestamp);
			$typeId = $request->input('avl-booking-cat');
			$enqType = $request->input('enquiry-type');
			$imageUploadType = $request->input('enq-image-type');
			$imageLinks = $request->input('imageLinks');

			$rules = [
				'avl-party-date'          => 'required|date|after:now',
				'avl-budget-price'        => 'required|numeric',
				'avl-booking-cat'         => 'required|integer|min:1',
				'avl-booking-cat-details' => 'required',
				'avl-booking-info'        => 'min:10'
			];

			$messages = [
				'avl-party-date.required'          => 'Party date time is required.',
				'avl-party-date.date'              => 'Party date time must be a valid date.',
				'avl-party-date.after'             => 'Party date time must be after current time.',
				'avl-budget-price.required'        => 'Budget/price is required.',
				'avl-budget-price.numeric'         => 'Budget/price must be numeric.',
				'avl-booking-cat.required'         => 'Booking category is required.',
				'avl-booking-cat.integer'          => 'Please select a valid booking category.',
				'avl-booking-cat.min'              => 'Please select a valid booking category.',
				'avl-booking-cat-details.required' => 'Category details is required.',
				'avl-booking-info.min'             => 'Info field cannot be less that 10 character.'
			];

			$eventId = $ticket->event_id ? $ticket->event_id : 1;
			$parentEventId = $this->getParentEventId($eventId);

			$crmFields = CheckoutFields::where('type_ticket_booking_id', $typeId)
			                           ->where(function ($query) use ($parentEventId) {
				                           $query->where('event_id', $parentEventId)
				                                 ->orWhereNull('event_id');
			                           })
			                           ->where('is_crm', 1)
			                           ->get();

			foreach ($crmFields as $crmField)
			{
				$rules[$crmField->name] = "required";
				$messages[$crmField->name . '.required'] = "$crmField->identifier is required. If not applicable type n/a";
			}

			$validator = Validator::make($request->all(), $rules, $messages);

			if ($validator->fails())
			{
				return $res = ['success' => false, 'errorMsg' => $validator->messages()->first()];
			}

			// check if any active mapping exists for the option and option type id
			$mapping = TicketMapping::find($optionData['ticketMappingId']);
			if (!$mapping)
			{
				$mappingData = [
					'ticket_id'   => $ticketId,
					'map_id'      => $optionData['optionId'],
					'map_type_id' => $optionData['optionTypeId'],
					//'enquiry_created_at' => Carbon::now(),
					'created_at'  => Carbon::now(),
					'updated_at'  => Carbon::now()
				];

				$mapping = TicketMapping::create($mappingData);
			}

			if (!$mapping)
			{
				$notificationFailure = true;
				Log::error("notify team");

				return $res = [
					'success'  => false,
					'errorMsg' => 'Unable to find / create mapping to check availability'
				];
			}

			$mapping->handler_id = $handlerId;
			$mapping->party_date_time = $partyDate;
			$mapping->price = $request->input('avl-budget-price');
			$mapping->type_id = $typeId;
			$mapping->type_details = $request->input('avl-booking-cat-details');
			$mapping->info = $request->input('avl-booking-info');
			$mapping->enquiry_type_id = $enqType;
			$mapping->save();

			$validImages = [];
			$invalidImageLinks=[];

			// upload images from file
			if ($request->has('isFromDash') && $request->input('isFromDash'))
			{
				// validate and upload images from links
				if ($imageUploadType == 2)
				{
					foreach ($imageLinks as $key => $imageUrl)
					{
						if (!empty($imageUrl))
						{
							try
							{
								$validImg = getimagesize($imageUrl);
								if ($validImg)
								{
									$validImages[] = [
										'title'             => pathinfo($imageUrl, PATHINFO_FILENAME),
										'url'               => $imageUrl,
										'ticket_mapping_id' => $mapping->id,
										'type_id'           => 2,
									];
								}
								else
								{
									$invalidImageLinks[] = $imageUrl;
								}

							} catch (\Exception $e)
							{
								$invalidImageLinks[] = $imageUrl;
							}

						}
					}
					if (count($invalidImageLinks) > 0)
					{
						return $res = [
							'success'       => false,
							'errorMsg'      => 'Some image links doesn\'t have images or invalid, please check the links below',
							'hasImageError' => true,
							'invalidImage'  => implode(",", $invalidImageLinks)
						];
					}

					// save images
					if (count($validImages))
					{
						foreach ($validImages as $validImage)
						{
							$mappingGallery = TicketMappingGallery::where('title', $validImage['title'])
							                                      ->where('url', $validImage['url'])
							                                      ->where('ticket_mapping_id', $validImage['ticket_mapping_id'])
							                                      ->where('type_id', $validImage['type_id'])
							                                      ->whereNull('deleted_at')
							                                      ->first();
							if (!$mappingGallery)
							{
								TicketMappingGallery::create($validImage);
							}
						}
					}

				}
			}

			// save the value of mapping fields(crm checkout fields)
			$this->saveTicketMappingFieldValue($mapping, $typeId, $request->all(), 1);

			$ticket->event_date = $avlDateTimestamp;
			$ticket->save();

			$ticketUpdate = [
				'ticket'     => $ticket,
				'typeUpdate' => TypeTicketStatus::MANUAL,
				'comments'   => "Avail check initiated for mapping id " . $mapping->id
			];
			$this->updateTicketAction($ticketUpdate);

			foreach ($partnersData as $partnersDatum)
			{
				$partnerId = $partnersDatum['partnerId'];
				$plannerPartner = Planner::find($partnerId);
				if (!$plannerPartner)
				{
					$notificationFailure = true;
					Log::error("Unable to find partner data to send availability check. PartnerId: " . $partnerId);
					break;
				}

				$partnerUser = Planner::where('id', $partnerId)->select('user_id')->first();
				if ($partnerUser)
				{
					$partnerUserId = $partnerUser->user_id;
				}
				else
				{
					$notificationFailure = true;
					Log::error("Unable to find user data for the partner to create availability check. PartnerId: " . $partnerId);
					break;
				}

				$deadlineHrs = "2";

				$enquiryData = [
					'title'             => $mapping->type_details,
					'text'              => $mapping->info,
					'type_id'           => $mapping->enquiry_type_id,
					'created_by'        => $request->input('createdBy') ? $request->input('createdBy') : null,
					'created_for'       => $partnerUserId,
					'ticket_mapping_id' => $mapping->id,
					'budget_price'      => $mapping->price,
					'ticket_id'         => $mapping->ticket_id,
					'is_auto_booking'   => $request->has('isAutoBooking') ? $request->has('isAutoBooking') : 0,
					'deadline_at'       => Carbon::now()->addHour($deadlineHrs)->timestamp // putting deadline as 2 hrs
				];

				$newEnquiry = Notification::create($enquiryData);
				if ($newEnquiry)
				{
					$mapping->update(['enquiry_sent_at' => Carbon::now()]);

					$notificationData = [
						'screen'           => config('evibe.partner_app_key.enquiry.details'),
						'screenId'         => $newEnquiry->id,
						'title'            => "New avail. check for " . date('d/m/y', $ticket->event_date),
						'message'          => "Pls. respond within $deadlineHrs hours",
						'typeNotification' => 'newEnq',
						'userId'           => $partnerUserId,
						'isAutoBook'       => $newEnquiry->is_auto_booking
					];

					if (isset($notificationData))
					{
						$url = url('partner/v1/notification/create') . '?isFromApi=true';

						try
						{
							$client = new Client();
							$client->request('POST', $url, [
								'headers' => [
								],
								'json'    => $notificationData
							]);

							$res['success'] = true;
							array_push($mappingIds, $mapping->id);
						} catch (ClientException $e)
						{
							$apiResponse = $e->getResponse()->getBody(true);
							$apiResponse = \GuzzleHttp\json_decode($apiResponse);
							$res['errorMsg'] = $apiResponse->errorMessage;
							$this->sendAndSaveReport($apiResponse->errorMessage);
							$notificationFailure = true;
						}

					}
				}
			}

			if ($notificationFailure)
			{
				//return $res = [
				//	'success'    => true,
				//	'errorMsg'   => 'Avail check notification might have not been asked for all the partner/options',
				//	'mappingIds' => $mappingIds
				//];
				Log::error("avail check notifications partially sent");
			}

			return $res = [
				'success'    => true,
				'mappingIds' => $mappingIds
			];

		} catch (\Exception $exception)
		{
			return $res = [
				'success'  => false,
				'errorMsg' => $exception->getMessage()
			];
		}
	}

	protected function checkVenueOptionsAvailability(Request $request)
	{
		try
		{

			// validate
			// create transactions
			// create notification if successful
			$ticketId = $request->input('ticketId') ?: null;
			$handlerId = $request->input('handlerId') ?: null;
			$mappingIds = [];

			$notificationFailure = false;

			$ticket = Ticket::find($ticketId);
			if (!$ticketId)
			{
				Log::error("Unable to find ticket from ticketId: $ticketId");

				return $res = [
					'success' => false,
				];
			}

			$optionsData = $request->input('optionsData');
			if (!count($optionsData))
			{
				Log::error("No partners are selected");

				return $res = [
					'success'  => false,
					'errorMsg' => 'Kindly select at least one option to check availability'
				];
			}

			// ticket action updated
			$avlDateTimestamp = strtotime($request->input('avl-party-date'));
			$partyDate = date('Y-m-d H:i:s', $avlDateTimestamp);
			$enqType = $request->input('enquiry-type');
			$imageUploadType = $request->input('enq-image-type');
			$imageLinks = $request->input('imageLinks');

			$rules = [
				'avl-party-date'   => 'required|date|after:now',
				'avl-budget-price' => 'sometimes|numeric'
			];

			$messages = [
				'avl-party-date.required'   => 'Party date time is required.',
				'avl-party-date.date'       => 'Party date time must be a valid date.',
				'avl-party-date.after'      => 'Party date time must be after current time.',
				'avl-budget-price.required' => 'Budget/price is required.',
				'avl-budget-price.numeric'  => 'Budget/price must be numeric.'
			];

			$eventId = $ticket->event_id ? $ticket->event_id : 1;
			$parentEventId = $this->getParentEventId($eventId);

			$validator = Validator::make($request->all(), $rules, $messages);

			if ($validator->fails())
			{
				return $res = ['success' => false, 'errorMsg' => $validator->messages()->first()];
			}

			foreach ($optionsData as $optionsDatum)
			{
				$optionTypeDetail = null;
				$optionPrice = null;
				$optionInfo = null;
				$partnerId = null;
				$partnerTypeId = null;
				switch ($optionsDatum['optionTypeId'])
				{
					case config('evibe.ticket.type.villa'):
					case config('evibe.ticket.type.couple-experiences'):
					case config('evibe.ticket.type.generic-package'):
						// @see: only venue packages
						$option = Package::find($optionsDatum['optionId']);
						if (!$option)
						{
							$notificationFailure = true;
							Log::error("Unable to find venue option to check availability. OptionId: " . $optionsDatum['optionId']);
							break;
						}

						$optionPrice = $option->price;
						$optionInfo = $option->info;
						$partnerId = $option->map_id;
						$partnerTypeId = $option->map_type_id;

						// @see: hardcoded
						if ($option->type_ticket_id == config('evibe.ticket.type.couple-experiences'))
						{
							$optionTypeDetail = "Surprise Package";
						}
						elseif ($option->type_ticket_id == config('evibe.ticket.type.villa'))
						{
							$optionTypeDetail = "Villa";
						}
						else
						{
							$optionTypeDetail = "Package";
						}
						break;
				}

				$partnerUser = Venue::where('id', $partnerId)->select('user_id')->first();
				if ($partnerUser)
				{
					$partnerUserId = $partnerUser->user_id;
				}
				else
				{
					$notificationFailure = true;
					Log::error("Unable to find user data for the partner to create availability check. PartnerId: " . $partnerId);
					break;
				}

				$mapping = TicketMapping::where('map_id', $optionsDatum['optionId'])
				                        ->where('map_type_id', $optionsDatum['optionTypeId'])
				                        ->where('ticket_id', $ticketId)
				                        ->whereNull('deleted_at')
				                        ->first();

				if (!$mapping)
				{
					$mappingData = [
						'ticket_id'   => $ticketId,
						'map_id'      => $optionsDatum['optionId'],
						'map_type_id' => $optionsDatum['optionTypeId'],
						//'enquiry_created_at' => Carbon::now(),
						'created_at'  => Carbon::now(),
						'updated_at'  => Carbon::now()
					];

					$mapping = TicketMapping::create($mappingData);
				}

				if (!$mapping)
				{
					Log::error("notify team");
					$res = [
						'success'  => false,
						'errorMsg' => 'Unable to find / create mapping to check availability'
					];
				}

				$mapping->handler_id = $handlerId;
				$mapping->party_date_time = $partyDate;
				$mapping->price = $request->input('avl-budget-price') ?: $optionPrice;
				$mapping->type_details = $optionTypeDetail;
				$mapping->info = $optionInfo;
				$mapping->enquiry_type_id = $enqType;
				$mapping->target_partner_id = $partnerId;
				$mapping->save();

				$validImages = [];
				$invalidImageLinks=[];

				// upload images from file
				if ($request->has('isFromDash') && $request->input('isFromDash'))
				{
					// validate and upload images from links
					if ($imageUploadType == 2)
					{
						foreach ($imageLinks as $key => $imageUrl)
						{
							if (!empty($imageUrl))
							{
								try
								{
									$validImg = getimagesize($imageUrl);
									if ($validImg)
									{
										$validImages[] = [
											'title'             => pathinfo($imageUrl, PATHINFO_FILENAME),
											'url'               => $imageUrl,
											'ticket_mapping_id' => $mapping->id,
											'type_id'           => 2,
										];
									}
									else
									{
										$invalidImageLinks[] = $imageUrl;
									}

								} catch (\Exception $e)
								{
									$invalidImageLinks[] = $imageUrl;
								}

							}
						}
						if (count($invalidImageLinks) > 0)
						{
							return $res = [
								'success'       => false,
								'errorMsg'      => 'Some image links doesn\'t have images or invalid, please check the links below',
								'hasImageError' => true,
								'invalidImage'  => implode(",", $invalidImageLinks)
							];
						}

						// save images
						if (count($validImages))
						{
							foreach ($validImages as $validImage)
							{
								$mappingGallery = TicketMappingGallery::where('title', $validImage['title'])
								                                      ->where('url', $validImage['url'])
								                                      ->where('ticket_mapping_id', $validImage['ticket_mapping_id'])
								                                      ->where('type_id', $validImage['type_id'])
								                                      ->whereNull('deleted_at')
								                                      ->first();
								if (!$mappingGallery)
								{
									TicketMappingGallery::create($validImage);
								}
							}
						}

					}
				}

				$ticket->event_date = $avlDateTimestamp;
				$ticket->save();

				$ticketUpdate = [
					'ticket'     => $ticket,
					'typeUpdate' => TypeTicketStatus::MANUAL,
					'comments'   => "Avail check initiated for mapping id " . $mapping->id
				];
				$this->updateTicketAction($ticketUpdate);

				$deadlineHrs = "2";

				$enquiryData = [
					'title'             => $mapping->type_details,
					'text'              => $mapping->info,
					'type_id'           => $mapping->enquiry_type_id,
					'created_by'        => $request->input('createdBy') ? $request->input('createdBy') : null,
					'created_for'       => $partnerUserId,
					'ticket_mapping_id' => $mapping->id,
					'budget_price'      => $mapping->price,
					'ticket_id'         => $mapping->ticket_id,
					'is_auto_booking'   => $request->has('isAutoBooking') ? $request->has('isAutoBooking') : 0,
					'deadline_at'       => Carbon::now()->addHour($deadlineHrs)->timestamp // putting deadline as 2 hrs
				];

				$newEnquiry = Notification::create($enquiryData);
				if ($newEnquiry)
				{
					$mapping->update(['enquiry_sent_at' => Carbon::now()]);

					$notificationData = [
						'screen'           => config('evibe.partner_app_key.enquiry.details'),
						'screenId'         => $newEnquiry->id,
						'title'            => "New avail. check for " . date('d/m/y', $ticket->event_date),
						'message'          => "Pls. respond within $deadlineHrs hours",
						'typeNotification' => 'newEnq',
						'userId'           => $partnerUserId,
						'isAutoBook'       => $newEnquiry->is_auto_booking
					];

					if (isset($notificationData))
					{
						$url = url('partner/v1/notification/create') . '?isFromApi=true';

						try
						{
							$client = new Client();
							$client->request('POST', $url, [
								'headers' => [
								],
								'json'    => $notificationData
							]);

							$res['success'] = true;
							array_push($mappingIds, $mapping->id);
						} catch (ClientException $e)
						{
							$apiResponse = $e->getResponse()->getBody(true);
							$apiResponse = \GuzzleHttp\json_decode($apiResponse);
							$res['errorMsg'] = $apiResponse->errorMessage;
							$this->sendAndSaveReport($apiResponse->errorMessage);
							$notificationFailure = true;
						}

					}
				}
			}

			if ($notificationFailure)
			{
				//return $res = [
				//	'success'    => true,
				//	'errorMsg'   => 'Avail check notification might have not been asked for all the partner/options',
				//	'mappingIds' => $mappingIds
				//];
				Log::error("avail check notifications partially sent");
			}

			return $res = [
				'success'    => true,
				'mappingIds' => $mappingIds
			];

		} catch (\Exception $exception)
		{
			return $res = [
				'success'  => false,
				'errorMsg' => $exception->getMessage()
			];
		}
	}
}