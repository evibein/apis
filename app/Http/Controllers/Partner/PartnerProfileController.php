<?php

namespace App\Http\Controllers\Partner;

namespace App\Http\Controllers\Partner;

use App\Http\Controllers\BaseController;
use App\Http\Models\Cake;
use App\Http\Models\CakeEvent;
use App\Http\Models\CakeGallery;
use App\Http\Models\Decor;
use App\Http\Models\DecorEvent;
use App\Http\Models\DecorGallery;
use App\Http\Models\Package;
use App\Http\Models\PackageGallery;
use App\Http\Models\Planner;
use App\Http\Models\PlannerPackageEvent;
use App\Http\Models\TrendEvent;
use App\Http\Models\TrendGallery;
use App\Http\Models\Trends;
use App\Http\Models\Venue;
use App\Http\Models\VenueHall;
use App\Http\Models\VenueHallEvent;
use App\Http\Models\VenueHallGallery;
use Illuminate\Http\Request;

class PartnerProfileController extends BaseController
{
	public function getBasicDetails($mapId, $mapTypeId, Request $request)
	{
		$userId = $this->checkForAccessToken($request);

		if ($userId)
		{
			$partner = null;
			if ($mapTypeId == config('evibe.ticket.type.planners'))
			{
				$partner = Planner::find($mapId);
			}
			else
			{
				$partner = Venue::find($mapId);
			}

			if (is_null($partner))
			{
				$res = [
					"success"          => false,
					"error"            => "Please select the correct partner type and partner name",
					"nameOfThePartner" => null,
					"data"             => null
				];

				return response()->json($res);
			}

			$partnerCityId = $partner ? $partner->city_id : null;

			// @see: We use partnerOptions array common for all options i.e, decors,cakes etc.
			$partnerOptions = [];

			// venues
			if ($mapTypeId == config('evibe.ticket.type.venues'))
			{
				// venue packages
				$packages = Package::where(['map_type_id' => config('evibe.ticket.type.venues'), 'map_id' => $mapId])->get();

				if ($packages)
				{
					foreach ($packages as $package)
					{
						$packageEventIds = PlannerPackageEvent::where('id', $package->id)->get()->pluck('event_id')->toArray();
						$plannerPackageGallery = PackageGallery::where('planner_package_id', $package->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $package->id,
							'mapTypeId'   => config('evibe.ticket.type.package'),
							'cityId'      => $package->city_id ? $package->city_id : null,
							'occasionId'  => $packageEventIds ? $packageEventIds : null,
							'isLive'      => $package->is_live,
							'name'        => $package->name,
							'code'        => $package->code,
							'minPrice'    => $package->price,
							'worthPrice'  => $package->price_worth,
							'maxPrice'    => $package->price_max,
							'is_package'  => true,
							'displayName' => 'Package',
							'galleryUrl'  => $plannerPackageGallery ? config("evibe.hosts.gallery") . "/package/new/" . $package->id . "/" . $plannerPackageGallery->url : null
						];
					}
				}

				$halls = VenueHall::with('venue')->where('venue_id', $mapId)->get();

				if ($halls)
				{
					foreach ($halls as $hall)
					{
						$venue = $hall->venue;
						$hallEventIds = VenueHallEvent::where('venue_hall_id', $hall->id)->get()->pluck('event_id')->toArray();
						$venueHallGallery = VenueHallGallery::where('venue_hall_id', $hall->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $hall->id,
							'mapTypeId'   => config('evibe.ticket.type.venue_halls'),
							'cityId'      => $venue->city_id ? $venue->city_id : null,
							'eventId'     => $hallEventIds ? $hallEventIds : null,
							'isLive'      => $venue->is_live,
							'displayName' => 'Venues',
							'is_package'  => false,
							'name'        => $hall->name,
							'minPrice'    => $hall->rent_min,
							'worthPrice'  => $hall->rent_worth,
							'maxPrice'    => $hall->rent_max,
							'code'        => $hall->code,
							'galleryUrl'  => $venueHallGallery ? config("evibe.hosts.gallery") . "/venues/" . $venue->id . "/halls/" . $hall->id . '/images/' . $venueHallGallery->url : null
						];
					}
				}
			}
			else
			{
				// planner/artist packages (non venue packages)
				$packages = Package::where('map_id', $mapId)
				                   ->where('map_type_id', config('evibe.ticket.type.planners'))
				                   ->get();

				if (count($packages) > 0)
				{
					foreach ($packages as $package)
					{
						$packageEventIds = PlannerPackageEvent::where('package_id', $package->id)
						                                      ->get()
						                                      ->pluck('event_id')
						                                      ->toArray();
						$packageGallery = PackageGallery::where('planner_package_id', $package->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $package->id,
							'mapTypeId'   => config('evibe.ticket.type.packages'),
							'displayName' => 'Package',
							'isLive'      => $package->is_live,
							'name'        => $package->name,
							'code'        => $package->code,
							'minPrice'    => $package->price,
							'worthPrice'  => $package->price_worth,
							'maxPrice'    => $package->price_max,
							'cityId'      => $package->city_id ? $package->city_id : null,
							'occasionId'  => $packageEventIds ? $packageEventIds : null,
							'is_package'  => true,
							'galleryUrl'  => $packageGallery ? config("evibe.hosts.gallery") . "/planner/" . $mapId . "/images/packages/" . $package->id . "/" . $packageGallery->url : null
						];
					}
				}

				// trend
				$trends = Trends::where('planner_id', $mapId)->get();

				if ($trends)
				{
					foreach ($trends as $trend)
					{
						$trendEventIds = TrendEvent::where('trend_id', $trend->id)->get()->pluck('type_event_id')->toArray();
						$trendingUrl = TrendGallery::where('trending_id', $trend->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $trend->id,
							'mapTypeId'   => config('evibe.ticket.type.trends'),
							'cityId'      => $trend->city_id ? $trend->city_id : null,
							'occasionId'  => $trendEventIds ? $trendEventIds : null,
							'isLive'      => $trend->is_live,
							'displayName' => 'Trend',
							'name'        => $trend->name,
							'code'        => $trend->code,
							'minPrice'    => $trend->price,
							'maxPrice'    => $trend->price_max,
							'worthPrice'  => $trend->price_worth,
							'is_package'  => false,
							'galleryUrl'  => $trendingUrl ? config('evibe.hosts.gallery') . '/trends/' . $trend->id . '/images/results/' . $trendingUrl->url : null
						];
					}
				}
				// decor
				$decors = Decor::where('provider_id', $mapId)->get();
				if ($decors)
				{
					foreach ($decors as $decor)
					{
						$decorEventIds = DecorEvent::where('decor_id', $decor->id)->get()->pluck('event_id')->toArray();
						$decorUrl = DecorGallery::where('decor_id', $decor->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $decor->id,
							'mapTypeId'   => config('evibe.ticket.type.decors'),
							'cityId'      => $partnerCityId,
							'eventId'     => $decorEventIds ? $decorEventIds : null,
							'isLive'      => $decor->is_live,
							'name'        => $decor->name,
							'code'        => $decor->code,
							'is_package'  => false,
							'minPrice'    => $decor->min_price,
							'maxPrice'    => $decor->max_price,
							'worthPrice'  => $decor->worth,
							'displayName' => 'Decor Style',
							'galleryUrl'  => $decorUrl ? config('evibe.hosts.gallery') . '/decors/' . $decor->id . '/images/' . $decorUrl->url : null
						];
					}

				}

				// cake
				$cakes = Cake::where('provider_id', $mapId)->get();
				if ($cakes)
				{
					foreach ($cakes as $cake)
					{
						$cakeEventIds = CakeEvent::where('cake_id', $cake->id)->get()->pluck('event_id')->toArray();
						$cakeUrl = CakeGallery::where('cake_id', $cake->id)->orderBy('is_profile', 'DESC')->first();

						$partnerOptions[] = [
							'mapId'       => $cake->id,
							'name'        => $cake->title,
							'code'        => $cake->code,
							'mapTypeId'   => config('evibe.ticket.type.cakes'),
							'cityId'      => $partnerCityId,
							'eventId'     => $cakeEventIds ? $cakeEventIds : null,
							'isLive'      => $cake->is_live,
							'displayName' => 'Cake',
							'minPrice'    => $cake->price,
							'maxPrice'    => $cake->price_per_kg,
							'worthPrice'  => $cake->price_worth,
							'is_package'  => false,
							'galleryUrl'  => $cakeUrl ? config('evibe.hosts.gallery') . '/cakes/' . $cake->id . '/' . $cakeUrl->url : null
						];

					}
				}
			}

			// In case of no data found for that partner
			if (is_null($partnerOptions))
			{
				$res = [
					"success"          => true,
					"error"            => "No Data found about this partner",
					"nameOfThePartner" => $partner->name,
					"data"             => null
				];

				return response()->json($res);
			}

			// success case and sending data
			$res =
				[
					"success"          => true,
					"data"             => $partnerOptions,
					"nameOfThePartner" => $partner->name
				];

			return response()->json($res);
		}
		else
		{
			$res = [
				"success"          => false,
				"error"            => "Your not authorized to access this service",
				"nameOfThePartner" => null,
				"data"             => null
			];

			return response()->json($res);
		}
	}
}
