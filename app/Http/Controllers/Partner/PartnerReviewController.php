<?php

namespace App\Http\Controllers\Partner;

use App\Jobs\Mail\Partner\Review\MailPartnerAutoAcceptFail;
use App\Jobs\Mail\Partner\Review\MailPartnerAutoAcceptSuccess;
use App\Jobs\Mail\Partner\Review\MailTeamAutoAcceptFail;
use App\Models\Review\Review;

class PartnerReviewController extends BasePartnerController
{
	public function sendAutoAcceptRejectReviewMail($status, $vendorFeedbackId)
	{
		$vendorFeedback = Review::find($vendorFeedbackId);

		if ($vendorFeedback)
		{
			$booking = $vendorFeedback->booking;
			if ($booking)
			{
				$provider = $booking->provider;
				$ticket = $booking->ticket;

				if (!is_null($provider) && !is_null($ticket))
				{
					$additionalRatings = [];
					$sub = '[Evibe.in] You got a new review from ' . $ticket->name . ' for party on ' . date("d M Y", strtotime($vendorFeedback->created_at));

					$answers = $vendorFeedback->individualAnswers;

					if (!is_null($answers))
					{
						foreach ($answers as $answer)
						{
							if ($answer->question)
							{
								$additionalRatings[] = [
									'question'  => $answer->question->question,
									'rating'    => $answer->rating,
									'maxRating' => $answer->question->max_rating
								];
							}
						}
					}

					$to = $provider->email;
					if (!$to)
					{
						$to = config('evibe.tech_group_email'); // quick fix to understand the issue
					}

					$data = [
						'to'                => $to,
						'subject'           => $sub,
						'vendorName'        => $provider->person,
						'reviewData'        => $vendorFeedback->review ? $vendorFeedback->review : "---",
						'vendorRating'      => $vendorFeedback->rating,
						'vendorUniform'     => $vendorFeedback->uniform ? $vendorFeedback->uniform : "---",
						'vendorMapTypeId'   => $vendorFeedback->map_type_id,
						'additionalRatings' => $additionalRatings,
						'partyDate'         => date("d M Y", $booking->party_date_time),
						'clientName'        => $ticket->name,
						'ticketId'          => $ticket->id
					];

					if ($status == "success")
					{
						$this->dispatch(new MailPartnerAutoAcceptSuccess($data));

						return response()->json(['success' => true]);
					}
					elseif ($status == "fail")
					{
						$this->dispatch(new MailPartnerAutoAcceptFail($data));
						$this->dispatch(new MailTeamAutoAcceptFail($data));

						return response()->json(['success' => true]);
					}
				}
			}
		}

		return response()->json(['success' => false, 'error' => "Booking/Provider not found"]);
	}
}