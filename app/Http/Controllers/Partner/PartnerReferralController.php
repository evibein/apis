<?php

/**
 * @author : Saachi <savyasaachi.nb@evibe.in>
 * @since  : 8 January 2017
 * @info   : Partner's referrals related information
 */

namespace App\Http\Controllers\Partner;

use App\Http\Models\Referral;
use App\Http\Models\Ticket;
use App\Http\Models\TicketUpdate;
use App\Exceptions\CustomException;

use App\Http\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use App\Jobs\Mail\MailPartnerAppLeadToTeamJob;
use App\Jobs\Mail\MailReferralSuccessToPartnerJob;
use App\Jobs\Mail\MailPartnerAppFollowUpToTeamJob;
use App\Jobs\Notification\ReferralWebPushNotificationToTeamJob;

class PartnerReferralController extends BasePartnerController
{
	public function getAboutReferral(Request $request)
	{
		// validate access token
		$this->checkForAccessToken($request);

		// fetch about refer and earn program content from file
		try
		{
			$content = File::get(resource_path('jsons/referrals/about.json'));
		} catch (\Exception $e)
		{
			// custom response on unable to fetch the file content
			throw new CustomException("Some error occurred while fetching the content.", 500);
		}

		$content = json_decode($content, true);

		// successful response
		return response()->json($content);
	}

	public function getReferralList(Request $request)
	{
		// validate access token
		$userId = $this->checkForAccessToken($request);

		// get the count of referrals to skip and take
		$skip = $request->input('skip') ? $request->input('skip') : 0;
		$take = $request->input('take') ? $request->input('take') : 20;

		// get the referrals for the user in descending order
		$referrals = Referral::where('referee_id', "=", $userId)
		                     ->orderBy('created_at', 'DESC')
		                     ->skip($skip)
		                     ->take($take)
		                     ->get();

		$data = [
			'counts' => [],
			'list'   => []
		];

		$data['counts'] = $this->getPartnerAppReferralCount($userId);

		foreach ($referrals as $referral)
		{
			$data['list'][] = $this->getFormattedReferralData($referral);
		}

		return response()->json($data);
	}

	public function getReferralDetails($id, Request $request)
	{
		// validate access token
		$userId = $this->checkForAccessToken($request);

		// validate referral id
		$referral = $this->validateReferral($userId, $id);

		// fetch referral data
		$data = $this->getFormattedReferralData($referral);

		return response()->json($data);
	}

	public function postNewReferral(Request $request)
	{
		// validate access token
		$userId = $this->checkForAccessToken($request);

		// get user details
		$user = User::find($userId);

		$rules = [
			'phone' => 'required|phone'
		];

		$messages = [
			'phone.required' => 'Your 10 digit phone number is required',
			'phone.phone'    => 'Phone number is not valid (ex: 9640204000)',
		];

		//  valiate phone number
		$this->validateJSONPayload($request, $rules, $messages);

		// status of the tickets
		$inValidStatus = [config('evibe.ticket.status.rejected'), config('evibe.ticket.status.booked')];

		// check if latest ticket with the phone number exists without the status
		$existingTicket = Ticket::where('phone', '=', $request->json('phone'))
		                        ->whereBetween('created_at', [date('Y-m-d H:i:s', strtotime('-2 months')), date('Y-m-d H:i:s')])
		                        ->orderBy('created_at', 'DESC')
		                        ->whereNotIn('status_id', $inValidStatus)
		                        ->first();

		$name = $request->json('name') ? $request->json('name') : '--';
		$comments = $request->json('comments') ? $request->json('comments') : "--";

		// if ticket exists send success: true, create the ticket with rejected status ,isAccepted value as false
		if ($existingTicket)
		{
			$ticket = Ticket::create([
				                         'phone'      => $request->json('phone'),
				                         'name'       => $name,
				                         'comments'   => $comments,
				                         'source_id'  => config('evibe.ticket.source.partnerApp'),
				                         'status_id'  => config('evibe.ticket.status.rejected'),
				                         'referee_id' => $userId
			                         ]);

			// update enquiry id
			$ticket->enquiry_id = 'ENQ-BLR-RE-' . $ticket->id;
			$ticket->save();

			$message = 'Sorry, we could not save your referral. The enquiry has already been made with us on ' . date("d M Y", strtotime($existingTicket->created_at));
			$isAccepted = false;
		}

		else
		{
			$ticket = Ticket::create([
				                         'phone'      => $request->json('phone'),
				                         'name'       => $name,
				                         'comments'   => $comments,
				                         'source_id'  => config('evibe.ticket.source.partnerApp'),
				                         'status_id'  => config('evibe.ticket.status.initiated'),
				                         'referee_id' => $userId
			                         ]);

			// update enquiry id
			$ticket->enquiry_id = 'ENQ-BLR-RE-' . $ticket->id;
			$ticket->save();

			$message = 'We\'ve received your referral. We will keep you updated when the customer books with Evibe.in.';
			$isAccepted = true;
		}

		if (!$ticket)
		{
			throw new CustomException("Some error occurred while saving the referral.", 500);
		}

		// create the referral once the ticket creation is successful
		// epoints_earned are 0 and epoints_redeemed values as 0 ( 1 represents redeemed )
		$referral = Referral::create([
			                             'ticket_id'        => $ticket->id,
			                             'referee_id'       => $userId,
			                             'epoints_earned'   => 0,
			                             'epoints_redeemed' => 0
		                             ]);

		if (!$referral)
		{
			throw new CustomException("Some error occurred while saving the referral.", 500);
		}

		// sending notification email to team and partner on successful ticket creation and referral creation
		if ($isAccepted)
		{
			$teamEmailData = [
				'subject'  => "New lead from partner for $ticket->phone - $ticket->name",
				'name'     => $ticket->name,
				'phone'    => $ticket->phone,
				'dashLink' => config('evibe.hosts.dash') . '/tickets/' . $ticket->id
			];

			$this->dispatch(new MailPartnerAppLeadToTeamJob($teamEmailData));

			$partnerEmailData = [
				'subject'      => "Thanks for your referral",
				'partnerName'  => $user->name,
				'partnerEmail' => $user->username,
				'name'         => $name,
				'phone'        => $ticket->phone,
				'comments'     => $comments
			];

			$this->dispatch(new MailReferralSuccessToPartnerJob($partnerEmailData));
		}
		else
		{
			// notify the team to follow up on the particular ticket
			$teamEmailData = [
				'subject'  => "Please follow up for ticket $existingTicket->phone - $existingTicket->name",
				'name'     => $existingTicket->name,
				'phone'    => $existingTicket->phone,
				'dashLink' => config('evibe.hosts.dash') . '/tickets/' . $existingTicket->id
			];

			$this->dispatch(new MailPartnerAppFollowUpToTeamJob($teamEmailData));

			// send web push notification to team for existing ticket
			$this->dispatch(new ReferralWebPushNotificationToTeamJob($existingTicket));
		}

		$updateTicketActionData = [
			'ticket_id'   => $ticket->id,
			'status_id'   => $ticket->status_id,
			'handler_id'  => $ticket->referee_id,
			'comments'    => $ticket->comments,
			'status_at'   => Carbon::now()->timestamp,
			'type_update' => 'Auto'
		];

		// create ticket action log for the ticket created
		TicketUpdate::create($updateTicketActionData);

		$data = [
			'success'    => true,
			'counts'     => $this->getPartnerAppReferralCount($userId),
			'referral'   => $this->getFormattedReferralData($referral),
			'message'    => $message,
			'isAccepted' => $isAccepted
		];

		return response()->json($data);
	}

	protected function validateReferral($userId, $referralId)
	{
		$referral = Referral::where('referee_id', '=', $userId)
		                    ->where('id', '=', $referralId)
		                    ->first();
		if (!$referral)
		{
			throw new CustomException("Unauthorised. You don't have access to this order.", 401);
		}

		return $referral;
	}

	protected function getPartnerAppReferralCount($userId)
	{
		$referrals = Referral::where('referee_id', '=', $userId);

		$totalReferrals = $referrals->count();

		$totalEpointsEarned = 0;
		$epointsEarned = $referrals->sum('epoints_earned');
		$totalEpointsEarned = $epointsEarned ? $epointsEarned : $totalEpointsEarned;

		$referralsConverted = $referrals->where('epoints_redeemed', '=' , 1)->count();

		$totalEpointsRedeemed = 0;
		$epointsRedeemed = $referrals->where('epoints_redeemed', '=', 1)
		                             ->sum('epoints_earned');
		$totalEpointsRedeemed = $epointsRedeemed ? $epointsRedeemed : $totalEpointsRedeemed;

		$data = [
			'total'           => $totalReferrals,
			'converted'       => $referralsConverted,
			'epointsEarned'   => (int)$totalEpointsEarned,
			'epointsRedeemed' => (int)$totalEpointsRedeemed
		];

		return $data;
	}

	protected function getFormattedReferralData($referral)
	{
		$list = [
			'id'              => (int)$referral->id,
			'customerNanme'   => $referral->ticket->name,
			'customerPhone'   => $referral->ticket->phone,
			'comments'        => $referral->ticket->comments,
			'epoints'         => $referral->epoints_earned,
			'status'          => $this->getReferralStatus($referral),
			'epointsRedeemed' => $referral->epoints_redeemed == 0 ? false : true,
			"timeline"        => $this->getTimeLine($referral->ticket)
		];

		$formattedCreatedTimestamps = $this->getFormattedCreatedAtTimestamps($referral->created_at);
		$list = array_merge($list, $formattedCreatedTimestamps);

		return $list;
	}

	protected function getReferralStatus($referral)
	{
		// if the status of the ticket is booked
		// then check referral epoints_redeemed value ( default 0 , redeemed : 1 )
		if ((int)$referral->ticket->status_id == config('evibe.ticket.status.booked'))
		{
			if ($referral->epoints_redeemed == 1)
			{
				return 'ePOINTS redeemed';
			}
			else
			{
				return $referral->ticket->status->name;
			}
		}
		else
		{
			return $referral->ticket->status->name;
		}
	}

	protected function getTimeLine($ticket)
	{
		$ticketId = $ticket->id;
		$timeLine = TicketUpdate::where('ticket_id', '=', $ticketId)
		                        ->groupBy('status_id')
		                        ->orderBy('status_at', 'DESC')
		                        ->get();

		$data = [];

		foreach ($timeLine as $timeLineStatus)
		{
			$timeLineData = [
				'title'    => $timeLineStatus->status->name,
				"comments" => $timeLineStatus->comments
			];

			$formattedCreatedTimestamps = $this->getFormattedDateTimestamps($timeLineStatus->status_at);
			$timeLineData = array_merge($timeLineData, $formattedCreatedTimestamps);

			$data[] = $timeLineData;
		}

		return $data;
	}
}