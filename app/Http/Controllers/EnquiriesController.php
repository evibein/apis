<?php

namespace App\Http\Controllers;

use App\Exceptions\CustomException;
use App\Http\Models\Ticket;
use App\Http\Models\TicketUpdate;
use App\Http\Models\TypeTicketSource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class EnquiriesController extends BaseController
{
	public function createEnquiry(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		try
		{
			$rules = [
				//"customerGender" => 'required',
				'name'      => 'required',
				'event_id'  => 'required|integer',
				'city_id'   => 'required|integer',
				'source_id' => 'required|integer',
				//'event_date'      => 'required|date|after:yesterday',
				//'zip_code'       => 'required|integer',
				//'area_id'        => 'required',
				//'email'          => 'required|email',
				'phone'     => 'required'
			];

			$messages = [
				'customerGender.required' => 'Please select gender',
				'event_id.required'       => 'Occasion should not be empty',
				'event_id.integer'        => 'Please select a valid occasion',
				'city_id.required'        => 'City should not be empty',
				'city_id.integer'         => 'Please select a valid city',
				'source_id.required'      => 'Source should not be empty',
				'source_id.integer'       => 'Please select a valid source',
				'zip_code.required'       => 'Please enter the pin code',
				'area_id.required'        => 'Please enter area name',
				'zip_code.integer'        => 'Please enter valid pin code',
				'email.required'          => 'Your email is required',
				'email.email'             => 'Your email id should be in the form of example@abc.com',
				'phone.required'          => 'Your phone is required',
				'phone.phone'             => 'kindly provide a valid phone number',
			];

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success'   => false,
					                        'errorMsg'  => $validation['error'],
					                        'sheetText' => $validation['error']
				                        ]
				);
			}

			$phone = $request['phone'];
			$callingCode = null;
			if (strlen($phone) > 10)
			{
				// country calling cod ein included
				$callingCode = substr($phone, 0, strlen($phone) - 10);
				$phone = substr($phone, strlen($phone) - 10);
			}

			// if an existing active ticket exists, do not include this and update sheet as duplicate
			$timeLimit = Carbon::createFromTimestamp(time())->startOfDay()->subDays(30)->toDateTimeString();
			$existingTicket = Ticket::where('phone', $phone)
			                        ->where('created_at', '>=', $timeLimit)
			                        ->whereNotIn('status_id', [
				                        config('evibe.ticket.status.booked'),
				                        config('evibe.ticket.status.autocancel'),
				                        config('evibe.ticket.status.cancelled'),
				                        config('evibe.ticket.status.duplicate'),
				                        config('evibe.ticket.status.related'),
				                        config('evibe.ticket.status.irrelevant')
			                        ])
			                        ->orderBy('updated_at', 'DESC')
			                        ->first();
			if ($existingTicket)
			{
				// update the ticket
				TicketUpdate::create([
					                     'ticket_id'   => $existingTicket->id,
					                     'status_id'   => $existingTicket->status_id,
					                     'handler_id'  => $userId,
					                     'comments'    => $this->getTicketSourceName($existingTicket->source_id) . ' Ads',
					                     'status_at'   => time(),
					                     'type_update' => config('evibe.ticket.type_update.auto')
				                     ]);

				return response()->json([
					                        'success'   => true,
					                        'ticketId'  => $existingTicket->id,
					                        'sheetText' => 'Existing ticketId: ' . $existingTicket->id
				                        ]
				);
			}

			// create new ticket
			$ticketData = [
				'name'            => $request['name'],
				'city_id'         => $request['city_id'],
				'event_id'        => $request['event_id'],
				'phone'           => $phone,
				'calling_code'    => $callingCode,
				'status_id'       => config('evibe.ticket.status.initiated'),
				'source_id'       => $request['source_id'],
				'source_specific' => "Ad campaign",
				'created_at'      => Carbon::now(),
				'updated_at'      => Carbon::now(),
				//'customer_gender'   => $request['customerGender'],
				//'event_id'          => $request['eventId'],
				//'email'             => $request['email'],
				//'event_date'        => $eventDate,
				//'enquiry_source_id' => config('evibe.ticket.enquiry_source.direct'),
				//'zip_code'          => $request['zip_code'],
				//'area_id'           => $request['area_id'],
				//'team_notes'        => $request['teamNotes'],
				//'event_slot'        => $eventSlot,
			];

			//return response()->json($ticketData);

			$ticket = Ticket::create($ticketData);

			if ($ticket)
			{
				$ticket->enquiry_id = config('evibe.ticket.enq.pre.ad_campaign') . $ticket->id;
				$ticket->save();

				TicketUpdate::create([
					                     'ticket_id'   => $ticket->id,
					                     'status_id'   => $ticket->status_id,
					                     'handler_id'  => $userId,
					                     'comments'    => $this->getTicketSourceName($ticket->source_id) . ' Ads',
					                     'status_at'   => time(),
					                     'type_update' => config('evibe.ticket.type_update.auto')
				                     ]);

				return response()->json(['success' => 'true', 'ticketId' => $ticket->id, 'sheetText' => 'New ticketId: ' . $ticket->id]);
			}

			return response()->json(['success' => 'false', 'error' => 'Some error occurred while creating a ticket', 'sheetText' => 'Some error occurred while creating a ticket']);

		} catch (\Exception $e)
		{
			return response()->json(['success' => 'false', 'errorMsg' => $e->getMessage(), 'sheetText' => $e->getMessage()]);
		}
	}

	public function getTicketSourceName($sourceId)
	{
		$source = TypeTicketSource::find($sourceId);
		if ($source)
		{
			return $source->name;
		}
		else
		{
			return "other Source";
		}

	}
}