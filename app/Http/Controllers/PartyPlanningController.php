<?php

namespace App\Http\Controllers;

use App\Http\Models\Cake;
use App\Http\Models\City;
use App\Http\Models\Decor;
use App\Http\Models\Package;
use App\Http\Models\Plan\Plan;
use App\Http\Models\Plan\PlanQuestion;
use App\Http\Models\Plan\PlanQuestionAnswer;
use App\Http\Models\Plan\PlanService;
use App\Http\Models\Plan\PlanServiceTag;
use App\Http\Models\Shortlist\Shortlist;
use App\Http\Models\Shortlist\ShortlistOption;
use App\Http\Models\Ticket;
use App\Http\Models\Trends;
use App\Http\Models\TypeCategoryTags;
use App\Http\Models\TypeEvent;
use App\Http\Models\TypeService;
use App\Http\Models\TypeTicket;
use App\Http\Models\VenueHall;
use App\Jobs\Mail\Enquiry\WorkflowEnquiryAlertToCustomer;
use App\Jobs\Mail\Enquiry\WorkflowEnquiryAlertToTeam;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class PartyPlanningController extends BaseController
{
	public function getPlanningDetails($partyPlanToken = null, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{

			$partyPlanningData = [];
			$events = [];
			$eventDetails = [];

			// construct data required with respective 'ids' as keys

			// event details
			$typeEvents = TypeEvent::all();

			if ($typeEvents)
			{
				foreach ($typeEvents as $typeEvent)
				{
					if ($typeEvent->parent_id)
					{
						if (!array_key_exists($typeEvent->parent_id, $events))
						{
							$events[$typeEvent->parent_id] = [];
						}

						$events[$typeEvent->parent_id]['childEvents'][] = [
							'eventId'       => $typeEvent->id,
							'eventName'     => $typeEvent->name,
							'eventUrl'      => $typeEvent->url,
							'eventMetaName' => $typeEvent->meta_name,
							'showCustomer'  => $typeEvent->show_customer
						];
					}
					else
					{
						$events[(string)$typeEvent->id] = [
							'eventId'       => $typeEvent->id,
							'eventName'     => $typeEvent->name,
							'eventUrl'      => $typeEvent->url,
							'eventMetaName' => $typeEvent->meta_name,
							'showCustomer'  => $typeEvent->show_customer
						];
					}
				}
			}

			// event specific questions
			$planQuestions = PlanQuestion::join('type_question', 'type_question.id', '=', 'plan_questions.type_question_id')
			                             ->select('plan_questions.*', 'type_question.type')
			                             ->whereNull('plan_questions.deleted_at')
			                             ->get();

			if ($planQuestions)
			{
				foreach ($planQuestions as $planQuestion)
				{
					if (!array_key_exists($planQuestion->event_id, $eventDetails))
					{
						$eventDetails[$planQuestion->event_id] = [];
					}

					$optionArray = [];
					$options = $planQuestion->options ?: null;

					if ($options)
					{
						foreach ($options as $option)
						{
							array_push($optionArray, [
								'id'   => $option->id,
								'name' => $option->option_name,
								'info' => $option->option_info,
							]);
						}
					}

					$eventDetails[$planQuestion->event_id][$planQuestion->id] = [
						'questionId'     => $planQuestion->id,
						'question'       => $planQuestion->question,
						'questionHint'   => $planQuestion->hint,
						'questionTypeId' => $planQuestion->type_question_id,
						'questionType'   => $planQuestion->type,
						'options'        => $optionArray
					];
				}
			}

			// event specific services with tags
			$serviceArray = config('party-plan.type-event-services');

			if ($serviceArray)
			{
				// get all applicable category tags for the services
				// @see: event id and type ticket id are mandatory for category tags
				$typeCategoryTags = TypeCategoryTags::join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
				                                    ->select('type_category_tags.*', DB::raw('type_tags.identifier AS tag_name, type_tags.map_type_id AS map_type_id, type_tags.type_event AS event_id'))
				                                    ->where("is_main", 1)
				                                    ->whereNotNull('type_tags.map_type_id')
					//->whereNotNull('type_tags.type_event') // @see: check again
					                                ->whereNull('type_tags.deleted_at')
				                                    ->get();

				$typeTickets = TypeTicket::all();

				foreach ($serviceArray as $eventId => $eventServices)
				{
					if ($eventServices)
					{
						foreach ($eventServices as $key => $eventService)
						{
							$typeTicket = $typeTickets->find($eventService);

							if ($typeTicket)
							{
								if (!isset($serviceArray[$eventId][$key]['serviceName']))
								{
									unset($serviceArray[$eventId][$key]);
								}

								$serviceSpecificTags = $typeCategoryTags->filter(function ($item) use ($eventId) {
									return (!$item->event_id || $item->event_id == $eventId);
								})->where('map_type_id', $eventService);

								$serviceTags = [];

								if ($serviceSpecificTags)
								{
									foreach ($serviceSpecificTags as $serviceSpecificTag)
									{
										$serviceTags[$serviceSpecificTag->id] = [
											"name"         => $serviceSpecificTag->tag_name,
											"optionsCount" => $serviceSpecificTag->count,
											"minPrice"     => $serviceSpecificTag->min_price,
											"maxPrice"     => $serviceSpecificTag->max_price,
											"description"  => $serviceSpecificTag->info,
										];
									}
								}

								$serviceArray[$eventId][] = [
									'serviceId'   => $eventService,
									'serviceName' => $typeTicket->name,
									'isPackage'   => $typeTicket->is_package,
									'serviceTags' => $serviceTags
								];
							}
						}
					}
				}
			}

			// include 'isSelected' keys in partPlanningData if partyPlanToken exists
			// same structure has been followed (structure in which data is constructed)
			if ($partyPlanToken)
			{
				$token = $this->decodePartyPlanToken($partyPlanToken);
				$plan = Plan::where('token', $token)->first();

				if ($plan)
				{
					// selected event
					if ($typeEvents)
					{
						$selectedEvent = $typeEvents->find($plan->event_id);
						if ($selectedEvent)
						{
							if ($selectedEvent->parent_id)
							{
								$parentEventId = $selectedEvent->parent_id;
								$childEventId = $selectedEvent->id;
							}
							else
							{
								$parentEventId = $selectedEvent->id;
								$childEventId = null;
							}

							if ($parentEventId && isset($events[$parentEventId]))
							{
								$events[$parentEventId]['isSelected'] = 1;

								if ($childEventId && isset($events[$parentEventId]['childEvents']))
								{
									foreach ($events[$parentEventId]['childEvents'] as $key => $cEvent)
									{
										if ($childEventId == $cEvent["eventId"])
										{
											$events[$parentEventId]['childEvents'][$key]['isSelected'] = 1;
										}
									}
								}
							}
						}
					}

					// selected answers for question related to event details
					$planQuestionAnswers = PlanQuestionAnswer::join('plan_questions', 'plan_questions.id', '=', 'plan_question_answers.plan_question_id')
					                                         ->where('plan_question_answers.plan_id', $plan->id)
					                                         ->select('plan_question_answers.*', 'plan_questions.event_id')
					                                         ->get();
					if (count($planQuestionAnswers))
					{
						foreach ($planQuestionAnswers as $answer)
						{
							if (isset($eventDetails[$answer->event_id][$answer->plan_question_id]))
							{
								$eventDetails[$answer->event_id][$answer->plan_question_id]['answerText'] = $answer->answer_text;

								if (!isset($eventDetails[$answer->event_id][$answer->plan_question_id]['answerOptions']))
								{
									$eventDetails[$answer->event_id][$answer->plan_question_id]['answerOptions'] = [];
								}

								if ($answer->answer_option_id)
								{
									array_push($eventDetails[$answer->event_id][$answer->plan_question_id]['answerOptions'], $answer->answer_option_id);
								}
							}
						}
					}

					// selected services along with category tags
					// @see: considering tags which have both eventId and typeTicketId - similar while constructing data earlier
					$planServices = PlanService::where('plan_id', $plan->id)->whereNull('deleted_at')->get();
					if (count($planServices))
					{
						foreach ($planServices as $planService)
						{
							$parentEventId = $this->getParentEventId($planService->event_id);
							if (isset($serviceArray[$parentEventId]))
							{
								foreach ($serviceArray[$parentEventId] as $key => $sArray)
								{
									if ($sArray["serviceId"] == $planService->type_service_id)
									{
										$serviceArray[$parentEventId][$key]['isSelected'] = 1;
									}
								}
							}
						}
					}

					$planServiceTags = PlanServiceTag::tagDetails()
					                                 ->where('plan_service_tags.plan_id', $plan->id)
					                                 ->whereNotNull('type_tags.map_type_id')
					                                 ->whereNotNull('type_tags.type_event')
					                                 ->get();
					if (count($planServiceTags))
					{
						foreach ($planServiceTags as $planServiceTag)
						{
							if (isset($serviceArray[$planServiceTag->type_event]))
							{
								foreach ($serviceArray[$planServiceTag->type_event] as $key => $sTag)
								{
									if ($sTag["serviceId"] == $planServiceTag->map_type_id)
									{
										$serviceArray[$planServiceTag->type_event][$key]['serviceTags'][$planServiceTag->category_tag_id]['isSelected'] = 1;
									}
								}
							}
						}
					}
				}
			}

			$priorityIds = [36, 37, 38, 8, 19, 34, 35, 39];
			usort($events[config("evibe.event.kids_birthday")]["childEvents"], function ($a, $b) use ($priorityIds) {
				return $this->sortItemsByPriority($a, $b, $priorityIds);
			});

			$partyPlanningData = [
				'events'                => $events,
				'eventDetails'          => $eventDetails,
				'eventSpecificServices' => $serviceArray,
			];

			// todo: comments everywhere - what have you done !!!
			// todo: if - else case error management

			return response()->json([
				                        'success'           => true,
				                        'partyPlanningData' => $partyPlanningData
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function savePlanningDetails(Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{

			$eventId = $request['eventId'];
			$cityId = $request['cityId'];
			$partyDate = $request['partyDate'];
			$eventDetails = $request['eventDetails'];
			$eventServices = $request['eventServices'];
			$serviceTags = $request['serviceTags'];
			//Log::info("eventDetails: " . print_r($request['eventDetails'], true));
			//Log::info("eventServices: " . print_r($request['eventServices'], true));
			//Log::info("serviceTags: " . print_r($request['serviceTags'], true));
			//return response()->json(['success' => false]);
			$event = TypeEvent::find($eventId);
			if (!$event)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find event from eventId'
				                        ]);
			}

			$city = City::find($cityId);
			if (!$city)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find city from cityId'
				                        ]);
			}

			$customerId = null;
			if ($userId != config('evibe.default.user_id'))
			{
				$customerId = $userId;
			}

			// store data: temporarily, or email team with data,
			$errorData = $eventId ? '[EventId: ' . $eventId . ']' : '';
			$errorData .= $cityId ? '[CityId: ' . $cityId . ']' : '';
			$errorData .= $partyDate ? '[PartyDate: ' . $partyDate . ']' : '';
			$errorData .= $customerId ? '[CustomerId: ' . $customerId . ']' : '';

			// @see: token length should be above 10
			$partyPlanToken = str_random(16);

			$plan = Plan::create([
				                     'token'      => $partyPlanToken,
				                     'event_id'   => $eventId,
				                     'city_id'    => $cityId,
				                     'party_date' => $partyDate,
				                     'user_id'    => $customerId,
				                     'created_at' => Carbon::now(),
				                     'updated_at' => Carbon::now()
			                     ]);

			if ($plan)
			{
				if ($eventDetails)
				{
					foreach ($eventDetails as $eventDetail)
					{
						if (isset($eventDetail['questionId']) && $eventDetail['questionId'] &&
							((isset($eventDetail['answerOptionId']) && $eventDetail['answerOptionId']) || (isset($eventDetail['optionText']) && $eventDetail['optionText']))
						)
						{
							$this->saveQuestionAnswer($plan->id, $eventDetail['questionId'], "", $eventDetail['answerOptionId']);
						}
						else
						{
							// todo: notify team regarding the issue
						}
					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific details(Q&A) have been given',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific details(Q&A) have been given',
						                                     'trace'     => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
						                                     'details'   => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
					                                     ]);
				}

				if ($eventServices)
				{
					foreach ($eventServices as $eventService)
					{
						$this->savePlanService($plan->id, $eventId, $eventService);
					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific services have been given',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific services have been given',
						                                     'trace'     => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
						                                     'details'   => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
					                                     ]);
				}

				if ($serviceTags)
				{
					foreach ($serviceTags as $serviceTag)
					{
						$this->savePlanServiceTag($plan->id, $serviceTag);

					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific category tags have been given',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'No event specific category tags have been given',
						                                     'trace'     => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
						                                     'details'   => '[Plan Id: ' . $plan->id . '][Event Id: ' . $eventId . '][City Id: ' . $cityId . ']',
					                                     ]);
				}

				return response()->json([
					                        'success'        => true,
					                        'partyPlanToken' => $this->encodePartyPlanToken($partyPlanToken)
				                        ]);
			}
			else
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to create plan to save data',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to create plan to save data',
					                                     'trace'     => $errorData,
					                                     'details'   => $errorData,
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Some error occurred while submitting details. Kindly try again.'
				                        ]);
			}

			// todo: comments everywhere - what have you done !!!
			// todo: if - else case error management

			// validate data based on the categorised order
			// create plan token
			// save data accordingly
		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			$this->sendAndSaveNonExceptionReport([
				                                     'code'      => config('evibe.error_code.create_function'),
				                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
				                                     'method'    => \Illuminate\Support\Facades\Request::method(),
				                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to create plan to save data',
				                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to create plan to save data',
				                                     'trace'     => $errorData ?: null,
				                                     'details'   => $errorData ?: null,
			                                     ]);

			return response()->json(['success' => false]);
		}
	}

	public function updatePlanningDetails($partyPlanToken, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{

			// todo: validate token based on algorithm
			$eventDetails = $request['eventDetails'];
			$eventServices = $request['eventServices'];
			$serviceTags = $request['serviceTags'];

			$customerId = null;
			if ($userId != config('evibe.default.user_id'))
			{
				$customerId = $userId;
			}

			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if ($plan)
			{
				// fetch plan data, compare and update or delete or create
				$eventId = $request['eventId'] ?: $plan->event_id;
				$cityId = $request['cityId'] ?: $plan->city_id;
				$partyDate = $request['partyDate'] ?: $plan->party_date;

				$plan->update([
					              'event_id'   => $eventId,
					              'city_id'    => $cityId,
					              'party_date' => $partyDate,
					              'user_id'    => $customerId,
					              'updated_at' => Carbon::now()
				              ]);

				$parentEventId = $this->getParentEventId($eventId);

				// todo: check join() code everywhere
				/* === event specific questions code starts === */
				// join answers with their respective questions
				$planAnswers = PlanQuestionAnswer::join('plan_questions', 'plan_questions.id', '=', 'plan_question_answers.plan_question_id')
				                                 ->where('plan_question_answers.plan_id', $plan->id)
				                                 ->where('plan_questions.event_id', $parentEventId)
				                                 ->select('plan_question_answers.*')
				                                 ->get();

				$planQuestions = PlanQuestion::where('event_id', $parentEventId)->get();

				if ($planQuestions)
				{
					foreach ($planQuestions as $planQuestion)
					{
						if ($planQuestion->type_question_id == config('evibe.question-type.text'))
						{
							$oldAnswer = $planAnswers->where('plan_question_id', $planQuestion->id)->first();
							$newAnswer = null;
							if ($eventDetails && in_array($planQuestion->id, array_column($eventDetails, 'questionId')))
							{
								$newAnswerKey = array_search($planQuestion->id, array_column($eventDetails, 'questionId'));
								if ($newAnswerKey && isset($eventDetails[$newAnswerKey]) && $eventDetails[$newAnswerKey])
								{
									$newAnswer = $eventDetails[$newAnswerKey]['answerText'];
								}
							}

							if ($oldAnswer && $newAnswer)
							{
								$oldAnswer->update([
									                   'answer_text' => $newAnswer,
									                   'updated_at'  => Carbon::now()
								                   ]);
							}
							elseif ($oldAnswer)
							{
								$oldAnswer->update(['updated_at' => Carbon::now()]);
							}
							elseif ($newAnswer)
							{
								$this->saveQuestionAnswer($plan->id, $planQuestion->id, $newAnswer, null);
							}
							else
							{
								// means, new answer is null
							}
						}
						else
						{
							$oldAnswerOptionIds = [];
							$newAnswerOptionIds = [];
							$saveAnswerOptionIds = [];
							$updateAnswerOptionIds = [];
							$deleteAnswerOptionIds = [];

							$oldAnswers = $planAnswers->where('plan_question_id', $planQuestion->id);
							if ($oldAnswers)
							{
								$oldAnswerOptionIds = $oldAnswers->pluck('answer_option_id')->toArray();
							}

							if ($eventDetails)
							{
								foreach ($eventDetails as $eventDetail)
								{
									if ($eventDetail['questionId'] == $planQuestion->id)
									{
										array_push($newAnswerOptionIds, $eventDetail['answerOptionId']);
									}
								}
							}

							if (count($oldAnswerOptionIds) && count($newAnswerOptionIds))
							{
								// find separate array to save, update and delete
								// array subtraction
								// array intersection
								$saveAnswerOptionIds = array_diff($newAnswerOptionIds, $oldAnswerOptionIds);
								$deleteAnswerOptionIds = array_diff($oldAnswerOptionIds, $newAnswerOptionIds);
								$updateAnswerOptionIds = array_intersect($newAnswerOptionIds, $oldAnswerOptionIds);
							}
							elseif (count($newAnswerOptionIds))
							{
								// save array
								$saveAnswerOptionIds = $newAnswerOptionIds;
							}
							elseif (count($oldAnswerOptionIds))
							{
								// delete array
								$deleteAnswerOptionIds = $oldAnswerOptionIds;
							}

							if (count($saveAnswerOptionIds))
							{
								foreach ($saveAnswerOptionIds as $saveAnswerOptionId)
								{
									$this->saveQuestionAnswer($plan->id, $planQuestion->id, null, $saveAnswerOptionId);
								}
							}

							if (count($updateAnswerOptionIds))
							{
								foreach ($updateAnswerOptionIds as $updateAnswerOptionId)
								{
									$oldAnswer = $oldAnswers->where('answer_option_id', $updateAnswerOptionId)->first();
									if ($oldAnswer)
									{
										$oldAnswer->update(['updated_at' => Carbon::now()]);
									}
								}
							}

							if (count($deleteAnswerOptionIds))
							{
								foreach ($deleteAnswerOptionIds as $deleteAnswerOptionId)
								{
									$oldAnswer = $oldAnswers->where('answer_option_id', $deleteAnswerOptionId)->first();
									if ($oldAnswer)
									{
										$oldAnswer->update([
											                   'updated_at' => Carbon::now(),
											                   'deleted_at' => Carbon::now()
										                   ]);
									}
								}
							}
						}
					}
				}
				/* === event specific questions code ends === */

				/* === event specific service code starts === */
				// save, update, delete services
				// @see: event specific
				$oldServiceIds = [];
				$newServiceIds = [];
				$saveServiceIds = [];
				$updateServiceIds = [];
				$deleteServiceIds = [];
				$oldServices = PlanService::where('plan_id', $plan->id)
				                          ->where('event_id', $eventId)
				                          ->get();
				if ($oldServices)
				{
					$oldServiceIds = $oldServices->pluck('type_service_id')->toArray();
				}
				if ($eventServices)
				{
					$newServiceIds = $eventServices;
				}

				if (count($oldServiceIds) && count($newServiceIds))
				{
					// find separate array to save, update and delete
					// array subtraction
					// array intersection
					$saveServiceIds = array_diff($newServiceIds, $oldServiceIds);
					$deleteServiceIds = array_diff($oldServiceIds, $newServiceIds);
					$updateServiceIds = array_intersect($newServiceIds, $oldServiceIds);
				}
				elseif (count($newServiceIds))
				{
					// save array
					$saveServiceIds = $newServiceIds;
				}
				elseif (count($oldServiceIds))
				{
					// delete array
					$deleteServiceIds = $oldServiceIds;
				}

				if (count($saveServiceIds))
				{
					foreach ($saveServiceIds as $saveServiceId)
					{
						$this->savePlanService($plan->id, $eventId, $saveServiceId);
					}
				}

				if (count($updateServiceIds))
				{
					foreach ($updateServiceIds as $updateServiceId)
					{
						$oldService = $oldServices->where('type_service_id', $updateServiceId)->first();
						if ($oldService)
						{
							$oldService->update(['updated_at' => Carbon::now()]);
						}
					}
				}

				if (count($deleteServiceIds))
				{
					foreach ($deleteServiceIds as $deleteServiceId)
					{
						$oldService = $oldServices->where('type_service_id', $deleteServiceId)->first();
						if ($oldService)
						{
							$oldService->update([
								                    'updated_at' => Carbon::now(),
								                    'deleted_at' => Carbon::now()
							                    ]);
						}
					}
				}
				/* === event specific service code ends === */

				/* === event specific service tags code starts === */
				// save, update, delete services tags
				// @see: event specific
				$oldServiceTagIds = [];
				$newServiceTagIds = [];
				$saveServiceTagIds = [];
				$updateServiceTagIds = [];
				$deleteServiceTagIds = [];
				$oldServiceTags = PlanServiceTag::tagDetails()
				                                ->where('plan_service_tags.plan_id', $plan->id)
				                                ->whereNotNull('type_tags.map_type_id')
					//->whereNotNull('type_tags.type_event') // @see: check again
					//->where('type_tags.type_event', $eventId) // @see: check again
					                            ->get();
				if ($oldServiceTags)
				{
					$oldServiceTagIds = $oldServiceTags->pluck('category_tag_id')->toArray();
				}
				if ($serviceTags)
				{
					$newServiceTagIds = $serviceTags;
				}

				if (count($oldServiceTagIds) && count($newServiceTagIds))
				{
					// find separate array to save, update and delete
					// array subtraction
					// array intersection
					$saveServiceTagIds = array_diff($newServiceTagIds, $oldServiceTagIds);
					$deleteServiceTagIds = array_diff($oldServiceTagIds, $newServiceTagIds);
					$updateServiceTagIds = array_intersect($newServiceTagIds, $oldServiceTagIds);
				}
				elseif (count($newServiceTagIds))
				{
					// save array
					$saveServiceTagIds = $newServiceTagIds;
				}
				elseif (count($oldServiceTagIds))
				{
					// delete array
					$deleteServiceTagIds = $oldServiceTagIds;
				}

				if (count($saveServiceTagIds))
				{
					foreach ($saveServiceTagIds as $saveServiceTagId)
					{
						$this->savePlanServiceTag($plan->id, $saveServiceTagId);
					}
				}

				if (count($updateServiceTagIds))
				{
					foreach ($updateServiceTagIds as $updateServiceTagId)
					{
						$oldServiceTag = $oldServiceTags->where('category_tag_id', $updateServiceTagId)->first();
						if ($oldServiceTag)
						{
							$oldServiceTag->update(['updated_at' => Carbon::now()]);
						}
					}
				}

				if (count($deleteServiceTagIds))
				{
					foreach ($deleteServiceTagIds as $deleteServiceTagId)
					{
						$oldServiceTag = $oldServiceTags->where('category_tag_id', $deleteServiceTagId)->first();
						if ($oldServiceTag)
						{
							$oldServiceTag->update([
								                       'updated_at' => Carbon::now(),
								                       'deleted_at' => Carbon::now()
							                       ]);
						}
					}
				}

				/* === event specific service tags code starts === */

				return response()->json([
					                        'success'        => true,
					                        'partyPlanToken' => $this->encodePartyPlanToken($plan->token)
				                        ]);
			}
			else
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.update_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan with provided token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan with provided token',
					                                     'trace'     => '[PartyPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartyPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find plan with the provided token'
				                        ]);
			}
		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function getOptions($partyPlanToken, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			// todo: customer Id
			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if ($plan)
			{

				$event = TypeEvent::find($plan->event_id);
				if (!$event)
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.fetch_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch event details from the event id in party plan',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch event details from the event id in party plan',
						                                     'trace'     => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
						                                     'details'   => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
					                                     ]);

					return response()->json(['success' => false]);
				}

				$res = [
					'eventDetails' => [
						'id'        => $event->id,
						'name'      => $event->name,
						'url'       => $event->url,
						'parent_id' => $event->parent_id
					],
					'partyDate'    => $plan->party_date
				];

				$serviceArray = [];
				$serviceTagArray = [];
				$optionsArray = [];

				// selected services
				$planServices = PlanService::join('type_ticket', 'type_ticket.id', '=', 'plan_services.type_service_id')
				                           ->where('plan_services.plan_id', $plan->id)
				                           ->where('plan_services.event_id', $plan->event_id)
				                           ->select('plan_services.*', 'type_ticket.name')
				                           ->get();

				if ($planServices)
				{
					foreach ($planServices as $planService)
					{
						array_push($serviceArray, [
							'id'   => $planService->type_service_id,
							'name' => $planService->name,
						]);
					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.fetch_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch selected services for a party plan event',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch selected services for a party plan event',
						                                     'trace'     => '[PlanId: ' . $plan->id . '][EventId: ' . $event->id . ']',
						                                     'details'   => '[PlanId: ' . $plan->id . '][EventId: ' . $event->id . ']',
					                                     ]);
				}

				$eventId = $event->id;
				$parentEventId = $this->getParentEventId($eventId);

				// selected service specific tags
				$planServiceTags = PlanServiceTag::tagDetails()
				                                 ->where('plan_service_tags.plan_id', $plan->id)
				                                 ->whereNotNull('type_tags.map_type_id')
				                                 ->where(function ($query) use ($parentEventId) {
					                                 $query->where('type_tags.type_event', $parentEventId)
					                                       ->orWhere('type_tags.type_event', null);
				                                 })
				                                 ->get();
				if ($planServiceTags)
				{
					foreach ($planServiceTags as $planServiceTag)
					{
						if (!array_key_exists($planServiceTag->map_type_id, $serviceTagArray))
						{
							$serviceTagArray[$planServiceTag->map_type_id] = [];
						}

						array_push($serviceTagArray[$planServiceTag->map_type_id], [
							'id'          => $planServiceTag->category_tag_id,
							'name'        => $planServiceTag->name,
							'info'        => $planServiceTag->info,
							'optionCount' => $planServiceTag->count,
							'minPrice'    => $planServiceTag->min_price,
							'maxPrice'    => $planServiceTag->max_price
						]);
					}
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.fetch_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch selected service tags for a party plan event',
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to fetch selected service tags for a party plan event',
						                                     'trace'     => '[PlanId: ' . $plan->id . '][EventId: ' . $event->id . ']',
						                                     'details'   => '[PlanId: ' . $plan->id . '][EventId: ' . $event->id . ']',
					                                     ]);
				}
				// todo: filters - like list page filters

				$categoryTags = TypeCategoryTags::select('type_category_tags.*', 'type_tags.map_type_id')
				                                ->join('plan_service_tags', 'plan_service_tags.category_tag_id', '=', 'type_category_tags.id')
				                                ->join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
				                                ->whereNull('plan_service_tags.deleted_at')
				                                ->whereIn('type_category_tags.id', $planServiceTags->pluck("category_tag_id")->toArray())
				                                ->where(function ($query) use ($parentEventId) {
					                                $query->where('type_tags.type_event', $parentEventId)
					                                      ->orWhere('type_tags.type_event', null);
				                                })
				                                ->distinct('type_category_tags.type_tag_id')
				                                ->get();

				$request['limit'] = 30; // take
				$request['offset'] = 0; // skip
				// todo: options
				if ($planServices)
				{
					$shortlistedIds = ShortlistOption::join('shortlist', 'shortlist.id', '=', 'shortlist_option.shortlist_id')
					                                 ->where('shortlist.plan_id', $plan->id)
					                                 ->select('shortlist_option.*')
					                                 ->whereNull('shortlist.deleted_at')
					                                 ->whereNull('shortlist_option.deleted_at')
					                                 ->get();

					foreach ($planServices as $planService)
					{
						$serviceCategoryTags = $categoryTags->where('map_type_id', $planService->type_service_id)
						                                    ->pluck('type_tag_id')
						                                    ->toArray();

						// Tags that had been selected in the second screen, related to gender & place type
						$planQuestionIds = [config("evibe.workflow.question.gender"), config("evibe.workflow.question.place"), config("evibe.workflow.question.guest_count")];
						$eventRequirementTags = PlanQuestionAnswer::join('plan_question_options', 'plan_question_options.id', '=', 'plan_question_answers.answer_option_id')
						                                          ->where('plan_question_answers.plan_id', $plan->id)
						                                          ->whereIn('plan_question_answers.plan_question_id', $planQuestionIds)
						                                          ->whereNull('plan_question_answers.deleted_at')
						                                          ->whereNull('plan_question_options.deleted_at')
						                                          ->pluck('plan_question_options.type_tag_id')
						                                          ->toArray();

						$resOptions = $this->fetchServiceOptions($request, $planService->type_service_id, $serviceCategoryTags, $eventRequirementTags, $plan->city_id, $plan->event_id);
						$resOptionsArray = isset($resOptions['options']) ? $resOptions['options'] : null;

						// todo: isSelected code
						$categoryShortlistedIds = $shortlistedIds->where('map_type_id', $planService->type_service_id)
						                                         ->pluck('map_id')
						                                         ->toArray();
						if ($categoryShortlistedIds && count($resOptionsArray))
						{
							foreach ($resOptionsArray as $key => $option)
							{
								if (in_array($option['id'], $categoryShortlistedIds))
								{
									$resOptionsArray[$key]['isSelected'] = 1;
								}
							}
						}

						$optionsArray[$planService->type_service_id] = [
							'totalOptionsCount' => isset($resOptions['totalOptionsCount']) ? $resOptions['totalOptionsCount'] : null,
							'categoryOptions'   => $resOptionsArray,
							'filters'           => isset($resOptions['filters']) ? $resOptions['filters'] : null
						];

					}
				}

				$res['services'] = $serviceArray;
				$res['serviceTags'] = $serviceTagArray;
				$res['options'] = $optionsArray;
				$res['success'] = true;

				return response()->json($res);

			}
			else
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan with provided token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan with provided token',
					                                     'trace'     => '[PartyPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartyPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find party plan with the provided token'
				                        ]);
			}

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function getMoreOptions($partyPlanToken, $serviceId, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			// todo: custome Id
			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if (!$plan)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find party plan from given token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find party plan from given token',
					                                     'trace'     => '[PartyPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartyPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json(['success' => false]);
			}

			$event = TypeEvent::find($plan->event_id);
			if (!$event)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find event from given party plan event',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find event from given party plan event',
					                                     'trace'     => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
				                                     ]);

				return response()->json(['success' => false]);
			}

			$city = City::find($plan->city_id);
			if (!$city)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find city from given party plan city',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find city from given party plan city',
					                                     'trace'     => '[PlanId: ' . $plan->id . '][Plan CityId: ' . $plan->city_id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . '][Plan CityId: ' . $plan->city_id . ']',
				                                     ]);

				return response()->json(['success' => false]);
			}

			$eventId = $event->id;
			$serviceType = TypeTicket::find($serviceId);
			if (!$serviceType)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find product type from given serviceId',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find product type from given serviceId',
					                                     'trace'     => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . '][Plan EventId: ' . $plan->event_id . ']',
				                                     ]);

				return response()->json(['success' => false]);
			}

			$categoryTags = TypeCategoryTags::select('type_category_tags.*', 'type_tags.map_type_id')
			                                ->join('plan_service_tags', 'plan_service_tags.category_tag_id', '=', 'type_category_tags.id')
			                                ->join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
			                                ->whereNull('plan_service_tags.deleted_at')
				//->where('type_tags.type_event', $plan->event_id) // @see: check again
				                            ->where(function ($query) use ($eventId) {
					$query->where('type_tags.type_event', $eventId)
					      ->orWhere('type_tags.type_event', null);
				})
			                                ->where('type_tags.map_type_id', $serviceId)
			                                ->distinct('type_category_tags.type_tag_id')
			                                ->pluck('type_category_tags.type_tag_id')
			                                ->toArray();

			// offset and limit should be given in request itself
			$resOptions = $this->fetchServiceOptions($request, $serviceId, $categoryTags, [], $plan->city_id, $plan->event_id);
			$categoryOptions = isset($resOptions['options']) ? $resOptions['options'] : null;

			// 'isSelected' code
			$shortlistedIds = ShortlistOption::join('shortlist', 'shortlist.id', '=', 'shortlist_option.shortlist_id')
			                                 ->where('shortlist.plan_id', $plan->id)
			                                 ->select('shortlist_option.*')
			                                 ->whereNull('shortlist.deleted_at')
			                                 ->whereNull('shortlist_option.deleted_at')
			                                 ->where('shortlist_option.map_type_id', $serviceId)
			                                 ->pluck('shortlist_option.map_id')
			                                 ->toArray();

			if ($shortlistedIds && count($categoryOptions))
			{
				foreach ($categoryOptions as $key => $option)
				{
					if (in_array($option['id'], $shortlistedIds))
					{
						$categoryOptions[$key]['isSelected'] = 1;
					}
				}
			}

			return response()->json([
				                        'success'         => true,
				                        'categoryOptions' => $categoryOptions
			                        ]);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function createShortlistOption($partyPlanToken, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			// todo: customer Id
			$productId = $request['productId'];
			$productTypeId = $request['productTypeId'];

			// create a shortlist if not already exists

			// save to shortlist options table
			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if (!$plan)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'trace'     => '[PartPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json(['success' => false]);
			}

			if (!$productId || !$productTypeId)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'ProductId and ProductTypeId are not provided',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'ProductId and ProductTypeId are not provided',
					                                     'trace'     => '[Plan Id: ' . $plan->id . ']',
					                                     'details'   => '[Plan Id: ' . $plan->id . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'ProductId and ProductTypeId are required to shortlist an option'
				                        ]);
			}

			$shortlist = Shortlist::where('user_id', $userId)->where('plan_id', $plan->id)->first();
			if (!$shortlist)
			{
				// create shortlist
				$shortlist = Shortlist::create([
					                               'user_id'    => $userId,
					                               'party_date' => $plan->party_date ? strtotime($plan->party_date) : null,
					                               'plan_id'    => $plan->id,
					                               'created_at' => Carbon::now(),
					                               'updated_at' => Carbon::now()
				                               ]);
			}

			// check if shortlist option already exists
			$oldShortlistOption = ShortlistOption::where('shortlist_id', $shortlist->id)
			                                     ->where('map_id', $productId)
			                                     ->where('map_type_id', $productTypeId)
			                                     ->where('occasion_id', $plan->event_id)
			                                     ->where('city_id', $plan->city_id)
			                                     ->whereNull('deleted_at')
			                                     ->first();
			if ($oldShortlistOption)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Product has already been shortlisted'
				                        ]);
			}

			// save shortlist options
			$shortlistOption = ShortlistOption::create([
				                                           'shortlist_id' => $shortlist->id,
				                                           'city_id'      => $plan->city_id,
				                                           'map_id'       => $productId,
				                                           'map_type_id'  => $productTypeId,
				                                           'occasion_id'  => $plan->event_id,
				                                           'created_at'   => Carbon::now(),
				                                           'updated_at'   => Carbon::now()
			                                           ]);

			if ($shortlistOption)
			{
				return response()->json([
					                        'success'     => true,
					                        'shortlistId' => $shortlist->id
				                        ]);
			}

			return response()->json([
				                        'success' => false,
				                        'error'   => 'Some error occurred while shortlisting an options'
			                        ]);

		} catch (\Exception $exception)
		{
			return response()->json(['success' => false]);
		}
	}

	public function deleteShortlistOption($partyPlanToken, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{

			$productId = $request['productId'];
			$productTypeId = $request['productTypeId'];
			$shortlistId = $request['shortlistId'];

			// create a shortlist if not already exists

			// save to shortlist options table

			// todo: customerId
			// todo: restrict access

			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if (!$plan)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.delete_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'trace'     => '[PartPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find party plan from given token'
				                        ]);
			}

			if (!$productId || !$productTypeId)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'ProductId and ProductTypeId are not provided',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'ProductId and ProductTypeId are not provided',
					                                     'trace'     => '[PlanId: ' . $plan->id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'ProductId and ProductTypeId are required to remove an option from shortlist'
				                        ]);
			}

			$shortlist = null;
			if ($shortlist)
			{
				$shortlist = Shortlist::find($shortlistId);
			}

			if (!$shortlist)
			{
				$shortlist = Shortlist::where('user_id', $userId)->where('plan_id', $plan->id)->first();
			}

			if (!$shortlist)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find shortlist to remove option'
				                        ]);
			}

			$shortlistedOption = ShortlistOption::where('shortlist_id', $shortlist->id)
			                                    ->where('map_type_id', $productTypeId)
			                                    ->where('map_id', $productId)
			                                    ->first();
			if (!$shortlistedOption)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find the option in shortlist to remove'
				                        ]);
			}

			$shortlistedOption->deleted_at = Carbon::now();
			$shortlistedOption->save();

			return response()->json(['success' => true]);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function getShortlistedOptions($partyPlanToken, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			// todo: customer Id - required?
			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if (!$plan)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.fetch_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'trace'     => '[PartPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find party plan from given token'
				                        ]);
			}

			$shortlist = Shortlist::where('user_id', $userId)->where('plan_id', $plan->id)->first();
			if (!$shortlist)
			{
				return response()->json([
					                        'success'            => true,
					                        'shortlistedOptions' => []
				                        ]);
			}

			$shortlistOptions = ShortlistOption::where('shortlist_id', $shortlist->id)
			                                   ->where('city_id', $plan->city_id)
			                                   ->where('occasion_id', $plan->event_id)
			                                   ->get();

			$optionsArray = [];
			if ($shortlistOptions)
			{
				foreach ($shortlistOptions as $option)
				{
					if (!array_key_exists($option->map_type_id, $optionsArray))
					{
						$optionsArray[$option->map_type_id]['options'] = [];
					}

					array_push($optionsArray[$option->map_type_id]['options'], $option->map_id);
				}
			}

			$productCategories = TypeTicket::all();

			foreach ($optionsArray as $serviceId => $serviceDetails)
			{
				$categoryDetails = $productCategories->find($serviceId);
				if ($categoryDetails)
				{
					$optionsArray[$serviceId]['name'] = $categoryDetails->name;
					$optionsArray[$serviceId]['isPage'] = $categoryDetails->is_page;
				}

				$serviceOptions = isset($serviceDetails['options']) ? $serviceDetails['options'] : [];
				if (count($serviceOptions))
				{
					$options = [];

					switch ($serviceId)
					{
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.resort'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.lounge'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.couple-experiences'):
						case config('evibe.ticket.type.venue-deals'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.generic-package'):
							$packages = Package::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $packages);
							break;

						case config('evibe.ticket.type.cakes'):
							$cakes = Cake::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $cakes);
							break;

						case config('evibe.ticket.type.decors'):
							$decors = Decor::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $decors);
							break;

						case config('evibe.ticket.type.trends'):
							$trends = Trends::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $trends);
							break;

						// @see: services == ent
						case config('evibe.ticket.type.entertainments'):
						case config('evibe.ticket.type.services'):
							$services = TypeService::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $services);
							break;

						case config('evibe.ticket.type.venue_halls'):
						case config('evibe.ticket.type.venues'):
							$halls = VenueHall::whereIn('id', $serviceOptions)->get();
							$options = $this->workflowOptionsArrayFormat($serviceId, $halls);
							break;

						default:
							Log::error("Unknown category type");
							break;
					}

					if (count($options))
					{
						foreach ($options as $key => $option)
						{
							$options[$key]['isSelected'] = 1;
						}

						$optionsArray[$serviceId]['options'] = $options;
					}
				}
			}

			return response()->json([
				                        'success'            => true,
				                        'shortlistedOptions' => $optionsArray
			                        ]);
			// fetch options details

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	public function postEnquiry($partyPlanToken, Request $request)
	{
		// access-token
		$userId = $this->checkForAccessToken($request);

		try
		{
			$token = $this->decodePartyPlanToken($partyPlanToken);
			$plan = Plan::where('token', $token)->first();
			if (!$plan)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find plan from given party plan token',
					                                     'trace'     => '[PartPlanToken: ' . $partyPlanToken . ']',
					                                     'details'   => '[PartPlanToken: ' . $partyPlanToken . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find party plan from given token'
				                        ]);
			}

			$rules = [
				'name'              => 'required|min:4',
				'phone'             => 'required|digits:10',
				'email'             => 'required|email',
				'bookingLikeliness' => 'required|numeric|min:1',
				"timeSlot"          => "required|not_in:-1"
			];

			$messages = [
				'name.required'              => 'Your name is required',
				'name.min'                   => 'Name should not be less than 4 characters',
				'phone.required'             => 'Your phone number is required',
				'phone.digits'               => 'Phone number is not valid. Enter only 10 digits number (ex: 9640204000)',
				'email.required'             => 'Your email is required',
				'email.email'                => 'Your email id should be in the form of example@abc.com',
				"timeSlot.required"          => "Please select your available time slot",
				"timeSlot.not_in"            => "Please select your available time slot",
				"bookingLikeliness.required" => "Please select your planning status",
				"bookingLikeliness.min"      => "Please select your planning status"
			];

			$validation = $this->validateJSONData($request, $rules, $messages);
			if ($validation['error'])
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => $validation['error']
				                        ]
				);
			}

			// update user id
			$oldUserId = $plan->user_id;
			if ($oldUserId != $userId)
			{
				$plan->user_id = $userId;
				$plan->save();
			}

			$ticket = Ticket::create([
				                         'plan_id'                 => $plan->id,
				                         'city_id'                 => $plan->city_id,
				                         'name'                    => $request['name'],
				                         'email'                   => $request['email'],
				                         'phone'                   => $request['phone'],
				                         'status_id'               => config('evibe.ticket.status.initiated'),
				                         'event_date'              => $plan->party_date ? strtotime($plan->party_date) : null,
				                         'event_id'                => $plan->event_id,
				                         'comments'                => $request['requirement'],
				                         'enquiry_source_id'       => config('evibe.ticket.enquiry_source.workflow_enquiry'),
				                         'booking_likeliness_id'   => $request['bookingLikeliness'],
				                         "customer_preferred_slot" => $request["timeSlot"],
				                         'created_at'              => Carbon::now(),
				                         'updated_at'              => Carbon::now()
			                         ]);

			if (!$ticket)
			{
				Log::error("Failed to create ticket. Email: " . $request['email'] . ", Phone: " . $request['phone']);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Some error occurred while posting enquiry, please try again'
				                        ]);
			}

			$ticket->update(['enquiry_id' => config("evibe.ticket.enq.pre.customer_workflow") . $ticket->id]);

			$this->updateTicketAction([
				                          'ticket'   => $ticket,
				                          'comments' => "Workflow ticket created by customer"
			                          ]);

			// create mappings from shortlist
			$shortlist = Shortlist::where('user_id', $userId)->where('plan_id', $plan->id)->first();
			if (!$shortlist)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find shortlist to find shortlisted options',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find shortlist to find shortlisted options',
					                                     'trace'     => '[PlanId: ' . $plan->id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find shortlisted options'
				                        ]);
			}

			$shortlistOptions = ShortlistOption::where('shortlist_id', $shortlist->id)
			                                   ->where('occasion_id', $plan->event_id)
			                                   ->get();
			if (!$shortlistOptions)
			{
				$this->sendAndSaveNonExceptionReport([
					                                     'code'      => config('evibe.error_code.create_function'),
					                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
					                                     'method'    => \Illuminate\Support\Facades\Request::method(),
					                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find shortlisted options from the shortlist',
					                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . 'Unable to find shortlisted options from the shortlist',
					                                     'trace'     => '[PlanId: ' . $plan->id . '][ShortlistId: ' . $shortlist->id . ']',
					                                     'details'   => '[PlanId: ' . $plan->id . '][ShortlistId: ' . $shortlist->id . ']',
				                                     ]);

				return response()->json([
					                        'success' => false,
					                        'error'   => 'No options have been shortlisted'
				                        ]);
			}

			// ticket mapping base function
			foreach ($shortlistOptions as $shortlistOption)
			{
				$this->createMapping($ticket->id, $shortlistOption->map_id, $shortlistOption->map_type_id);

				if (isset($res['success']) && !$res['success'])
				{
					$errorMsg = 'Some error occurred while creating ticket mapping';

					if (isset($res['error']))
					{
						$errorMsg = $res['error'];
					}

					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
						                                     'method'    => \Illuminate\Support\Facades\Request::method(),
						                                     'message'   => '[PartyPlanningController.php - ' . __LINE__ . '] ' . $errorMsg,
						                                     'exception' => '[PartyPlanningController.php - ' . __LINE__ . '] ' . $errorMsg,
						                                     'trace'     => '[Ticket Id: ' . $ticket->id . ']',
						                                     'details'   => '[Ticket Id: ' . $ticket->id . ']'
					                                     ]);
				}
			}

			$city = City::find($plan->city_id);
			$cityName = $city ? $city->name : null;

			$event = TypeEvent::find($plan->event_id);
			$eventName = $event ? $event->name : null;

			// mail customer and team
			$emailData = [
				'customerName'          => $ticket->name,
				'customerPhone'         => $ticket->phone,
				'customerEmail'         => $ticket->email,
				'partyDate'             => $plan->party_date,
				'cityName'              => $cityName,
				'eventName'             => $eventName,
				'dashLink'              => config('evibe.hosts.dash') . '/tickets/' . $ticket->id,
				'enquiryId'             => $ticket->enquiry_id,
				'optionsCount'          => count($shortlistOptions),
				"customerPreferredSlot" => $ticket->customer_preferred_slot,
				"bookingLikelinessId"   => $ticket->booking_likeliness_id
			];

			$this->dispatch(new WorkflowEnquiryAlertToCustomer($emailData));
			$this->dispatch(new WorkflowEnquiryAlertToTeam($emailData));

			// sms customer

			return response()->json(['success' => true, 'ticket' => $ticket]);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception);

			return response()->json(['success' => false]);
		}
	}

	// todo: handle user id everywhere
	private function saveQuestionAnswer($planId, $questionId, $answerText = null, $answerOptionId = null)
	{
		if ($planId && $questionId && ($answerText || $answerOptionId))
		{
			$planQuestionAnswer = PlanQuestionAnswer::create([
				                                                 'plan_id'          => $planId,
				                                                 'plan_question_id' => $questionId,
				                                                 'answer_text'      => $answerText,
				                                                 'answer_option_id' => $answerOptionId,
				                                                 'created_at'       => Carbon::now(),
				                                                 'updated_at'       => Carbon::now()
			                                                 ]);

			return $planQuestionAnswer;
		}
		else
		{
			// todo: inform team
			return null;
		}
	}

	private function savePlanService($planId, $eventId, $typeServiceId)
	{
		if ($planId && $eventId && $typeServiceId)
		{
			$planService = PlanService::create([
				                                   'plan_id'         => $planId,
				                                   'type_service_id' => $typeServiceId,
				                                   'event_id'        => $eventId,
				                                   'created_at'      => Carbon::now(),
				                                   'updated_at'      => Carbon::now()
			                                   ]);

			return $planService;
		}
		else
		{
			// todo: notify team
			return null;
		}
	}

	private function savePlanServiceTag($planId, $serviceTag)
	{
		if ($planId && $serviceTag)
		{
			$planServiceTag = PlanServiceTag::create([
				                                         'plan_id'         => $planId,
				                                         'category_tag_id' => $serviceTag,
				                                         'created_at'      => Carbon::now(),
				                                         'updated_at'      => Carbon::now(),
			                                         ]);

			return $planServiceTag;
		}
		else
		{
			// todo: inform team
			return null;
		}
	}

	private function encodePartyPlanToken($token)
	{
		// assuming the token in alpha-numeric
		// string length should be above 10
		$tokenLength = strlen($token);

		// fetch all the numbers in the string
		preg_match_all('!\d+!', $token, $matches);
		$numbers = $matches[0];

		// find the sum of numbers present in the string (sum to a single digit)
		$sum = array_sum($numbers);
		while ($sum >= 10)
		{
			$sum = array_sum(str_split($sum));
		}

		// rotate the string '$sum' times
		$rotateToken = substr($token, $sum) . substr($token, 0, $sum);

		return $rotateToken;
	}

	private function decodePartyPlanToken($token)
	{
		// assuming the token in alpha-numeric
		// string length should be above 10
		$tokenLength = strlen($token);

		// fetch all the numbers in the string
		preg_match_all('!\d+!', $token, $matches);
		$numbers = $matches[0];

		// find the sum of numbers present in the string (sum to a single digit)
		$sum = array_sum($numbers);
		while ($sum >= 10)
		{
			$sum = array_sum(str_split($sum));
		}

		// calculate the difference to rotate back
		$diff = $tokenLength - $sum;

		// rotate the string '$diff' times
		$originalString = substr($token, $diff) . substr($token, 0, $diff);

		return $originalString;
	}

	public function sortItemsByPriority($item1, $item2, $priority)
	{
		// @see: assuming that 'items' are in array format
		$item1Id = $item1['eventId'];
		$item2Id = $item2['eventId'];

		$search1 = array_search($item1Id, $priority);
		$search2 = array_search($item2Id, $priority);

		if ($search1 == $search2)
		{
			return 0;
		}
		elseif ($search1 > $search2)
		{
			return 1;
		}
		else
		{
			return -1;
		}
	}
}