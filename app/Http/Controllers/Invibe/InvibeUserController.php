<?php namespace App\Http\Controllers\Invibe;

use Illuminate\Http\Request;
use App\Http\Controllers\BaseUserController;

use App\Http\Models\InvibeEvent;
use App\Exceptions\CustomException;

use App\Http\Models\User;
use Evibe\Utility\Roles;

class InvibeUserController extends BaseUserController
{
	public function signup(Request $request)
	{
		if (!$request->header('client-id'))
		{
			throw new CustomException("Client id is required.", 403);
		}

		$authClient = $this->validateClientId($request->header('client-id'));

		$this->validateJSONPayload($request, [
			'name'        => 'required',
			'emailId'     => 'required',
			'phoneNumber' => 'required'
		]);

		$name = $request->json('name');
		$emailId = $request->json('emailId');
		$phoneNumber = $request->json('phoneNumber');

		$userFromPhoneNumber = User::where('phone', '=', $phoneNumber)
		                           ->where('role_id', '=', Roles::INVITES_USER)
		                           ->first();

		if (!$userFromPhoneNumber)
		{
			$isNewUser = 1;

			$newUser = new User;
			$newUser->name = $name;
			$newUser->username = $emailId;
			$newUser->phone = $phoneNumber;
			$newUser->role_id = Roles::INVITES_USER;

			$newUser->save();

			$requiredUser = $newUser;
		}
		else
		{
			$requiredUser = $userFromPhoneNumber;
			$isNewUser = 0;

			// update email id and name in case those are different
			$requiredUser->name = $name;
			$requiredUser->username = $emailId;

			$requiredUser->save();
		}

		// generate OTP 
		$otp = rand(10001, 99999);

		$requiredUser->invibe_otp = $otp;
		$requiredUser->save();

		// @mytodo: uncomment this line , I have commented for testing purpose.
		$this->sendOtp($otp, $phoneNumber);

		return response()->json([
			                        'userId'    => $requiredUser->id,
			                        'isNewUser' => $isNewUser
		                        ]);

	}

    public function login(Request $request)
    {
        if (!$request->header('client-id'))
        {
            throw new CustomException("Client id is required.", 403);
        }

        $authClient = $this->validateClientId($request->header('client-id'));

        $this->validatePayload($request, [
            'phoneNumber' => 'required'
        ]);

        $phoneNumber = $request['phoneNumber'];

        $userFromPhoneNumber = User::where('phone', '=', $phoneNumber)
            ->where('role_id', '=', Roles::INVITES_USER)
            ->first();

        if ($userFromPhoneNumber)
        {
            $requiredUser = $userFromPhoneNumber;
            $isNewUser = 0;

            $otp = rand(10001, 99999);

            $requiredUser->invibe_otp = $otp;
            $requiredUser->save();

            $this->sendOtp($otp, $phoneNumber);

            $events = InvibeEvent::ofUser($requiredUser->id);

            $totalEventCount = $events->count();

            return response()->json([
                'userId'    => $requiredUser->id,
                'userName' => $requiredUser->name,
                'eventsCount' => $totalEventCount
            ]);
        }
        else
        {
            throw new CustomException("User doesn't exist", 400);
        }
    }

	public function verifyOtp(Request $request)
	{
		if (!$request->header('client-id'))
		{
			throw new CustomException("Client id is required.", 403);
		}

		$authClient = $this->validateClientId($request->header('client-id'));

		$this->validateJSONPayload($request, [
			'userId' => 'required',
			'otp'    => 'required'
		]);

		$userId = $request->json('userId');
		if (!$user = User::find($userId))
		{
			throw new CustomException("Invalid user id.", 400);
		}

		$otp = $user->invibe_otp;

		if ($otp != $request->json('otp'))
		{
			throw new CustomException("Invalid otp.", 400);
		}

		$accessToken = $this->issueAccessToken($user, $authClient);
        $eventCount = InvibeEvent::ofUser($userId)->count();

		return response()->json(
		    [
		        'accessToken' => $accessToken,
                'eventsCount' => $eventCount
            ]);
	}

	/**
	 * [resendOtp description]
	 *
	 * @return [type] [description]
	 */
	public function resendOtp(Request $request)
	{
		if (!$request->header('client-id'))
		{
			throw new CustomException("Client id is required.", 403);
		}

		$authClient = $this->validateClientId($request->header('client-id'));

		$this->validateJSONPayload($request, [
			'userId' => 'required'
		]);

		$userId = $request->json('userId');

		if (!$user = User::find($userId))
		{
			throw new CustomException("Invalid user id.", 400);
		}

		$otp = $user->invibe_otp;
		$phoneNumber = $user->phone;

		// @mytodo: uncomment this line , I have commented for testing purpose.
		$this->sendOtp($otp, $phoneNumber);

		return response()->json(['success' => 'true']);
	}

	public function getUserDetails(Request $request, $usrId)
	{
		$userDetails = $this->_getUserDetails($request, $usrId);

		return response()->json($userDetails);
	}

	public function updateUserProfile(Request $request, $usrId)
	{
		$actualUserId = $this->checkForAccessToken($request);

		if ($actualUserId != $usrId)
		{
			throw new CustomException("Unauthorised. You don't have access to this profile.", 401);
		}

		$this->validateJSONPayload($request, [
			"phoneNumber" => "required | digits:10"
		]);

		User::where('id', '=', $usrId)
		    ->update(['phone' => $request->json('phoneNumber')]);

		return response()->json(["phoneNumber" => $request->json('phoneNumber')]);
	}

	public function updateProfilePic(Request $request, $usrId)
	{
		return $this->_updateProfilePic($request, $usrId);
	}

    public function registerCommunicationIds(Request $request)
    {
        if (!$request->header('client-id'))
        {
            throw new CustomException("Client id is required.", 403);
        }

        $authClient = $this->validateClientId($request->header('client-id'));

        //TODO: Register player IDs for push messages
    }

}
