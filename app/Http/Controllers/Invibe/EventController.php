<?php namespace App\Http\Controllers\Invibe;

use Illuminate\Support\Facades\App;
use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Http\Models\InvibeEvent;
use App\Http\Models\InvibeEventGuest;
use App\Http\Models\InvibeTheme;
use Carbon\Carbon;
use App\Exceptions\CustomException;

class EventController extends BaseController
{
	public function getAllEvents(Request $request)
	{
		$usrId = $this->checkForAccessToken($request);

		// checks for any filters and return as variable
		extract($this->checkForFilters($request));

		// Checks for pagination and ordering related query params in the request and 
		// return $limit, $offset, $order and $orderBy
		extract($this->checkForPaginationAndOrderParams($request));

		$events = InvibeEvent::ofUser($usrId)
		                     ->ofType($typeOfEvent)
		                     ->orderBy($orderBy, $order);

		//$guests

		$totalEventCount = $events->count();

		if ($limit)
		{
			$events = $events->skip($offset)->take($limit);
		}

		$events = $events->get();

		foreach ($events as &$event)
		{
			$event = $this->appendThemeBaseURL($event);
		}

		$events = $events->toArrayCamel();

		//Getting RSVP details from guests table.
		foreach ($events as &$event)
		{
			$this->getRSVPDetails($event, $event['id']);
		}

		$response =
			[
				"events"      => $events,
				"totalEvents" => $totalEventCount,
				"limit"       => $limit,
				"offset"      => $offset
			];

		return response()->json($response);

	}

	public function getEventCount(Request $request)
	{
		$usrId = $this->checkForAccessToken($request);

		$eventCount = InvibeEvent::ofUser($usrId)->count();

		$response =
			[
				"count" => $eventCount
			];

		return response()->json($response);

	}

	public function getASpecificEvent(Request $request, $evtId)
	{
		$usrId = $this->checkForAccessToken($request);

		if (!$event = InvibeEvent::find($evtId))
		{
			throw new CustomException("The event doesn't exists.", 400);
		}

		$this->checkForEventOwner($event, $usrId);

		$event = InvibeEvent::find($evtId)->toArrayCamel();

		$this->getRSVPDetails($event, $event['id']);

		return response()->json($event);

	}

	public function createANewEvent(Request $request)
	{
		$usrId = $this->checkForAccessToken($request);

		$this->validateCreateEventPayload($request);

		$eventPayload = $this->parseRequestAndConvertToSnakeCase($request);

		$eventPayload['user_id'] = $usrId;

		$eventPayload['url_code'] = preg_replace('/[^\w]/', '', $eventPayload['name']) . rand(10000001, 99999999);

		$newEvent = InvibeEvent::create($eventPayload);

		$fileName = $this->updateEventPic($request, $newEvent['id']);

		if ($fileName != null)
		{
			$eventPayload['event_pic'] = env('GALLERY_BASE_URL') .
				env('EVENT_IMAGE_PATH') . $fileName;

		}

		InvibeEvent::find($newEvent['id'])->update($eventPayload);

		$newEvent = InvibeEvent::find($newEvent['id']);

		return response()->json($this->appendThemeBaseURL($newEvent)->toArrayCamel());

	}

	private function appendThemeBaseURL($event)
	{
		$eventTheme = InvibeTheme::find($event['theme_id']);

		$themeImageBaseURL = env('GALLERY_BASE_URL') . env('THEME_BACKGROUND_IMAGE_PATH');
		$themeSecondaryImageBaseURL = env('GALLERY_BASE_URL') . env('THEME_SECONDARY_BACKGROUND_IMAGE_PATH');

		$event['img_path'] = $themeImageBaseURL . $eventTheme['img_path'];
		$event['secondary_img_path'] = $themeSecondaryImageBaseURL . $eventTheme['secondary_img_path'];

		return $event;
	}

	public function updateAParticularEvent(Request $request, $evtId) //$request->json()->all()
	{
		$usrId = $this->checkForAccessToken($request);

		if (!$event = InvibeEvent::find($evtId))
		{
			throw new CustomException("The event doesn't exists.", 400);
		}

		$this->checkForEventOwner($event, $usrId);

		$this->validateUpdateEventPayload($request);

		$eventPayload = $this->parseRequestAndConvertToSnakeCase($request);

		$editedEvent = InvibeEvent::find($evtId);

		$fileName = $this->updateEventPic($request, $editedEvent['id']);

		if ($fileName != null)
		{
			$eventPayload['event_pic'] = env('GALLERY_BASE_URL') .
				env('EVENT_IMAGE_PATH') . $fileName;

		}

		InvibeEvent::find($evtId)->update($eventPayload);

		$editedEvent = InvibeEvent::find($editedEvent['id']);

		return response()->json($this->appendThemeBaseURL($editedEvent)->toArrayCamel());

	}

	public function deleteAParticularEvent(Request $request, $evtId)
	{
		$usrId = $this->checkForAccessToken($request);

		if (!$event = InvibeEvent::find($evtId))
		{
			throw new CustomException("The event doesn't exists.", 400);
		}

		$this->checkForEventOwner($event, $usrId);

		InvibeEvent::where('id', '=', $evtId)->delete();

		return response()->json(["success" => "true"]);
	}

	public function addGuestsToEvent(Request $request, $evtId)
	{
		$usrId = $this->checkForAccessToken($request);

		if (!$event = InvibeEvent::find($evtId))
		{
			throw new CustomException("The event doesn't exists.", 400);
		}

		$this->checkForEventOwner($event, $usrId);

		$this->validateAddGuestPayload($request);

		$sendInvite = 0;

		if ($request->has('sendInvite'))
		{
			$sendInvite = $request->get('sendInvite');
		}

		$guests = InvibeEventGuest::bulkInsert($request->json()->all(), $evtId);

		if ($sendInvite)
		{
			// Write function to send invite
			//InvibeEventGuest::sendInvite($request->json()->all());
		}

		return response()->json($guests);
	}

	public function getGuestsForEvent(Request $request, $evtId)
	{
		$usrId = $this->checkForAccessToken($request);

		if (!$event = InvibeEvent::find($evtId))
		{
			throw new CustomException("The event doesn't exists.", 400);
		}

		$this->checkForEventOwner($event, $usrId);

		$response['guests'] = InvibeEventGuest::where('event_id', $evtId)->get()->toArrayCamel();;

		$this->getRSVPDetails($response, $evtId);

		return response($response);
	}

	/**
	 * Private functions
	 */

	private function validateCreateEventPayload($request)
	{
		$this->validatePayload($request, [
			"typeId"            => "required | integer",
			"name"              => "string",
			"eventDate"         => "date",
			"eventStartTime"    => "date_format:H:i",
			"eventEndTime"      => "date_format:H:i",
			"venueAddress"      => "string",
			"venuePin"          => "numeric",
			"venueLandmark"     => "string",
			"hosts"             => "string",
			"guestMessage"      => "string",
			"venueMapLatitude"  => "numeric | between:-90,+90",
			"venueMapLongitude" => "numeric | between:-180,180",
			"themeId"           => "exists:invibe_themes,id",
			"contactNumber"     => "numeric",
			"altContactNumber"  => "numeric",
			"eventPic"          => "required|mimes:png,jpg,jpeg,bmp,pdf"
		]);
	}

	private function validateUpdateEventPayload($request)
	{
		$this->validateJSONPayload($request, [
			"typeId"                    => "integer",
			"name"                      => "string",
			"eventDate"                 => "date",
			"eventStartTime"            => "date_format:H:i",
			"eventEndTime"              => "date_format:H:i",
			"venueAddress"              => "string",
			"venuePin"                  => "numeric",
			"venueLandmark"             => "string",
			"hosts"                     => "string",
			"guestMessage"              => "string",
			"venueMapLatitude"          => "numeric | between:-90,+90",
			"venueMapLongitude"         => "numeric | between:-180,180",
			"themeId"                   => "exists:invibe_themes,id",
			"contactNumber"             => "numeric",
			"altContactNumber"          => "numeric",
			"sendAutoreminderToGuest"   => "in:0,1",
			"sendUpdatesOnModification" => "in:0,1"
		]);
	}

	private function validateAddGuestPayload($request)
	{
		$payload = $request->json()->all();

		if (gettype($payload) != 'array')
		{
			throw new CustomException("Invalid payload, it should be an array.", 400);
		}

		foreach ($payload as $guest)
		{
			$this->validateArray($guest, [
				"name"  => "string",
				"email" => "email",
				"phone" => "required | numeric"
			]);
		}
	}

	private function parseRequestAndConvertToSnakeCase($request)
	{
		$eventPayload = $request->except(['eventPic']);

		$pic = $request->file('eventPic');
		if ($pic != null)
		{
			$eventPayload['eventPicExt'] = $pic->getClientOriginalExtension();
		}

		if (isset($eventPayload['eventDate']))
		{
			$eventPayload['eventDate'] = Carbon::parse($eventPayload['eventDate'])->toDateString();
		}

		$eventPayload = $this->camelCaseToSnakeCaseArrayAttibutes($eventPayload);

		return $eventPayload;
	}

	private function checkForPaginationAndOrderParams($request)
	{
		$params = $this->checkForPagination($request);

		$order = "asc";

		if ($request->has('order') && in_array($request->get('order'), ["asc", "desc"]))
		{
			$order = $request->get('order');
		}

		$orderBy = "event_date";

		// This has done like this, so that in future if any other params comes for ordering , it can be done very easily
		if ($request->has('orderBy') && in_array($request->get('orderBy'), ["eventDate"]))
		{
			$orderBy = $request->get('orderBy');

			switch ($orderBy)
			{
				case 'eventDate':
					$orderBy = "event_date";
					break;
				default:
					# code...
					break;
			}
		}

		$params['order'] = $order;
		$params['orderBy'] = $orderBy;

		return $params;
	}

	private function checkForFilters($request)
	{
		// type of booking
		$typeOfEvent = "all";

		if ($request->has('eventType'))
		{
			$typeOfEvent = $request->get('eventType');
		}

		return ["typeOfEvent" => $typeOfEvent];
	}

	private function checkForEventOwner($event, $usrId)
	{
		if ($event->user_id != $usrId)
		{
			throw new CustomException("You don't have access to this event.", 400);
		}
	}

	private function updateEventPic($request, $eventId)
	{
		$pic = $request->file('eventPic');
		$filename = null;
		if ($pic != null)
		{
			$target = storage_path() . '/invibe/event_pic/';
			$filename = $eventId . rand(101, 999) . "." . $pic->getClientOriginalExtension();
			$pic->move($target, $filename);
		}

		return $filename;
	}

	private function getRSVPDetails(&$object, $evtId)
	{
		$object['rsvpNo'] = InvibeEventGuest::where('event_id', '=', $evtId)->where('rsvp_status', 0)->count();
		$object['rsvpYes'] = InvibeEventGuest::where('event_id', '=', $evtId)->where('rsvp_status', 1)->count();
	}
}
