<?php namespace App\Http\Controllers\Invibe;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;

use App\Jobs\TestJob;

use App\Http\Models\InvibeEventType;

class EventTypeController extends BaseController
{
    public function getAllEventTypes(Request $request)
    {
    	$this->checkForAccessToken($request);

        $eventTypes = $this->appendThemeBaseURL(InvibeEventType::with('themes')->get()->toArrayCamel());

    	return response($eventTypes);
    }

    public function testJob()
    {
    	$this->dispatch(new TestJob());
    }

    private function appendThemeBaseURL($eventTypes)
    {
        $themeImageBaseURL = env('GALLERY_BASE_URL') . env('THEME_BACKGROUND_IMAGE_PATH');
        $themeSecondaryImageBaseURL = env('GALLERY_BASE_URL') . env('THEME_SECONDARY_BACKGROUND_IMAGE_PATH');

        foreach ($eventTypes as &$eventType) {
            foreach ($eventType['themes'] as &$eventTheme) {
                $eventTheme['imgPath'] = $themeImageBaseURL . $eventTheme['imgPath'];
                $eventTheme['secondaryImgPath'] = $themeSecondaryImageBaseURL . $eventTheme['secondaryImgPath'];
            }
        }

        return $eventTypes;
    }
}