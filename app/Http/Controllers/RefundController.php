<?php

namespace App\Http\Controllers;

use App\Http\Models\Refunds\CustomerRefunds;
use App\Http\Models\Settlements\LogBookingFinance;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\Settlements\SettlementDetails;
use App\Http\Models\TicketBooking;
use App\Http\Models\User;
use App\Jobs\Mail\Finance\MailProcessRefundAlertToCustomerJob;
use App\Jobs\Sms\SMSProcessRefundAlertToCustomerJob;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RefundController extends BaseController
{
	public function getCustomerRefunds($query, Request $request)
	{
		// access-token
		$this->checkForAccessToken($request);

		try
		{
			// Joined with ticket and ticket_bookings for the sake of search
			$customerRefunds = CustomerRefunds::join('ticket_bookings', 'ticket_bookings.id', '=', 'customer_refunds.ticket_booking_id')
			                                  ->join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                  ->select('customer_refunds.*')
			                                  ->whereNull('customer_refunds.deleted_at')
			                                  ->whereNull('ticket_bookings.deleted_at')
			                                  ->whereNull('ticket.deleted_at')
			                                  ->whereNull('customer_refunds.rejected_at')
			                                  ->orderBy('updated_at', 'DESC');
			$clearFilter = false;

			if ($query == 'pending')
			{
				$customerRefunds->whereNull('customer_refunds.refunded_at');
			}
			elseif ($query == 'refunded')
			{
				$customerRefunds->whereNotNull('customer_refunds.refunded_at');
			}

			$userSearch = "";
			if ($request['search'] && trim($request['search']))
			{
				$clearFilter = true;
				$userSearch = trim($request['search']);
				$searchWords = explode(' ', $userSearch);

				$customerRefunds->where(function ($searchQuery) use ($searchWords) {
					foreach ($searchWords as $word)
					{
						$searchQuery->orWhere('ticket.name', 'LIKE', "%$word%")
						            ->orWhere('ticket_bookings.booking_id', 'LIKE', "%$word%");
					}
				});
			}

			if ($request['minRefundDate'])
			{
				$customerRefunds->where('customer_refunds.refunded_at', '>=', date('Y-m-d H:i:s', strtotime($request['minRefundDate'])));
				$clearFilter = true;
			}

			if ($request['maxRefundDate'])
			{
				$customerRefunds->where('customer_refunds.refunded_at', '<', date('Y-m-d H:i:s', strtotime($request['maxRefundDate']) + (24 * 60 * 60)));
				$clearFilter = true;
			}

			$customerRefunds = $customerRefunds->get();
			$refundBookingIds = $customerRefunds->pluck('ticket_booking_id');

			$ticketBookingsArray = TicketBooking::join('ticket', 'ticket.id', '=', 'ticket_bookings.ticket_id')
			                                    ->where('ticket_bookings.party_date_time', '>=', time() - (90 * (24 * 60 * 60)))
			                                    ->whereNotIn('ticket_bookings.id', $refundBookingIds)
			                                    ->whereNull('ticket.deleted_at')
			                                    ->whereNull('ticket_bookings.deleted_at')
			                                    ->select('ticket_bookings.*', 'ticket.name')
			                                    ->orderBy('ticket_bookings.updated_at', 'DESC')
			                                    ->get()
			                                    ->toArray();

			$refundsList = [];
			if (count($customerRefunds))
			{
				foreach ($customerRefunds as $customerRefund)
				{
					$ticketBooking = $customerRefund->booking;

					if (!$ticketBooking)
					{
						$this->sendAndSaveNonExceptionReport([
							                                     'code'      => config('evibe.error_code.get_customer_refund'),
							                                     'url'       => \Illuminate\Support\Facades\Request::fullUrl(),
							                                     'method'    => \Illuminate\Support\Facades\Request::method(),
							                                     'message'   => '[RefundController.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking',
							                                     'exception' => '[RefundController.php - ' . __LINE__ . '] ' . 'Unable to find ticket booking',
							                                     'trace'     => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . '] [Customer Refund Id: ' . $customerRefund->id . ']',
							                                     'details'   => '[Ticket Booking Id: ' . $customerRefund->ticket_booking_id . '] [Customer Refund Id: ' . $customerRefund->id . ']'
						                                     ]);
						continue; // cannot happen
					}

					$customerName = null;
					$customerEmail = null;
					$customerPhone = null;
					$partyLocation = null;
					$partyCity = null;
					$partyDateTime = $ticketBooking->party_date_time ? date("d M Y h:i A", $ticketBooking->party_date_time) : null;

					$ticket = $ticketBooking->ticket;
					if ($ticket)
					{
						$area = $ticket->area;
						if ($area)
						{
							$partyLocation = $area->name;
						}

						$city = $ticket->city;
						if ($city)
						{
							$partyCity = $city->name;
						}
						$customerName = $ticket->name;
						$customerEmail = $ticket->email;
						$customerPhone = $ticket->phone;
					}

					$handlerDetails = [];
					$handler = $customerRefund->handler;
					if ($handler)
					{
						$handlerDetails = [
							'id'     => $handler->id,
							'name'   => $handler->name,
							'email'  => $handler->username,
							'phone'  => $handler->phone,
							'roleId' => $handler->role_id,
							'role'   => config('evibe.role.id.' . $handler->role_id)
						];
					}

					$cancelledUserDetails = [];
					$user = User::find($ticketBooking->cancelled_user_id);
					if ($user)
					{
						$cancelledUserDetails = [
							'id'     => $user->id,
							'name'   => $user->name,
							'email'  => $user->username,
							'phone'  => $user->phone,
							'roleId' => $user->role_id,
							'role'   => config('evibe.role.id.' . $user->role_id)
						];
					}

					$partnerDetails = [];
					$partner = $ticketBooking->provider;
					if ($partner)
					{
						$partnerDetails = [
							'partnerId'     => $ticketBooking->map_id,
							'partnerTypeId' => $ticketBooking->map_type_id,
							'partnerType'   => config('evibe.partner_type.' . $ticketBooking->map_type_id),
							'name'          => $partner->name,
							'email'         => $partner->email,
							'phone'         => $partner->phone,
						];
					}

					array_push($refundsList, [
						'id'                   => $customerRefund->id,
						'ticketBookingId'      => $customerRefund->ticket_booking_id,
						'bookingId'            => $ticketBooking->booking_id,
						'partyLocation'        => $partyLocation,
						'partyCity'            => $partyCity,
						'partyDateTime'        => $partyDateTime,
						'customerName'         => $customerName,
						'customerEmail'        => $customerEmail,
						'customerPhone'        => $customerPhone,
						'bookingAmount'        => $ticketBooking->booking_amount,
						'advanceAmount'        => $ticketBooking->advance_amount,
						'refundAmount'         => $customerRefund->refund_amount,
						'refundedAt'           => $customerRefund->refunded_at,
						'refundReference'      => $customerRefund->refund_reference,
						'cancelledAt'          => $ticketBooking->cancelled_at ? date("d M Y h:i A", strtotime($ticketBooking->cancelled_at)) : null,
						'customerCancelledAt'  => $ticketBooking->customer_cancelled_at ? date("d M Y h:i A", strtotime($ticketBooking->customer_cancelled_at)) : null,
						'handlerCancelledAt'   => $ticketBooking->handler_cancelled_at ? date("d M Y h:i A", strtotime($ticketBooking->handler_cancelled_at)) : null,
						'cancellationComments' => $ticketBooking->cancellation_comments,
						'comments'             => $customerRefund->comments,
						'evibeBear'            => $customerRefund->evibe_bear,
						'partnerBear'          => $customerRefund->partner_bear,
						//'scheduleDate'    => $customerRefund->schedule_date,
						//'haltedAt'        => $customerRefund->halted_at,
						//'haltComments'    => $customerRefund->halt_comments,
						//'resumedAt'       => $customerRefund->resumed_at,
						//'resumeComments'  => $customerRefund->resume_comments,
						'bookingDashLink'      => config('evibe.hosts.dash') . '/tickets/' . $ticketBooking->ticket_id . '/bookings',
						'partnerDetails'       => $partnerDetails,
						'handlerDetails'       => $handlerDetails,
						'cancelledUserDetails' => $cancelledUserDetails
					]);
				}
			}

			return response()->json([
				                        'success'        => true,
				                        'refundsList'    => $refundsList,
				                        'ticketBookings' => $ticketBookingsArray,
				                        'filters'        => [
					                        'clearFilter'   => $clearFilter,
					                        'search'        => $userSearch,
					                        'minRefundDate' => $request['minRefundDate'],
					                        'maxRefundDate' => $request['maxRefundDate']
				                        ]
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);

		}
	}

	public function createCustomerRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$ticket = $ticketBooking->ticket;

			// advance payment
			if (!$ticketBooking->is_advance_paid)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'No payment has been recorded to initiate refund [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			//// party done || cancelled ticket
			//if (!((($ticket->status_id == config('evibe.ticket.status.booked')) && ($ticketBooking->party_date_time < time()))
			//	|| ($ticket->status_id == config('evibe.ticket.status.cancelled'))
			//	|| ($ticket->status_id == config('eivbe.ticket.status.autocancel'))))
			//{
			//	return response()->json([
			//		                        'success' => false,
			//		                        'error'   => 'Ticket booking is not in a valid state to initiate refund [Ticket Booking Id: ' . $ticketBookingId . ']'
			//	                        ]);
			//}

			$refundAmount = $request['refundAmount'];
			$evibeBear = $request['evibeBear'];
			$partnerBear = $request['partnerBear'];
			$refundComments = $request['comments'];

			// validation
			if (!$refundAmount)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly enter the refund amount.'
				                        ]);
			}

			if ($refundAmount > $ticketBooking->advance_amount)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Refund amount cannot be greater that advance amount'
				                        ]);
			}

			if ($refundAmount < ($evibeBear + $partnerBear))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Sum of evibe bear and partner bear should not be greater than refund amount.'
				                        ]);
			}

			if (!$refundComments)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly mention the reasons for initiating the refund.'
				                        ]);
			}

			$existingRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if ($existingRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'A customer refund already exists for this booking'
				                        ]);
			}
			else
			{
				//$ticketBooking->refund_amount = $refundAmount;
				//$ticketBooking->evibe_bear = $evibeBear;
				//$ticketBooking->partner_bear = $partnerBear;
				//$ticketBooking->refund_comments = $refundComments;
				//
				//$ticketBooking->save();

				$customerRefund = CustomerRefunds::create([
					                                          'ticket_booking_id' => $ticketBookingId,
					                                          'refund_amount'     => $refundAmount,
					                                          'evibe_bear'        => $evibeBear,
					                                          'partner_bear'      => $partnerBear,
					                                          'handler_id'        => $handlerId,
					                                          'comments'          => $refundComments,
					                                          'created_at'        => Carbon::now(),
					                                          'updated_at'        => Carbon::now()
				                                          ]);

				if ($customerRefund)
				{
					LogBookingFinance::create([
						                          'ticket_booking_id'    => $ticketBookingId,
						                          'handler_id'           => $handlerId,
						                          'type_update'          => 'Manual',
						                          'comments'             => 'Customer refund has been created',
						                          'total_booking_amount' => $ticketBooking->booking_amount,
						                          'refund_amount'        => $refundAmount,
						                          'evibe_bear'           => $evibeBear,
						                          'partner_bear'         => $partnerBear,
						                          'created_at'           => Carbon::now(),
						                          'updated_at'           => Carbon::now()
					                          ]);

					// @todo: alert admin that a refund has been initiated

					return response()->json([
						                        'success'          => true,
						                        'customerRefundId' => $customerRefund->id
					                        ]);
				}
				else
				{
					return response()->json([
						                        'success' => false,
						                        'error'   => 'Some error occurred while creating customer refund'
					                        ]);
				}
			}
			// auto pay cancel
			// status check

			// settlement check?
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function updateCustomerRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if (!$customerRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to update as the customer refund for the booking is either refunded or deleted [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$refundAmount = $request['refundAmount'];
			$evibeBear = $request['evibeBear'];
			$partnerBear = $request['partnerBear'];

			// validation
			if (!$refundAmount)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly enter the refund amount.'
				                        ]);
			}

			if ($refundAmount < ($evibeBear + $partnerBear))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Sum of evibe bear and partner bear should not be greater than refund amount.'
				                        ]);
			}

			$comments = 'Customer refund has been updated.';
			$oldRefundAmount = $customerRefund->refund_amount;
			$oldEvibeBear = $customerRefund->evibe_bear;
			$oldPartnerBear = $customerRefund->partner_bear;
			//$oldScheduleDate = $customerRefund->schedule_date;

			if ($refundAmount != $oldRefundAmount)
			{
				$comments .= ' Refund amount has been updated from ' . $oldRefundAmount . ' to ' . $refundAmount . '.';
				$customerRefund->refund_amount = $refundAmount;
			}

			if ($evibeBear != $oldEvibeBear)
			{
				$comments .= ' Evibe bear has been updated from ' . $oldEvibeBear . ' to ' . $evibeBear . '.';
				$customerRefund->evibe_bear = $evibeBear;
			}

			if ($partnerBear != $oldPartnerBear)
			{
				$comments .= ' Partner bear has been updated from ' . $oldPartnerBear . ' to ' . $partnerBear . '.';
				$customerRefund->partner_bear = $partnerBear;
			}

			//if ($request['scheduleDate'] && ($request['scheduleDate'] != $oldScheduleDate))
			//{
			//	$comments .= ' Schedule date has been updated from ' . $oldScheduleDate . ' to ' . $request['scheduleDate'] . '.';
			//	$customerRefund->schedule_date = $request['scheduleDate'];
			//}
			//
			//if ($request['resumeHalt'] && !(is_null($customerRefund->halted_at)))
			//{
			//	$comments .= ' Halt has been resumed.';
			//	$customerRefund->resumed_at = Carbon::now();
			//	$customerRefund->resume_comments = $request['resumeComments'];
			//}

			$customerRefund->handler_id = $handlerId;
			$customerRefund->updated_at = Carbon::now();
			$customerRefund->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => $comments,
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'refund_amount'        => $refundAmount,
				                          'evibe_bear'           => $evibeBear,
				                          'partner_bear'         => $partnerBear,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			return response()->json([
				                        'success'          => true,
				                        'customerRefundId' => $customerRefund->id
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function oldApproveCustomerRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if (!$customerRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to approve as the refund for the booking is either refunded or deleted [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$refundAmount = $request['refundAmount'];
			$evibeBear = $request['evibeBear'];
			$partnerBear = $request['partnerBear'];

			// validation
			if (!($refundAmount && $evibeBear && $partnerBear))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly enter all the required amount details [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			if ($refundAmount != ($evibeBear + $partnerBear))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Amount refunded should be equal to the sum of evibe bear and partner bear [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			// attach images and reference id
			$comments = 'Customer has been refunded.';
			$oldRefundAmount = $customerRefund->refund_amount;
			$oldEvibeBear = $customerRefund->evibe_bear;
			$oldPartnerBear = $customerRefund->partner_bear;

			if ($refundAmount && ($refundAmount != $oldRefundAmount))
			{
				$comments .= ' Refund amount has been updated from ' . $oldRefundAmount . ' to ' . $refundAmount . '.';
				$customerRefund->refund_amount = $refundAmount;
			}

			if ($evibeBear && ($evibeBear != $oldEvibeBear))
			{
				$comments .= ' Evibe bear has been updated from ' . $oldEvibeBear . ' to ' . $evibeBear . '.';
				$customerRefund->evibe_bear = $evibeBear;
			}

			if ($partnerBear && ($partnerBear != $oldPartnerBear))
			{
				$comments .= ' Partner bear has been updated from ' . $oldPartnerBear . ' to ' . $partnerBear . '.';
				$customerRefund->partner_bear = $partnerBear;
			}

			$customerRefund->refunded_at = Carbon::now();
			$customerRefund->updated_at = Carbon::now();
			$customerRefund->hnadler_id = $handlerId;
			$customerRefund->save();

			$ticketBooking->refund_amount = $refundAmount;
			$ticketBooking->refund_comments = $customerRefund->comments;
			$ticketBooking->evibe_bear = $evibeBear;
			$ticketBooking->partner_bear = $partnerBear;
			$ticketBooking->updated_at = Carbon::now();
			$ticketBooking->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => $comments,
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'refund_amount'        => $refundAmount,
				                          'evibe_bear'           => $evibeBear,
				                          'partner_bear'         => $partnerBear,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			return response()->json([
				                        'success'          => true,
				                        'customerRefundId' => $customerRefund->id,
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function approveCustomerRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if (!$customerRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to approve as the refund for the booking is either refunded or deleted.'
				                        ]);
			}

			$refundReference = $request['refundReference'];
			if (!$refundReference)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide reference for the transfer of refund amount to the customer.'
				                        ]);
			}

			// attach images and reference id
			$comments = 'Customer has been refunded.';

			$customerRefund->refund_reference = $refundReference;
			$customerRefund->refunded_at = Carbon::now();
			$customerRefund->updated_at = Carbon::now();
			$customerRefund->handler_id = $handlerId;
			$customerRefund->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => $comments,
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'refund_amount'        => $customerRefund->refund_amount,
				                          'evibe_bear'           => $customerRefund->evibe_bear,
				                          'partner_bear'         => $customerRefund->partner_bear,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			// if partner bear, update in schedule booking settlement (or) partner adjustment
			// partner bear won't be there for cancellation refund

			$scheduledBooking = ScheduleSettlementBookings::where('ticket_booking_id', $ticketBookingId)
			                                              ->whereNull('deleted_at')
			                                              ->first();

			$autoSettlement = new AutoSettlementController();

			if ($scheduledBooking)
			{
				$settlementDetails = SettlementDetails::where('settlement_booking_id', $scheduledBooking->id)
				                                      ->whereNull('deleted_at')
				                                      ->first();

				if (!$settlementDetails)
				{
					// update ticket booking
					$ticketBooking->refund_amount = $customerRefund->refund_amount;
					$ticketBooking->refund_comments = $customerRefund->comments;
					$ticketBooking->evibe_bear = $customerRefund->evibe_bear;
					$ticketBooking->partner_bear = $customerRefund->partner_bear;
					$ticketBooking->updated_at = Carbon::now();
					$ticketBooking->save();

					if ($ticketBooking->cancelled_at)
					{
						$settlementAmount = $ticketBooking->advance_amount - $ticketBooking->refund_amount - $ticketBooking->evibe_service_fee;

						$scheduledBooking->advance_amount = $ticketBooking->advance_amount;
						$scheduledBooking->cgst_amount = 0;
						$scheduledBooking->sgst_amount = 0;
						$scheduledBooking->igst_amount = 0;
						$scheduledBooking->gst_amount = 0;
						$scheduledBooking->evibe_service_fee = $ticketBooking->evibe_service_fee;
						$scheduledBooking->partner_bore_refund = 0;
					}
					else
					{
						// recalculate settlement amount
						$settlementRes = $autoSettlement->calculateBookingSettlementAmount($ticketBooking);

						$settlementAmount = isset($settlementRes['settlementAmount']) ? $settlementRes['settlementAmount'] : null;

						$scheduledBooking->booking_amount = isset($settlementRes['bookingAmount']) ? $settlementRes['bookingAmount'] : null;
						$scheduledBooking->advance_amount = isset($settlementRes['advanceAmount']) ? $settlementRes['advanceAmount'] : null;
						$scheduledBooking->cgst_amount = isset($settlementRes['cgstAmount']) ? $settlementRes['cgstAmount'] : null;
						$scheduledBooking->sgst_amount = isset($settlementRes['sgstAmount']) ? $settlementRes['sgstAmount'] : null;
						$scheduledBooking->igst_amount = isset($settlementRes['igstAmount']) ? $settlementRes['igstAmount'] : null;
						$scheduledBooking->gst_amount = isset($settlementRes['gstAmount']) ? $settlementRes['gstAmount'] : null;
						$scheduledBooking->evibe_service_fee = isset($settlementRes['evibeServiceFee']) ? $settlementRes['evibeServiceFee'] : null;
						$scheduledBooking->partner_bore_refund = isset($settlementRes['partnerBoreRefund']) ? $settlementRes['partnerBoreRefund'] : null;
					}

					$scheduledBooking->settlement_amount = $settlementAmount;
					$scheduledBooking->created_at = Carbon::now();
					$scheduledBooking->save();

					LogBookingFinance::create([
						                          'ticket_booking_id'         => $ticketBookingId,
						                          'handler_id'                => $handlerId,
						                          'type_update'               => 'Auto',
						                          'comments'                  => 'Scheduled booking settlement has been updated',
						                          'total_booking_amount'      => $ticketBooking->booking_amount,
						                          'partner_settlement_amount' => $settlementAmount,
						                          'created_at'                => Carbon::now(),
						                          'updated_at'                => Carbon::now()
					                          ]);

					$comments .= ' Ticket Booking has been updated.';
				}
				else
				{
					// since 'partner_bear' is in decimal
					if (round($customerRefund->partner_bear))
					{
						// partner adjustment
						$partnerAdjustment = PartnerAdjustments::create([
							                                                'partner_id'        => $ticketBooking->map_id,
							                                                'partner_type_id'   => $ticketBooking->map_type_id,
							                                                'ticket_booking_id' => $ticketBooking->id,
							                                                'adjustment_amount' => $customerRefund->partner_bear,
							                                                'pay_evibe'         => 1,
							                                                'handler_id'        => $handlerId,
							                                                'comments'          => 'Adjustment created because of partner bear in customer refund',
							                                                'schedule_date'     => $autoSettlement->calculateSettlementScheduleDateTime(time())
						                                                ]);

						LogBookingFinance::create([
							                          'ticket_booking_id'         => $ticketBooking->id,
							                          'handler_id'                => $handlerId,
							                          'type_update'               => 'Auto',
							                          'comments'                  => 'Partner Adjustment has been created [Ticket Booking Id: ' . $ticketBooking->id . ']',
							                          'total_booking_amount'      => $ticketBooking->booking_amount,
							                          'partner_settlement_amount' => $customerRefund->partner_bear,
							                          'created_at'                => Carbon::now(),
							                          'updated_at'                => Carbon::now()
						                          ]);

						$comments .= ' Partner adjustment has been created.';
					}
				}
			}
			else
			{
				// since 'partner_bear' is in decimal
				if (round($customerRefund->partner_bear))
				{
					// partner adjustment
					$partnerAdjustment = PartnerAdjustments::create([
						                                                'partner_id'        => $ticketBooking->map_id,
						                                                'partner_type_id'   => $ticketBooking->map_type_id,
						                                                'ticket_booking_id' => $ticketBooking->id,
						                                                'adjustment_amount' => $customerRefund->partner_bear,
						                                                'pay_evibe'         => 1,
						                                                'handler_id'        => $handlerId,
						                                                'comments'          => 'Adjustment created because of partner bear in customer refund',
						                                                'schedule_date'     => $autoSettlement->calculateSettlementScheduleDateTime(time())
					                                                ]);

					LogBookingFinance::create([
						                          'ticket_booking_id'         => $ticketBooking->id,
						                          'handler_id'                => $handlerId,
						                          'type_update'               => 'Auto',
						                          'comments'                  => 'Partner Adjustment has been created [Ticket Booking Id: ' . $ticketBooking->id . ']',
						                          'total_booking_amount'      => $ticketBooking->booking_amount,
						                          'partner_settlement_amount' => $customerRefund->partner_bear,
						                          'created_at'                => Carbon::now(),
						                          'updated_at'                => Carbon::now()
					                          ]);

					$comments .= ' Partner adjustment has been created.';
				}
			}

			$ticket = $ticketBooking->ticket;
			$partyLocation = $ticket->area ? $ticket->area->name : null;
			$city = $ticket->city ? $ticket->city->name : null;
			$occasion = $ticket->event ? $ticket->event->name : null;

			$optionDetails = new BaseController();
			$mappedValues = $optionDetails->fillMappingValues([
				                                                  'isCollection' => false,
				                                                  'mapId'        => $ticketBooking->ticketMapping->map_id,
				                                                  'mapTypeId'    => $ticketBooking->ticketMapping->map_type_id
			                                                  ]);

			$mailData = [
				'customerName'        => $ticket->name,
				'customerPhone'       => $ticket->phone,
				'customerEmail'       => $ticket->email,
				'bookingId'           => $ticketBooking->booking_id,
				'partyDateTime'       => date('d M Y h:i A', $ticketBooking->party_date_time),
				'city'                => $city,
				'occasion'            => $occasion,
				'partyLocation'       => $partyLocation,
				'optionName'          => ($mappedValues && isset($mappedValues['name'])) ? $mappedValues['name'] : '--',
				'optionLink'          => ($mappedValues && isset($mappedValues['publicLink'])) ? $mappedValues['publicLink'] : null,
				'dashLink'            => config('evibe.hosts.dash') . '/tickets/' . $ticket->id . '/bookings',
				'bookingAmount'       => $ticketBooking->booking_amount,
				'refundAmount'        => $this->formatPrice($customerRefund->refund_amount),
				'refundReference'     => $customerRefund->refund_reference,
				'refundComments'      => $customerRefund->comments,
				'customerCancelledAt' => $ticketBooking->customer_cancelled_at,
				'handlerCancelledAt'  => $ticketBooking->handler_cancelled_at,
				'cancelledAt'         => $ticketBooking->cancelled_at,
				'date'                => date('d/m/y'),
				'evibePhone'          => config('evibe.contact.company.plain_phone'),
				'evibeEmail'          => config('evibe.contact.accounts.group'),
				'startTime'           => config('evibe.contact.company.working.start_time'),
				'endTime'             => config('evibe.contact.company.working.end_time')
			];

			// refund mail alerts (sms + emails)
			// @see: removed on 09 Jun 2018 by @Anji
			//$this->dispatch(new MailProcessRefundAlertToTeamJob($mailData));
			$this->dispatch(new MailProcessRefundAlertToCustomerJob($mailData));
			$this->dispatch(new SMSProcessRefundAlertToCustomerJob($mailData));

			return response()->json([
				                        'success'          => true,
				                        'customerRefundId' => $customerRefund->id,
				                        'successMsg'       => $comments
			                        ]);
		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json(['success' => false,
			                         'error'   => $e->getMessage()]);
		}
	}

	public function rejectCustomerRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find ticket booking [TB Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if (!$customerRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to reject as the refund for the booking is either refunded or deleted [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$rejectionComments = $request['comments'];
			if (!$rejectionComments)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Kindly provide the reason to reject this refund.'
				                        ]);
			}

			$customerRefund->rejected_at = Carbon::now();
			$customerRefund->reject_comments = $rejectionComments;
			$customerRefund->handler_id = $handlerId;
			$customerRefund->updated_at = Carbon::now();
			$customerRefund->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => 'Customer refund has been rejected',
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'refund_amount'        => $customerRefund->refund_amount,
				                          'evibe_bear'           => $customerRefund->evibe_bear,
				                          'partner_bear'         => $customerRefund->partner_bear,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			return response()->json([
				                        'success'          => true,
				                        'customerRefundId' => $customerRefund->id
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}

	public function haltScheduledRefund($ticketBookingId, Request $request)
	{
		// access-token
		$handlerId = $this->checkForAccessToken($request);

		try
		{
			if (!$ticketBooking = TicketBooking::find($ticketBookingId))
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Unable to find booking [Ticket Booking Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund = CustomerRefunds::where('ticket_booking_id', $ticketBookingId)
			                                 ->whereNull('deleted_at')
			                                 ->whereNull('refunded_at')
			                                 ->whereNull('rejected_at')
			                                 ->first();

			if (!$customerRefund)
			{
				return response()->json([
					                        'success' => false,
					                        'error'   => 'Scheduled settlement is not in valid state to be halted [Ticket Bookimg Id: ' . $ticketBookingId . ']'
				                        ]);
			}

			$customerRefund->halted_at = Carbon::now();
			$customerRefund->halt_comments = $request['haltComments'];
			$customerRefund->updated_at = Carbon::now();
			$customerRefund->save();

			LogBookingFinance::create([
				                          'ticket_booking_id'    => $ticketBookingId,
				                          'handler_id'           => $handlerId,
				                          'type_update'          => 'Manual',
				                          'comments'             => 'Customer refund has been halted',
				                          'total_booking_amount' => $ticketBooking->booking_amount,
				                          'created_at'           => Carbon::now(),
				                          'updated_at'           => Carbon::now()
			                          ]);

			return response()->json([
				                        'success'          => true,
				                        'customerRefundId' => $customerRefund->id
			                        ]);

		} catch (\Exception $e)
		{
			$this->sendAndSaveReport($e);

			return response()->json([
				                        'success' => false,
				                        'error'   => $e->getMessage()
			                        ]);
		}
	}
}