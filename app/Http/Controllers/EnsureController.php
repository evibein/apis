<?php

namespace App\Http\Controllers;

use App\Http\Models\Planner;
use App\Http\Models\ReportBookingArchive;
use App\Http\Models\ReportBookingLive;
use App\Http\Models\TrackBookingArchive;
use App\Http\Models\TrackBookingLive;
use App\Http\Models\TypeCloudPhone;
use App\Http\Models\ReportRecordPin;

use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Queue;
use Illuminate\Support\Facades\Input;
use Laravel\Lumen\Routing\Controller;

class EnsureController extends Controller
{
	public function track()
	{
		$from = ltrim(Input::get('From'), 0); // convert 07259509827 to 7259509827
		$to = Input::get('To');
		$callTime = Input::get('StartTime');
		$response = ['success' => false];
		$handleData = [
			'from'            => $from,
			'callSid'         => Input::get('CallSid'),
			'callTime'        => $callTime,
			'cloudPhoneId'    => null,
			'vendor'          => null,
			'liveItem'        => null,
			'trackBookingIds' => null
		];

		// get phone id
		$cloudPhone = TypeCloudPhone::where('phone', $to)
		                            ->where('is_for_otp', 0)
		                            ->first();
		if (!$cloudPhone)
		{
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Could not find cloud phone with number: $to",
				'body'    => "Someone with phone number $from has tried reaching cloud phone $to, but $to is not a valid number. Please check and fix this issue."
			];

			$this->notifyAdmin($data);
			$response['error'] = 'No cloud phone found';

			return response()->json($response);
		}
		$cloudPhoneId = $cloudPhone->id;
		$handleData['cloudPhoneId'] = $cloudPhoneId;

		// get vendor
		$vendor = $this->getVendor($from);
		if ($vendor === false)
		{
			$response['error'] = "Problem with finding vendor with phone: $from (either no / multiple)";

			return response()->json($response);
		}

		$handleData['vendor'] = $vendor;
		$trackLive = TrackBookingLive::with('booking', 'booking.ticket')
		                             ->where('type_cloud_phone_id', $cloudPhoneId)
		                             ->where('vendor_phone', $from)
		                             ->where('expires_at', '>=', $callTime)
		                             ->get();

		if ($trackLive->count() == 1)
		{
			$handleData['liveItem'] = $trackLive->first();
			$this->handleTrack($handleData);
		}
		elseif ($trackLive->count() > 1)
		{
			// Multiple mappings to cloud phone - should never happen
			$handleData['trackBookingIds'] = implode(", ", $trackLive->pluck('id')->all());
			$this->handleMultiplePhoneMapping($handleData);
		}
		else
		{
			// trackLive count == 0
			// do nothing
		}

		return response()->json(['success' => true]);
	}

	public function recordPin()
	{
		// get call details and store in a temp table
		$from = ltrim(Input::get('From'), 0); // Convert 07259509827 to 7259509827
		$to = Input::get('To');
		$callTime = Input::get('StartTime');
		$callSid = Input::get('CallSid');
		$response = ['success' => false];
		$pin = (int)(trim(trim(Input::get('digits'), '"'), "'")); // remove quotes around
		$pin = str_pad($pin, 4, 0, STR_PAD_LEFT); // pad with 0 if not

		// validate cloud phone
		$cloudPhone = TypeCloudPhone::where('phone', $to)
		                            ->where('is_for_otp', 1)
		                            ->first();

		if (!$cloudPhone)
		{
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Could not find cloud phone with number: $to, came in for recording OTP",
				'body'    => "Someone with phone number $from has tried reaching cloud phone $to, but $to is not a valid number (recording OTP). Please check and fix this issue."
			];

			$this->notifyAdmin($data);
			$response['error'] = 'No cloud phone found';

			return response()->json($response);
		}

		// save it to temp table
		$recordPin = new ReportRecordPin;
		$recordPin->type_cloud_phone_id = $cloudPhone->id;
		$recordPin->pin = $pin;
		$recordPin->reporting_phone = $from;
		$recordPin->call_ref_id = $callSid;
		$recordPin->pin_entered_at = $callTime;
		$recordPin->created_at = date('Y-m-d H:i:s');
		$recordPin->updated_at = date('Y-m-d H:i:s');
		$recordPin->save();

		return response()->json(['success' => true]);
	}

	public function recordOtp()
	{
		// verify CallSid, PIN & OTP
		$from = ltrim(Input::get('From'), 0); // Convert 07259509827 to 7259509827
		$to = Input::get('To');
		$callTime = Input::get('StartTime');
		$callSid = Input::get('CallSid');
		$response = ['success' => false];
		$otp = (int)(trim(trim(Input::get('digits'), '"'), "'")); // remove quotes around
		$otp = str_pad($otp, 4, 0, STR_PAD_LEFT); // pad with 0 if not
		$now = date('Y-m-d H:i:s');
		$statusCode = 200;

		$recordPin = ReportRecordPin::where('call_ref_id', '=', $callSid)
		                            ->where('reporting_phone', $from)
		                            ->get();

		if ($recordPin->count() == 1)
		{
			$recordPin = $recordPin->first();
			$pin = $recordPin->pin;

			// check PIN & OTP from report_booking_live table
			$reportLive = ReportBookingLive::with('booking', 'booking.ticket', 'booking.ticket.area')
			                               ->where('pin', '=', $pin)
			                               ->where('otp', '=', $otp)
			                               ->where('send_otp_at', '<=', $now)
			                               ->get();

			// valid combination, notify team, vendor
			if ($reportLive->count() == 1)
			{
				$reportLive = $reportLive->first();

				$vendorReportSuccessSmsTpl = config('evibe.sms_tpl.report.vendor.success');
				$booking = $reportLive->booking;
				$vendor = $booking->vendor;
				$ticket = $booking->ticket;
				$delay = strtotime($callTime) - strtotime($booking->reports_at);
				$delay = (int)($delay / 60); // in minutes

				// notify success to vendor
				// Ex: Thank you for entering PIN & OTP for Divya's party (Indiranagar). Reported time is 7:10 PM. Team Evibe.
				$tplVars = [
					'#field1#' => $ticket->name . "'s",
					'#field2#' => $ticket->area->name,
					'#field3#' => date('h:i A', strtotime($callTime)),
				];
				$vendorReportSuccessSms = str_replace(array_keys($tplVars), array_values($tplVars), $vendorReportSuccessSmsTpl);
				$smsData = ['to' => $reportLive->vendor_phone, 'text' => $vendorReportSuccessSms];
				Queue::push('Evibe\Utility\SendSMS@send', $smsData);

				// send to reporting_phone too if not same as vendor phone
				if ($from != $reportLive->vendor_phone)
				{
					$smsData = ['to' => $from, 'text' => $vendorReportSuccessSms];
					Queue::push('Evibe\Utility\SendSMS@send', $smsData);
				}

				// email success to team
				$notifyData = [
					'customerName'  => $ticket->name,
					'partyLocation' => $ticket->area->name,
					'vendorName'    => $vendor->person,
					'reportedAt'    => $callTime,
					'delay'         => $delay,
					'reportingTime' => date('h:i A, d M', strtotime($booking->reports_at)),
					'subject'       => $vendor->person . " has reported for " . $ticket->name . " party"
				];
				$this->notifyTeamReportSuccess($notifyData);

				// move to archive table with appropriate values
				$reportingArchive = new ReportBookingArchive;
				$reportingArchive->ticket_booking_id = $reportLive->ticket_booking_id;
				$reportingArchive->type_scenario_id = $reportLive->type_scenario_id;
				$reportingArchive->type_cloud_phone_id = $reportLive->type_cloud_phone_id;
				$reportingArchive->call_ref_id = $callSid; // save this call id
				$reportingArchive->expires_at = $reportLive->expires_at;
				$reportingArchive->vendor_phone = $reportLive->vendor_phone;
				$reportingArchive->send_otp_at = $reportLive->send_otp_at;
				$reportingArchive->otp = $otp;
				$reportingArchive->pin = $pin;
				$reportingArchive->vendor_sms_text = $vendorReportSuccessSms; // set the latest
				$reportingArchive->cust_sms_text = $recordPin->cust_sms_text;
				$reportingArchive->vendor_reported_at = $callTime;
				$reportingArchive->cust_sms_sent_at = $reportLive->cust_sms_sent_at;
				$reportingArchive->reporting_phone = $from;
				$reportingArchive->delayed_by = $delay;
				$reportingArchive->pin_entered_at = $recordPin->pin_entered_at;
				$reportingArchive->created_at = date('Y-m-d H:i:s');
				$reportingArchive->updated_at = date('Y-m-d H:i:s');

				// delete PIN & live mapping
				$recordPin->forceDelete();
				$reportLive->forceDelete();
				$response['success'] = true;
			}
			else
			{
				// inform Admin
				$errorData = [
					'name'    => config('evibe.contact.admin.name'),
					'subject' => "Invalid OTP / PIN entered by $from on $to at $callTime",
					'body'    => "Someone has entered either PIN / OTP wrong with OTP: $otp, PIN: $pin. The call was from $from and to $to at $callTime. Please check the issue and fix if applicable."
				];
				$this->notifyAdmin($errorData);
				$response['error'] = 'OTP & PIN combination failed';
				$statusCode = 302;
			}
		}
		else
		{
			// error case - inform admin
			$errorData = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Could not find values in record pin by $from on $to at $callTime",
				'body'    => "Call with $callSid could not result any valid values from report_record table. OTP give in $otp, call was from $from and cloud phone $to. Please check and fix if applicable."
			];
			$this->notifyAdmin($errorData);
			$response['error'] = 'No value found for PIN';
			$statusCode = 302;
		}

		return response()->json($response, $statusCode);
	}

	public function handleTrack($handleData)
	{
		$trackLive = $handleData['liveItem'];
		$vendor = $handleData['vendor'];
		$callTime = $handleData['callTime'];
		$callSid = $handleData['callSid'];

		$booking = $trackLive->booking;
		$ticket = $booking->ticket;
		$vendorName = $vendor->person;
		$customerName = $ticket->name;
		$reportingTime = date('h:i A', strtotime($booking->reports_at));

		$handleData['reportingTime'] = $reportingTime;
		$handleData['ticket'] = $ticket;
		$handleData['booking'] = $booking;
		$handleData['scenario'] = config('evibe.scenario.track');
		$handleData['vendorName'] = $vendorName;
		$handleData['customerName'] = $customerName;
		$handleData['partyLocation'] = $ticket->area->name;

		// notify success to customer
		$customerSuccessSms = $this->notifySuccessToCustomer($handleData);

		// notify vendor (success feedback)
		$this->notifyTrackVendorSuccess($handleData);

		// email operation team (success)
		$successEmailData = [
			'name'    => config('evibe.contact.operations.name'),
			'subject' => "$customerName party is on track, reporting at $reportingTime",
			'body'    => "$vendorName has reported that $customerName party services are on track and will be reporting the venue at $reportingTime"
		];
		$this->notifyTeamTrackSuccess($successEmailData);

		// push to report booking, generate OTP, PIN
		// @see: PIN is same as vendor ID (4 digits, starting with 0s)

		// get cloud phone number available for OTP (reusing same number)
		$otpCloudPhone = TypeCloudPhone::where('is_for_otp', 1)->first();
		$pin = str_pad($vendor->id, 4, 0, STR_PAD_LEFT);
		$sendOtpAt = strtotime($booking->reports_at) - 30 * 60; // 30 minutes before reporting time
		$sendOtpAt = date('Y-m-d H:i:s', $sendOtpAt);

		// save info to send SMS before reporting time
		$reportLive = new ReportBookingLive;
		$reportLive->ticket_booking_id = $trackLive->ticket_booking_id;
		$reportLive->type_scenario_id = config('evibe.scenario.report');
		$reportLive->type_cloud_phone_id = $otpCloudPhone->id;
		$reportLive->vendor_phone = $trackLive->vendor_phone;
		$reportLive->otp = $this->generateOTP();
		$reportLive->pin = $pin;
		$reportLive->send_otp_at = $sendOtpAt;
		$reportLive->expires_at = null; // this will be set on sending SMS
		$reportLive->created_at = date('Y-m-d H:i:s');
		$reportLive->updated_at = date('Y-m-d H:i:s');
		$reportLive->save();

		// copy values to archive table and set new values
		$trackArchive = new TrackBookingArchive;
		$trackArchive->ticket_booking_id = $trackLive->ticket_booking_id;
		$trackArchive->type_scenario_id = $trackLive->type_scenario_id;
		$trackArchive->type_cloud_phone_id = $trackLive->type_cloud_phone_id;
		$trackArchive->expires_at = $trackLive->expires_at;
		$trackArchive->vendor_phone = $trackLive->vendor_phone;
		$trackArchive->vendor_sms_text = $trackLive->vendor_sms_text;
		$trackArchive->vendor_responded_at = $callTime;
		$trackArchive->call_ref_id = $callSid;
		$trackArchive->cust_sms_text = $customerSuccessSms;
		$trackArchive->cust_sms_sent_at = date('Y-m-d H:i:s');
		$trackArchive->team_sms_text = null;
		$trackArchive->team_sms_sent_at = null;
		$trackArchive->created_at = date('Y-m-d H:i:s');
		$trackArchive->updated_at = date('Y-m-d H:i:s');
		$trackArchive->save();

		// force delete this mapping
		$trackLive->forceDelete();
	}

	public function handleMultiplePhoneMapping($handleData)
	{
		// something went wrong - same cloud phone used multiple times - notify admin
		$trackBookingIds = $handleData['trackBookingIds'];

		$data = [
			'name'    => config('evibe.contact.admin.name'),
			'subject' => "Issue with cloud phones",
			'body'    => "Same cloud phone has been used multiple times for same vendor, track booking live IDs: $trackBookingIds"
		];

		$this->notifyAdmin($data);
	}

	public function getVendor($vendorPhone)
	{
		$vendor = Planner::where('phone', '=', $vendorPhone)->get();

		if ($vendor->count() == 1)
		{
			$vendor = $vendor->first();
		}
		elseif ($vendor->count() > 1)
		{
			$vendor = false;

			// notify admin - multiple vendors with same phone number exists
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => "Multiple vendors found with phone number: $vendorPhone",
				'body'    => "There was a missed call from $vendorPhone, and there are multiple vendors matching this number. Please check and fix the issue."
			];
			$this->notifyAdmin($data);
		}
		elseif ($vendor->count() == 0)
		{
			$vendor = false;

			// notify admin - someone else tried reaching our cloud phone number
			$data = [
				'name'    => config('evibe.contact.admin.name'),
				'subject' => 'Could not find vendor with phone: ' . $vendorPhone,
				'body'    => "There was a missed call from $vendorPhone, and there is no vendor matching this number. Please check and fix the issue."
			];
			$this->notifyAdmin($data);

		}
		else
		{
			// do nothing
		}

		return $vendor;
	}

	public function notifySuccessToCustomer($data)
	{
		// send SMS to customer (primary, alt number)
		$ticket = $data['ticket'];
		$vendorName = $data['vendorName'];
		$customerName = $data['customerName'];
		$customerPhone = $ticket->phone;
		$customerAltPhone = $ticket->alt_phone;
		$customerSuccessSms = null;
		$dayType = "today";

		if ($data['scenario'] == config('evibe.scenario.track'))
		{
			// determine if party day is today / tomorrow
			$tomMidnight = strtotime("tomorrow midnight");
			$partyDateTime = $data['booking']->party_date_time;
			if ($partyDateTime > $tomMidnight)
			{
				$dayType = "tomorrow";
			}

			// Hi #field1#, I'm #field2# from Evibe. We are happy to inform that all your party arrangements are on track. We will reach the venue by #field3# today. Cheers.
			// Hi Divya, I'm Kiran from Evibe. We are happy to inform that all your party arrangements are on track. We will reach the venue by 05:00 PM today. Cheers.
			$customerSuccessSmsTpl = config('evibe.sms_tpl.track.customer.success');
			$tplVars = [
				'#field1#'  => $customerName,
				'#field2#'  => $vendorName,
				'#field3#'  => $data['reportingTime'],
				'#dayType#' => $dayType
			];

			$customerSuccessSms = str_replace(array_keys($tplVars), array_values($tplVars), $customerSuccessSmsTpl);
			$smsData = ['to' => $customerPhone, 'text' => $customerSuccessSms];
			Queue::push('Evibe\Utility\SendSMS@send', $smsData);

			// notify customer alt phone number
			if ($customerAltPhone)
			{
				$smsData = ['to' => $customerAltPhone, 'text' => $customerSuccessSms];
				Queue::push('Evibe\Utility\SendSMS@send', $smsData);
			}
		}
		else
		{
			// do nothing
		}

		return $customerSuccessSms;
	}

	public function notifyAdmin($data)
	{
		Mail::send('emails.report.admin.error', $data, function ($message) use ($data)
		{
			$sub = array_key_exists('subject', $data) ? $data['subject'] : "An API error occurred at " . date('d-m-Y H:i');

			$message->from('ping@evibe.in', 'Api Error');
			$message->to(config('evibe.contact.admin.email'), config('evibe.contact.admin.name'));
			$message->subject($sub);
		});
	}

	public function notifyTeamTrackSuccess($data)
	{
		Mail::send('emails.track.team.success', $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', 'Track Booking');
			$message->to(config('evibe.contact.operations.email'), config('evibe.contact.operations.name'));
			$message->subject($data['subject']);
		});
	}

	public function notifyTeamReportSuccess($data)
	{
		$data['name'] = config('evibe.contact.operations.name');
		Mail::send('emails.report.team.success', $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', 'Report Booking');
			$message->to(config('evibe.contact.operations.email'), config('evibe.contact.operations.name'));
			$message->subject($data['subject']);
		});
	}

	public function notifyTrackVendorSuccess($data)
	{
		// success template is same for both track
		// Hi #field1#, we have informed your reporting time to #field2# (#field3#). Best wishes for your event. Team Evibe.
		$vendorSuccessSMSTpl = config('evibe.sms_tpl.track.vendor.success');
		$tplVars = [
			'#field1#' => $data['vendorName'],
			'#field2#' => $data['customerName'],
			'#field3#' => $data['partyLocation'],
		];
		$vendorSuccessSMSText = str_replace(array_keys($tplVars), array_values($tplVars), $vendorSuccessSMSTpl);
		$smsData = [
			'to'   => $data['from'],
			'text' => $vendorSuccessSMSText
		];
		Queue::push('Evibe\Utility\SendSMS@send', $smsData);
	}

	public function generateOTP()
	{
		// ensure OTP is unique
		$existingOTPs = ReportBookingLive::pluck('otp')->toArray();
		do
		{
			$otp = rand(1000, 9999);
		} while (in_array($otp, $existingOTPs));

		return (string)$otp;
	}
}