<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Notification\PaymentNotificationController;
use App\Http\Models\Package;
use App\Http\Models\TicketBooking;
use App\Http\Models\Util\OptionAvailability;
use Carbon\Carbon;
use Evibe\Utility\TicketStatus;
use Evibe\Utility\TypeTicketUtility;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;

class ProductController extends PaymentNotificationController
{
	public function handleAvailabilityCheck()
	{
		$res = ['success' => true];

		$productId = Input::get('optionId');
		$productTypeId = Input::get('optionTypeId');
		$partyDate = Input::get('partyDate');
		$partyTime = Input::get('partyTime');
		$partyTime = $partyTime ? date('H:i', strtotime($partyTime)) : null;

		if (!$partyDate)
		{
			Log::error("Party date is missing. Unable to check availability.");

			return response()->json(['success' => true, 'error' => 'Party Date is missing']);
		}
		elseif (!$partyTime)
		{
			Log::error("Party time is missing. Unable to check availability.");

			return response()->json(['success' => true, 'error' => 'Party Time is missing']);
		}
		elseif (!$productId)
		{
			Log::error("Product id is missing. Unable to check availability.");

			return response()->json(['success' => true, 'error' => 'Product Id is missing']);
		}
		elseif (!$productTypeId)
		{
			Log::error("Product type id is missing. Unable to check availability.");

			return response()->json(['success' => true, 'error' => 'Product Type Id is missing']);
		}

		// disabling surprise decors for 14 Feb 2020
		// @todo: remove after 14 Feb 2021
		// if (($productTypeId == config('evibe.ticket.type.couple-experiences')) && ($partyDate == "2021/02/14"))
		// {
		// 	$product = Package::find($productId);
		// 	if ($product && ($product->type_ticket_id == config('evibe.ticket.type.couple-experiences')) && ($product->map_type_id == config('evibe.ticket.type.venues')))
		// 	{
		// 		return response()->json(['success' => false]);
		// 	}
		// }

		$bookingSlot = OptionAvailability::where('map_id', $productId)
		                                 ->where('map_type_id', $productTypeId)
		                                 ->where('slot_start_time', '<=', $partyTime)
		                                 ->where('slot_end_time', '>', $partyTime)
		                                 ->first();

		if (!$bookingSlot)
		{
			return response()->json($res);
		}

		$bookingsPossible = $bookingSlot->bookings_available;
		$slotStartTime = $bookingSlot->slot_start_time;
		$slotEndTime = $bookingSlot->slot_end_time;

		if (!$bookingsPossible)
		{
			// if the availability count is null or 0, but the slot exists
			// consider this as unavailability
			return response()->json(['success' => false]);
		}

		$todayStartTime = Carbon::createFromTimestamp(time())->startOfDay()->toDateTimeString();
		$todayStartTimeEpoch = strtotime($todayStartTime);
		$partyStartTimeEpoch = strtotime($partyTime);
		$partyDateEpoch = strtotime($partyDate);
		$partyStartDateTimeEpoch = $partyDateEpoch + ($partyStartTimeEpoch - $todayStartTimeEpoch);

		$slotStartTimeEpoch = strtotime($slotStartTime);
		$slotStartDateTimeEpoch = $partyDateEpoch + ($slotStartTimeEpoch - $todayStartTimeEpoch);

		$slotEndTimeEpoch = strtotime($slotEndTime);
		$slotEndDateTimeEpoch = $partyDateEpoch + ($slotEndTimeEpoch - $todayStartTimeEpoch);

		$bookings = null;
		switch ($productTypeId)
		{
			case TypeTicketUtility::COUPLE_EXPERIENCE:
				$bookings = TicketBooking::select("ticket_bookings.*")
				                         ->join("ticket", "ticket.id", "=", "ticket_bookings.ticket_id")
				                         ->join("ticket_mapping", "ticket_mapping.id", "=", "ticket_bookings.ticket_mapping_id")
				                         ->where("ticket.status_id", TicketStatus::BOOKED)
				                         ->where("ticket_bookings.is_advance_paid", 1)
				                         ->where("ticket_mapping.map_id", $productId)
				                         ->where("ticket_mapping.map_type_id", $productTypeId)
				                         ->whereBetween('ticket_bookings.party_date_time', [$slotStartDateTimeEpoch, $slotEndDateTimeEpoch])
				                         ->whereNull("ticket_mapping.deleted_at")
				                         ->whereNull("ticket_bookings.deleted_at")
				                         ->whereNull("ticket_bookings.cancelled_at")
				                         ->whereNull("ticket.deleted_at")
				                         ->get();
				break;
		}

		if ($bookings && ($bookingsPossible <= $bookings->count()))
		{
			return response()->json(['success' => false]);
		}
		else
		{
			return response()->json($res);
		}
	}
}