<?php

namespace App\Http\Controllers;

use App\Http\Models\Cake;
use App\Http\Models\CakeGallery;
use App\Http\Models\City;
use App\Http\Models\Decor;
use App\Http\Models\DecorGallery;
use App\Http\Models\Package;
use App\Http\Models\PackageGallery;
use App\Http\Models\ServiceGallery;
use App\Http\Models\TrendGallery;
use App\Http\Models\Trends;
use App\Http\Models\TypeEvent;
use App\Http\Models\TypeService;
use App\Http\Models\TypeTicket;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class ImageController extends BaseController
{
	public function processInputFromWaterMarkConsole(Request $request)
	{
		$categoryId = $request['categoryId'];
		$isDASH = $request['isDASH'] ? $request['isDASH'] : 0;

		$imageData = [];

		$categories = TypeTicket::where('is_page', 1)
		                        ->whereNull('deleted_at')
		                        ->pluck('name', 'id')
		                        ->toArray();

		$cities = City::where('is_active', 1)
		              ->whereNull('deleted_at')
		              ->pluck('name', 'id')
		              ->toArray();

		$occasions = TypeEvent::whereNull('deleted_at')
		                      ->where('show_customer', 1)
		                      ->pluck('name', 'id')
		                      ->toArray();

		if (!is_null($categoryId) &&
			$categoryId > 0 &&
			isset($categories[$categoryId]) &&
			!is_null($categories[$categoryId]))
		{
			switch ($categoryId)
			{
				case config('evibe.ticket.type.cakes'):
					Cake::select('cake.id', 'cake.title', 'cake.code')
					    ->chunk(50, function ($cakes) use (&$imageData, $isDASH) {
						    $currentImagesData = [];
						    foreach ($cakes as $cake)
						    {
							    $images = $cake->gallery;
							    foreach ($images as $image)
							    {
								    if (is_null($image->deleted_at))
								    {
									    $imageRootBase = config('evibe.root.gallery_queue') . '/cakes/' . $image->cake_id . '/';
									    $dataArr = [
										    //[
										    //   "name" => $image->url,
										    //   "url"  => $image->getLink(),
										    //   "root" => $imageRootBase . $image->url
										    //],
										    [
											    "name" => $image->url,
											    "url"  => $image->getLink("profile"),
											    "root" => $imageRootBase . 'profile/' . $image->url
										    ],
										    [
											    "name" => $image->url,
											    "url"  => $image->getLink("results"),
											    "root" => $imageRootBase . 'results/' . $image->url
										    ],
									    ];

									    $currentImagesData[$cake->id][$image->id] = $dataArr;
									    $imageData['urls'][$cake->id][$image->id] = $dataArr;
								    }
							    }
						    }

						    if (count($currentImagesData))
						    {
							    if ($isDASH == 0)
							    {
								    $this->dispatch(new \App\Jobs\Util\ApplyImageWatermark($currentImagesData));
							    }

							    $imageData['urls'] = $currentImagesData;
							    if ($isDASH == 1)
							    {
								    $imageData['categoryDetails'] = Cake::select('id', 'title', 'code')->whereNull('deleted_at')->get()->toArray();
							    }
						    }
					    });
					break;

				case config('evibe.ticket.type.decors'):
					Decor::select('decor.id', 'decor.name', 'decor.code')
					     ->chunk(50, function ($decors) use (&$imageData, $isDASH) {
						     $currentImagesData = [];
						     foreach ($decors as $decor)
						     {
							     $images = $decor->gallery;
							     if ($images)
							     {
								     foreach ($images as $image)
								     {
									     if (is_null($image->deleted_at))
									     {
										     $imageRootBase = config('evibe.root.gallery_queue') . '/decors/' . $image->decor_id . '/images/';
										     $dataArr = [
											     //[
											     //   "name" => $image->url,
											     //   "url"  => $image->getLink(),
											     //   "root" => $imageRootBase . $image->url
											     //],
											     [
												     "name" => $image->url,
												     "url"  => $image->getLink("results"),
												     "root" => $imageRootBase . 'results/' . $image->url
											     ],
											     [
												     "name" => $image->url,
												     "url"  => $image->getLink("profile"),
												     "root" => $imageRootBase . 'profile/' . $image->url
											     ],
										     ];

										     $currentImagesData[$decor->id][$image->id] = $dataArr;
										     $imageData['urls'][$decor->id][$image->id] = $dataArr;
									     }
								     }
							     }
						     }

						     if (count($currentImagesData))
						     {
							     if ($isDASH == 0)
							     {
								     $this->dispatch(new \App\Jobs\Util\ApplyImageWatermark($currentImagesData));
							     }
							     $imageData['urls'] = $currentImagesData;
							     if ($isDASH == 1)
							     {
								     $imageData['categoryDetails'] = Decor::select('id', 'name', 'code')->whereNull('deleted_at')->get()->toArray();
							     }
						     }
					     });
					break;

				case config('evibe.ticket.type.packages'):
					Package::select('planner_package.id', 'planner_package.name', 'planner_package.code', 'planner_package.map_id', 'planner_package.map_type_id')
					       ->with('gallery')
					       ->whereIn('map_type_id', [2, 3])
					       ->chunk(20, function ($packages) use (&$imageData, $isDASH) {
						       $currentImagesData = [];
						       foreach ($packages as $package)
						       {
							       $images = $package->gallery;
							       foreach ($images as $image)
							       {
								       if ($image->type != config('evibe.gallery.type.image'))
								       {
									       continue;
								       }

								       $providerType = "planner";
								       if ($package->map_type_id == config('evibe.ticket.type.venues'))
								       {
									       $providerType = "venues";
								       }

								       $url = config('evibe.hosts.gallery') . "/$providerType/" . $package->map_id . '/images/packages/' . $package->id . '/';
								       $imageRootBase = config('evibe.root.gallery_queue') . '/' . $providerType . '/' . $package->map_id . '/images/packages/' . $package->id . '/';
								       $dataArr = [
									       //[
									       //   "name" => $image->url,
									       //   "url"  => $url . $image->url,
									       //   "root" => $imageRootBase . $image->url
									       //],
									       [
										       "name" => $image->url,
										       "url"  => $url . 'results/' . $image->url,
										       "root" => $imageRootBase . 'results/' . $image->url
									       ],
									       [
										       "name" => $image->url,
										       "url"  => $url . 'profile/' . $image->url,
										       "root" => $imageRootBase . 'profile/' . $image->url
									       ],
								       ];

								       $currentImagesData[$package->id][$image->id] = $dataArr;
								       $imageData['urls'][$package->id][$image->id] = $dataArr;
							       }
						       }

						       if (count($currentImagesData))
						       {
							       if ($isDASH == 0)
							       {
								       $this->dispatch(new \App\Jobs\Util\ApplyImageWatermark($currentImagesData));
							       }
							       $imageData['urls'] = $currentImagesData;
							       if ($isDASH == 1)
							       {
								       $imageData['categoryDetails'] = Package::select('id', 'name', 'code')->whereNull('deleted_at')->get()->toArray();
							       }
						       }
					       });
					break;

				case config('evibe.ticket.type.entertainments'):
					TypeService::select('type_service.id', 'type_service.name', 'type_service.code')
					           ->chunk(50, function ($services) use (&$imageData, $isDASH) {
						           $currentImagesData = [];
						           foreach ($services as $service)
						           {
							           $images = $service->gallery;
							           if ($images)
							           {
								           foreach ($images as $image)
								           {
									           if (is_null($image->deleted_at))
									           {
										           $imageRootBase = config('evibe.root.gallery_queue') . '/services/' . $service->id . "/images/";
										           $dataArr = [
											           //[
											           //   "name" => $image->url,
											           //   "url"  => $image->getLink(),
											           //   "root" => $imageRootBase . $image->url
											           //],
											           [
												           "name" => $image->url,
												           "url"  => $image->getLink("results"),
												           "root" => $imageRootBase . "results/" . $image->url
											           ],
											           [
												           "name" => $image->url,
												           "url"  => $image->getLink("profile"),
												           "root" => $imageRootBase . "profile/" . $image->url
											           ]
										           ];

										           $currentImagesData[$service->id][$image->id] = $dataArr;
										           $imageData['urls'][$service->id][$image->id] = $dataArr;
									           }
								           }
							           }
						           }

						           if (count($currentImagesData))
						           {
							           if ($isDASH == 0)
							           {
								           $this->dispatch(new \App\Jobs\Util\ApplyImageWatermark($currentImagesData));
							           }
							           $imageData['urls'] = $currentImagesData;
							           if ($isDASH == 1)
							           {
								           $imageData['categoryDetails'] = TypeService::select('id', 'name', 'code')->whereNull('deleted_at')->get()->toArray();
							           }
						           }
					           });
					break;

				case config('evibe.ticket.type.trends'):
					Trends::select('trending.id', 'trending.name', 'trending.code')
					      ->chunk(50, function ($trends) use (&$imageData, $isDASH) {
						      $currentImagesData = [];
						      foreach ($trends as $trend)
						      {
							      $images = $trend->gallery;
							      if ($images)
							      {
								      foreach ($images as $image)
								      {
									      if (is_null($image->deleted_at))
									      {
										      $imageRootBase = config('evibe.root.gallery_queue') . '/trends/' . $trend->id . "/images/";
										      $dataArr = [
											      //[
											      //   "name" => $image->url,
											      //   "url"  => $image->getLink("results"),
											      //   "root" => $imageRootBase . "results/" . $image->url
											      //],
											      //[
											      //   "name" => $image->url,
											      //   "url"  => $image->getLink("profile"),
											      //   "root" => $imageRootBase . "profile/" . $image->url
											      //],
											      [
												      "name" => $image->url,
												      "url"  => $image->getLink(),
												      "root" => $imageRootBase . $image->url
											      ]
										      ];

										      $currentImagesData[$trend->id][$image->id] = $dataArr;
										      $imageData['urls'][$trend->id][$image->id] = $dataArr;
									      }
								      }
							      }
						      }

						      if (count($currentImagesData))
						      {
							      if ($isDASH == 0)
							      {
								      $this->dispatch(new \App\Jobs\Util\ApplyImageWatermark($currentImagesData));
							      }
							      $imageData['urls'] = $currentImagesData;
							      if ($isDASH == 1)
							      {
								      $imageData['categoryDetails'] = Trends::select('id', 'name', 'code')->whereNull('deleted_at')->get()->toArray();
							      }
						      }
					      });
					break;
			}
		}

		return response()->json([
			                        "success" => true,
			                        "data"    => [
				                        "categories" => $categories,
				                        "cities"     => $cities,
				                        "occasions"  => $occasions,
				                        "imageData"  => $imageData
			                        ]
		                        ]);
	}

	public function removeWaterMarkConsole(Request $request)
	{
		$categoryId = $request['categoryId'];

		$imageData = [];

		$categories = TypeTicket::where('is_page', 1)
		                        ->whereNull('deleted_at')
		                        ->pluck('name', 'id')
		                        ->toArray();

		if (!is_null($categoryId) && $categoryId > 0 && isset($categories[$categoryId]) && !is_null($categories[$categoryId]))
		{
			switch ($categoryId)
			{
				case config('evibe.ticket.type.packages'):
					Package::select('planner_package.id', 'planner_package.name', 'planner_package.code', 'planner_package.map_id', 'planner_package.map_type_id')
					       ->with('gallery')
					       ->whereIn('map_type_id', [2, 3])
					       ->chunk(20, function ($packages) use (&$imageData) {
						       $currentImagesData = [];
						       foreach ($packages as $package)
						       {
							       $images = $package->gallery;
							       foreach ($images as $image)
							       {
								       if (is_null($image->deleted_at))
								       {
									       if ($image->type != config('evibe.gallery.type.image'))
									       {
										       continue;
									       }

									       $providerType = "planner";
									       if ($package->map_type_id == config('evibe.ticket.type.venues'))
									       {
										       $providerType = "venues";
									       }

									       $url = config('evibe.hosts.gallery') . "/$providerType/" . $package->map_id . '/images/packages/' . $package->id . '/';
									       $imageRootBase = config('evibe.root.gallery_queue') . '/' . $providerType . '/' . $package->map_id . '/images/packages/' . $package->id . '/';

									       $dataArr = [
										       //[
										       //   "name" => $image->url,
										       //   "url"  => $image->getLink(),
										       //   "root" => $imageRootBase . $image->url
										       //],
										       [
											       "name" => $image->url,
											       "url"  => $url . 'profile/' . $image->url,
											       "root" => $imageRootBase . 'profile/' . $image->url
										       ],
										       [
											       "name" => $image->url,
											       "url"  => $url . 'results/' . $image->url,
											       "root" => $imageRootBase . 'results/' . $image->url
										       ],
									       ];

									       $currentImagesData[$package->id][$image->id] = $dataArr;
									       $imageData['urls'][$package->id][$image->id] = $dataArr;
								       }
							       }
						       }

						       if (count($currentImagesData))
						       {
							       $this->dispatch(new \App\Jobs\Util\RemoveImageWatermark($currentImagesData));
						       }
					       });
					break;

				case config('evibe.ticket.type.cakes'):
					Cake::select('cake.id', 'cake.title', 'cake.code')
					    ->chunk(50, function ($cakes) use (&$imageData) {
						    $currentImagesData = [];
						    foreach ($cakes as $cake)
						    {
							    $images = $cake->gallery;
							    foreach ($images as $image)
							    {
								    if (is_null($image->deleted_at))
								    {
									    $imageRootBase = config('evibe.root.gallery_queue') . '/cakes/' . $image->cake_id . '/';
									    $dataArr = [
										    //[
										    //   "name" => $image->url,
										    //   "url"  => $image->getLink(),
										    //   "root" => $imageRootBase . $image->url
										    //],
										    [
											    "name" => $image->url,
											    "url"  => $image->getLink("profile"),
											    "root" => $imageRootBase . 'profile/' . $image->url
										    ],
										    [
											    "name" => $image->url,
											    "url"  => $image->getLink("results"),
											    "root" => $imageRootBase . 'results/' . $image->url
										    ],
									    ];

									    $currentImagesData[$cake->id][$image->id] = $dataArr;
									    $imageData['urls'][$cake->id][$image->id] = $dataArr;
								    }
							    }
						    }

						    if (count($currentImagesData))
						    {
							    $this->dispatch(new \App\Jobs\Util\RemoveImageWatermark($currentImagesData));
						    }
					    });
					break;

				case config('evibe.ticket.type.decors'):
					Decor::select('decor.id', 'decor.name', 'decor.code')
					     ->chunk(50, function ($decors) use (&$imageData) {
						     $currentImagesData = [];
						     foreach ($decors as $decor)
						     {
							     $images = $decor->gallery;
							     if ($images)
							     {
								     foreach ($images as $image)
								     {
									     if (is_null($image->deleted_at))
									     {
										     $imageRootBase = config('evibe.root.gallery_queue') . '/decors/' . $image->decor_id . '/images/';
										     $dataArr = [
											     //[
											     //   "name" => $image->url,
											     //   "url"  => $image->getLink(),
											     //   "root" => $imageRootBase . $image->url
											     //],
											     [
												     "name" => $image->url,
												     "url"  => $image->getLink("results"),
												     "root" => $imageRootBase . 'results/' . $image->url
											     ],
											     [
												     "name" => $image->url,
												     "url"  => $image->getLink("profile"),
												     "root" => $imageRootBase . 'profile/' . $image->url
											     ],
										     ];

										     $currentImagesData[$decor->id][$image->id] = $dataArr;
										     $imageData['urls'][$decor->id][$image->id] = $dataArr;
									     }
								     }
							     }
						     }

						     if (count($currentImagesData))
						     {
							     $this->dispatch(new \App\Jobs\Util\RemoveImageWatermark($currentImagesData));
						     }
					     });
					break;

				case config('evibe.ticket.type.entertainments'):
					TypeService::select('type_service.id', 'type_service.name', 'type_service.code')
					           ->chunk(50, function ($services) use (&$imageData) {
						           $currentImagesData = [];
						           foreach ($services as $service)
						           {
							           $images = $service->gallery;
							           if ($images)
							           {
								           foreach ($images as $image)
								           {
									           if (is_null($image->deleted_at))
									           {
										           $imageRootBase = config('evibe.root.gallery_queue') . '/services/' . $service->id . "/images/";
										           $dataArr = [
											           //[
											           //   "name" => $image->url,
											           //   "url"  => $image->getLink(),
											           //   "root" => $imageRootBase . $image->url
											           //],
											           [
												           "name" => $image->url,
												           "url"  => $image->getLink("results"),
												           "root" => $imageRootBase . "results/" . $image->url
											           ],
											           [
												           "name" => $image->url,
												           "url"  => $image->getLink("profile"),
												           "root" => $imageRootBase . "profile/" . $image->url
											           ]
										           ];

										           $currentImagesData[$service->id][$image->id] = $dataArr;
										           $imageData['urls'][$service->id][$image->id] = $dataArr;
									           }
								           }
							           }
						           }

						           if (count($currentImagesData))
						           {
							           $this->dispatch(new \App\Jobs\Util\RemoveImageWatermark($currentImagesData));
						           }
					           });
					break;

				case config('evibe.ticket.type.trends'):
					Trends::select('trending.id', 'trending.name', 'trending.code')
					      ->chunk(50, function ($trends) use (&$imageData) {
						      $currentImagesData = [];
						      foreach ($trends as $trend)
						      {
							      $images = $trend->gallery;
							      if ($images)
							      {
								      foreach ($images as $image)
								      {
									      if (is_null($image->deleted_at))
									      {
										      $imageRootBase = config('evibe.root.gallery_queue') . '/trends/' . $trend->id . "/images/";
										      $dataArr = [
											      [
												      "name" => $image->url,
												      "url"  => $image->getLink("results"),
												      "root" => $imageRootBase . "results/" . $image->url
											      ],
											      [
												      "name" => $image->url,
												      "url"  => $image->getLink("profile"),
												      "root" => $imageRootBase . "profile/" . $image->url
											      ],
											      [
												      "name" => $image->url,
												      "url"  => $image->getLink(),
												      "root" => $imageRootBase . $image->url
											      ]
										      ];

										      $currentImagesData[$trend->id][$image->id] = $dataArr;
										      $imageData['urls'][$trend->id][$image->id] = $dataArr;
									      }
								      }
							      }
						      }

						      if (count($currentImagesData))
						      {
							      $this->dispatch(new \App\Jobs\Util\RemoveImageWatermark($currentImagesData));
						      }
					      });
					break;
			}
		}

		return response()->json(["success" => true]);
	}

	public function removeWaterMarkOnSelectedImages(Request $request)
	{
		$picIds = $request['picIds'];

		$originalFile = null;
		$temporaryFileName = null;

		foreach ($picIds as $picId)
		{
			$parentDirs = $picId[0];
			$temporaryFileName = $parentDirs;

			if (!is_null($parentDirs) && $parentDirs && file_exists($parentDirs))
			{
				$pathInfo = pathinfo($parentDirs);
				$fileName = $pathInfo["filename"];
				$extension = $pathInfo["extension"];
				$directory = $pathInfo["dirname"];

				$originalFile = $directory . '/' . $fileName . '_original.' . $extension;
				if (!is_null($originalFile) && $originalFile && file_exists($originalFile))
				{
					unlink($parentDirs);
					$copiedResult = copy($originalFile, $temporaryFileName);
					if ($copiedResult)
					{
						chmod($parentDirs, 0777);
						unlink($originalFile);
					}
				}
			}
		}

		return response()->json([
			                        "success" => true,
			                        "message" => "Has been deleted successfully"
		                        ]);
	}

	public function deleteDefunctImages(Request $request)
	{
		$categoryId = $request['categoryId'];
		$defunctImagesBank = [];
		$defunctProductsBank = [];

		switch ($categoryId)
		{
			// 13
			case config('evibe.ticket.type.decors'):
				$products = Decor::withTrashed()
				                 ->whereNotNull('deleted_at')
				                 ->orderBy('id')
				                 ->get();

				foreach ($products as $product)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/decors/' . $product->id . '/';
					array_push($defunctProductsBank, $imageRootBase);
				}

				$defunctImages = DecorGallery::withTrashed()
				                             ->whereNotNull('deleted_at')
				                             ->orderBy('decor_id')
				                             ->get();

				foreach ($defunctImages as $image)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/decors/' . $image->decor_id . '/images/';
					$this->pushToProductImagesBank($defunctImagesBank, $imageRootBase, $image->url);
				}
				break;

			// 6
			case config('evibe.ticket.type.cakes'):
				$products = Cake::withTrashed()
				                ->whereNotNull('deleted_at')
				                ->get();

				foreach ($products as $product)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/cakes/' . $product->id . '/';
					array_push($defunctProductsBank, $imageRootBase);
				}

				$defunctImages = CakeGallery::withTrashed()
				                            ->whereNotNull('deleted_at')
				                            ->get();

				foreach ($defunctImages as $image)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/cakes/' . $image->cake_id . '/images/';
					$this->pushToProductImagesBank($defunctImagesBank, $imageRootBase, $image->url);
				}
				break;

			// 5
			case config('evibe.ticket.type.trends'):
				$products = Trends::withTrashed()
				                  ->whereNotNull('deleted_at')
				                  ->get();

				foreach ($products as $product)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/trends/' . $product->id . '/';
					array_push($defunctProductsBank, $imageRootBase);
				}

				$defunctImages = TrendGallery::withTrashed()
				                             ->whereNotNull('deleted_at')
				                             ->get();

				foreach ($defunctImages as $image)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/trends/' . $image->trending_id . "/images/";
					$this->pushToProductImagesBank($defunctImagesBank, $imageRootBase, $image->url);
				}
				break;

			// 14
			case config('evibe.ticket.type.entertainments'):
				$products = TypeService::withTrashed()
				                       ->whereNotNull('deleted_at')
				                       ->get();

				foreach ($products as $product)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/services/' . $product->id . '/';
					array_push($defunctProductsBank, $imageRootBase);
				}

				$defunctImages = ServiceGallery::withTrashed()
				                               ->whereNotNull('deleted_at')
				                               ->get();

				foreach ($defunctImages as $image)
				{
					$imageRootBase = config('evibe.root.gallery_queue') . '/services/' . $image->type_service_id . "/images/";
					$this->pushToProductImagesBank($defunctImagesBank, $imageRootBase, $image->url);
				}
				break;

			// 1
			case config('evibe.ticket.type.packages'):
				$products = Package::withTrashed()
				                   ->whereNotNull('deleted_at')
				                   ->get();

				foreach ($products as $product)
				{
					$providerType = ($product->map_type_id == config('evibe.ticket.type.venues') ? "venues" : "planner");
					$imageRootBase = config('evibe.root.gallery_queue') . '/' . $providerType . '/' . $product->map_id . '/images/packages/' . $product->id . '/';
					array_push($defunctProductsBank, $imageRootBase);
				}

				$defunctImages = PackageGallery::withTrashed()
				                               ->with('package')
				                               ->whereNotNull('deleted_at')
				                               ->get();

				foreach ($defunctImages as $image)
				{
					$product = $image->package;
					if ($product)
					{
						$providerType = ($product->map_type_id == config('evibe.ticket.type.venues') ? "venues" : "planner");
						$imageRootBase = config('evibe.root.gallery_queue') . '/' . $providerType . '/' . $product->map_id . '/images/packages/' . $product->id . '/';

						$this->pushToProductImagesBank($defunctImagesBank, $imageRootBase, $image->url);
					}
				}
				break;
		}

		// delete defunct images
		$defunctImagesBank = collect($defunctImagesBank);
		foreach ($defunctImagesBank->chunk(50) as $chunk)
		{
			$this->dispatch(new \App\Jobs\Util\DeleteDefunctImage($chunk));
		}

		// delete defunct products
		$defunctProductsBank = collect($defunctProductsBank);
		foreach ($defunctProductsBank->chunk(50) as $chunk)
		{
			$this->dispatch(new \App\Jobs\Util\DeleteDefunctProduct($chunk));
		}

		return response()->json([
			                        "success" => true,
			                        "data"    => [
				                        "defunctProducts" => $defunctProductsBank->toArray(),
				                        "defunctImages"   => $defunctImagesBank->toArray()
			                        ]
		                        ]);
	}

	private function pushToProductImagesBank(&$defunctImagesBank, $imageRootBase, $imageUrl)
	{
		array_push($defunctImagesBank, $imageRootBase . $imageUrl);
		array_push($defunctImagesBank, $imageRootBase . "profile/" . $imageUrl);
		array_push($defunctImagesBank, $imageRootBase . "results/" . $imageUrl);
	}
}
