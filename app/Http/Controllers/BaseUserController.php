<?php namespace App\Http\Controllers;

use App\Jobs\Sms\SMSOtp;
use Evibe\HttpClient\ApiClient;
use Evibe\HttpClient\ClientError;
use Evibe\HttpClient\ServerError;

use App\Http\Models\AccessToken;
use App\Http\Models\User;
use App\Exceptions\CustomException;

use Illuminate\Http\Request;

class BaseUserController extends BaseController
{
	function __construct()
	{
		$config = [
			"baseUrl" => env('DASH_API_BASE_URL')
		];

		/**
		 * ApiClient to be removed and implement all the methods here
		 *
		 * @author Anji
		 * @since  23 May 2016
		 */
		//ApiClient::instance()->configure($config);
	}

	protected function sendOtp($otp, $phoneNumber)
	{
		$tpl = config('evibe.sms_tpl.invites.otp');
		$replaces = ['#field1#' => $otp];
		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tpl);
		$data = ['to' => $phoneNumber, 'text' => $smsText];

		$this->dispatch(new SMSOtp($data));
	}

	protected function _getUserDetails(Request $request, $usrId, $isPro = false)
	{
		$actualUserId = $this->checkForAccessToken($request);

		if ($actualUserId != $usrId)
		{
			throw new CustomException("Unauthorised. You don't have access to this profile info.", 401);
		}

		$user = User::find($usrId);

		$userDetails = $user->toArrayCamel();
		$userDetails['profilePic'] = env('GALLERY_BASE_URL') . "user/" . $usrId . "/" . $userDetails['imgUrl'];

		if ($isPro)
		{
			if (!$planner = $user->planner()->first())
			{
				throw new CustomException("No planner with the associated user id.", 401);
			}

			$phoneNumber = $planner->phone;
			$userDetails['phoneNumber'] = $phoneNumber;
		}

		return $userDetails;
	}

	protected function _updateProfilePic(Request $request, $usrId)
	{
		$actualUserId = $this->checkForAccessToken($request);

		if ($actualUserId != $usrId)
		{
			throw new CustomException("Unauthorised. You don't have access to this profile.", 401);
		}

		$this->validatePayload($request, [
			"profilepic" => "required|mimes:png,jpg,jpeg,bmp,pdf"
		]);

		$pic = $request->file('profilepic');
		$target = storage_path() . '/app/profile_pic/';
		$filename = $usrId . "." . $pic->getClientOriginalExtension();
		$pic->move($target, $filename);

		try
		{
			$formData = [
				"type"          => "profile",
				"typeId"        => $usrId,
				"attachments[]" => '@' . $target . $filename
			];

			$attachments = ApiClient::instance()->call('upload-attachments', 'POST_FORM_DATA', $formData);

			$newFileName = $attachments['attachments'][0]['newFileName'];
		} catch (ClientError $e)
		{
			throw new CustomException($e->getMessage(), 400);
		} catch (ServerError $e)
		{
			throw new CustomException($e->getMessage(), 500);
		}

		User::where('id', '=', $usrId)->update(['img_url' => $newFileName]);

		return response()->json(['success' => true]);
	}

	protected function issueAccessToken($user, $client)
	{
		$accessToken = new AccessToken;
		$accessToken->access_token = $this->getNewAccessToken();
		$accessToken->auth_client_id = $client->auth_client_id;
		$accessToken->user_id = $user->id;

		$accessToken->save();

		return $accessToken->access_token;
	}

	private function getNewAccessToken($len = 40)
	{
		$stripped = '';
		do
		{
			$bytes = openssl_random_pseudo_bytes($len, $strong);

			// We want to stop execution if the key fails because, well, that is bad.
			if ($bytes === false || $strong === false)
			{
				// @codeCoverageIgnoreStart
				throw new \Exception('Error Generating Key');
				// @codeCoverageIgnoreEnd
			}
			$stripped .= str_replace(['/', '+', '='], '', base64_encode($bytes));
		} while (strlen($stripped) < $len);

		return substr($stripped, 0, $len);
	}
}