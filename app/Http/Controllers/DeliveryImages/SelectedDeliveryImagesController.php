<?php

namespace App\Http\Controllers\DeliveryImages;

use App\Http\Controllers\BaseController;
use App\Http\Models\Decor\DecorTags;
use App\Http\Models\Util\CakeTags;
use App\Http\Models\SelectedDeliveryImages;
use Illuminate\Http\Request;

class SelectedDeliveryImagesController extends BaseController
{
	public function getSelectedDeliveryImages(Request $request)
	{
		$this->checkForAccessToken($request);
		$mapId = $request->input('mapId');
		$mapTypeId = $request->input('mapTypeId');

		if ($mapId && $mapTypeId && $mapId > 0 && $mapTypeId > 0)
		{
			$productPrice = $request->input('productPrice') ? (int)$request->input('productPrice') : 0;

			if ($productPrice <= 0)
			{
				$minPrice = 0;
				$maxPrice = 100000;
			}
			else
			{
				$minPrice = $productPrice - 1000 <= 0 ? 0 : $productPrice - 1000;
				$maxPrice = $productPrice + 1000;
			}

			$tags = [];
			switch ($mapTypeId)
			{
				case config('evibe.ticket.type.decors'):
					$tags = DecorTags::where('decor_id', '=', $mapId)
					                 ->get()
					                 ->pluck('tag_id')
					                 ->toArray();
					break;

				case config('evibe.ticket.type.cakes'):
					$tags = CakeTags::where('cake_id', '=', $mapId)
					                ->get()
					                ->pluck('tag_id')
					                ->toArray();
					break;
			}

			$deliveryImageUrls = SelectedDeliveryImages::select('id as image_id', 'image_url')
			                                           ->where(function ($query) use ($tags, $minPrice, $maxPrice) {
				                                           $query->whereIn('sub_category', $tags)
				                                                 ->whereBetween('booking_price', [$minPrice, $maxPrice]);
			                                           })
			                                           ->orWhere(function ($query) use ($mapId, $mapTypeId) {
				                                           $query->where('map_type_id', '=', $mapTypeId)
				                                                 ->where('map_id', '=', $mapId);
			                                           })
			                                           ->where('is_profile', '=', 0)
			                                           ->get();

			return response()->json([
				                        'success'           => true,
				                        'deliveryImageUrls' => $deliveryImageUrls
			                        ]);
		}

		return response()->json([
			                        'success' => false
		                        ]);
	}
}