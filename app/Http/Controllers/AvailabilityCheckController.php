<?php

namespace App\Http\Controllers;

use App\Http\Models\AvailCheck\AvailabilityCheck;
use App\Http\Models\AvailCheck\AvailabilityCheckGallery;
use App\Http\Models\Cake;
use App\Http\Models\CakeGallery;
use App\Http\Models\Decor;
use App\Http\Models\DecorGallery;
use App\Http\Models\Notification;
use App\Http\Models\Package;
use App\Http\Models\PackageGallery;
use App\Http\Models\Planner;
use App\Http\Models\ServiceGallery;
use App\Http\Models\TypeService;
use App\Http\Models\TypeTicket;
use App\Http\Models\AddOn\AddOn;
use App\Http\Models\Venue;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class AvailabilityCheckController extends BaseController
{
	public function showTicketAvailChecks(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$ticketId = $request->input('ticketId');

			$availChecks = AvailabilityCheck::select('availability_check.*', 'ticket.name', 'ticket.phone', 'ticket.event_date', 'type_ticket.name AS type_ticket_name')
			                                ->join('ticket', 'availability_check.ticket_id', '=', 'ticket.id')
			                                ->join('type_ticket', 'type_ticket.id', '=', 'availability_check.map_type_id')
			                                ->where('availability_check.created_at', '>=', '2019-06-01 17:06:18')
			                                ->where('availability_check.ticket_id', $ticketId)
			                                ->whereNull('availability_check.deleted_at')
			                                ->whereNull('ticket.deleted_at')
			                                ->whereNull('type_ticket.deleted_at')
			                                ->orderby('availability_check.created_at', 'DESC')
			                                ->get();
			$availCheckData = [];
			$option = [];
			$availCheckImagesData = [];
			$planners = null;
			if (count($availChecks))
			{
				foreach ($availChecks as $availCheck)
				{
					switch ($availCheck->map_type_id)
					{

						// packages
						case config('evibe.ticket.type.packages'):
						case config('evibe.ticket.type.food'):
						case config('evibe.ticket.type.priests'):
						case config('evibe.ticket.type.tents'):
						case config('evibe.ticket.type.villa'):
						case config('evibe.ticket.type.couple-experiences'):
							$option = Package::select('id', 'code')
							                 ->where('is_live', 1)
							                 ->where('id', '=', $availCheck->map_id)
							                 ->first();
							$planner_package = Package::find($availCheck->map_id);
							if ($planner_package->map_type_id == config('evibe.ticket.type.venues'))
							{
								$planners = Venue::select('venue.name', 'venue.user_id')
									//->where('is_live','=',1)
									             ->where('id', '=', $planner_package->map_id)
								                 ->get();

							}
							else
							{
								$planners = Planner::select('planner.name', 'planner.user_id')
								                   ->where('is_live', '=', 1)
								                   ->wherenull('deleted_at')
								                   ->get();

							}
							break;
						case config('evibe.ticket.type.entertainments'):
							$option = TypeService::select('id', 'code')
							                     ->where('type_service.is_live', 1)
							                     ->where('type_service.id', '=', $availCheck->map_id)
							                     ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.cakes'):
							$option = Cake::select('id', 'code')
							              ->where('cake.is_live', 1)
							              ->where('cake.id', '=', $availCheck->map_id)
							              ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						// decors
						case config('evibe.ticket.type.decors'):
							$option = Decor::select('id', 'code')
							               ->where('decor.is_live', 1)
							               ->where('decor.id', '=', $availCheck->map_id)
							               ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.add-on'):
							$option = AddOn::select('id', 'code')
							               ->where('add_on.is_live', 1)
							               ->where('add_on.id', '=', $availCheck->map_id)
							               ->first();
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

							break;
						case config('evibe.ticket.type.planners'):
							$planners = Planner::select('planner.name', 'planner.user_id')
							                   ->where('is_live', '=', 1)
							                   ->wherenull('deleted_at')
							                   ->get();

					}

					$partnersData = [];
					$partners = Notification::select('notifications.*')
					                        ->where('notifications.availability_check_id', '=', $availCheck->id)
					                        ->whereNull('notifications.deleted_at')
					                        ->get();
					if (count($partners) > 0)
					{

						foreach ($partners as $partner)
						{
							$plannerName = $planners->where('user_id', '=', $partner->created_for)->first();
							array_push($partnersData, [
								'Name'          => isset($plannerName->name) ? $plannerName->name : null,
								'isReplied'     => $partner->is_replied,
								'repliedAt'     => $partner->replied_at,
								'replyDecision' => $partner->reply_decision,
								'replyText'     => $partner->reply_text,
								'budget'        => $partner->budget_price,
								'info'          => $partner->text
							]);
						}
					}

					array_push($availCheckData, [
						'availCheckId' => $availCheck->id,
						'ticketId'     => $availCheck->ticket_id,
						'createdDate'  => $availCheck->created_at,
						'name'         => $availCheck->name,
						'phoneNumber'  => $availCheck->phone,
						'partyDate'    => $availCheck->event_date,
						'optionType'   => $availCheck->type_ticket_name,
						'budget'       => $availCheck->budget,
						'option'       => (isset($option->code) && $option->code) ? $option->code : " Custom ",
						'optionId'     => (isset($option->id) && $option->id) ? $option->id : null,
						'partners'     => $partnersData,

					]);
				}

				$availCheckIds = $availChecks->pluck('id');
				if (count($availCheckIds))
				{
					$availCheckImages = AvailabilityCheckGallery::whereIn('availability_check_id', $availCheckIds)->whereNull('deleted_at')->get();
					if (count($availCheckImages))
					{
						foreach ($availCheckImages as $image)
						{
							$imageData = [
								'id'           => $image->id,
								'availCheckId' => $image->availability_check_id,
								'title'        => $image->title,
								'url'          => $image->url,
								'type'         => $image->type_id
							];

							if ($image->type_id == config('evibe.gallery.type.image'))
							{
								$imageData['url'] = config('evibe.hosts.gallery') . '/ticket/' . $ticketId . '/images/avail-check/' . $image->availability_check_id . '/' . $image->url;
							}

							array_push($availCheckImagesData, $imageData);
						}
					}

				}
			}

			$optionType = TypeTicket::select("name", "id")->get();
			$optionData = [];

			$requiredOptionIds = [
				config("evibe.ticket.type.packages"),
				config("evibe.ticket.type.cakes"),
				config("evibe.ticket.type.decors"),
				config("evibe.ticket.type.entertainments"),
				config("evibe.ticket.type.villa"),
				config("evibe.ticket.type.food"),
				config("evibe.ticket.type.couple-experiences"),
				config("evibe.ticket.type.priests"),
				config("evibe.ticket.type.tents"),
				config("evibe.ticket.type.add-on")
			];

			if (count($optionType))
			{
				foreach ($optionType as $option)
				{
					if (in_array($option->id, $requiredOptionIds))
					{
						array_push($optionData, [
							'optionId'    => $option->id,
							'optionValue' => $option->name
						]);
					}
				}
			}

			$data = [
				'options'          => $optionData,
				'availCheckData'   => $availCheckData,
				'availCheckImages' => $availCheckImagesData
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}

	}

	public function getOptionsByType(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		$mapTypeId = $request->input('optionId');
		$hasProvider = false;
		$mappingValues = $mappers = [];
		switch ($mapTypeId)
		{
			// packages
			case config('evibe.ticket.type.packages'):
			case config('evibe.ticket.type.food'):
			case config('evibe.ticket.type.priests'):
			case config('evibe.ticket.type.tents'):
			case config('evibe.ticket.type.villa'):
			case config('evibe.ticket.type.couple-experiences'):
				$mappers = Package::with("provider")
				                  ->where('is_live', 1)
				                  ->where('type_ticket_id', $mapTypeId)
				                  ->get();
				$hasProvider = true;
				break;

			case config('evibe.ticket.type.entertainments'):
				$mappers = TypeService::select('type_service.*')
				                      ->where('type_service.is_live', 1)
				                      ->whereNull('type_service.deleted_at')
				                      ->get();

				break;

			case config('evibe.ticket.type.cakes'):
				$mappers = Cake::with("provider")
				               ->join('cake_event', 'cake_event.cake_id', '=', 'cake.id')
				               ->where('cake.is_live', 1)
				               ->whereNull('cake.deleted_at')
				               ->whereNull('cake_event.deleted_at')
				               ->select('cake.*')
				               ->get();
				$hasProvider = true;
				break;

			// decors
			case config('evibe.ticket.type.decors'):
				$mappers = Decor::with("provider")
				                ->join('decor_event', 'decor_event.decor_id', '=', 'decor.id')
				                ->where('decor.is_live', 1)
				                ->whereNull('decor.deleted_at')
				                ->select('decor.*')
				                ->get();
				$hasProvider = true;
				break;

			case config('evibe.ticket.type.add-on'):
				$mappers = AddOn::select('add_on.*')
				                ->where('add_on.is_live', 1)
				                ->whereNull('add_on.deleted_at')
				                ->get();
		}

		if (count($mappers) > 0)
		{
			foreach ($mappers as $mapper)
			{
				$basicVal = [
					'code' => $mapper->code,
					'name' => $mapTypeId == config('evibe.ticket.type.cakes') ? $mapper->title : ($mapper->name ? $mapper->name : ""),
					'info' => $mapper->info
				];

				if ($mapTypeId == config('evibe.ticket.type.decors'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->min_price,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.cakes'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price_per_kg,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.entertainments'))
				{
					$servicePrice = $this->formatPrice($mapper->min_price);
					$servicePrice .= $mapper->max_price ? ' - Rs. ' . $this->formatPrice($mapper->max_price) : '';

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $servicePrice,
					]);
				}
				elseif ($mapTypeId == config('evibe.ticket.type.add-on'))
				{

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price_per_unit,

					]);
				}
				else
				{
					// packages exists, but vendors is not active
					$isValid = $hasProvider ? $mapper->provider : $mapper->person;
					if (!$isValid)
					{
						continue;
					}

					$mappingValues[$mapper->id] = array_merge($basicVal, [
						'price' => $mapper->price,

					]);
				}
			}
		}

		return $mappingValues;
	}

	public function getInfo(Request $request)
	{
		$userId = $this->checkForAccessToken($request);
		try
		{
			$availCheckOptionsData = $request->input('availCheckOptionsData');
			$price = null;
			$info = "";
			$response = [];
			$imagesData = [];

			$optionData = $availCheckOptionsData[0]; // only one product exists for non-venue
			$optionUnavailable = false;
			$name = null;
			switch ($optionData['optionTypeId'])
			{
				case config('evibe.ticket.type.packages'):
				case config('evibe.ticket.type.villa'):
				case config('evibe.ticket.type.food'):
				case config('evibe.ticket.type.couple-experiences'):
				case config('evibe.ticket.type.priests'):
				case config('evibe.ticket.type.tents'):
				case config('evibe.ticket.type.generic-package'):
					$option = Package::find($optionData['optionId']);
					if (!$option)
					{
						$optionUnavailable = true;
						break;
					}

					$price = $option->price;
					$info = $option->info;
					$name = $option->name;
					//getting image urls
					$images = PackageGallery::select('planner_package_gallery.url')
					                        ->where('planner_package_gallery.planner_package_id', '=', $optionData['optionId'])
					                        ->wherenull('planner_package_gallery.deleted_at')
					                        ->get();
					if (count($images) > 0)
					{
						foreach ($images as $image)
						{
							$url = config('evibe.hosts.gallery') . '/package/' . $optionData['optionId'] . '/images/';
							$url .= $image->url;
							$imagesData[] = $url;
						}
					}

					break;
				case config('evibe.ticket.type.decors'):
					$option = Decor::find($optionData['optionId']);
					if (!$option)
					{
						$optionUnavailable = true;
						break;
					}

					$price = $option->min_price;
					$info = $option->info;
					$name = $option->name;
					//getting images
					$images = DecorGallery::select('url', 'decor_id')
					                      ->wherenull('decor_gallery.deleted_at')
					                      ->where('decor_gallery.decor_id', '=', $optionData['optionId'])
					                      ->get();
					if (count($images) > 0)
					{
						foreach ($images as $image)
						{
							$url = $image->getlink();
							$imagesData[] = $url;
						}
					}
					break;
				case config('evibe.ticket.type.cakes'):
					$option = Cake::find($optionData['optionId']);
					if (!$option)
					{
						$optionUnavailable = true;
						break;
					}

					$price = $option->price;
					$info = $option->info;
					$name = $option->title;

					//getting images
					$images = CakeGallery::select('url', 'cake_id')
					                     ->wherenull('deleted_at')
					                     ->where('cake_id', '=', $optionData['optionId'])
					                     ->get();
					if (count($images) > 0)
					{
						foreach ($images as $image)
						{
							$url = $image->getlink();
							$imagesData[] = $url;
						}
					}
					break;
				case config('evibe.ticket.type.entertainments'):
					// todo: test services
					$option = TypeService::find($optionData['optionId']);
					if (!$option)
					{
						$optionUnavailable = true;
						break;
					}

					$price = $option->min_price;
					$info = $option->info;
					$name = $option->name;

					$images = ServiceGallery::select('url', 'type_service_id')
					                        ->wherenull('deleted_at')
					                        ->where('type_service_id', '=', $optionData['optionId'])
					                        ->get();
					if (count($images) > 0)
					{
						foreach ($images as $image)
						{
							$url = $image->getlink();
							$imagesData[] = $url;
						}
					}

					break;
				case config('evibe.ticket.type.add-on'):
					$option = AddOn::find($optionData['optionId']);
					if (!$option)
					{
						$optionUnavailable = true;
						break;
					}

					$price = $option->price_per_unit;
					$info = $option->info;
					$name = $option->name;

					$images = AddOn::select('id', 'image_url')
					               ->wherenull('deleted_at')
					               ->where('id', '=', $optionData['optionId'])
					               ->get();
					if (count($images) > 0)
					{
						foreach ($images as $image)
						{
							$url = $image->getlink();
							$imagesData[] = $url;
						}
					}

					break;
			}

			if ($optionUnavailable)
			{
				Log::error("Unable to fetch option data from optionId: " . $optionData['optionId'] . ", optionTypeId: " . $optionData['optionTypeId']);

				array_push($response, [
					'success'  => false,
					'errorMsg' => 'Unable to fetch option data. Kindly refresh the page and try again'
				]);

				return $response;
			}

			$availCheckOptionsData[0]['name'] = $name;

			// fetch price
			// fetch info

			$response = [
				'success'     => true,
				'optionsData' => $availCheckOptionsData,
				'price'       => $price,
				'info'        => $info,
				'imageLinks'  => $imagesData
			];

			return response()->json($response);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}

	}

	public function postAvailCheck(Request $request)
	{
		try
		{

			$userId = $this->checkForAccessToken($request);
			$option = $request->input('optionsData');
			$option = json_decode($option);
			$options = $option[0];
			$optionTypeId = $options->optionType;
			$optionId = $options->optionId;
			$partyDate = $options->partyDate;
			$ticketId = $options->ticketId;
			$budget = $options->optionBudget;
			$info = $options->optionInfo;

			$uploadImageLinks = $request->input('uploadedImageLInks');
			$uploadImageLinks = json_decode($uploadImageLinks);
			$invalidImageLinks = [];
			$validImages = [];

			$imageUpload = true;
			$validationArray = [
				'optionId'     => $optionId,
				'ticketId'     => $ticketId,
				'optionBudget' => $budget,
				'info'         => $info
			];

			$rules = [
				'optionId'     => 'required|numeric',
				'ticketId'     => 'required|numeric',
				'optionBudget' => 'numeric',
				'info'         => 'required|min:10'
			];

			$messages = [
				'optionId.required'    => 'Please choose a valid option Id ',
				'ticketId.required'    => 'Ticket Id cannot be found ',
				'optionId.numeric'     => 'Please choose a valid option Id ',
				'ticketId.numeric'     => 'Ticket Id must be a number ',
				'optionBudget.numeric' => 'Budget must be a number ',
				'info.required'        => 'Please enter some Info about availability check',
				'info.min'             => 'info length should be minimum of 10'
			];

			$validator = Validator::make($validationArray, $rules, $messages);

			if ($validator->fails())
			{
				return response()->json([
					                        'success'  => false,
					                        'errorMsg' => $validator->messages()->first()
				                        ]);
			}

			foreach ($uploadImageLinks as $imageUrl)
			{
				if (!empty($imageUrl))
				{
					try
					{
						$validImg = getimagesize($imageUrl);
						if ($validImg)
						{
							$validImages[] = [
								'title'   => pathinfo($imageUrl, PATHINFO_FILENAME),
								'url'     => $imageUrl,
								'type_id' => 2,
							];
						}
						else
						{
							$invalidImageLinks[] = $imageUrl;
						}
					} catch (\Exception $e)
					{
						$invalidImageLinks[] = $imageUrl;
					}
				}
			}

			if (count($invalidImageLinks) > 0)
			{
				return $res = [
					'success'       => false,
					'errorMsg'      => 'Some image links doesn\'t have images or invalid, please check the links below',
					'hasImageError' => true,
					'invalidImage'  => implode(",", $invalidImageLinks)
				];
			}

			$id = DB::table('availability_check')->insertGetId([
				                                                   'map_type_id' => $optionTypeId,
				                                                   'map_id'      => $optionId,
				                                                   'ticket_id'   => $ticketId,
				                                                   'budget'      => $budget,
				                                                   'info'        => $info,
				                                                   'enquired_by' => $userId,
				                                                   'created_at'  => Carbon::now(),
				                                                   'updated_at'  => Carbon::now(),
			                                                   ]);
			if (isset($validImages) && count($validImages) > 0)
			{
				foreach ($validImages as $key => $value)
				{
					$validImages[$key]['availability_check_id'] = $id;
				}
				$imageUpload = AvailabilityCheckGallery::insert($validImages);

			}

			if ($id && $imageUpload)
			{
				$res = [
					'success'      => true,
					'availCheckId' => $id
				];
			}
			else
			{
				$res = [
					'success'  => false,
					'errorMsg' => 'Unable to create availability check,please refresh the browser and try again'
				];
			}

			return response()->json($res);

		} catch (\Exception $exception)
		{
			$this->sendAndSaveReport($exception->getMessage());

			return response()->json([
				                        'success' => false
			                        ]);
		}
	}

}