<?php

namespace App\Http\Controllers\PIAB;

use App\Http\Controllers\BaseController;
use App\Http\Models\Coupon\Coupon;
use App\Http\Models\Product\ProductBooking;
use App\Http\Models\Product\ProductBookingStatus;
use App\Http\Models\Product\ProductBookingUpdate;
use App\Http\Models\Product\ProductStyleGallery;
use App\Jobs\Mail\MailProductBookingStatusUpdateToCustomerJob;
use App\Jobs\Sms\SMSProductBookingUpdateToCustomerJob;
use App\Jobs\Sms\SMSProofValidationStatusToCustomerJob;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class piabDashboardController extends BaseController
{
	public function showProductBookings(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$productBookings = ProductBooking::select('product_booking.*', 'product_booking_status.name AS status', 'product.name AS product_type', 'product_category.name AS product_category'
				, 'product_style.name AS product_style')
			                                 ->join('product_booking_status', 'product_booking.product_booking_status_id', '=', 'product_booking_status.id')
			                                 ->join('product', 'product_booking.product_id', '=', 'product.id')
			                                 ->join('product_style', 'product_style.id', '=', 'product_booking.product_style_id')
			                                 ->join('product_category', 'product.product_category_id', '=', 'product_category.id')
			                                 ->whereNotNull('product_booking.paid_at')
			                                 ->whereNull('product_booking.deleted_at')
			                                 ->whereNull('product_booking_status.deleted_at')
			                                 ->whereNull('product.deleted_at')
			                                 ->whereNull('product_style.deleted_at')
			                                 ->whereNull('product_category.deleted_at')
			                                 ->get();

			$productBookingData = [];
			if (count($productBookings))
			{
				foreach ($productBookings as $productBooking)
				{
					array_push($productBookingData, [
						'productBookingId' => $productBooking->id,
						'customerName'     => $productBooking->name,
						'customerPhone'    => $productBooking->phone,
						'customerEmail'    => $productBooking->email,
						'address'          => $productBooking->address,
						'city'             => $productBooking->city,
						'state'            => $productBooking->state,
						'status'           => $productBooking->status,
						'productName'      => $productBooking->product_type,
						'productCategory'  => $productBooking->product_category,
						'productStyle'     => $productBooking->product_style,
						'paymentDate'      => $productBooking->paid_at ? date('d-m-Y H:i:s', $productBooking->paid_at) : '',
						'productPrice'     => $productBooking->product_price,
						'deliveryCharge'   => $productBooking->delivery_charges,
						'bookingAmount'    => $productBooking->booking_amount,
						'couponDiscount'   => $productBooking->coupon_discount_amount,
						'createdAt'        => date('d M Y, h:i A', strtotime($productBooking->created_at))
					]);
				}
			}

			$data = [
				'productBookingData' => $productBookingData
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}

	}

	public function showProductBookingInfo(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$productBookingId = $request['productBookingId'];
			$productBooking = ProductBooking::select('product_booking.*', 'product_booking_status.name AS status', 'product.name AS product_type', 'product_category.name AS product_category'
				, 'product_style.name AS product_style')
			                                ->join('product_booking_status', 'product_booking.product_booking_status_id', '=', 'product_booking_status.id')
			                                ->join('product', 'product_booking.product_id', '=', 'product.id')
			                                ->join('product_style', 'product_style.id', '=', 'product_booking.product_style_id')
			                                ->join('product_category', 'product.product_category_id', '=', 'product_category.id')
			                                ->where('product_booking.id', '=', $productBookingId)
			                                ->whereNull('product_style.deleted_at')
			                                ->whereNull('product_category.deleted_at')
			                                ->first();

			$coupon = null;
			if (isset($productBooking->coupon_id))
			{
				$coupon = Coupon::find($productBooking->coupon_id);
			}

			$productBookingStatus = ProductBookingStatus::select('product_booking_status.*')
			                                            ->whereNull('product_booking_status.deleted_at')
			                                            ->get();

			$productBookingUpdate = ProductBookingUpdate::select('product_booking_update.*', 'product_booking_status.name AS status_name', 'user.name AS handler_name')
			                                            ->join('product_booking_status', 'product_booking_status.id', '=', 'product_booking_update.status_id')
			                                            ->join('user', 'user.id', '=', 'product_booking_update.handler_id')
			                                            ->where('product_booking_update.product_booking_id', '=', $productBookingId)
			                                            ->whereNull('product_booking_update.deleted_at')
			                                            ->whereNull('product_booking_status.deleted_at')
			                                            ->whereNull('user.deleted_at')
			                                            ->orderby('product_booking_update.status_at', 'DESC')
			                                            ->get();
			$productBookingData = [];
			array_push($productBookingData, [
				'id'               => $productBooking->id,
				'productBookingId' => $productBooking->booking_order_id,
				'customerName'     => $productBooking->name,
				'customerPhone'    => $productBooking->phone,
				'customerEmail'    => $productBooking->email,
				'callingCode'      => $productBooking->calling_code,
				'customerGender'   => $productBooking->customer_gender,
				'address'          => $productBooking->address,
				'zipCode'          => $productBooking->zipcode,
				'landmark'         => $productBooking->landmark,
				'city'             => $productBooking->city,
				'state'            => $productBooking->state,
				'status'           => $productBooking->status,
				'statusId'         => $productBooking->product_booking_status_id,
				'productName'      => $productBooking->product_type,
				'productCategory'  => $productBooking->product_category,
				'productStyle'     => $productBooking->product_style,
				'paymentDate'      => $productBooking->paid_at ? date('d-m-Y H:i:s', $productBooking->paid_at) : '',
				'productPrice'     => $productBooking->product_price,
				'deliveryCharge'   => $productBooking->delivery_charges,
				'bookingAmount'    => $productBooking->booking_amount,
				'couponDiscount'   => $productBooking->coupon_discount_amount,
				'comment'          => $productBooking->comment,
				//'couponID'         => $productBooking->coupon_id,
				'couponCode'       => ($coupon && $coupon->coupon_code) ? $coupon->coupon_code : null,
				'deliveryDate'     => $productBooking->expected_delivery_date ?: null,
				'createdAt'        => date('d M Y, h:i A', strtotime($productBooking->created_at))
			]);

			$productBookingStatuses = [];
			if (count($productBookingStatus))
			{
				foreach ($productBookingStatus as $status)
				{
					array_push($productBookingStatuses, [
						'statusId'   => $status->id,
						'statusName' => $status->name
					]);
				}
			}

			$productBookingUpdateData = [];
			if (count($productBookingUpdate))
			{
				foreach ($productBookingUpdate as $update)
				{
					array_push($productBookingUpdateData, [
						'statusName'  => $update->status_name,
						'comments'    => $update->comments,
						'handlerName' => $update->handler_name,
						'statusAt'    => $update->status_at
					]);
				}
			}

			$data = [
				'productBookingInfo'    => $productBookingData[0],
				'productBookingStatus'  => $productBookingStatuses,
				'productBookingUpdates' => $productBookingUpdateData,
			];

			return response()->json($data);

		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());
		}

	}

	public function updateProductBookingStatus(Request $request)
	{
		try
		{
			$userId = $this->checkForAccessToken($request);
			$productBookingId = $request->input('productBookingId');
			$comment = $request->input('comment');
			$statusId = $request->input('statusId');
			$expectedDeliveryDate = $request->input('expectedDeliveryDate');
			$isCustomerUpdate = $request->input('isCustomerUpdate');
			$statusAt = strtotime(carbon::now());

			$rules = [
				'statusId'             => 'required',
				'comment'              => 'required|min:4',
				'expectedDeliveryDate' => 'required'
			];

			$messages = [
				'statusId.required'             => 'Please select status',
				'comment.required'              => 'Update comment is required',
				'comment.min'                   => 'Comment should be at least 4 characters long',
				'expectedDeliveryDate.required' => 'Expected delivery date is required'
			];

			$validation = Validator::make(Input::all(), $rules, $messages);

			if ($validation->fails())
			{
				$res = [
					"success"  => false,
					"errorMsg" => $validation->messages()->first()
				];

				return response()->json($res);
			}

			else
			{
				$update = ProductBookingUpdate::create([
					                                       'product_booking_id' => $productBookingId,
					                                       'handler_id'         => $userId,
					                                       'comments'           => $comment,
					                                       'status_id'          => $statusId,
					                                       'created_at'         => Carbon::now(),
					                                       'updated_at'         => Carbon::now(),
					                                       'status_at'          => $statusAt
				                                       ]);
				if ($update)
				{
					$productBooking = ProductBooking::find($productBookingId);
					$productBooking->product_booking_status_id = $statusId;
					$productBooking->expected_delivery_date = $expectedDeliveryDate;
					$productBooking->save();

					if ($productBooking)
					{
						if ($isCustomerUpdate == 0)
						{
							return response()->json([
								                        'success'    => true,
								                        'successMsg' => 'Status updated successfully.'
							                        ]);
						}
						else
						{
							$customerData = ProductBooking::select('product_booking.*', 'product_booking_status.name AS status', 'product.name AS product_type', 'product_category.name AS product_category', 'product_style.name AS product_style', 'product_style.id AS product_style_id', 'product_style.info AS product_info')
							                              ->join('product_booking_status', 'product_booking.product_booking_status_id', '=', 'product_booking_status.id')
							                              ->join('product', 'product_booking.product_id', '=', 'product.id')
							                              ->join('product_style', 'product_style.id', '=', 'product_booking.product_style_id')
							                              ->join('product_category', 'product.product_category_id', '=', 'product_category.id')
							                              ->where('product_booking.id', '=', $productBookingId)
							                              ->whereNull('product_booking_status.deleted_at')
							                              ->first();

							$imgUrl = config('evibe.hosts.gallery') . '/product-styles/' . $customerData->product_style_id . '/images/results/';

							$imgData = ProductStyleGallery::select('product_style_gallery.*')
							                              ->where('product_style_gallery.product_style_id', '=', $customerData->product_style_id)
							                              ->whereNull('product_style_gallery.deleted_at')
							                              ->orderBy('priority', 'ASC')
							                              ->get();

							if (count($imgData))
							{
								$profileImgData = $imgData->where('is_profile', '1')->first();
								if ($profileImgData && $profileImgData->url)
								{
									$imgUrl .= $profileImgData->url;
								}
								else
								{
									$profileImgData = $imgData->first();
									if ($profileImgData)
									{
										$imgUrl .= $profileImgData->url;
									}
									else
									{
										// @todo: show any default img
									}
								}
							}

							$tmoLink = config('evibe.live.host') . '/track?ref=PIABTracking';
							$tmoLink = $this->getShortUrl($tmoLink);

							$emailData = [
								'productBookingId'              => $customerData->booking_order_id,
								'customerName'                  => $customerData->name,
								'customerEmail'                 => $customerData->email,
								'status'                        => $customerData->status,
								'statusId'                      => $customerData->product_booking_status_id,
								'tmoLink'                       => $tmoLink,
								'productName'                   => $customerData->product_type,
								'productCategory'               => $customerData->product_category,
								'productStyle'                  => $customerData->product_style,
								'productPrice'                  => $customerData->product_price,
								'productPriceStr'               => $customerData->product_price ? $this->formatPrice($customerData->product_price) : 0,
								'deliveryCharge'                => $customerData->delivery_charges,
								'deliveryChargeStr'             => $customerData->delivery_charges ? $this->formatPrice($customerData->delivery_charges) : 0,
								'bookingAmount'                 => $customerData->booking_amount,
								'bookingAmountStr'              => $customerData->booking_amount ? $this->formatPrice($customerData->booking_amount) : 0,
								'couponDiscount'                => $customerData->coupon_discount_amount,
								'totalAmountPaidStr'            => ($customerData->booking_amount - $customerData->coupon_discount_amount) ? $this->formatPrice($customerData->booking_amount - $customerData->coupon_discount_amount) : 0,
								'imageUrl'                      => $imgUrl,
								'bookingInfo'                   => $customerData->booking_info,
								'productInfo'                   => $customerData->product_info,
								'updateComment'                 => $comment,
								'expectedDeliveryDate'          => $customerData->expected_delivery_date ? date('jS M Y', strtotime($customerData->expected_delivery_date)) : null,
								'productBookingStatusDelivered' => config('evibe.product-booking-status.delivered'),
								'productBookingStatusCancelled' => config('evibe.product-booking-status.cancelled'),
							];

							$this->dispatch(new MailProductBookingStatusUpdateToCustomerJob($emailData));

							$smsCustomer = config('evibe.sms_tpl.piab.update.customer');
							$replaces = [
								'#customer#'         => $customerData->name,
								'#orderId#'          => '#' . $customerData->booking_order_id,
								'#status#'           => $customerData->status,
								'#tmoLink#'          => $tmoLink,
								'#piabSupportEmail#' => config('evibe.contact.support.group')
							];

							$smsText = str_replace(array_keys($replaces), array_values($replaces), $smsCustomer);

							$smsData = [
								'customerPhone' => $customerData->phone,
								'smsText'       => $smsText
							];

							$this->dispatch(new SMSProductBookingUpdateToCustomerJob($smsData));

							return response()->json([
								                        'success'    => true,
								                        'successMsg' => 'Status updated successfully. Status update sent to customer'
							                        ]);

						}
					}
					else
					{
						return response()->json([
							                        'success'  => false,
							                        'errorMsg' => 'Unable to update the status, please try again'

						                        ]);
					}

				}
				else
				{
					return response()->json([
						                        'success'  => false,
						                        'errorMsg' => 'Unable to update the status, please try again'

					                        ]);

				}
			}
		} catch (\Exception $exception)
		{
			Log::error($exception->getMessage());

			return response()->json(['success' => false]);
		}
	}

}