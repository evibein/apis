<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketMapping extends BaseModel
{
	use SoftDeletes;

	protected $table = 'ticket_mapping';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function type()
	{
		return $this->belongsTo(TypeBooking::class, 'type_id');
	}

	public function galleries()
	{
		return $this->hasMany(TicketMappingGallery::class, 'ticket_mapping_id');
	}

}