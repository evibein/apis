<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketRemindersStack extends BaseModel
{
	use SoftDeletes;
	protected $table = 'ticket_reminders_stack';
	protected $guarded = ['id'];
}