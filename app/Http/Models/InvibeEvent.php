<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class InvibeEvent extends BaseModel {

	use SoftDeletes;

	protected $table = 'invibe_events';

	protected $primaryKey = 'id';

	protected $hidden = ['user_id'];

	protected $guarded = [];
	

	public function scopeOfUser($query, $usrId)
	{
		return $query->where('user_id','=',$usrId);	
	}

	public function scopeOfType($query, $typeOfEvent)
	{
		if ($typeOfEvent != 'all') {
			return $query->where('type_id','=',$typeOfEvent);
		}

		return $query;
	}

}