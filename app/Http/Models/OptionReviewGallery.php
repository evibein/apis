<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OptionReviewGallery extends BaseModel
{
	use SoftDeletes;

	protected $table = 'option_review_gallery';

}