<?php

namespace App\Http\Models;

class TicketAdditionalFieldValues extends BaseModel
{
	protected $table = 'ticket_additional_field_values';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
	public $timestamps = false;
}