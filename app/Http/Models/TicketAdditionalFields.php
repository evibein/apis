<?php

namespace App\Http\Models;

class TicketAdditionalFields extends BaseModel
{
	protected $table = 'ticket_additional_fields';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
	public $timestamps = false;

	public function getAdditionalFieldValue($ticketId)
	{
		$additionalFieldValue = TicketAdditionalFieldValues::where([
			                                                           "ticket_id"           => $ticketId,
			                                                           "additional_field_id" => $this->id
		                                                           ])
		                                                   ->first();
		if ($additionalFieldValue)
		{
			return $additionalFieldValue->value;
		}
	}
}