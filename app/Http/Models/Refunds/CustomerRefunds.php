<?php

namespace App\Http\Models\Refunds;

use App\Http\Models\BaseModel;
use App\Http\Models\TicketBooking;
use App\Http\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerRefunds extends BaseModel
{
	use SoftDeletes;

	protected $table = 'customer_refunds';

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function booking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}
}