<?php

namespace App\Http\Models\Coupon;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CouponUsers extends BaseModel
{
	use SoftDeletes;
	protected $table = 'coupon_users';
}