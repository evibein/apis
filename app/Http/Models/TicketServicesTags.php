<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 28/9/16
 * Time: 12:39 PM
 */

namespace App\Http\Models;

class TicketServicesTags extends BaseModel
{
	protected $table = 'ticket_services_tags';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}