<?php namespace App\Http\Models;

class NotificationType extends BaseModel {

	protected $table = 'type_notification';

	protected $primaryKey = 'nott_id';

	public $incrementing = false;

	protected $guarded = [];

	public function notification()
	{
		return $this->hasMany('App\Http\Models\Notification','type_id');
	}

}