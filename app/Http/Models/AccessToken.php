<?php namespace App\Http\Models;

class AccessToken extends BaseModel
{

	protected $table = 'access_tokens';
	protected $primaryKey = 'access_token';
	public $incrementing = false;
	protected $guarded = [];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

}