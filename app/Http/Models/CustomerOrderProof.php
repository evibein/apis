<?php

namespace App\Http\Models;

use App\Http\Models\Types\TypeProof;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerOrderProof extends BaseModel
{
	use SoftDeletes;

	protected $table = 'customer_order_proofs';

	public function typeProof()
	{
		return $this->belongsTo(TypeProof::class, 'type_proof_id');
	}
}