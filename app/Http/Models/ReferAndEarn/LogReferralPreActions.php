<?php

namespace App\Http\Models\ReferAndEarn;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogReferralPreActions extends BaseModel
{
	use SoftDeletes;
	protected $table = 'log_referral_pre_actions';
	protected $guarded = ['id'];
}