<?php

namespace App\Http\Models\ReferAndEarn;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserSpecialDate extends BaseModel
{
	use SoftDeletes;
	protected $table = 'user_special_date';
}