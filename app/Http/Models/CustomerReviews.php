<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerReviews extends BaseModel
{
    protected $table = 'ticket_review';

    use SoftDeletes;
}