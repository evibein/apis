<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeTicketStatus extends BaseModel
{

	use SoftDeletes;

	protected $table = 'type_ticket_status';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

}