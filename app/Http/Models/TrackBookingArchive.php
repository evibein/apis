<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TrackBookingArchive extends BaseModel
{
	use SoftDeletes;

	protected $table = 'track_booking_archive';
	protected $primaryKey = 'id';

	public function booking()
	{
		return $this->belongsTo('App\Http\Models\TicketBooking', 'ticket_booking_id');
	}
}