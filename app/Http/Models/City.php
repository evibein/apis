<?php namespace App\Http\Models;

class City extends BaseModel {

	protected $table = 'city';

	protected $primaryKey = 'id';

	public function area()
	{
		return $this->hasMany('App\Http\Models\Area','city_id');
	}
	
}