<?php

namespace App\Models\Review;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class ReviewReply extends Model
{
	use SoftDeletes;

	protected $table = 'review_reply';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function review()
	{
		return $this->belongsTo('Review', 'review_id');
	}
}