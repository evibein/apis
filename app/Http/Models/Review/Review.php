<?php

namespace App\Models\Review;

use Illuminate\Database\Eloquent\SoftDeletes;
use \Illuminate\Database\Eloquent\Model;

class Review extends Model
{
	use SoftDeletes;

	protected $table = 'planner_review';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function vendor()
	{
		return $this->belongsTo(\App\Http\Models\Planner::class, 'planner_id');
	}

	public function booking()
	{
		return $this->belongsTo(\App\Http\Models\TicketBooking::class, 'ticket_booking_id');
	}

	public function reply()
	{
		return $this->hasMany(ReviewReply::class, 'review_id');
	}

	public function individualAnswers()
	{
		return $this->hasMany(PartnerReviewAnswer::class, 'review_id');
	}
}