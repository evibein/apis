<?php

namespace App\Models\Review;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypePartnerReviewQuestion extends Model
{
	use SoftDeletes;
	
	protected $table = 'type_partner_review_question';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}
