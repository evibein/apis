<?php

namespace App\Http\Models;

use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\Settlements;

class Planner extends BaseModel
{
	protected $table = 'planner';
	protected $primaryKey = 'id';
	protected $guarded =['id'];

	public function user()
	{
		return $this->belongsTo('App\Http\Models\User', 'user_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function area()
	{
		return $this->belongsTo(Area::class, 'area_id');
	}

	public function bookings()
	{
		return $this->morphMany(TicketBooking::class, null, 'map_type_id', 'map_id');
	}

	public function getProfilePic()
	{
		return "";
	}

	public function getPriceWorth()
	{
		return 0;
	}

	public function packages()
	{
		return $this->morphMany(Package::class, null, 'map_type_id', 'map_id');
	}

	public function adjustments()
	{
		return $this->morphMany(PartnerAdjustments::class, null, 'partner_type_id', 'partner_id');
	}

	public function settlements()
	{
		return $this->morphMany(Settlements::class, null, 'partner_type_id', 'partner_id');
	}
}