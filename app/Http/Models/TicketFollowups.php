<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFollowups extends BaseModel
{
	use SoftDeletes;

	protected $table = 'ticket_followups';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}