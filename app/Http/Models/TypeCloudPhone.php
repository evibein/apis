<?php

namespace App\Http\Models;

class TypeCloudPhone extends BaseModel
{
	protected $table = 'type_cloud_phone';

	protected $primaryKey = 'id';
}