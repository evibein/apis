<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 1/10/16
 * Time: 4:10 PM
 */

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class PlannerPackageEvent extends BaseModel
{
	use SoftDeletes;
	protected $table = 'planner_package_event';
	protected $guarded = ['id'];
}