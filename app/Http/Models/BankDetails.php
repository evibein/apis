<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class BankDetails extends BaseModel
{
	use SoftDeletes;

	protected $table = 'bank_details';
	protected $primaryKey = 'id';
	protected $guarded = [];

	public function user()
	{
		return $this->belongsTo(User::class, 'user_id');
	}

}