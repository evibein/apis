<?php namespace App\Http\Models;

class AuthClient extends BaseModel {

	protected $table = 'auth_clients';

	protected $primaryKey = 'auth_client_id';

	public $incrementing = false;

	protected $guarded = [];

}