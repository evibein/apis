<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class ReportBookingLive extends BaseModel
{
	use SoftDeletes;

	protected $table = 'report_booking_live';
	protected $primaryKey = 'id';

	public function booking()
	{
		return $this->belongsTo('App\Http\Models\TicketBooking', 'ticket_booking_id');
	}

	public function cloudPhone()
	{
		return $this->belongsTo('App\Http\Models\TypeCloudPhone', 'type_cloud_phone_id');
	}
}