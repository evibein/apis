<?php

namespace App\Http\Models\Product;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductBooking extends BaseModel
{
	protected $table = 'product_booking';
	protected $primaryKey='id';

	use SoftDeletes;
}
