<?php

namespace App\Http\Models\Product;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductBookingStatus extends BaseModel
{
	protected $table = 'product_booking_status';

	use SoftDeletes;
}
