<?php

namespace App\Http\Models\Product;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductStyleGallery extends BaseModel
{
	protected $table = 'product_style_gallery';
	protected $primaryKey='id';

	use SoftDeletes;
}
