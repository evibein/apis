<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class User extends BaseModel
{
	use softDeletes;

	protected $table = 'user';
	protected $primaryKey = 'id';
	protected $hidden = ["password", "remember_token", "otp"];

	protected $toCamelCaseException = [
		"id"       => "userId",
		"username" => "emailId"
	];

	public function planner()
	{
		return $this->hasOne('App\Http\Models\Planner', 'user_id');
	}

	public function bankDetails()
	{
		return $this->hasOne(BankDetails::class, 'user_id');
	}
}