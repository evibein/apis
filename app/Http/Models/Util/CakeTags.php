<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class CakeTags extends BaseModel
{
	use SoftDeletes;

	protected $table = 'cake_tags';
	protected $guarded = [];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function cake()
	{
		return $this->belongsTo('Cake', 'cake_id');
	}

	public function tag()
	{
		return $this->belongsTo('TileTag', 'tag_id');
	}
}