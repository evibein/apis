<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use App\Http\Models\User;

class SocialAccount extends BaseModel
{
	protected $table = 'user_info';

	protected $fillable = ['user_id', 'provider_id', 'provider', 'user_email', 'name', 'img_url'];

	public function user()
	{
		return $this->belongsTo(User::class);
	}
}