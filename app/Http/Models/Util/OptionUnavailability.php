<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionUnavailability extends BaseModel
{
	protected $table = 'option_unavailability';

	use SoftDeletes;
}