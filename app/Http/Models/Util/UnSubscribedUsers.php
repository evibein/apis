<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class UnSubscribedUsers extends BaseModel
{
	use SoftDeletes;

	protected $table = 'unsubscribed_users';
}