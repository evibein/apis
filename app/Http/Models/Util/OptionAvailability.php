<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class OptionAvailability extends BaseModel
{
	protected $table = 'option_availability';

	use SoftDeletes;
}