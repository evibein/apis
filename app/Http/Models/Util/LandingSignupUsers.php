<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;

class LandingSignupUsers extends BaseModel
{
	protected $table = 'landing_signup_users';
}