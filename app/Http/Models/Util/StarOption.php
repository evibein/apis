<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class StarOption extends BaseModel
{
	use SoftDeletes;

	protected $table = 'star_option';
}