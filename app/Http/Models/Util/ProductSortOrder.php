<?php

namespace App\Http\Models\Util;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ProductSortOrder extends BaseModel
{
	use SoftDeletes;

	protected $table = 'product_sort_order';
}