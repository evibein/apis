<?php namespace App\Http\Models;

class InvibeEventType extends BaseModel {

	protected $table = 'invibe_event_types';

	protected $primaryKey = 'id';

	public function themes()
	{
		return $this->hasMany('App\Http\Models\InvibeTheme','event_type_id');
	}
	
}