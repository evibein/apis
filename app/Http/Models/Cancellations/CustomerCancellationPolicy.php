<?php

namespace App\Http\Models\Cancellations;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CustomerCancellationPolicy extends Model
{
	use SoftDeletes;

	protected $table = 'customer_cancellation_policy';
}