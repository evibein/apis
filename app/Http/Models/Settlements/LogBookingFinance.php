<?php

namespace App\Http\Models\Settlements;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class LogBookingFinance extends BaseModel
{
	use SoftDeletes;

	protected $table = 'log_booking_finance';
}