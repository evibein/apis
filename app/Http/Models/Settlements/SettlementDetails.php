<?php

namespace App\Http\Models\Settlements;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class SettlementDetails extends BaseModel
{
	use SoftDeletes;

	protected $table = 'settlement_details';
}