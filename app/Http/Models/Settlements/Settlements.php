<?php

namespace App\Http\Models\Settlements;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Settlements extends BaseModel
{
	use SoftDeletes;

	protected $table = 'settlements';

	public function partner()
	{
		return $this->morphTo(null, 'partner_type_id', 'partner_id');
	}
}