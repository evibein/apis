<?php

namespace App\Http\Models\Settlements;

use App\Http\Models\BaseModel;
use App\Http\Models\TicketBooking;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleSettlementBookings extends BaseModel
{
	use SoftDeletes;

	protected $table = 'schedule_settlement_bookings';

	public function bookings()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}
}