<?php

namespace App\Http\Models\Settlements;

use App\Http\Models\BaseModel;
use App\Http\Models\TicketBooking;
use App\Http\Models\User;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerAdjustments extends BaseModel
{
	use SoftDeletes;

	protected $table = 'partner_adjustments';

	public function partner()
	{
		return $this->morphTo(null, 'partner_type_id', 'partner_id');
	}

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}
}