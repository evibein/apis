<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ReportRecordPin extends Model
{
	use SoftDeletes;
	protected $table = 'report_record_pin';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}
