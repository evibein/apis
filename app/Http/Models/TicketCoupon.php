<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketCoupon extends BaseModel
{
	use SoftDeletes;

	protected $table = 'ticket_coupons';
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];
}