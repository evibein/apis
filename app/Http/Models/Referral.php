<?php

namespace App\Http\Models;

class Referral extends BaseModel
{
	protected $table = 'ticket_referral';
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	public function ticket()
	{
		return $this->hasOne(Ticket::class, 'id' , 'ticket_id');
	}
}