<?php namespace App\Http\Models;

class NotificationAttachment extends BaseModel
{

	protected $table = 'notification_attachments';

	protected $primaryKey = 'id';

	public $incrementing = false;

	protected $guarded = [];

	protected $hidden = ["id", "notification_id"];

	public function notification()
	{
		return $this->belongsTo('App\Http\Models\Notification', 'notification_id');
	}

}