<?php

namespace App\Http\Models\Shortlist;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Shortlist extends BaseModel
{
	use SoftDeletes;

	protected $table = 'shortlist';
}