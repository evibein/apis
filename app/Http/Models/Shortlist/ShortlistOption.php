<?php

namespace App\Http\Models\Shortlist;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class ShortlistOption extends BaseModel
{
	use SoftDeletes;

	protected $table = 'shortlist_option';
}