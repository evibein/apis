<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	public function toArrayCamel()
	{
		return $this->arrayCamel($this->toArray());
	}

	public function arrayCamel($arr)
	{
		$return = [];

		foreach ($arr as $key => $value)
		{
			if (isset($this->toCamelCaseException[$key]))
			{
				$transformedKey = $this->toCamelCaseException[$key];
			}
			else
			{
				$transformedKey = camel_case($key);
			}

			if (is_array($value))
			{
				$return[$transformedKey] = $this->arrayCamel($value);
			}
			else
			{
				$return[$transformedKey] = $value;
			}

		}

		return $return;

	}

	public function newCollection(array $models = [])
	{
		return new CustomCollection($models);
	}

	public function getFormattedName($name)
	{
		return ucwords(strtolower(trim($name)));
	}
}