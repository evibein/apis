<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Area extends BaseModel
{
	use SoftDeletes;

	protected $table = 'area';
	protected $primaryKey = 'id';

	public function city()
	{
		return $this->belongsTo('App\Http\Models\City', 'city_id');
	}
}