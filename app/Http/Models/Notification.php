<?php namespace App\Http\Models;

use Carbon\Carbon;
use Evibe\Utility\TypeTicketUtility;
use Illuminate\Database\Eloquent\SoftDeletes;

class Notification extends BaseModel
{
	use SoftDeletes;

	protected $table = 'notifications';

	protected $primaryKey = 'id';

	protected $hidden = ["created_for", "ticket_booking_id"];

	protected $toCamelCaseException = [
		"created_by" => "author"
	];

	protected $guarded = ['id'];

	public function type()
	{
		return $this->belongsTo('App\Http\Models\NotificationType', 'type_id');
	}

	public function attachments()
	{
		return $this->hasMany('App\Http\Models\NotificationAttachment', 'notification_id');

	}

	public function ticketBooking()
	{
		return $this->belongsTo(TicketBooking::class, 'ticket_booking_id');
	}

	public function ticketMapping()
	{
		return $this->belongsTo(TicketMapping::class, 'ticket_mapping_id');
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}

	public function scopeIsActive($query)
	{
		return $query->join('ticket', 'ticket.id', '=', 'notifications.ticket_id')
		             ->join('ticket_mapping', 'notifications.ticket_mapping_id', '=', 'ticket_mapping.id')
		             ->where('ticket.event_date', '>', Carbon::now()->timestamp)
		             ->whereIn('ticket.status_id', TypeTicketUtility::VALID_NOTIFICATION_STATUS)
		             ->whereNull('notifications.is_replied')
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('ticket_mapping.deleted_at');
	}

	public function scopeIsActiveAvailabilityChecks($query)
	{
		return $query->join('ticket', 'ticket.id', '=', 'notifications.ticket_id')
		             ->join('availability_check', 'availability_check.id', '=', 'notifications.availability_check_id')
		             ->where('ticket.event_date', '>', Carbon::now()->timestamp)
		             ->whereIn('ticket.status_id', TypeTicketUtility::VALID_NOTIFICATION_STATUS)
		             ->whereNull('notifications.is_replied')
		             ->whereNotNull('notifications.availability_check_id')
		             ->whereNull('notifications.ticket_mapping_id')
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('availability_check.deleted_at');
	}

	public function scopeIsInternalAvailActive($query)
	{
		return $query->join('ticket', 'ticket.id', '=', 'notifications.ticket_id')
		             ->join('ticket_mapping', 'notifications.ticket_mapping_id', '=', 'ticket_mapping.id')
		             ->where('ticket.event_date', '>', Carbon::now()->timestamp)
		             ->whereIn('ticket.status_id', TypeTicketUtility::VALID_INTERNAL_AVAIL_NOTIFICATION_STATUS)
		             ->whereNull('notifications.is_replied')
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('ticket_mapping.deleted_at');
	}

	public function scopeSelectCols($query)
	{
		$query->select('notifications.*', 'ticket.id as ticketId', 'ticket.event_id', 'ticket_mapping.id as mappingId',
		               'ticket_mapping.party_date_time', 'ticket_mapping.type_id as typeId', 'ticket_mapping.type_details',
		               'ticket_mapping.type_id as typeId', 'ticket.status_id');
	}

	public function scopeOfUser($query, $usrId)
	{
		return $query->where('created_for', '=', $usrId);
	}

	public function scopeFilter($query, $filter)
	{
		switch ($filter)
		{
			case 'archived':
				return $query->where('is_archived', '=', 1);
				break;
			case 'unreplied':
				return $query->where('is_replied', '=', 0);
				break;
			case 'replied':
				return $query->where('is_replied', '=', 1);
				break;
			case 'unread':
				return $query->where('is_read', '=', 0);
				break;
			case 'read':
				return $query->where('is_read', '=', 1);
				break;
			default:
				// Default is all unreplied
				// return $query->where('is_replied','=',0);
				break;
		}
	}

	public function createdFor()
	{
		return $this->belongsTo(User::class, 'created_for');
	}

}