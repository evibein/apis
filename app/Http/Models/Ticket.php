<?php

namespace App\Http\Models;

use Evibe\Utility\TicketStatus;
use Illuminate\Database\Eloquent\SoftDeletes;

class Ticket extends BaseModel
{
	use SoftDeletes;

	protected $table = 'ticket';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
	public $timestamps = false;

	public function bookings()
	{
		return $this->hasMany('App\Http\Models\TicketBooking', 'ticket_id');
	}

	public function mappings()
	{
		return $this->hasMany(TicketMapping::class, 'ticket_id');
	}

	public function area()
	{
		return $this->belongsTo('App\Http\Models\Area', 'area_id');
	}

	public function city()
	{
		return $this->belongsTo(City::class, 'city_id');
	}

	public function source()
	{
		return $this->belongsTo('App\Http\Models\TypeTicketSource', 'source_id');
	}

	public function setNameAttribute($value)
	{
		$this->attributes['name'] = $this->getFormattedName($value);
	}

	public function getNameAttribute($value)
	{
		return $this->getFormattedName($value);
	}

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}

	public function slot()
	{
		return $this->belongsTo(Slot::class, 'type_slot_id');
	}

	public function typeVenue()
	{
		return $this->belongsTo(TypeVenue::class, 'type_venue_id');
	}

	public function status()
	{
		return $this->belongsTo('App\Http\Models\TypeTicketStatus', 'status_id');
	}

	public function isBooked()
	{
		$isBooked = false;

		if ($this->status_id == TicketStatus::BOOKED)
		{
			$isBooked = true;
		}

		return $isBooked;
	}

	public function gallery()
	{
		return $this->hasMany(TicketGallery::class, 'ticket_id');
	}

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function scopeSelectColsForSalesDash($query)
	{
		return $query->select('ticket.id', 'ticket.name', 'ticket.created_at', 'ticket.deleted_at', 'ticket.phone', 'ticket.calling_code',
		                      'ticket.handler_id', 'ticket.event_id', 'ticket.status_id', 'ticket.paid_at', 'ticket.event_date','ticket.lead_status_id',
		                      'ticket.enquiry_source_id', 'ticket.created_handler_id', 'ticket.is_auto_booked')
		             ->whereNull('ticket.deleted_at')
		             ->orderBy('ticket.created_at', 'DESC');
	}

	public function scopeSelectColsForCRMDash($query)
	{
		return $query->select('ticket.id', 'ticket.name', 'ticket.created_at', 'ticket.deleted_at', 'ticket.phone', 'ticket.calling_code',
		                      'ticket.handler_id', 'ticket.lead_status_id', 'ticket.source_id', 'ticket.booking_likeliness_id',
		                      'ticket.enquiry_source_id', 'ticket.city_id', 'ticket.event_id', 'ticket.status_id',
		                      'ticket.paid_at', 'ticket.success_email_type', 'ticket.success_email_sent_at',
		                      'ticket.event_date', 'ticket.asked_order_details_at', 'ticket.created_handler_id', 'ticket.is_auto_booked',
		                      'ticket.last_reco_rating', 'ticket.last_reco_rated_at', 'ticket.last_reco_dislike_at', 'ticket.expected_closure_date',
		                      'ticket.customer_preferred_slot')
		             ->whereNull('ticket.deleted_at')
		             ->orderBy('ticket.created_at', 'DESC')
		             ->orderBy('ticket.lead_status_id');
	}

	public function customerProofs()
	{
		return $this->hasMany(CustomerOrderProof::class, 'ticket_id');
	}
}