<?php

namespace App\Http\Models\AvailCheck;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PartnerAvailability extends BaseModel
{
	use SoftDeletes;

	protected $table = 'partner_calendar';
}