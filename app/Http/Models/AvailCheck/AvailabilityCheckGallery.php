<?php

namespace App\Http\Models\AvailCheck;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AvailabilityCheckGallery extends BaseModel
{
	use SoftDeletes;

	protected $table = 'availability_check_gallery';
}