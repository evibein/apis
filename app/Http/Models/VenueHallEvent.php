<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 1/10/16
 * Time: 4:15 PM
 */

namespace App\Http\Models;

class VenueHallEvent extends BaseModel
{
	protected $table = "venue_hall_event";

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'event_id');
	}
}