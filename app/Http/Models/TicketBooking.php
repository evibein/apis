<?php

namespace App\Http\Models;

use Carbon\Carbon;
use Evibe\Utility\TicketStatus;
use Evibe\Utility\TypeTicketUtility;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketBooking extends BaseModel
{
	protected $table = 'ticket_bookings';
	protected $primaryKey = 'id';
	protected $dates = ['deleted_at'];
	protected $guarded = ['id'];

	use SoftDeletes;

	public function ticket()
	{
		return $this->belongsTo('App\Http\Models\Ticket', 'ticket_id');
	}

	public function notes()
	{
		return $this->hasMany('App\Http\Models\TicketBookingNote', 'booking_id');
	}

	public function galleries()
	{
		return $this->hasMany(TicketBookingGallery::class, 'ticket_booking_id');
	}

	public function deliveryGalleries()
	{
		return $this->hasMany(DeliveryGallery::class, 'ticket_booking_id');
	}

	public function ticketMapping()
	{
		return $this->belongsTo(TicketMapping::class, 'ticket_mapping_id');
	}

	public function scopeOfUser($query, $usrId)
	{
		return $query->join('planner', 'ticket_bookings.vendor_id', '=', 'planner.id')
		             ->join('user', 'planner.user_id', '=', 'user.id')
		             ->where('user.id', '=', $usrId);
	}

	public function scopeWithTicketDetails($query)
	{
		return $query->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id');
	}

	public function scopeWithAreaDetails($query)
	{
		return $query->join('area', 'area.id', '=', 'ticket.area_id');
	}

	public function scopeOfType($query, $typeOfBooking, $startTime, $endTime)
	{
		switch ($typeOfBooking)
		{
			case 'upcoming':
				if ($endTime == 0)
				{
					$query->where('ticket.event_date', '>', $startTime)
					      ->where('ticket.status_id', '=', TicketStatus::BOOKED);
				}
				else
				{
					$query->whereBetween('ticket.event_date', [$startTime, $endTime])
					      ->where('ticket.status_id', '=', TicketStatus::BOOKED);
				}

				break;
			case 'inprogress':
				$query->where('ticket.status_id', '=', TicketStatus::CONFIRMED);
				break;
			case 'past':
				if ($startTime == 0)
				{
					$query->where('ticket.event_date', '<', $endTime);
				}
				else
				{
					$query->whereBetween('ticket.event_date', [$startTime, $endTime]);
				}
				break;

			default:
				# code...
				break;
		}

		return $query;
	}

	public function scopeBookedTicket($query)
	{
		return $query->join('ticket', 'ticket_bookings.ticket_id', '=', 'ticket.id')
		             ->where('ticket.status_id', TypeTicketUtility::STATUS_BOOKED)
		             ->where('ticket_bookings.is_advance_paid', 1)
		             ->whereNull('ticket.deleted_at')
		             ->whereNull('ticket_bookings.deleted_at');
	}

	public function scopeLiveOrders($query, $diffMinutes = 60)
	{
		$start = Carbon::now()->timestamp;
		$start -= $diffMinutes * 60; // we show 60 minutes after party start time as live order

		return $query->where('party_date_time', '>=', $start)
		             ->orderBy('party_date_time', 'ASC');
	}

	public function scopeForDeliverable($query, $diffMinutes = 120)
	{
		$end = Carbon::now()->timestamp;
		$end += $diffMinutes * 60; // we show 60 minutes before party stat time as live delivery
		$tenDaysStart = Carbon::today()->addDay('-10')->startOfDay()->timestamp;
		$dateRange = [$tenDaysStart, $end];

		return $query->whereBetween('ticket_bookings.party_date_time', $dateRange)
		             ->where('ticket_bookings.is_delivered', 0)
		             ->orderBy('ticket_bookings.party_date_time', 'DESC');
	}

	public function scopeSelectCols($query)
	{
		return $query->select('ticket_bookings.*', 'ticket.event_id', 'ticket.id as ticketId', 'ticket.status_id');
	}

	public function provider()
	{
		return $this->morphTo(null, 'map_type_id', 'map_id');
	}

	public function handler()
    {
        return $this->belongsTo(User::class,'partner_confirmed_handler_id');
    }
}