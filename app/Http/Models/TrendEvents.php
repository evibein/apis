<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 1/10/16
 * Time: 4:15 PM
 */

namespace App\Http\Models;

class TrendEvent extends BaseModel
{
	protected $table = "trend_event";

	public function event()
	{
		return $this->belongsTo(TypeEvent::class, 'type_event_id');
	}
}