<?php

namespace App\Http\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeLeadStatus extends Model
{
	use SoftDeletes;

	protected $table = 'type_lead_status';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}