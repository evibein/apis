<?php

namespace App\Http\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeEnquirySource extends Model
{
	use SoftDeletes;

	protected $table = 'type_enquiry_source';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}