<?php

namespace App\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeBookingConcept extends Model
{
	use SoftDeletes;

	protected $table = 'type_booking_concept';
}