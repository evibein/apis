<?php

namespace App\Http\Models\Types;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeReminderGroup extends Model
{
	use SoftDeletes;

	protected $table = 'type_reminder_group';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}