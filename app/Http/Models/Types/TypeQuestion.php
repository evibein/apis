<?php

namespace App\Http\Models\Types;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class TypeQuestion extends BaseModel
{
	use SoftDeletes;

	protected $table = 'type_question';
}