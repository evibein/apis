<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SelectedDeliveryImages extends Model
{
	use SoftDeletes;
	protected $table = 'selected_delivery_images';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
}