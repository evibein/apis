<?php

namespace App\Http\Models;

class TicketGallery extends BaseModel
{
	protected $table = 'ticket_gallery';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function imageLink()
	{
		return config('evibe.hosts.gallery') . '/ticket/' . $this->ticket_id . '/images/' . $this->url;
	}
}