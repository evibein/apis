<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class LogTicketReminders extends BaseModel
{
	use SoftDeletes;
	protected $table = 'log_ticket_reminders';
	protected $guarded = ['id'];
}