<?php

namespace App\Http\Models\AddOn;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddOn extends BaseModel
{
	use SoftDeletes;

	protected $table = 'add_on';

	public function scopeForEvent($query, $eventId)
	{
		return $query->join('add_on_event', 'add_on_event.add_on_id', '=', 'add_on.id')
		             ->whereNull('add_on_event.deleted_at')
		             ->where('add_on_event.event_id', $eventId);
	}

	public function scopeIsLive($query)
	{
		return $query->where('add_on.is_live', 1);
	}

	public function scopeForCity($query, $cityId = null)
	{
		return $query->where('add_on.city_id', $cityId);
	}

	public function getLink()
	{
		$imgLink = config('evibe.hosts.gallery');
		$imgLink .= '/addons/' . $this->id . '/';
		$imgLink .= $this->image_url;

		return $imgLink;
	}
}