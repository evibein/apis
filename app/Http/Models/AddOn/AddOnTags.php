<?php

namespace App\Http\Models\AddOn;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class AddOnTags extends BaseModel
{
	use SoftDeletes;

	protected $table = 'add_on_tags';
}