<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class OptionReview extends BaseModel
{
	use SoftDeletes;

	protected $table = 'option_review';

}