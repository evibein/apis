<?php namespace App\Http\Models;

use Illuminate\Database\Eloquent\Collection;

class CustomCollection extends Collection {

	private $customModelArray = null;

	function __construct(array $models = []) {
		$this->customModelArray = $models;
		parent::__construct($models);
	}

	public function toArrayCamel()
	{
		$camelCaseArray = array();
		foreach ($this->customModelArray as $model) {
			array_push($camelCaseArray, $model->toArrayCamel());
		}
		return $camelCaseArray;
	}
}