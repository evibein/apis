<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketUpdate extends BaseModel
{
	use SoftDeletes;

	protected $table = 'ticket_update';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];

	public function handler()
	{
		return $this->belongsTo(User::class, 'handler_id');
	}

	public function status()
	{
		return $this->belongsTo(TypeTicketStatus::class, 'status_id');
	}

	public function ticket()
	{
		return $this->belongsTo(Ticket::class, 'ticket_id');
	}
}