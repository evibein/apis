<?php

namespace App\Http\Models;

class InvibeTheme extends BaseModel
{
	protected $table = 'invibe_themes';

	protected $primaryKey = 'id';

	protected $hidden = ["event_type_id"];

	protected $guarded = [];
}