<?php

namespace App\Http\Models\Decor;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class DecorTags extends BaseModel
{
	use SoftDeletes;

	protected $table = 'decor_tags';
}