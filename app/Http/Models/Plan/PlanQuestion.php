<?php

namespace App\Http\Models\Plan;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanQuestion extends BaseModel
{
	use SoftDeletes;

	protected $table = 'plan_questions';

	public function options()
	{
		return $this->hasMany(PlanQuestionOption::class, 'plan_question_id');
	}
}