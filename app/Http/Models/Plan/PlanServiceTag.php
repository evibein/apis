<?php

namespace App\Http\Models\Plan;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanServiceTag extends BaseModel
{
	use SoftDeletes;

	protected $table = 'plan_service_tags';

	public function scopeTagDetails($query)
	{
		return $query->join('type_category_tags', 'type_category_tags.id', '=', 'plan_service_tags.category_tag_id')
		             ->join('type_tags', 'type_tags.id', '=', 'type_category_tags.type_tag_id')
		             ->select('plan_service_tags.*', 'type_tags.type_event', 'type_tags.map_type_id', 'type_tags.name', 'type_category_tags.count', 'type_category_tags.min_price', 'type_category_tags.max_price', 'type_category_tags.info');
	}
}