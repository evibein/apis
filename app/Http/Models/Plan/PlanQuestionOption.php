<?php

namespace App\Http\Models\Plan;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanQuestionOption extends BaseModel
{
	use SoftDeletes;

	protected $table = 'plan_question_options';
}