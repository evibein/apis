<?php

namespace App\Http\Models\Plan;

use App\Http\Models\BaseModel;
use Illuminate\Database\Eloquent\SoftDeletes;

class Plan extends BaseModel
{
	use SoftDeletes;

	protected $table = 'plan';
}