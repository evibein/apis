<?php namespace App\Http\Models;

use App;

class InvibeEventGuest extends BaseModel
{
	protected $table = 'invibe_event_guests';

	protected $primaryKey = 'id';

	protected $hidden = ['event_id', 'user_id'];

	protected $guarded = [];

	public static function bulkInsert($guests, $evtId)
	{
		$utility = \app()->make('Utility');

		$existingGuests = InvibeEventGuest::where('event_id', '=', $evtId)->pluck('user_phone')->toArray();

		$guestsToBeUpdated = [];
		$guestsPhoneNumber = [];

		foreach ($guests as $key => $guest)
		{

			array_push($guestsPhoneNumber, $guest['phone']);

			if (isset($guest['phone']) && in_array($guest['phone'], $existingGuests))
			{
				array_push($guestsToBeUpdated, $guest);
				unset($guests[$key]);
				continue;
			}

			$guest = $utility->chageKeyName($guest, 'name', 'user_name');
			$guest = $utility->chageKeyName($guest, 'email', 'user_email');
			$guest = $utility->chageKeyName($guest, 'phone', 'user_phone');

			$guests[$key] = $guest;

			$guests[$key]['event_id'] = $evtId;
		}

		if (count($guestsToBeUpdated) > 0)
		{

			$nameUpdateStatementToBeUpdated = false;
			$nameUpdateStatement = " ( CASE ";
			foreach ($guestsToBeUpdated as $value)
			{
				if (isset($value['name']))
				{
					$nameUpdateStatement .= " WHEN user_phone=" . $value['phone'] . " THEN '" . $value['name'] . "'";
					$nameUpdateStatementToBeUpdated = true;
				}
			}
			$nameUpdateStatement .= " ELSE user_name END ) ";

			$emailUpdateStatementToBeUpdated = false;
			$emailUpdateStatement = " ( CASE ";
			foreach ($guestsToBeUpdated as $value)
			{
				if (isset($value['email']))
				{
					$emailUpdateStatement .= " WHEN user_phone=" . $value['phone'] . " THEN '" . $value['email'] . "'";
					$emailUpdateStatementToBeUpdated = true;
				}
			}
			$emailUpdateStatement .= " ELSE user_email END ) ";

			$finalQueryToBeExecuted = false;
			$finalQuery = "UPDATE invibe_event_guests 
						   SET ";

			if ($nameUpdateStatementToBeUpdated)
			{
				$finalQuery .= " user_name = " . $nameUpdateStatement . ",";
				$finalQueryToBeExecuted = true;
			}

			if ($emailUpdateStatementToBeUpdated)
			{
				$finalQuery .= " user_email = " . $emailUpdateStatement . ",";
				$finalQueryToBeExecuted = true;
			}

			if ($finalQueryToBeExecuted)
			{

				$finalQuery = substr($finalQuery, 0, -1);
				$finalQuery .= " WHERE event_id =" . $evtId;

				\DB::statement($finalQuery);
			}

		}

		if (!InvibeEventGuest::insert($guests))
		{
			throw new App\Exceptions\CustomException("Error while inserting guests for this event.", 500);
		}

		$newGuests = InvibeEventGuest::where('event_id', '=', $evtId)
		                             ->whereIn('user_phone', $guestsPhoneNumber)
		                             ->get()->toArrayCamel();

		return $newGuests;
	}

}