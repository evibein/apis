<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TypeVenueHall extends BaseModel
{
	use SoftDeletes;
	protected $table = 'type_venue_hall';

}