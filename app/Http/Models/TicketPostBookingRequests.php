<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class TicketPostBookingRequests extends BaseModel
{
	protected $table = 'post_booking_requests';

	use SoftDeletes;
}