<?php
/**
 * Created by PhpStorm.
 * User: BlackOut
 * Date: 10-10-2018
 * Time: 15:56
 */
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TicketFollowupsType extends Model
{
	use SoftDeletes;
	protected $table = 'type_followups';
	protected $guarded = ['id'];
	public static $rules = [];
	protected $dates = ['deleted_at'];
}