<?php

namespace App\Http\Models;

class TicketBookingNote extends BaseModel
{
	protected $table = 'ticket_bookings_notes';
	protected $primaryKey = 'id';
	protected $hidden = ["booking_id"];

	public function booking()
	{
		return $this->belongsTo('App\Http\Models\TicketBooking', 'booking_id');
	}

}