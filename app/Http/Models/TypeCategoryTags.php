<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 28/9/16
 * Time: 12:39 PM
 */

namespace App\Http\Models;

use Carbon\Carbon;

class TypeCategoryTags extends BaseModel
{
	protected $table = 'type_category_tags';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];

	public function getCategoryTagSelection($ticketId)
	{
		$success = false;
		$handlerName = null;
		$selectedAt = null;

		$ticketServiceTag = TicketServicesTags::where([
			                                              'ticket_id'            => $ticketId,
			                                              'type_category_tag_id' => $this->id,
		                                              ])
		                                      ->whereNull("deleted_at")
		                                      ->first();
		if ($ticketServiceTag)
		{
			$success = true;

			$handler = User::find($ticketServiceTag->handler_id);
			if ($handler)
			{
				$handlerName = $handler->name;
			}

			$selectedAt = date("d/m/y h:i A", strtotime($ticketServiceTag->created_at));
		}

		return $res = [
			"success"     => $success,
			"handlerName" => $handlerName,
			"selectedAt"  => $selectedAt
		];
	}
}