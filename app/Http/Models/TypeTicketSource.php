<?php

namespace App\Http\Models;

class TypeTicketSource extends BaseModel
{
	protected $table = 'type_ticket_source';
	protected $primaryKey = 'id';
	protected $guarded = ['id'];
	protected $dates = ['deleted_at'];
}