<?php

namespace App\Providers;

use App\Http\Models\Planner;
use App\Http\Models\Venue;
use App\Evibe\Validator\EvibeValidator;
use Evibe\Utility\TypeTicketUtility;
use Illuminate\Database\Eloquent\Relations\Relation;
use Illuminate\Support\ServiceProvider;
use Evibe\Utility\Utility;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
	/**
	 * Register any application services.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->singleton('Utility', function ($app) {
			return new Utility();
		});
	}

	public function boot()
	{
		Validator::resolver(function ($translator, $data, $rules, $messages) {
			return new EvibeValidator($translator, $data, $rules, $messages);
		});

		// Model morphs
		$morphs = [
			TypeTicketUtility::PLANNER => Planner::class,
			TypeTicketUtility::VENUE   => Venue::class,
			TypeTicketUtility::ARTIST  => Planner::class
		];

		Relation::morphMap($morphs, false);
	}
}
