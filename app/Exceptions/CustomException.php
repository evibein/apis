<?php namespace App\Exceptions;

use Exception;

class CustomException extends Exception
{
	private $customCode = 400;

	function __construct($message = "", $customCode = 400)
	{
		$this->customCode = $customCode;
		parent::__construct($message);
	}

	public function getCustomCode()
	{
		return $this->customCode;
	}
}

