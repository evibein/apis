<?php namespace App\Exceptions;

use App\Http\Models\SiteError;
use App\Jobs\Notification\mailGenericErrorToAdmin;
use Exception;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Request;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;

class Handler extends ExceptionHandler
{

	/**
	 * A list of the exception types that should not be reported.
	 *
	 * @var array
	 */
	protected $dontReport = [];

	/**
	 * Report or log an exception.
	 *
	 * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
	 *
	 * @param  \Exception $e
	 *
	 * @return void
	 */
	public function report(Exception $e)
	{
		$this->sendAndSaveReport($e);
	}

	/**
	 * Render an exception into an HTTP response.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  \Exception               $e
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function render($request, Exception $e)
	{
		if ($e instanceof CustomException)
		{
			return response()->json(["errorMessage" => $e->getMessage()], $e->getCustomCode());
		}

		return parent::render($request, $e);
	}

	private function sendAndSaveReport($e)
	{
		$errorMessage = $e->getMessage() ?: '';
		if (strpos($errorMessage, "partner email") ||
			strpos($errorMessage, "user name / password"))
		{
			return false; // invalid logins are not errors
		}

		$code = $e->getCode() ?: 'E500';
		if ($e instanceof CustomException)
		{
			$code = $e->getCustomCode();
		}

		$sendEmail = !in_array($code, [401, 403]) && (strpos($errorMessage, "The email id must be a valid email address") != 0) ? true : false;

		if (config('evibe.email_error'))
		{
			$data = [
				'fullUrl'  => Request::fullUrl(),
				'code'     => $code,
				'messages' => $errorMessage,
				'details'  => $e->getTraceAsString()
			];

			if ($sendEmail)
			{
				$emailData = [
					'code'     => $code,
					'url'      => $data['fullUrl'],
					'method'   => Request::method(),
					'messages' => $data['messages'],
					'trace'    => $data['details']
				];

				dispatch(new mailGenericErrorToAdmin($emailData));
			}

			$this->saveError($data);
		}
	}

	private function saveError($data)
	{
		SiteError::create([
			                  'project_id' => config('evibe.project_id'),
			                  'url'        => $data['fullUrl'],
			                  'exception'  => $data['messages'],
			                  'code'       => $data['code'],
			                  'details'    => $data['details']
		                  ]);
	}

}
