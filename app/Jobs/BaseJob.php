<?php

namespace App\Jobs;

use App\Http\Models\PackageTag;
use App\Http\Models\Tags;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class BaseJob extends Job implements ShouldQueue
{
	protected $utility;

	function __construct()
	{
		$this->utility = \app()->make('Utility');
	}

	protected function checkPackageType($package, $checkForType)
	{
		$isType = false;
		$eventId = $package->event_id;

		$tagIds = Tags::where('type_event', $eventId)
		              ->where('map_type_id', $checkForType)
		              ->pluck('id');

		$packageTagObj = PackageTag::whereIn('tile_tag_id', $tagIds)
		                           ->where('planner_package_id', $package->id)
		                           ->get();

		if ($packageTagObj->count() > 0)
		{
			$isType = true;
		}

		return $isType;
	}

	protected function getPartnerCCAddress($partner, $businessCc = true)
	{
		$ccEmails = [];
		$emailCols = ['alt_email', 'alt_email_1', 'alt_email_2', 'alt_email_3'];

		if ($businessCc)
		{
			array_push($ccEmails, config('evibe.contact.business.group'));
		}

		foreach ($emailCols as $emailCol)
		{
			if (isset($partner->{$emailCol}) && filter_var($partner->{$emailCol}, FILTER_VALIDATE_EMAIL))
			{
				array_push($ccEmails, $partner->{$emailCol});
			}
		}

		return $ccEmails;
	}

	public function formatPrice($price)
	{
		$explrestUnits = "";
		$paise = "";

		if (strpos($price, '.'))
		{
			$numArray = explode('.', $price);
			$paise = $numArray[1];
			$price = $numArray[0];
		}

		if (strlen($price) > 3)
		{
			$lastthree = substr($price, strlen($price) - 3, strlen($price));
			$restunits = substr($price, 0, strlen($price) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestUnits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestUnits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestUnits . $lastthree;

		}
		else
		{
			$thecash = $price;
		}

		if ($paise)
		{
			$thecash .= '.' . $paise;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}

	public function getShortUrl($longUrl, $debug = false)
	{
		$shortUrl = $longUrl;

		if (!strlen($longUrl))
		{
			Log::error("empty link sent, debug message: " . $debug);

			return $shortUrl;
		}

		if (strpos($longUrl, "goo.gl") === false)
		{
			try
			{
				//$shortUrl = $this->getEvibesShortLink($longUrl);
				$shortUrl = $this->getGooGlShortLink($longUrl);

			} catch (\Exception $e)
			{
				Log::error("ERROR:: While using the bitly url short with link " . $longUrl);
			}
		}

		return $shortUrl;
	}

	private function getEvibesShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ((strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			$url = "http://evib.es/api/v2/action/shorten";
			$method = "POST";
			$accessToken = "";
			$jsonData = [
				'url'           => $longUrl,
				'key'           => config("evibe.evibes.access_token"),
				'custom_ending' => '',
				'is_secret'     => false,
				'response_type' => 'json'
			];

			try
			{
				$client = new Client();
				$res = $client->request($method, $url, [
					'headers' => [
						'access-token' => $accessToken
					],
					'json'    => $jsonData,
				]);

				$res = $res->getBody();
				$res = \GuzzleHttp\json_decode($res, true);
				if (isset($res["action"]) && $res["action"] == "shorten" && isset($res["result"]) && $res["result"] != $longUrl)
				{
					$shortUrl = $res["result"];
				}

			} catch (ClientException $e)
			{
				$apiResponse = $e->getResponse()->getBody(true);
				$apiResponse = \GuzzleHttp\json_decode($apiResponse);
				$res['error'] = $apiResponse->errorMessage;

				$this->saveError([
					                 'fullUrl' => request()->fullUrl(),
					                 'message' => 'Error occurred in Base controller while doing guzzle request',
					                 'code'    => 0,
					                 'details' => $res['error']
				                 ]);

				return false;
			}
		}

		return $shortUrl;
	}

	private function getGooGlShortLink($longUrl)
	{
		// longUrl definitely exists at this point
		$shortUrl = $longUrl;

		if ($longUrl && (strpos($longUrl, "bit.ly") === false) && (strpos($longUrl, "evib.es") === false))
		{
			try
			{
				/* @see: changing goo.gl to bit.ly
				$baseUrl = "https://www.googleapis.com/urlshortener/v1/url?key=" . config("evibe.google.shortener_key");

				$postData = ['longUrl' => $longUrl];
				$jsonData = json_encode($postData);

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
				curl_setopt($curlObj, CURLOPT_HEADER, 0);
				curl_setopt($curlObj, CURLOPT_HTTPHEADER, ['Content-type:application/json']);
				curl_setopt($curlObj, CURLOPT_POST, 1);
				curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);

				$response = curl_exec($curlObj);

				// Change the response json string to object
				curl_close($curlObj);

				$shortUrl = \GuzzleHttp\json_decode($response)->id;
				 */

				$accessToken = config('evibe.bitly.access_token');
				$switchTime = Carbon::createFromTimestamp(time())->startOfMonth()->addDays(15)->startOfDay()->toDateTimeString();
				if (time() > strtotime($switchTime))
				{
					$accessToken = config('evibe.bitly.alt_access_token');
				}
				$encodedUrl = urlencode($longUrl);
				// default format is json
				$baseUrl = "https://api-ssl.bitly.com/v3/shorten?access_token=" . $accessToken . "&longUrl=" . $encodedUrl;

				$curlObj = curl_init();

				curl_setopt($curlObj, CURLOPT_URL, $baseUrl);
				curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);

				$response = curl_exec($curlObj);

				curl_close($curlObj);

				// response is in json format
				// normal decode without 2nd var as 'true' will return in stdObjectClass
				$response = json_decode($response, true);
				if (isset($response['status_txt']) && $response['status_txt'] == "OK")
				{
					$shortUrl = isset($response['data']['url']) && $response['data']['url'] ? $response['data']['url'] : $longUrl;
				}
				else
				{
					$this->sendAndSaveNonExceptionReport([
						                                     'code'      => config('evibe.error_code.create_function'),
						                                     'url'       => 'Bitly Url Shortener',
						                                     'method'    => 'GET',
						                                     'message'   => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'exception' => '[BaseController.php - ' . __LINE__ . '] ' . 'Unable to create short url for [longUrl: ' . $longUrl . ']',
						                                     'trace'     => print_r($response, true),
						                                     'details'   => print_r($response, true)
					                                     ]);
				}
			} catch (\Exception $exception)
			{
				$this->saveError([
					                 'fullUrl' => "bitly short url",
					                 'message' => 'Error occurred in Base controller while doing bitly short request',
					                 'code'    => 0,
					                 'details' => "bitly short url details"
				                 ]);

				return false;
			}
		}

		return $shortUrl;
	}

	public function getTruncateString($string)
	{
		return str_limit($string);
	}

	public function getVenueMapLink($venue)
	{
		$baseMapLink = "https://www.google.com/maps/place/";
		if (!$venue)
		{
			return "";
		}

		$venueMapLink = $baseMapLink . str_replace(" ", "+", $venue->name);
		if ($venue->map_lat && $venue->map_long)
		{
			$venueMapLink = $baseMapLink . $venue->map_lat . "+" . $venue->map_long;
			$venueMapLink .= "/" . $venue->map_lat . "," . $venue->map_long . ",17z";
		}

		return $venueMapLink;
	}

	public function sendFailedJobsToTeam($exception)
	{
		$data = [
			"from"       => config('evibe.contact.company.email'),
			"to"         => config('evibe.contact.tech.group'),
			"errorTitle" => "Default Error Title",
			"code"       => "Default Code",
			"details"    => "Default Details",
			"file"       => "Default File",
			"line"       => "Default Line"
		];

		if (!is_null($exception) && $exception instanceof \Exception)
		{
			$data['errorTitle'] = $exception->getMessage();
			$data['code'] = $exception->getCode();
			$data['details'] = $exception->getTraceAsString();
			$data['file'] = $exception->getFile();
			$data['line'] = $exception->getLine();
		}

		$data['project'] = 'APIs';
		$data['sub'] = '[Failed Job] - ' . date('F j, Y, g:i a');

		Mail::send('emails.report.team.failed-jobs', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Evibe Errors')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});

		return true;
	}
}