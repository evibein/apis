<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 31/12/16
 * Time: 12:52 PM
 */

namespace App\Jobs\Sms;

class SMSDecorAutoBookingCancelledToPartner extends UtilSendSMS
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// send SMS to vendor
		$tplVendor = config('evibe.sms_tpl.auto_book.decor.cancel.partner');
		$replaces = [
			'#field1#' => $data['booking']['provider']['person'],
			'#field2#' => str_limit($data['booking']['name'], 10),
			'#field3#' => date('d-M-y',strtotime($data['booking']['checkInDate'])).' ('.date('D', strtotime($data['booking']['checkInDate'])).'), '.$data['booking']['checkInTime'],
			'#field4#' => str_limit($data['additional']['venueLocation'], 10),
			'#field5#' => config('evibe.contact.business.phone')
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);

		$this->sms($data['booking']['provider']['phone'], $smsText);
	}
}