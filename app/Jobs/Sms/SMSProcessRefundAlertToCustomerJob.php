<?php

namespace App\Jobs\Sms;

class SMSProcessRefundAlertToCustomerJob extends UtilSendSMS
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.finance.refunds.customer');
		$replaces = [
			'#customerName#' => $data['customerName'],
			'#refundAmount#' => $data['refundAmount'],
			'#bookingId#'    => $data['bookingId']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['customerPhone'],
			'text' => $text
		];

		$this->sms($smsData['to'], $smsData['text']);
	}
}