<?php

namespace App\Jobs\Sms;

class SMSProcessSettlementAlertToPartnerJob extends UtilSendSMS
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.finance.settlements.partner');
		$replaces = [
			'#partnerFirstName#' => $data['partnerName'],
			'#netDueAmount#'     => $data['settlementAmount'],
			'#startDate#'        => $data['settlementStartDate'],
			'#endDate#'          => $data['settlementEndDate'],
			'#partnerDashLink#'  => $data['partnerDashSMSLink']
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['partnerPhone'],
			'text' => $text
		];

		$this->sms($smsData['to'], $smsData['text']);
	}
}