<?php

namespace App\Jobs\Sms;

class SMSRecommendationsAlertToCustomer extends UtilSendSMS
{
	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if (!count($data))
		{
			return;
		}

		$tpl = config('evibe.sms_tpl.reco.customer');
		$replaces = [
			'#field1#' => $data['name'],
			'#field2#' => $data['recoLink'],
		];

		$text = str_replace(array_keys($replaces), array_values($replaces), $tpl);

		$smsData = [
			'to'   => $data['to'],
			'text' => $text
		];

		$this->sms($smsData['to'], $smsData['text']);
	}
}