<?php

namespace App\Jobs\Sms;

class SMSProofValidationStatusToCustomerJob extends UtilSendSMS
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$this->sms($data['to'], $data['text']);
	}
}