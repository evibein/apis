<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 30/12/16
 * Time: 5:08 PM
 */

namespace App\Jobs\Sms;

class SMSInvalidEmailToUserJob extends UtilSendSMS
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.invalid_email.customer');
		$replaces = [
			'#field1#' => $data['customerName'] ? $data['customerName'] : 'Customer',
			'#field2#' => $data['incorrectEmail'],
			'#field3#' => $data['emailCorrectionLink']
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms($data['customerPhone'], $smsText);
	}
}