<?php


namespace App\Jobs\Sms;

class SMSProductBookingUpdateToCustomerJob extends UtilSendSMS
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;


		$this->sms($data['customerPhone'], $data['smsText']);
	}
}