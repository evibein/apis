<?php

namespace App\Jobs\Sms\AutoBook;

use App\Jobs\Sms\UtilSendSMS;
use Illuminate\Support\Facades\Hash;

class SMSAutoBookingOrderConfirmationToCustomerJob extends UtilSendSMS
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		// todo:first booking id - needs to be changed (at-least one booking exists)
		$token = Hash::make($data["ticketId"] . "EVBTMO");

		// send SMS to customer

		if (isset($data['needProof']) && $data['needProof'] && isset($data['proofUpload']) && (!$data['proofUpload']))
		{
			$tplCustomer = config('evibe.sms_tpl.auto_book.confirm.customer_proof');

			$replaces = [
				'#customer#' => $data['customer']['name'],
				'#orderId#'  => $data["enquiryId"],
				'#tmoLink#'  => $this->getShortUrl(config("evibe.live.host") . config("evibe.track_my_order.url") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
			];
		}
		else
		{
			$tplCustomer = config('evibe.sms_tpl.auto_book.confirm.customer');

			$replaces = [
				'#field1#' => $data['customer']['name'],
				'#field2#' => $this->formatPrice($data['totalAdvancePaid']),
				'#field3#' => $data["enquiryId"],
				'#field4#' => $this->getShortUrl(config("evibe.live.host") . config("evibe.track_my_order.url") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
			];
		}

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms($data['customer']['phone'], $smsText);
	}
}