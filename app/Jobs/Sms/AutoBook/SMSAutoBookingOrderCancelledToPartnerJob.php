<?php

namespace App\Jobs\Sms\AutoBook;

use App\Jobs\Sms\UtilSendSMS;

class SMSAutoBookingOrderCancelledToPartnerJob extends UtilSendSMS
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		// todo: change sms content (first booking name)

		// send SMS to vendor
		$tplVendor = config('evibe.sms_tpl.auto_book.decor.cancel.partner');
		$replaces = [
			'#field1#' => $data['person'],
			'#field2#' => str_limit($data['bookings'][0]['name'], 10),
			'#field3#' => $data['partyDateTime'],
			'#field4#' => str_limit($data['additional']['venueLocation'], 10),
			'#field5#' => config('evibe.contact.business.phone')
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);

		$this->sms($data['phone'], $smsText);
	}
}