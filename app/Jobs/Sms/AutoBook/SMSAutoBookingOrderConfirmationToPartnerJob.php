<?php

namespace App\Jobs\Sms\AutoBook;

use App\Jobs\Sms\UtilSendSMS;

class SMSAutoBookingOrderConfirmationToPartnerJob extends UtilSendSMS
{
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$tplVendor = config('evibe.sms_tpl.auto_book.confirm.vendor');
		$replaces = [
			'#field1#' => $data['person'],
			'#field2#' => $data['additional']['partyDate'],
			'#field3#' => $this->formatPrice($data['totalAdvancePaid']),
			'#field4#' => $data['customer']['name'],
			'#field5#' => $this->formatPrice($data['totalBookingAmount']),
			'#field6#' => $data['email']
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplVendor);

		$this->sms($data['phone'], $smsText);
	}
}