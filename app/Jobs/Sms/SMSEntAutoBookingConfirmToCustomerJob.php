<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 30/12/16
 * Time: 5:08 PM
 */

namespace App\Jobs\Sms;

use Illuminate\Support\Facades\Hash;

class SMSEntAutoBookingConfirmToCustomerJob extends UtilSendSMS
{

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	private $data;

	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$token = Hash::make($data["ticketId"] . "EVBTMO");

		// send SMS to customer
		$tplCustomer = config('evibe.sms_tpl.auto_book.decor.confirm.customer');
		$replaces = [
			'#field1#' => $data['customer']['name'],
			'#field2#' => $this->formatPrice($data['booking']['advanceAmount']),
			'#field3#' => $data["enquiryId"],
			'#field4#' => $this->getShortUrl(config("evibe.live.host") . config("evibe.track_my_order.url") . "?ref=SMS&id=" . $data["ticketId"] . "&token=" . $token)
		];

		$smsText = str_replace(array_keys($replaces), array_values($replaces), $tplCustomer);

		$this->sms($data['customer']['phone'], $smsText);
	}
}