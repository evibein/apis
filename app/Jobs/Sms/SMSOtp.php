<?php

namespace App\Jobs\Sms;

class SMSOtp extends UtilSendSMS
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$this->sms($data['to'], $data['text']);
	}
}