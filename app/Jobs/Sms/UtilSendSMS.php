<?php

namespace App\Jobs\Sms;

use App\Jobs\BaseJob;

use Illuminate\Support\Facades\Mail;

class UtilSendSMS extends BaseJob
{
	public function sms($to, $text)
	{
		$username = config('smsc.username');
		$password = config('smsc.password');
		$senderId = config('smsc.sender_id');
		$smsType = config('smsc.route');
		$text = rawurlencode($text);

		if ($to && strlen($to) == 10)
		{
			$smsGatewayApi = "http://smsc.biz/httpapi/send?username=" . $username .
				"&password=" . $password .
				"&sender_id=" . $senderId .
				"&route=" . $smsType .
				"&phonenumber=" . $to .
				"&message=" . $text;

			$ch = curl_init($smsGatewayApi);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);

			// sms sending error occurred
			if ($result < 0)
			{
				$this->triggerErrorEmail([
					                         'to'         => $to,
					                         'text'       => $text,
					                         'error_code' => $result
				                         ]);
			}
		}
		else
		{
			$this->triggerErrorEmail([
				                         'to'         => $to,
				                         'text'       => $text,
				                         'error_code' => 'Invalid'
			                         ]);
		}
	}

	private function triggerErrorEmail($data)
	{
		$data = [
			'name'    => config('evibe.contact.admin.name'),
			'subject' => "[SMS Error] SMS sending failed - " . $data['to'] . " - " . $data['error_code'],
			'body'    => isset($data['text']) ? $data['text'] : 'SMS Sending Failed, check and fix at the earliest. Data unavailable. Number will be in the subject'
		];

		Mail::send('emails.report.admin.error', $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', 'API Error');
			$message->to(config('evibe.contact.tech.email'), config('evibe.contact.admin.name'));
			$message->subject($data['subject']);
		});
	}
}