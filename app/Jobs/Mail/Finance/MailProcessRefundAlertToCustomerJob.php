<?php

namespace App\Jobs\Mail\Finance;

use App\Http\Controllers\BaseController;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailProcessRefundAlertToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$data['sub'] = 'Refund of INR ' . $data['refundAmount'] . ' is processed';

		Mail::send('emails.finance.process-refund-customer', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.accounts.group'), 'Evibe.in Payments')
			  ->to($data['customerEmail'])
			  ->cc(config('evibe.contact.accounts.group'))
			  ->replyTo(config('evibe.contact.accounts.group'))
			  ->subject($data['sub']);
		});
	}
}