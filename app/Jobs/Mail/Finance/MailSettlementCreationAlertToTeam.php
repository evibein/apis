<?php

namespace App\Jobs\Mail\Finance;

use App\Http\Controllers\BaseController;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailSettlementCreationAlertToTeam extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$data['sub'] = 'New settlements have been created';

		Mail::send('emails.finance.settlement-create-team', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Evibe.in')
			  ->to(config('evibe.contact.accounts.group'))
			  ->replyTo(config('evibe.contact.accounts.group'))
			  ->subject($data['sub']);
		});
	}
}