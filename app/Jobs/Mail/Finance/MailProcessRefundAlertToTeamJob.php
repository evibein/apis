<?php

namespace App\Jobs\Mail\Finance;

use App\Http\Controllers\BaseController;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailProcessRefundAlertToTeamJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$data['sub'] = 'Amount refunded for party on '. $data['partyDateTime'];

		Mail::send('emails.finance.process-refund-team', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Evibe.in')
			  ->to(config('evibe.contact.accounts.group'))
			  ->cc(config('evibe.contact.enquiry.group'))
			  ->replyTo(config('evibe.contact.accounts.group'))
			  ->subject($data['sub']);
		});
	}
}