<?php

namespace App\Jobs\Mail\Finance;

use App\Http\Controllers\BaseController;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailInitiateCancellationRefundToTeamJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$data['sub'] = 'Cancellation Refund of INR ' . $data['refundAmount'] . ' is initiated';

		Mail::send('emails.finance.initiate-cancellation-refund', ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.system_alert_email'), 'System Alert')
			  ->to(config('evibe.contact.accounts.group'))
			  ->cc(config('evibe.contact.operations.alert_no_action_email'))
			  ->replyTo(config('evibe.contact.enquiry.group'))
			  ->subject($data['sub']);
		});
	}
}