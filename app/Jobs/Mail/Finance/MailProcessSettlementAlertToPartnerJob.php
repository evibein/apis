<?php

namespace App\Jobs\Mail\Finance;

use App\Http\Controllers\BaseController;
use App\Http\Models\Settlements\PartnerAdjustments;
use App\Http\Models\Settlements\ScheduleSettlementBookings;
use App\Http\Models\TicketBooking;
use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailProcessSettlementAlertToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$data['sub'] = 'Week settlement (' . $data['settlementStartDate'] . ' - ' . $data['settlementEndDate'] . ') - #' . $data['invoiceNumber'];

		// evibe mail requirements
		$data['date'] = date('d/m/y h:i A', time());
		$data['evibePhone'] = config('evibe.contact.accounts.phone');
		$data['evibeEmail'] = config('evibe.contact.accounts.group');
		$data['startTime'] = config('evibe.contact.company.working.start_time');
		$data['endTime'] = config('evibe.contact.company.working.end_time');

		Mail::send('emails.finance.process-settlement-partner', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.accounts.group'), 'Evibe.in Payments')
			  ->to($data['partnerEmail'])
			  ->cc($data['ccAddress'])
			  ->replyTo(config('evibe.contact.accounts.group'))
			  ->subject($data['sub'])
			  ->attach(config('evibe.hosts.gallery').'/invoices/'.$data['invoiceUrl']);
		});
	}
}