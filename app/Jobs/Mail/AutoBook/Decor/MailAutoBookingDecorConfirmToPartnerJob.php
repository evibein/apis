<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 31/12/16
 * Time: 3:32 PM
 */

namespace App\Jobs\Mail\AutoBook\Decor;

use App\Jobs\Mail\BaseDecorMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorConfirmToPartnerJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$subAutoBooking = $data['booking']['provider']['person'] . ', you have a confirmed decor booking for ';
		$subAutoBooking .= $data['booking']['checkInDate'] . ' from ' . $data['customer']['name'];

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;

		if ($mailData['booking']['provider']['email'])
		{
			Mail::send('emails.auto-book.accept.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.enquiry.group'), 'Team Evibe.in')
				  ->to($mailData['booking']['provider']['email'])
				  ->cc($mailData['vendorCcAddresses'])
				  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($mailData['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.accept.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.enquiry.group'), 'Team Evibe.in')
				  ->to(config('evibe.contact.enquiry.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($mailData['sub']);
			});
		}
	}
}
