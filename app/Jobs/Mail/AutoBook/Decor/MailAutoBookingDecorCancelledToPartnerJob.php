<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 31/12/16
 * Time: 3:32 PM
 */

namespace App\Jobs\Mail\AutoBook\Decor;

use App\Jobs\Mail\BaseDecorMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorCancelledToPartnerJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$subAutoBooking = $data['booking']['provider']['person'] . ', ' . $data['booking']['name'] . ' UNAVAILABLE confirmation for ' . $data['booking']['checkInDate'];

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;

		$mailData['ccAddresses'] = $mailData['vendorCcAddresses'];
		array_push($mailData['ccAddresses'], config('evibe.contact.enquiry.group'));

		if ($mailData['booking']['provider']['email'])
		{
			Mail::send('emails.auto-book.reject.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.enquiry.group'), 'Team Evibe.in')
				  ->to($mailData['booking']['provider']['email'])
				  ->cc($mailData['ccAddresses'])
				  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($mailData['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.reject.decor.alert-partner', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.enquiry.group'), 'Team Evibe.in')
				  ->to(config('evibe.contact.business.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->bcc(config('evibe.contact.operations.alert_no_action_email'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $mailData['sub']);
			});
		}
	}
}