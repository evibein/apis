<?php
/**
 * Created by PhpStorm.
 * User: vikash
 * Date: 31/12/16
 * Time: 3:32 PM
 */

namespace App\Jobs\Mail\AutoBook\Decor;

use App\Jobs\Mail\BaseDecorMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingDecorConfirmToCustomerJob extends BaseDecorMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$subAutoBooking = 'Your Event Is Booked';
		$subAutoBooking .= isset($data["ticketId"]) ? $this->getEnquiryIdForEmailSubject($data["ticketId"]) : "";

		$mailData = $this->getDecorMailData($data);
		$mailData['sub'] = $subAutoBooking;
		$cc = config('evibe.contact.enquiry.group');
		$altEmail = $data['customer']['altEmail'];
		$mailData['cc'] = $data['customer']['altEmail'] ? array_push($altEmail, $cc) : $cc;

		if ($mailData['customer']['email'])
		{
			Mail::send('emails.auto-book.accept.decor.alert-customer', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.enquiry.group'), 'Evibe Receipts')
				  ->to($mailData['customer']['email'])
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($mailData['sub']);
			});

			$this->EmailReceiptSuccessToTeam($data["ticketId"], "Booking Email was sent successfully to the customer", "Booking Email Delivered Successfully");
		}
		else
		{
			Mail::send('emails.auto-book.accept.decor.alert-customer', ['data' => $mailData], function ($m) use ($mailData) {
				$m->from(config('evibe.contact.company.email'), 'Evibe.in')
				  ->to(config('evibe.contact.enquiry.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $mailData['sub']);
			});
		}
	}
}
