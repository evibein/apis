<?php

namespace App\Jobs\Mail\AutoBook;

use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingOrderConfirmationToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job - Send receipt email
	 * Sent to customer with vendor, handler and admin in cc
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		//$bookingName = $this->getShortName($data['booking']['name'], 20);
		$subAutoBooking = 'Your Event Is Booked';
		$subAutoBooking .= isset($data["ticketId"]) ? $this->getEnquiryIdForEmailSubject($data["ticketId"]) : "";

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);
		$data['sub'] = $subAutoBooking;

		if ($data['customer']['email'])
		{
			Mail::send('emails.auto-book.accept.alert-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.enquiry.group'), 'Evibe Receipts')
				  ->to($data['customer']['email'])
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($data['sub']);
			});

			$this->EmailReceiptSuccessToTeam($data["ticketId"], "Booking Email was sent successfully to the customer", "Booking Email Delivered Successfully");
		}
		else
		{
			Mail::send('emails.auto-book.accept.alert-customer', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.enquiry.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}
