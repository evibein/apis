<?php

namespace App\Jobs\Mail\AutoBook;

use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingOrderCancelledToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		$partyDateTime = $data['partyDateTime'];
		$subAutoBooking = $data['person'] . ", order UNAVAILABLE confirmation for " . $data['customer']['name'] . "'s party on " . $partyDateTime;

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);
		$data['sub'] = $subAutoBooking;

		if ($data['email'])
		{
			Mail::send('emails.auto-book.reject.alert-partner', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to($data['email'])
				  ->cc($data['vendorCcAddresses'])
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.reject.alert-partner', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.customer.group'))
				  ->cc(config('evibe.contact.business.group'))
				  ->bcc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.business.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}
}