<?php

namespace App\Jobs\Mail\AutoBook;

use App\Jobs\Mail\BaseMailerJob;
use Illuminate\Support\Facades\Mail;

class MailAutoBookingOrderConfirmationToPartnerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		// @see: comes here only if booking data exists
		//$bookingName = $this->getShortName($data['booking']['name'], 20);
		$partyDateTime = $data['partyDateTime'];
		$subAutoBooking = '[#' . $data['ticketId'] . '] ' . $data['person'] . ', you have a confirmed order for ' . $partyDateTime . ' from ' . $data['customer']['name'];

		$mailData = $this->getABMailData();
		$data = array_merge($data, $mailData);
		$data['sub'] = $subAutoBooking;

		if ($data['email'])
		{
			Mail::send('emails.auto-book.accept.alert-partner', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to($data['email'])
				  ->cc($data['vendorCcAddresses'])
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.auto-book.accept.alert-partner', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.company.email'), 'Team Evibe')
				  ->to(config('evibe.contact.enquiry.group'))
					//->cc(config('evibe.contact.operations.group'))
                  ->bcc(config('evibe.contact.tech.group'))
				  ->replyTo(config('evibe.contact.enquiry.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}
