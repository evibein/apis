<?php

namespace App\Jobs\Mail\Enquiry;

use App\Jobs\Mail\UtilEmail;
use Illuminate\Support\Facades\Mail;

class WorkflowEnquiryAlertToCustomer extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['sub'] = ucfirst($data['customerName']) . ', thanks for placing your enquiry'; // todo:change

		$data['view'] = 'emails.enquiry.workflow.notify-customer';
		$data['from'] = config('evibe.contact.enquiry.group');
		$data['fromText'] = 'Team Evibe.in';

		if ($data['customerEmail'])
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data)
			{
				$m->from($data['from'], $data['fromText'])
				  ->to($data['customerEmail'])
				  ->cc(config('evibe.contact.enquiry.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data)
			{
				$m->from($data['from'], 'Invalid Email Address')
				  ->to(config('evibe.contact.enquiry.group'))
				  ->subject('Failed Sub:' . $data['sub']);
			});
		}
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}