<?php

namespace App\Jobs\Mail\Enquiry;

use App\Jobs\Mail\UtilEmail;
use Illuminate\Support\Facades\Mail;

class WorkflowEnquiryAlertToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['sub'] = isset($data['cityName']) ? '[' . $data['cityName'] . '] ' : '';
		$data['sub'] .= isset($data['eventName']) ? '[' . $data['eventName'] . '] ' : '';
		$data['sub'] .= 'Party on ' . $data['partyDate'] . ' from ' . $data['customerName'];

		$data['view'] = 'emails.enquiry.workflow.alert-team';
		$data['from'] = config('evibe.contact.company.email');
		$data['fromText'] = 'Workflow Enquiry';

		Mail::send($data['view'], ['data' => $data], function ($m) use ($data)
		{
			$m->from($data['from'], $data['fromText'])
			  ->to(config('evibe.contact.enquiry.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}