<?php

namespace App\Jobs\Mail;

use App\Http\Models\CheckoutFields;
use App\Http\Models\CheckoutFieldValue;
use App\Http\Models\Decor;
use Illuminate\Support\Facades\Log;

class BaseDecorMailerJob extends BaseMailerJob
{
	public function getDecorMailData($data)
	{
		$decor = Decor::find($data['booking']['itemMapId']);
		if (!$decor)
		{
			Log::error('error while getting decor');
			return $data;
		}
		$baseBookingAmount = $decor->min_price;

		// get decor checkout fields with values
		$decorFields = $this->getDecorCheckoutFields($data['additional']['eventId'], $data['booking']['typeTicketBookingId']);
		$decorCheckoutFieldsWithValues = $this->getDecorCheckoutFieldsWithValuesInArray($decorFields, $data['ticketId']);

		$emailData = [
			'date'           => date('d/m/y h:i A'),
			'evibePhone'     => config('evibe.contact.company.phone'),
			'delivery'       => $data['booking']['bookingAmount'] - $baseBookingAmount,
			'baseAmt'        => $this->formatPrice($baseBookingAmount),
			'deliveryAmt'    => $this->formatPrice($data['booking']['bookingAmount'] - $baseBookingAmount),
			'bookAmt'        => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'          => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'         => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'checkoutFields' => $decorCheckoutFieldsWithValues,
			'bizHead'        => config('evibe.contact.business.phone'),
			'evibeLink'      => config('evibe.host'),
			'evibeEmail'     => config('evibe.contact.company.email'),
			'startTime'      => config('evibe.contact.company.working.start_time'),
			'endTime'        => config('evibe.contact.company.working.end_time'),
		];

		return array_merge($data, $emailData);
	}

	public function getDecorCheckoutFields($eventId, $typeTicketBookingId)
	{
		$checkoutFields = CheckoutFields::where('type_ticket_booking_id', $typeTicketBookingId)
		                                ->where(function ($query) use ($eventId)
		                                {
			                                $query->where('event_id', $eventId)
			                                      ->orWhereNull('event_id');
		                                })
		                                ->where('is_crm', 0)
		                                ->where('is_auto_booking', 1)
		                                ->get();

		return $checkoutFields;
	}

	public function getDecorCheckoutFieldsWithValuesInArray($decorFields, $ticketId)
	{
		$decorCheckoutFields = [];

		foreach ($decorFields as $decorField)
		{
			$decorCheckoutField = [
				$decorField->identifier => $this->getDecorCheckoutValue($ticketId, $decorField->id)
			];
			$decorCheckoutFields = array_merge($decorCheckoutFields, $decorCheckoutField);
		}

		return $decorCheckoutFields;
	}

	private function getDecorCheckoutValue($ticketId, $id)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $id,
		                                           ])->first();

		return $checkoutValue->value;
	}
}