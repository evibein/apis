<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class MailPartnerAppLeadToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		Mail::send('emails.tickets.partnerapp', $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', 'Ticket Alert');
			$message->to(config('evibe.contact.enquiry.group'));
			$message->subject($data['subject']);
		});
	}
}