<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class InvalidEmailAddressAlertToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		switch ($data['roleId'])
		{
			case config('evibe.role.customer'):
				$data['sub'] = "Invalid Email address detected for a customer";
				$data['view'] = 'emails.report.team.invalid-customer-email';
				$data['to'] = config('evibe.contact.enquiry.group');
				break;

			case config('evibe.role.planner'):
			case config('evibe.role.venue'):
			case config('evibe.role.artist'):
				$data['sub'] = "Invalid Email address detected for a partner";
				$data['view'] = 'emails.report.team.invalid-partner-email';
				$data['to'] = config('evibe.contact.business.group');
				break;

			default:
				$data['sub'] = "Invalid Email address detected";
				$data['view'] = 'emails.report.team.invalid-email';
				$data['to'] = config('evibe.contact.enquiry.group');
				break;
		}

		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.company.email'), 'Invalid Email Alert')
			  ->to($data['to'])
			  ->cc(config('evibe.contact.tech.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}