<?php

namespace App\Jobs\Mail\Booking;

use App\Jobs\Mail\UtilEmail;
use Illuminate\Support\Facades\Mail;

class MailBookingCancelToPartnerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['date'] = date('d/m/y h:i A', time());
		$data['evibePhone'] = config('evibe.contact.company.phone');
		$data['bizHead'] = config('evibe.contact.business.phone');
		$data['evibeLink'] = config('evibe.live.host');
		$data['evibeEmail'] = config('evibe.contact.company.email');
		$data['startTime'] = config('evibe.contact.company.working.start_time');
		$data['endTime'] = config('evibe.contact.company.working.end_time');

		//$data['sub'] = $data['partnerName'] . ', ' . ($data['optionName'] ? $data['optionName'] : 'booking') . ' UNAVAILABLE confirmation for ' . $data['partyDateTime'];
		$data['sub'] = '[#' . $data['ticketId'] . '] Order Rejected - ' . $data['partyDateTime'] . ' - ' . ($data['optionName'] ? $data['optionName'] : 'booking');
		$data['view'] = 'emails.cancellation.booking.notify-partner';

		if ($data['partnerEmail'])
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.business.group'), 'Evibe.in Business')
				  ->to($data['partnerEmail'])
				  ->cc(config('evibe.contact.business.group'))
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.business.group'), 'Evibe.in Business')
				  ->to(config('evibe.contact.business.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}