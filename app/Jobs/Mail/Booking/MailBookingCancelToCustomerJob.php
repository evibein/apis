<?php

namespace App\Jobs\Mail\Booking;

use App\Jobs\Mail\UtilEmail;
use Illuminate\Support\Facades\Mail;

class MailBookingCancelToCustomerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data['date'] = date('d/m/y h:i A', time());
		$data['evibePhone'] = config('evibe.contact.company.phone');
		$data['evibePlainPhone'] = config('evibe.contact.company.plain_phone');
		$data['bizHead'] = config('evibe.contact.business.phone');
		$data['evibeLink'] = config('evibe.live.host');
		$data['evibeEmail'] = config('evibe.contact.company.email');
		$data['startTime'] = config('evibe.contact.company.working.start_time');
		$data['endTime'] = config('evibe.contact.company.working.end_time');

		// @see: comments below
		//if (isset($data['bookedBookings']) && $data['bookedBookings'])
		//{
		//	$data['sub'] = '[Partial Order Cancellation] ' . ucfirst($data['customerName']) . ', ' . ($data['optionName'] ? $data['optionName'] : 'booking') . ' cancelled for ' . $data['partyDateTime'];
		//}
		//else
		//{
		//	$data['sub'] = ucfirst($data['customerName']) . ', ' . ($data['optionName'] ? $data['optionName'] : 'booking') . ' cancelled for ' . $data['partyDateTime'];
		//}

		// @see: single thread cancellations (even for partial cancellations) - changed while make email communication optimisations - 07/03/2020 @NGK
		$data['sub'] = '[#' . $data['ticketId'] . '] ' . ($data['optionName'] ? $data['optionName'] : 'booking') . ' cancelled for ' . $data['partyDateTime'];

		$data['view'] = 'emails.cancellation.booking.notify-customer';
		$data['from'] = config('evibe.contact.enquiry.group');
		$data['fromText'] = isset($data['handlerName']) && $data['handlerName'] ? ucfirst($data['handlerName']) . ' from Evibe.in' : 'Team Evibe.in';

		if ($data['customerEmail'])
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from($data['from'], $data['fromText'])
				  ->to($data['customerEmail'])
					//->cc(config('evibe.contact.enquiry.group'))
                  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
				$m->from($data['from'], 'Invalid Email Address')
				  ->to(config('evibe.contact.enquiry.group'))
				  ->subject('Failed Sub:' . $data['sub']);
			});
		}
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}