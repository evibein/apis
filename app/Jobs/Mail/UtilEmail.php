<?php

namespace App\Jobs\Mail;

use App\Jobs\BaseJob;
use Aws\Laravel\AwsFacade as AWS;

class UtilEmail extends BaseJob
{
	public function sendMail($data)
	{
		$ses = AWS::createClient('Ses');

		if ($data['to'])
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => array_key_exists('replyTo', $data) ? $data['replyTo'] : [],
				                'Destination'      => [
					                'ToAddresses'  => $data['to'],
					                'CcAddresses'  => array_key_exists('cc', $data) ? $data['cc'] : [],
					                'BccAddresses' => array_key_exists('bcc', $data) ? $data['bcc'] : []
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
		else
		{
			$ses->sendEmail([
				                'Source'           => $data['from'],
				                'ReplyToAddresses' => config('evibe.contact.enquiry.group'),
				                'Destination'      => [
					                'ToAddresses'  => config('evibe.contact.enquiry.group'),
					                'CcAddresses'  => config('evibe.contact.tech.group'),
					                'BccAddresses' => []
				                ],
				                'Message'          => [
					                'Subject' => [
						                'Data' => '[Invalid Email Address]. Sub: ' . $data['sub']
					                ],
					                'Body'    => [
						                'Html' => [
							                'Data' => $data['body']
						                ]
					                ]
				                ]
			                ]);
		}
	}
}