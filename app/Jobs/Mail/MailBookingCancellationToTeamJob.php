<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class MailBookingCancellationToTeamJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		//$data['from'] = ucfirst($data['handlerName']) . '<enquiry@evibe.in>';
		$data['sub'] = "A customer has cancelled a ticket booking [Ticket Id: " . $data['ticketId'] . "]";

		Mail::send('emails.cancellation.customer-cancellation-team', ['data' => $data], function ($m) use ($data)
		{
			$m->from(config('evibe.contact.company.email'), 'Booking Cancellation')
			  ->to(config('evibe.contact.enquiry.group'))
			  ->subject($data['sub']);
		});
	}
}