<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class MailReferralSuccessToPartnerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		if ($data['partnerEmail'])
		{
			Mail::send('emails.tickets.referralsuccess', $data, function ($message) use ($data) {
				$message->from('ping@evibe.in', 'Referral Alert');
				$message->to($data['partnerEmail']);
				$message->subject($data['subject']);
			});
		}
		else
		{
			Mail::send('emails.tickets.referralsuccess', $data, function ($message) use ($data) {
				$message->from('ping@evibe.in', 'Referral Alert');
				$message->to(config('evibe.contact.enquiry.group'));
				$message->cc(config('evibe.contact.tech.group'));
				$message->subject('[Invalid Email Address]. Sub: ' . $data['subject']);
			});
		}
	}
}