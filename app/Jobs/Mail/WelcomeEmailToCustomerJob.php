<?php

namespace App\Jobs\Mail;

use App\Http\Models\TypeEvent;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class WelcomeEmailToCustomerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$ticket = $data['ticket'];
		$handler = $ticket->handler;

		$event = TypeEvent::find($ticket->event_id);
		$data['customerName'] = $ticket->name ? $ticket->name : 'Customer';
		$data['partyDateTime'] = $ticket->event_date ? date('d/m/y h:i A', $ticket->event_date) : null;
		$data['eventName'] = $event ? $event->name : null;
		$data['customerPhone'] = $ticket->phone;
		$data['enquiryId'] = $ticket->enquiry_id;
		$data['from'] = $handler ? $handler->username : config('evibe.contact.enquiry.group');

		$data['view'] = 'emails.util.welcome-email';
		$data['to'] = $data['emailId'];
		$data['sub'] = ucfirst($data['customerName']) . ', thanks for your time';

		// @see: if else case not required
		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from($data['from'], 'Evibe.in')
			  ->to($data['to'])
			  ->cc(config('evibe.contact.enquiry.group'))
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}