<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class MailRecommendationsToCustomerJob extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		//$data['from'] = ucfirst($data['handlerName']) . '<enquiry@evibe.in>';
		$data['sub'] = "Regarding your event on " . $data["partyDate"] . " - #" . $data["enquiryId"];

		if ($data['to'])
		{
			Mail::send('emails.tickets.recommendations', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.enquiry.group'), ucfirst($data['handlerName']) . " from Evibe")
				  ->to($data['to'])
				  ->cc($data['ccAddresses'])
				  ->subject($data['sub']);
			});
		}
		else
		{
			Mail::send('emails.tickets.recommendations', ['data' => $data], function ($m) use ($data) {
				$m->from(config('evibe.contact.enquiry.group'), ucfirst($data['handlerName']))
				  ->to(config('evibe.contact.enquiry.group'))
				  ->cc(config('evibe.contact.tech.group'))
				  ->subject('[Invalid Email Address]. Sub: ' . $data['sub']);
			});
		}
	}
}