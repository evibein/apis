<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class InvalidEmailUpdateAlertToTeam extends UtilEmail
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;
		$data['view'] = 'emails.report.team.update-invalid-email';
		$data['to'] = config('evibe.contact.enquiry.group');
		$data['sub'] = 'A customer has provided correct email address';

		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from(config('evibe.contact.enquiry.group'), 'Updated Invalid Email')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}