<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class MailProductBookingStatusUpdateToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{

		$data = $this->data;
		$data['view'] = 'emails.util.product-booking-status-update';
		$data = array_merge($data, $this->getABMailData());
		$data['from'] = config('evibe.contact.operations.email');
		$data['to'] = $data['customerEmail'];
		$data['sub'] = 'Your Order #' . $data['productBookingId'] . ' is ' . $data['status'];
		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from($data['from'], 'Party In A Box')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}