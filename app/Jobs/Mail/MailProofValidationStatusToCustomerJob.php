<?php

namespace App\Jobs\Mail;

use Illuminate\Support\Facades\Mail;

class MailProofValidationStatusToCustomerJob extends BaseMailerJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$data = $this->data;

		$data = array_merge($data, $this->getABMailData());

		$data['view'] = 'emails.util.proof-status-customer';
		$data['from'] = config('evibe.contact.operations.email');
		$data['to'] = $data['customerEmail'];
		if($data['proofStatus'])
		{
			$data['sub'] = ucfirst($data['customerName']) . ', your id proof validation is successful';
		}
		else
		{
			$data['sub'] = ucfirst($data['customerName']) . ', your id proof validation failed';
		}

		Mail::send($data['view'], ['data' => $data], function ($m) use ($data) {
			$m->from($data['from'], 'Evibe.in')
			  ->to($data['to'])
			  ->subject($data['sub']);
		});
	}

	public function failed(\Exception $exception)
	{
		$data['exception'] = $exception;
		$this->sendFailedJobsToTeam($data);
	}
}