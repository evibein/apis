<?php

namespace App\Jobs\Mail;

use App\Http\Models\CheckoutFields;
use App\Http\Models\CheckoutFieldValue;
use App\Http\Models\Ticket;
use App\Http\Models\TypeService;

class BaseEntMailerJob extends BaseMailerJob
{
	public function getEntMailData($data)
	{
		$ent = TypeService::find($data['booking']['itemMapId']);
		$baseBookingAmount = $ent->min_price;

		$ticket = Ticket::find($data['ticketId']);
		$occasionId = $ticket->event_id;

		// get ent checkout fields with values
		$entFields = $this->getEntCheckoutFields($occasionId, config('evibe.ticket.type_booking.entertainment'));
		$entCheckoutFieldsWithValues = $this->getEntCheckoutFieldsWithValuesInArray($entFields, $data['ticketId']);

		$emailData = [
			'date'           => date('d/m/y h:i A'),
			'partyTime'      => date('h:i A', $data['booking']['partyDateTime']),
			'evibePhone'     => config('evibe.contact.company.phone'),
			'delivery'       => $data['booking']['bookingAmount'] - $baseBookingAmount,
			'baseAmt'        => $this->formatPrice($baseBookingAmount),
			'deliveryAmt'    => $this->formatPrice($data['booking']['bookingAmount'] - $baseBookingAmount),
			'bookAmt'        => $this->formatPrice($data['booking']['bookingAmount']),
			'advPd'          => $this->formatPrice($data['booking']['advanceAmount']),
			'balAmt'         => $this->formatPrice($data['booking']['bookingAmount'] - $data['booking']['advanceAmount']),
			'checkoutFields' => $entCheckoutFieldsWithValues,
			'bizHead'        => config('evibe.contact.business.phone'),
			'evibeLink'      => config('evibe.host'),
			'evibeEmail'     => config('evibe.contact.company.email'),
			'startTime'      => config('evibe.contact.company.working.start_time'),
			'endTime'        => config('evibe.contact.company.working.end_time'),
		];

		return array_merge($data, $emailData);
	}

	public function getEntCheckoutFields($eventId, $typeTicketBookingId)
	{
		$checkoutFields = CheckoutFields::where('type_ticket_booking_id', $typeTicketBookingId)
		                                ->where(function ($query) use ($eventId) {
			                                $query->where('event_id', $eventId)
			                                      ->orWhereNull('event_id');
		                                })
		                                ->where('is_crm', 0)
		                                ->where('is_auto_booking', 1)
		                                ->get();

		return $checkoutFields;
	}

	public function getEntCheckoutFieldsWithValuesInArray($entFields, $ticketId)
	{
		$entCheckoutFields = [];

		foreach ($entFields as $entField)
		{
			$entCheckoutField = [
				$entField->identifier => $this->getEntCheckoutValue($ticketId, $entField->id)
			];
			$entCheckoutFields = array_merge($entCheckoutFields, $entCheckoutField);
		}

		return $entCheckoutFields;
	}

	private function getEntCheckoutValue($ticketId, $id)
	{
		$checkoutValue = CheckoutFieldValue::where([
			                                           'ticket_id'         => $ticketId,
			                                           'checkout_field_id' => $id,
		                                           ])->first();

		if($checkoutValue)
		{
			return $checkoutValue->value;
		}
		return null;
	}
}