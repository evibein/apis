<?php
/**
 * Created by PhpStorm.
 * User: saachi
 * Date: 12/01/17
 * Time: 6:05 PM
 */

namespace App\Jobs\Notification;

use GuzzleHttp\Client;
use Illuminate\Support\Facades\Log;

class ReferralWebPushNotificationToTeamJob extends PartnerWebPushNotificationToTeamJob
{
	private $existingTicket;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($existingTicket)
	{
		$this->existingTicket = $existingTicket;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle()
	{
		$existingTicket = $this->existingTicket;

		if ($existingTicket)
		{
			// send web push notification to team
			$dashLink = config('evibe.hosts.dash') . '/tickets/' . $existingTicket->id;
			$subject = "[Follow up] Please follow up immediately for the existing ticket";
			$segments = array_merge(config("onesignal.web.segments.bd"), config("onesignal.web.segments.admins"));

			$notificationData = [
				'title'       => "Follow up on ticket",
				'message'     => $subject,
				'segments'    => $segments,
				'buttonUrl'   => $dashLink,
				'buttonText'  => 'Ticket Details',
				'buttonId'    => 'existing_ticket',
				'messageFrom' => 'partner app referral'
			];

			$this->sendWebPushNotificationForPartnerApp($notificationData);
		}
	}

}