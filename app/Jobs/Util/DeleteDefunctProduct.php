<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Log;

class DeleteDefunctProduct extends BaseJob
{
	private $dirs;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($dirs)
	{
		$this->dirs = $dirs;
	}

	public function handle()
	{
		$dirs = $this->dirs;

		foreach ($dirs as $dir)
		{
			if (is_dir($dir))
			{
				try
				{
					$this->deleteFiles($dir);
				} catch (\Exception $e)
				{
					Log::info("Catch ERROR: " . $e->getMessage());
				}
			}
		}
	}

	private function deleteFiles($target)
	{
		if (is_dir($target))
		{
			$files = glob($target . '*', GLOB_MARK); //GLOB_MARK adds a slash to directories returned

			foreach ($files as $file)
			{
				$this->deleteFiles($file);
			}

			rmdir($target);
		}
		elseif (is_file($target))
		{
			unlink($target);
		}
	}

	public function failed(\Exception $e)
	{
		Log::info("JOB FAILED ERROR: " . $e->getMessage());
	}
}