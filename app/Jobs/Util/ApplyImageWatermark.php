<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Log;

class ApplyImageWatermark extends BaseJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		foreach ($data as $productData)
		{
			foreach ($productData as $imagesData)
			{
				foreach ($imagesData as $imageData)
				{
					// Image path
					$parentDirs = $imageData["root"];

					if (!is_null($parentDirs) && $parentDirs && file_exists($parentDirs))
					{
						// It will give filename, basename, extension, dir in array structure
						$pathInfo = pathinfo($parentDirs);
						$fileName = isset($pathInfo["filename"]) ? $pathInfo["filename"] : false;
						$extension = isset($pathInfo["extension"]) ? $pathInfo["extension"] : false;
						$directory = isset($pathInfo["dirname"]) ? $pathInfo["dirname"] : false;

						if (!$fileName || !$extension || !$directory)
						{
							continue;
						}

						try
						{
							// @see: If already file exists with filename_original, it will override it
							if (!copy($directory . '/' . $fileName . '.' . $extension, $directory . '/' . $fileName . '_original.' . $extension))
							{
								Log::error("_original file is not created for file(path):" . $parentDirs);
							}
							else
							{
								try
								{
									$image = false;

									// Designate image depending on extension
									if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG')
									{
										$image = imagecreatefromjpeg($parentDirs);
									}
									elseif ($extension == 'png' || $extension == 'PNG')
									{
										$image = imagecreatefrompng($parentDirs);
									}
									elseif ($extension == 'gif' || $extension == 'GIF')
									{
										$image = imagecreatefromgif($parentDirs);
									}

									if (!$image)
									{
										Log::error("unable to create image from given extension");
										continue;
									}

									$imageDimensions = getimagesize($parentDirs);
									$stampData = $this->getWatermarkImagePosition($imageDimensions);

									if (isset($stampData["stamp"]))
									{
										$stamp = imagecreatefrompng($stampData["stamp"]);

										// Set the margins for the stamp and get the height/width of the stamp image
										$sx = imagesx($stamp);
										$sy = imagesy($stamp);
										$imageWidth = isset($imageDimensions[0]) ? $imageDimensions[0] : 0;
										$imageHeight = isset($imageDimensions[1]) ? $imageDimensions[1] : 0;

										// Copy the stamp image onto our photo using the margin offsets and the photo
										// width to calculate positioning of the stamp.
										imagecopy(
											$image,
											$stamp,
											(2 * $imageWidth) / 25,
											$imageHeight - $sy - ($imageHeight - $sy) / 2,
											0,
											0,
											$sx,
											$sy
										);

										// Remove the original file
										unlink($parentDirs);

										// rename the copied watermark image to original image
										if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'JPG' || $extension == 'JPEG')
										{
											imagejpeg($image, $parentDirs);
										}
										if ($extension == 'png' || $extension == 'PNG')
										{
											imagepng($image, $parentDirs);
										}
										if ($extension == 'gif' || $extension == 'GIF')
										{
											imagegif($image, $parentDirs);
										}

										chmod($parentDirs, 0777);
										imagedestroy($image);
									}
									else
									{
										Log::error("error while getting watermark image for file(path)" . $parentDirs);
									}
								} catch (\Exception $e)
								{
									Log::error("CATCH ERROR: while watermarking image for file(path)" . $parentDirs);
									Log::error($e->getMessage());
								}
							}
						} catch (\Exception $e)
						{
							Log::error("CATCH ERROR: while watermarking image for file(path)" . $parentDirs);
							Log::error($e->getMessage());
						}

					}
					else
					{
						Log::error("Image URL not found for file(path):" . $parentDirs);
					}
				}
			}
		}
	}

	public function getWatermarkImagePosition($originalImageDimensions)
	{
		$calculatedWidth = 0;
		$stamp = null;

		$imageWidth = isset($originalImageDimensions[0]) ? $originalImageDimensions[0] : 0;
		$imageHeight = isset($originalImageDimensions[1]) ? $originalImageDimensions[1] : 0;
		$stamp = config('evibe.root.gallery_queue') . "/watermarks/new/";
		if ($imageHeight > $imageWidth)
		{
			$stamp = config('evibe.root.gallery_queue') . "/watermarks/vertical/";
		}

		if (($imageWidth != 0) && ($imageHeight != 0))
		{
			if ($imageWidth >= 4000)
			{
				$calculatedWidth = 530;
			}
			elseif ($imageWidth >= 3500)
			{
				$calculatedWidth = 470;
			}
			elseif ($imageWidth >= 3000)
			{
				$calculatedWidth = 400;
			}
			elseif ($imageWidth >= 2500)
			{
				$calculatedWidth = 330;
			}
			elseif ($imageWidth >= 2000)
			{
				$calculatedWidth = 260;
			}
			elseif ($imageWidth >= 1400)
			{
				$calculatedWidth = 190;
			}
			elseif ($imageWidth >= 1200)
			{
				$calculatedWidth = 160;
			}
			elseif ($imageWidth >= 1000)
			{
				$calculatedWidth = 135;
			}
			elseif ($imageWidth >= 900)
			{
				$calculatedWidth = 120;
			}
			elseif ($imageWidth >= 800)
			{
				$calculatedWidth = 110;
			}
			elseif ($imageWidth >= 700)
			{
				$calculatedWidth = 100;
			}
			elseif ($imageWidth >= 500)
			{
				$calculatedWidth = 70;
			}
			elseif ($imageWidth >= 300)
			{
				$calculatedWidth = 40;
			}
			elseif ($imageWidth < 300)
			{
				$calculatedWidth = 40;
			}

			$stamp .= "$calculatedWidth.png";
		}

		return [
			"stamp" => $stamp
		];
	}

	public function failed(\Exception $e)
	{
		Log::info("JOB FAILED MESSAGE: " . $e->getMessage());
		Log::info("JOB FAILED TRACE: " . $e->getTraceAsString());

		$data['exception'] = $e;
		$this->sendFailedJobsToTeam($data);
	}
}