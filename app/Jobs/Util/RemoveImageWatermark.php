<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Log;

class RemoveImageWatermark extends BaseJob
{
	private $data;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($data)
	{
		$this->data = $data;
	}

	public function handle()
	{
		$data = $this->data;

		foreach ($data as $productData)
		{
			foreach ($productData as $imagesData)
			{
				foreach ($imagesData as $imageData)
				{
					// Image path
					$parentDirs = $imageData["root"];

					if (!is_null($parentDirs) && $parentDirs && file_exists($parentDirs))
					{
						// It will give filename, basename, extension, dir in array structure
						$pathInfo = pathinfo($parentDirs);
						$fileName = isset($pathInfo["filename"]) ? $pathInfo["filename"] : false;
						$extension = isset($pathInfo["extension"]) ? $pathInfo["extension"] : false;
						$directory = isset($pathInfo["dirname"]) ? $pathInfo["dirname"] : false;

						if (!$fileName || !$extension || !$directory)
						{
							continue;
						}

						try
						{
							if (file_exists($directory . '/' . $fileName . '_original.' . $extension))
							{
								if (!unlink($directory . '/' . $fileName . '.' . $extension))
								{
									Log::error("Unable to delete the file(path):" . $parentDirs);
								}
								else
								{
									try
									{
										copy($directory . '/' . $fileName . '_original.' . $extension, $directory . '/' . $fileName . '.' . $extension);
										chmod($parentDirs, 0777);
									} catch (\Exception $e)
									{
										Log::error("CATCH ERROR: while renaming image for file(path)" . $parentDirs);
										Log::error($e->getMessage());
									}
								}
							}
							else
							{
								Log::info("Backup File not found: " . $directory . '/' . $fileName . '_original.' . $extension);
							}
						} catch (\Exception $e)
						{
							Log::error("CATCH ERROR: while watermarking image for file(path)" . $parentDirs);
							Log::error($e->getMessage());
						}
					}
					else
					{
						Log::error("Image URL not found for file(path):" . $parentDirs);
					}
				}
			}
		}
	}

	public function failed(\Exception $e)
	{
		Log::info("JOB FAILED MESSAGE: " . $e->getMessage());
		Log::info("JOB FAILED TRACE: " . $e->getTraceAsString());

		$data['exception'] = $e;
		$this->sendFailedJobsToTeam($data);
	}
}