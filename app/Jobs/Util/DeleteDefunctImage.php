<?php

namespace App\Jobs\Util;

use App\Jobs\BaseJob;
use Illuminate\Support\Facades\Log;

class DeleteDefunctImage extends BaseJob
{
	private $images;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct($images)
	{
		$this->images = $images;
	}

	public function handle()
	{
		$images = $this->images;

		foreach ($images as $image)
		{
			if ($image && file_exists($image))
			{
				try
				{
					unlink($image);
				} catch (\Exception $e)
				{
					Log::info("--- issue with deleting image");
					Log::info("Catch ERROR: " . $e->getMessage());
				}
			}
		}
	}

	public function failed(\Exception $e)
	{
		Log::info("JOB FAILED ERROR: " . $e->getMessage());
	}
}