<?php

namespace App\Evibe\Utility;

class PartnerAppNotification
{
	private $userId;
	private $screen;
	private $screenId;
	private $notificationTitle;
	private $notificationMessage;
	private $typeNotification;
	private $isAutoBook;
	private $deliveriesCount;
	private $url;
	private $isAvailCheck;

	/**
	 * @return mixed
	 */
	public function getUserId()
	{
		return $this->userId;
	}

	/**
	 * @param mixed $userId
	 */
	public function setUserId($userId)
	{
		$this->userId = $userId;
	}

	/**
	 * @return mixed
	 */
	public function getScreen()
	{
		return $this->screen;
	}

	/**
	 * @param mixed $screen
	 */
	public function setScreen($screen)
	{
		$this->screen = $screen;
	}

	/**
	 * @return mixed
	 */
	public function getScreenId()
	{
		return $this->screenId;
	}

	/**
	 * @param mixed $screenId
	 */
	public function setScreenId($screenId)
	{
		$this->screenId = $screenId;
	}

	/**
	 * @return mixed
	 */
	public function getNotificationTitle()
	{
		return $this->notificationTitle;
	}

	/**
	 * @param mixed $notificationTitle
	 */
	public function setNotificationTitle($notificationTitle)
	{
		$this->notificationTitle = $notificationTitle;
	}

	/**
	 * @return mixed
	 */
	public function getNotificationMessage()
	{
		return $this->notificationMessage;
	}

	/**
	 * @param mixed $notificationMessage
	 */
	public function setNotificationMessage($notificationMessage)
	{
		$this->notificationMessage = $notificationMessage;
	}

	/**
	 * @return mixed
	 */
	public function getTypeNotification()
	{
		return $this->typeNotification;
	}

	/**
	 * @param mixed $typeNotification
	 */
	public function setTypeNotification($typeNotification)
	{
		$this->typeNotification = $typeNotification;
	}

	/**
	 * @return mixed
	 */
	public function getIsAutoBook()
	{
		return $this->isAutoBook;
	}

	/**
	 * @param mixed $isAutoBook
	 */
	public function setIsAutoBook($isAutoBook)
	{
		$this->isAutoBook = $isAutoBook;
	}
	/**
	 * @return mixed
	 */
	public function getIsAvailCheck()
	{
		return $this->isAvailCheck;
	}

	/**
	 * @param mixed $isAvailCheck
	 */
	public function setIsAvailCheck($isAvailCheck)
	{
		$this->isAvailCheck = $isAvailCheck;
	}

	/**
	 * @return mixed
	 */
	public function getDeliveriesCount()
	{
		return $this->deliveriesCount;
	}

	/**
	 * @param mixed $deliveriesCount
	 */
	public function setDeliveriesCount($deliveriesCount)
	{
		$this->deliveriesCount = $deliveriesCount;
	}

	/**
	 * @return mixed
	 */
	public function getUrl()
	{
		return $this->url;
	}

	/**
	 * @param mixed $url
	 */
	public function setUrl($url)
	{
		$this->url = $url;
	}

}