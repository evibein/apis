<?php

namespace Evibe\Utility;

class Utility
{
	/**
	 * To create a hash from a normal indexed array with specified field as key
	 *
	 */
	public function createHash($array, $keyField = 'id')
	{
		$hash = [];
		foreach ($array as $item)
		{
			$hash[$item[$keyField]] = $item;
		}

		return $hash;
	}

	/**
	 * To dehash associative array and make it as indexed array
	 */
	public function deHash($hash)
	{
		return array_values($hash);
	}

	/**
	 * [camelCaseToSnakeCaseArrayAttibutes Helper function to convert all camel case attributes of an array to snake
	 * case]
	 *
	 * @param  [type] $array [description]
	 *
	 * @return [type]        [description]
	 */
	public function camelCaseToSnakeCaseArrayAttibutes($array)
	{
		$return = [];

		foreach ($array as $key => $value)
		{
			$transformedKey = snake_case($key);

			if (is_array($value))
			{
				$return[$transformedKey] = $this->camelCaseToSnakeCaseArrayAttibutes($value);
			}
			else
			{
				$return[$transformedKey] = $value;
			}
		}

		return $return;
	}

	public function chageKeyName($arr, $key, $newKey)
	{
		if (isset($arr[$key]))
		{
			$arr[$newKey] = $arr[$key];
			unset($arr[$key]);
		}

		return $arr;
	}

	public function formatPrice($price)
	{
		$explrestUnits = "";
		$paise = "";

		if (strpos($price, '.'))
		{
			$numArray = explode('.', $price);
			$paise = $numArray[1];
			$price = $numArray[0];
		}

		if (strlen($price) > 3)
		{
			$lastthree = substr($price, strlen($price) - 3, strlen($price));
			$restunits = substr($price, 0, strlen($price) - 3); // extracts the last three digits
			$restunits = (strlen($restunits) % 2 == 1) ? "0" . $restunits : $restunits; // explodes the remaining digits in 2's formats, adds a zero in the beginning to maintain the 2's grouping.
			$expunit = str_split($restunits, 2);

			for ($i = 0; $i < sizeof($expunit); $i++)
			{
				// creates each of the 2's group and adds a comma to the end
				if ($i == 0)
				{
					$explrestUnits .= (int)$expunit[$i] . ","; // if is first value , convert into integer
				}
				else
				{
					$explrestUnits .= $expunit[$i] . ",";
				}
			}

			$thecash = $explrestUnits . $lastthree;

		}
		else
		{
			$thecash = $price;
		}

		if ($paise)
		{
			$thecash .= '.' . $paise;
		}

		return $thecash; // writes the final format where $currency is the currency symbol.
	}
}