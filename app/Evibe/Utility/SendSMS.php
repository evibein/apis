<?php

namespace Evibe\Utility;

/**
 * @author Anji <anji@evibe.in>
 * @since  01 Feb 2016
 */

use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Mail;

class SendSMS
{
	public function send($job, $data)
	{
		$this->sendViaSMSGateway($job, $data);
	}

	private function sendViaSMSGateway($job, $data)
	{
		$username = Config::get('smsc.username');
		$password = Config::get('smsc.password');
		$senderId = Config::get('smsc.sender_id');
		$smsType = Config::get('smsc.route');
		$text = rawurlencode($data['text']);
		$to = $data['to'];

		if ($to && strlen($to) == 10)
		{
			$smsGatewayApi = "http://smsc.biz/httpapi/send?username=" . $username .
				"&password=" . $password .
				"&sender_id=" . $senderId .
				"&route=" . $smsType .
				"&phonenumber=" . $to .
				"&message=" . $text;

			$ch = curl_init($smsGatewayApi);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			$result = curl_exec($ch);
			curl_close($ch);

			// sms sending error occurred
			if ($result < 0)
			{
				$this->triggerErrorEmail([
					                         'to'         => $to,
					                         'text'       => $data['text'],
					                         'error_code' => $result
				                         ]);
			}
		}
		else
		{
			$this->triggerErrorEmail([
				                         'to'         => $to,
				                         'text'       => $data['text'],
				                         'error_code' => 'Invalid'
			                         ]);
		}

		$job->delete();
	}

	private function triggerErrorEmail($data)
	{
		$data = [
			'name'    => config('evibe.contact.admin.name'),
			'subject' => "[SMS Error] SMS sending failed - " . $data['to'] . " - " . $data['error_code'],
			'body'    => isset($data['text']) ? $data['text'] : 'SMS Sending Failed, check and fix at the earliest. Data unavailable. Number will be in the subject'
		];

		Mail::send('emails.report.admin.error', $data, function ($message) use ($data)
		{
			$message->from('ping@evibe.in', 'API Error');
			$message->to(config('evibe.contact.tech.email'), config('evibe.contact.admin.name'));
			$message->subject($data['subject']);
		});
	}

}