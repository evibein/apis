<?php

namespace Evibe\Utility;

class Roles
{
	const PLANNER_USER = 3;
	const VENUE_USER = 4;
	const ARTIST_USER = 7;
	const INVITES_USER = 10;
}