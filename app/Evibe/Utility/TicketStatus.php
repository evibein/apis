<?php namespace Evibe\Utility;

class TicketStatus {
	const INITIATED = 1;
	const INPROGRESS = 2;
	const CONFIRMED = 3;
	const BOOKED = 4;
	const CANCELLED = 5;
	const JUSTENQUIRY = 6;
}