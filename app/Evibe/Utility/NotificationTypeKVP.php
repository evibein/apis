<?php namespace Evibe\Utility;

class NotificationTypeKVP {
	const NORMAL = 1;
	const CHECK_AVAILABILITY = 2;
	const READ_ONLY = 3;
}