<?php

namespace App\Evibe\Validator;

use Illuminate\Support\Facades\Input;
use Illuminate\Validation\Validator;

class EvibeValidator extends Validator
{
	public function validatePhone($attribute, $value, $parameters)
	{
		if (is_numeric($value) && strlen($value) == 10)
		{
			return true;
		}

		return false;
	}
}