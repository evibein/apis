<?php

namespace Evibe\HttpClient;

use Guzzle\Http\Client;
use Guzzle\Common\Collection;
use Guzzle\Common\Exception\BadResponseException;
use Guzzle\Plugin\Backoff\BackoffPlugin;
use Guzzle\Http\Exception\ClientErrorResponseException;
use Guzzle\Http\Exception\ServerErrorResponseException;

class ApiClient extends Client
{

	public static $instance = null;

	private $apikey;
	private $apiSecret;
	private $apiBaseUrl;

	private $defaultHeader;

	public static function instance()
	{
		if (!self::$instance)
		{
			self::$instance = new ApiClient();
		}

		return self::$instance;
	}

	/**
	 * Helps to configure the KNOLSKAPE API sdk
	 */
	public function configure($config = [], $defaultHeader = [])
	{
		$default = [
			'baseUrl' => 'http://test-dash.evibe.in/apis/',
			'apikey'  => 'ABCD'
		];

		// The following values are required when creating the client
		$required = [
			'baseUrl',
			'apikey'
		];

		// Merge in default settings and validate the config
		$config = Collection::fromConfig($config, $default, $required);

		// Create a new Knolskape client
		$this->setBaseUrl($config->get('baseUrl'));
		$this->setConfig($config);

		$this->setApiBaseUrl($config->get('baseUrl'));
		$this->setApiKey($config->get('apikey'));

		// client default configuration
		$this->setUserAgent('Evibe');
		$this->setDefaultHeaders([
			                         'Accept' => 'application/json',
			                         'apikey' => $this->getApiKey()
		                         ]);

		// Use a static factory method to get a backoff plugin using the exponential backoff strategy
		$backoffPlugin = BackoffPlugin::getExponentialBackoff();

		// Add the backoff plugin to the client object
		$this->addSubscriber($backoffPlugin);

		$this->defaultHeader = $defaultHeader;

		return $this;
	}

	/**
	 * Call any end point from KNOLSKAPE api.
	 *
	 * @param
	 *      $url: the end point
	 *      $method: the HTTP verb
	 *      $payload: the payload in case of POST or PUT request
	 *      $queryparams: array of query parameters
	 *      $header: array of header parameters if any
	 *
	 * @return the reponse if the HTTP call is success, else it will return with appropriate exception
	 */
	public function call($url, $method, $payload = null, $queryparams = null, $header = null)
	{

		$request = null;

		switch ($method)
		{
			case 'GET':
				$request = $this->get($url);
				break;
			case 'POST':
				$request = $this->post($url);
				if ($payload)
				{
					$request->setBody(json_encode($payload), 'application/json');
				}
				break;
			case 'POST_FORM_DATA':
				$request = $this->post($url, null, $payload);
				break;
			case 'PUT':
				$request = $this->put($url);
				$request->setBody(json_encode($payload), 'application/json');
				break;
			case 'DELE':
				$request = $this->delete($url);
				break;
			default:
				# code...
				break;
		}

		if ($queryparams)
		{
			foreach ($queryparams as $key => $value)
			{
				$request->getQuery()->set($key, $value);
			}
		}

		if ($this->defaultHeader)
		{
			foreach ($this->defaultHeader as $key => $value)
			{
				$request->setHeader($key, $value);
			}
		}

		if ($header)
		{
			foreach ($header as $key => $value)
			{
				$request->setHeader($key, $value);
			}
		}

		$request->getCurlOptions()->set(CURLOPT_TIMEOUT, 30);

		try
		{
			$response = $request->send();

			return $response->json();
		} catch (ClientErrorResponseException $e)
		{
			$response = $e->getResponse()->json();
			throw new ClientError($response['errorMessage'], 400);
		} catch (ServerErrorResponseException $e)
		{
			$response = $e->getResponse()->json();
			throw new ServerError($response['errorMessage'], 500);
		}
	}

	/**
	 * Gets the value of apiKey.
	 *
	 * @return mixed
	 */
	public function getApiKey()
	{
		return $this->apikey;
	}

	/**
	 * Sets the value of apiKey.
	 *
	 * @param mixed $apiKey the apiKey
	 *
	 * @return self
	 */
	public function setApiKey($apikey)
	{
		$this->apikey = $apikey;

		return $this;
	}

	/**
	 * Gets the value of apiBaseUrl.
	 *
	 * @return mixed
	 */
	public function getApiBaseUrl()
	{
		return $this->apiBaseUrl;
	}

	/**
	 * Sets the value of apiBaseUrl.
	 *
	 * @param mixed $apiBaseUrl the apiBaseUrl
	 *
	 * @return self
	 */
	public function setApiBaseUrl($apiBaseUrl)
	{
		$this->apiBaseUrl = $apiBaseUrl;

		return $this;
	}

	/**
	 * Gets the value of apiBaseUrl.
	 *
	 * @return mixed
	 */
	public function getDefaultHeader()
	{
		return $this->defaultHeader;
	}

	/**
	 * Sets the value of apiBaseUrl.
	 *
	 * @param mixed $apiBaseUrl the apiBaseUrl
	 *
	 * @return self
	 */
	public function setDefaultHeader($header)
	{
		$this->defaultHeader = $header;

		return $this;
	}

}