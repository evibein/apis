<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class ExampleTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        // $this->post('v1/client')
        //      ->seeJson([
        //         "errorMessage" => "Client name missing in the payload."
        //      ]);

        // $this->post('v1/client',[],[],[],[], [ 'clientName' => 'Evibe pro client test' ])
        //      ->seeJson([
        //         "authClientName" => "Evibe pro client test"
        //      ]);
        $response = $this->call('POST','v1/client',[],[],[],[], json_encode([ 'clientName' => 'Evibe pro client test' ]));

        $this->assertContains('"authClientName":"Evibe pro client test"', $response->getContent());

    }
}
