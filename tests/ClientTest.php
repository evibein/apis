<?php

use Illuminate\Foundation\Testing\DatabaseTransactions;

class ClientTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testCreateANewClient()
    {
        $this->post('v1/client')
             ->seeStatusCode(400)
             ->seeJson([
                "errorMessage" => "Client name missing in the payload."
             ]);

        $this->postWithPayload('v1/client',[ 'clientName' => 'Evibe pro client test' ])
             ->seeStatusCode(200)
             ->seeJson([
                "authClientName" => "Evibe pro client test"
             ]);

        $response = json_decode($this->response);

        $authClientId = $response['authClientId'];
        $randomClientId = "12345678";

        $this->get('v1/client')
            ->seeStatusCode(200)
            ->seeJson([
                "authClientName" => "Evibe pro client test"
            ]);

        $this->get('v1/client/'.$authClientId)
            ->seeStatusCode(200)
            ->seeJson([
                "authClientName" => "Evibe pro client test"
            ]);
        $this->get('v1/client/'.$randomClientId)
             ->seeStatusCode('400')
             ->seeJson([
                "errorMessage" => "Invalid client id."
             ]);

    }

    public function testGetUserDetails()
    {
        
    }
    
}
