<?php

use Illuminate\Database\Seeder;

use App\Http\Models\User;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // factory('App\Http\Models\Notification')->create();

  //   	$city = factory('App\Http\Models\City')->create();

  //   	$city->area()->save(factory('App\Http\Models\Area')->make());

		factory('App\Http\Models\User',1)->create();
    }
}
