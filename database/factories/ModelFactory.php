<?php
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\Http\Models\City::class, function($faker) {
	return [
		'name' => $faker->city
	];
});

$factory->define(App\Http\Models\Area::class, function($faker) {
	return [
		'name' => $faker->streetName
	];
});

$factory->define(App\Http\Models\User::class, function ($faker) {
    return [
        'username' => $faker->email,
        'password' => Hash::make('qwerty'),
        'remember_token' => str_random(10),
        'role_id' => 1,
        'created_at' => Carbon::now(),
        'updated_at' => Carbon::now()
    ];
});

$factory->define(App\Http\Models\Notification::class, function ($faker) {
    return [
        'title' => $faker->sentence,
        'text' => $faker->paragraph,
        'type_id' => 3,
        'created_by' => 219,
        'created_for' => 220,
        'is_read' => 1,
        'read_at' => Carbon::now(),
        'is_replied' => 0,
        'replied_at' => null,
        'reply_decision' => 0,
        'ticket_booking_id' => 1,
        'reply_text' => null,
        'is_archived' => 1,
        'archived_at' => Carbon::now()->addHours(10),
        'expires' => null
    ];
});






