<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeEventPhoneNumbers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_event_phone_numbers', function($table){
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('phone_number',20);

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('event_id')
                  ->references('id')->on('invibe_events')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invibe_event_phone_numbers');
    }
}
