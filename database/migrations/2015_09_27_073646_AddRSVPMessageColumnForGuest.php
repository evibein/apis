<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRSVPMessageColumnForGuest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_event_guests', function($table) {
            $table->renameColumn('rsvp_staus','rsvp_status');
        }); 
        
        Schema::table('invibe_event_guests', function($table) {
            $table->text('rsvp_message')->after('rsvp_status');
            $table->timestamp('rsvp_at')->after('rsvp_message');
        }); 
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_event_guests', function($table) {
            $table->renameColumn('rsvp_status','rsvp_staus');
        }); 

        Schema::table('invibe_event_guests', function($table) {
            $table->dropColumn('rsvp_message');
            $table->dropColumn('rsvp_at');
        });
    }
}
