<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeEventGuests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_event_guests', function($table)
        {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('user_name',1000)->nullable();
            $table->string('user_email',1000)->nullable();
            $table->string('user_phone',20)->nullable();
            $table->integer('user_id')->unsigned()->nullable();

            $table->boolean('is_invite_sent')->default(false);

            // rsvp status can be either 0 (not set), 1 (Yes) or 2 (No)
            $table->tinyInteger('rsvp_staus')->default(0)
                  ->comment('rsvp status can be either 0 (not set), 1 (Yes) or 2 (No)');
            
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('event_id')
                  ->references('id')->on('invibe_events')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('user_id')
                  ->references('id')->on('user');


        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invibe_event_guests');
    }
}
