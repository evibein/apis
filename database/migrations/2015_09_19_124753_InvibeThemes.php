<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeThemes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_themes', function($table)
        {
            $table->increments('id');
            $table->string('name',1000);
            $table->text('img_path');
            $table->integer('event_type_id')->unsigned();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('event_type_id')
                  ->references('id')->on('invibe_event_types')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invibe_themes');
    }
}
