<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeletePhoneNumberTableAndAddItToEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::drop('invibe_event_phone_numbers');

         // As we allow upto maximum of 3 phone numbers for any events we will have three columns for the same
         Schema::table('invibe_events', function($table) {
            $table->string('phone_number',20)->after('hosts');
            $table->string('alt_phone_number',20)->after('phone_number');
            $table->string('alt_phone_number1',20)->after('alt_phone_number');
         });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('invibe_event_phone_numbers', function($table){
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('phone_number',20);

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('event_id')
                  ->references('id')->on('invibe_events')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

        });

        Schema::table('invibe_events', function($table) {
            $table->dropColumn(['phone_number','alt_phone_number','alt_phone_number1']);
        });
    }
}
