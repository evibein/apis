<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameColumnLongitude extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_events', function($table) {
            $table->renameColumn('venue_map_longitudes','venue_map_longitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_events', function($table) {
            $table->renameColumn('venue_map_longitude','venue_map_longitudes');
        });
        
    }
}
