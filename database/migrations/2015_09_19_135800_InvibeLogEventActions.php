<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeLogEventActions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_log_event_actions', function($table)
        {
            $table->increments('id');
            $table->integer('event_id')->unsigned();
            $table->string('action_type');
            $table->mediumText('message');

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('event_id')
                  ->references('id')->on('invibe_events');
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invibe_log_event_actions');
    }
}
