<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNullableEventAndUserIdInGuestList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_guest_lists', function($table) {
           $table->integer('event_id')->nullable()->unsigned();
           $table->integer('user_id')->nullable()->unsigned();

           $table->foreign('event_id')
                  ->references('id')->on('invibe_events')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

           $table->foreign('user_id')
                  ->references('id')->on('user')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_guest_lists', function($table) {
            $table->dropForeign('invibe_guest_lists_event_id_foreign');
            $table->dropForeign('invibe_guest_lists_user_id_foreign');

            $table->dropColumn('user_id');
            $table->dropColumn('event_id');
        });
    }
}
