<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeColumnNamePhoneNumberToContactNumber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_events', function($table) {
            $table->renameColumn('phone_number','contact_number');
            $table->renameColumn('alt_phone_number','alt_contact_number');
            $table->dropColumn('alt_phone_number1');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_events', function($table) {
            $table->renameColumn('contact_number','phone_number');
            $table->renameColumn('alt_contact_number','alt_phone_number');
        });

        Schema::table('invibe_events', function($table) {
            $table->string('alt_phone_number1',20)->after('alt_phone_number');
        });
        
    }
}
