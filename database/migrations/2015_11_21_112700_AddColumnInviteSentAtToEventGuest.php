<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInviteSentAtToEventGuest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_event_guests', function($table) {
            $table->timestamp('invite_sent_at')->nullable()->after('is_invite_sent');
        });

        // Make rsvp_at nullable
         DB::statement('
            ALTER TABLE `invibe_event_guests` 
            MODIFY `rsvp_at` timestamp;
        ');

        // Make user_phone not nullable
        DB::statement('
            ALTER TABLE `invibe_event_guests` 
            MODIFY `user_phone` varchar(20) NOT NULL;
        ');

        // make rsvp_message nullable
        Schema::table('invibe_event_guests', function($table) {
            $table->text('rsvp_message')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_event_guests', function($table) {
            $table->dropColumn('invite_sent_at');
        });

        DB::statement('
            ALTER TABLE `invibe_event_guests` 
            MODIFY `rsvp_at` timestamp NOT NULL;
        ');

        Schema::table('invibe_event_guests', function($table) {
            $table->string('user_phone')->nullable()->change();
        });

        DB::statement('
            ALTER TABLE `invibe_event_guests` 
            MODIFY `rsvp_message` text NOT NULL;
        ');

    }
}
