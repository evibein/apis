<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeGuestListUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_guest_list_users', function($table)
        {
            $table->increments('id');
            $table->integer('guest_list_id')->unsigned();
            $table->string('user_name',1000)->nullable();
            $table->string('user_email',1000)->nullable();
            $table->string('user_phone_number',20)->nullable();

            $table->timestamps();
            $table->softDeletes();

            $table->foreign('guest_list_id')
                  ->references('id')->on('invibe_guest_lists')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('invibe_guest_list_users');
    }
}
