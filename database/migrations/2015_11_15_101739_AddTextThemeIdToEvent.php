<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTextThemeIdToEvent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_events', function($table) {
            $table->integer('text_theme_id')->unsigned()->nullable()->after('theme_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invibe_events', function($table) {
            $table->dropColumn('text_theme_id');
        });
    }
}
