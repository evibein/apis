<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InvibeEvents extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invibe_events', function($table)
        {
            $table->increments('id');
            $table->text('name');
            $table->integer('type_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->date('event_date');
            $table->time('event_start_time');
            $table->time('event_end_time');
            $table->text('venue_address')->nullable();
            $table->text('venue_pin')->nullable();
            $table->text('venue_landmark')->nullable();
            $table->text('hosts');
            $table->text('guess_message')->nullable();
            $table->float('venue_map_latitude')->nullable();
            $table->float('venue_map_longitudes')->nullable();

            $table->integer('theme_id')->unsigned()->nullable();
            $table->boolean('is_thank_note_sent')->default(false);
            $table->boolean('is_cancelled')->default(false);
            $table->timestamp('cancellation_timestamp')->nullable;

            $table->boolean('send_autoreminder_to_guest')->default(true);
            $table->boolean('send_updates_on_modification')->default(true);

            // Event status can be 0 - Basic event data entered , 1 - Theme selected, 2 - Guest selected, 3 - Invitation sent, 4 - Event complete 
            $table->tinyInteger('event_status')->default(1)->comment('Event status can be 0 - Basic event data entered , 1 - Theme selected, 2 - Guest selected, 3 - Invitation sent, 4 - Event complete');

            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('type_id')
                  ->references('id')->on('invibe_event_types')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('user_id')
                  ->references('id')->on('user')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');

            $table->foreign('theme_id')
                  ->references('id')->on('invibe_themes')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invibe_events');
    }
}
