<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPhoneNumberAndOtpColumnToUserTable extends Migration
{
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('user', function ($table)
		{
			// $table->string('phone_number',20)->unique()->nullable();
			$table->string('invibe_otp', 45)->nullable();
			//  $table->string('name',1000)->after('password')->nullable();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('user', function ($table)
		{
			// $table->dropColumn('phone_number');
			$table->dropColumn('invibe_otp');
			// $table->dropColumn('name');
		});
	}
}
