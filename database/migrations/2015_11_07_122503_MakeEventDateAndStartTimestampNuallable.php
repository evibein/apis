<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeEventDateAndStartTimestampNuallable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invibe_events', function($table) {
            $table->date('event_date')->nullable()->change();
            $table->time('event_start_time')->nullable()->change();
            $table->time('event_end_time')->nullable()->change();
            $table->text('hosts')->nullable()->change();
            $table->string('phone_number',20)->nullable()->change();
            $table->string('alt_phone_number',20)->nullable()->change();
            $table->string('alt_phone_number1',20)->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Note: laravel schema builder doesn't support this syntax to make the nullable column's not nullable

        // Schema::table('invibe_events', function($table) {
        //     $table->date('event_date')->change();
        //     $table->time('event_start_time')->change();
        //     $table->time('event_end_time')->change();
        //     $table->text('hosts')->change();
        //     $table->string('phone_number',20)->change();
        //     $table->string('alt_phone_number',20)->change();
        //     $table->string('alt_phone_number1',20)->change();
        // });
        
        DB::statement('
            ALTER TABLE `invibe_events` 
            MODIFY `event_date` DATE NOT NULL,
            MODIFY `event_start_time` TIME NOT NULL,
            MODIFY `event_end_time` TIME NOT NULL,
            MODIFY `hosts` TEXT NOT NULL,
            MODIFY `phone_number` VARCHAR(20) NOT NULL,
            MODIFY `alt_phone_number` VARCHAR(20) NOT NULL,
            MODIFY `alt_phone_number1` VARCHAR(20) NOT NULL;
            ');
    }
}
