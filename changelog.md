# Evibe.in API's

This change log will contain all the code changes for bug fixes, new features etc. Each version will contain sections: `Added`, `Changed`, `Removed`, `Fixed` (whichever is applicable).


### v2.6.10 (*07 Jan 2022*) `@NGK`
##### Changed
- SMS templates as per DLT appproval

### v2.6.9 (*23 Apr 2020*) `@NGK`
##### Added
- Option availability console support

### v2.6.8 (*14 Mar 2020*) `@NGK`
##### Changed
- evib.es to bit.ly

### v2.6.7 (*13 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 2

### v2.6.6 (*9 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 2

### v2.6.5 (*7 Mar 2020*) `@NGK`
##### Changed
- Email communication optimisations 1 part 1

### v2.6.4 (*6 Mar 2020*) `@NGK`
##### Added
- Detailed cancellation email to partners (along with currently active bookings for that ticket)

### v2.6.3 (*5 Mar 2020*) `@NGK`
##### Changed
- Changed CRM dashboard default timings

### v2.6.2 (*14 Feb 2020*) `@NGK`
##### Added
- Surprise decors booking restriction for Feb 14 2020

### v2.6.1 (*11 Jan 2020*) `@KJ`
##### Added
- Area filter in reco surprises page

### v2.6.0 (*09 Jan 2020*) `@JR`
##### Upgraded
- Laravel version upgrade

### v2.5.52 (*07 Dec 2019*) `@NGK`
##### Added
- API support to add fb, instagram ad leads

### v2.5.51 (*21 Nov 2019*) `@NGK`
##### Changed
- Increased retention coupon amount from 100 to 300
- Coupon code expiry time validation

### v2.5.50 (*13 Oct 2019*) `@NGK`
##### Removed
- Removed email update alert to team
- Removed team from cc in email update alert to customer

### v2.5.49 (*05 Oct 2019*) `@NGK`
##### Added
- GSTIN and default processing date for settled settlements

### v2.5.48 (*04 Oct 2019*) `@NGK`
##### Added
- Add-ons in booking communication emails

### v2.5.47 (*29 Sep 2019*) `@NGK`
##### Changed
- Hid customer and partner contact information in emails
##### Added
- TMO and partner dash quick links in emails

### v2.5.46 (*20 Sep 2019*) `@NGK`
##### Added
- GSTIN, billing address
- Additional functionality to regenerate invoices for settled settlements of a partner

### v2.5.45 (*19 Sep 2019*) `@JR`
##### Added
- Customer app login backend

### v2.5.44 (*13 Sep 2019*) `@NGK`
##### Added
- Settlement requests from partner dashboard will not receive partners list

### v2.5.43 (*11 Sep 2019*) `@NGK`
##### Added
- Settlement calculation will consider manual star order marking while computing

### v2.5.42 (*10 Sep 2019*) `@NGK`
##### Added
- Absolute settlement amount calculation for star orders

### v2.5.41 (*10 Sep 2019*) `@NGK`
##### Added
- Quick Settlements console

### v2.5.40 (*04 Sep 2019*) `@NGK`
##### Added
- Edit and delete functionality for processed adjustments

### v2.5.39 (*31 Aug 2019*) `@NGK`
##### Changed
- Booking specific checkout fields

### v2.5.38 (*26 Aug 2019*) `@NGK`
##### Changed
- Partner delivery email will be sent to business group

### v2.5.37 (*07 Aug 2019*) `@NGK`
##### Changed
- Changed settlement pending limit

### v2.5.36 (*05 Aug 2019*) `@NGK`
##### Added
- Avail check uploaded images

### v2.5.35 (*26 Jul 2019*) `@NGK`
##### Added
- Added new roles in CRM dashboard

### v2.5.34 (*04 Jul 2019*) `@NGK`
##### Added
- Star Order highlight in ticket bookings, OPS order confirmations and avail-checks

### v2.5.33 (*03 Jul 2019*) `@GS` `@KJ`
##### Added
- Support to map partner reviews and delivery images to respective options

### v2.5.32 (*02 Jul 2019*) `@NGK`
##### Added
- Notification info in avail-checks list

### v2.5.31 (*02 Jul 2019*) `@NGK`
##### Added
- Evibe commission rate will be taken from star_option table for star orders

### v2.5.30 (*02 Jul 2019*) `@NGK`
##### Added
- Added settlement deviations API support for btn in pending settlement tab

### v2.5.29 (*01 Jul 2019*) `@JR`
##### Added
- Added support in Emails to show coupon discount & international charges
### v2.5.29 (*29 Jun 2019*) `@GS` `@KJ`
##### Added
- Console to map reviews to options

### v2.5.28 (*28 Jun 2019*) `@GS` `@KJ`
##### Added
- New 2 step avail check model

### v2.5.27 (*27 Jun 2019*) `@NGK`
##### Changed
- Skipping the category tag selection step in DASH workflow

### v2.5.26 (*26 Jun 2019*) `@NGK`
##### Added
- Availability of surprise options based on booking availability per slot

### v2.5.25 (*25 Jun 2019*) `@NGK`
##### Changed
- Special notes for each booking

### v2.5.24 (*22 Jun 2019*) `@JR`
##### Added
- Added in progress in CRM dashboard

### v2.5.23 (*21 Jun 2019*) `@GS`
##### Changed
- Changed AB and enquiry communication from business to ops

### v2.5.22 (*20 Jun 2019*) `@GS` `@KJ`
##### Removed
- Landmark and followup creation in ticket workflow

### v2.5.21 (*17 Jun 2019*) `@KJ`
##### Added
- Partner cancelled (Auto Cancel) booking in OPS order confirmations

### v2.5.20 (*13 Jun 2019*) `@NGK`
##### Fixed
- Venue partner info to add-on partner fix by correcting config keys

### v2.5.19 (*7 Jun 2019*) `@NGK`
##### Fixed
- Data table sorting for PIAB product bookings

### v2.5.18 (*7 Jun 2019*) `@NGK`
##### Fixed
- Delivery date in customer communication for delivered and cancelled status.

### v2.5.17 (*31 May 2019*) `@GS` `@KJ`
##### Added
- Order update functionality for PIAB dashboard

### v2.5.16 (*29 May 2019*) `@GS` `@KJ`
##### Fixed
- Avail check image error notification

### v2.5.15 (*29 May 2019*) `@NGK`
##### Added
- Updated receipt sent at is being updated in tickets for AB confirmations

### v2.5.14 (*28 May 2019*) `@NGK`
##### Changed
- API support for ticket details edit

### v2.5.13 (*25 May 2019*) `@KJ`
##### Changed
- Partner accept email will now be sent to ops@evibe.in

### v2.5.12 (*23 May 2019*) `@KJ` `@GS`
##### Added
- PIAB dashboard

### v2.5.11 (*18 May 2019*) `@KJ`
##### Added
- Customer Reviews in OPS dashboard

### v2.5.10 (*18 May 2019*) `@KJ`
##### Added
- Individual ticket details edit functionality

### v2.5.9 (*15 May 2019*) `@KJ` `@GS`
##### Added
- List to view confirmed partners list in OPS dashboard
##### Changed
- Accept payment notification format to support OPS partner confirm requests

### v2.5.8 (*14 May 2019*) `@JR`
##### Modified
- Modified subject lines

### v2.5.7 (*08 Apr 2019*) `@NGK`
##### Added
- Avail check notification API

### v2.5.6 (*06 Apr 2019*) `@NGK`
##### Fixed
- If bookings exist, update min booking party date time when ticket is updated

### v2.5.5 (*03 Apr 2019*) `@NGK`
##### Added
- API support for customer order proofs

### v2.5.4 (*04 Apr 2019*) `@JR`
##### Added
- Added code support for removal of watermark

### v2.5.3 (*22 Mar 2019*) `@NGK`
##### Changed
- SMS sender id to EVIBES from EEVIBE

### v2.5.2 (*22 Mar 2019*) `@JR`
##### Fixed
- Converting from goo.gl to evib.es

### v2.5.1 (*9 Mar 2019*) `@NGK`
##### Fixed
- API to fetch partner unavailable slots

### v2.5.0 (*6 Mar 2019*) `@NGK`
##### Added
- OPS Dashboard

### v2.4.63 (*18 Feb 2019*) `@NGK`
##### Added
- Country calling code

### v2.4.62 (*13 Feb 2019*) `@NGK`
##### Fixed
- Closure date cannot be kept after party date

### v2.4.61 (*12 Feb 2019*) `@JR`
##### Fixed
- Issue with broken images in partner communication

### v2.4.60 (*11 Feb 2019*) `@JR`
##### Modified
- Enhancements in CRM dashboard

### v2.4.59 (*31 Jan 2019*) `@JR`
##### Added
- Appending comments to the old tickets rather that creating an old ticket if customer call us

### v2.4.58 (*29 Jan 2019*) `@JR`
##### Fixed
- Avail check console

### v2.4.57 (*25 Jan 2019*) `@NGK`
##### Fixed
- Issue with sending AB receipts to only one partner

### v2.4.56 (*21 Jan 2019*) `@NGK`
##### Changed
- Using the term 'Track My Order' and hid partner details

### v2.4.55 (*10 Jan 2019*) `@JR`
##### Modified
- Separate email for enquiry for proper tracking

### v2.4.54 (*09 Jan 2019*) `@JR`
##### Modified
- Converting bitly to goo.gl until 31st mar 2019

### v2.4.53 (*02 Jan 2019*) `@JR`
##### Modified
- Not converting the status to invalid email, if the entered email address is wrong

### v2.4.52 (*24 Dec 2018*) `@JR`
##### Modified
- Workflow v3

### v2.4.51 (*14 Dec 2018*) `@JR`
##### Added
- Booking SMS templates modifications

### v2.4.50 (*8 Dec 2018*) `@NGK`
##### Added
- Communication for add-ons partner to deliver to the venue manager for applicable add-ons

### v2.4.49 (*8 Dec 2018*) `@JR`
##### Added
- CTA's in Auto-book email's for TMO

### v2.4.48 (*6 Dec 2018*) `@JR`
##### Updated
- Empty enquiry_id fix 
- Getting the previous booking enquiry Id for same phone, party date

### v2.4.47 (*4 Dec 2018*) `@JR`
##### Updated
- Change Booking subject lines

### v2.4.46 (*4 Dec 2018*) `@AR`
##### Fixed
- Updated website phone number

### v2.4.45 (*4 Dec 2018*) `@NGK`
##### Fixed
- Booking receipt email

### v2.4.44 (*4 Dec 2018*) `@NGK`
##### Added
- Event name partner and customer booking communication

### v2.4.43 (*3 Dec 2018*) `@NGK`
##### Changed
- Cancellation policies

### v2.4.42 (*19 Nov 2018*) `@JR`
##### Added
- Special notes for each booking

### v2.4.41 (*15 Nov 2018*) `@JR`
##### Fixed
- Internal avail check not showing confirmed, booked tickets fix

### v2.4.40 (*12 Nov 2018*) `@JR`
##### Fixed
- RE duplicate user fix

### v2.4.39 (*10 Nov 2018*) `@NGK`
##### Fixed
- Temporary time switch to avoid bitly error

### v2.4.38 (*29 Oct 2018*) `@Manav`
##### Added
- Now allow user to change the partner and select a default evibe partner.
- Can be changes before enquiring and finalizing

### v2.4.37 (*27 Oct 2018*) `@NGK`
##### Added
- Extra params in log code to debug settlement status issue

### v2.4.36(*26 Oct 2018*) `@JR`
##### Fixed
- Delivery Images for a product

### v2.4.35(*08 Oct 2018*) `@MC`
##### Fixed
- Fixed for venue availability check

### v2.4.34 (*22 Oct 2018*) `@Manav`
##### Fixed
- Now not showing updates done by any partner in previous state.
- Fixed the issue of showing previous state. It show the state less than the selected start time and not done by partner and the most recent update within these filters.

### v2.4.34 (*22 Oct 2018*) `@Manav`
##### Fixed
- Showing reset filter button in dash for history tab

### v2.4.33 (*20 Oct 2018*) `@Manav`
##### Added
- Created at filter for all tabs

### v2.4.32 (*20 Oct 2018*) `@AR`
##### Changed
- "New Tickets" tab in CRM Dashboard does not show old tickets without event date in end state.  

### v2.4.31 (*16 Oct 2018*) `@Manav`
##### Added
- History Tab in crm dashboard

### v2.4.30 (*15 Oct 2018*) `@Manav`
##### Added
- Asking for gender of customer.
- Asking for time slot of party.
- Team Notes on every page.
- No default for any select box
- Making ticket source selectize.
- Asking min and max budget of customer.
- Added new select box asking the Best time to contact.
- Added new select box asking the decision maker.
- Added new box asking the alt email and alt phone in 5th screen.

### v2.4.29 (*15 Oct 2018*) `@AR`
##### Changed
- Updated logic in fetching tickets for each tab

### v2.4.28 (*12 Oct 2018*) `@Manav`
##### Changed
- Reverted way to calculate followup tickets
- Showing tickets in Followup tab where followup date > current date 
- Calculating closure date for ticket creation based on booking likeliness
- Making sure that booking likeliness is filled

### v2.4.27 (*12 Oct 2018*) `@NGK`
##### Fixed
- Fetching deleted 'users' no not create retention reminders for deleted users

### v2.4.26 (*10,11 Oct 2018*) `@Manav`
##### Added
- Filter based on type followup 
- Showing the new tickets in return tickets which have followup.
- Filter for the closure date.
- New tab prospects.

### v2.4.25 (*09 Oct 2018*) `@NGK`
##### Fixed
- Added validation to fetch bank details only if user exists

### v2.4.24(*09 Oct 2018*) `@Manav`
##### Added
- Added filter for customer source

### v2.4.23(*06 Oct 2018*) `@Manav`
##### Added
- Party Date filters and followup filters for new and missed tickets

### v2.4.22(*06 Oct 2018*) `@Jeevan`
##### Added
- Added logic for missed followup

### v2.4.21(*05 Oct 2018*) `@Anji` `@JR`
##### Modified
- Modified logic for getting return tickets

### v2.4.20(*24 Sep 2018*) `@JR`
##### Fixed
- Modified code for getting options based on gender tag in workflow

### v2.4.19(*15 Sep 2018*) `@JR`
##### Fixed
- Getting refer & earn users without considering spaces for duplicate user issue

### v2.4.18(*12 Sep 2018*) `@JR`
##### Fixed
- Number mismatch in dashboard due to enquiry source value 0

### v2.4.17(*08 Sep 2018*) `@Manav`
##### Fixed
- Not taking special characters for referral code
- Not showing mails for login error in partners app
- Showing booking likeliness in crm dashboard

### v2.4.16 (*10 Sep 2018*) `@JR`
##### Fixed
- Getting proper options for workflow based on the tags selected

### v2.4.15 (*07 Sep 2018*) `@JR`
##### Fixed
- Error creating duplicate users

### v2.4.14 (*06 Sep 2018*) `@JR`
##### Fixed
- Workflow for customers

### v2.4.13 (*29 Aug 2018*) `@Manav`
##### Fixed
- Now showing checkout fields for manual tickets

### v2.4.12 (*29 Aug 2018*) `@NGK`
##### Added
- Post feedback add-on changes

### v2.4.11 (*29 Aug 2018*) `@JR`
##### Fixed
- Added data for whatsapp reco url generation in dash

### v2.4.10 (*25 Aug 2018*) `@NGK`
##### Fixed
- Included sms error details and will be sent to sysad@evibe.in

### v2.4.9 (*25 Aug 2018*) `@NGK`
##### Added
- Added support to accept/reject multiple auto-bookings

### v2.4.8 (*20 Aug 2018*) `@NGK`
##### Fixed
- Live status validation to show trends in DASH recos page

### v2.4.7 (*18 Aug 2018*) `@JR`
##### Modified
- Updated code to get HOT/MEDIUM count for CRM dashboard

### v2.4.6 (*11 Aug 2018*) `@JR`
##### Modified
- CSS and text modification in R&E email

### v2.4.5 (*10 Aug 2018*) `@JR`
##### Modified
- Refer & earn iteration 2

### v2.4.4 (*06 Aug 2018*) `@Manav`
##### Added
- Added sort button on the recon page

### v2.4.3 (*03 Aug 2018*) `@JR`
##### Modified
- Modified referral friend signup page

### v2.4.2 (*01 Aug 2018*) `@NGK`
##### Changed
- Temporary fix to sort reco options based on price rather than sorting algorithm.
- 30% to 50% payment advance in mail

### v2.4.1 (*28 Jul 2018*) `@NGK`
##### Changed
- Retention reminders won't be created for deleted users.

### v2.4.0 (*21 Jul 2018 ~ 24 Jul 2018*)
##### Added `@JR`
- Refer & Earn

### v2.3.27 (*11 Jul 2018 ~ 19 Jul 2018*)
##### Added `@JR`
- Generate coupon code for user

### v2.3.26 (*17 Jul 2018 ~ 19 Jul 2018*) `@NGK`
##### Added
- Added retention reminders to auto followups.

### v2.3.25 (*19 Jul 2018*) `@NGK`
##### Fixed
- Made changes to send reco email without error (handler error fix).

### v2.3.24 (*18 Jul 2018*) `@NGK`
##### Added
- Settlement support for post party cancellation.

### v2.3.23 (*06 Jul 2018 ~ 09 Jul 2018*)
##### Added `@NGK`
- API to stop auto followup will now support thank you card

### v2.3.22 (*27 June 2018*)
##### Added
- UTM tagging for recommendation links `@NGK`

### v2.3.21 (*20 Apr 2018 ~ 26 June 2018*)
##### Added
- Bounced email detection for partners and other users `@NGK`
- Welcome email and update ticket status for customers `@NGK`
- Added ENS for Log::error() where ever is possible in APIs `@NGK`

### v2.3.20 (*15 June 2018 ~ 21 June 2018*)
##### Added
- Recommendation options will be shown as per sort order score `@NGK`

### v2.3.19 (*21 June 2018*)
##### Added
- Included partner uniform status in emails `@NGK`

### v2.3.18 (*14 June 2018*)
##### Fixed
- Fixed issue with deleting defunct images, directories `@AR`

### v2.3.17 (*13 June 2018*)
##### Added
- Delete defunct products, images `@AR`

### v2.3.16 (*11 June 2018*)
##### Added
- Added watermarking `@RAMU`

### v2.3.15 (*18 May 2018*)
##### Changed
- Rounding off the final settlement amount for a partner `@NGK`

### v2.3.14 (*08 May 2018*)
##### Added
- Ability to send reminders to partners for pending deliveries
- Access token validation for CRM dashboard `@JR`

### v2.3.13 (*02 May 2018*)
##### Fixed
- CS workflow second screen has been validated with custom exception

### v2.3.13 (*02 Apr 2018*)
##### Added
- Special Notes for the booking
##### Fixed
- CS workflow second screen has been validated with custom exception
- Delivery images uploading Issues
- CRM dashboard metrics fixes `@JR`

### v2.3.13 (*04 May 2018*)
##### Changed
- Pending partner delivery reminders will be calculated in APIs rather than Dash `@NGK`

### v2.3.12 (*30 Apr 2018*)
##### Added
- Minor upgrades in CRM dashboard Url, Extended search from 7days to 30days `@JR`

### v2.3.11 (*28 Apr 2018*)
##### Added
- CRM dashboard metrics

### v2.3.10 (*28 Apr 2018*)
##### Added
- Refund reference in team email and refunds console `@NGK`

### v2.3.9 (*20 Apr 2018*)
##### Fixed
- Partner adjustments with 0/- will not be created `@NGK`
##### Added
- Pending settlements tab `@NGK`
- Clicking on 'create settlements' will update settlements rather than creating new `@NGK`

### V2.3.8 (*18 Apr 2018*)
##### Added
- Partner city in settlements sheet and console `@NGK`
##### Changed
- Minor changes in cancellation email sent to customer `@NGK`
- Auto followups will continue in no-response state also `@NGK`
- RR will not stop even if ticket is in no-response state `@NGK`

### v2.3.7 (*18 Apr 2018*)
##### Added
- Using bitly url shortener as goo.gl is no longer functional

### v2.3.6 (*09 Apr 2018 ~ 17 Apr 2018*)
##### Added
- Booking Amount will be taken from DB instead of calculations `@NGK`
##### Removed
- 'Change in settlement system' text in weekly settlements email to partners `@NGK`
- Email sender group and using option type instead of option name in cancellation receipts `@NGK`

### v2.3.5 (*07 Apr 2018*)
##### Added
- Sending booking cancellation receipts `@NGK`

### v2.3.5 (*06 Apr 2018*)
##### Added
- Implemented system to detect bounced emails and update the ticket accordingly `@NGK`

### v2.3.4 (*05 Apr 2018*)
##### Fixed
- Undefined index 'messages' in ENS `@NGK`
##### Removed
- Logs which were places to debug object type error `@NGK`
##### Added
- Sub function to update lookup table and used in resend receipts `@NGK`

### v2.3.3 (*31 Mar 2018 ~ 04 Apr 2018*)
##### Added
- Modified Cancellation and Refunds logic `@NGK`

### v2.3.2 (*31 Mar 2018*)
##### Added
- Live profile of partner `@Ramu`

### v2.3.1 (*14 Mar 2018*)
##### Fixed
- Feedback failure `@Ramu`

### v2.3.0 (*14 Mar 2018*)
##### Added
- Auto Settlements v1 `@NGK`

### v2.2.25 (*16 Mar 2018*)
##### Fixed
- Service DASH redirect link bug `@JR`

### v2.2.24 (*16 Mar 2018*)
##### Fixed
- Multiple quote validation error `@JR`

### v2.2.23 (*10 Mar 2018*)
##### Added
- Planning status storing in DB `@JR`

### v2.2.22 (*10 Mar 2018*)
##### Changed
- Upgrades in CS1 workflow v2 upgrades `@RAMU`

### v2.2.21 (*02 Mar 2018*)
##### Changed
- Resolved the DASH partner link for feedbackpage 

### v2.2.20 (*23 Feb 2018*)
##### Added
- Created handler support

### v2.2.19 (*16 Feb 2018*)
##### Added
- Automate customer review accept `@JR`

### v2.2.18 (*08 Feb 2018*)
##### Added
- Ticket customer source in contact details page `@JR`

### v2.2.17 (*06 Jan 2018*)
##### Fixed
- Added party date validation in auto followups `@NGK`

### v2.2.16 (*30 Jan 2018*)
##### Fixed
- Delete package mappings in CS2A recommendations `@NGK`

### v2.2.15 (*27 Jan 2018*)
##### Fixed
- Auto-booking enquiries not shown in the app(enquiries tab) `@JR`

### v2.2.14 (*19 Jan 2018*)
##### Added
- Enquiries will be shown only for followup & in progress tickets
##### Fixed
- Ajax error for the getting new orders in partner dashboard `@JR`

### v2.2.12 (*13 Jan 2018*)
##### Added
- Switch case support for automation console related API `@NGK`

### v2.2.11 (*11 Jan 2018*)
##### Fixed
- Revamped auto followup APIs architecture `@NGK`

### v2.2.10 (*03 Jan 2018*)
##### Added
- Getting new orders from the partner digest function `@JR`

### v2.2.9 (*29 Dec 2017*)
##### Fixed
- Fixed partner digest data issue `@ANJI`

### v2.2.8 (*27 Dec 2017*)
##### Fixed
- API support with fall back and basic case fix `@NGK`

### v2.2.7 (*26 Dec 2017*)
##### Added
- API support to create and update auto followup ticket reminders `@NGK`

### v2.2.6 (*22 Dec 2017*)
##### Fixed
- Reco mail more mappings, AWS message size bug fix `@JR`

### v2.2.5 (*14 Dec 2017*)
##### Added
- Fetching results in service specific options screen in CS2A is now ordered by price `@NGK`
- Baby Shower and Naming Ceremony occasions will have Kids Birthday results `@NGK`

### v2.2.4 (*08 Dec 2017*)
##### Fixed
- Cake and Venue gallery code `@NGK`

### v2.2.3 (*05 Dec 2017*)
##### Added
- APIs supporting the new ticket enquiry workflow (DASH workflow 2.0) - CS2A (Recommendation Stage) `@NGK`
- Bachelor Venue Packages cannot be auto booked for 31st Dec `@NGK`

### v2.2.1 (*02 Dec 2017*)
##### Fixed
- Query to fetch 'type_category_tags' will now not consider whether parent id value and also implemented 'SoftDeletes' for 'area' model`@NGK`

### v2.2.0 (*01 Dec 2017*)
##### Added
- APIs supporting the new ticket enquiry workflow (DASH workflow 2.0) - CS1 (Enquiry Stage) `@NGK`

### v2.1.1 (*28 Nov 2017*)
##### Fixed
- Error message to catch the error exception while sending enquiry pending remainders to team `@NGK`

### v2.1.0 (*18 Nov 2017*)
##### Added
- Partner dashboard related API to fetch analytics for given dates `@JR`

### v2.0.1 (*13 Nov 2017*)
##### Fixed
- After changing auto-book partner, Emails are still going to the old partner.
