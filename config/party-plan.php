<?php

return [

	/*
	|--------------------------------------------------------------------------s
	| Event specific services
	|--------------------------------------------------------------------------
	*/
	'type-event-services' => [
		config('evibe.event.kids_birthday')      => [
			config('evibe.ticket.type.decors'),
			/*
			 * Should enable it once tags are fixed
			config('evibe.ticket.type.entertainments'),
			config('evibe.ticket.type.food'),*/
			config('evibe.ticket.type.cakes'),
			config('evibe.ticket.type.packages')
		],
		config('evibe.event.bachelor_party')     => [
			config('evibe.ticket.type.cakes'),
			config('evibe.ticket.type.entertainments'),
			config('evibe.ticket.type.resort'),
			config('evibe.ticket.type.villa'),
			config('evibe.ticket.type.lounge'),
		],
		config('evibe.event.pre_post')           => [
			config('evibe.ticket.type.cakes'),
			config('evibe.ticket.type.decors'),
			config('evibe.ticket.type.entertainments'),
		],
		config('evibe.event.house_warming')      => [
			config('evibe.ticket.type.cakes'),
			config('evibe.ticket.type.decors'),
			config('evibe.ticket.type.entertainments'),
			config('evibe.ticket.type.food'),
			config('evibe.ticket.type.priests'),
			config('evibe.ticket.type.tents'),
		],
		config('evibe.event.special_experience') => [
			config('evibe.ticket.type.couple-experiences'),
		],
	],

];