<?php

return [

	/*
	|--------------------------------------------------------------------------s
	| Company Business Details
	|--------------------------------------------------------------------------
	|
	| Business details related to company
	*/
	'business'    => [
		'gstin' => '29AADCE4396F1ZE',
		'pan'   => 'AADCE4396F',
	],

	/*
	|--------------------------------------------------------------------------s
	| Head Contacts
	|--------------------------------------------------------------------------
	|
	| Related to various teams in the company
	*/
	'contact'     => [
		'operations' => [
			'name'                  => env('OPERATION_HEAD_NAME', 'Jyothi'),
			'phone'                 => env('OPERATION_HEAD_PHONE', '9640204000'),
			'email'                 => env('OPERATION_HEAD_EMAIL', 'operations@evibe.in'),
			'group'                 => env('OPERATION_GROUP_EMAIL', 'ops@evibe.in'),
			'alert_no_action_email' => env('OPERATIONS_ALERT_NO_ACTION_EMAIL', 'ops.alert.noaction@evibe.in')
		],
		'admin'      => [
			'name'  => env('ADMIN_NAME', 'Anji'),
			'phone' => env('ADMIN_PHONE', '7259509827'),
			'email' => env('ADMIN_EMAIL', 'anji@evibe.in')
		],
		'enquiry'    => [
			'group'    => env('CONTACT_ENQ_GRP', 'enquiry@evibe.in'),
			'crm_head' => env('CONTACT_ENQ_CRM_HEAD', 'jyothi.m@evibe.in'),
		],
		'business'   => [
			'group2' => env('CONTACT_BIZ_GRP_2', 'business@evibe.in'),
			'group'  => env('CONTACT_BIZ_GRP', 'ops@evibe.in'),
			'phone'  => env('BIZ_HEAD_PHONE', '9640807000')
		],
		'tech'       => [
			'group' => env('CONTACT_TECH_GRP', 'tech@evibe.in'),
			'email' => env('CONTACT_TECH_ADMIN_EMAIL', 'sysad@evibe.in'),
		],
		'accounts'   => [
			'group' => env('CONTACT_ACC_GRP', 'accounts@evibe.in'),
			'phone' => env('CONTACT_ACC_HEAD_PHONE', '7259509827')
		],
		'company'    => [
			'phone'              => env('COMPANY_PHONE', '+91 9640204000'),
			'plain_phone'        => env('COMPANY_PLAIN_PHONE', '9640204000'),
			'mobile'             => env('COMPANY_MOBILE', '9640804000'),
			'email'              => env('PING_EMAIL', 'ping@evibe.in'),
			'group'              => env('CONTACT_BIZ_GRP', 'business@evibe.in'),
			'system_alert_email' => env('SYSTEM_ALERT_EMAIL', 'system.alert@evibe.in'),
			'working'            => [
				'start_day'     => 'Monday',
				'end_day'       => 'Saturday',
				'start_time'    => '10 AM',
				'end_time'      => '8 PM',
				'deadline_time' => '8 PM'
			],
		],
		'support'    => [
			'group' => env('SUPPORT_EMAIL', 'support@evibe.in')
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Phone number
	|--------------------------------------------------------------------------
	*/
	'phone'       => '+91 9640204000',
	'phone_plain' => '9640204000',

	/*
	|--------------------------------------------------------------------------
	| Email address
	|--------------------------------------------------------------------------
	*/
	'email'       => 'ping@evibe.in',

	/*
	|--------------------------------------------------------------------------
	| Links
	|--------------------------------------------------------------------------
	*/
	'hosts'       => [
		'dash'    => env('HOST_DASH', 'http://dash.evibe.in'),
		'gallery' => env('GALLERY_HOST', 'http://gallery.evibe.in'),
		'api'     => env('API_HOST', "http://apis.evibe.in/"),
	],

	'root' => [
		'gallery'       => env('GALLERY_ROOT', '../../gallery'),
		'gallery_queue' => env('GALLERY_QUEUE_ROOT', '/var/www/html/gallery')
	],

	'prefix'  => [
		'partner_app' => 'partner/v1/'
	],
	'advance' => [
		'percentage' => env('PAYMENT_ADVANCE', 50),
	],

	'partner_app_link'      => 'https://play.google.com/store/apps/details?id=in.evibe.evibe',

	/*
	|--------------------------------------------------------------------------
	| Event type
	|--------------------------------------------------------------------------
	*/
	'event'                 => [
		'kids_birthday'      => env('EVENT_KIDS_BIRTHDAYS', 1),
		'youth_birthday'     => env('EVENT_YOUTH_BIRTHDAYS', 2),
		'senior_birthday'    => env('EVENT_SENIOR_BIRTHDAYS', 3),
		'reception'          => env('EVENT_RECEPTIONS', 4),
		'corporate_event'    => env('EVENT_CORPORATE_EVENTS', 5),
		'team_building'      => env('EVENT_TEAM_BUILDING', 6),
		'sangeeth'           => env('EVENT_SANGEETHS', 7),
		'naming_ceremony'    => env('EVENT_NAMING_CEREMONY', 8),
		'wedding'            => env('EVENT_WEDDING', 9),
		'board_meeting'      => env('EVENT_BOARD_MEETINGS', 10),
		'bachelor_party'     => env('EVENT_BACHELOR_PARTY', 11),
		'engagement'         => env('EVENT_ENGAGEMENT', 12),
		'pre_post'           => env('EVENT_PRE_POST', 13),
		'house_warming'      => env('EVENT_HOUSE_WARMING', 14),
		'office_pooja'       => env('EVENT_OFFICE_POOJA', 15),
		'special_experience' => env('EVENT_COUPLE_EXPERIENCES', 16),
		'christmas'          => env('EVENT_CHRISTMAS', 17),
		'valentines_day'     => env('EVENT_VALENTINES_DAY', 18),
		'baby_shower'        => env('EVENT_BABY_SHOWER', 19),
		'store_opening'      => env('EVENT_STORE_OPENING', 20),
		'mothers_day'        => env('EVENT_MOTHERS_DAY', 21),
		'anniversary'        => env('EVENT_ANNIVERSARY', 22),
		'other'              => env('EVENT_OTHER', 23),
		'fathers_day'        => env('EVENT_FATHERS_DAY', 24),
		'raksha_bandhan'     => env('EVENT_RAKSHA_BANDHAN', 25),
		'saree_ceremony'     => env('EVENT_SAREE_CEREMONY', 34),
		'dothi_ceremony'     => env('EVENT_DOTHI_CEREMONY', 35),
		'first_birthday'     => env('EVENT_FIRST_BIRTHDAY', 36),
		'birthday_2-5'       => env('EVENT_BIRTHDAY_2-5', 37),
		'birthday_6-12'      => env('EVENT_BIRTHDAY_6-12', 38)
	],

	/*
	|--------------------------------------------------------------------------
	| SMS Templates
	|--------------------------------------------------------------------------
	|
	| All the various SMS templates available to send SMS
	*/
	'sms_tpl'               => [
		'reco'           => [
			'customer' => "Hi #field1#, here are the best recommendations for your party: #field2#. Please check & submit your shortlist. Team Evibe.in"
		],
		'track'          => [
			'vendor'   => [
				'success' => 'Hi #field1#, we have informed your reporting time to #field2# (#field3#). Best wishes for your event. Team Evibe.'
			],
			'customer' => [
				'success' => 'Hi #field1#, I\'m #field2# from Evibe. We are happy to inform that all your party arrangements are on track. We will reach the venue by #field3# #dayType#. Cheers. Team Evibe.',
			],
			'team'     => [
				'fail' => 'Hi #field1#, #field2# gave missed call to an expired / invalid number. Pls call #field3# and take necessary action. Evibe.in'
			]
		],
		'report'         => [
			'vendor' => [
				'success' => 'Thank you for entering PIN & OTP for #field1# party (#field2#). Reported time is #field3#. Team Evibe.'
			]
		],
		'invites'        => [
			'otp' => 'Hi, Welcome to Invibe - simplest way to invite & manage your guests. Your OTP to signup is #field1#. Please do not share with anyone. Happy Invibing.'
		],
		'partner_app'    => [
			'enquiry'  => [
				'new'     => [
					'avl_check'    => 'Hi #field1#, availability check for party order on #field2#. Pls. check details & Accept / Reject the booking from Evibe.in Partner App within #field3# hours.',
					'custom_quote' => 'Hi #field1#, quote check for party on #field2#. Pls. check details & provide your Best Quote from Evibe.in Partner App within #field3# hours.'
				],
				'pending' => [
					'avl_check'    => 'Hi #field1#, pls. respond to availability check for #field2# immediately from Evibe.in Partner App to avoid auto cancellation.',
					'custom_quote' => 'Hi #field1#, pls. respond with your best quote for #field2# immediately from Evibe.in Partner App to avoid losing order.'
				]
			],
			'delivery' => [
				'pending' => 'Hi #field1#, pls. upload photos for #field2# pending deliveries from Evibe.in Partner App. Photos will be used to get new bookings & protect you in case of any issues.'
			]
		],
		'auto_book'      => [
			'confirm'     => [
				'customer'       => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Track and manage your order here: #field4#',
				'customer_proof' => 'Hi #customer#, Congrats! your event (#orderId#) is booked with Evibe.in. We still did not receive your ID proof. Kindly upload ASAP to avoid cancellation: #tmoLink#',
				'vendor'         => 'Hi #field1#, order booked for #field2#. Received Rs. #field3# as advance from #field4#. Total booking amount: Rs. #field5#. Pls check email sent to #field6# for complete order & customer details. Team Evibe'
			],
			'cancel'      => [
				'customer' => 'Hi #field1#, sorry! your order for #field2# for #field3# in not available. Full refund of Rs. #field4# will be initiated in 2 business days. For any queries, pls reply to the email sent to #field5#. Team Evibe',
				'vendor'   => 'Hi #field1#, you have rejected booking order for #field2# at #field3#. For any queries, please call #field4#. Team Evibe'
			],
			'venue_deals' => [
				'confirm' => [
					'customer' => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Track and manage your order here: #field4#',
					'vendor'   => 'Hi #field1#, you confirmed availability of #field2# for #field3# (#field4#) party on #field5# (#field6# slot) for #field7# guests. Please call customer to schedule site visit & advance amount payment. Customer already paid token amount of Rs. #field8#. Pls. check email sent to #field9# for complete details with next steps. Team Evibe.in'
				],
				'cancel'  => [
					'vendor' => 'Hi #field1#, as #field2# is NOT available for #field3# (#field4# slot), we have cancelled the order from #field5#. For any queries, pls call #field6#. Team Evibe.in'
				]
			],
			'cake'        => [
				'confirm' => [
					'customer' => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Track and manage your order here: #field4#',
					'partner'  => 'Hi #field1#, you confirmed #field2# (#field3#) order for #field4# (#field5# slot) at #field6#. Customer paid advance of Rs. #field7#. Balance amount is Rs. #field8# Pls. check email sent to #field9# for complete order & delivery details. Team Evibe.in'
				],
				'cancel'  => [
					'partner' => 'Hi #field1#, as #field2#,#field3# is NOT available for #field4# (#field5#), we have cancelled the order from #field6#. For any queries, pls call #field7#. Team Evibe.in'
				]
			],
			'decor'       => [
				'confirm' => [
					'customer' => 'Hi #field1#, Congrats! your event is booked with Evibe.in. We received payment of Rs. #field2# for your order #field3#. Track and manage your order here: #field4#',
					'partner'  => 'Hi #field1#, order booked for #field2# for party on #field3# at #field4#. Received Rs. #field5# as advance from #field6#. Total booking amount: Rs. #field7#. Please check email sent to #field8# for complete order & customer details. Team Evibe.in'
				],
				'cancel'  => [
					'partner' => 'Hi #field1#, based on your consent, we cancelled party order for #field2# on #field3# at #field4#. For any queries, please call #field5#. Team Evibe.in'
				]
			]
		],
		'finance'        => [
			'settlements' => [
				'partner' => 'Hi #partnerFirstName#, INR #netDueAmount# is processed to settle orders from #startDate# to #endDate#. Click here: #partnerDashLink# to get settlement invoice. Evibe.in'
			],
			'refunds'     => [
				'customer' => 'Hi #customerName#, refund of INR #refundAmount# has been processed for your booking #bookingId#. Thank you. Evibe.in'
			]
		],
		'invalid_email'  => [
			'customer' => 'Hi #field1#, given email id #field2# is not valid. Pls. provide correct email id here: #field3# to receive all your party related updates. Evibe.in',
		],
		'customer_proof' => [
			'accept' => "Hi #customer#, your uploaded ID proof validation is successful. You can track and manage your order here: #tmoLink# Evibe.in",
			'reject' => "Hi #customer#, we regret to inform that there is some issue with the ID proof that you have uploaded. Kindly re-upload ASAP to avoid cancellation: #tmoLink# Evibe.in"
		],
		'piab'           => [
			'update' => [
				'customer' => "Hi #customer#, your order (#orderId#) is #status#. Use this link #tmoLink# to track your order. Queries? Write to #piabSupportEmail# Team Evibe."
			],
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Track & Reporting
	|--------------------------------------------------------------------------
	|
	| These values should be in sync with database table type_scenario
	*/
	'scenario'              => [
		'track'  => 1,
		'report' => 2
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket status
	|--------------------------------------------------------------------------
	*/
	'ticket'                => [
		'status'         => [
			'initiated'        => 1,
			'progress'         => 2,
			'confirmed'        => 3,
			'booked'           => 4,
			'cancelled'        => 5,
			'enquiry'          => 6,
			'followup'         => 7,
			'autopay'          => 8,
			'autocancel'       => 9,
			'rejected'         => env('STATUS_REJECTED', 10),
			'redeemed'         => env('STATUS_REDEEMED', 11),
			'service_auto_pay' => env('STATUS_SERVICE_AP', 12),
			'not_interested'   => env('STATUS_NOT_INTERESTED', 13),
			'duplicate'        => env('STATUS_DUPLICATE', 14),
			'related'          => env('STATUS_RELATED', 15),
			'irrelevant'       => env('STATUS_IRRELEVANT', 16),
			'no_response'      => env('STATUS_NO_RESPONSE', 17),
			'invalid_email'    => env('STATUS_INVALID_EMAIL', 18)
		],
		'source'         => [
			'website'    => env('SOURCE_WEBSITE', 1),
			'justDial'   => env('SOURCE_JUSTDIAL', 18),
			'auto'       => env('SOURCE_AUTO', 6),
			'partnerApp' => env('SOURCE_PARTNERAPP', 25)
		],
		'type_booking'   => [
			'photo'         => env('TYPE_BOOKING_PHOTO', 12),
			'video'         => env('TYPE_BOOKING_VIDEO', 13),
			'entertainment' => env('TYPE_BOOKING_ENT', 6)
		],
		'type_update'    => [
			'phone'  => 'Phone',
			'email'  => 'Email',
			'auto'   => 'Auto',
			'manual' => 'Manual',
			'other'  => 'Other'
		],
		'success_type'   => [
			'confirmed' => 1,
			'booked'    => 2
		],
		'enquiry_source' => [
			'phone'             => 1,
			'header_enquiry'    => 2,
			'chat'              => 3,
			'feasibility_check' => 4,
			'autobook'          => 5,
			'cd_decor'          => env('SOURCE_CUSTOM_DECOR', 6),
			'cd_food'           => env('SOURCE_CUSTOM_FOOD', 7),
			'cd_cake'           => env('SOURCE_CUSTOM_CAKE', 8),
			'product_enquiry'   => env('SOURCE_PRODUCT_ENQUIRY', 9),
			'direct'            => 10,
			'direct_copy'       => 11,
			'pb_enquiry'        => 12,
			'home_enquiry'      => 13,
			'google_adwords'    => 14,
			'workflow_enquiry'  => env('SOURCE_WORKFLOW_ENQUIRY', 18)
		],
		'type'           => [
			'packages'           => env('TYPE_PAGE_PACKAGE', 1),
			'planners'           => env('TYPE_PAGE_PLANNER', 2),
			'venues'             => env('TYPE_PAGE_VENUE', 3),
			'services'           => env('TYPE_PAGE_SERVICE', 4),
			'trends'             => env('TYPE_PAGE_TREND', 5),
			'cakes'              => env('TYPE_PAGE_CAKE', 6),
			'home_page'          => 7,
			'no_results'         => 8,
			'dash'               => 9,
			'header'             => 10,
			'error'              => 11,
			'experience'         => 12,
			'decors'             => env('TYPE_PAGE_DECOR', 13),
			'entertainments'     => env('TYPE_PAGE_ENT', 14),
			'venue_halls'        => 15,
			'artists'            => 16,
			'resort'             => 17,
			'villa'              => env('TYPE_PAGE_VILLA', 18),
			'lounge'             => env('TYPE_PAGE_LOUNGE', 19),
			'food'               => env('TYPE_PAGE_FOOD', 20),
			'couple-experiences' => env('TYPE_PAGE_CE', 21),
			'venue-deals'        => env('TYPE_PAGE_VENUE_DEAL', 22),
			'priests'            => env('TYPE_PAGE_PRIEST', 23),
			'tents'              => env('TYPE_PAGE_TENT', 24),
			'party_bag'          => 25,
			'campaign'           => 26,
			'generic-package'    => env('TYPE_PAGE_GENERIC_PACKAGE', 27),
			'add-on'             => env('TYPE_PAGE_ADD_ON', 28)
		],

		'enq' => [
			'pre' => [
				'home_page'         => 'ENQ-HP-',
				'no_result'         => 'ENQ-NR-',
				'header'            => 'ENQ-HR-',
				'planner'           => 'ENQ-PL-',
				'venue'             => 'ENQ-VE-',
				'service'           => 'ENQ-SE-',
				'package'           => 'ENQ-PA-',
				'experience'        => 'ENQ-EX-',
				'trend'             => 'ENQ-TR-',
				'cake'              => 'ENQ-CK-',
				'decor'             => 'ENQ-DS-',
				'ent_addon'         => 'ENQ-SR-',
				"custom_ticket"     => "EVB-CT-",
				'auto_booked'       => 'EVB-AU-',
				'site_visit'        => 'EVB-SM-',
				'customer_workflow' => 'ENQ-CW-',
				'ad_campaign'       => 'ENQ-AD-'
			]
		],

		'leadStatus' => [
			"hot"    => 1,
			"medium" => 2,
			"cold"   => 3
		],

		'bookingLikeliness' => [
			"1" => "Immediately",
			"2" => "In a day",
			"6" => "In 3 days",
			"3" => "In a week",
			"4" => "In a month",
			"5" => "Just started planning"
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Ticket status message
	|--------------------------------------------------------------------------
	*/
	'ticket_status_message' => [
		'booking_edit'       => 'Ticket booking edited',
		'order_process'      => 'Order process email sent',
		'more_info_edit'     => 'Booking more info edited',
		'payment_received'   => 'Payment received',
		'cancelled'          => 'Ticket got cancelled',
		'booking_deleted'    => 'Ticket booking deleted',
		'custom_quote'       => 'Custom quote email sent',
		'recommendation'     => 'Recommendation email sent',
		'exchange_contact'   => 'Contacts exchanged',
		'booking_finalize'   => 'Ticket booking finalized',
		'custom_email'       => 'Custom email sent',
		'no_response'        => 'No response notification sent to customer',
		'cancel_email'       => 'Cancel email sent to customer & vendor manually',
		'cancel_toggle_on'   => 'Auto cancel email toggle switched on',
		'cancel_toggle_off'  => 'Auto cancel toggle switched off',
		'reopen'             => 'Ticket reopened',
		'edit_ticket'        => 'Ticket information has been edited',
		'receipt_automatic'  => 'Booked receipt sent automatically after payment',
		'receipt_manual'     => 'Booked receipt sent manually via Dash',
		'availability_asked' => 'Venue availability asked',
	],

	/*
	|--------------------------------------------------------------------------
	| Notification type
	|--------------------------------------------------------------------------
	*/
	'enquiry'               => [
		'type' => [
			'avlCheck'    => env('ENQ_AVL_CHECK_ID', 2),
			'customQuote' => env('ENQ_CUSTOM_QUOTE_ID', 4),
		]
	],

	/*
	|--------------------------------------------------------------------------
	| Role Id
	|--------------------------------------------------------------------------
	*/
	'role'                  => [
		'super_admin'      => env('ROLE_SUPER_ADMIN', 1),
		'admin'            => env('ROLE_ADMIN', 2),
		'planner'          => env('ROLE_PLANNER', 3),
		'venue'            => env('ROLE_VENUE', 4),
		'bd'               => env('ROLE_BD', 5),
		'customer_delight' => env('ROLE_CUSTOMER_DELIGHT', 6),
		'artist'           => env('ROLE_ARTIST', 7),
		'operations'       => env('ROLE_OPERATIONS', 8),
		'sr_crm'           => env('ROLE_SENIOR_CRM', 9),
		'invite_usr'       => env('ROLE_INVITE_USER', 10),
		'marketing'        => env('ROLE_MARKETING', 11),
		'tech'             => env('ROLE_TECH', 12),
		'customer'         => env('ROLE_CUSTOMER', 13),
		'sr_bd'            => env('ROLE_SENIOR_BD', 14),
		'ops_agent'        => env('ROLE_OPS_AGENT', 16),
		'ops_tl'           => env('ROLE_OPS_TL', 17),
		'bd_agent'         => env('ROLE_BD_AGENT', 18),
		'bd_tl'            => env('ROLE_BD_TL', 19),
		'id'               => [
			env('ROLE_SUPER_ADMIN', 1)      => 'Super Admin',
			env('ROLE_ADMIN', 2)            => 'Admin',
			env('ROLE_PLANNER', 3)          => 'Planner',
			env('ROLE_VENUE', 4)            => 'Venue',
			env('ROLE_BD', 5)               => 'BD',
			env('ROLE_CUSTOMER_DELIGHT', 6) => 'Customer Delight',
			env('ROLE_ARTIST', 7)           => 'Artist',
			env('ROLE_OPERATIONS', 8)       => 'Operations',
			env('ROLE_SENIOR_CRM', 9)       => 'Sr Crm',
			env('ROLE_INVITE_USER', 10)     => 'Invite_usr',
			env('ROLE_MARKETING', 11)       => 'Marketing',
			env('ROLE_TECH', 12)            => 'Tech',
			env('ROLE_CUSTOMER', 13)        => 'Customer',
			env('ROLE_SENIOR_BD', 14)       => 'Sr BD',
			env('ROLE_OPS_AGENT', 16)       => 'OPS Agent',
			env('ROLE_OPS_TL', 17)          => 'OPS Team Lead',
			env('ROLE_BD_AGENT', 18)        => 'BD Agent',
			env('ROLE_BD_TL', 19)           => 'OPS Team Lead',
		]
	],

	/*
	|----------------------------------------------------------------------------
	| Partner app key, being used for app notification for partner application
	|----------------------------------------------------------------------------
	*/
	'partner_app_key'       => [
		'enquiry'   => [
			'list'    => 'enquiry_list',
			'details' => 'enquiry_details'
		],
		'order'     => [
			'list'    => 'order_list',
			'details' => 'order_details'
		],
		'delivery'  => [
			'list'    => 'delivery_list',
			'details' => 'delivery_details'
		],
		"test_user" => 202
	],

	'gallery' => [
		'youtube_host'        => 'https://www.youtube.com/watch?v=',
		'type'                => [
			'image' => 0,
			'video' => 1,
			'link'  => 2
		],
		'default_profile_pic' => [
			'root'    => 'img/app',
			'package' => 'default_package.jpg',
			'venue'   => 'default_venue.png',
			'planner' => 'default_planner.png',
			'trend'   => 'default_planner.png',  // @todo: change this to trends
			'decor'   => 'default_planner.png', // @todo: change this to decors
			'service' => 'default_planner.png' // @todo: change this to service
		],
	],

	'live' => [
		'host'        => env('LIVE_HOST', 'https://evibe.in'),
		'checkout'    => env('LIVE_CHECKOUT', 'https://evibe.in/pay/checkout'),
		'profile_url' => [
			'planner'           => 'birthday-party-planners',
			'venues'            => 'birthday-party-planners/venues',
			'packages'          => 'birthday-party-planners/packages',
			'experiences'       => 'special-couple-experiences',
			'trends'            => 'birthday-party-planners/trends',
			'cakes'             => 'birthday-party-planners/cakes',
			'decors'            => 'birthday-party-planners/birthday-party-decorations',
			'entertainment'     => 'birthday-party-planners/entertainment-activities-stalls',
			'bachelor'          => [
				'resorts'       => 'bachelor-party/resorts',
				'villas'        => 'bachelor-party/villas',
				'lounges'       => 'bachelor-party/lounges',
				'entertainment' => 'bachelor-party/entertainment',
				'food'          => 'bachelor-party/food',
				'cakes'         => 'bachelor-party/cakes',
			],
			'pre-post'          => [
				'venues'        => 'engagement-wedding-reception/venues',
				'decors'        => 'engagement-wedding-reception/decorations/all',
				'entertainment' => 'engagement-wedding-reception/entertainment',
				'cakes'         => 'engagement-wedding-reception/cakes'
			],
			'couple_experience' => [
				'packages' => 'special-couple-experiences/packages'
			],
		],
	],

	'google' => [
		're_cap_site'   => env('RE_CAP_SITE', '6LelxSITAAAAAKBxOWQTDSRUtNcapYZvgMu8daz5'),
		're_cap_secret' => env('RE_CAP_SECRET', '6LelxSITAAAAAGiaySEJd4l7gDKol5W0dAWiQLsO'),
		'map_key'       => env('GOOGLE_MAP_KEY', 'AIzaSyC6FR0Ajmt-28vUUwSfLMluBIkCezeilRU'),
		'shortener_key' => env('GOOGLE_SHORTENER_KEY', 'AIzaSyC6FR0Ajmt-28vUUwSfLMluBIkCezeilRU')
	],

	'bitly' => [
		'access_token'     => env('BITLY_ACCESS_TOKEN'),
		'alt_access_token' => env('BITLY_ALT_ACCESS_TOKEN'),
	],

	'evibes' => [
		'access_token' => env('EVIBES_SHORTEN_ACCESS_TOKEN', "11feae8ebff7ca417beab3586e0f8a")
	],

	'checkout_field'      => [
		'cake' => [
			'message'         => env('CHECKOUT_CAKE_MESSAGE', 1),
			'delivery_slot'   => env('CHECKOUT_CAKE_DELIVERY_SLOT', 155),
			'delivery_charge' => env('CHECKOUT_CAKE_DELIVERY_CHARGE', 156),
			'type'            => env('CHECKOUT_CAKE_TYPE', 21),
			'weight'          => env('CHECKOUT_CAKE_WEIGHT', 22),
			'flavour'         => env('CHECKOUT_CAKE_FLAVOUR', 23)
		],
	],

	/*
	 * Type Reminder
	 */
	'type_reminder'       => [
		'followup'       => env('TYPE_REMINDER_FOLLOWUP', 1),
		'thank-you-card' => env('TYPE_REMINDER_THANK_YOU_CARD', 2),
		'retention'      => env('TYPE_REMINDER_RETENTION', 3)
	],

	/*
	 * Type Reminder Group [should be in sync with 'type_reminder_group' table]
	 */
	'type_reminder_group' => [
		'followup'  => [
			'type' => [
				'case_not_viewed' => 1,
				'case_viewed'     => 2
			],
			'rr1'  => 1,
			'rr2'  => 2,
			'rr3'  => 3,
			'rr4'  => 4,
			'rr5'  => 5,
			'rr6'  => 6,
			'rr7'  => 7,
			'rr8'  => 8,
			'rr9'  => 9,
			'rr10' => 10,
			'rr11' => 11,
		],
		'retention' => [
			'rt1' => env('RETENTION_REMINDER_1', 13),
			'rt2' => env('RETENTION_REMINDER_2', 14),
			'rt3' => env('RETENTION_REMINDER_3', 15),
			'rt4' => env('RETENTION_REMINDER_4', 16),
			'rt5' => env('RETENTION_REMINDER_5', 17)
		]
	],

	'reminders' => [
		'followup_forward'  => 7,
		'followup_backward' => 7
	],

	'email_error' => env('EMAIL_ERROR', true),
	'project_id'  => env('PROJECT_ID_API', 3),

	'partner_type' => [
		'dash_link' => [
			2 => 'vendors',
			3 => 'venues'
		],
		2           => 'Planner',
		3           => 'Venue',
		16          => 'Artist'
	],

	/*
	|--------------------------------------------------------------------------
	| Cities (Must be in sync with 'city' table)
	|--------------------------------------------------------------------------
	|
	| Used for Google places API.
	*/
	'city'         => [
		'Bengaluru'    => env('CITY_BANGALORE_ID', 1),
		'Hyderabad'    => env('CITY_HYDERABAD_ID', 2),
		'Secunderabad' => env('CITY_HYDERABAD_ID', 2),
		'New Delhi'    => env('CITY_DELHI_ID', 3),
		'Mumbai'       => env('CITY_MUMBAI_ID', 4),
		'Pune'         => env('CITY_PUNE_ID', 5)

	],

	'default' => [
		'evibe_service_fee_percent' => 15,
		'evibe_service_fee'         => 0.15,
		'gst'                       => 0.18,
		'partial_gst'               => 0.09,
		'gst_star'                  => 0.05,
		'partial_gst_star'          => 0.025,
		'user_id'                   => env('DEFAULT_USER_ID', 2),
	],

	'error_code' => [
		'create_settlement'   => 15,
		'cancel_booking'      => 16,
		'get_customer_refund' => 17,
		'create_function'     => 21,
		'fetch_function'      => 22,
		'update_function'     => 23,
		'delete_function'     => 24
	],

	'bookingLikeliness' => [
		"immediately"           => 1,
		"in_a_day"              => 2,
		"in_3_days"             => 6,
		"in_a_week"             => 3,
		"in_a_month"            => 4,
		"just_started_planning" => 5
	],

	/* question types */
	'question-type'     => [
		'text'     => 1,
		'radio'    => 2,
		'checkbox' => 3,
		'select'   => 4
	],

	'workflow' => [
		'question' => [
			'gender'      => env("WORKFLOW_GENDER_QUESTION_ID", 1),
			'place'       => env("WORKFLOW_PLACE_QUESTION_ID", 2),
			'guest_count' => env("WORKFLOW_GUESTCOUNT_QUESTION_ID", 3)
		],
		'gender'   => [
			'tags' => [
				"boy"     => env("WORKFLOW_BOY_GENDER_ID", 56),
				"girl"    => env("WORKFLOW_GIRL_GENDER_ID", 57),
				"neutral" => env("WORKFLOW_NEUTRAL_GENDER_ID", 58)
			]
		]
	],

	'profile_url' => [
		'decors'        => 'decorations',
		'packages'      => 'packages',
		'trends'        => 'trends',
		'planner'       => 'birthday-planner',
		'venues'        => 'venues',
		'experiences'   => 'unique-celebration-experience',
		'cakes'         => 'cakes',
		'entertainment' => 'entertainment',
		'venue_deals'   => 'venue-deals',
	],

	'type_event_slot' => [
		'1' => 'Breakfast - 7Am',
		'2' => 'Lunch - 11Am',
		'3' => 'HiTea - 3Pm',
		'4' => 'Dinner - 7Pm',
	],

	'type_customer_budget'   => [
		'0',
		'1000',
		'3000',
		'5000',
		'10000',
		'15000',
		'20000',
		'25000',
		'50000',
		'75000',
		'100000'
	],
	'type_customer_relation' => [
		'1' => 'Self',
		'2' => 'Spouse',
		'3' => 'Family',
		'4' => 'Client',
		'5' => 'Friend'
	],

	'customer_preferred_slot' => [
		'Within 1 Hour'                   => [
			'name'    => 'Within 1 Hour',
			'timeMin' => '36000',
			'timeMax' => '72000'
		],
		'Today Morning(10 AM - 1 PM)'     => [
			'name'    => 'Today Morning(10 AM - 1 PM)',
			'timeMin' => '0',
			'timeMax' => '36000'
		],
		'Today Afternoon(1 PM - 3 PM)'    => [
			'name'    => 'Today Afternoon(1 PM - 3 PM)',
			'timeMin' => '0',
			'timeMax' => '46800'
		],
		'Today Evening(3 PM - 6 PM)'      => [
			'name'    => 'Today Evening(3 PM - 6 PM)',
			'timeMin' => '0',
			'timeMax' => '54000'
		],
		'Today Night(6 PM - 8 PM)'        => [
			'name'    => 'Today Night(6 PM - 8 PM)',
			'timeMin' => '0',
			'timeMax' => '64800'
		],
		'Tomorrow Morning(10 AM - 1 PM)'  => [
			'name'    => 'Tomorrow Morning(10 AM - 1 PM)',
			'timeMin' => '46800',
			'timeMax' => '86399'
		],
		'Tomorrow Afternoon(1 PM - 3 PM)' => [
			'name'    => 'Tomorrow Afternoon(1 PM - 3 PM)',
			'timeMin' => '54000',
			'timeMax' => '86399'
		],
		'Tomorrow Evening(3 PM - 6 PM)'   => [
			'name'    => 'Tomorrow Evening(3 PM - 6 PM)',
			'timeMin' => '64800',
			'timeMax' => '86399'
		],
		'Tomorrow Night(6 PM - 8 PM)'     => [
			'name'    => 'Tomorrow Night(6 PM - 8 PM)',
			'timeMin' => '72000',
			'timeMax' => '86399'
		],
	],
	'type_customer_gender'    => [
		'1' => 'Mr',
		'2' => 'Ms'
	],

	'default_partner_id' => env("DEFAULT_PARTNER_ID", 809),

	'track_my_order' => [
		"url" => env("TRACK_MY_ORDER_URL", "/track")
	],

	'avail_check' => [
		"slots" => [
			2 => [0, 39600],
			3 => [39600, 54000],
			4 => [54000, 64800],
			5 => [64800, 86400]
		],

		"ids" => [
			"all"       => 1,
			"breakfast" => 2,
			"lunch"     => 3,
			"hi_tea"    => 4,
			"dinner"    => 5
		]
	],

	'token'                  => [
		'track-my-order' => 'EVBTMO',
	],

	/*
	 * PIAB booking status
	 */
	'product-booking-status' => [
		'initiated'        => env('PRODUCT_BOOKING_STATUS_INITIATED', 1),
		'pain'             => env('PRODUCT_BOOKING_STATUS_PAID', 2),
		'accepted'         => env('PRODUCT_BOOKING_STATUS_ACCEPTED', 3),
		'shipped'          => env('PRODUCT_BOOKING_STATUS_SHIPPED', 4),
		'in-transit'       => env('PRODUCT_BOOKING_STATUS_IN_TRANSIT', 5),
		'out-for-delivery' => env('PRODUCT_BOOKING_STATUS_OUT_FOR_DELIVERY', 6),
		'delivered'        => env('PRODUCT_BOOKING_STATUS_DELIVERED', 7),
		'cancelled'        => env('PRODUCT_BOOKING_STATUS_CANCELLED', 8)
	],

	'default-category-tag' => [
		env('TYPE_PAGE_DECOR', 13)  => env('ALL_DECORS_CATEGORY_ID', 323),
		env('TYPE_PAGE_CAKE', 6)    => env('ALL_CAKES_CATEGORY_ID', 324),
		env('TYPE_PAGE_SERVICE', 4) => env('ALL_SERVICES_CATEGORY_ID', 325),
		env('TYPE_PAGE_PACKAGE', 1) => env('ALL_KIDS_PACKAGES_CATEGORY_ID', 326),
		env('TYPE_PAGE_FOOD', 20)   => env('ALL_FOOD_PACKAGES_CATEGORY_ID', 327),
		env('TYPE_PAGE_TENT', 24)   => env('ALL_TENT_PACKAGES_CATEGORY_ID', 328),
		env('TYPE_PAGE_PRIEST', 23) => env('ALL_PRIEST_PACKAGES_CATEGORY_ID', 329),
		env('TYPE_PAGE_VILLA', 18)  => env('ALL_VILLA_PACKAGES_CATEGORY_ID', 330),
		env('TYPE_PAGE_CE', 21)     => env('ALL_SURPRISE_PACKAGES_CATEGORY_ID', 331),
		env('TYPE_PAGE_TREND', 5)   => env('ALL_TRENDS_CATEGORY_ID', 332)
	],
	'event-categories'     => [
		env('EVENT_KIDS_BIRTHDAYS', 1)      => [
			env('TYPE_PAGE_PACKAGE', 1),
			env('TYPE_PAGE_SERVICE', 4),
			env('TYPE_PAGE_TREND', 5),
			env('TYPE_PAGE_CAKE', 6),
			env('TYPE_PAGE_DECOR', 13),
			env('TYPE_PAGE_FOOD', 20),
			env('TYPE_PAGE_PRIEST', 23),
			env('TYPE_PAGE_TENT', 24)
		],
		env('EVENT_HOUSE_WARMING', 14)      => [
			env('TYPE_PAGE_SERVICE', 4),
			env('TYPE_PAGE_CAKE', 6),
			env('TYPE_PAGE_DECOR', 13),
			env('TYPE_PAGE_FOOD', 20),
			env('TYPE_PAGE_PRIEST', 23),
			env('TYPE_PAGE_TENT', 24)
		],
		env('EVENT_BACHELOR_PARTY', 11)     => [
			env('TYPE_PAGE_CAKE', 6),
			env('TYPE_PAGE_VILLA', 18)
		],
		env('EVENT_PRE_POST', 13)           => [
			env('TYPE_PAGE_CAKE', 6),
			env('TYPE_PAGE_DECOR', 13),
			env('TYPE_PAGE_FOOD', 20)
		],
		env('EVENT_COUPLE_EXPERIENCES', 16) => [
			env('TYPE_PAGE_CE', 21)
		],
		env('EVENT_CORPORATE_EVENTS', 5)    => [
			env('TYPE_PAGE_SERVICE', 4),
			env('TYPE_PAGE_DECOR', 13)
		],

	],
];
