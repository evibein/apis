## Evibe API

Evibe API is a list of APIs to power a series of mobile applications namely Evibe Pro, Invibe etc.

### Installation procedure
1. Clone/checkout the code from GIT.
2. Install composer[https://getcomposer.org/download/].
3. Go to your project folder in terminal and run `composer install`.
4. Copy .env.example to .env and change the DB credentials.
5. You are good to go!