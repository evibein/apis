<?php

$app->group(['namespace' => 'App\Http\Controllers'], function ($app) {
	/**
	 * Client CRUD end points
	 */
	$app->group(['prefix' => 'v1'], function ($app) {
		$app->get('client', 'AuthClientController@getListOfAuthClients');
		$app->get('client/{clientId}', 'AuthClientController@getASpecificClient');
		$app->post('client', 'AuthClientController@createAuthClient');
		$app->put('client/{clientId}', 'AuthClientController@updateASpecificClient');
		$app->delete('client/{clientId}', 'AuthClientController@deleteASpecificClient');
	});

	/**
	 * Tracking & Reporting Routes - ensure service is delivered on time
	 * Removed by @JR on 04 Jan 2019
	 */
	//$app->group(['prefix' => 'ensure/v1'], function ($app) {
	//	$app->get('track', 'EnsureController@track');
	//	$app->get('record-pin', 'EnsureController@recordPin');
	//	$app->get('record-otp', 'EnsureController@recordOtp');
	//});

	/**
	 * Create coupon code based on the details sent
	 */
	$app->group(['prefix' => 'coupon'], function ($app) {
		$app->post('generate', 'BaseController@createCouponRestricted');
	});

	/**
	 * Product Related
	 */
	$app->group(['prefix' => 'product'], function ($app) {
		$app->post('availability-check', 'ProductController@handleAvailabilityCheck');
	});

	/**
	 * Controller used for watermarking images
	 * Removed by @JR on 04 Jan 2019
	 */
	// $app->group(['prefix' => 'images'], function ($app) {
	// 	$app->post('water-mark-images-a9640204000', 'ImageController@processInputFromWaterMarkConsole');
	// 	$app->post('remove-watermark-a9640204000', 'ImageController@removeWaterMarkConsole');
	// 	$app->post('remove-water-mark-images', 'ImageController@removeWaterMarkOnSelectedImages');
	// 	$app->get('delete-defunct', 'ImageController@deleteDefunctImages');
	// });

	/**
	 * Tickets update
	 */
	$app->group(['prefix' => 'tickets'], function ($app) {
		$app->get('update', 'TicketController@updateCustomerInfo');
	});

	/**
	 * Review mapping
	 */
	$app->group(['prefix' => 'reviews-mapping'], function ($app) {
		$app->get('pending-reviews', 'OptionReviewController@showUnmappedReviews');
		$app->get('mapped-reviews', 'OptionReviewController@showMappedReviews');
		$app->get('get-options', 'OptionReviewController@getOptionsByType');
		$app->post('save', 'OptionReviewController@saveReviewMapping');
	});

	/**
	 * Availabilty check
	 */
	$app->group(['prefix' => 'avail-checks'], function ($app) {
		$app->get('info', 'AvailabilityCheckController@showTicketAvailChecks');
		$app->get('get-options', 'AvailabilityCheckController@getOptionsByType');
		$app->get('get-option-info', 'AvailabilityCheckController@getInfo');
		$app->post('post-avail-check', 'AvailabilityCheckController@postAvailCheck');
	});

	/**
	 * Common APIs for different application
	 */
	$app->group(['prefix' => 'common'], function ($app) {
		/**
		 * Customer workflow
		 */
		$app->group(['prefix' => 'tickets/v1'], function ($app) {
			// Basic Party Details (screen 1)
			$app->get('details/basic/{ticketId}', 'TicketController@getBasicPartyDetails');
			$app->post('details/basic', 'TicketController@createTicketWithBasicDetails');
			$app->put('details/basic/{ticketId}', 'TicketController@updateBasicPartyDetails');

			// Additional Party Details (screen 2)
			$app->get('details/additional/{ticketId}', 'TicketController@getAdditionalPartyDetails');
			$app->post('details/additional/{ticketId}', 'TicketController@saveAdditionalPartyDetails');
			$app->put('details/additional/{ticketId}', 'TicketController@updateAdditionalPartyDetails');

			// Service Details (screen 3)
			$app->get('services/requirements/{ticketId}', 'TicketController@getServicesRequirementFields');
			$app->get('services/{ticketId}', 'TicketController@getPartyServices');
			$app->post('services/{ticketId}', 'TicketController@saveServicesRequirements');
			$app->put('services/{ticketId}', 'TicketController@updateServicesRequirements');

			// Contact Details (screen 4)
			$app->get('details/contact/{ticketId}', 'TicketController@getContactDetails');
			$app->post('details/contact/{ticketId}', 'TicketController@saveContactDetails');
			$app->put('details/contact/{ticketId}', 'TicketController@updateContactDetails');

			// Party Status Details (screen 5)
			$app->get('details/status/{ticketId}', 'TicketController@getStatusDetails');
			$app->post('details/status/{ticketId}', 'TicketController@saveStatusDetails');
			$app->put('details/status/{ticketId}', 'TicketController@updateStatusDetails');

			// CS2A Recommendations
			// @todo: (may be) change order, service specific options
			$app->post('suggestions/{ticketId}', 'TicketController@createTicketMapping');
			$app->delete('suggestions/{ticketId}', 'TicketController@deleteTicketMapping');
			$app->get('suggestions/{ticketId}', 'TicketController@getServiceStartOverOptions');
			$app->get('suggestions/{ticketId}/{serviceId}', 'TicketController@getServiceSpecificOptions');

			$app->get('recos/{ticketId}/preview', 'TicketController@previewShortlistedOptions');
			$app->get('recos/{ticketId}/send', 'TicketController@sendRecommendations');

		});

		$app->group(['prefix' => 'enquiries/v1'], function ($app) {
			$app->post('create', 'EnquiriesController@createEnquiry');
		});

		/**
		 * Ticket Booking related
		 */
		$app->group(['prefix' => 'bookings/v1'], function ($app) {
			$app->put('{ticketBookingId}/partner-change', 'TicketBookingController@changeBookingPartner');
			$app->put('{ticketBookingId}/update', 'TicketBookingController@updateBookingDetails');
			$app->post('{ticketId}/add', 'TicketBookingController@addBooking');
		});

		/**
		 * Auto Followups
		 */
		$app->group(['prefix' => 'auto-followups/v1'], function ($app) {
			$app->post('{ticketId}/create/{typeReminderId}', 'AutoFollowupController@createAutoFollowup');
			$app->put('{ticketId}/stop/{typeReminderId}', 'AutoFollowupController@stopAutoFollowup');
			$app->put('{ticketId}/recalculate/{typeReminderId}', 'AutoFollowupController@recalculateAutoFollowup');
			$app->get('test/followup-reminders', 'AutoFollowupController@testFollowupReminderCases');
			$app->post('create/retention-reminders', 'AutoFollowupController@createRetentionReminders');
		});

		/**
		 * Finance - Settlements
		 */
		$app->group(['prefix' => 'finance/v1'], function ($app) {
			$app->get('bookings/{query}', 'AutoSettlementController@getBookingSettlementsList');
			$app->post('bookings/{ticketBookingId}/create', 'AutoSettlementController@createScheduledBookingSettlement');
			$app->put('bookings/{ticketBookingId}/halt', 'AutoSettlementController@haltScheduledBookingSettlement');
			$app->put('bookings/{ticketBookingId}/update', 'AutoSettlementController@updateScheduledBookingSettlement');
			$app->put('bookings/{ticketBookingId}/delete', 'AutoSettlementController@deleteScheduledBookingSettlement');
			$app->get('adjustments/{query}', 'AutoSettlementController@getPartnerAdjustmentsList');
			$app->post('adjustments/{partnerId}/{partnerTypeId}/create', 'AutoSettlementController@createPartnerAdjustment');
			$app->put('adjustments/{adjustmentId}/update', 'AutoSettlementController@updatePartnerAdjustment');
			$app->put('adjustments/{adjustmentId}/delete', 'AutoSettlementController@deletePartnerAdjustment');
			$app->get('settlements/{query}', 'AutoSettlementController@getSettlementsList');
			$app->post('settlements/create', 'AutoSettlementController@createSettlements');
			$app->get('settlements/{settlementId}/info', 'AutoSettlementController@getSettlementDetails');
			$app->post('settlements/{partnerId}/{partnerTypeId}/create', 'AutoSettlementController@createPartnerSettlement');
			$app->put('settlements/{settlementId}/process', 'AutoSettlementController@processSettlement');
			$app->get('settlements/download/{query}', 'AutoSettlementController@fetchSettlementsSheet');
			$app->get('quick-settlements', 'AutoSettlementController@getQuickSettlementsData');
			$app->put('quick-settlements/process', 'AutoSettlementController@processQuickSettlement');

			/**
			 * Additional extra routes for settlements
			 */
			$app->post('settlements/create-invoice/{settlementId}', 'AutoSettlementController@createPartnerSettlementInvoice');

			/**
			 * Refunds
			 */
			$app->group(['prefix' => 'refunds'], function ($app) {
				$app->get('bookings/{query}', 'RefundController@getCustomerRefunds');
				$app->post('bookings/{ticketBookingId}/create', 'RefundController@createCustomerRefund');
				$app->put('bookings/{ticketBookingId}/update', 'RefundController@updateCustomerRefund');
				$app->put('bookings/{ticketBookingId}/approve', 'RefundController@approveCustomerRefund');
				$app->put('bookings/{ticketBookingId}/reject', 'RefundController@rejectCustomerRefund');
				$app->put('bookings/{ticketBookingId}/halt', 'RefundController@haltScheduledRefund');
			});

			/**
			 * Cancellations
			 */
			$app->group(['prefix' => 'cancellations'], function ($app) {
				$app->get('bookings/{ticketBookingId}/calculate', 'CancellationController@fetchCancellationCharges');
				$app->put('bookings/{ticketBookingId}/cancel', 'CancellationController@cancelTicketBooking');
				$app->get('bookings/{ticketBookingId}/resend-cancel-receipts', 'CancellationController@resendBookingCancellationReceipts');
			});
		});

		/**
		 * Refer & Earn
		 */
		$app->group(['prefix' => 're/v1', 'namespace' => 'ReferAndEarn'], function ($app) {
			$app->post('/', 'ReferAndEarnController@generateUniqueReferralLink');
			$app->post('friend/signup/', 'ReferAndEarnController@submitFriendSignup');
			$app->get('statistics/{userId}', 'ReferAndEarnController@showUserReferAndEarnStatistics');
		});

		/**
		 * Workflow
		 * Removed by @JR on 04 Jan 2019
		 */
		// $app->group(['prefix' => 'plan/v1'], function ($app) {
		// 	$app->post('/', 'PartyPlanningController@savePlanningDetails');
		// 	$app->get('/', 'PartyPlanningController@getPlanningDetails');
		// 	$app->get('{partyPlanToken}', 'PartyPlanningController@getPlanningDetails');
		// 	$app->put('{partyPlanToken}', 'PartyPlanningController@updatePlanningDetails');
		// 	$app->get('{partyPlanToken}/options', 'PartyPlanningController@getOptions');
		// 	$app->get('{partyPlanToken}/more-options/{serviceId}', 'PartyPlanningController@getMoreOptions');
		// 	$app->post('{partyPlanToken}/shortlist', 'PartyPlanningController@createShortlistOption');
		// 	$app->delete('{partyPlanToken}/shortlist', 'PartyPlanningController@deleteShortlistOption');
		// 	$app->get('{partyPlanToken}/shortlist', 'PartyPlanningController@getShortlistedOptions');
		// 	$app->post('{partyPlanToken}/enquire', 'PartyPlanningController@postEnquiry');
		// });

		/**
		 * Delivery Images
		 */
		$app->group(['prefix' => 'partner/delivery-images', 'namespace' => 'DeliveryImages'], function ($app) {
			$app->post('/', 'SelectedDeliveryImagesController@getSelectedDeliveryImages');
		});

		/**
		 * Inside availability check console
		 */
		$app->group(['prefix' => 'partner/avail-check', 'namespace' => 'AvailCheck'], function ($app) {
			$app->post('initialize', 'AvailCheckController@getInitializedData');
			$app->post('add/{partnerCode}', 'AvailCheckController@storeUnavailableSlot');
			$app->post('edit/{partnerCode}', 'AvailCheckController@editUnavailableSlot');
			$app->post('delete/{partnerCode}', 'AvailCheckController@deleteUnavailableSlot');
			$app->post('check/{partnerCode}', 'AvailCheckController@checkUnavailableSlot');
			$app->post('venue/{partnerTypeId}/{partnerCode}', 'AvailCheckController@getSpecificPartnerVenueData');
			$app->post('{partnerTypeId}/{partnerCode}', 'AvailCheckController@getSpecificPartnerData');
		});
	});

	// AWS SNS
	$app->group(['prefix' => 'aws/v1', 'namespace' => 'AWS'], function ($app) {
		$app->group(['prefix' => 'sns'], function ($app) {
			$app->post('subscribe', 'SNSController@subscribeNotifications');
			$app->post('update-email', 'SNSController@updateCustomerEmails');
		});
	});

	// InvibeApp related routes
	$app->group(['prefix' => 'invibe/v1', 'namespace' => 'Invibe'], function ($app) {
		/**
		 * Invibe users end points
		 */
		$app->post('user/signup', 'InvibeUserController@signup');
		$app->get('user/login', 'InvibeUserController@login');
		$app->post('user/otp/verify', 'InvibeUserController@verifyOtp');
		$app->post('user/otp/resend', 'InvibeUserController@resendOtp');
		$app->get('user/{usrId}', 'InvibeUserController@getUserDetails');
		$app->put('user/{usrId}/profile', 'InvibeUserController@updateUserProfile');
		$app->put('user/{usrId}/profile-pic', 'InvibeUserController@updateProfilePic');

		/**
		 * Event type and themes
		 * Removed by @JR on 04 Jan 2019
		 */
		// $app->get('event-type', 'EventTypeController@getAllEventTypes');
		// $app->get('test-job', 'EventTypeController@testJob');

		/**
		 * Event related end points
		 * Removed by @JR on 04 Jan 2019
		 */
		// $app->get('event/count', 'EventController@getEventCount');
		// $app->get('event', 'EventController@getAllEvents');
		// $app->get('event/{evtId}', 'EventController@getASpecificEvent');
		// $app->post('event', 'EventController@createANewEvent');
		// $app->put('event/{evtId}', 'EventController@updateAParticularEvent');
		// $app->delete('event/{evtId}', 'EventController@deleteAParticularEvent');
		// $app->post('event/{evtId}/guests', 'EventController@addGuestsToEvent');
		// $app->get('event/{evtId}/guests', 'EventController@getGuestsForEvent');
	});

	/**
	 * IVR CRM Integrations: MyOperator + Just Dial
	 */
	$app->group(['prefix' => 'crm/v1', 'namespace' => 'CRM'], function ($app) {
		$app->post('integrate', 'CRMController@integrateMyOperator');

		$app->get('just-dial', 'CRMController@saveLeadFromJustDial');

		// Exotel
		$app->get('integrate/exotel', 'CRMController@integrateExotel');

		// Exotel missed call campaign
		//$app->get('integrate/exotel-missed-camp', 'CRMController@exotelMissedCallCampaign');
	});

	/**
	 * CRM Metrics
	 */
	$app->group(['prefix' => 'crm/dashboard/tickets', 'namespace' => 'CRM'], function ($app) {
		$app->get('new', 'CRMDashboardController@showNewTickets');
		$app->get('progress', 'CRMDashboardController@showInProgressTickets');
		$app->get('return', 'CRMDashboardController@showReturnTickets');
		$app->get('missed', 'CRMDashboardController@showReturnTickets');
		$app->get('prospect', 'CRMDashboardController@showReturnTickets');
		$app->get('history', 'CRMDashboardController@showHistoryTickets');
		$app->get('updates/{ticketId}', 'CRMDashboardController@showReturnTicketUpgrades');
		$app->get('history/{ticketId}', 'CRMDashboardController@showReturnTicketUpgrades');
		$app->get('avail-checks', 'CRMDashboardController@showAvailchecks');
		$app->get('metrics/new-tickets', 'CRMDashboardController@showMetricsNewTicketsData');
		$app->get('metrics/followups', 'CRMDashboardController@showMetricsFollowupsData');
		$app->get('metrics/in-progress', 'CRMDashboardController@showMetricsInProgressData');
	});

	/**
	 * Sales Dashboard
	 */
	$app->group(['prefix' => 'sales/dashboard/tickets', 'namespace' => 'CRM'], function ($app) {
		$app->get('new', 'CRMDashboardController@showSalesNewTickets');
		$app->get('followup', 'CRMDashboardController@showSalesFollowupTickets');
		$app->get('admin-data', 'SalesDashboardController@getBookingsDataForTL');
	});

	/**
	 * OPS Metrics
	 */
	$app->group(['prefix' => 'ops/dashboard', 'namespace' => 'OPS'], function ($app) {
		$app->get('tabs-count', 'OpsDashboardController@fetchTabsCount');
		$app->group(['prefix' => 'order-confirmations'], function ($app) {
			$app->get('/', 'OpsDashboardController@showOrderConfirmationTickets');
			$app->POST('confirm-partner', 'OpsDashboardController@confirmPartner');
			$app->get('search', 'OpsDashboardController@searchOrderConfirmationsData');
		});
		$app->group(['prefix' => 'customer-updates'], function ($app) {
			$app->get('/', 'OpsDashboardController@showCustomerUpdates');
			$app->get('search', 'OpsDashboardController@searchCustomerUpdatesData');
		});
		$app->group(['prefix' => 'customer-reviews'], function ($app) {
			$app->get('/', 'OpsDashboardController@showCustomerReviews');
			$app->get('resolve', 'OpsDashboardController@resolveCustomerReviews');
		});
		$app->group(['prefix' => 'customer-proofs'], function ($app) {
			$app->get('/', 'OpsDashboardController@showCustomerProofs');
			$app->get('search', 'OpsDashboardController@searchCustomerProofsData');
			$app->post('validate', 'OpsDashboardController@validateCustomerProofs');
		});
		$app->group(['prefix' => 'avail-checks'], function ($app) {
			$app->get('/', 'OpsDashboardController@showAvailChecks');
			$app->get('data', 'OpsDashboardController@fetchAvailabilityData');
			$app->post('confirm', 'OpsDashboardController@confirmAvailabilityCheck');
		});
		$app->group(['prefix' => 'option-availability'], function ($app) {
			$app->get('/', 'OpsDashboardController@showOptionAvailability');
			$app->get('option-slots', 'OpsDashboardController@fetchOptionSlots');
			$app->post('add-unavailability', 'OpsDashboardController@addUnavailability');
		});
	});

	/**
	 * PIAB -dashboard
	 */
	$app->group(['prefix' => 'piab/dashboard', 'namespace' => 'PIAB'], function ($app) {
		$app->group(['prefix' => 'product-bookings'], function ($app) {
			$app->get('/', 'PiabDashboardController@showProductBookings');
			$app->get('info', 'PiabDashboardController@showProductBookingInfo');
			$app->post('update-status', 'PiabDashboardController@updateProductBookingStatus');
		});
	});

	/**
	 *  Evibe.in customer App routes endpoints
	 */
	$app->group(['prefix' => 'customer/v1', 'namespace' => 'Partner'], function ($app) {
		$app->post('login', 'PartnerLoginController@customerLogin');
		$app->post('signup', 'PartnerLoginController@customerSignUp');
	});

	/**
	 *  Evibe.in Partner App routes endpoints
	 */
	$app->group(['prefix' => 'partner/v1', 'namespace' => 'Partner'], function ($app) {
		$app->post('login', 'PartnerLoginController@login');

		// enquiries related routes
		$app->get('enquiries', 'PartnerEnquiryController@getEnquiryList');
		$app->get('enquiries/{id}', 'PartnerEnquiryController@getEnquiryDetails');
		$app->post('enquiries/{id}/reply', 'PartnerEnquiryController@replyEnquiry');
		$app->post('enquiries/initiated/{mappingId}', 'PartnerEnquiryController@initiatedEnquiry');
		$app->post('enquiries/ask/{mappingId}', 'PartnerEnquiryController@askEnquiry');
		$app->post('enquiries/create/ask/{mappingId}', 'PartnerEnquiryController@createAndAskEnquiry');
		$app->post('enquiries/create/mcq', 'PartnerEnquiryController@createAndAskMultipleEnquiry');
		$app->post('enquiries/temp', 'PartnerEnquiryController@generateTestEnquiries');

		// availability check related routes
		$app->post('availability-checks', 'PartnerAvailabilityCheckController@checkOptionsAvailability');

		// order related routes
		$app->get('orders', 'PartnerOrderController@getOrderList');
		$app->get('orders/{id}', 'PartnerOrderController@getOrderDetails');

		// delivery related api
		$app->get('deliveries', 'PartnerDeliveryController@getDeliveryList');
		$app->get('deliveries/{id}', 'PartnerDeliveryController@getDeliveryDetails');
		$app->post('deliveries/{id}/upload', 'PartnerDeliveryController@uploadDeliveryImages');
		$app->delete('deliveries/{id}/image/{imageId}', 'PartnerDeliveryController@deleteDeliveryImage');
		$app->post('deliveries/{id}/done', 'PartnerDeliveryController@markDeliveryAsDone');
		$app->post('deliveries/pending', 'PartnerDeliveryController@sendPendingDeliveryReminders');
		$app->post('deliveries/temp/reset', 'PartnerDeliveryController@resetDeliveriesForDummyUser'); // rest dummy user deliveries

		// api for notification
		$app->post('notification/create', 'BasePartnerController@createPartnerAppNotification');
		$app->post('notification/enquiry-pending', 'PartnerEnquiryController@sendPendingEnquiryNotificationToTeam');

		// referrals related api
		$app->get('referrals', 'PartnerReferralController@getReferralList');
		$app->get('referrals/about', 'PartnerReferralController@getAboutReferral');
		$app->get('referrals/{id}', 'PartnerReferralController@getReferralDetails');
		$app->post('referrals', 'PartnerReferralController@postNewReferral');

		// first activation
		$app->post('first-booking/{partnerId}', 'PartnerFirstActivationController@sendNotification');

		// this APIs are related to the getting all packages of selected partner
		$app->get('options/{mapId}/{mapTypeId}', 'PartnerProfileController@getBasicDetails');
	});

	/**
	 * Payment Notification
	 */
	$app->group(['prefix' => 'payment/notification', 'namespace' => 'Notification'], function ($app) {
		$app->post('send/{ticketId}', 'PaymentNotificationController@handlePaymentNotification');
		$app->post('first-booking/{bookingId}', 'PaymentNotificationController@sendFirstBookingNotification');
	});

	/**
	 * Partner digest report
	 */
	$app->group(['prefix' => 'partner-digest', 'namespace' => 'Partner'], function ($app) {
		$app->post('{providerId}/{providerTypeId}', 'PartnerDigestController@getReportData');
	});

	/**
	 * partner related
	 */
	$app->group(['prefix' => 'common', 'namespace' => 'Partner'], function ($app) {
		$app->group(['prefix' => 'partner'], function ($innerApp) {
			$innerApp->post('review/auto-accept/{status}/{reviewId}', 'PartnerReviewController@sendAutoAcceptRejectReviewMail');
		});
	});
});