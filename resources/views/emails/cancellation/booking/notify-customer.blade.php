<div>
	<p>Namaste {{ $data['customerName'] }},</p>
	@if(isset($data['customerCancelledAt']) && $data['customerCancelledAt'])
		@if(isset($data['optionName']) && $data['optionName'])
			<p>
				Based on your consent, we've cancelled the booking order of
				<b>{{ $data['optionName'] }}</b> for {{ $data['partyDateTime'] }}.
			</p>
		@else
			<p>
				Based on your consent, we've cancelled the booking for {{ $data['partyDateTime'] }}.
			</p>
		@endif
	@else
		@if(isset($data['optionName']) && $data['optionName'])
			<p>
				We regret to inform you that we had to cancel your booking order of
				<b>{{ $data['optionName'] }}</b> for {{ $data['partyDateTime'] }} because of unavailability of the option.
			</p>
		@else
			<p>
				We regret to inform you that we had to cancel your booking for {{ $data['partyDateTime'] }} because of unavailability of the option.
			</p>
		@endif
	@endif
	@if(isset($data['refundAmount']) && $data['refundAmount'])
		<div>Refund Amount of <b>INR {{ $data['refundAmount'] }}</b> will be processed within 2 - 3 business days.</div>
	@endif
	<div style="margin:15px auto;">
		In case you have any queries, please reply to this email. We will respond to you at the earliest.
	</div>
	@if(isset($data['bookedBookings']) && $data['bookedBookings'])
		<p>
			<b><u>Note:</u></b> This cancellation receipt is only for this particular order booking. Not for the entire party order.
		</p>
	@endif
</div>
<div style="margin:15px 0;">
	<div>Thanks in advance,</div>
	@if(isset($data['handlerName']) && $data['handlerName'])
		<div>{{ $data['handlerName'] }}</div>
	@else
		<div><a href="{{ $data['evibeLink'] }}" style='color:#333'>Team Evibe.in</a></div>
	@endif
	<div>
		@if(isset($data['handlerPhone']) && $data['handlerPhone'])
			{{ $data['handlerPhone'] }},
		@endif
		@if(isset($data['evibePlainPhone']) && $data['evibePlainPhone'])
			{{ $data['evibePlainPhone'] }}
		@endif
	</div>
</div>