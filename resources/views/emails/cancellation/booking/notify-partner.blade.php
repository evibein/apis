<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div><a href='https://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png'
								alt='evibe.in'></a></div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px;background:#BF423C;text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/cancel.png' height="20px" width="20px"
						alt='Success'> Party Booking Rejected
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding:5px 30px 20px;background-color:#FFFFFF;">
			<div>
				<p>Dear {{ $data['partnerName'] }},</p>
				@if(isset($data['customerCancelledAt']) && $data['customerCancelledAt'])
					@if(isset($data['optionName']) && $data['optionName'])
						<p>We regret to inform that customer has cancelled the booking order of
							<b>{{ $data['optionName'] }}</b></p> for {{ $data['partyDateTime'] }}.
					@else
						<p>We regret to inform that customer has cancelled the booking for {{ $data['partyDateTime'] }}.
					@endif
				@else
					@if(isset($data['optionName']) && $data['optionName'])
						<p>
							Based on your consent, we've cancelled the booking order of
							<b>{{ $data['optionName'] }}</b> for {{ $data['partyDateTime'] }} as you've confirmed it is NOT AVAILABLE.
							There is no action required from your side.
						</p>
					@else
						<p>
							Based on your consent, we've cancelled the booking for {{ $data['partyDateTime'] }} as you've confirmed it is NOT AVAILABLE.
							There is no action required from your side.
						</p>
					@endif
				@endif
				@if(isset($data['partnerBookings']) && count($data['partnerBookings']))
					<div style="margin-top: 15px;">
						<div>Your orders status for <b>{{ $data['customerName'] }}</b>'s party:</div>
						<table style="border: 1px solid black; width: 100%;">
							<tr>
								<th style="border: 1px solid black; width: 50%;">Order</th>
								<th style="border: 1px solid black; width: 10%;">Booking Amount</th>
								<th style="border: 1px solid black; width: 25%;">Party Date Time</th>
								<th style="border: 1px solid black; width: 15%;">Order Status</th>
							</tr>
							@foreach($data['partnerBookings'] as $partnerBooking)
								<tr>
									<td style="border: 1px solid black; width: 50%; padding: 5px; @if($partnerBooking['bookingStatus']) @else background-color: indianred; @endif">
										<div>{{ $partnerBooking['bookingOption'] }}</div>
										<div>[{{ $partnerBooking['bookingId'] }}]</div>
									</td>
									<td style="border: 1px solid black; width: 10%; padding: 5px; @if($partnerBooking['bookingStatus']) @else background-color: indianred; @endif">&#8377; {{ $partnerBooking['bookingAmount'] }}</td>
									<td style="border: 1px solid black; width: 25%; padding: 5px; @if($partnerBooking['bookingStatus']) @else background-color: indianred; @endif">{{ $partnerBooking['partyDateTime'] }}</td>
									<td style="border: 1px solid black; width: 15%; padding: 5px; @if($partnerBooking['bookingStatus']) @else background-color: indianred; @endif">
										@if($partnerBooking['bookingStatus'])
											<span style="color: green;">Confirmed</span>
										@else
											<b>Cancelled</b>
										@endif
									</td>
								</tr>
							@endforeach
						</table>
					</div>
					@if(isset($data['livePartnerBookingsCount']) && count($data['livePartnerBookingsCount']))
						<div style="margin-top: 5px;">
							<span><u>Note:</u></span>
							<span style="margin-left: 5px;">Orders which are in <b>Confirmed</b> status needs to be <b>executed</b></span>
						</div>
					@endif
				@endif
				<div style="margin:15px auto;">
					In case you have any queries, please reply to this email (or) call you Evibe.in account manager / {{ $data['evibePhone'] }} (ext: 4). We will respond to you at the earliest.
				</div>
			</div>
			<div style="margin:15px 0;">
				<div>Best Regards,</div>
				<div>Your well-wishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a></div>
			</div>
		</div>
	</div>
</div>