<div>
	Namaste {{ $name }},
	<p>
		{!! $body !!}
	</p>
	<p>
		Best,<br/>
		Admin
	</p>
</div>