<td style="padding:10px; width: 100%;vertical-align: top">
	<div style="text-align: left">
		<div style="text-transform: uppercase; color: #ED3E72">
			<b>Cancellation Policy</b>
		</div>
		<div style="margin-top: 10px;">
			<div>In case of party cancellation by the customer, you will receive your fee based on the following policies:</div>
			<div>
				<table style="margin-top: 10px; border: 1px solid #DDDDDD; border-spacing: 0; border-collapse: collapse;">
					<tr>
						<th style="border: 1px solid #DDDDDD; padding: 5px;">Customer Cancellation Time </th>
						<th style="border: 1px solid #DDDDDD; padding: 5px;">You Receive*</th>
					</tr>
					<tr>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">0 - 24 hours before party</td>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">70% of Advance Paid By Customer</td>
					</tr>
					<tr>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">1 day- 3 days before party</td>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">45% of Advance Paid By Cusotmer</td>
					</tr>
					<tr>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">4 days - 10 days before party</td>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">25% of Advance Paid By Customer </td>
					</tr>
					<tr>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">11 days or above before party</td>
						<td style="border: 1px solid #DDDDDD; padding: 5px;">15% of Advance Paid By Customer</td>
					</tr>
				</table>
			</div>
			<div style="margin-top: 10px;">* Fee Percentage will be calculated on Total Advance Amount paid by the customer</div>
		</div>
	</div>
</td>