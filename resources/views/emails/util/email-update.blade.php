<p>Namaste {{ $data['customerName'] }},</p>

<p>
	This is to inform that your email address has been updated to
	<span class="mar-l-2"><b>{{ $data['emailId'] }}</b>@if(isset($data['invalidEmail']) && $data['invalidEmail']) from ({{ $data['invalidEmail'] }}) @endif </span>
	<span>. You will start receiving updates regarding party to this updated email.</span>
</p>

<p>We look forward to being part of your celebration.</p>

<p>
	Happy Partying,<br/>
	Team Evibe.in <br/>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>