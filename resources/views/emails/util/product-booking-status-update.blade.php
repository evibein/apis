<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>

		<div style="font-size:14px;line-height:22px; padding: 5px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ $data['customerName'] }},</div>
				<div style="text-align: left;">
					<div style="margin:15px 0; text-align: center;">
						<span>
							Your order #{{ $data['productBookingId'] }} is <b>{{ $data['status'] }}</b>
						</span>
						@if(isset($data['statusId']) && (($data['statusId'] == $data['productBookingStatusDelivered']) || ($data['statusId'] == $data['productBookingStatusCancelled'])))
						@else
							@if(isset($data['expectedDeliveryDate']) && $data['expectedDeliveryDate'])
								<span>
								and will be delivered by {{$data['expectedDeliveryDate'] }}
							</span>
							@endif
						@endif
						<span>.</span>
						@if(isset($data['updateComment']) && $data['updateComment'])
							<div style="padding:15px;font-style:italic;text-align:center;">
								<span style>&#8220;</span>
								{{$data['updateComment']}}
								<span>&#8222;</span>

							</div>
						@endif
					</div>
				</div>
				@if(isset($data['tmoLink']) && $data['tmoLink'])
					<div style='padding: 25px 0 25px;text-align:center;'>
						<a href='{{ $data['tmoLink'] }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
							Track My Order
						</a>
					</div>
				@endif
			</div>
			<div style="margin:15px 0;">
				<div>Happy Partying,</div>
				<div>Team Evibe</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
		<div style="margin-top: 15px;">
			<div style="border: 1px solid #F5F5F5; padding: 10px; margin-bottom: 15px;">
				<div style="">
					<table style="width: 100%;">
						<tr>
							<td style="width: 30%;">
								<div style="height: 50px; text-align: center;">
									<img style="height: 100%; max-width: 100%;" src="{{ $data['imageUrl'] }}">
								</div>
							</td>
							<td style="width: 50%; font-size: 18px;">
								<div>{!! $data['bookingInfo'] !!}</div>
							</td>
							<td style="width: 20%; font-size: 18px; text-align: right;">
								<div>&#8377; {{ $data['productPrice'] }}</div>
							</td>
						</tr>
					</table>
					<hr>
					<table style="width: 100%;">
						<tr style="border-top: 1px solid #EFEFEF;">
							<td style="width: 80%; text-align: right">
								<div>Product Price</div>
								<div>Shipping Fee</div>
								@if(isset($data['couponDiscount']) && $data['couponDiscount'])
									<div>Discount Amount</div>
								@endif
								<div style="margin-top:10px; font-size: 16px; font-weight: 600;">Total Order Amount</div>
							</td>
							<td style="width: 20%; text-align: right">
								<div>&#8377; {{ $data['productPriceStr'] }}</div>
								<div>
									@if(isset($data['deliveryCharge']) && $data['deliveryCharge'])
										<span>
											&#8377; {{ $data['deliveryChargeStr'] }}
										</span>
									@else
										<span style="color: #30AC15; font-size: 13px;">FREE</span>
									@endif
								</div>
								@if(isset($data['couponDiscount']) && $data['couponDiscount'])
									<div style="color: green;">- &#8377; {{ $data['couponDiscount'] }}</div>
								@endif
								<div style="margin-top:10px; font-size: 16px; font-weight: 600;">&#8377; {{ $data['totalAmountPaidStr'] }}</div>
							</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="margin:15px 0;">
			For any queries over refunds or cancellations, kindly write to support@evibe.in
		</div>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>

