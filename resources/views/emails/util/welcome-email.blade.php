<p>Namaste {{ $data['customerName'] }},</p>

<p>
	@if(isset($data['eventName']) && $data['eventName'] && isset($data['partyDateTime']) && $data['partyDateTime'])
		Thanks for taking time to call us. Our party planning expert shall make sure that all your requirements for
		<strong>{{ $data['eventName'] }}</strong> on <strong>{{ $data['partyDateTime'] }}</strong> are met.
	@else
		Thanks for taking time to call us. Our party planning expert shall make sure that all your requirements are met.
	@endif
</p>

@if(isset($data['enquiryId']) && $data['enquiryId'])
	<p>Please note your enquiry number <strong>#{{ $data['enquiryId'] }}</strong> for future reference.</p>
@endif

<p>We look forward to being part of your celebration.</p>

<p>
	Happy Partying,<br/>
	Team Evibe.in <br/>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>

<p style="padding-top: 10px;">
	@include("emails.enquiry.process")
</p>