<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>

		@if($data['proofStatus'])
			<div style="padding:4px; background:#008000; text-align:center;">
				<h2 style="color:#FFFFFF">
					<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px" alt='Success'> Validation Successful
				</h2>
			</div>
		@else
			<div style="padding:4px; background:#BF423C; text-align:center;">
				<h2 style="color:#FFFFFF">
					<img src='http://gallery.evibe.in/main/img/icons/cancel.png' height="20px" width="20px" alt='Success'> Validation Failed
				</h2>
			</div>
		@endif

		<div style="font-size:14px;line-height:22px; padding: 5px 30px 20px; background-color:#FFFFFF;">
			<div style="text-align:center; ">
				<div style="font-size:16px;margin:15px 0;">Dear {{ ucwords($data['customerName']) }},</div>
				<div style="margin:15px 0;">
					@if($data['proofStatus'])
						<div>
							Your id proofs upload have been validated successfully for your party on {{ $data['partyDate'] }}
						</div>
						<div style="margin-top:15px;">
							Kindly use the link to track and manage your order
						</div>
						<div style="margin-top: 15px;">
							<a href='{{ $data['tmoLink'] }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
								Track My Order
							</a>
						</div>
						@if(isset($data['ticketStatusId']) && isset($data['autoPayStatus']) && ($data['ticketStatusId'] == $data['autoPayStatus']))
							<h3 style="font-weight:bold;color:#444444;text-decoration:underline;margin-top:20px;margin-bottom:5px;">Important:</h3>
							<div style="margin:15px 0;">
								This is
								<span style="background: #F4FF97;">NOT BOOKING CONFIRMATION</span>. We will check the availability and confirm your booking/s within next 4 business hours.
							</div>
						@endif
					@else
						<div>
							Your id proof validation failed for your party on {{ $data['partyDate'] }}
						</div>
						<div style="margin-top:15px;">
							Our operations team executive has given the following reasons:
							<div style="margin-top: 5px; font-style: italic;">
								{{ $data['messageToCustomer'] }}
							</div>
						</div>
						<div style="margin-top:15px;">
							Kindly use the link to reapply ID proof immediately
						</div>
						<div style="margin-top: 15px;">
							<a href='{{ $data['tmoLink'] }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
								Upload ID Proof </a>
						</div>
					@endif
				</div>
			</div>
			<div style="margin:15px 0;">
				<div>Thanks in advance,</div>
				<div><a href="{{ $data['evibeLink'] }}" style='color:#333'>Team Evibe.in</a></div>
				<div>
					@if(isset($data['evibePlainPhone']) && $data['evibePlainPhone'])
						{{ $data['evibePlainPhone'] }}
					@endif
				</div>
			</div>
		</div>
	</div>
</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>