<div style="background-color:#ffffff;margin-top:30px;padding:20px;">
	<div style="color:#ef3e75;text-transform:uppercase;font-size:14px;font-weight:bold;">Cancellation & Refunds</div>
	<div style="margin-top: 10px;">
		<div>In case of cancellation, refundable amount is calculated based on the following policies:</div>
		<div>
			<table style="margin-top: 10px; border: 1px solid #DDDDDD; border-spacing: 0; border-collapse: collapse;">
				<tr>
					<th style="border: 1px solid #DDDDDD; padding: 5px;">Party Cancellation Time</th>
					<th style="border: 1px solid #DDDDDD; padding: 5px;">Refund Percentage*</th>
				</tr>
				<tr>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">0 - 24 hours before party</td>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">0%</td>
				</tr>
				<tr>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">1 day- 3 days before party</td>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">25%</td>
				</tr>
				<tr>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">4 days - 10 days before party</td>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">50%</td>
				</tr>
				<tr>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">11 days or above before party</td>
					<td style="border: 1px solid #DDDDDD; padding: 5px;">70%</td>
				</tr>
			</table>
		</div>
		<div style="margin-top: 10px;">* Refund Percentage will be calculated on <b>Total Advance Amount</b></div>
	</div>
</div>