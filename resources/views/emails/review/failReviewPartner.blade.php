<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
	<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
		<div>
			<div style="float: left;">
				<div>
					<a href="http://evibe.in">
						<img src="http://gallery.evibe.in/img/logo/logo_evibe.png" alt="">
					</a>
				</div>
			</div>
			<div style="float:right;">
				<div>
					<span>Phone: </span>
					<span>{{ config('evibe.phone') }}</span>
				</div>
				<div style="padding-top:10px;">Website: <a href="http://evibe.in">www.evibe.in</a></div>
			</div>
			<div style="clear:both;"></div>
		</div>
		<hr>
		<div style="padding-top: 10px;">
			<p>Namaste {{ $vendorName }},</p>
			<p>Greetings from Evibe.in. Trust you are doing great.</p>
			<p>We have received a new review for your party services. Please check below for more details.</p>
			<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
				<tr>
					<td style="border:1px solid #AAA;padding:4px;"><b>Name</b></td>
					<td style="border:1px solid #AAA;padding:4px;">{{ $clientName }}</td>
				</tr>
				<tr>
					<td style="border:1px solid #AAA;padding:4px;"><b>Party Date</b></td>
					<td style="border:1px solid #AAA;padding:4px;">{{ $partyDate }}</td>
				</tr>
				@if (isset($additionalRatings))
					@foreach ($additionalRatings as $additionalRating)
						<tr>
							<td style="border:1px solid #AAA;padding:4px;"><b>{{ $additionalRating['question'] }}</b>
							</td>
							<td style="border:1px solid #AAA;padding:4px;">{{ $additionalRating['rating']." / ". $additionalRating['maxRating']}}</td>
						</tr>
					@endforeach
				@endif
				<tr>
					<td style="border:1px solid #AAA;padding:4px;"><b>Review</b></td>
					<td style="border:1px solid #AAA;padding:4px;">{{ $reviewData }}</td>
				</tr>
				@if($vendorMapTypeId == config('evibe.ticket.type.planners'))
					<tr>
						<td style="border:1px solid #AAA;padding:4px;"><b>Did the staff wear Evibe.in jackets?</b></td>
						<td style="border:1px solid #AAA;padding:4px;">{{ $vendorUniform }}</td>
					</tr>
				@endif
			</table>
			<p>If you would like to inform anything to the client (or) have something to tell, please reply to this email, it will be added to your profile.</p>
			<div>{{ config('evibe.email_salutation') }}</div>
			<div>Your wellwishers at <a href="{{ config(' evibe.live.host') }}" style='color:#333'>Evibe.in</a></div>
		</div>
	</div>
	<div style="padding:0 30px 10px 30px;color:#999;">
		<div style="padding-top:10px;">Like us on Facebook: <a
					href="http://facebook.com/evibe.in">http://facebook.com/evibe.in</a> for all the latest updates.
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as "not spam" and add senders id to contact list or safe list.</div>
<div style="padding-top:10px;font-size:10px;color:#333">
	<div>Disclaimer</div>
	<div>The information contained in this electronic message and any attachments to this message are intended for the exclusive use of the addressee(s) and may contain proprietary, confidential or privileged information. If you are not the intended recipient, you should not disseminate, distribute or copy this e-mail. Please notify the sender immediately and destroy all copies of this message and any attachments.</div>
</div>