<div style="background-color:#F5F5F5;padding:10px;max-width:700px;">
	<div style="padding: 20px 30px 10px 30px;background-color: #FFFFFF;">
		<p>Hi Team,</p>
		<p>We have received a new review for a customer who is not satisfied with the partner. Please go through the details & Accept / Reject it here(<a
					href="{{ config("evibe.hosts.dash")."/reviews/pending" }}">Dash link</a>).
		</p>
		<table style="max-width:600px;border:1px solid #AAA;border-collapse:collapse;">
			<tr>
				<td style="border:1px solid #AAA;padding:4px;"><b>Customer Name</b></td>
				<td style="border:1px solid #AAA;padding:4px;">{{ $clientName }}</td>
			</tr>
			<tr>
				<td style="border:1px solid #AAA;padding:4px;"><b>Partner Name</b></td>
				<td style="border:1px solid #AAA;padding:4px;">{{ $vendorName }}</td>
			</tr>
			<tr>
				<td style="border:1px solid #AAA;padding:4px;"><b>Party Date</b></td>
				<td style="border:1px solid #AAA;padding:4px;">{{ $partyDate }}</td>
			</tr>
			@if (isset($additionalRatings))
				@foreach ($additionalRatings as $additionalRating)
					<tr>
						<td style="border:1px solid #AAA;padding:4px;"><b>{{ $additionalRating['question'] }}</b>
						</td>
						<td style="border:1px solid #AAA;padding:4px;">{{ $additionalRating['rating']." / ". $additionalRating['maxRating']}}</td>
					</tr>
				@endforeach
			@endif
			<tr>
				<td style="border:1px solid #AAA;padding:4px;"><b>Review</b></td>
				<td style="border:1px solid #AAA;padding:4px;">{{ $reviewData }}</td>
			</tr>
			@if($vendorMapTypeId == config('evibe.ticket.type.planners'))
				<tr>
					<td style="border:1px solid #AAA;padding:4px;"><b>Did staff wear Evibe.in jackets?</b></td>
					<td style="border:1px solid #AAA;padding:4px;">{{ $vendorUniform }}</td>
				</tr>
			@endif
			<tr>
				<td style="border:1px solid #AAA;padding:4px;"><b>Ticket Info</b></td>
				<td style="border:1px solid #AAA;padding:4px;"><a
							href="{{ config("evibe.hosts.dash")."/tickets/".$ticketId."/bookings" }}">Dash Link</a>
				</td>
			</tr>
		</table>
		<div style="margin-top:10px">
			<div>Best,</div>
			<div>Admin</div>
		</div>
	</div>
</div>