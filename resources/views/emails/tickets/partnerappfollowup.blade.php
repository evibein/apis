<div>
	<p>Hi Team,</p>
	<p>We have received a lead from Partner App for the existing ticket. Please check details below and follow up:</p>
	<table>
		<tr>
			<td>Name:</td>
			<td> {{ $name }}</td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td> {{ $phone }}</td>
		</tr>
		<tr>
			<td>More Details:</td>
			<td><a href="{{ $dashLink }}" target="_blank">See Ticket Details</a></td>
		</tr>
	</table>
	<p>
		Best, <br/>
		Admin
	</p>
</div>

