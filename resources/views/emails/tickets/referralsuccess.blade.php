<div>
	<p>Hi {{ $partnerName }},</p>
	<p>Thank you for sending the referral.</p>
	<br/>
	<p>We have successfully received referral from Evibe Partner App and working on it.</p>
	<p>Please check details below:</p>
	<table>
		<tr>
			<td>Name:</td>
			<td> {{ $name }}</td>
		</tr>
		<tr>
			<td>Phone:</td>
			<td> {{ $phone }}</td>
		</tr>
		<tr>
			<td>Comments:</td>
			<td>{!! $comments !!}</td>
		</tr>
	</table>
	<p>
		@include('emails.layout.partner_salutation')
	</p>
</div>

