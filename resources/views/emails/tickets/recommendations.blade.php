<p>Namaste {{ $data['name'] }},</p>
<p>Based on your preferences, we've sent you the best recommendations for your party. Please click on the button below to check and shortlist the options as per your choice.</p>
<div style='text-align:center;margin:35px auto;'>
	<a href="{{ $data['recommendationUrl'] }} " style=' text-decoration:none; border-radius: 4px;text-align: center; border:0;font-weight:500; background-color:#fa4052;color:#fffff1;
                                         font-size:20px;padding:10px 12px;'>Click To Shortlist Recommendations</a>
</div>
<p>If you have any trouble in shortlisting the options from the above link, please reply to this email. We will respond to you within 1 Business hour.</p>
<div style="margin-top: 20px;">
	<div style="text-transform: uppercase; font-weight: bold; border-bottom: 1px solid #000000; margin-bottom: 10px; display: inline-block;">Next Steps</div>
	<ol>
		<li>
			<b><span style="color: #ED3E72">Shortlist</span></b>:
			Shortlist your choices. Get your clarifications from our expert planning team (if any).
		</li>
		<li>
			<b><span style="color: #ED3E72">Avail. Check</span></b>:
			We will check availability of option(s) you finalize.
		</li>
		<li>
			<b><span style="color: #ED3E72">Order Process</span></b>:
			If option(s) available, an email with order details will be sent.
		</li>
		<li>
			<b><span style="color: #ED3E72">Book</span></b>:
			Securely pay {{config('evibe.advance.percentage')}}% of total order amount to block your slot.
		</li>
		<li>
			<b><span style="color: #ED3E72">Relax & Enjoy</span></b>:
			A coordinator will be assigned to take care of your party.
		</li>
	</ol>
</div>
<p style="padding-top:10px;"><b><u>Note:</u> Everything is subjected to availability.</b></p>
<p style="padding-top:3px;">
	<div>Thanks in advance,</div>
	<div>{{ $data['handlerName'] }}</div>
	<div>{{ config('evibe.contact.company.phone') }}</div>
</p>