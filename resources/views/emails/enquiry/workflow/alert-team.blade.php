<p>Hello Team,</p>

<p>A customer has placed an enquiry after going through the workflow and shortlisting few options. Kindly confirm with the customer and process the order.</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data["customerPreferredSlot"]) && $data["customerPreferredSlot"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Call Preference</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data["customerPreferredSlot"] }}</td>
		</tr>
	@endif
	@if(isset($data["bookingLikelinessId"]) && $data["bookingLikelinessId"])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Likeliness</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ config("evibe.ticket.bookingLikeliness." . $data["bookingLikelinessId"], "Not Set") }}</td>
		</tr>
	@endif
	@if(isset($data['customerName']) && $data['customerName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerName'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerPhone']) && $data['customerPhone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerPhone'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerEmail']) && $data['customerEmail'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerEmail'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDate']) && $data['partyDate'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

@if(isset($data['optionsCount']) && $data['optionsCount'])
	<p><b>Customer shortlisted options have been included as mappings</b></p>
@endif

<p>
	Best Regards,<br>
	Admin
</p>