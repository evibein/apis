<p>Namaste {{ $data['customerName'] }},</p>

<p>
	This is to confirm that we've have received your party enquiry. Our party planning expert shall contact you shortly. However, if you need anything urgent, kindly reply to this email. We will respond to you ASAP.
</p>

<p>Please note your enquiry number <strong>#{{ $data['enquiryId'] }}</strong> for future reference.</p>

<p>We look forward to being part of your celebration.</p>

<p>
	Happy Partying,<br/>
	Team Evibe.in <br/>
	<a href="http://youtu.be/6lwEXs51qJw" target="_blank" rel="noopener">Why customers choose Evibe.in?</a>
</p>

<p style="padding-top: 10px;">
<div style="margin-top: 15px;">
	<div style="text-transform: uppercase; font-weight: bold; border-bottom: 1px solid #999999; margin-bottom: 10px; display: inline-block;">Next Steps</div>
	<div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/write.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Clarify</span>
				</div>
				<div style="color: #696969;">
					Get all your queries answered from our expert planning team.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/send-symbol.png" alt="provider check" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Avail. Check</span>
				</div>
				<div style="color: #696969;">
					We will check availability of option(s) you finalize.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/finalize.png" alt="order process" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Order Process</span>
				</div>
				<div style="color: #696969;">
					If option(s) available, an email with order details will be sent.
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/credit-card.png" alt="book" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Book</span>
				</div>
				<div style="color: #696969;">
					Securely pay 50% of total order amount to block your slot.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/wine-glasses.png" alt="relax & enjoy" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Relax & Enjoy</span>
				</div>
				<div style="color: #696969;">
					A coordinator will be assigned to take care of your party.
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>
</p>

<div style="margin-top: 15px;">
	<div style=" margin-bottom: 10px; font-family: 'Montserrat', 'Roboto', 'Open Sans';font-size:20px; padding-top:15px">Why Choose Evibe<span style="text-transform: lowercase">.in</span>?</div>
	<div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ config("evibe.hosts.gallery") }}/main/img/icons/guarantee.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Trusted by 1,50,000+ Customers</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ config("evibe.hosts.gallery") }}/main/img/icons/easy-booking.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Stress-Free Party Planning</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{config("evibe.hosts.gallery")}}/main/img/icons/real_photos_reviews.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Real Pictures & Reviews</div>
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;text-align:center">
				<div style="margin-bottom: 4px;">
					<img src="{{ config("evibe.hosts.gallery") }}/main/img/icons/best_deals.png" alt="clarify" style="margin-top: -4px;margin-right: 3px;vertical-align: middle;height:50px;width:50px">
					<div style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Best Prices & Packages</div>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>