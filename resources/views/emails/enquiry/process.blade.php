<div style="margin-top: 15px;">
	<div style="text-transform: uppercase; font-weight: bold; border-bottom: 1px solid #999999; margin-bottom: 10px; display: inline-block;">Next Steps</div>
	<div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/write.png" alt="clarify"
							style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Clarify</span>
				</div>
				<div style="color: #696969;">
					Get all your queries answered from our expert planning team.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/send-symbol.png" alt="provider check"
							style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Avail. Check</span>
				</div>
				<div style="color: #696969;">
					We will check availability of option(s) you finalize.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/finalize.png" alt="order process"
							style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Order Process</span>
				</div>
				<div style="color: #696969;">
					If option(s) available, an email with order details will be sent.
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/credit-card.png" alt="book"
							style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Book</span>
				</div>
				<div style="color: #696969;">
					Securely pay advance amount to block your slot.
				</div>
			</div>
			<div style="width: 150px; display: inline-block; margin-right: 20px; margin-top: 15px; vertical-align: top;">
				<div style="margin-bottom: 4px;">
					<img src="https://gallery.evibe.in/img/icons/wine-glasses.png" alt="relax & enjoy"
							style="margin-top: -4px;margin-right: 3px;vertical-align: middle;">
					<span style="vertical-align: middle;font-size: 16px;color: #ED3E72;">Relax & Enjoy</span>
				</div>
				<div style="color: #696969;">
					A coordinator will be assigned to take care of your party.
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>