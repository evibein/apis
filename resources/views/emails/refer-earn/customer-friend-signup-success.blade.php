@extends("emails.refer-earn.base-template")

@section("content")
	<p class="text-header text-center rc-text" style="-moz-hyphens: none; -ms-hyphens: none; -webkit-hyphens: none; Margin: 0; Margin-bottom: 10px; color: #5B5B5B; font-size: 36px; font-weight: bold; hyphens: none; line-height: 1.25; margin: 0; margin-bottom: 10px; padding: 0; text-align: center">Congrats {{ $data["customerName"] }}, Keep Going 🙌!</p>
	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
		<tbody>
		<tr style="padding: 0; text-align: left; vertical-align: top">
			<td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0A0A0A; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&nbsp;</td>
		</tr>
		</tbody>
	</table>
	<p class="text-subheader text-center rc-text" style="Margin: 0; Margin-bottom: 10px; color: #5B5B5B; font-size: 16px; font-weight: normal; line-height: 1.25; margin: 0; margin-bottom: 10px; padding: 0; text-align: center"> Awesome news. Your friend {{ $data["name"] }} has just signed up! Sweet!<br>You are one step closer to get your &#8377; 200* off reward coupon.
		<br><br><a class="rc-link" href="{{ $data["dashboardUrl"] }}" style="Margin: 0; color: #ED3279; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">Track Your Referrals</a>
	</p>
@endsection