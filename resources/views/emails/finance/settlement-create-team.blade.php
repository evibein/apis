<p>Hello Team,</p>

<p>
	New @if(isset($data['settlementsCount']) && $data['settlementsCount']) {{ $data['settlementsCount'] }} @endif settlements have been create for bookings and adjustments between @if(isset($data['startDate']) && $data['startDate']) {{ $data['startDate'] }} @endif and @if(isset($data['endDate']) && $data['endDate']) {{ $data['endDate'] }} @endif.
	Kindly verify and settle the created settlements.
</p>

<p>
	@if(isset($data['settlementsDashLink']) && $data['settlementsDashLink'])
		<a href="{{ $data['settlementsDashLink'] }}" target="_blank">Dash link to the created settlements</a>
	@endif
</p>

<p>
	<b>Note: </b>Kindly settle the partner payments within 2 days of settlement creation.
</p>

<p>
	Best Regards,<br>
	Admin
</p>