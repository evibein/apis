<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<!-- keep meta for rupee symbol -->
</head>
<body>
<div>
	<div style="text-align:left;">
		<p>Namaste <span style="text-transform: capitalize">{{ $data['partnerName'] }}</span>,</p>
		<p>
			We are pleased to inform that total settlement amount of <span
					style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['settlementAmount'] }} for all the orders executed between
			@if(isset($data['settlementStartDate']) && $data['settlementStartDate']) {{ $data['settlementStartDate'] }} @endif and
			@if(isset($data['settlementEndDate']) && $data['settlementEndDate']) {{ $data['settlementEndDate'] }} @endif
			has been transferred to your registered bank account.
		</p>
		@if(isset($data['partnerDashEmailLink']) && $data['partnerDashEmailLink'])
		<p>
			<div style="text-align: center; margin-top: 15px; margin-bottom: 20px;">
				<a href="{{ $data['partnerDashEmailLink'] }}" target="_blank" style="padding: 4px 12px; border-radius: 4px; display: inline-block; background-color: #ED2E72; color: #FFFFFF; font-size: 20px; font-weight: bold; text-decoration: none;">View Settlement Details</a>
				<div style="margin-top: 10px">Please click the above button for complete settlement details.</div>
			</div>
		</p>
		@endif
		<p>
			A settlement invoice has been attached for your reference. Generally, it will take 1-3 business days for the amount to reflect in your bank account.
		</p>
		<p>
			If you have any queries, please feel free to reply to this email. We will respond to you within 4 Business hours.
		</p>
		<p>
			Thank you very much for being a trusted partner.
		</p>
	</div>
	<div style="margin-top:10px">
		<div>Your well wishers,</div>
		<div>Team Evibe.in</div>
	</div>
</div>
<div style="margin-top: 20px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>
</body>
</html>