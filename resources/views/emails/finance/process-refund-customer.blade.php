<div style="background-color:#F5F5f5;padding:10px 20px 20px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div><a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png'
								alt='evibe.in'></a></div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px;text-align:center;">
				<p>Namaste <span style="text-transform: capitalize">{{ $data['customerName'] }}</span>,</p>
				<p>
					As promised, we have processed the refund of INR {{ $data['refundAmount'] }} for your party booking #{{ $data['bookingId'] }}.
				</p>
				<P>
					It may take up tp 7-10 working days for the amount to reflect in your bank/card based on the
					payment method that choose at the time of booking.
				</P>
				<p>
					Should you have any queries, please reply to this email. We will respond to you within 4 Business hours.
				</p>
			</div>
			<div style="margin-top:10px">
				<div>Best Regards,</div>
				<div>Team Evibe.in</div>
			</div>
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>