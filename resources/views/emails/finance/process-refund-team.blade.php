<p>Hello Team,</p>

<p>
	Refund amount has been transferred to the customer for the party on
	@if(isset($data['partyDateTime']) && $data['partyDateTime']) <b>{{ $data['partyDateTime'] }}</b> @endif
	at
	@if(isset($data['partyDateTime']) && $data['partyDateTime']) <b>{{ $data['partyLocation'] }}</b> @endif
</p>

@if(isset($data['cancelledAt']) && $data['cancelledAt'])
	<p>
		@if(isset($data['customerCancelledAt']) && $data['customerCancelledAt'])
			Ticket Booking has been cancelled by customer
		@elseif(isset($data['handlerCancelledAt']) && $data['handlerCancelledAt'])
			Ticket Booking has been cancelled by us
		@endif
	</p>
@endif

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['refundAmount']) && $data['refundAmount'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Refund Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['refundAmount'] }}</td>
		</tr>
	@endif
	@if(isset($data['refundReference']) && $data['refundReference'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Refund Reference</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['refundReference'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerName']) && $data['customerName'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerName'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerPhone']) && $data['customerPhone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerPhone'] }}</td>
		</tr>
	@endif
	@if(isset($data['customerEmail']) && $data['customerEmail'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['customerEmail'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDateTime']) && $data['partyDateTime'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDateTime'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyLocation']) && $data['partyLocation'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Location</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyLocation'] }}</td>
		</tr>
	@endif
	@if(isset($data['occasion']) && $data['occasion'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Occasion</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['occasion'] }}</td>
		</tr>
	@endif
	@if(isset($data['city']) && $data['city'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>City</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['city'] }}</td>
		</tr>
	@endif
	@if(isset($data['ticketId']) && $data['ticketId'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Ticket Id</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['ticketId'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket</a></td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>