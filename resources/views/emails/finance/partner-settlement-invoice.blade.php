<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
</head>
<body>
<div style="padding: 10px ;width: 100%; font-size: 14px; line-height: 22px;">
	<div style="border: 1px solid black; padding: 5px;">
		<div style="padding: 0 5px;">
			<div style="display: table; width: 100%; margin-top: 10px;">
				<div style="display: table-cell; width: 50%; vertical-align: top;">
					<div>
						<img style="width: 150px;" src='{{ $data['logoFullPath'] }}' alt='evibe.in'>
					</div>
					<div style="text-transform: uppercase; font-weight: 700; margin-top: 8px; font-size: 14px;">
						Invoice
					</div>
				</div>
				<div style="display: table-cell; width: 50%;">
					<div>
						<div>Evibe Technologies Private Limited</div>
						<div>No. 238, Raheja Arcade, 5th Block,</div>
						<div>Koramangala, Bangalore</div>
						<div>Karnataka - 560095</div>
					</div>
					<div style="margin-top: 10px;">
						@if(isset($data['gstin']) && $data['gstin'])
							<div>GSTIN: {{ $data['gstin'] }} </div>
						@endif
						@if(isset($data['pan']) && $data['pan'])
							<div>PAN: {{ $data['pan'] }} </div>
						@endif
					</div>
				</div>
			</div>
			<div style="display: table; width: 100%; margin-top: 30px;">
				<div style="display: table-cell; width: 50%; padding-right: 10px;">
					<div style="text-transform: capitalize; font-weight: 700">Issued To</div>
					@if(isset($data['partnerDetails']['billingAddress']) && $data['partnerDetails']['billingAddress'])
						<div style="text-transform: capitalize;">
							{{ $data['partnerDetails']['billingAddress'] }}
						</div>
					@else
						<div style="text-transform: capitalize;">
							@if(isset($data['partnerDetails']['firstName']) && $data['partnerDetails']['firstName'])
								{{ $data['partnerDetails']['firstName'] }}
							@else
								Partner
							@endif
						</div>
						<div style="text-transform: capitalize;">
							@if(isset($data['partnerDetails']['companyName']) && $data['partnerDetails']['companyName'])
								{{ $data['partnerDetails']['companyName'] }}
							@else
								Your Company
							@endif
						</div>
						<div style="text-transform: capitalize;">
							@if(isset($data['partnerDetails']['area']) && $data['partnerDetails']['area'])
								{{ $data['partnerDetails']['area'] }},
							@endif
							@if(isset($data['partnerDetails']['city']) && $data['partnerDetails']['city'])
								{{ $data['partnerDetails']['city'] }}
							@endif
							@if(isset($data['partnerDetails']['zip']) && $data['partnerDetails']['zip'])
								- {{ $data['partnerDetails']['zip'] }}
							@endif
						</div>
					@endif
					@if(isset($data['partnerDetails']['gstin']) && $data['partnerDetails']['gstin'])
						<div>
							GSTIN: {{ $data['partnerDetails']['gstin'] }}
						</div>
					@endif
				</div>
				<div style="display: table-cell; width: 50%;">
					<div>
						<div style="text-transform: capitalize; font-weight: 700">Invoice Details</div>
						<div>Number: @if(isset($data['invoiceNumber']) && $data['invoiceNumber']) {{ $data['invoiceNumber'] }} @endif</div>
						<div>Date: @if(isset($data['date']) && $data['date']) {{ $data['date'] }} @endif</div>
						<div>For: @if(isset($data['settlementStartDate']) && $data['settlementStartDate']) {{ $data['settlementStartDate'] }} @endif - @if(isset($data['settlementEndDate']) && $data['settlementEndDate']) {{ $data['settlementEndDate'] }} @endif</div>
					</div>
					<div style="margin-top: 10px;">
						<div style="border: 1px solid black; padding: 5px 10px; color: #8000FF; font-weight: 600;">
							<span>Net Due Amount: </span>
							@if(isset($data['settlementAmount']) && $data['settlementAmount'])
								<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>
								<span>{{ $data['settlementAmount'] }}</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<div style="margin-top: 40px;">
			<div style="font-weight: 700;">
				(I) Section: Completed Orders
			</div>
			<div style="margin-top: 10px;">
				@if(isset($data['settlementBookingsList']) && count($data['settlementBookingsList']))
					<div>
						@if(isset($data['settlementStartDate']) && $data['settlementStartDate'] && isset($data['settlementEndDate']) && $data['settlementEndDate'])
							List of all the party orders completed between <b>{{ $data['settlementStartDate'] }}</b> to
							<b>{{ $data['settlementEndDate'] }}</b>.
						@else
							List of all the party orders completed.
						@endif
					</div>
					<div style="margin-top: 10px;">
						<table style="font-size: 14px; width: 100%; border: 1px solid black; border-collapse: collapse; text-align: left; vertical-align: top;">
							<tr>
								<th style="border: 1px solid black; padding: 5px;">No.</th>
								<th style="border: 1px solid black; padding: 5px;">Party Details</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Order Amount</div>
									<div>(A)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Advance Received</div>
									<div>(B)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Customer Refund</div>
									<div>(C)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Service Fee @if(isset($data['commissionRate']) && $data['commissionRate']) @ {{ $data['commissionRate'] }}% @else @ {{ config('evibe.default.evibe_service_fee') * 100 }}% @endif</div>
									<div>(D)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>GST @ {{ config('evibe.default.gst') * 100 }}%</div>
									<div>(E)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Due Amount</div>
									<div>(B-C-D-E)</div>
								</th>
							</tr>
							@foreach($data['settlementBookingsList'] as $key => $bookingSettlement)
								<tr valign="top">
									<td style="border: 1px solid black; padding: 5px;">{{ $key + 1 }}</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['partyDate']) && $bookingSettlement['partyDate'])
											<div>{{ $bookingSettlement['partyDate'] }}</div>
										@endif
										@if(isset($bookingSettlement['bookingId']) && $bookingSettlement['bookingId'])
											<div>{{ $bookingSettlement['bookingId'] }}</div>
										@endif
										@if(isset($bookingSettlement['customerName']) && $bookingSettlement['customerName'])
											<div>{{ $bookingSettlement['customerName'] }}</div>
										@endif
										@if(isset($bookingSettlement['partyLocation']) && $bookingSettlement['partyLocation'])
											<div>{{ $bookingSettlement['partyLocation'] }}</div>
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['bookingAmount']) && $bookingSettlement['bookingAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['bookingAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['advanceAmount']) && $bookingSettlement['advanceAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['advanceAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['partnerBoreRefund']) && $bookingSettlement['partnerBoreRefund'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['partnerBoreRefund'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['evibeServiceFee']) && $bookingSettlement['evibeServiceFee'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['evibeServiceFee'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['gst']) && $bookingSettlement['gst'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['gst'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['settlementAmount']) && $bookingSettlement['settlementAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['settlementAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
								</tr>
							@endforeach
							<tr valign="top">
								<th colspan="2" style="border: 1px solid black; padding: 5px;">Grand Total</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalBookingOrderAmount']) && $data['totalBookingOrderAmount'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingOrderAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalBookingAdvanceAmount']) && $data['totalBookingAdvanceAmount'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingAdvanceAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalBookingPartnerBoreRefund']) && $data['totalBookingPartnerBoreRefund'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingPartnerBoreRefund'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalBookingEvibeServiceFee']) && $data['totalBookingEvibeServiceFee'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingEvibeServiceFee'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalBookingGST']) && $data['totalBookingGST'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingGST'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<td style="border: 1px solid black; padding: 5px;">
									<div>
										<div>Total Due:</div>
										<div>
											@if(isset($data['totalBookingSettlementAmount']) && $data['totalBookingSettlementAmount'])
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingSettlementAmount'] }}
											@else
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
											@endif
										</div>
									</div>
									<hr>
									<div>
										<div>Already Settled:</div>
										<div>
											@if(isset($data['totalBookingAlreadySettledAmount']) && $data['totalBookingAlreadySettledAmount'])
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingAlreadySettledAmount'] }}
											@else
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
											@endif
										</div>
									</div>
									<hr>
									<div style="font-weight: 700;">
										<div>Net Due</div>
										<div>
											@if(isset($data['totalBookingSettlementAmount']) && $data['totalBookingSettlementAmount'])
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingSettlementAmount'] }}
											@else
												<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
											@endif
										</div>
									</div>
								</td>
							</tr>
						</table>
					</div>
				@else
					<div>
						@if(isset($data['settlementStartDate']) && $data['settlementStartDate'] && isset($data['settlementEndDate']) && $data['settlementEndDate'])
							No completed party orders between <b>{{ $data['settlementStartDate'] }}</b> to
							<b>{{ $data['settlementEndDate'] }}</b>.
						@else
							No completed party orders.
						@endif
					</div>
				@endif
			</div>
		</div>
		<div style="margin-top: 40px;">
			<div style="font-weight: 700;">
				(II) Section: Cancelled Orders
			</div>
			<div style="margin-top: 10px;">
				@if(isset($data['cancelledBookingsList']) && count($data['cancelledBookingsList']))
					<div>
						@if(isset($data['settlementStartDate']) && $data['settlementStartDate'] && isset($data['settlementEndDate']) && $data['settlementEndDate'])
							List of all the party orders cancelled between <b>{{ $data['settlementStartDate'] }}</b> to
							<b>{{ $data['settlementEndDate'] }}</b>.
						@else
							List of all the party orders cancelled.
						@endif
					</div>
					<div style="margin-top: 10px;">
						<table style="font-size: 14px; width: 100%; border: 1px solid black; border-collapse: collapse; text-align: left; vertical-align: top;">
							<tr>
								<th style="border: 1px solid black; padding: 5px;">No.</th>
								<th style="border: 1px solid black; padding: 5px;">Party Details</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Order Amount</div>
									<div>(A)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Advance Received</div>
									<div>(B)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Customer Refund</div>
									<div>(C)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Service Fee</div>
									<div>(D)</div>
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									<div>Due Amount</div>
									<div>(B-C-D)</div>
								</th>
							</tr>
							@foreach($data['cancelledBookingsList'] as $key => $bookingSettlement)
								<tr valign="top">
									<td style="border: 1px solid black; padding: 5px;">{{ $key + 1 }}</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['partyDate']) && $bookingSettlement['partyDate'])
											<div>{{ $bookingSettlement['partyDate'] }}</div>
										@endif
										@if(isset($bookingSettlement['bookingId']) && $bookingSettlement['bookingId'])
											<div>{{ $bookingSettlement['bookingId'] }}</div>
										@endif
										@if(isset($bookingSettlement['customerName']) && $bookingSettlement['customerName'])
											<div>{{ $bookingSettlement['customerName'] }}</div>
										@endif
										@if(isset($bookingSettlement['partyLocation']) && $bookingSettlement['partyLocation'])
											<div>{{ $bookingSettlement['partyLocation'] }}</div>
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['bookingAmount']) && $bookingSettlement['bookingAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['bookingAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['advanceAmount']) && $bookingSettlement['advanceAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['advanceAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['customerRefund']) && $bookingSettlement['customerRefund'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['customerRefund'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['evibeServiceFee']) && $bookingSettlement['evibeServiceFee'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['evibeServiceFee'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
									<td style="border: 1px solid black; padding: 5px;">
										@if(isset($bookingSettlement['settlementAmount']) && $bookingSettlement['settlementAmount'])
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $bookingSettlement['settlementAmount'] }}
										@else
											<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
										@endif
									</td>
								</tr>
							@endforeach
							<tr valign="top">
								<th colspan="2" style="border: 1px solid black; padding: 5px;">Grand Total</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalCancelledBookingOrderAmount']) && $data['totalCancelledBookingOrderAmount'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingOrderAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalCancelledBookingAdvanceAmount']) && $data['totalCancelledBookingAdvanceAmount'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingAdvanceAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalCancelledBookingCustomerRefund']) && $data['totalCancelledBookingCustomerRefund'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingCustomerRefund'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<th style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalCancelledBookingEvibeServiceFee']) && $data['totalCancelledBookingEvibeServiceFee'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingEvibeServiceFee'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</th>
								<td style="border: 1px solid black; padding: 5px;">
									@if(isset($data['totalCancelledBookingSettlementAmount']) && $data['totalCancelledBookingSettlementAmount'])
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingSettlementAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</td>
							</tr>
						</table>
					</div>
				@else
					<div>
						@if(isset($data['settlementStartDate']) && $data['settlementStartDate'] && isset($data['settlementEndDate']) && $data['settlementEndDate'])
							No cancelled party orders between <b>{{ $data['settlementStartDate'] }}</b> to
							<b>{{ $data['settlementEndDate'] }}</b>.
						@else
							No cancelled party orders.
						@endif
					</div>
				@endif
			</div>
		</div>
		<div style="margin-top: 40px;">
			<div style="font-weight: 700;">
				(III) Section: Adjustments
			</div>
			<div style="margin-top: 10px;">
				@if(isset($data['adjustmentsList']) && count($data['adjustmentsList']))
					<table style="font-size: 14px; width: 100%; border: 1px solid black; border-collapse: collapse; text-align: left; vertical-align: top;">
						<tr>
							<th style="border: 1px solid black; padding: 5px;">S. No</th>
							<th style="border: 1px solid black; padding: 5px;">Adjustment Details</th>
							<th style="border: 1px solid black; padding: 5px;">Adjustment Amount</th>
						</tr>
						@foreach($data['adjustmentsList'] as $key => $adjustment)
							<tr valign="top">
								<td style="border: 1px solid black; padding: 5px;">{{ $key + 1 }}</td>
								<td style="border: 1px solid black; padding: 5px;">
									@if(isset($adjustment['bookingId']) && $adjustment['bookingId'])
										<div style="margin-bottom: 10px;">
											<div>
												Party Date:
												@if(isset($adjustment['partyDate']) && $adjustment['partyDate'])
													{{ $adjustment['partyDate'] }}
												@else
													--
												@endif
											</div>
											<div>
												Customer Name:
												@if(isset($adjustment['customerName']) && $adjustment['customerName'])
													{{ $adjustment['customerName'] }}
												@else
													--
												@endif
											</div>
											<div>
												Booking Id:
												@if(isset($adjustment['bookingId']) && $adjustment['bookingId'])
													{{ $adjustment['bookingId'] }}
												@else
													--
												@endif
											</div>
										</div>
									@endif
									<div>
										<div>Adjustment Comments:</div>
										<div>
											@if(isset($adjustment['adjustmentComments']) && $adjustment['adjustmentComments'])
												{{ $adjustment['adjustmentComments'] }}
											@else
												--
											@endif
										</div>
									</div>
								</td>
								<td style="border: 1px solid black; padding: 5px;">
									@if(isset($adjustment['adjustmentAmount']) && $adjustment['adjustmentAmount'])
										@if(isset($adjustment['payEvibe']) && $adjustment['payEvibe']) - @endif
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $adjustment['adjustmentAmount'] }}
									@else
										<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
									@endif
								</td>
							</tr>
						@endforeach
						<tr valign="top">
							<th colspan="2" style="border: 1px solid black; padding: 5px;">Grand Total</th>
							<th style="border: 1px solid black; padding: 5px;">
								@if(isset($data['totalAdjustmentAmount']) && $data['totalAdjustmentAmount'])
									<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalAdjustmentAmount'] }}
								@else
									<span style="font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
								@endif
							</th>
						</tr>
					</table>
				@else
					<div>No applicable adjustments for this period.</div>
				@endif
			</div>
		</div>
		<div style="margin-top: 40px;">
			<table style="font-size: 14px; width: 100%; border: 1px solid black; border-collapse: collapse; text-align: center; vertical-align: top;">
				<tr>
					<td style="border: 1px solid black; padding: 5px;">
						(I) Net Due - Completed Orders:
						<div>
							@if(isset($data['totalBookingSettlementAmount']) && $data['totalBookingSettlementAmount'])
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalBookingSettlementAmount'] }}
							@else
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
							@endif
						</div>
					</td>
				</tr>
				<tr>
					<td style="border: 1px solid black; padding: 5px;">
						(I) Net Due - Cancelled Orders:
						<div>
							@if(isset($data['totalCancelledBookingSettlementAmount']) && $data['totalCancelledBookingSettlementAmount'])
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalCancelledBookingSettlementAmount'] }}
							@else
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
							@endif
						</div>
					</td>
				</tr>
				<tr>
					<td style="border: 1px solid black; padding: 5px;">
						(III) Net Due - Adjustments:
						<div>
							@if(isset($data['totalAdjustmentAmount']) && $data['totalAdjustmentAmount'])
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['totalAdjustmentAmount'] }}
							@else
								<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
							@endif
						</div>
					</td>
				</tr>
				<tr style="color: #8000FF;">
					<td style="border: 1px solid black; padding: 5px;">
						<div style="font-weight: 700;">
							Net Due:
							<div>
								@if(isset($data['settlementAmount']) && $data['settlementAmount'])
									<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>{{ $data['settlementAmount'] }}
								@else
									<span style="font-size: 18px; font-family: DejaVu Sans, sans-serif;">&#8377;</span>0
								@endif
							</div>
						</div>
						@if(isset($data['settlementAmountText']) && $data['settlementAmountText'])
							<div>
								{{ $data['settlementAmountText'] }}
							</div>
						@endif
					</td>
				</tr>
			</table>
		</div>
		@if(isset($data['partnerDetails']['accountNumber']) && $data['partnerDetails']['accountNumber'])
			<div style="margin-top: 40px;">
				<div style="font-weight: bold;">
					Amount Transferred To
				</div>
				@if(isset($data['partnerDetails']['accountNumber']) && $data['partnerDetails']['accountNumber'])
					<div>
						<span>A/C Number: </span>
						<span>{{ $data['partnerDetails']['accountNumber'] }}</span>
					</div>
				@endif
				@if(isset($data['partnerDetails']['bankName']) && $data['partnerDetails']['bankName'])
					<div>
						<span>Bank: </span>
						<span>{{ $data['partnerDetails']['bankName'] }}</span>
					</div>
				@endif
				@if(isset($data['partnerDetails']['ifscCode']) && $data['partnerDetails']['ifscCode'])
					<div>
						<span>IFSC Code: </span>
						<span>{{ $data['partnerDetails']['ifscCode'] }}</span>
					</div>
				@endif
			</div>
		@endif
		<div style="margin-top: 40px;">
			<div>
				We thank you for your Business.
			</div>
			<div style="margin-top: 20px; font-weight: 700;">
				Note: This is an auto generated invoice, no signature is required.
			</div>
		</div>
	</div>
</div>
</body>
</html>
