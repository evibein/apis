<p>Hello Team,</p>

<p>A refund has been initiated because of a booking cancellation.
</p>

<p>
	@if(isset($data['customerCancelledAt']) && $data['customerCancelledAt'])
		Cancellation is done by the customer
	@else

	@endif
</p>

<table style="border-collapse: collapse;text-align: left;">
	<tr>
		<th colspan="2" style="border: 1px solid #dddddd; padding: 8px; text-align: center;">Available Details</th>
	</tr>
	@if(isset($data['refundAmount']) && $data['refundAmount'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Refund Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['refundAmount'] }}</td>
		</tr>
	@endif
	@if(isset($data['name']) && $data['name'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Name</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['name'] }}</td>
		</tr>
	@endif
	@if(isset($data['phone']) && $data['phone'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Phone</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['phone'] }}</td>
		</tr>
	@endif
	@if(isset($data['email']) && $data['email'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Customer Email</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['email'] }}</td>
		</tr>
	@endif
	@if(isset($data['partyDate']) && $data['partyDate'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Party Date</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partyDate'] }}</td>
		</tr>
	@endif
	@if(isset($data['bookingAmount']) && $data['bookingAmount'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Booking Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['bookingAmount'] }}</td>
		</tr>
	@endif
	@if(isset($data['advanceAmount']) && $data['advanceAmount'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Advance Amount</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['advanceAmount'] }}</td>
		</tr>
	@endif
	@if(isset($data['evibeFee']) && $data['evibeFee'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Evibe Fee</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['evibeFee'] }}</td>
		</tr>
	@endif
	@if(isset($data['partnerFee']) && $data['partnerFee'])
		<tr>
			<td style="border: 1px solid #dddddd; padding: 8px;"><b>Partner Fee</b></td>
			<td style="border: 1px solid #dddddd; padding: 8px;">{{ $data['partnerFee'] }}</td>
		</tr>
	@endif
	@if(isset($data['dashLink']) && $data['dashLink'])
		<tr>
			<td colspan="2" style="border: 1px solid #dddddd; padding: 8px;"><a
						href="{{ $data['dashLink'] }}">Dash link to the ticket booking</a></td>
		</tr>
	@endif
</table>

<p>
	Best Regards,<br>
	Admin
</p>