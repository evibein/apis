<div style="padding:10px">
	<p>
		Hi Team,
	</p>
	<p>A customer has updated his invalid email with a correct email.</p>
	<table border="1" style="margin-top:10px; border-collapse: collapse">
		<tr>
			<th style="padding:8px">Customer Name</th>
			<td style="padding:8px">
				@if(isset($data['customerName']))
					{{ $data['customerName'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Customer Phone</th>
			<td style="padding:8px">
				@if(isset($data['customerPhone']))
					{{ $data['customerPhone'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Invalid Email Address</th>
			<td style="padding:8px">
				@if(isset($data['invalidEmailAddress']))
					{{ $data['invalidEmailAddress'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Corrected Email Address</th>
			<td style="padding:8px">
				@if(isset($data['correctedEmail']))
					{{ $data['correctedEmail'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 5px 10px;">
				@if(isset($data['dashTicketLink']))
					<a href="{{ $data['dashTicketLink'] }}" target="_blank">DASH link to ticket</a>
				@else
					--
				@endif
			</td>
		</tr>
	</table>
</div>