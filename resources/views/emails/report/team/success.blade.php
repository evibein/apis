Hello {{ $name }},

<p>
	<b>{{ $vendorName }}</b> has reported his arrival for {{ $customerName }}'s party happening at {{ $partyLocation }}.
	Original reporting time is {{ $reportingTime }}, and (s)he / team have reported at
	<b>{{ $reportedAt }}</b> and delayed by
	<b>{{ $delay }}</b> minutes.
</p>
<p>
	Best Regards, <br/>
	Web Admin
</p>