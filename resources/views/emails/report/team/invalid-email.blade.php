<div style="padding:10px">
	<p>
		Hi Team,
	</p>
	<p>We have detected an invalid email address of an user. Kindly check whether the user is a customer, partner or anyone related to our team and do the needful.</p>
	<p>
		<span>Wrong Email Id: </span>
		<span>
			@if(isset($data['invalidEmailAddress']))
				<b>{{ $data['invalidEmailAddress'] }}</b>
			@else
				--
			@endif
		</span>
	</p>
</div>