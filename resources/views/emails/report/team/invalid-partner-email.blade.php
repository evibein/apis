<div style="padding:10px">
	<p>
		Hi Team,
	</p>
	<p>We have detected an invalid email address of a partner. Kindly go through the following details and do the needful.</p>
	<table border="1" style="margin-top:10px; border-collapse: collapse">
		<tr>
			<th style="padding:8px">Partner Name</th>
			<td style="padding:8px">
				@if(isset($data['partnerName']))
					{{ $data['partnerName'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Partner Type</th>
			<td style="padding:8px">
				@if(isset($data['partnerType']))
					{{ $data['partnerType'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Partner Phone</th>
			<td style="padding:8px">
				@if(isset($data['partnerPhone']))
					{{ $data['partnerPhone'] }}
				@else
					--
				@endif
			</td>
		</tr>
		@if(isset($data['isAltEmail']) && $data['isAltEmail'])
			<tr>
				<td colspan="2" style="padding: 5px 10px;">
					This is an alternate email for the partner
				</td>
			</tr>
		@endif
		<tr>
			<th style="padding:8px">Invalid Email Address</th>
			<td style="padding:8px">
				@if(isset($data['invalidEmailAddress']))
					{{ $data['invalidEmailAddress'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<th style="padding:8px">Unsent Email Subject</th>
			<td style="padding:8px">
				@if(isset($data['unsentEmailSubject']))
					{{ $data['unsentEmailSubject'] }}
				@else
					--
				@endif
			</td>
		</tr>
		<tr>
			<td colspan="2" style="padding: 5px 10px;">
				@if(isset($data['dashPartnerLink']))
					<a href="{{ $data['dashPartnerLink'] }}" target="_blank">DASH link to partner profile</a>
				@else
					--
				@endif
			</td>
		</tr>
	</table>
</div>