<div style="padding:10px">
	<p>
		Hi Team,
	</p>
	<p>{{ $data["notification"] }}, Please click on the dash link below to see complete details.</p>
	<table border="1" style="margin-top:10px; border-collapse: collapse">
		<tr>
			<td colspan="2" style="padding: 5px 10px;">
				<a href="{{ config("evibe.hosts.dash"). "/tickets/" . $data["ticketId"] . "/bookings" }}" target="_blank">DASH link to ticket</a>
			</td>
		</tr>
	</table>
</div>