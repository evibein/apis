<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div><a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png'
								alt='evibe.in'></a></div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px;background:#BF423C;text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/cancel.png' height="20px" width="20px"
						alt='Success'> Booking Rejected - #{{ $data['booking']['bookingId'] }}
			</h2>
		</div>
		<div style="font-size:14px;line-height:22px; padding:5px 30px 20px;background-color:#FFFFFF;">
			<div>
				<p>Dear {{ $data['booking']['provider']['person'] }},</p>
				<p>
					Based on your consent, we've cancelled the booking order of {!! $data['booking']['bookingInfo'] !!} for {{ $data['booking']['checkInDate'] }} ({{ $data['booking']['checkInTime'] }}) at {{ $data['additional']['venueLocation'] }} as you've
					confirmed it is NOT AVAILABLE.
					There is no action required from your side.
				</p>
				<div style="margin:15px auto;">
					In case you have any queries, please reply to this email (or) call you Evibe.in account manager / {{ $data['evibePhone'] }} (ext: 4). We will respond to you at the earliest.
				</div>
			</div>
			<div style="margin:15px 0;">
				<div>Best Regards,</div>
				<div>Your wellwishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a></div>
			</div>
		</div>
	</div>
</div>