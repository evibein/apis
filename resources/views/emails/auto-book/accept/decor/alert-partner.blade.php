<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div><a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png'
								alt='evibe.in'></a></div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px"
						alt='Success'> Booking Accepted #{{ $data['booking']['bookingId'] }}
			</h2>
		</div>

		<div style="line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div>
				<p>Namaste {{ $data['booking']['provider']['person'] }},</p>
				<p>You have confirmed {!! $data['booking']['bookingInfo'] !!} order for {{ $data['booking']['checkInDate'] }}, {{ $data['booking']['checkInTime'] }} at {{ $data['additional']['venueLocation'] }}.
					Please check below for complete order & delivery details. Customer has already paid advance amount of Rs. {{ $data['advPd'] }}. Balance amount to be collected is Rs. {{ $data['balAmt'] }}.
				</p>
				<div style="margin:15px 0;">
					If you have any queries, please reply to this email (or) contact your Evibe.in account manager / {{ $data['evibePhone'] }} (ext: 4). We will respond at the earliest.
				</div>
			</div>
			<div style="margin:15px 0;">
				<div>Best Regards,</div>
				<div>Your wellwishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a></div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<table style="border-collapse: collapse; width:100%" border="1">
			<tr>
				<td style="padding:10px; width: 50%;vertical-align: top">
					<div style="text-align: left">
						<div style="text-align: center"><b>Order Details</b></div>
						<hr>
						<div><strong>Customer Name:</strong> {{ $data['customer']['name'] }}</div>
						@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
							<div><strong>Occasion:</strong> {{ $data['additional']['eventName'] }}</div>
						@endif
						<div><strong>Decor:</strong> {!! $data['booking']['bookingInfo'] !!}</div>
						<div><strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
							@if($data['additional']['specialNotes'])
								{!! $data['additional']['specialNotes'] !!}
							@else
								--
							@endif
						</div>
						@if(isset($data['checkoutFields']))
							@foreach($data['checkoutFields'] as $key => $value)
								<div><strong>{{ $key }}:</strong> {!! $value !!}</div>
							@endforeach
						@endif
					</div>
				</td>
				<td style="padding:10px; width: 50%;vertical-align: top">
					<div style="text-align: left;">
						<div style="text-align: center"><b>Delivery Details</b></div>
						<hr>
						<div><strong>Date:</strong> {{ $data['booking']['checkInDate'] }}</div>
						<div><strong>Time:</strong> {{ $data['booking']['checkInTime'] }}</div>
						<div><strong>Location:</strong> {{ $data['additional']['venueLocation'] }}</div>
						<div><strong>Full Address:</strong> {{ $data['additional']['venueAddress'] }}</div>
					</div>
				</td>
			</tr>
			<tr>
				<td style="padding:10px; width: 50%">
					<div style="text-align: left">
						<div><strong>Decor Price:</strong> Rs. {{ $data['baseAmt'] }}</div>
						<div><strong>Delivery Charge:</strong>
							@if($data['delivery'] > 0)
								Rs. {{ $data['deliveryAmt'] }}
							@else
								<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>
							@endif
						</div>
						<div><strong>Total Order Amount:</strong> Rs. {{ $data['bookAmt'] }}</div>
					</div>
				</td>
				<td style="padding:10px; width: 50%">
					<div style="text-align: left">
						<div><strong>Advance Paid:</strong> Rs. {{ $data['advPd'] }}</div>
						<div><strong>Balance Amount:</strong> Rs. {{ $data['balAmt'] }}</div>
					</div>
				</td>
			</tr>
		</table>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Customer Information</div>
		<ul style="display:table;table-layout:fixed;padding:0;margin:10px 0 0 0;list-style:none;width:100%;">
			<li style="display:table-cell;">
				<div><b>Name</b></div>
				<div>{{ $data['customer']['name'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Phone</b></div>
				<div>{{ $data['customer']['phone'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Alt. Phone</b></div>
				@if(isset($data['additional']['altPhone']))
					<div>{{ $data['additional']['altPhone'] }}</div>
				@else
					<div> --</div>
				@endif
			</li>
		</ul>
	</div>

	<div style="background-color:#FFFFFF;margin-top:30px;padding:20px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Next Steps</div>
		<div style="margin-top:15px;">
			<div style="font-weight:bold;text-decoration:underline;">During Event</div>
			<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:14px;">
				<li>You should fulfil the complete order as finalised with the customer with full spirits of customer service and professionalism.</li>
				<li style="padding: 5px">We advise you to click pictures and send it to us after executing the order. In case of any disputes raised from the customer, this will be helpful for you.​ Email to ping@evibe.in (or) WhatsApp on 9108672805.</li>
				<li style="padding: 5px">It is your responsibility to collect balance payment from the customer on the party date (if any).​</li>
				<li style="padding: 5px">A representative from Evibe.in may attend the party for quality check purpose, please co-operate with him/her.</li>
			</ul>
		</div>
		<div style="margin-top:15px;">
			<div style="font-weight:bold;text-decoration:underline;">After Event</div>
			<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:14px;">
				<li>Advance / token amount settlement from Evibe.in will happen at the end of each month after deducting our agreed service charges.</li>
				<li style="padding: 5px">​Evibe.in will collect feedback from customer after the party. We will share any such feedback / suggestions to help you improve your services.</li>
				<li style="padding: 5px">​If you have any queries, please contact your ​Evibe.in ​account manager / call us on 9640204000​ / email us on ping@evibe.in​.​</li>
			</ul>
		</div>
	</div>

	@include('emails.util.cancellation-policy-partner')

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>