<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div><a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png'
								alt='evibe.in'></a></div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px"
						alt='Success'> Booking Confirmed
			</h2>
		</div>

		<div style="line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px;text-align:center;">
				<p>Namaste {{ $data['customer']['name'] }},</p>
				<p>
					We are pleased to inform that your order of {!! $data['booking']['bookingInfo'] !!} is confirmed. Please check below for complete order and partner details.
					Please find the details of our partner below.
				</p>
				<div style='padding: 25px 0 25px;text-align:center;'>
					<a href='{{ config("evibe.live.host") . config("evibe.track_my_order.url") . "?ref=email&id=" . $data["ticketId"] . "&token=" . \Illuminate\Support\Facades\Hash::make($data["ticketId"] . "EVBTMO") }}' style='font-size:20px;text-decoration:none;background-color:#4584ee;color:#ffffff;padding:8px 20px;border-radius:4px;' target='_blank'>
						Track My Order
					</a>
				</div>
				<p>
					If you have any queries, please feel free to contact us on {{ $data['evibePhone'] }} ({{ $data['startTime'] }} - {{ $data['endTime'] }}) or write to us at {{ $data['evibeEmail'] }}.
				</p>
			</div>
			<div style="margin-top:10px">
				<div>Best Regards,</div>
				<div>Team Evibe.in</div>
			</div>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Order Details</div>
		<ul style="display:table;table-layout:fixed;padding:0;margin:10px 0 0 0;list-style:none;width:100%;">
			<li style="display:table-cell;">
				<div><b>Customer Name</b></div>
				<div>{{ $data['customer']['name'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Service</b></div>
				<div>{!! $data['booking']['bookingInfo'] !!}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Booking Id</b></div>
				<div>#{{ $data['booking']['bookingId'] }}</div>
			</li>
		</ul>
		<div style="margin-top:5px;">
			<b>Special Notes*: (Needs approval if not already discussed) </b>
			<span>
				@if($data['additional']['specialNotes'])
					{!! $data['additional']['specialNotes'] !!}
				@else
					--
				@endif
			</span>
		</div>
		@if(isset($data['checkoutFields']))
			@foreach($data['checkoutFields'] as $key => $value)
				<div style="margin-top:5px;">
					<b>{{ $key }}: </b><span>{!! $value !!}</span>
				</div>
			@endforeach
		@endif
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Party Details</div>
		@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
			<div style="margin-top:5px;">
				<b>Occasion: </b><span> {{ $data['additional']['eventName'] }} </span>
			</div>
		@endif
		<div style="margin-top:5px;">
			<b>Party Date & Time: </b><span>{{ $data['booking']['checkInDate'] }}</span> ,<span>{{ $data['partyTime'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Location: </b><span>{{ $data['additional']['venueLocation'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Full Address: </b><span>{{ $data['additional']['venueAddress'] }}</span>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Booking Details</div>
		<div style="margin-top:5px;">
			<b>Total Booking Amount: </b><span>Rs. {{ $data['bookAmt'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Advance Paid: </b><span>Rs. {{ $data['advPd'] }}</span>
		</div>
		<div style="margin-top:5px;">
			<b>Balance Amount: </b><span>Rs. {{ $data['balAmt'] }}</span>
		</div>
	</div>

	<div style="background-color:#FFFFFF;padding:20px;margin-top:30px;display: none">
		<div style="color:#EF3E75;text-transform:uppercase;font-size:14px;font-weight:bold;">Provide Information</div>
		<ul style="display:table;table-layout:fixed;padding:0;margin:10px 0 0 0;list-style:none;width:100%;">
			<li style="display:table-cell;">
				<div><b>Name</b></div>
				<div>{{ $data['booking']['provider']['person'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Phone</b></div>
				<div>{{ $data['booking']['provider']['phone'] }}</div>
			</li>
			<li style="display:table-cell;">
				<div><b>Alt. Phone</b></div>
				@if(isset($data['booking']['provider']['altPhone']))
					<div>{{ $data['booking']['provider']['altPhone'] }}</div>
				@else
					<div> --</div>
				@endif
			</li>
		</ul>
	</div>

	@include('emails.util.cancellation-policy-customer')

	<div style="background-color:#ffffff;margin-top:30px;padding:20px;">
		<div style="color:#ef3e75;text-transform:uppercase;font-size:14px;font-weight:bold;">Terms of booking</div>
		<ul style="list-style-type:decimal;padding:0 15px 0 20px;font-size:13px;">
			<li>All the order details and amount agreed are for direct clients only. Third party transactions are strictly not allowed and is subjected to high penalty.</li>
			<li style="padding-top:5px">All the services booked are valid for party duration of 3 - 4 hours until and unless it is explicitly mentioned.</li>
			<li style="padding-top:5px">Remaining balance payment needs to done in cash only, after your event is completed.</li>
			<li style="padding-top:5px">All the decoration items are on hire basis only. Extra charges are applicable if you want to own it.</li>
			<li style="padding-top:5px">Booking does not include any furniture and fixture.</li>
			<li style="padding-top:5px">Anything not mentioned in order details will cost extra accordingly.</li>
			<li style="padding-top:5px">It is your responsibility to ensure sufficient time is available to execute all the order details mentioned.</li>
			<li style="padding-top:5px">It is your responsibility to ensure that all necessary permissions / copyrights and authorizations are in place.</li>
			<li style="padding-top:5px">It is your responsibility to take care of all your belongings including gifts. We cannot be held liable for any of your assets.</li>
			<li style="padding-top:5px">Although we use all reasonable safety precautions, we cannot be held liable for any casualties arising at any stage.</li>
			<li style="padding-top:5px">Although we have a policy for execution team to not consume any of the food items served at your party, it is your responsibility to ensure and instruct the same on your party date.</li>
			<li style="padding-top:5px">We will use all reasonable endeavors to meet the obligations in a prompt and efficient manner, however we will not accept responsibility for failure or delay caused by circumstances beyond its control.</li>
		</ul>
	</div>

</div>

<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>