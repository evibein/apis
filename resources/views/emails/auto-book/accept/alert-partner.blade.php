<div style="background-color:#F5F5f5;padding:10px 20px;width:700px;font-size:14px;line-height:22px;">
	<div>
		<div style="padding:20px;">
			<div style='float:left;'>
				<div>
					<a href='http://evibe.in'><img src='http://gallery.evibe.in/img/logo/logo_evibe.png' alt='evibe.in'></a>
				</div>
			</div>
			<div style='float:right;'>
				<div><i>Date:</i> {{ $data['date'] }}</div>
				<div style="padding-top:4px;"><i>Phone:</i> {{ $data['evibePhone'] }}</div>
			</div>
			<div style='clear:both;'></div>
		</div>
		<div style="padding:4px; background:#008000; text-align:center;">
			<h2 style="color:#FFFFFF">
				<img src='http://gallery.evibe.in/main/img/icons/check.png' height="20px" width="20px"
						alt='Success'> Order Accepted
			</h2>
		</div>

		<div style="font-size:14px;line-height:22px; padding: 20px 30px 20px; background-color:#FFFFFF;">
			<div style="margin-top:30px; text-align:center; ">
				<div>
					<p>Namaste {{ $data['person'] }},</p>
					<p>You have a confirmed order for {{ $data['partyDateTime'] }} at {{ $data['additional']['venueLocation'] }}.
						Please click below for complete order & delivery details. Customer has already paid advance amount of Rs. {{ $data['totalAdvancePaidStr'] }}. Balance amount to be collected is Rs. {{ $data['totalBalanceAmountStr'] }}.
					</p>
					<div style="text-align: center; margin-top: 20px; margin-bottom: 25px;">
						<a href="{{ $data['partnerDashEmailLink'] }}" target="_blank" style="padding: 8px 20px; border-radius: 4px; display: inline-block; background-color: #ED2E72; color: #FFFFFF; font-size: 20px; font-weight: bold; text-decoration: none;">View My Orders</a>
					</div>
					<div style="margin:15px 0;">
						If you have any queries, please reply to this email (or) contact your Evibe.in account manager / {{ $data['evibePhone'] }} (ext: 4). We will respond at the earliest.
					</div>
				</div>
				<div style="margin-top: 30px;">
					<table style="border-collapse: collapse; width:100%" border="1">
						<tr>
							<td style="padding:10px; width: 100%;vertical-align: top">
								<div style="text-align: left">
									<div style="text-transform: uppercase; color: #ED3E72"><b>Order Details</b></div>
									@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
										<div><strong>Customer Name: </strong>{{ $data['customer']['name'] }}</div>
									@endif
									@if(isset($data['additional']['eventName']) && $data['additional']['eventName'])
										<div><strong>Occasion:</strong> {{ $data['additional']['eventName'] }}</div>
									@endif
									<div><strong>Party Date Time:</strong> {{ $data['partyDateTime'] }}</div>
									<div>
										<strong>Customer Special Notes* (Needs approval if not already discussed):</strong>
										@if($data['additional']['specialNotes'])
											{!! $data['additional']['specialNotes'] !!}
										@else
											--
										@endif
									</div>
									@if(isset($data['bookings']) && $data['bookings'])
										<div style="margin-top: 10px;">
											<table style="border-collapse: collapse; width:100%" border="1">
												@foreach($data['bookings'] as $booking)
													<tr>
														<td style="padding:10px; width: 60%;vertical-align: top">
															<div style="margin-bottom: 10px;">
																<span style="padding:3px 10px;background: #efefef;border-radius:2px;color:#333">{{ $booking['typeBookingDetails'] }}</span>
															</div>
															<div>
																<strong>Booking Details:</strong> {!! $booking['bookingInfo'] !!}
															</div>
															@if(isset($booking['checkoutFields']))
																@foreach($booking['checkoutFields'] as $key => $value)
																	<div><strong>{{ $key }}:</strong> {!! $value !!}</div>
																@endforeach
															@endif
														</td>
														<td style="padding:10px; width: 40%;vertical-align: top">
															@if(isset($booking['itemMapTypeId']) && isset($data['decorTypeId']) && ($booking['itemMapTypeId'] == $data['decorTypeId']))
																<div>
																	<strong>Product Price:</strong> Rs. {{ $booking['productPriceStr'] }}
																</div>
																<div>
																	<strong>Delivery Charge:</strong>
																	@if($booking['transportCharges'] > 0)
																		Rs. {{ $booking['transportChargesStr'] }}
																	@else
																		<span style='color:rgba(0, 169, 12, 0.83)'> Free </span>
																	@endif
																</div>
															@endif
															@if(isset($data['typeTicketAddOn']) && isset($booking['itemMapTypeId']) && ($booking['itemMapTypeId'] == $data['typeTicketAddOn']))
																<div>
																	<strong>Booking Units:</strong> {{ $booking['bookingUnits'] }}
																</div>
															@endif
															<div>
																<strong>Booking Amount:</strong> Rs. {{ $booking['bookingAmountStr'] }}
															</div>
															@if(isset($booking['itemMapTypeId']) && isset($data['venueDealsTypeId']) && ($booking['itemMapTypeId'] == $data['venueDealsTypeId']))
																<div>
																	<strong>Advance Paid:</strong> Rs. {{ $booking['tokenAmountStr'] }}
																</div>
															@else
																<div>
																	<strong>Advance Paid:</strong> Rs. {{ $booking['advanceAmountStr'] }}
																</div>
															@endif
															<div>
																<strong>Balance Amount:</strong> Rs. {{ $booking['balanceAmountStr'] }}
															</div>
														</td>
													</tr>
												@endforeach
											</table>
										</div>
										@if(count($data['bookings']) > 1)
											<div style="margin-top: 10px;">
												<div>
													<strong>Total Booking Amount:</strong> Rs. {{ $data['totalBookingAmountStr'] }}
												</div>
												<div>
													<strong>Total Advance Paid:</strong> Rs. {{ $data['totalAdvancePaidStr'] }}
												</div>
												<div>
													<strong>Total Balance Amount:</strong> Rs. {{ $data['totalBalanceAmountStr'] }}
												</div>
											</div>
										@endif
									@endif
								</div>
							</td>
						</tr>
						@if(isset($data['customer']))
							<tr style="display: none;">
								<td style="padding:10px; width: 100%;vertical-align: top">
									<div style="text-align: left">
										<div style="text-transform: uppercase; color: #ED3E72">
											<b>Customer Details</b>
										</div>
										@if(isset($data['venuePartnerName']) && $data['venuePartnerName'])
											<div><strong>Name: </strong>{{ $data['venuePartnerName'] }} [Venue Manager]
											</div>
											<div><strong>Phone: </strong>{{ $data['venuePartnerPhone'] }}</div>
											<div><strong>Alt. Phone: </strong>{{ $data['$venuePartnerAltPhone'] }}</div>
											<div><strong><u>Note:</u></strong> This order is for
												<strong>{{ $data['customer']['name'] }}</strong> customer. Deliver to the Venue Manager.
											</div>
										@else
											<div><strong>Name: </strong>{{ $data['customer']['name'] }}</div>
											<div><strong>Phone: </strong>{{ $data['customer']['phone'] }}</div>
											<div><strong>Alt. Phone: </strong>{{ $data['customer']['altPhone'] }}</div>
										@endif
									</div>
								</td>
							</tr>
						@endif
						@if(isset($data['venuePartnerTypeId']) && ($data['partnerTypeId'] == $data['venuePartnerTypeId']))
						@else
							<tr>
								<td style="padding:10px; width: 100%;vertical-align: top">
									<div style="text-align: left">
										<div style="text-transform: uppercase; color: #ED3E72">
											<b>Party Venue Details</b>
										</div>
										<div><strong>Location: </strong>{{ $data['additional']['venueLocation'] }}</div>
										<div><strong>Full Address: </strong>{{ $data['additional']['venueAddress'] }}
										</div>
									</div>
								</td>
							</tr>
						@endif
						@if(isset($data['gallery']) && count($data['gallery']))
							<tr>
								<td style="padding:10px; width: 100%;vertical-align: top">
									<div style="text-align: left">
										<div style="text-transform: uppercase; color: #ED3E72">
											<b>Images provided by Customer</b>
										</div>
										@foreach($data['gallery'] as $imageLink)
											<div style="height: 60px; width: 90px; display: inline-block; margin-top: 5px; margin-right: 5px;">
												<img style="height: 100%; width: 100%;" src="{{ $imageLink }}">
											</div>
										@endforeach
									</div>
								</td>
							</tr>
						@endif
						<tr>
							@include('emails.util.cancellation-policy-partner-table')
						</tr>
					</table>
				</div>
				<div style="text-align:left">
					<div style="margin:10px 0 10px;">
						<u>Note:</u> If there is any change in product details, please reply to this email / call {{ $data['bizHead'] }} at the earliest. We will update the same on our website.
					</div>
					<div style="margin:15px 0;">
						<div>Best Regards,</div>
						<div>Your wellwishers at <a href="{{ $data['evibeLink'] }}" style='color:#333'>Evibe.in</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div style="padding-top:10px;font-size:12px;color:#999">If you are receiving the message in Spam or Junk folder, please mark it as 'not spam' and add senders id to contact list or safe list.</div>